import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/models/reference_number_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchDocumentTypeChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<InfoDocumentTypeModel> _listDocumentType = [];
  List<InfoDocumentTypeModel> _listDocumentTypeTemp = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<InfoDocumentTypeModel> get listDocumentType {
    return UnmodifiableListView(this._listDocumentType);
  }

  UnmodifiableListView<InfoDocumentTypeModel> get listDocumentTypeTemp {
    return UnmodifiableListView(this._listDocumentTypeTemp);
  }

  void getDocumentType(BuildContext context) async{
    var _providerListOid = Provider.of<ListOidChangeNotifier>(context, listen: false);
    this._listDocumentType.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "refOne": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id,
      "refTwo": "099",
      "refThree": _providerListOid.customerType,
      "refFour": _providerListOid.customerType == "PER" ? Provider.of<FormMFotoChangeNotifier>(context, listen: false).kegiatanUsahaSelected.id : Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).businessActivitiesModelSelected.id,
      "refFive": _providerListOid.customerType == "PER" ? Provider.of<FormMFotoChangeNotifier>(context, listen: false).jenisKegiatanUsahaSelected.id : Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).businessActivitiesTypeModelSelected.id,
    });
    print(_body);

    final _response = await _http.post(
        "${BaseUrl.urlPublic}proses-dokumen/get_list_dokumen",
        body: _body,
        headers: {"Content-Type":"application/json"}
    );

    List<InfoDocumentTypeModel> _listTemp = [];
    _listTemp.clear();

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result;
      if(_data.isNotEmpty){
        for(int i = 0; i < _data.length; i++){
          _listDocumentType.add(InfoDocumentTypeModel(
              _data[i]['docTypeId'],
              _data[i]['docTypeName'],
              _data[i]['mandatory'],
              _data[i]['display'],
              _data[i]['flag_unit']));
        }
//        var _uniqueDocTypeId = _listTemp.map((e) => e.docTypeId.trim()).toSet().toList();
//        var _uniqueTypeName = _listTemp.map((e) => e.docTypeName.trim()).toSet().toList();
//        var _uniqueMandatory = _listTemp.map((e) => e.mandatory).toSet().toList();
//        var _uniqueDisplay = _listTemp.map((e) => e.display).toSet().toList();
//        var _uniqueFlagUnit = _listTemp.map((e) => e.flag_unit).toSet().toList();

//        for(var i =0; i<_uniqueDocTypeId.length; i++){
//          InfoDocumentTypeModel _myData = InfoDocumentTypeModel(
//              _uniqueDocTypeId[i],
//              _uniqueTypeName[i],
//              _uniqueMandatory[i],
//              _uniqueDisplay[i],
//              _uniqueFlagUnit[i]
//          );
//          this._listDocumentType.add(_myData);
//        }
        loadData = false;
      }
      else{
        showSnackBar("Jenis tidak ditemukan");
        loadData = false;
      }
    }
    else{
      showSnackBar("Error get document type response ${_response.statusCode}");
      loadData = false;
    }
    notifyListeners();
  }

  void searchDocumentType(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listDocumentTypeTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listDocumentType.forEach((dataSectorEconomic) {
        if (dataSectorEconomic.docTypeId.contains(query) || dataSectorEconomic.docTypeName.contains(query)) {
          _listDocumentTypeTemp.add(dataSectorEconomic);
        }
      });
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void clearSearchTemp() {
    _listDocumentTypeTemp.clear();
  }
}
