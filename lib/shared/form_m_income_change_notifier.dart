import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_income_model.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'form_m_info_nasabah_change_notif.dart';

class FormMIncomeChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  NumberFormat _oCcy = NumberFormat("#,##0.00", "en_US");
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');
  TextEditingController _controllerIncome = TextEditingController();
  TextEditingController _controllerSpouseIncome = TextEditingController();
  TextEditingController _controllerOtherIncome = TextEditingController();
  TextEditingController _controllerTotalIncome = TextEditingController();
  TextEditingController _controllerCostOfLiving = TextEditingController();
  TextEditingController _controllerRestIncome = TextEditingController();
  TextEditingController _controllerOtherInstallments = TextEditingController();
  TextEditingController _controllerMonthlyIncome = TextEditingController();
  TextEditingController _controllerCostOfGoodsSold = TextEditingController();
  TextEditingController _controllerGrossProfit = TextEditingController();
  TextEditingController _controllerOperatingCosts = TextEditingController();
  TextEditingController _controllerOtherCosts = TextEditingController();
  TextEditingController _controllerNetProfitBeforeTax = TextEditingController();
  TextEditingController _controllerTax = TextEditingController();
  TextEditingController _controllerNetProfitAfterTax = TextEditingController();
  DbHelper _dbHelper = DbHelper();

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  TextEditingController get controllerOtherInstallments =>
      _controllerOtherInstallments;

  TextEditingController get controllerRestIncome => _controllerRestIncome;

  TextEditingController get controllerCostOfLiving => _controllerCostOfLiving;

  TextEditingController get controllerTotalIncome => _controllerTotalIncome;

  TextEditingController get controllerOtherIncome => _controllerOtherIncome;

  TextEditingController get controllerSpouseIncome => _controllerSpouseIncome;

  TextEditingController get controllerIncome => _controllerIncome;

  TextEditingController get controllerNetProfitAfterTax =>
      _controllerNetProfitAfterTax;

  TextEditingController get controllerTax => _controllerTax;

  TextEditingController get controllerNetProfitBeforeTax =>
      _controllerNetProfitBeforeTax;

  TextEditingController get controllerOtherCosts => _controllerOtherCosts;

  TextEditingController get controllerOperatingCosts =>
      _controllerOperatingCosts;

  TextEditingController get controllerGrossProfit => _controllerGrossProfit;

  TextEditingController get controllerCostOfGoodsSold =>
      _controllerCostOfGoodsSold;

  TextEditingController get controllerMonthlyIncome => _controllerMonthlyIncome;

  RegExInputFormatter get amountValidator => _amountValidator;

  set amountValidator(RegExInputFormatter value) {
    this._amountValidator = value;
    notifyListeners();
  }

  NumberFormat get oCcy => _oCcy;

  set oCcy(NumberFormat value) {
    this._oCcy = value;
    notifyListeners();
  }

  FormatCurrency get formatCurrency => _formatCurrency;

  bool _isMarried = false;

  bool get isMarried => _isMarried;

  set isMarried(bool value) {
    this._isMarried = value;
  }

  void setIsMarried(BuildContext context) {
    var model = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).maritalStatusSelected;
    if(model.id == "02") {
      isMarried = true;
    } else {
      isMarried = false;
    }
  }

  // Pekerjaan Wiraswasta
  void calculateTotalIncomeEntrepreneur() {
    print(isMarried);
    if(isMarried) {
      var _monthlyIncome = _controllerMonthlyIncome.text.isNotEmpty ? double.parse(_controllerMonthlyIncome.text.replaceAll(",", "")) : 0.0;
      // var _spouseIncome = _controllerSpouseIncome.text.isNotEmpty ? double.parse(_controllerSpouseIncome.text.replaceAll(",", "")) : 0.0;
      var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", "")) : 0.0;
      // var _totalIncome = _income + _spouseIncome + _otherIncome;
      var _totalIncome = _monthlyIncome + _otherIncome;
      _controllerTotalIncome.text = _formatCurrency.formatCurrency(_totalIncome.toString());
    } else {
      var _monthlyIncome = _controllerMonthlyIncome.text.isNotEmpty ? double.parse(_controllerMonthlyIncome.text.replaceAll(",", "")) : 0.0;
      var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", "")) : 0.0;
      var _totalIncome = _monthlyIncome + _otherIncome;
      _controllerTotalIncome.text = _formatCurrency.formatCurrency(_totalIncome.toString());
    }
  }

  void calculateGrossProfitEntrepreneur() {
    var _costOfGoodsSold = _controllerCostOfGoodsSold.text.isNotEmpty ? double.parse(_controllerCostOfGoodsSold.text.replaceAll(",", "")) : 0.0;
    var _totalIncome = _controllerTotalIncome.text.isNotEmpty ? double.parse(_controllerTotalIncome.text.replaceAll(",", "")) : 0.0;
    var _grossProfit = _totalIncome - _costOfGoodsSold;
    _controllerGrossProfit.text = _formatCurrency.formatCurrency(_grossProfit.toString());
  }

  void calculateNetProfitBeforeTaxEntrepreneur() {
    var _operatingCosts = _controllerOperatingCosts.text.isNotEmpty ? double.parse(_controllerOperatingCosts.text.replaceAll(",", "")) : 0.0;
    var _otherCosts = _controllerOtherCosts.text.isNotEmpty ? double.parse(_controllerOtherCosts.text.replaceAll(",", "")) : 0.0;
    var _grossProfit = _controllerGrossProfit.text.isNotEmpty ? double.parse(_controllerGrossProfit.text.replaceAll(",", "")) : 0.0;
    var _netProfitBeforeTax = _grossProfit - _operatingCosts - _otherCosts;
    _controllerNetProfitBeforeTax.text = _formatCurrency.formatCurrency(_netProfitBeforeTax.toString());
  }

  void calculateNetProfitAfterTaxEntrepreneur() {
    var _tax = _controllerTax.text.isNotEmpty ? double.parse(_controllerTax.text.replaceAll(",", "")) : 0.0;
    var _netProfitBeforeTax = _controllerNetProfitBeforeTax.text.isNotEmpty ? double.parse(_controllerNetProfitBeforeTax.text.replaceAll(",", "")) : 0.0;
    var _netProfitAfterTax = _netProfitBeforeTax - _tax;
    _controllerNetProfitAfterTax.text = _formatCurrency.formatCurrency(_netProfitAfterTax.toString());
  }

  void calculatedRestOfIncomeEntrepreneur() {
    var _costOfLiving = _controllerCostOfLiving.text.isNotEmpty ? double.parse(_controllerCostOfLiving.text.replaceAll(",", "")) : 0.0;
    var _netProfitATax = _controllerNetProfitAfterTax.text.isNotEmpty ? double.parse(_controllerNetProfitAfterTax.text.replaceAll(",", "")) : 0.0;
    var _restIncome = _netProfitATax - _costOfLiving;
    _controllerRestIncome.text = _formatCurrency.formatCurrency(_restIncome.toString());
  }

  // Pekerjaan Profesional dan Lainnya
  void calculateTotalIncomeNonEntrepreneur() {
    print('isMarried');
    print(isMarried);
    if(isMarried) {
      var _income = _controllerIncome.text.isNotEmpty ? double.parse(_controllerIncome.text.replaceAll(",", "")) : 0.0;
      var _spouseIncome = _controllerSpouseIncome.text.isNotEmpty ? double.parse(_controllerSpouseIncome.text.replaceAll(",", ""))  : 0.0;
      var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", ""))  : 0.0;
      var _totalIncome = _income + _spouseIncome + _otherIncome;
      _controllerTotalIncome.text = _formatCurrency.formatCurrency(_totalIncome.toString());
    } else {
      var _income = _controllerIncome.text.isNotEmpty ? double.parse(_controllerIncome.text.replaceAll(",", "")) : 0.0;
      // var _spouseIncome = 0.0;
      var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", "")) : 0.0;
      var _totalIncome = _income + _otherIncome;
      _controllerTotalIncome.text = _formatCurrency.formatCurrency(_totalIncome.toString());
    }
  }

  void calculateRestIncomeNonEntrepreneur() {
    var _costOfLiving = _controllerCostOfLiving.text.isNotEmpty ? double.parse(_controllerCostOfLiving.text.replaceAll(",", "")) : 0.0;
    var _totalIncome = _controllerTotalIncome.text.isNotEmpty ? double.parse(_controllerTotalIncome.text.replaceAll(",", "")) : 0.0;
    var _restIncome = _totalIncome - _costOfLiving;
    _controllerRestIncome.text = _formatCurrency.formatCurrency(_restIncome.toString());
  }

  // formating apabila tidak di klik done pada keyboard
  void formattingWiraswasta() {
    _controllerMonthlyIncome.text = formatCurrency.formatCurrency(_controllerMonthlyIncome.text);
    _controllerOtherIncome.text = formatCurrency.formatCurrency(_controllerOtherIncome.text);
    _controllerCostOfGoodsSold.text = formatCurrency.formatCurrency(_controllerCostOfGoodsSold.text);
    _controllerOperatingCosts.text = formatCurrency.formatCurrency(_controllerOperatingCosts.text);
    _controllerOtherCosts.text = formatCurrency.formatCurrency(_controllerOtherCosts.text);
    _controllerTax.text = formatCurrency.formatCurrency(_controllerTax.text);
    _controllerCostOfLiving.text = formatCurrency.formatCurrency(_controllerCostOfLiving.text);
    _controllerSpouseIncome.text = formatCurrency.formatCurrency(_controllerSpouseIncome.text);
    _controllerOtherInstallments.text = formatCurrency.formatCurrency(_controllerOtherInstallments.text);
  }

  void formattingProfessionalOther() {
    _controllerIncome.text = formatCurrency.formatCurrency(_controllerIncome.text);
    _controllerSpouseIncome.text = formatCurrency.formatCurrency(_controllerSpouseIncome.text);
    _controllerOtherIncome.text = formatCurrency.formatCurrency(_controllerOtherIncome.text);
    _controllerCostOfLiving.text = formatCurrency.formatCurrency(_controllerCostOfLiving.text);
    _controllerOtherInstallments.text = formatCurrency.formatCurrency(_controllerOtherInstallments.text);
  }

  void clearData(){
    this._autoValidate = false;
    this._controllerIncome.clear();
    this._controllerSpouseIncome.clear();
    this._controllerOtherIncome.clear();
    this._controllerTotalIncome.clear();
    this._controllerCostOfLiving.clear();
    this._controllerRestIncome.clear();
    this._controllerOtherInstallments.clear();
    this._controllerMonthlyIncome.clear();
    this._controllerCostOfGoodsSold.clear();
    this._controllerGrossProfit.clear();
    this._controllerOperatingCosts.clear();
    this._controllerOtherCosts.clear();
    this._controllerNetProfitBeforeTax.clear();
    this._controllerTax.clear();
    this._controllerNetProfitAfterTax.clear();
  }

  // Future<void> clearDataIncome() async {
  void clearDataIncome() {
    this._autoValidate = false;
    // Wiraswasta
    this._controllerMonthlyIncome.clear();
    this._controllerOtherIncome.clear();
    this._controllerTotalIncome.clear();
    this._controllerCostOfGoodsSold.clear();
    this._controllerGrossProfit.clear();
    this._controllerOperatingCosts.clear();
    this._controllerOtherCosts.clear();
    this._controllerNetProfitBeforeTax.clear();
    this._controllerTax.clear();
    this._controllerNetProfitAfterTax.clear();
    this._controllerCostOfLiving.clear();
    this._controllerNetProfitAfterTax.clear();
    this._controllerSpouseIncome.clear();
    this._controllerOtherInstallments.clear();
    // Profesional
    this._controllerIncome.clear();
    this._controllerSpouseIncome.clear();
    // this._controllerOtherIncome.clear();
    // this._controllerTotalIncome.clear();
    // this._controllerCostOfLiving.clear();
    this._controllerRestIncome.clear();
    // this._controllerOtherInstallments.clear();
  }

  void saveToSQLite(BuildContext context){
    var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE;
    List<MS2CustIncomeModel> _listIncome = [];
    if(occupation == "05" || occupation == "07"){
    //  wiraswasta
      if(isMarried){
      //  nikah
        print("usaha nikah");
        _listIncome.add(MS2CustIncomeModel("123", "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 1));
        _listIncome.add(MS2CustIncomeModel("123", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 2));
        _listIncome.add(MS2CustIncomeModel("123", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 3));
        _listIncome.add(MS2CustIncomeModel("123", "-", "004", "HARGA POKOK PENDAPATAN", _controllerCostOfGoodsSold.text.replaceAll(",", ""),
            null, null, null, null, 1, 4));
        _listIncome.add(MS2CustIncomeModel("123", "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
            null, null, null, null, 1, 5));
        _listIncome.add(MS2CustIncomeModel("123", "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
            null, null, null, null, 1, 6));
        _listIncome.add(MS2CustIncomeModel("123", "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
            null, null, null, null, 1, 7));
        _listIncome.add(MS2CustIncomeModel("123", "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 8));
        _listIncome.add(MS2CustIncomeModel("123", "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 9));
        _listIncome.add(MS2CustIncomeModel("123", "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 10));
        _listIncome.add(MS2CustIncomeModel("123", "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text.replaceAll(",", ""),
            null, null, null, null, 1, 11));
        _listIncome.add(MS2CustIncomeModel("123", "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 12));
        _listIncome.add(MS2CustIncomeModel("123", null, "012", "PENDAPATAN PASANGAN", _controllerSpouseIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, null));
        _listIncome.add(MS2CustIncomeModel("123", null, "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
            null, null, null, null, 1, null));
      } else {
        print("usaha belum nikah");
        _listIncome.add(MS2CustIncomeModel("123", "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 1));
        _listIncome.add(MS2CustIncomeModel("123", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 2));
        _listIncome.add(MS2CustIncomeModel("123", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 3));
        _listIncome.add(MS2CustIncomeModel("123", "-", "004", "HARGA POKOK PENDAPATAN", _controllerCostOfGoodsSold.text.replaceAll(",", ""),
            null, null, null, null, 1, 4));
        _listIncome.add(MS2CustIncomeModel("123", "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
            null, null, null, null, 1, 5));
        _listIncome.add(MS2CustIncomeModel("123", "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
            null, null, null, null, 1, 6));
        _listIncome.add(MS2CustIncomeModel("123", "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
            null, null, null, null, 1, 7));
        _listIncome.add(MS2CustIncomeModel("123", "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 8));
        _listIncome.add(MS2CustIncomeModel("123", "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 9));
        _listIncome.add(MS2CustIncomeModel("123", "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 10));
        _listIncome.add(MS2CustIncomeModel("123", "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text.replaceAll(",", ""),
            null, null, null, null, 1, 11));
        _listIncome.add(MS2CustIncomeModel("123", "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 12));
        _listIncome.add(MS2CustIncomeModel("123", null, "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
            null, null, null, null, 1, null));

      }
    } else {
    //  pegawai
      if(isMarried){
        print("pegawai married");
        _listIncome.add(MS2CustIncomeModel("123", "S", "001", "PENDAPATAN PERBULAN", _controllerIncome.text,
          null, null, null, null, 1, 1));
        _listIncome.add(MS2CustIncomeModel("123", null, "012", "PENDAPATAN PASANGAN", _controllerSpouseIncome.text,
            null, null, null, null, 1, null));
        _listIncome.add(MS2CustIncomeModel("123", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text,
            null, null, null, null, 1, 2));
        _listIncome.add(MS2CustIncomeModel("123", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text,
            null, null, null, null, 1, 3));
        _listIncome.add(MS2CustIncomeModel("123", "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text,
            null, null, null, null, 1, 11));
        _listIncome.add(MS2CustIncomeModel("123", "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text,
            null, null, null, null, 1, 12));
        _listIncome.add(MS2CustIncomeModel("123", null, "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text,
            null, null, null, null, 1, null));
      }
      else {
        print("pegawai tdk married");
        _listIncome.add(MS2CustIncomeModel("123", "S", "001", "PENDAPATAN PERBULAN", _controllerIncome.text,
            null, null, null, null, 1, 1));
        _listIncome.add(MS2CustIncomeModel("123", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text,
            null, null, null, null, 1, 2));
        _listIncome.add(MS2CustIncomeModel("123", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text,
            null, null, null, null, 1, 3));
        _listIncome.add(MS2CustIncomeModel("123", "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text,
            null, null, null, null, 1, 11));
        _listIncome.add(MS2CustIncomeModel("123", "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text,
            null, null, null, null, 1, 12));
        _listIncome.add(MS2CustIncomeModel("123", null, "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text,
            null, null, null, null, 1, null));
      }
    }
    _dbHelper.insertMS2CustIncome(_listIncome);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustIncome();
  }

}
