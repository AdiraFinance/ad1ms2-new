import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_income_model.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'constants.dart';

class FormMCompanyPendapatanChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  NumberFormat _oCcy = NumberFormat("#,##0.00", "en_US");
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');
  TextEditingController _controllerMonthlyIncome = TextEditingController();
  TextEditingController _controllerOtherIncome = TextEditingController();
  TextEditingController _controllerTotalIncome = TextEditingController();
  TextEditingController _controllerCostOfRevenue = TextEditingController();
  TextEditingController _controllerGrossProfit = TextEditingController();
  TextEditingController _controllerOperatingCosts = TextEditingController();
  TextEditingController _controllerOtherCosts = TextEditingController();
  TextEditingController _controllerNetProfitBeforeTax = TextEditingController();
  TextEditingController _controllerTax = TextEditingController();
  TextEditingController _controllerNetProfitAfterTax = TextEditingController();
  TextEditingController _controllerOtherInstallments = TextEditingController();
  DbHelper _dbHelper = DbHelper();

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Pendapatan Perbulan
  TextEditingController get controllerMonthlyIncome {
    return this._controllerMonthlyIncome;
  }

  set controllerMonthlyIncome(value) {
    this._controllerMonthlyIncome = value;
    this.notifyListeners();
  }

  // Pendapatan Lainnya
  TextEditingController get controllerOtherIncome {
    return this._controllerOtherIncome;
  }

  set controllerOtherIncome(value) {
    this._controllerOtherIncome = value;
    this.notifyListeners();
  }

  // Total Pendapatan
  TextEditingController get controllerTotalIncome {
    return this._controllerTotalIncome;
  }

  set controllerTotalIncome(value) {
    this._controllerTotalIncome = value;
    this.notifyListeners();
  }

  // Harga Pokok Pendapatan
  TextEditingController get controllerCostOfRevenue {
    return this._controllerCostOfRevenue;
  }

  set controllerCostOfRevenue(value) {
    this._controllerCostOfRevenue = value;
    this.notifyListeners();
  }

  // Laba Kotor
  TextEditingController get controllerGrossProfit {
    return this._controllerGrossProfit;
  }

  set controllerGrossProfit(value) {
    this._controllerGrossProfit = value;
    this.notifyListeners();
  }

  // Biaya Operasional
  TextEditingController get controllerOperatingCosts {
    return this._controllerOperatingCosts;
  }

  set controllerOperatingCosts(value) {
    this._controllerOperatingCosts = value;
    this.notifyListeners();
  }

  // Biaya Lainnya
  TextEditingController get controllerOtherCosts {
    return this._controllerOtherCosts;
  }

  set controllerOtherCosts(value) {
    this._controllerOtherCosts = value;
    this.notifyListeners();
  }

  // Laba Bersih Sebelum Pajak
  TextEditingController get controllerNetProfitBeforeTax {
    return this._controllerNetProfitBeforeTax;
  }

  set controllerNetProfitBeforeTax(value) {
    this._controllerNetProfitBeforeTax = value;
    this.notifyListeners();
  }

  // Pajak
  TextEditingController get controllerTax {
    return this._controllerTax;
  }

  set controllerTax(value) {
    this._controllerTax = value;
    this.notifyListeners();
  }

  // Laba Bersih Setelah Pajak
  TextEditingController get controllerNetProfitAfterTax {
    return this._controllerNetProfitAfterTax;
  }

  set controllerNetProfitAfterTax(value) {
    this._controllerNetProfitAfterTax = value;
    this.notifyListeners();
  }

  // Angsuran Lainnya
  TextEditingController get controllerOtherInstallments {
    return this._controllerOtherInstallments;
  }

  set controllerOtherInstallments(value) {
    this._controllerOtherInstallments = value;
    this.notifyListeners();
  }

  RegExInputFormatter get amountValidator => _amountValidator;

  set amountValidator(RegExInputFormatter value) {
    this._amountValidator = value;
    notifyListeners();
  }

  NumberFormat get oCcy => _oCcy;

  set oCcy(NumberFormat value) {
    this._oCcy = value;
    notifyListeners();
  }

  FormatCurrency get formatCurrency => _formatCurrency;

  void calculateTotalIncome() {
    var _income = _controllerMonthlyIncome.text.isNotEmpty ? double.parse(_controllerMonthlyIncome.text.replaceAll(",", "")) : 0.0;
    var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", "")) : 0.0;
    var _totalIncome = _income + _otherIncome;
    _controllerTotalIncome.text = _formatCurrency.formatCurrency(_totalIncome.toString());
  }

  void calculateGrossProfit() {
    var _costOfRevenue = _controllerCostOfRevenue.text.isNotEmpty ? double.parse(_controllerCostOfRevenue.text.replaceAll(",", "")) : 0.0;
    var _totalIncome = _controllerTotalIncome.text.isNotEmpty ? double.parse(_controllerTotalIncome.text.replaceAll(",", "")) : 0.0;
    var _grossProfit = _totalIncome - _costOfRevenue;
    _controllerGrossProfit.text = _formatCurrency.formatCurrency(_grossProfit.toString());
  }

  void calculateNetProfitBeforeTax() {
    var _operatingCosts = _controllerOperatingCosts.text.isNotEmpty ? double.parse(_controllerOperatingCosts.text.replaceAll(",", "")) : 0.0;
    var _otherCosts = _controllerOtherCosts.text.isNotEmpty ? double.parse(_controllerOtherCosts.text.replaceAll(",", "")) : 0.0;
    var _grossProfit = _controllerGrossProfit.text.isNotEmpty ? double.parse(_controllerGrossProfit.text.replaceAll(",", "")) : 0.0;
    var _netProfitBeforeTax = _grossProfit - _operatingCosts - _otherCosts;
    _controllerNetProfitBeforeTax.text = _formatCurrency.formatCurrency(_netProfitBeforeTax.toString());
  }

  void calculateNetProfitAfterTax() {
    var _tax = _controllerTax.text.isNotEmpty ? double.parse(_controllerTax.text.replaceAll(",", "")) : 0.0;
    var _netProfitBeforeTax = _controllerNetProfitBeforeTax.text.isNotEmpty ? double.parse(_controllerNetProfitBeforeTax.text.replaceAll(",", "")) : 0.0;
    var _netProfitAfterTax = _netProfitBeforeTax - _tax;
    _controllerNetProfitAfterTax.text = _formatCurrency.formatCurrency(_netProfitAfterTax.toString());
  }

  void formattingCompany() {
    _controllerMonthlyIncome.text = formatCurrency.formatCurrency(_controllerMonthlyIncome.text);
    _controllerOtherIncome.text = formatCurrency.formatCurrency(_controllerOtherIncome.text);
    _controllerTotalIncome.text = formatCurrency.formatCurrency(_controllerTotalIncome.text);
    _controllerCostOfRevenue.text = formatCurrency.formatCurrency(_controllerCostOfRevenue.text);
    _controllerGrossProfit.text = formatCurrency.formatCurrency(_controllerGrossProfit.text);
    _controllerOperatingCosts.text = formatCurrency.formatCurrency(_controllerOperatingCosts.text);
    _controllerOtherCosts.text = formatCurrency.formatCurrency(_controllerOtherCosts.text);
    _controllerNetProfitBeforeTax.text = formatCurrency.formatCurrency(_controllerNetProfitBeforeTax.text);
    _controllerTax.text = formatCurrency.formatCurrency(_controllerTax.text);
    _controllerNetProfitAfterTax.text = formatCurrency.formatCurrency(_controllerNetProfitAfterTax.text);
    _controllerOtherInstallments.text = formatCurrency.formatCurrency(_controllerOtherInstallments.text);
  }

  // Future<void> clearDataPendapatan() async {
  void clearDataPendapatan() {
    this._controllerMonthlyIncome.clear();
    this._controllerOtherIncome.clear();
    this._controllerTotalIncome.clear();
    this._controllerCostOfRevenue.clear();
    this._controllerGrossProfit.clear();
    this._controllerOperatingCosts.clear();
    this._controllerOtherCosts.clear();
    this._controllerNetProfitBeforeTax.clear();
    this._controllerTax.clear();
    this._controllerNetProfitAfterTax.clear();
    this._controllerOtherInstallments.clear();
  }

  void saveToSQLite(BuildContext context){
    List<MS2CustIncomeModel> _listIncome = [];
    _listIncome.add(MS2CustIncomeModel("123", "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
        null, null, null, null, 1, 1));
    _listIncome.add(MS2CustIncomeModel("123", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
        null, null, null, null, 1, 2));
    _listIncome.add(MS2CustIncomeModel("123", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
        null, null, null, null, 1, 3));
    _listIncome.add(MS2CustIncomeModel("123", "-", "004", "HARGA POKOK PENDAPATAN", _controllerOperatingCosts.text.replaceAll(",", ""),
        null, null, null, null, 1, 4));
    _listIncome.add(MS2CustIncomeModel("123", "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
        null, null, null, null, 1, 5));
    _listIncome.add(MS2CustIncomeModel("123", "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
        null, null, null, null, 1, 6));
    _listIncome.add(MS2CustIncomeModel("123", "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
        null, null, null, null, 1, 7));
    _listIncome.add(MS2CustIncomeModel("123", "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
        null, null, null, null, 1, 8));
    _listIncome.add(MS2CustIncomeModel("123", "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
        null, null, null, null, 1, 9));
    _listIncome.add(MS2CustIncomeModel("123", "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
        null, null, null, null, 1, 10));
    _listIncome.add(MS2CustIncomeModel("123", null, "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
        null, null, null, null, 1, null));

    _dbHelper.insertMS2CustIncome(_listIncome);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustIncome();
  }
}
