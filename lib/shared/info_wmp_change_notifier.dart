import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_wmp_model.dart';
import 'package:ad1ms2_dev/models/wmp_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class InfoWMPChangeNotifier with ChangeNotifier {

  String _valueWMP;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String get valueWMP => _valueWMP;
  List<WMPModel> _listWMP = [];
  DbHelper _dbHelper = DbHelper();

  set valueWMP(String value) {
    this._valueWMP = value;
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getWMP(BuildContext context) async {
    // var _infoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    // var _infoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    var _providerListOID = Provider.of<ListOidChangeNotifier>(context, listen: false);
    var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerInfApplication = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerInfObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);

    var formatTanggal = DateFormat("ddMMyy");
    var date = formatTanggal.format(_providerInfApplication.initialDateOrder);

    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "paraFinTypeId": _providerListOID.customerType != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfObjectUnit.typeOfFinancingModelSelected.financingTypeId}", // Fin Type, 1 - Konvensional, 2 - Syariah
      "paraObjectId": _providerInfObjectUnit.objectSelected != null ? _providerInfObjectUnit.objectSelected.id : "", // Kode Object
      "paraProdMatrixId": _providerInfObjectUnit.prodMatrixId != null ? _providerInfObjectUnit.prodMatrixId : "", // Kode Product Matrix
      "paraProdChannelId": _providerInfObjectUnit.sourceOrderSelected != null ? _providerInfObjectUnit.sourceOrderSelected.kode : "", // Kode Sumber Order
      "paraProdProgramId": _providerInfObjectUnit.productTypeSelected != null ? _providerInfObjectUnit.productTypeSelected.id : "", // Kode Jenis Produk
      "paraObjectBrandId": _providerInfObjectUnit.brandObjectSelected != null ? _providerInfObjectUnit.brandObjectSelected.id : "", // Kode Object Brand
      "date": date, // Tanggal Aplikasi format ddmmyy
      "aoroId": "ALL", // Kode Status AORO
      "paraInstallmentTypeId": _providerInfCreditStructure.installmentTypeSelected != null ? _providerInfCreditStructure.installmentTypeSelected.id : "", // Kode Jenis Angsuran
      "paraObjectTypeId": _providerInfObjectUnit.objectTypeSelected != null ? _providerInfObjectUnit.objectTypeSelected.id : "", // Kode Tipe Object
      "paraObjectModelId": _providerInfObjectUnit.modelObjectSelected != null ? _providerInfObjectUnit.modelObjectSelected.id : "", // Kode Model Object
      "paraDealOutletId": _providerInfObjectUnit.thirdPartySelected != null ? _providerInfObjectUnit.thirdPartySelected.kode : "", // Kode Pihak Ketiga
      "tenor": _providerInfCreditStructure.periodOfTimeSelected != null ? _providerInfCreditStructure.periodOfTimeSelected : "", // Jangka Waktu
      "lendingRate": _providerInfCreditStructure.controllerInterestRateEffective != null ? _providerInfCreditStructure.controllerInterestRateEffective : "" // Value Eff Rate
    });
    // print(_body);

    final _response = await _http.post(
        "${BaseUrl.urlAcction}adira-acction/acction/service/wmp/getProposalNo",
        body: _body,
        headers: {"Content-Type":"application/json"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      List _data = _result['proposal'];
      if(_data.isEmpty){
        showSnackBar("WMP tidak ditemukan");
        loadData = false;
      } else{
        for(int i=0; i <_data.length; i++){
          _listWMP.add(WMPModel(_data[i]['amount'], _data[i]['deskripsi'], _data[i]['deskripsiProposal'], _data[i]['isHidden'], _data[i]['noProposal'], _data[i]['type'], _data[i]['typeSubsidi']));
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  List<WMPModel> get listWMP => _listWMP;

  void saveToSQLite(){
    List<MS2ApplObjtWmpModel> _listData = [];
    for(int i=0; i<_listWMP.length; i++){
      _listData.add(MS2ApplObjtWmpModel(
        null,
        _listWMP[i].noProposal,
        _listWMP[i].type,
        _listWMP[i].deskripsi,
        _listWMP[i].typeSubsidi,
        _listWMP[i].deskripsiProposal,
        double.parse(_listWMP[i].amount),
        null,
        null,
        null,
        null,
        null,
      ));
    }
    _dbHelper.insertMS2ApplObjtWmp(_listData);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2ApplOBJWmp();
  }
}