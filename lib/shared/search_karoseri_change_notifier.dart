import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/karoseri_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import 'change_notifier_app/information_object_unit_change_notifier.dart';
import 'form_m_company_alamat_change_notif.dart';

class SearchKaroseriChangeNotifier with ChangeNotifier {
    bool _showClear = false;
    TextEditingController _controllerSearch = TextEditingController();

    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    List<KaroseriModel> _listKaroseri = [
//        KaroseriModel("01", "PRORATE"),
//        KaroseriModel("02", "UNIT"),
    ];

    List<KaroseriModel> _listKaroseriTemp = [];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<KaroseriModel> get listKaroseriModel {
        return UnmodifiableListView(this._listKaroseri);
    }

    UnmodifiableListView<KaroseriModel> get listKaroseriTemp {
        return UnmodifiableListView(this._listKaroseriTemp);
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    void getKaroseri(BuildContext context) async{
        var _providerListOid = Provider.of<ListOidChangeNotifier>(context, listen: false);
        var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context,listen: false);
        var _providerAlamatLembaga = Provider.of<FormMCompanyAlamatChangeNotifier>(context,listen: false);
        this._listKaroseri.clear();
        loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);

        var _body = jsonEncode({
            "refOne": _providerListOid.customerType == "PER" ? _providerInfoAlamat.kelurahanSelected.KEC_ID : _providerAlamatLembaga.controllerKelurahan.text
        });
        print(_body);

        final _response = await _http.post(
            // "${BaseUrl.unit}api/parameter/get-group-object",
            "${BaseUrl.urlPublic}pihak-ketiga/get_data_karo",
            body: _body,
            headers: {"Content-Type":"application/json"}
        );
        if(_response.statusCode == 200){
            final _result = jsonDecode(_response.body);
            if(_result.isEmpty){
                showSnackBar("Program tidak ditemukan");
                loadData = false;
            }
            else{
                for(int i=0; i <_result.length; i++){
                    this._listKaroseri.add(
                        KaroseriModel(_result[i]['kode'], _result[i]['deskripsi'])
                    );
                }
                loadData = false;
            }
        }
        else{
            showSnackBar("Error response status ${_response.statusCode}");
            this._loadData = false;
        }
        notifyListeners();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void searchKaroseri(String query) {
        if(query.length < 3) {
            showSnackBar("Input minimal 3 karakter");
        } else {
            _listKaroseriTemp.clear();
            if (query.isEmpty) {
                return;
            }

            _listKaroseri.forEach((dataProgram) {
                if (dataProgram.kode.contains(query) || dataProgram.deskripsi.contains(query)) {
                    _listKaroseriTemp.add(dataProgram);
                }
            });
        }
        notifyListeners();
    }

    void clearSearchTemp() {
        _listKaroseriTemp.clear();
    }
}
