import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:image_picker/image_picker.dart';

class ChooseImagePicker{
  final _picker = ImagePicker();
  Future<Map> imageFromCamera() async{
    var _image = await _picker.getImage(
        source: ImageSource.camera, maxHeight: 1920.0, maxWidth: 1080.0,imageQuality: 50);
    if(_image!= null){
      File _imagePicked = File(_image.path);
      List _splitFileName = _image.path.split("/");
      int _idFileName = _splitFileName.length - 1;
      var _data = {
        "file":_imagePicked,
        "file_name": _splitFileName[_idFileName],
      };
      return _data;
    }
    else{
      return null;
    }
  }

  Future<Map> fileFromGallery() async{
    File _file = await FilePicker.getFile(type: FileType.custom,allowedExtensions: ['pdf','docx','xlsx','jpg','png','jpeg']);
    if(_file != null){
      List _splitFileName = _file.path.split("/");
      int _idFileName = _splitFileName.length - 1;
      var _dataFile = {
        "file":_file,
        "file_name": _splitFileName[_idFileName],
      };
      return _dataFile;
    }
    else{
      return null;
    }
  }
}