import 'dart:collection';

import 'package:ad1ms2_dev/models/collateral_usage_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class SearchCollateralUsageChangeNotifier with ChangeNotifier{
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();

  List<CollateralUsageModel> _listCollateralUsage = [
    CollateralUsageModel("001", "PERSONAL"),
    CollateralUsageModel("002", "Passenger"),
    CollateralUsageModel("003", "PERSONAL/B2B/B2C"),
    CollateralUsageModel("004", "B2C"),
    CollateralUsageModel("005", "Commercial"),
  ];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<CollateralUsageModel> get listCollateralUsage {
    return UnmodifiableListView(this._listCollateralUsage);
  }
}