import 'dart:collection';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_individu_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';
import 'constants.dart';

class FormMInfoNasabahChangeNotif with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  TextEditingController _controllerNoIdentitas = TextEditingController();
  TextEditingController _controllerTglIdentitas = TextEditingController();
  TextEditingController _controllerIdentitasBerlakuSampai =
      TextEditingController();
  TextEditingController _controllerNamaLengkapSesuaiIdentitas =
      TextEditingController();
  TextEditingController _controllerNamaLengkap = TextEditingController();
  TextEditingController _controllerTglLahir = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitas =
      TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitasLOV =
  TextEditingController();
  TextEditingController _controllerJumlahTanggungan = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerNoHp = TextEditingController();
  TextEditingController _controllerNoHp2 = TextEditingController();
  TextEditingController _controllerNoHp3 = TextEditingController();
  TextEditingController _controllerNoNPWP = TextEditingController();
  TextEditingController _controllerNamaSesuaiNPWP = TextEditingController();
  TextEditingController _controllerAlamatSesuaiNPWP = TextEditingController();
  DateTime _initialDateForTglIdentitas =
      DateTime(dateNow.year, dateNow.month, dateNow.day);
  DateTime _initialDateForTglLahir =
      DateTime(dateNow.year, dateNow.month, dateNow.day);
  DateTime _initialDateForTglIdentitasBerlakuSampai =
      DateTime(dateNow.year, dateNow.month, dateNow.day);
  bool _valueCheckBox = true;
  bool _autoValidate = false;
  bool _flag = false;
  bool _isNoHp1WA = false;
  bool _isNoHp2WA = false;
  bool _isNoHp3WA = false;
  String _radioValueGender = "01";
  int _radioValueIsHaveNPWP = 0;
  IdentityModel _identitasModel;
  GCModel _gcModel;
  ReligionModel _religionSelected;
  EducationModel _educationSelected;
  MaritalStatusModel _maritalStatusSelected;
  JenisNPWPModel _jenisNPWPSelected;
  TandaPKPModel _tandaPKPSelected;
  BirthPlaceModel _birthPlaceSelected;
  bool _isEnableFieldFullName = false;
  bool _isEnableFieldBirthDate = false;
  bool _isEnableFieldBirthPlace = false;
  bool _isEnableFieldIdentityNumber = false;
  bool _isGuarantorVisible = true;
  DbHelper _dbHelper = DbHelper();

  List<IdentityModel> _itemsIdentitas = IdentityType().lisIdentityModel;
//  [
//    IdentityModel("01", "KTP"),
//    IdentityModel("03", "PASSPORT"),
//    IdentityModel("04", "SIM"),
//    IdentityModel("05", "KTP Sementara"),
//    IdentityModel("06", "Resi KTP"),
//    IdentityModel("07", "Ket. Domisili"),
//  ];

  List<GCModel> _itemsGC = [
    GCModel("001", "Fleet"),
    GCModel("002", "Retail"),
    GCModel("003", "RETAIL TAC NOL"),
    GCModel("004", "FLEET TAC NOL"),
  ];

  List<ReligionModel> _itemsReligion = [
    ReligionModel("01", "ISLAM"),
    ReligionModel("02", "KATHOLIK"),
    ReligionModel("03", "KRISTEN"),
    ReligionModel("04", "HINDU"),
    ReligionModel("05", "BUDHA"),
    ReligionModel("06", "KONGHUCU"),
    ReligionModel("99", "-"),
  ];

  List<EducationModel> _listEducation = [
    EducationModel("01", "PASCASARJANA"),
    EducationModel("02", "SARJANA"),
    EducationModel("03", "DIPLOMA"),
    EducationModel("04", "SMTA/SEDERAJAT"),
    EducationModel("05", "SMTP/SEDERAJAT"),
    EducationModel("06", "DIBAWAH SMTP"),
    EducationModel("07", "TIDAKSEKOLAH"),
  ];

  List<MaritalStatusModel> _listMaritalStatus = [
    MaritalStatusModel("01", "Single"),
    MaritalStatusModel("02", "Kawin"),
    MaritalStatusModel("03", "Duda/Janda tanpa anak"),
    MaritalStatusModel("04", "Duda/Janda dengan anak"),
  ];

  List<JenisNPWPModel> _listJenisNPWP = [
    JenisNPWPModel("JN01", "BADAN USAHA"),
    JenisNPWPModel("JN01", "PERORANGAN"),
  ];

  List<TandaPKPModel> _listTandaPKP = [
    TandaPKPModel("1", "PKP"),
    TandaPKPModel("2", "NON PKP"),
  ];

  TextEditingController get controllerTglIdentitas {
    return this._controllerTglIdentitas;
  }

  set controllerTglIdentitas(TextEditingController value) {
    this._controllerTglIdentitas = value;
    this.notifyListeners();
  }

  TextEditingController get controllerNoIdentitas {
    return this._controllerNoIdentitas;
  }

  set controllerNoIdentitas(value) {
    this._controllerNoIdentitas = value;
    this.notifyListeners();
  }

  IdentityModel get identitasModel {
    return this._identitasModel;
  }

  set identitasModel(IdentityModel value) {
    if (_valueCheckBox) {
      this._valueCheckBox = false;
    }
    this._identitasModel = value;
    this._controllerNoIdentitas.clear();
    this.notifyListeners();
  }

  UnmodifiableListView<IdentityModel> get items {
    return UnmodifiableListView(this._itemsIdentitas);
  }

  UnmodifiableListView<GCModel> get itemsGC {
    return UnmodifiableListView(this._itemsGC);
  }

  DateTime get initialDateForTglIdentitas => _initialDateForTglIdentitas;

  void selectTglIdentitas(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      // builder: (BuildContext context, Widget child) {
      //  return Theme(
      //    data: ThemeData.light().copyWith(
      //      buttonTheme: ButtonThemeData(
      //          textTheme: ButtonTextTheme.normal
      //      ),
      //      colorScheme: ColorScheme.light(
      //        primary: const Color(0xFFec7e00),
      //        surface: const Color(0xFFec7e00),
      //        onSurface: Colors.black, // tanggal
      //      ),
      //    ),
      //    child: child,
      //  );
      // },
      context: context,
      initialDate: this._initialDateForTglIdentitas,
      firstDate: DateTime(dateNow.year - 5, dateNow.month, dateNow.day),
      lastDate: DateTime(dateNow.year + 5, dateNow.month, dateNow.day));
    if (picked != null) {
      this.controllerTglIdentitas.text = dateFormat.format(picked);
      this._initialDateForTglIdentitas = picked;
      this.notifyListeners();
    } else {
      return;
    }
  }

  void selectTglIdentitasBerlakuSampai(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      // builder: (BuildContext context, Widget child) {
      //   return Theme(
      //     data: ThemeData.light().copyWith(
      //       buttonTheme: ButtonThemeData(
      //           textTheme: ButtonTextTheme.normal
      //       ),
      //       colorScheme: ColorScheme.light(
      //         primary: const Color(0xFF3f4f7f),
      //         surface: const Color(0xFF3f4f7f),
      //         onSurface: Colors.black, // tanggal
      //       ),
      //     ),
      //     child: child,
      //   );
      // },
      context: context,
      initialDate: this._initialDateForTglIdentitasBerlakuSampai,
      firstDate: DateTime(dateNow.year, dateNow.month, dateNow.day),
      lastDate: DateTime(dateNow.year + 7, dateNow.month, dateNow.day)
    );
    if (picked != null) {
      this.controllerIdentitasBerlakuSampai.text = dateFormat.format(picked);
      this._initialDateForTglIdentitasBerlakuSampai = picked;
      this.notifyListeners();
    } else {
      return;
    }
  }

  TextEditingController get controllerNamaLengkapSesuaiIdentitas {
    return this._controllerNamaLengkapSesuaiIdentitas;
  }

  set controllerNamaLengkapSesuaiIdentitas(TextEditingController value) {
    this._controllerNamaLengkapSesuaiIdentitas = value;
    notifyListeners();
  }

  TextEditingController get controllerNamaLengkap {
    return this._controllerNamaLengkap;
  }

  set controllerNamaLengkap(TextEditingController value) {
    this._controllerNamaLengkap = value;
    notifyListeners();
  }

  TextEditingController get controllerTglLahir => _controllerTglLahir;

  DateTime get initialDateForTglLahir => _initialDateForTglLahir;


  set initialDateForTglLahir(DateTime value) {
    this._initialDateForTglLahir = value;
  }

  void selectBirthDate(BuildContext context) async {
    DatePickerShared _datePickerShared = DatePickerShared();
    var _datePickerSelected = await _datePickerShared.selectStartDate(
        context, this._initialDateForTglLahir,
        canAccessNextDay: false);
    if (_datePickerSelected != null) {
      this.controllerTglLahir.text = dateFormat.format(_datePickerSelected);
      this._initialDateForTglLahir = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  TextEditingController get controllerTempatLahirSesuaiIdentitas =>
      _controllerTempatLahirSesuaiIdentitas;

  GCModel get gcModel => _gcModel;

  set gcModel(GCModel value) {
    this._gcModel = value;
    notifyListeners();
  }

  bool get autoValidate {
    return _autoValidate;
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  String validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  TextEditingController get controllerIdentitasBerlakuSampai =>
      _controllerIdentitasBerlakuSampai;

  bool get valueCheckBox {
    return _valueCheckBox;
  }

  set valueCheckBox(bool value) {
    this._valueCheckBox = value;
    this.controllerIdentitasBerlakuSampai.text = "";
    notifyListeners();
  }

  String get radioValueGender {
    return _radioValueGender;
  }

  set radioValueGender(String value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  UnmodifiableListView<ReligionModel> get itemsReligion {
    return UnmodifiableListView(this._itemsReligion);
  }

  ReligionModel get religionSelected {
    return this._religionSelected;
  }

  set religionSelected(ReligionModel value) {
    this._religionSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<EducationModel> get listEducation {
    return UnmodifiableListView(this._listEducation);
  }

  EducationModel get educationSelected {
    return this._educationSelected;
  }

  set educationSelected(EducationModel value) {
    this._educationSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerJumlahTanggungan =>
      _controllerJumlahTanggungan;

  UnmodifiableListView<MaritalStatusModel> get listMaritalStatus {
    return UnmodifiableListView(this._listMaritalStatus);
  }

  MaritalStatusModel get maritalStatusSelected {
    return this._maritalStatusSelected;
  }

  set maritalStatusSelected(MaritalStatusModel value) {
    this._maritalStatusSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerEmail => _controllerEmail;

  TextEditingController get controllerNoHp => _controllerNoHp;

  TextEditingController get controllerNoHp2 => _controllerNoHp2;

  TextEditingController get controllerNoHp3 => _controllerNoHp3;

  int get radioValueIsHaveNPWP => _radioValueIsHaveNPWP;

  set radioValueIsHaveNPWP(int value) {
    this._radioValueIsHaveNPWP = value;
    notifyListeners();
  }

  bool get isNoHp1WA => _isNoHp1WA;

  set isNoHp1WA(bool value) {
    this._isNoHp1WA = value;
    notifyListeners();
  }

  bool get isNoHp2WA => _isNoHp2WA;

  set isNoHp2WA(bool value) {
    this._isNoHp2WA = value;
    notifyListeners();
  }

  bool get isNoHp3WA => _isNoHp3WA;

  set isNoHp3WA(bool value) {
    this._isNoHp3WA = value;
    notifyListeners();
  }

  TextEditingController get controllerNoNPWP => _controllerNoNPWP;

  TextEditingController get controllerNamaSesuaiNPWP =>
      _controllerNamaSesuaiNPWP;

  UnmodifiableListView<JenisNPWPModel> get listJenisNPWP {
    return UnmodifiableListView(this._listJenisNPWP);
  }

  UnmodifiableListView<TandaPKPModel> get listTandaPKP {
    return UnmodifiableListView(this._listTandaPKP);
  }

  JenisNPWPModel get jenisNPWPSelected {
    return this._jenisNPWPSelected;
  }

  set jenisNPWPSelected(JenisNPWPModel value) {
    this._jenisNPWPSelected = value;
    notifyListeners();
  }

  TandaPKPModel get tandaPKPSelected {
    return this._tandaPKPSelected;
  }

  set tandaPKPSelected(TandaPKPModel value) {
    this._tandaPKPSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerAlamatSesuaiNPWP =>
      _controllerAlamatSesuaiNPWP;

  BirthPlaceModel get birthPlaceSelected => _birthPlaceSelected;

  set birthPlaceSelected(BirthPlaceModel value) {
    this._birthPlaceSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerTempatLahirSesuaiIdentitasLOV =>
      _controllerTempatLahirSesuaiIdentitasLOV;

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerTempatLahirSesuaiIdentitasLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  bool get isEnableFieldFullName => _isEnableFieldFullName;

  set isEnableFieldFullName(bool value) {
    this._isEnableFieldFullName = value;
    notifyListeners();
  }

  bool get isEnableFieldBirthDate => _isEnableFieldBirthDate;

  set isEnableFieldBirthDate(bool value) {
    this._isEnableFieldBirthDate = value;
    notifyListeners();
  }

  bool get isEnableFieldBirthPlace => _isEnableFieldBirthPlace;

  set isEnableFieldBirthPlace(bool value) {
    this._isEnableFieldBirthPlace = value;
    notifyListeners();
  }

  bool get isEnableFieldIdentityNumber => _isEnableFieldIdentityNumber;

  set isEnableFieldIdentityNumber(bool value) {
    this._isEnableFieldIdentityNumber = value;
    notifyListeners();
  }

  void clearFormInfoNasabah(){
    this._flag = false;
    this._autoValidate = false;
    this._gcModel = null;
    this._identitasModel = null;
    this._controllerTglIdentitas.clear();
    this._valueCheckBox = false;
    this._controllerIdentitasBerlakuSampai.clear();
    this._controllerNamaLengkapSesuaiIdentitas.clear();
    this._controllerTempatLahirSesuaiIdentitasLOV.clear();
    this._birthPlaceSelected = null;
    this._educationSelected = null;
    this._maritalStatusSelected = null;
    this._controllerNoHp.clear();
    this._isNoHp1WA = false;
    this._initialDateForTglLahir = DateTime(dateNow.year, dateNow.month, dateNow.day);
  }

  void setDefaultValue(){
    this._identitasModel = this._itemsIdentitas[0];
  }

  bool get isGuarantorVisible => _isGuarantorVisible;

  set isGuarantorVisible(bool value) {
    this._isGuarantorVisible = value;
    notifyListeners();
  }

  void checkNeedGuarantor(){
    if(this._maritalStatusSelected.id == "01" && _isNotValidAge()){
      this._isGuarantorVisible = true;
    }
    else{
      this._isGuarantorVisible = false;
    }
    print(this._isGuarantorVisible);
    notifyListeners();
  }

  bool _isNotValidAge(){
    Duration difference = dateNow.difference(_initialDateForTglLahir);
    print(difference.inDays);
    if (difference.inDays < 7670) {
      return true;
    }
    else{
      return false;
    }
  }

  void saveToSQLite(){
    _dbHelper.insertMS2CustomerPersonal(MS2CustomerIndividuModel(null, null, _gcModel, _educationSelected, _identitasModel,
        _controllerNoIdentitas.text, _controllerNamaLengkapSesuaiIdentitas.text, _controllerNamaLengkap.text, "nama alias",
        "degree", _initialDateForTglLahir.toString(), _controllerTempatLahirSesuaiIdentitas.text, _birthPlaceSelected, _radioValueGender,
        _radioValueGender == "01"? "Laki laki" : "Perempuan", "id_date", "id_expired_date", null, _maritalStatusSelected,
        _controllerNoHp.text, "email", "fb", "bb", "kk", "fav_color", "fav_brand", "relation_status_emg", "full_name_id_emg",
        "full_name_emg", "degree_emg", "email_emg", "handphone_emg", "created_date", "created_by", "modified_date", "modified_by",
        1, 2, "no_wa", "no_wa2", "no_wa3"));

    if(_radioValueIsHaveNPWP == 1){
      print("npwp");
      _dbHelper.insertMS2Customer(MS2CustomerModel(null, null, null, _controllerAlamatSesuaiNPWP.text, null, _controllerNoNPWP.text,
        null, null, null, null, null, null, null, null, null, null, null, null, 1));
    }
  }

  Future<bool> deleteSQLite() async{
    print("jalan bro info nasabah");
    return await _dbHelper.deleteMS2CustomerPersonal();
  }

  bool _setupData = false;

  bool get setupData => _setupData;

  set setupData(bool value) {
    this._setupData = value;
  }

  void setDataSQLite() async{
    // setupData = true;
    List _data = await _dbHelper.selectDataInfoNasabah();
    if(_data.isEmpty){
      List _dedup = await _dbHelper.selectDataInfoNasabahFromDataDedup();
      if(_dedup.isNotEmpty){
        this._controllerNamaLengkap.text = _dedup[0]['nama'];
        this._controllerTglLahir.text = dateFormat.format(DateTime.parse(_dedup[0]['tanggal_lahir']));
        this._controllerNoIdentitas.text = _dedup[0]['no_identitas'];
        this._controllerTempatLahirSesuaiIdentitas.text = _dedup[0]['tempat_lahir_sesuai_identitas'];
      }
      // setupData = false;
    }
    else{
      for(int i=0; i < this._itemsGC.length; i++){
        if(_data[0]['cust_group'] == _itemsGC[i].id){
          _gcModel = _itemsGC[i];
        }
      }
      this._controllerNamaLengkap.text = _data[0]['full_name'];
      this._controllerNamaLengkapSesuaiIdentitas.text = _data[0]['full_name_id'];
      print("cek birthdate ${_data[0]['date_of_birth']}");
      this._controllerTglLahir.text = dateFormat.format(DateTime.parse(_data[0]['date_of_birth']));
      // this._controllerTglLahir.text = _data[0]['date_of_birth'];
      this._controllerNoIdentitas.text = _data[0]['id_no'];
      this._controllerTempatLahirSesuaiIdentitas.text = "${_data[0]['place_of_birth']}";
      this._controllerTempatLahirSesuaiIdentitasLOV.text = "${_data[0]['place_of_birth_kabkota']} - ${_data[0]['place_of_birth_kabkota_desc']}";
      this._birthPlaceSelected = BirthPlaceModel(_data[0]['place_of_birth_kabkota'], _data[0]['place_of_birth_kabkota_desc']);
      _data[0]['gender'] == "01" ? _radioValueGender = "01" : _radioValueGender = "02";
      this._controllerNoHp.text = _data[0]['handphone_no'];
      for(int i=0; i < _listEducation.length; i++){
        if(_data[0]['education'] == _listEducation[i].id){
          _educationSelected = _listEducation[i];
        }
      }
      for(int i=0; i < _listMaritalStatus.length; i++){
        if(_data[0]['marital_status'] == _listMaritalStatus[i].id){
          _maritalStatusSelected = _listMaritalStatus[i];
        }
      }
      if(_radioValueIsHaveNPWP == 1){
        List _dataNPWP = await _dbHelper.selectDataInfoNasabahNPWP();
        if(_dataNPWP.isNotEmpty){
          this._controllerAlamatSesuaiNPWP.text = _dataNPWP[0]['npwp_address'];
          this._controllerNoNPWP.text = _dataNPWP[0]['npwp_no'];
        }
      }
      // setupData = false;
    }
    notifyListeners();
  }
}
