import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/task_list_model.dart';
import 'package:ad1ms2_dev/screens/SA/sa_job_mayor.dart';
import 'package:ad1ms2_dev/screens/form_AOS/form_AOS.dart';
import 'package:ad1ms2_dev/screens/form_IA/form_IA.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_parent.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeChangeNotifier with ChangeNotifier{
  List<TaskListModel> _taskList = [];
  String _username;
  String _fullname;

  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<TaskListModel> get taskList => _taskList;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  String get username => _username;

  set username(String value) {
    this._username = value;
    notifyListeners();
  }

  String get fullname => _fullname;

  set fullname(String value) {
    this._fullname = value;
    notifyListeners();
  }

  void navigateForm(String typeFormId, BuildContext context,String orderNo,String orderDate,String custName) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _preferences.setString("order_no", orderNo);
    _preferences.setString("order_date",orderDate);
    _preferences.setString("cust_name",custName);
    _preferences.setString("last_known_state",typeFormId);
    if(typeFormId == "SA"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => SAJobMayor()));
    }
    else if(typeFormId == "AOS"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormAOS()));
    }
    else if(typeFormId == "IA"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormIA()));
    }
    else{
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId)));
    }
  }

  void getTaskList() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    username = _preferences.getString("username");
    fullname = _preferences.getString("fullname");
    this._taskList.clear();
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    try {
    var _body = jsonEncode({
      "P_NIK": _preferences.getString("username"),
      // "10065901"
    });
    print(_body);

    final _response = await _http.post(
      "${BaseUrl.urlPublic}api/tasklist/get-tasklist-ms2",
        // "https://103.110.89.34/public/ms2dev/api/tasklist/get-tasklist-ms2",
        body: _body,
        headers: {"Content-Type":"application/json"}
    );
    print(_response.statusCode);

    if(_response.statusCode == 200){
      List _result = jsonDecode(_response.body);
      print(_result);
      if(_result.isEmpty) {
        print('a');
        showSnackBar("Task List tidak ditemukan");
        // this._loadData = false;
      } else {
        print('b');
        for(int i=0; i < _result.length; i++){
          var _date = DateTime.parse(_result[i]['ORDER_DATE'].toString().replaceAll("T", " "));
          this._taskList.add(
            TaskListModel(
              _result[i]['ORDER_NO'],
              _date,
              _result[i]['CUST_NAME'],
              _result[i]['CUST_TYPE'],
              _result[i]['LAST_KNOWN_STATE'],
              _result[i]['LAST_KNOWN_HANDLED_BY'],
              _result[i]['IDX'],
              _result[i]['SOURCE_APPLICATION'],
              _result[i]['PRIORITY']
            )
          );
        }
        print('c');
        // this._loadData = false;
      }
    } else {
      showSnackBar("Error response status ${_response.statusCode}");
      // this._loadData = false;
    }
    } catch(e) {
      showSnackBar("Error ${e.toString()}");
      // this._loadData = false;
    }
    this._loadData = false;
    print(this._loadData);
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}