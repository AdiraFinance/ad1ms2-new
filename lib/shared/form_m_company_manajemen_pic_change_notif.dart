import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/manajemen_pic_sqlite_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants.dart';

class FormMCompanyManajemenPICChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  int _selectedIndex = -1;
  TypeIdentityModel _typeIdentitySelected;
  TextEditingController _controllerIdentityNumber = TextEditingController();
  TextEditingController _controllerFullNameIdentity = TextEditingController();
  TextEditingController _controllerFullName = TextEditingController();
  TextEditingController _controllerAlias = TextEditingController();
  TextEditingController _controllerDegree = TextEditingController();
  TextEditingController _controllerBirthOfDate = TextEditingController();
  DateTime _initialDateForBirthOfDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  TextEditingController _controllerPlaceOfBirthIdentity = TextEditingController();
  TextEditingController _controllerBirthPlaceIdentityLOV = TextEditingController();
  TextEditingController _controllerPosition = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerHandphoneNumber = TextEditingController();
  BirthPlaceModel _birthPlaceSelected;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  DbHelper _dbHelper = DbHelper();

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<TypeIdentityModel> _listTypeIdentity = [
//    TypeIdentityModel("01", "KTP"),
//    TypeIdentityModel("02", "NPWP"),
    TypeIdentityModel("01", "KTP"),
    TypeIdentityModel("03", "PASSPORT"),
    TypeIdentityModel("04", "SIM"),
    TypeIdentityModel("05", "KTP Sementara"),
    TypeIdentityModel("06", "Resi KTP"),
    TypeIdentityModel("07", "Ket. Domisili"),
  ];

  // Index
  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  // Jenis Identitas
  UnmodifiableListView<TypeIdentityModel> get listTypeIdentity {
    return UnmodifiableListView(this._listTypeIdentity);
  }

  TypeIdentityModel get typeIdentitySelected => _typeIdentitySelected;

  set typeIdentitySelected(TypeIdentityModel value) {
    this._typeIdentitySelected = value;
    this._controllerIdentityNumber.clear();
    notifyListeners();
  }

  // No Identitas
  TextEditingController get controllerIdentityNumber {
    return this._controllerIdentityNumber;
  }

  set controllerIdentityNumber(value) {
    this._controllerIdentityNumber = value;
    this.notifyListeners();
  }

  // Nama Lengkap Sesuai Identitas
  TextEditingController get controllerFullNameIdentity {
    return this._controllerFullNameIdentity;
  }

  set controllerFullNameIdentity(value) {
    this._controllerFullNameIdentity = value;
    this.notifyListeners();
  }

  // Nama Lengkap
  TextEditingController get controllerFullName {
    return this._controllerFullName;
  }

  set controllerFullName(value) {
    this._controllerFullName = value;
    this.notifyListeners();
  }

  // Alias
  TextEditingController get controllerAlias {
    return this._controllerAlias;
  }

  set controllerAlias(value) {
    this._controllerAlias = value;
    this.notifyListeners();
  }

  // Gelar
  TextEditingController get controllerDegree {
    return this._controllerDegree;
  }

  set controllerDegree(value) {
    this._controllerDegree = value;
    this.notifyListeners();
  }

  // Tanggal Lahir
  TextEditingController get controllerBirthOfDate => _controllerBirthOfDate;

  DateTime get initialDateForBirthOfDate => _initialDateForBirthOfDate;

  void selectBirthDate(BuildContext context) async {
    // DatePickerShared _datePickerShared = DatePickerShared();
    // var _datePickerSelected = await _datePickerShared.selectStartDate(
    //     context, this._initialDateForBirthOfDate,
    //     canAccessNextDay: false);
    // if (_datePickerSelected != null) {
    //   this.controllerBirthOfDate.text = dateFormat.format(_datePickerSelected);
    //   this._initialDateForBirthOfDate = _datePickerSelected;
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _datePickerSelected = await selectDate(context, this._initialDateForBirthOfDate);
    if (_datePickerSelected != null) {
      this.controllerBirthOfDate.text = dateFormat.format(_datePickerSelected);
      this._initialDateForBirthOfDate = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  // Tempat Lahir Sesuai Identitas
  TextEditingController get controllerPlaceOfBirthIdentity {
    return this._controllerPlaceOfBirthIdentity;
  }

  set controllerPlaceOfBirthIdentity(value) {
    this._controllerPlaceOfBirthIdentity = value;
    this.notifyListeners();
  }

  // Tempat Lahir Sesuai Identitas LOV
  TextEditingController get controllerBirthPlaceIdentityLOV =>
      _controllerBirthPlaceIdentityLOV;

  set controllerBirthPlaceIdentityLOV(TextEditingController value) {
    this._controllerBirthPlaceIdentityLOV = value;
    notifyListeners();
  }

  // Jabatan
  TextEditingController get controllerPosition {
    return this._controllerPosition;
  }

  set controllerPosition(value) {
    this._controllerPosition = value;
    this.notifyListeners();
  }

  // Email
  TextEditingController get controllerEmail {
    return this._controllerEmail;
  }

  set controllerEmail(value) {
    this._controllerEmail = value;
    this.notifyListeners();
  }

  // Handphone
  TextEditingController get controllerHandphoneNumber {
    return this._controllerHandphoneNumber;
  }

  set controllerHandphoneNumber(value) {
    this._controllerHandphoneNumber = value;
    this.notifyListeners();
  }

  String validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }


  BirthPlaceModel get birthPlaceSelected => _birthPlaceSelected;

  set birthPlaceSelected(BirthPlaceModel value) {
    this._birthPlaceSelected = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerBirthPlaceIdentityLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  // Future<void> clearDataManajemenPIC() async {
  void clearDataManajemenPIC() {
    this._autoValidate = false;
    this._selectedIndex = -1;
    this._typeIdentitySelected = null;
    this._controllerIdentityNumber.clear();
    this._controllerFullNameIdentity.clear();
    this._controllerFullName.clear();
    // this._controllerAlias.clear();
    // this._controllerDegree.clear();
    this._controllerBirthOfDate.clear();
    this._controllerPlaceOfBirthIdentity.clear();
    this._controllerBirthPlaceIdentityLOV.clear();
    // _controllerPosition.clear();
    // _controllerEmail.clear();
    this._controllerHandphoneNumber.clear();
  }

  void saveToSQLite() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _dbHelper.insertMS2CustomerCompanyForPIC(ManagementPICSQLiteModel(this._typeIdentitySelected, this._controllerIdentityNumber.text, this._controllerFullNameIdentity.text, this._controllerFullName.text, this._controllerAlias.text, this._controllerDegree.text, this._controllerBirthOfDate.text, this._controllerFullNameIdentity.text, this._birthPlaceSelected, null, null, null, null, null, null, null, this._controllerHandphoneNumber.text, null, null, null, DateTime.now().toString(), _preferences.getString("username"), null, null, 1, null), "000001");
  }
}
