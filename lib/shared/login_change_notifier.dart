import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:ad1ms2_dev/shared/upper_clipper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:random_string/random_string.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class LoginChangeNotifier with ChangeNotifier {
    TextEditingController _controllerEmail = TextEditingController();
    TextEditingController _controllerPassword = TextEditingController();
    String _randomText;
    TextEditingController _valueRandomText = TextEditingController();
    bool _secureText = true;
    bool _autoValidate = false;
    bool _loginProcess = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    GlobalKey<FormState> _key = GlobalKey<FormState>();

    TextEditingController get controllerEmail => _controllerEmail;
    TextEditingController get controllerPassword => _controllerPassword;
    bool get secureText => _secureText;
    bool get loginProcess => _loginProcess;

    set loginProcess(bool value) {
        _loginProcess = value;
        notifyListeners();
    }

    set secureText(bool value) {
        this._secureText = value;
        notifyListeners();
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
    GlobalKey<FormState> get key => _key;

    showHidePass() {
       _secureText = !_secureText;
       notifyListeners();
    }

    Widget upperPart(BuildContext context) {
        var _size = Screen(MediaQuery.of(context).size);
        return Stack(
            children: <Widget>[
                ClipPath(
                    clipper: UpperClipper(),
                    child: Container(
                        height: _size.hp(35),
                        decoration: BoxDecoration(color: myPrimaryColor),
                    ),
                ),
                Container(
                    margin: EdgeInsets.only(top: _size.hp(4), left: _size.wp(8)),
                    child: Image.asset('img/logo_adira.png', height: 150, width: 150))
            ],
        );
    }

    void setRandomText() {
        _randomText = randomAlphaNumeric(4);
//        notifyListeners();
    }

    void checkLogin(BuildContext context) async{
        this._loginProcess = true;
        SharedPreferences _preference = await SharedPreferences.getInstance();
        String _username = _preference.getString("username");
        if(_username != null){
            this._loginProcess = false;
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Dashboard()));
        }
        else{
            this._loginProcess = false;
        }
        notifyListeners();
    }

    bool _errorRandomNotmatch = false;

    bool get errorRandomNotmatch => _errorRandomNotmatch;

    set errorRandomNotmatch(bool value) {
        this._errorRandomNotmatch = value;
        notifyListeners();
    }

    void validasiRandomText(BuildContext context) {
        if (_valueRandomText.text != _randomText) {
            _errorRandomNotmatch = true;
            autoValidate = true;
            _valueRandomText.clear();
            setRandomText();
        } else {
            _errorRandomNotmatch = false;
        }
    }

    void check(BuildContext context) async{
        validasiRandomText(context);
        if(_controllerEmail.text != "" && _controllerPassword.text != "" && _valueRandomText.text != "") {
            loginProcess = true;
            final ioc = new HttpClient();
            ioc.badCertificateCallback =
                (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            final _form = _key.currentState;
            SharedPreferences _preferences = await SharedPreferences
                .getInstance();
            //        var _body = jsonEncode({
            //            "login":_controllerEmail.text,
            //            "password": _controllerPassword.text
            //        });
            //        print(_body);
            try {
                // if(_form.validate()){
                final _response = await _http.post(
                    "${BaseUrl.urlPublic}api/account/authuser",
                    // "https://103.110.89.34/public/ms2dev/api/account/authuser",
                    body: {
                        "login": _controllerEmail.text,
                        "password": _controllerPassword.text
                    },
                    //            headers: {"Content-Type":"application/json"}
                );
                final _result = jsonDecode(_response.body);
                if (_result['result']) {
                    _preferences.setString(
                        "username", "${_result['username']}");
                    _preferences.setString(
                        "fullname", "${_result['fullname']}");
                    _preferences.setString("job_id", "${_result['job_id']}");
                    _preferences.setString(
                        "job_name", "${_result['job_name']}");
                    _preferences.setString(
                        "is_mayor", "${_result['is_mayor']}");
                    _preferences.setString(
                        "branchid", "${_result['branchid']}");
                    _preferences.setString("ou_type", "${_result['ou_type']}");
                    _preferences.setString("UnitD", "${_result['UnitD']}");
                    _preferences.setString("SentraD", "${_result['SentraD']}");
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => Dashboard()));
                } else {
                    showSnackBar(_result['message']);
                    _valueRandomText.clear();
                    setRandomText();
                }
                // } else {
                //     autoValidate = true;
                // }
            } catch (e) {
                print(e);
                showSnackBar(e.toString());
            }
            loginProcess = false;
        } else {
            autoValidate = true;
        }
    }

    showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating,
            backgroundColor: snackbarColor, duration: Duration(seconds: 2))
        );
    }

    TextEditingController get valueRandomText => _valueRandomText;

    String get randomText => _randomText;

    set randomText(String value) {
        this._randomText = value;
        notifyListeners();
    }
}
