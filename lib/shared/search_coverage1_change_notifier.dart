import 'dart:collection';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/coverage1_model.dart';
import 'package:ad1ms2_dev/shared/resource/get_insurance_type.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SearchCoverage1ChangeNotifier with ChangeNotifier {
    bool _showClear = false;
    TextEditingController _controllerSearch = TextEditingController();
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    List<CoverageModel> _listCoverage1 = [
//        Coverage1Model("01", "Coverage1 A"),
//        Coverage1Model("02", "Coverage1 B"),
//        Coverage1Model("03", "Coverage1 C")
    ];

    List<CoverageModel> _listCoverageTemp = [];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<CoverageModel> get listCoverage1Model {
        return UnmodifiableListView(this._listCoverage1);
    }

    UnmodifiableListView<CoverageModel> get listCoverageTemp {
        return UnmodifiableListView(this._listCoverageTemp);
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
      this._loadData = value;
    }

    void getCoverage(BuildContext context) async {
        this._listCoverage1.clear();
        this._loadData = true;

        try{
            _listCoverage1 = await getInsuranceType(context);
            this._loadData = false;
        }
        catch(e){
            showSnackBar(e.toString());
            this._loadData = false;
        }
        notifyListeners();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void searchCoverage(String query) {
        if(query.length < 3) {
            showSnackBar("Input minimal 3 karakter");
        } else {
            _listCoverageTemp.clear();
            if (query.isEmpty) {
                return;
            }

            _listCoverage1.forEach((dataSourceOrder) {
                if (dataSourceOrder.KODE.contains(query) || dataSourceOrder.DESKRIPSI.contains(query)) {
                    _listCoverageTemp.add(dataSourceOrder);
                }
            });
        }
        notifyListeners();
    }

    void clearSearchTemp() {
        _listCoverageTemp.clear();
    }
}
