import 'dart:collection';

import 'package:ad1ms2_dev/models/object_usage_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class SearchObjectUsageChangeNotifier with ChangeNotifier{
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();

  List<ObjectUsageModel> _listObjectUsage = [
    ObjectUsageModel("001", "PERSONAL"),
    ObjectUsageModel("002", "Passenger"),
    ObjectUsageModel("003", "PERSONAL/B2B/B2C"),
    ObjectUsageModel("004", "B2C"),
    ObjectUsageModel("005", "Commercial"),
  ];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ObjectUsageModel> get listObjectUsage {
    return UnmodifiableListView(this._listObjectUsage);
  }
}