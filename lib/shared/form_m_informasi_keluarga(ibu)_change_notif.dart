import 'dart:collection';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_family_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'date_picker.dart';

class FormMInformasiKeluargaIBUChangeNotif with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  List<RelationshipStatusModel> _listRelationShipStatus =
      RelationshipStatusList().relationshipStatusItems;
  bool _autoValidate = false;
  RelationshipStatusModel _relationshipStatusSelected;
  IdentityModel _identitySelected;
  TextEditingController _controllerNoIdentitas = TextEditingController();
  TextEditingController _controllerNamaLengkapdentitas =
      TextEditingController();
  TextEditingController _controllerNamaIdentitas = TextEditingController();
  TextEditingController _controllerTglLahir = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitas =
      TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitasLOV = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TextEditingController _controllerNoHp = TextEditingController();
  String _radioValueGender = "02";
  List<IdentityModel> _listIdentityType = IdentityType().lisIdentityModel;
  BirthPlaceModel _birthPlaceSelected;
  bool _isEnableFieldFullName = false;
  DbHelper _dbHelper = DbHelper();

  DateTime _initialDateForTglLahir =
      DateTime(dateNow.year, dateNow.month, dateNow.day);

  bool get autoValidate => _autoValidate;

  RelationshipStatusModel get relationshipStatusSelected =>
      _relationshipStatusSelected;

  set relationshipStatusSelected(RelationshipStatusModel value) {
    this._relationshipStatusSelected = value;
    notifyListeners();
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  IdentityModel get identitySelected => _identitySelected;

  set identitySelected(IdentityModel value) {
    this._identitySelected = value;
    this._controllerNoIdentitas.clear();
    notifyListeners();
  }

  String get radioValueGender => _radioValueGender;

  set radioValueGender(String value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  TextEditingController get controllerNoIdentitas => _controllerNoIdentitas;

  TextEditingController get controllerNamaLengkapdentitas =>
      _controllerNamaLengkapdentitas;

  TextEditingController get controllerNamaIdentitas => _controllerNamaIdentitas;

  TextEditingController get controllerTglLahir => _controllerTglLahir;

  TextEditingController get controllerTempatLahirSesuaiIdentitas =>
      _controllerTempatLahirSesuaiIdentitas;

  TextEditingController get controllerKodeArea => _controllerKodeArea;

  TextEditingController get controllerTlpn => _controllerTlpn;

  TextEditingController get controllerNoHp => _controllerNoHp;

  TextEditingController get controllerTempatLahirSesuaiIdentitasLOV =>
      _controllerTempatLahirSesuaiIdentitasLOV;

  UnmodifiableListView<RelationshipStatusModel> get listRelationShipStatus {
    return UnmodifiableListView(this._listRelationShipStatus);
  }

  UnmodifiableListView<IdentityModel> get listIdentityType {
    return UnmodifiableListView(this._listIdentityType);
  }

  void setDefaultValue() {
    this._relationshipStatusSelected = _listRelationShipStatus[4];
    this._identitySelected = _listIdentityType[0];
//    if (this._relationshipStatusSelected == null &&
//        this._identitySelected == null) {
//
//    }
  }

  void selectBirthDate(BuildContext context) async {
    DatePickerShared _datePickerShared = DatePickerShared();
    var _datePickerSelected = await _datePickerShared.selectStartDate(
        context, this._initialDateForTglLahir,
        canAccessNextDay: false);
    if (_datePickerSelected != null) {
      this._controllerTglLahir.text = dateFormat.format(_datePickerSelected);
      this._initialDateForTglLahir = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  void checkValidCodeArea(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerKodeArea.clear();
      });
    } else {
      return;
    }
  }

  void checkValidNotZero(String value) {
    if (value == "0") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerNoHp.clear();
      });
    } else {
      return;
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerTempatLahirSesuaiIdentitasLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  bool get isEnableFieldFullName => _isEnableFieldFullName;

  set isEnableFieldFullName(bool value) {
    this._isEnableFieldFullName = value;
    notifyListeners();
  }

  // Future<void> clearDataKeluargaIbu() async {
  void clearDataKeluargaIbu() {
    this._flag = false;
    this._autoValidate = false;
    this._relationshipStatusSelected = null;
    // this._identitySelected = null;
    // this._controllerNoIdentitas.clear();
    this._controllerNamaLengkapdentitas.clear();
//    this._controllerNamaIdentitas.clear();
    // this._controllerTglLahir.clear();
    // this._controllerTempatLahirSesuaiIdentitas.clear();
    // this._controllerTempatLahirSesuaiIdentitasLOV.clear();
    // this._radioValueGender = "02";
    // this._controllerKodeArea.clear();
    // this._controllerTlpn.clear();
    // this._controllerNoHp.clear();


  }

  void saveToSQLite(){
    _dbHelper.insertMS2CustFamily(MS2CustFamilyModel("123", _relationshipStatusSelected.PARA_FAMILY_TYPE_ID,
        _relationshipStatusSelected.PARA_FAMILY_TYPE_NAME, _controllerNamaLengkapdentitas.text, _controllerNamaIdentitas.text,
        _controllerNoIdentitas.text, _controllerTglLahir.text, _controllerTempatLahirSesuaiIdentitas.text, null, null,
        _radioValueGender, _radioValueGender == "01"? "Laki laki" : "Perempuan", null, _identitySelected.id, _identitySelected.name,
        null, null, null, null, 1, null, _controllerNoHp.text, _controllerKodeArea.text, null));
  }

  Future<bool> deleteSQLite() async{
    print("jalan bro info kel ibu");
    return await _dbHelper.deleteMS2CustFamily();
  }

  void setDataSQLite() async{
    var _dedup = await _dbHelper.selectDataInfoNasabahFromDataDedup();
    List _data = await _dbHelper.selectDataInfoKeluarga("${this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID}");
    if(_data.isEmpty){
      this._controllerNamaIdentitas.text = _dedup[0]['nama_gadis_ibu_kandung'];
    }
    else{
      this._controllerNamaIdentitas.text = _data[0]['full_name_id'];
      this._controllerNamaLengkapdentitas.text = _data[0]['full_name_id'];
    }
  }
}
