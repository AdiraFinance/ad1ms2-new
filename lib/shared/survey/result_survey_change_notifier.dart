import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/asset_type_model.dart';
import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/environmental_information_model.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:ad1ms2_dev/models/ms2_sby_assgnmt_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_asst_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_dtl_model.dart';
import 'package:ad1ms2_dev/models/photo_type_model.dart';
import 'package:ad1ms2_dev/models/recommendation_surveyor_model.dart';
import 'package:ad1ms2_dev/models/recources_survey_info_model.dart';
import 'package:ad1ms2_dev/models/result_survey_asset_model.dart';
import 'package:ad1ms2_dev/models/result_survey_detail_survey_model.dart';
import 'package:ad1ms2_dev/models/result_survey_model.dart';
import 'package:ad1ms2_dev/models/ownership_model.dart';
import 'package:ad1ms2_dev/models/road_type_model.dart';
import 'package:ad1ms2_dev/models/survey_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/io_client.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ResultSurveyChangeNotifier with ChangeNotifier{
  FormatCurrency _formatCurrency = FormatCurrency();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  DbHelper _dbHelper = DbHelper();
  FormatCurrency get formatCurrency => _formatCurrency;

  set formatCurrency(FormatCurrency value) {
    this._formatCurrency = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  /*
  *
  * Hasil Survey Group Catatan
  *
  * */
  bool _autoValidateResultSurveyGroupNote  = false;
  List<RecommendationSurveyorModel> _listRecomSurvey = [];
  List<ResultSurveyModel> _listResultSurvey = [];
  RecommendationSurveyorModel _recommendationSurveySelected;
  ResultSurveyModel _resultSurveySelected;
  TextEditingController _controllerNote = TextEditingController();
  TextEditingController _controllerResultSurveyDate = TextEditingController();
  DateTime _initialDateResultSurvey = DateTime(dateNow.year,dateNow.month,dateNow.day);
  DateTime _initialEndDateLease = DateTime(dateNow.year,dateNow.month,dateNow.day);
  bool _flagResultSurveyGroupNotes = false;
  GlobalKey<FormState> _keyResultSurveyGroupNotes = GlobalKey<FormState>();
  String _pathFile;


  String get pathFile => _pathFile;

  bool get autoValidateResultSurveyGroupNote => _autoValidateResultSurveyGroupNote;

  set autoValidateResultSurveyGroupNote(bool value) {
    this._autoValidateResultSurveyGroupNote = value;
    notifyListeners();
  }

  RecommendationSurveyorModel get recommendationSurveySelected =>
      _recommendationSurveySelected;

  set recommendationSurveySelected(RecommendationSurveyorModel value) {
    this._recommendationSurveySelected = value;
    notifyListeners();
  }

  List<RecommendationSurveyorModel> get listRecomSurvey => _listRecomSurvey;

  TextEditingController get controllerNote => _controllerNote;

  List<ResultSurveyModel> get listResultSurvey => _listResultSurvey;

  ResultSurveyModel get resultSurveySelected => _resultSurveySelected;

  set resultSurveySelected(ResultSurveyModel value) {
    this._resultSurveySelected = value;
    notifyListeners();
  }

  bool get flagResultSurveyGroupNotes => _flagResultSurveyGroupNotes;

  set flagResultSurveyGroupNotes(bool value) {
    this._flagResultSurveyGroupNotes = value;
    notifyListeners();
  }

  TextEditingController get controllerResultSurveyDate =>
      _controllerResultSurveyDate;

  GlobalKey<FormState> get keyResultSurveyGroupNotes =>
      _keyResultSurveyGroupNotes;

  void selectDateResultSurvey(BuildContext context) async {
    final DateTime _picked = await showDatePicker(
        context: context,
        initialDate: _initialDateResultSurvey,
        firstDate: DateTime(
            DateTime.now().year-5, DateTime.now().month, DateTime.now().day),
        lastDate: DateTime(
            DateTime.now().year + 5, DateTime.now().month, DateTime.now().day));
    if (_picked != null) {
      this._initialDateResultSurvey = _picked;
      this._controllerResultSurveyDate.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  void getStatusSurvey() async{
    this._listResultSurvey.clear();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
        "${BaseUrl.urlPublic}api/statussurvey/get-status-survey"
    );
    final _data = jsonDecode(_response.body);
    if(_response.statusCode == 200){
      for(int i=0; i < _data.length; i++){
        this._listResultSurvey.add(ResultSurveyModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
      notifyListeners();
    }
    else{
      showSnackBar("Error ${_response.statusCode}");
    }
  }

  void getRecomSurvey() async{
    this._listRecomSurvey.clear();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
        "${BaseUrl.urlPublic}api/rekomresurvey/get-rekom-resurvey"
    );
    final _data = jsonDecode(_response.body);
    if(_response.statusCode == 200){
      for(int i=0; i < _data.length; i++){
        this._listRecomSurvey.add(RecommendationSurveyorModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
      notifyListeners();
    }
    else{
      showSnackBar("Error ${_response.statusCode}");
    }
  }

  void checkResultSurveyGroupNotes(BuildContext context) {
    final _form = _keyResultSurveyGroupNotes.currentState;
    if (_form.validate()) {
      flagResultSurveyGroupNotes = true;
      autoValidateResultSurveyGroupNote = false;
      Navigator.pop(context);
    } else {
      flagResultSurveyGroupNotes = false;
      autoValidateResultSurveyGroupNote = true;
    }
  }

  Future<bool> onBackPressResultSurveyGroupNotes() async{
    final _form = _keyResultSurveyGroupNotes.currentState;
    if (_form.validate()) {
      flagResultSurveyGroupNotes = true;
      autoValidateResultSurveyGroupNote = false;
    } else {
      flagResultSurveyGroupNotes = false;
      autoValidateResultSurveyGroupNote = true;
    }
    return true;
  }

  /*
  *
  * Hasil Survey Group Lokasi
  *
  * */
  bool _autoValidateResultSurveyGroupLocation  = false;
  List<SurveyTypeModel> _listSurveyType= [];
  SurveyTypeModel _surveyTypeSelected;
  TextEditingController _controllerDistanceLocationWithSentraUnit = TextEditingController();
  TextEditingController _controllerDistanceLocationWithDealerMerchantAXI = TextEditingController();
  TextEditingController _controllerDistanceLocationWithUsageObjectWithSentraUnit = TextEditingController();
  bool _flagResultSurveyGroupLocation = false;
  GlobalKey<FormState> _keyResultSurveyGroupLocation = GlobalKey<FormState>();

  bool get autoValidateResultSurveyGroupLocation =>
      _autoValidateResultSurveyGroupLocation;

  set autoValidateResultSurveyGroupLocation(bool value) {
    this._autoValidateResultSurveyGroupLocation = value;
    notifyListeners();
  }

  List<SurveyTypeModel> get listSurveyType => _listSurveyType;

  SurveyTypeModel get surveyTypeSelected => _surveyTypeSelected;

  set surveyTypeSelected(SurveyTypeModel value) {
    this._surveyTypeSelected = value;
    notifyListeners();
  }

  TextEditingController
  get controllerDistanceLocationWithUsageObjectWithSentraUnit =>
          _controllerDistanceLocationWithUsageObjectWithSentraUnit;

  TextEditingController get controllerDistanceLocationWithDealerMerchantAXI =>
      _controllerDistanceLocationWithDealerMerchantAXI;

  TextEditingController get controllerDistanceLocationWithSentraUnit =>
      _controllerDistanceLocationWithSentraUnit;


  bool get flagResultSurveyGroupLocation => _flagResultSurveyGroupLocation;

  set flagResultSurveyGroupLocation(bool value) {
    this._flagResultSurveyGroupLocation = value;
    notifyListeners();
  }

  GlobalKey<FormState> get keyResultSurveyGroupLocation =>
      _keyResultSurveyGroupLocation;

  void checkResultSurveyGroupLocation(BuildContext context) {
    final _form = _keyResultSurveyGroupLocation.currentState;
    if (_form.validate()) {
      flagResultSurveyGroupLocation = true;
      autoValidateResultSurveyGroupLocation = false;
      Navigator.pop(context);
    } else {
      flagResultSurveyGroupLocation = false;
      autoValidateResultSurveyGroupLocation = true;
    }
  }

  Future<bool> onBackPressResultSurveyGroupLocation() async{
    final _form = _keyResultSurveyGroupLocation.currentState;
    if (_form.validate()) {
      flagResultSurveyGroupLocation = true;
      autoValidateResultSurveyGroupLocation = false;
    } else {
      flagResultSurveyGroupLocation = false;
      autoValidateResultSurveyGroupLocation = true;
    }
    return true;
  }

  void saveToSQLiteMS2SvyAssgnmt(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.insertMS2SvyAssgnmt(MS2SvyAssgnmtModel(
      "1",
      this._surveyTypeSelected != null ? this._surveyTypeSelected.id : "",
      _preferences.getString("username"),
      null,
      Provider.of<InfoAppChangeNotifier>(context, listen: false).controllerSurveyAppointmentDate.text,
      null,
      null,
      null,
      this._resultSurveySelected != null ? this._resultSurveySelected.KODE : "",
      1,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
      this._resultSurveySelected != null ? this._resultSurveySelected.KODE : "",
      this._recommendationSurveySelected != null ? this._recommendationSurveySelected.KODE : "",
      this._controllerNote.text != "" ? this._controllerNote.text : "",
      this._controllerResultSurveyDate.text != "" ? this._controllerResultSurveyDate.text : "",
      this._controllerDistanceLocationWithSentraUnit.text != "" ? double.parse(this._controllerDistanceLocationWithSentraUnit.text) : "",
      this._controllerDistanceLocationWithDealerMerchantAXI.text != "" ? double.parse(this._controllerDistanceLocationWithDealerMerchantAXI.text) : "",
      this._controllerDistanceLocationWithUsageObjectWithSentraUnit.text != "" ? double.parse(this._controllerDistanceLocationWithUsageObjectWithSentraUnit.text) : "",
      null,
      null,
    ));
  }

  /*
  *
  * Hasil Survey Buat Baru/Update Survey Detail
  *
  * */
  GlobalKey<FormState> _keyResultSurveyCreateEditDetailSurvey = GlobalKey<FormState>();
  List<ResultSurveyDetailSurveyModel> _listResultSurveyDetailSurveyModel = [];
  List<EnvironmentalInformationModel> _listEnviInfo = [
    // EnvironmentalInformationModel("001", "REPUTASI BAIK"),
    // EnvironmentalInformationModel("002", "REPUTASI BURUK"),
  ];
  EnvironmentalInformationModel _environmentalInformationSelected;
  EnvironmentalInformationModel _environmentalInformationTemp;
  List<ResourcesInfoSurveyModel> _listResourcesInfoSurvey = ResourceInfoSurveyList().resourcesInfoSurveyList;
  ResourcesInfoSurveyModel _resourcesInfoSurveySelected;
  ResourcesInfoSurveyModel _resourcesInfoSurveyTemp;
  TextEditingController _controllerResourceInformationName = TextEditingController();
  String _resourceInfoName;
  bool _autoValidateResultSurveyCreateEditDetailSurvey  = false;
  bool _flagResultSurveyCreateEditDetailSurvey = false;

  List<EnvironmentalInformationModel> get listEnviInfo => _listEnviInfo;

  EnvironmentalInformationModel get environmentalInformationSelected =>
      _environmentalInformationSelected;

  set environmentalInformationSelected(EnvironmentalInformationModel value) {
    this._environmentalInformationSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<ResourcesInfoSurveyModel> get listResourcesInfoSurvey {
    return UnmodifiableListView(this._listResourcesInfoSurvey);
  }

  TextEditingController get controllerResourceInformationName =>
      _controllerResourceInformationName;

  ResourcesInfoSurveyModel get resourcesInfoSurveySelected =>
      _resourcesInfoSurveySelected;

  set resourcesInfoSurveySelected(ResourcesInfoSurveyModel value) {
    this._resourcesInfoSurveySelected = value;
    notifyListeners();
  }

  List<ResultSurveyDetailSurveyModel> get listResultSurveyDetailSurveyModel =>
      _listResultSurveyDetailSurveyModel;

  void addResultSurveyDetailSurveyModel(ResultSurveyDetailSurveyModel value){
    this._listResultSurveyDetailSurveyModel.add(value);
    notifyListeners();
  }

  void updateResultSurveyDetailSurveyModel(ResultSurveyDetailSurveyModel value,int index){
    this._listResultSurveyDetailSurveyModel[index] = value;
    notifyListeners();
  }

  bool get autoValidateResultSurveyCreateEditDetailSurvey =>
      _autoValidateResultSurveyCreateEditDetailSurvey;

  set autoValidateResultSurveyCreateEditDetailSurvey(bool value) {
    this._autoValidateResultSurveyCreateEditDetailSurvey = value;
    notifyListeners();
  }

  void clearDataFieldResultSurveyGroupLocation(){
    this._environmentalInformationSelected = null;
    this._controllerResourceInformationName.clear();
    this._resourcesInfoSurveySelected = null;
  }

  GlobalKey<FormState> get keyResultSurveyCreateEditDetailSurvey =>
      _keyResultSurveyCreateEditDetailSurvey;

  EnvironmentalInformationModel get environmentalInformationTemp =>
      _environmentalInformationTemp;

  ResourcesInfoSurveyModel get resourcesInfoSurveyTemp =>
      _resourcesInfoSurveyTemp;

  String get resourceInfoName => _resourceInfoName;

  bool get flagResultSurveyCreateEditDetailSurvey =>
      _flagResultSurveyCreateEditDetailSurvey;

  set flagResultSurveyCreateEditDetailSurvey(bool value) {
    this._flagResultSurveyCreateEditDetailSurvey = value;
    notifyListeners();
  }

  // Informasi Lingkungan
  EnvironmentalInformationModel _environmentalInformationModelSelected;

  EnvironmentalInformationModel get environmentalInformationModelSelected => _environmentalInformationModelSelected;

  set environmentalInformationModelSelected(EnvironmentalInformationModel value) {
    this._environmentalInformationModelSelected = value;
    notifyListeners();
  }

  void getEnvironmentalInformation(BuildContext context) async {
    this._environmentalInformationModelSelected = this._listEnviInfo[0];
    _listEnviInfo.clear();
    this._environmentalInformationSelected = null;
    loadData = true;

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get("${BaseUrl.urlPublic}api/jenis-rehab/get_survey_info");
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listEnviInfo.add(EnvironmentalInformationModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi'].trim()));
      }
      loadData = false;
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void checkResultSurveyDetailSurvey(int flag,int index){
    final _form = _keyResultSurveyCreateEditDetailSurvey.currentState;
    if(_form.validate()){
      if(flag == 0){
        addResultSurveyDetailSurveyModel(
            ResultSurveyDetailSurveyModel(
                this._environmentalInformationSelected,
                this._resourcesInfoSurveySelected,
                this._controllerResourceInformationName.text
            )
        );
      }
      else{
        updateResultSurveyDetailSurveyModel(
            ResultSurveyDetailSurveyModel(
                this._environmentalInformationSelected,
                this._resourcesInfoSurveySelected,
                this._controllerResourceInformationName.text
            ),
            index
        );
      }
    }
    else{
      autoValidateResultSurveyCreateEditDetailSurvey = true;
    }
  }

  Future<void> setValueEditResultSurveyDetailSurvey(ResultSurveyDetailSurveyModel value) async{
    for(int i=0; i < this._listEnviInfo.length; i++){
      if(_listEnviInfo[i].id == value.environmentalInformationModel.id){
        this._environmentalInformationSelected = _listEnviInfo[i];
        this._environmentalInformationTemp = _listEnviInfo[i];
      }
    }

    for(int i=0; i < this._listResourcesInfoSurvey.length; i++){
      if(_listResourcesInfoSurvey[i].PARA_INFORMATION_SOURCE_ID ==
          value.recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_ID){
        this._resourcesInfoSurveySelected = _listResourcesInfoSurvey[i];
        this._resourcesInfoSurveyTemp = _listResourcesInfoSurvey[i];
      }
    }

    this._controllerResourceInformationName.text = value.resourceInformationName;
    this._resourceInfoName = this._controllerResourceInformationName.text;
  }

  void saveToSQLiteMS2SvyRsltDtl(BuildContext context) async {
    List<MS2SvyRsltDtlModel> _listSvyRsltDtl = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    if(_listResultSurveyDetailSurveyModel.isNotEmpty) {
      for(int i=0; i<_listResultSurveyDetailSurveyModel.length; i++){
        _listSvyRsltDtl.add(MS2SvyRsltDtlModel(
          "1",
          this._listResultSurveyDetailSurveyModel[i].environmentalInformationModel != null ? this._listResultSurveyDetailSurveyModel[i].environmentalInformationModel.id : "",
          this._listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel != null ? this._listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_ID : "",
          this._listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel != null ? this._listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_NAME : "",
          1,
          DateTime.now().toString(),
          _preferences.getString("username"),
          null,
          null,
        ));
      }
    }
    _dbHelper.insertMS2SvyRsltDtl(_listSvyRsltDtl);
  }

  /*
  *
  * Hasil Survey Buat Baru/Update Aset
  *
  * */
  bool _autoValidateResultSurveyAssetModel = false;
  GlobalKey<FormState> _keyResultSurveyAsset= GlobalKey<FormState>();
  List<ResultSurveyAssetModel> _listResultSurveyAssetModel = [];
  List<AssetTypeModel> _listAssetType = [];
  AssetTypeModel _assetTypeSelected;
  TextEditingController _controllerValueAsset = TextEditingController();
  List<OwnershipModel> _listOwnership = [
    OwnershipModel("001", "SENDIRI"),
    OwnershipModel("002", "KELUARGA"),
  ];
  OwnershipModel _ownershipSelected;
  TextEditingController _controllerSurfaceBuildingArea = TextEditingController();
  // List<RoadTypeModel> _listRoadType = [
  //   RoadTypeModel("001", "ASPAL"),
  //   RoadTypeModel("002", "PAVING")
  // ];
  List<RoadTypeModel> _listRoadType = [];
  TextEditingController _controllerRoadType = TextEditingController();
  RoadTypeModel _roadTypeSelected;
  List<ElectricityTypeModel> _listElectricityType = ElectricityType().electricityTypeList;
  ElectricityTypeModel _electricityTypeSelected;
  TextEditingController _controllerElectricityBills = TextEditingController();
  TextEditingController _controllerEndDateLease = TextEditingController();
  TextEditingController _controllerLengthStay= TextEditingController();
  AssetTypeModel _assetTypeTemp;
  String _valueAssetTemp,_surfaceBuildingAreaTemp,_electricityBillsTemp,_endDateLeaseTemp,_lengthStayTemp;
  OwnershipModel _ownershipTemp;
  RoadTypeModel _roadTypeTemp;
  ElectricityTypeModel _electricityTypeTemp;
  bool _flagResultSurveyAsset = false;

  List<ResultSurveyAssetModel> get listResultSurveyAssetModel =>
      _listResultSurveyAssetModel;

  List<AssetTypeModel> get listAssetType => _listAssetType;

  AssetTypeModel get assetTypeSelected => _assetTypeSelected;

  set assetTypeSelected(AssetTypeModel value) {
    this._assetTypeSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerValueAsset => _controllerValueAsset;

  List<OwnershipModel> get listOwnership => _listOwnership;

  OwnershipModel get ownershipSelected => _ownershipSelected;

  set ownershipSelected(OwnershipModel value) {
    this._ownershipSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerSurfaceBuildingArea =>
      _controllerSurfaceBuildingArea;

  List<RoadTypeModel> get listRoadType => _listRoadType;

  RoadTypeModel get roadTypeSelected => _roadTypeSelected;

  set roadTypeSelected(RoadTypeModel value) {
    this._roadTypeSelected = value;
    notifyListeners();
  }

  List<ElectricityTypeModel> get listElectricityType => _listElectricityType;

  ElectricityTypeModel get electricityTypeSelected => _electricityTypeSelected;

  set electricityTypeSelected(ElectricityTypeModel value) {
    this._electricityTypeSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerElectricityBills =>
      _controllerElectricityBills;

  TextEditingController get controllerEndDateLease => _controllerEndDateLease;

  TextEditingController get controllerLengthStay => _controllerLengthStay;

  bool get autoValidateResultSurveyAssetModel =>
      _autoValidateResultSurveyAssetModel;

  set autoValidateResultSurveyAssetModel(bool value) {
    this._autoValidateResultSurveyAssetModel = value;
    notifyListeners();
  }

  GlobalKey<FormState> get keyResultSurveyAsset => _keyResultSurveyAsset;

  bool get flagResultSurveyAsset => _flagResultSurveyAsset;

  set flagResultSurveyAsset(bool value) {
    this._flagResultSurveyAsset = value;
    notifyListeners();
  }

  void selectEndDateLease(BuildContext context) async {
    final DateTime _picked = await showDatePicker(
        context: context,
        initialDate: _initialEndDateLease,
        firstDate: DateTime(
            DateTime.now().year, DateTime.now().month, DateTime.now().day),
        lastDate: DateTime(
            DateTime.now().year + 20, DateTime.now().month, DateTime.now().day));
    if (_picked != null) {
      this._initialEndDateLease = _picked;
      this._controllerEndDateLease.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  void addResultSurveyAssetModel(ResultSurveyAssetModel value){
    this._listResultSurveyAssetModel.add(value);
    notifyListeners();
  }

  void updateResultSurveyAssetModel(ResultSurveyAssetModel value, int index){
    this._listResultSurveyAssetModel[index] = value;
    notifyListeners();
  }


  AssetTypeModel get assetTypeTemp => _assetTypeTemp;

  String get valueAssetTemp => _valueAssetTemp;

  get surfaceBuildingAreaTemp => _surfaceBuildingAreaTemp;

  get electricityBillsTemp => _electricityBillsTemp;

  get endDateLeaseTemp => _endDateLeaseTemp;

  get lengthStayTemp => _lengthStayTemp;

  OwnershipModel get ownershipTemp => _ownershipTemp;

  ElectricityTypeModel get electricityTypeTemp => _electricityTypeTemp;

  RoadTypeModel get roadTypeTemp => _roadTypeTemp;

  TextEditingController get controllerRoadType => _controllerRoadType;

  set controllerRoadType(TextEditingController value) {
    this._controllerRoadType = value;
    notifyListeners();
  }

  void checkResultSurveyAssetModel(int flag,int index){
    final _form = _keyResultSurveyCreateEditDetailSurvey.currentState;
    if(_form.validate()){
      if(flag == 0){
        addResultSurveyAssetModel(
            ResultSurveyAssetModel(
                this._assetTypeSelected,
                this._controllerValueAsset.text,
                this._ownershipSelected,
                this._controllerSurfaceBuildingArea.text,
                this._roadTypeSelected,
                this._electricityTypeSelected,
                this._controllerElectricityBills.text,
                this._initialEndDateLease,
                this._controllerLengthStay.text
            )
        );
      }
      else{
        updateResultSurveyAssetModel(
            ResultSurveyAssetModel(
                this._assetTypeSelected,
                this._controllerValueAsset.text,
                this._ownershipSelected,
                this._controllerSurfaceBuildingArea.text,
                this._roadTypeSelected,
                this._electricityTypeSelected,
                this._controllerElectricityBills.text,
                this._initialEndDateLease,
                this._controllerLengthStay.text
            ),
            index
        );
      }
    }
    else{
      autoValidateResultSurveyAssetModel = true;
    }
  }

  void setValueDefaultAsset(){
    this._listAssetType.clear();
    _listAssetType = [
      AssetTypeModel("001", "MOTOR"),
      AssetTypeModel("002", "MOBIL"),
      AssetTypeModel("003", "PROPERTY"),
    ];
    for(int i = 0; i <this._listAssetType.length; i++){
      if(this._listAssetType[i].id == "003"){
        this._assetTypeSelected = this._listAssetType[i];
      }
    }
  }

  void getRoadType() async{
    this._listRoadType.clear();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
        "${BaseUrl.urlPublic}jenis-rehab/get_street_type"
    );
    final _data = jsonDecode(_response.body);
    if(_response.statusCode == 200){
      for(int i=0; i < _data['data'].length; i++){
        this._listRoadType.add(RoadTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      notifyListeners();
    }
    else{
      showSnackBar("Error ${_response.statusCode}");
    }
  }

  Future<void> setValueEditResultSurveyDetailAsset(ResultSurveyAssetModel value) async{
    this._listAssetType.clear();
    _listAssetType = [
      AssetTypeModel("001", "MOTOR"),
      AssetTypeModel("002", "MOBIL"),
      AssetTypeModel("003", "PROPERTY"),
    ];
    for(int i=0; i < this._listAssetType.length; i++){
      if(_listAssetType[i].id == value.assetTypeModel.id){
        this._assetTypeSelected = _listAssetType[i];
        this._assetTypeTemp = _listAssetType[i];
      }
    }

    this._controllerValueAsset.text = value.valueAsset;
    this._valueAssetTemp = this._controllerValueAsset.text;

    for(int i=0; i < this._listOwnership.length; i++){
      if(_listOwnership[i].id == value.ownershipModel.id){
        this._ownershipSelected = _listOwnership[i];
        this._ownershipTemp = _listOwnership[i];
      }
    }

    if(value.assetTypeModel.id != "001" || value.assetTypeModel.id != "002"){
      this._controllerSurfaceBuildingArea.text = value.surfaceBuildingArea;
      this._surfaceBuildingAreaTemp = this._controllerSurfaceBuildingArea.text;

      for(int i=0; i < _listRoadType.length; i++){
        if(_listAssetType[i].id == value.assetTypeModel.id){
          this._assetTypeSelected = _listAssetType[i];
          this._assetTypeTemp = _listAssetType[i];
        }
      }

      for(int j=0; j < _listElectricityType.length; j++){
        if(_listElectricityType[j].PARA_ELECTRICITY_ID == value.electricityTypeModel.PARA_ELECTRICITY_ID){
          this._electricityTypeSelected = _listElectricityType[j];
          this._electricityTypeTemp = _listElectricityType[j];
        }
      }

      this._controllerElectricityBills.text = value.electricityBills;
      this._electricityBillsTemp = this._controllerElectricityBills.text;

      this._initialEndDateLease = value.endDateLease;
      this._controllerEndDateLease.text = dateFormat.format(this._initialEndDateLease);
      this._endDateLeaseTemp = this._controllerEndDateLease.text;

      this._controllerLengthStay.text = value.lengthStay;
      this._lengthStayTemp = this._controllerLengthStay.text;
    }
  }

  void saveToSQLiteMS2SvyResultAsset(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    List<MS2SvyRsltAsstModel> _list = [];
    if(_listResultSurveyAssetModel.isNotEmpty) {
      for(int i=0; i < _listResultSurveyAssetModel.length; i++){
        _list.add(MS2SvyRsltAsstModel(
            "22",
            this._listResultSurveyAssetModel[i].assetTypeModel != null ? this._listResultSurveyAssetModel[i].assetTypeModel : "",
            this._listResultSurveyAssetModel[i].valueAsset != null ? this._listResultSurveyAssetModel[i].valueAsset : "",
            this._listResultSurveyAssetModel[i].ownershipModel != null ? this._listResultSurveyAssetModel[i].ownershipModel : "",
            this._listResultSurveyAssetModel[i].surfaceBuildingArea != null ? this._listResultSurveyAssetModel[i].surfaceBuildingArea : "",
            this._listResultSurveyAssetModel[i].roadTypeModel != null ? this._listResultSurveyAssetModel[i].roadTypeModel : "",
            this._listResultSurveyAssetModel[i].electricityTypeModel != null ? this._listResultSurveyAssetModel[i].electricityTypeModel : "",
            this._listResultSurveyAssetModel[i].electricityBills != null ? double.parse(this._listResultSurveyAssetModel[i].electricityBills.replaceAll(",", "")) : 0,
            this._listResultSurveyAssetModel[i].endDateLease.toString() != "" ? this._listResultSurveyAssetModel[i].endDateLease.toString() : "",
            this._listResultSurveyAssetModel[i].lengthStay != null ? int.parse( this._listResultSurveyAssetModel[i].lengthStay) : 0,
            1,
            DateTime.now().toString(),
            _preferences.getString("username"),
            null,
            null
        ));
      }
    }
    _dbHelper.insertMS2SvyRsltAsst(_list);
  }

  /*
  *
  * Hasil Survey - Foto Survey
  *
  * */
  final _imagePicker = ImagePicker();
  List<PhotoTypeModel> _listPhotoType = [
    PhotoTypeModel("001", "DOKUMEN"),
    PhotoTypeModel("002", "RUMAH"),
    PhotoTypeModel("003", "USAHA"),
  ];
  PhotoTypeModel _photoTypeSelected;
  List<ImageFileModel> _listPhoto = [];
  bool _autoValidateResultSurveyPhoto = false;
  bool _flagResultSurveyPhoto = false;

  PhotoTypeModel get photoTypeSelected => _photoTypeSelected;

  set photoTypeSelected(PhotoTypeModel value) {
    this._photoTypeSelected = value;
    notifyListeners();
  }

  List<PhotoTypeModel> get listPhotoType => _listPhotoType;

  List<ImageFileModel> get listPhoto => _listPhoto;

  bool get autoValidateResultSurveyPhoto => _autoValidateResultSurveyPhoto;

  set autoValidateResultSurveyPhoto(bool value) {
    this._autoValidateResultSurveyPhoto = value;
    notifyListeners();
  }

  bool get flagResultSurveyPhoto => _flagResultSurveyPhoto;

  set flagResultSurveyPhoto(bool value) {
    this._flagResultSurveyPhoto = value;
    notifyListeners();
  }

  void addFile() async {
    var _image = await _imagePicker.getImage(
        source: ImageSource.camera,
        maxHeight: 1920.0,
        maxWidth: 1080.0,
        imageQuality: 50);
    savePhoto(_image);
    // if (_image != null) {
    //   File _imageFile = File(_image.path);
    //   Position _position = await Geolocator().getCurrentPosition(
    //       desiredAccuracy: LocationAccuracy.bestForNavigation);
    //
    //   this._listPhoto.add(ImageFileModel(
    //       _imageFile, _position.latitude, _position.longitude, ""));
    //   notifyListeners();
    // } else {
    //   return;
    // }
  }

  void deletePhoto(int index){
    this._listPhoto.removeAt(index);
    notifyListeners();
  }

  void clearListPhoto(){
    this._listPhoto.clear();
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void savePhoto(PickedFile data) async{
    try{
      if (data != null) {
        File _imageFile = File(data.path);
        List<String> split = data.path.split("/");
        String fileName = split[split.length-1];
        File _path = await _imageFile.copy("$globalPath/$fileName");
        _pathFile = _path.path;
        Position _position = await Geolocator().getCurrentPosition(
            desiredAccuracy: LocationAccuracy.bestForNavigation);

        this._listPhoto.add(ImageFileModel(
            _imageFile, _position.latitude, _position.longitude, this._pathFile));
        notifyListeners();
      } else {
        return;
      }
//      Uint8List bytes = this._fileDocumentUnitObject['file'].readAsBytesSync();
//      await file.writeAsBytes(bytes);
    }
    catch(e){
      print(e);
    }
  }

  void deleteFile(int index) async{
    final file =  File("${this._listPhoto[index].path}");
    await file.delete();
  }

  void saveToSQLitePhotoSurvey(){
    List<MS2DocumentModel> _listData = [];
    for(int i=0; i<_listPhoto.length; i++){
      _listData.add(MS2DocumentModel(
          "a",
          "b",
          "c",
          _listPhoto[i].path,
          null,
          null,
          null,
          null,
          _photoTypeSelected.id,
          _photoTypeSelected.name,
          _listPhoto[i].latitude.toString(),
          _listPhoto[i].longitude.toString()
      ));
    }
    _dbHelper.insertMS2Document(_listData);
  }
}