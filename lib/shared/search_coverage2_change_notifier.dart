import 'dart:collection';

import 'package:ad1ms2_dev/models/coverage2_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class SearchCoverage2ChangeNotifier with ChangeNotifier {
    bool _showClear = false;
    TextEditingController _controllerSearch = TextEditingController();

    List<Coverage2Model> _listCoverage2 = [
        Coverage2Model("01", "Coverage2 A"),
        Coverage2Model("02", "Coverage2 B"),
        Coverage2Model("03", "Coverage2 C")
    ];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<Coverage2Model> get listCoverage2Model {
        return UnmodifiableListView(this._listCoverage2);
    }
}
