import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/data_dedup_model.dart';
import 'package:ad1ms2_dev/models/list_oid_company_model.dart';
import 'package:ad1ms2_dev/models/list_oid_model.dart';
import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/screens/list_oid.dart';
import 'package:ad1ms2_dev/screens/task_list/task_list.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'constants.dart';

class ListOidChangeNotifier with ChangeNotifier {
  String _customerType;
  // GlobalKey<FormState> _key = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool _autoValidate = false;
  bool _flag = false;
  int _selectedIndex = -1;
  bool _loadData = false;
  List<ListOidModel> _listOid = [];
  List<ListOIDCompanyModel> _listOIDCompany = [];
  ListOidModel _listOIdPersonalSelected;
  ListOIDCompanyModel _listOIdCompanySelected;
  DbHelper _dbHelper = DbHelper();

  // GlobalKey<FormState> get key => _key;
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  // Auto Validate
  bool get autoValidate {
    return _autoValidate;
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Flag
  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }


  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  String get customerType => _customerType;

  set customerType(String value) {
    this._customerType = value;
  }

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  List<ListOidModel> get listOid => _listOid;

  List<ListOIDCompanyModel> get listOIDCompany => _listOIDCompany;

  void onBackPress(BuildContext context) {
    Navigator.pop(context);
  }


  ListOIDCompanyModel get listOIdCompanySelected => _listOIdCompanySelected;

  set listOIdCompanySelected(ListOIDCompanyModel value) {
    this._listOIdCompanySelected = value;
    notifyListeners();
  }

  ListOidModel get listOIdPersonalSelected => _listOIdPersonalSelected;

  set listOIdPersonalSelected(ListOidModel value) {
    this._listOIdPersonalSelected = value;
    notifyListeners();
  }

  void getListOid(String identityNumber, String fullname, DateTime birthDate, String birthPlace, String motherName, String identityAddress) async {
    this._listOid.clear();
    this._selectedIndex = -1;
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_AC_ID_NO": identityNumber,
      "P_AC_CUST_NAME": fullname,
      "P_AC_MOTHER_NAME": motherName,
      "P_AC_DATE_BIRTH": formatDateDedup(birthDate),
      "P_AC_PLACE_BIRTH": birthPlace,
      "P_AC_ADDRESS": identityAddress,
    });
    print(_body);
//    List<ListOidModel> _listTemp = [];
//    _listTemp.clear();

    try{
      final _response = await _http.post(
          "http://10.81.3.137:99/DedupAPI/api/Customer/getCustomerDedupInd",
//        "https://103.110.89.34/public/ms2dev/api/dedup/getCustomerDedupInd",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result['P_RC1'];
        if(_data.isNotEmpty){
          for(int i = 0; i < _data.length; i++){
            this._listOid.add(
                ListOidModel(
                    _data[i]['AC_CUST_ID'],
                    _data[i]['AC_ID_NO'],
                    _data[i]['AC_CUST_NAME'],
                    _data[i]['AC_DATE_BIRTH'],
                    _data[i]['AC_MOTHER_NAME'],
                    _data[i]['AC_PLACE_BIRTH'],
                    _data[i]['AC_ADDRESS'],
                    _data[i]['PARA_CUST_TYPE_ID'],
                    _data[i]['AC_FLAG_SOURCE'],
                    _data[i]['SCORE'].toString(),
                    _data[i]['MODIFIED_DATE'],
                    _data[i]['AC_FLAG_DETAIL'],
                    _data[i]['COLUMN_JOIN'].toString(),
                    _data[i]['PARA_CUST_NO'],
                    _data[i]['AC_BR_ID']
                )
            );
//          _listTemp.add(ListOidModel(
//              _data[i]['NUMBER_IDENTITY'],
//              _data[i]['FULLNAME'],
//              _data[i]['BIRTH_DATE'],
//              _data[i]['BIRTH_PLACE'],
//              _data[i]['MOTHER_NAME'],
//              _data[i]['IDENTITY_ADDRESS'],
//              _data[i]['SCORE'],
//              _data[i]['JOIN'],
//          ));
          }
//        var _uniqueIdentityNumber = _listTemp.map((e) => e.NUMBER_IDENTITY.trim()).toSet().toList();
//        var _uniqueFullname = _listTemp.map((e) => e.FULLNAME.trim()).toSet().toList();
//        var _uniqueBirthDate = _listTemp.map((e) => e.BIRTH_DATE.trim()).toSet().toList();
//        var _uniqueBirthPlace = _listTemp.map((e) => e.BIRTH_PLACE.trim()).toSet().toList();
//        var _uniqueMotherName = _listTemp.map((e) => e.MOTHER_NAME.trim()).toSet().toList();
//        var _uniqueIdentityAddress = _listTemp.map((e) => e.IDENTITY_ADDRESS.trim()).toSet().toList();
//        var _uniqueScore = _listTemp.map((e) => e.SCORE.trim()).toSet().toList();
//        var _uniqueJoin = _listTemp.map((e) => e.JOIN.trim()).toSet().toList();
//
//        for(var i =0; i<_uniqueIdentityNumber.length; i++){
//          ListOidModel _myData = ListOidModel(
//            _uniqueIdentityNumber[i],
//            _uniqueFullname[i],
//            _uniqueBirthDate[i],
//            _uniqueBirthPlace[i],
//            _uniqueMotherName[i],
//            _uniqueIdentityAddress[i],
//            _uniqueScore[i],
//            _uniqueJoin[i],
//          );
//          this._listOid.add(_myData);
//        }
          this._loadData = false;
        }
        else{
          showSnackBar("Hasil dedup tidak ditemukan");
          this._loadData = false;
        }
      }
      else{
        showSnackBar("Error get OID response ${_response.statusCode}");
        this._loadData = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      this._loadData = false;
    }
    catch(e){
      showSnackBar("Error $e");
      this._loadData = false;
    }
    notifyListeners();
  }

  void getListOidCompany(String npwpNumber, String custName, DateTime establishedDate, String address) async {
    this._listOIDCompany.clear();
    this._selectedIndex = -1;
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_AC_NPWP_NUMBER": npwpNumber,
      "P_AC_CUST_NAME": custName,
      "P_AC_ESTABILISHED_DATE": formatDateDedup(establishedDate),
      "P_AC_ADDRESS": address
    });

//    List<ListOidModel> _listTemp = [];
//    _listTemp.clear();

    try{
      final _response = await _http.post(
          "http://10.81.3.137:99/DedupAPI/api/Customer/getCustomerDedupCom",
//        "https://103.110.89.34/public/ms2dev/api/dedup/getCustomerDedupInd",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print(_result);
        final _data = _result['P_RC1'];
        if(_data != null){
          for(int i = 0; i < _data.length; i++){
            this._listOIDCompany.add(
                ListOIDCompanyModel(
                    _data[i]['AC_CUST_ID'],
                    _data[i]['AC_NPWP_NUMBER'],
                    _data[i]['AC_CUST_NAME'],
                    _data[i]['AC_ESTABILISHED_DATE'],
                    _data[i]['AC_MOTHER_NAME'],
                    _data[i]['AC_PLACE_BIRTH'],
                    _data[i]['AC_ADDRESS'],
                    _data[i]['PARA_CUST_TYPE_ID'],
                    _data[i]['AC_FLAG_SOURCE'],
                    _data[i]['SCORE'].toString(),
                    _data[i]['MODIFIED_DATE'],
                    _data[i]['AC_FLAG_DETAIL'],
                    _data[i]['COLUMN_JOIN'].toString(),
                    _data[i]['PARA_CUST_NO'],
                    _data[i]['AC_BR_ID']
                )
            );
          }
          loadData = false;
        }
        else{
          showSnackBar("Hasil dedup tidak ditemukan");
          this._loadData = false;
        }
      }
      else{
        showSnackBar("Error get OID response ${_response.statusCode}");
        this._loadData = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      this._loadData = false;
    }
    catch(e){
      showSnackBar("Error $e");
      this._loadData = false;
    }
    notifyListeners();
  }

  void submitNewOid(BuildContext context, String identityNumber, String fullname, DateTime birthDate, String birthPlace, String motherName, String identityAddress, String flag) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_ADDRESS": identityAddress != null ? identityAddress : "",
      "P_CP_UNIT_ID ": _preferences.getString("branchid"),
      "P_CREATED_BY ": _preferences.getString("username"),
      "P_CUST_NAME": fullname != null ? fullname : "",
      "P_DATE_BIRTH": formatDateDedup(birthDate) != null ? formatDateDedup(birthDate) : "",
      "P_FLAG_DETAIL": "0",
      "P_FLAG_SOURCE": "006",
      "P_ID_NO": identityNumber != null ? identityNumber : "",
      "P_MOTHER_NAME": motherName != null ? motherName : "",
      "P_PARA_CUST_TYPE_ID": flag,
      "P_PLACE_BIRTH": birthPlace != null ? birthPlace : "",
    });

    try{
      final _response = await _http.post(
          "${BaseUrl.urlPublic}api/save-draft/insert-dedup-customer",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print(_result);
        final _data = _result['status'];
        if(_data != "1"){
          showSnackBar("Hasil dedup tidak ditemukan");
          this._loadData = false;
        } else{
          loadData = false;
          showSnackBarSuccess("Sukses get data");
          _dbHelper.insertDataDedup(DataDedupModel(flag, identityNumber, fullname, birthDate.toString(), birthPlace, motherName, identityAddress, DateTime.now().toString(), _preferences.getString('username'), DateTime.now().toString(), _preferences.getString('username')),_result['data']);
          _preferences.setString("order_no", _data.toString());
          Timer(Duration(milliseconds: 1000), () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2)));
          });
        }
      } else{
        showSnackBar("Error get OID response ${_response.statusCode}");
        this._loadData = false;
      }
    } on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      // this._loadData = false;
    } catch(e){
      showSnackBar("Error $e");
      // this._loadData = false;
    }
    notifyListeners();
  }

  void submitNewOidCompany(BuildContext context, String flag, String npwpNumber, String custName, DateTime establishedDate, String address) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_ADDRESS": address != null ? address : "",
      "P_CP_UNIT_ID ": _preferences.getString("branchid"),
      "P_CREATED_BY ": _preferences.getString("username"),
      "P_CUST_NAME": custName != null ? custName : "",
      "P_DATE_BIRTH": formatDateDedup(establishedDate) != null ? formatDateDedup(establishedDate) : "",
      "P_FLAG_DETAIL": "0",
      "P_FLAG_SOURCE": "006",
      "P_ID_NO": npwpNumber != null ? npwpNumber : "",
      "P_MOTHER_NAME": "-",
      "P_PARA_CUST_TYPE_ID": flag,
      "P_PLACE_BIRTH": "-",
    });

    // try{
      final _response = await _http.post(
          "${BaseUrl.urlPublic}api/save-draft/insert-dedup-customer",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print(_result);
        final _data = _result['status'];
        if(_data != "1"){
          showSnackBar("Hasil dedup tidak ditemukan");
          this._loadData = false;
        }
        else{
          loadData = false;
          showSnackBarSuccess("Sukses get data");
          _dbHelper.insertDataDedup(DataDedupModel(flag,npwpNumber, custName, establishedDate.toString(), null, null, address, DateTime.now().toString(), _preferences.getString('username'), DateTime.now().toString(), _preferences.getString('username')),_result['data']);
          _preferences.setString("order_no", _result['data']);
          Timer(Duration(milliseconds: 1000), () {
            // Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2)));
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2)), (Route <dynamic> route) => false);
          });
        }
      }
      else{
        showSnackBar("Error get OID response ${_response.statusCode}");
        this._loadData = false;
      }
    // }
    // on TimeoutException catch(_){
    //   showSnackBar("Request Timeout");
    //   this._loadData = false;
    // }
    // catch(e){
    //   showSnackBar("Error $e");
    //   this._loadData = false;
    // }
    notifyListeners();
  }

  void moreOption(context, index) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Theme(
            data: ThemeData(fontFamily: "NunitoSans"),
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.close,
                              color: Colors.black,
                              size: 22.0,
                            ),
                          ])),
                  FlatButton(
                      onPressed: () {
                        // setState(() {
                        _selectedIndex = index;
                        // });
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              color: primaryOrange,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Pilih OID",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                  // FlatButton(
                  //     onPressed: () {
                  //       setState(() {
                  //         _selectedIndex = index;
                  //       });
                  //     },
                  //     child: Row(
                  //         mainAxisSize: MainAxisSize.max,
                  //         mainAxisAlignment: MainAxisAlignment.start,
                  //         children: <Widget>[
                  //           Icon(
                  //             Icons.delete,
                  //             color: Colors.red,
                  //             size: 22.0,
                  //           ),
                  //           SizedBox(
                  //             width: 12.0,
                  //           ),
                  //           Text(
                  //             "Hapus",
                  //             style: TextStyle(fontSize: 18.0, color: Colors.red),
                  //           )
                  //         ])),
                ],
              ),
            ),
          );
        }
    );
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,
        backgroundColor: snackbarColor, duration: Duration(seconds: 2))
    );
  }

  void showSnackBarSuccess(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.green, duration: Duration(seconds: 2))
    );
  }

  void setValueFromDedup(BuildContext context,Map data){
    Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context,listen: false).controllerNamaIdentitas.text = data['mother_name'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerNoIdentitas.text = data['identity_number'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerNamaLengkap.text = data['full_name'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerTempatLahirSesuaiIdentitas.text = data['birth_place'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerTglLahir.text = data['birth_date'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).initialDateForTglLahir = data['initial_date_birth_date'];
  }

  void setValueCompanyFromDedup(BuildContext context,Map data){
    Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).controllerInstitutionName.text = data['full_name'];
    Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).controllerNPWP.text = data['identity_number'];
    Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).initialDateForDateEstablishment = data['initial_date_birth_date'];
    Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).controllerDateEstablishment.text = data['birth_date'];
//    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerTglLahir.text = data['birth_date'];
//    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).initialDateForTglLahir = data['initial_date_birth_date'];
  }
}