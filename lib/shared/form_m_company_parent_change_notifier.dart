import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import '../main.dart';
import 'change_notifier_app/info_application_change_notifier.dart';
import 'change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'change_notifier_app/information_collatelar_change_notifier.dart';
import 'change_notifier_app/information_object_unit_change_notifier.dart';
import 'change_notifier_app/information_salesman_change_notifier.dart';
import 'change_notifier_app/marketing_notes_change_notifier.dart';
import 'change_notifier_app/taksasi_unit_change_notifier.dart';
import 'constants.dart';
import 'form_m_company_manajemen_pic_change_notif.dart';
import 'form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'form_m_company_pemegang_saham_kelembagaan_change_notif.dart';
import 'form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'form_m_company_pemegang_saham_pribadi_change_notif.dart';
import 'form_m_company_penjamin_change_notif.dart';
import 'form_m_informasi_alamat_change_notif.dart';
import 'info_wmp_change_notifier.dart';

class FormMParentCompanyChangeNotifier with ChangeNotifier {
    List<GlobalKey<FormState>> formKeys = [
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
    ];

    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    int _selectedIndex = 0;
    bool _isMenuCompany = false;
//    bool _isRincian = false;
//    bool _isAlamat = false;

    bool _isPendapatan = false;
    bool _isPenjamin = false;
    bool _isMenuManagementPIC = false;
//    bool _isManajemenPIC = false;
//    bool _isManajemenPICAlamat = false;

    bool _isMenuPemegangSaham = false;
//    bool _isPemegangSahamPribadi = false;
//    bool _isPemegangSahamPribadiAlamat = false;
//    bool _isPemegangSahamKelembagaan = false;
//    bool _isPemegangSahamKelembagaanAlamat = false;



    bool _isInfoAppDone = false;
    bool _isMenuObjectInformationDone = false;
    bool _isInfoObjKaroseriDone = false;
    bool _isMenuDetailLoanDone = false;

//    bool _isUnitObjectInfoDone = false;
//    bool _isSalesmanInfoDone = false;
//    bool _isCollateralInfoDone = false;
//    bool _isCreditStructureDone = false;
//    bool _isCreditStructureTypeInstallmentDone = false;
//    bool _isMajorInsuranceDone = false;
//    bool _isAdditionalInsuranceDone = false;
//    bool _isInfoWMPDone = false;
//    bool _isCreditIncomeDone = false;
    bool _isCreditSubsidyDone = false;
    bool _isDocumentInfoDone = false;
    bool _isTaksasiUnitDone = false;
    bool _isMarketingNotesDone = false;


    bool get isMenuDetailLoanDone => _isMenuDetailLoanDone;

    set isMenuDetailLoanDone(bool value) {
      this._isMenuDetailLoanDone = value;
      notifyListeners();
    }
    bool get isMarketingNotesDone => _isMarketingNotesDone;

    set isMarketingNotesDone(bool value) {
        this._isMarketingNotesDone = value;
        notifyListeners();
    }

    bool get isMenuObjectInformationDone => _isMenuObjectInformationDone;

    set isMenuObjectInformationDone(bool value) {
        this._isMenuObjectInformationDone = value;
        notifyListeners();
    }

    int get selectedIndex => _selectedIndex;

    set selectedIndex(int value) {
        this._selectedIndex = value;
        notifyListeners();
    }

    bool get isMenuCompany => _isMenuCompany;

    set isMenuCompany(bool value) {
        this._isMenuCompany = value;
        notifyListeners();
    }

//    bool get isRincian => _isRincian;
//
//    set isRincian(bool value) {
//        this._isRincian = value;
//        notifyListeners();
//    }
//
//    bool get isAlamat => _isAlamat;
//
//    set isAlamat(bool value) {
//        this._isAlamat = value;
//        notifyListeners();
//    }

    bool get isPendapatan => _isPendapatan;

    set isPendapatan(bool value) {
        this._isPendapatan = value;
        notifyListeners();
    }

    bool get isPenjamin => _isPenjamin;

    set isPenjamin(bool value) {
        this._isPenjamin = value;
        notifyListeners();
    }

    bool get isMenuManagementPIC => _isMenuManagementPIC;

    set isMenuManagementPIC(bool value) {
        this._isMenuManagementPIC = value;
        notifyListeners();
    }

    bool get isMenuPemegangSaham => _isMenuPemegangSaham;

    set isMenuPemegangSaham(bool value) {
        this._isMenuPemegangSaham = value;
        notifyListeners();
    }

//    bool get isManajemenPIC => _isManajemenPIC;
//
//    set isManajemenPIC(bool value) {
//        this._isManajemenPIC = value;
//        notifyListeners();
//    }
//
//    bool get isManajemenPICAlamat => _isManajemenPICAlamat;
//
//    set isManajemenPICAlamat(bool value) {
//        this._isManajemenPICAlamat = value;
//        notifyListeners();
//    }

//    bool get isPemegangSahamPribadi => _isPemegangSahamPribadi;
//
//    set isPemegangSahamPribadi(bool value) {
//        this._isPemegangSahamPribadi = value;
//        notifyListeners();
//    }
//
//    bool get isPemegangSahamPribadiAlamat => _isPemegangSahamPribadiAlamat;
//
//    set isPemegangSahamPribadiAlamat(bool value) {
//        this._isPemegangSahamPribadiAlamat = value;
//        notifyListeners();
//    }
//
//    bool get isPemegangSahamKelembagaan => _isPemegangSahamKelembagaan;
//
//    set isPemegangSahamKelembagaan(bool value) {
//        this._isPemegangSahamKelembagaan = value;
//        notifyListeners();
//    }
//
//    bool get isPemegangSahamKelembagaanAlamat => _isPemegangSahamKelembagaanAlamat;
//
//    set isPemegangSahamKelembagaanAlamat(bool value) {
//        this._isPemegangSahamKelembagaanAlamat = value;
//        notifyListeners();
//    }

    bool get isInfoAppDone => _isInfoAppDone;

    set isInfoAppDone(bool value) {
        this._isInfoAppDone = value;
        notifyListeners();
    }

//    bool get isUnitObjectInfoDone => _isUnitObjectInfoDone;
//
//    set isUnitObjectInfoDone(bool value) {
//        this._isUnitObjectInfoDone = value;
//        notifyListeners();
//    }
//
//    bool get isSalesmanInfoDone => _isSalesmanInfoDone;
//
//    set isSalesmanInfoDone(bool value) {
//        this._isSalesmanInfoDone = value;
//        notifyListeners();
//    }
//
//    bool get isCollateralInfoDone => _isCollateralInfoDone;
//
//    set isCollateralInfoDone(bool value) {
//        this._isCollateralInfoDone = value;
//        notifyListeners();
//    }

    bool get isInfoObjKaroseriDone => _isInfoObjKaroseriDone;

    set isInfoObjKaroseriDone(bool value) {
        this._isInfoObjKaroseriDone = value;
        notifyListeners();
    }

//    bool get isMajorInsuranceDone => _isMajorInsuranceDone;
//
//    set isMajorInsuranceDone(bool value) {
//        this._isMajorInsuranceDone = value;
//        notifyListeners();
//    }
//
//    bool get isAdditionalInsuranceDone => _isAdditionalInsuranceDone;
//
//    set isAdditionalInsuranceDone(bool value) {
//        this._isAdditionalInsuranceDone = value;
//        notifyListeners();
//    }


    bool get isDocumentInfoDone => _isDocumentInfoDone;

    set isDocumentInfoDone(bool value) {
        this._isDocumentInfoDone = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void checkBtnNext(BuildContext context){
        final _form = formKeys[this._selectedIndex].currentState;
        var _providerFormMCompanyRincianChangeNotif = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
        var _providerFormMCompanyAlamatChangeNotif = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPendapatanChangeNotif = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPenjaminChangeNotif = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICChangeNotif = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICAlamatChangeNotif = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiChangeNotif = Provider.of<FormMCompanyPemegangSahamPribadiChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamKelembagaanChangeNotif = Provider.of<FormMCompanyPemegangSahamKelembagaanChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif = Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context, listen: false);

        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
        var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
        var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
        var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
        var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);

        var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
        var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);
        var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context,listen: false);

// selectedIndex+=1;
        if(this._selectedIndex == 0){
            if(!_providerFormMCompanyRincianChangeNotif.flag){
                _providerFormMCompanyRincianChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyAlamatChangeNotif.flag){
                _providerFormMCompanyAlamatChangeNotif.autoValidate = true;
            }
            if(_providerFormMCompanyRincianChangeNotif.flag && _providerFormMCompanyAlamatChangeNotif.flag){
                selectedIndex+=1;
                isMenuCompany = true;
                _providerFormMCompanyAlamatChangeNotif.saveToSQLite("10");

            }
        }
        else if (this._selectedIndex == 1) {
            if (_form.validate()) {
                selectedIndex += 1;
                isPendapatan = true;
                _providerFormMCompanyPendapatanChangeNotif.saveToSQLite(context);
            } else {
                _providerFormMCompanyPendapatanChangeNotif.autoValidate = true;
            }
        }
        else if (this._selectedIndex == 2) {
            if (_form.validate()) {
                _providerFormMCompanyPenjaminChangeNotif.saveToSQLite();
                selectedIndex += 1;
                isPenjamin = true;
            } else {
                _providerFormMCompanyPenjaminChangeNotif.autoValidate = true;
            }
        }
        else if(this._selectedIndex == 3){
            if(!_providerFormMCompanyManajemenPICChangeNotif.flag){
                _providerFormMCompanyManajemenPICChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyManajemenPICAlamatChangeNotif.flag){
                _providerFormMCompanyManajemenPICAlamatChangeNotif.autoValidate = true;
            }
            if(_providerFormMCompanyManajemenPICChangeNotif.flag && _providerFormMCompanyManajemenPICAlamatChangeNotif.flag){
                _providerFormMCompanyManajemenPICChangeNotif.saveToSQLite();
               _providerFormMCompanyManajemenPICAlamatChangeNotif.saveToSQLite("9");
                selectedIndex+=1;
                isMenuManagementPIC = true;
            }
        }
        else if(this._selectedIndex == 4){
            if(!_providerFormMCompanyPemegangSahamPribadiChangeNotif.flag){
                _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif.flag){
                _providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate = true;
            }
//            if(!_providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag){
//                _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = true;
//            }
//            if(!_providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif.flag){
//                _providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate = true;
//            }
            if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.flag){
                _providerFormMCompanyPemegangSahamPribadiChangeNotif.saveToSQLite();
                selectedIndex+=1;
                isMenuPemegangSaham = true;
            }
        }
        else if(this._selectedIndex == 5){
            if (_form.validate()) {
                _providerInfoApp.saveToSQLite(context);
                selectedIndex += 1;
                isInfoAppDone = true;
            } else {
                _providerInfoApp.autoValidate = true;
            }
        }
        else if(this._selectedIndex == 6){
            selectedIndex +=1;
            if(!_providerInfoObjectUnit.flag){
                _providerInfoObjectUnit.autoValidate = true;
            }
            if(_providerInfoSales.listInfoSales.length == 0){
                _providerInfoSales.autoValidate = true;
            }
            if(!_providerKolateral.flag){
                if(_providerKolateral.collateralTypeModel == null){
                    _providerKolateral.autoValidateAuto = true;
                    _providerKolateral.autoValidateProp = false;
                } else {
                    if(_providerKolateral.collateralTypeModel.id == "001"){
                        _providerKolateral.autoValidateAuto = true;
                        _providerKolateral.autoValidateProp = false;
                    } else {
                        _providerKolateral.autoValidateAuto = false;
                        _providerKolateral.autoValidateProp = true;
                    }
                }
            }
            if(_providerInfoObjectUnit.flag && _providerInfoSales.listInfoSales.length != 0 && _providerKolateral.flag){
                _providerInfoObjectUnit.saveToSQLite(context);
                _providerInfoSales.saveToSQLite(context);
                _providerKolateral.collateralTypeModel.id == "001" ? _providerKolateral.saveToSQLiteOto() : _providerKolateral.saveToSQLiteProperti(context, "6");
                _providerWMP.saveToSQLite();
                selectedIndex +=1;
                isMenuObjectInformationDone = true;
            }
        }
        else if(this._selectedIndex == 7){
            if(_providerKaroseri.listFormKaroseriObject.isNotEmpty){
                _providerKaroseri.saveToSQLite();
                selectedIndex += 1;
                isInfoObjKaroseriDone = true;
            } else {
                _showSnackBar("Info objek karoseri Utama tidak boleh kosong");
                _providerKaroseri.autoValidate = true;
            }
        }
        else if(this._selectedIndex == 8){
            if(!_providerInfoCreditStructure.flag){
                _providerInfoCreditStructure.autoValidate = true;
            }
            if(_providerMajorInsurance.listFormMajorInsurance.length == 0){
                _providerMajorInsurance.flag = true;
            }
            if(_providerAdditionalInsurance.listFormAdditionalInsurance.length == 0){
                _providerAdditionalInsurance.flag = true;
            }
            if(_providerInfoCreditStructure.flag && _providerMajorInsurance.listFormMajorInsurance.length != 0 && _providerAdditionalInsurance.listFormAdditionalInsurance.length == 0){
                _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
                _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
                _providerMajorInsurance.saveToSQLite();
                _providerAdditionalInsurance.saveToSQLite();
                selectedIndex += 1;
                isMenuDetailLoanDone = true;
            }
        }
        else if(this._selectedIndex == 9){
            if(_providerCreditSubsidy.listInfoCreditSubsidy.isNotEmpty){
                _providerCreditSubsidy.saveToSQLite();
                selectedIndex += 1;
                _isCreditSubsidyDone = true;
            } else {
                _showSnackBar("Info Kredit Subsidi tidak boleh kosong");
            }
        }
        else if(this._selectedIndex == 10){
            if(_providerInfoDocument.listInfoDocument.isNotEmpty){
                _providerInfoDocument.saveToSQLite();
                if(_providerTaksasiUnit.isVisible){
                    selectedIndex += 1;
                }
                else {
                    selectedIndex += 2;
                }
                _isDocumentInfoDone = true;
            } else {
                _showSnackBar("Info Dokumen tidak boleh kosong");
            }
        }
        else if(this._selectedIndex == 11){
            if(_providerInfoObjectUnit.groupObjectSelected != null){
                if(_providerInfoObjectUnit.groupObjectSelected.KODE == "001"){
                    if(_form.validate()){
                        selectedIndex += 1;
                        isTaksasiUnitDone = true;
                    }
                    _providerTaksasiUnit.autoValidateMotor;
                }
                else if(_providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
                    if(_form.validate()){
                        selectedIndex += 1;
                        isTaksasiUnitDone = true;
                    }
                    _providerTaksasiUnit.autoValidateCar;
                }
            }
        }
        else if(this._selectedIndex == 12){
            if(_form.validate()){
                selectedIndex += 1;
                isTaksasiUnitDone = true;
            }
            else{
                _providerMarketingNotes.autoValidate = true;
            }
        }

    }

    void checkTabDrawer(BuildContext context, int index) {
        this.selectedIndex = index;
    }

    void backBtn(BuildContext context) {
        if(selectedIndex != 0){
            this._selectedIndex -= 1;
            notifyListeners();
        }
    }

    void _showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

//    bool get isCreditStructureDone => _isCreditStructureDone;
//
//    set isCreditStructureDone(bool value) {
//        this._isCreditStructureDone = value;
//        notifyListeners();
//    }

//    bool get isCreditStructureTypeInstallmentDone => _isCreditStructureTypeInstallmentDone;
//
//    set isCreditStructureTypeInstallmentDone(bool value) {
//        this._isCreditStructureTypeInstallmentDone = value;
//        notifyListeners();
//    }

    bool get isTaksasiUnitDone => _isTaksasiUnitDone;

    set isTaksasiUnitDone(bool value) {
        this._isTaksasiUnitDone = value;
        notifyListeners();
    }

    bool get isCreditSubsidyDone => _isCreditSubsidyDone;

    set isCreditSubsidyDone(bool value) {
        this._isCreditSubsidyDone = value;
        notifyListeners();
    }

    void clearData(BuildContext context){
        this._selectedIndex = 0;
        this._isMenuCompany = false;
        this._isPendapatan = false;
        this._isPenjamin = false;
        this._isMenuManagementPIC = false;
        this._isMenuPemegangSaham = false;

        // Form M Company
        Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false).clearDataRincian();
        Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false).clearDataAlamat();
        Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false).clearDataPendapatan();
        Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false).clearDataManajemenPIC();
        Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false).clearDataManajemenPICAlamat();
        Provider.of<FormMCompanyPemegangSahamPribadiChangeNotifier>(context, listen: false).clearDataPemegangSahamPribadi();
        Provider.of<FormMCompanyPemegangSahamKelembagaanChangeNotifier>(context, listen: false).clearDataPemegangSahamKelembagaan();

        // Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).clearDataInfoUnitObject();
        // Provider.of<InformationSalesmanChangeNotifier>(context, listen: false).clearDataInfoSales();
        // Provider.of<InformationCollateralChangeNotifier>(context, listen: false).clearDataInfoCollateral(); //diganti .clear()
        // Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).clearDataInfoObjectKaroseri();
        // Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false).clearInfoCreditStructure();
        // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).clearData();
        // Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).clearDataInfoObjectKaroseri();
        // Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).clearMajorInsurance();
        // Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).clearListAdditionalInsurance();
        // Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).clearDataInfoCreditIncome();
        // Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).clearInfoCreditSubsidy();
        // Provider.of<InfoDocumentChangeNotifier>(context, listen: false).clearInfoDocument();
        // Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).clearTaksasiUnit();
        // Provider.of<MarketingNotesChangeNotifier>(context, listen: false).clearMarketingNotes();
    }

    bool isValidateTabDrawer(BuildContext context,int index){
        final _form = formKeys[index].currentState;
        var _providerFormMCompanyRincianChangeNotif = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
        var _providerFormMCompanyAlamatChangeNotif = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPendapatanChangeNotif = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPenjaminChangeNotif = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICChangeNotif = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICAlamatChangeNotif = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiChangeNotif = Provider.of<FormMCompanyPemegangSahamPribadiChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false);
        var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
        var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
        var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
        var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
        var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);

        var _isValidate = false;


        if(index == 0){
            if(!_providerFormMCompanyRincianChangeNotif.flag){
                _providerFormMCompanyRincianChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyAlamatChangeNotif.flag){
                _providerFormMCompanyAlamatChangeNotif.autoValidate = true;
            }
            if(_providerFormMCompanyRincianChangeNotif.flag && _providerFormMCompanyAlamatChangeNotif.flag){
                selectedIndex+=1;
                isMenuCompany = true;
//                _providerFormMCompanyAlamatChangeNotif.saveToSQLite("10");
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }
        else if (index == 1) {
            if(isPendapatan){
                selectedIndex += 1;
                isPendapatan = true;
                _isValidate = true;
            } else {
                _isValidate = false;
            }
//            if (_form.validate()) {
//                selectedIndex += 1;
//                isPendapatan = true;
//                _providerFormMCompanyPendapatanChangeNotif.saveToSQLite(context);
//            } else {
//                _providerFormMCompanyPendapatanChangeNotif.autoValidate = true;
//            }
        }
        else if (index == 2) {
            if(isPenjamin){
                selectedIndex += 1;
                _isValidate = false;
            } else {
                _isValidate = true;
            }
//            if (_form.validate()) {
//                _providerFormMCompanyPenjaminChangeNotif.saveToSQLite();
//                selectedIndex += 1;
//                isPenjamin = true;
//            } else {
//                _providerFormMCompanyPenjaminChangeNotif.autoValidate = true;
//            }
        }
        else if(index == 3){
            if(!_providerFormMCompanyManajemenPICChangeNotif.flag){
                _providerFormMCompanyManajemenPICChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyManajemenPICAlamatChangeNotif.flag){
                _providerFormMCompanyManajemenPICAlamatChangeNotif.autoValidate = true;
            }
            if(_providerFormMCompanyManajemenPICChangeNotif.flag && _providerFormMCompanyManajemenPICAlamatChangeNotif.flag){
                _providerFormMCompanyManajemenPICAlamatChangeNotif.saveToSQLite("9");
                selectedIndex+=1;
                isMenuManagementPIC = true;
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }
        else if(index == 4){
            if(!_providerFormMCompanyPemegangSahamPribadiChangeNotif.flag){
                _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif.flag){
                _providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate = true;
            }
//            if(!_providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag){
//                _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = true;
//            }
//            if(!_providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif.flag){
//                _providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate = true;
//            }
            if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.flag){
                _providerFormMCompanyPemegangSahamPribadiChangeNotif.saveToSQLite();
                selectedIndex+=1;
                isMenuPemegangSaham = true;
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }

        else if (index == 5){
            print(_form);
            if(isInfoAppDone){
//        _providerInfoApp.saveToSQLite(context);
                selectedIndex += 1;
                isInfoAppDone = true;
                _isValidate = false;
            } else {
                _isValidate = true;
            }
//      if (_form.validate()) {
//        _providerInfoApp.saveToSQLite(context);
//        selectedIndex += 1;
//        isInfoAppDone = true;
//        _isValidate = false;
//      } else {
//        _providerInfoApp.autoValidate = true;
//        _isValidate = true;
//      }
        }
        else if (index == 6){
            if(!_providerInfoObjectUnit.flag){
                _providerInfoObjectUnit.autoValidate = true;
                _isValidate = true;
            }
            if(_providerInfoSales.listInfoSales.length == 0){
                _providerInfoSales.autoValidate = true;
                _isValidate = true;
            }
            if(!_providerKolateral.flag){
                if(_providerKolateral.collateralTypeModel == null){
                    _providerKolateral.autoValidateAuto = true;
                    _providerKolateral.autoValidateProp = false;
                } else {
                    if(_providerKolateral.collateralTypeModel.id == "001"){
                        _providerKolateral.autoValidateAuto = true;
                        _providerKolateral.autoValidateProp = false;
                        _isValidate = true;
                    } else {
                        _providerKolateral.autoValidateAuto = false;
                        _providerKolateral.autoValidateProp = true;
                        _isValidate = true;
                    }
                }
            }
            if(_providerInfoObjectUnit.flag && _providerInfoSales.listInfoSales.length != 0 && _providerKolateral.flag){
//        _providerInfoObjectUnit.saveToSQLite(context);
//        _providerInfoSales.saveToSQLite(context);
//        _providerKolateral.collateralTypeModel.id == "001" ? _providerKolateral.saveToSQLiteOto() : _providerKolateral.saveToSQLiteProperti(context, "6");
                selectedIndex +=1;
                isMenuObjectInformationDone = true;
                _isValidate = false;
            }
        }
        else if (index == 7){
            if(_providerKaroseri.listFormKaroseriObject.isNotEmpty){
//        _providerKaroseri.saveToSQLite();
                selectedIndex += 1;
                isInfoObjKaroseriDone = true;
                _isValidate = false;
            } else {
                _showSnackBar("Info objek karoseri Utama tidak boleh kosong");
                _providerKaroseri.autoValidate = true;
                _isValidate = true;
            }
        }
        else if (index == 8){
            if(!_providerInfoCreditStructure.flag){
                _providerInfoCreditStructure.autoValidate = true;
                _isValidate = true;
            }
            if(_providerMajorInsurance.listFormMajorInsurance.length == 0){
                _providerMajorInsurance.flag = true;
                _isValidate = true;
            }
            if(_providerAdditionalInsurance.listFormAdditionalInsurance.length == 0){
                _providerAdditionalInsurance.flag = true;
                _isValidate = true;
            }
            if(_providerInfoCreditStructure.flag && _providerMajorInsurance.listFormMajorInsurance.length != 0 && _providerAdditionalInsurance.listFormAdditionalInsurance.length == 0){
                _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
                _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
//        _providerMajorInsurance.saveToSQLite();
//        _providerAdditionalInsurance.saveToSQLite();
                selectedIndex += 1;
                isMenuDetailLoanDone = true;
                _isValidate = false;
            }
        }
        else if (index == 9){
            if(_providerCreditSubsidy.listInfoCreditSubsidy.isNotEmpty){
//        _providerCreditSubsidy.saveToSQLite();
                selectedIndex += 1;
                _isCreditSubsidyDone = true;
                _isValidate = false;
            } else {
                _showSnackBar("Info Kredit Subsidi tidak boleh kosong");
                _isValidate = true;
            }
        }
        else if (index == 10){
            if(_providerInfoDocument.listInfoDocument.isNotEmpty){
//        _providerInfoDocument.saveToSQLite();
                if(_providerTaksasiUnit.isVisible){
                    selectedIndex += 1;
                    _isValidate = false;
                }
                else {
                    selectedIndex += 2;
                    _isValidate = false;
                }
                _isDocumentInfoDone = true;
            } else {
                _showSnackBar("Info Dokumen tidak boleh kosong");
                _isValidate = true;
            }
        }
        else if (index == 11){
            if(_providerInfoObjectUnit.groupObjectSelected != null){
                if(_providerInfoObjectUnit.groupObjectSelected.KODE == "001"){
                    if(_form.validate()){
                        selectedIndex += 1;
                        isTaksasiUnitDone = true;
                        _isValidate = false;
                    }
                    _providerTaksasiUnit.autoValidateMotor;
                    _isValidate = true;
                }
                else if(_providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
                    if(_form.validate()){
                        selectedIndex += 1;
                        isTaksasiUnitDone = true;
                        _isValidate = false;
                    }
                    _providerTaksasiUnit.autoValidateCar;
                    _isValidate = true;
                }
            }
        }
        return _isValidate;
    }


    void actionMoreButton(BuildContext context,String action) async{

        var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);

        var _providerFormMCompanyRincianChangeNotif = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
        var _providerFormMCompanyAlamatChangeNotif = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPendapatanChangeNotif = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPenjaminChangeNotif = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICChangeNotif = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICAlamatChangeNotif = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiChangeNotif = Provider.of<FormMCompanyPemegangSahamPribadiChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false);

        var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
        var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
        var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
        var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
        var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);
        var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);

        for(int i=0; i <= this._selectedIndex; i++){
            if(action == LabelPopUpMenuButtonSaveToDraftBackToHome.SAVE_TO_DRAFT){
                if(i == 0){
                    //rincian lembaga
                    if(await _providerFormMCompanyRincianChangeNotif.deleteSQLite()){
                        _providerFormMCompanyRincianChangeNotif.saveToSQLite(context);
                    }
                    //alamat lembaga
                    if(await _providerInfoAlamat.deleteSQLite("10")){
                        _providerFormMCompanyAlamatChangeNotif.saveToSQLite("10");
                    }
                }
                else if(i == 1){
                    if(await _providerFormMCompanyPendapatanChangeNotif.deleteSQLite()){
                        _providerFormMCompanyPendapatanChangeNotif.saveToSQLite(context);
                    }
                }
                else if(i == 2){
                    _providerFormMCompanyPenjaminChangeNotif.deleteSQLite();
                    _providerFormMCompanyPenjaminChangeNotif.saveToSQLite();
                }
                else if(i == 3){
                    //manajemen pic
                    _providerFormMCompanyManajemenPICChangeNotif.saveToSQLite();
                    //alamat manajemen pic
                    if(await _providerInfoAlamat.deleteSQLite("9")){
                        _providerFormMCompanyManajemenPICAlamatChangeNotif.saveToSQLite("9");
                    }
                }
                else if(i == 4){
                    if(await _providerFormMCompanyPemegangSahamPribadiChangeNotif.deleteSQLite()){
                        _providerFormMCompanyPemegangSahamPribadiChangeNotif.saveToSQLite();
                    }
                }
                else if(i == 5){
                    _providerInfoApp.deleteSQLite();
                    _providerInfoApp.saveToSQLite(context);
                }
                else if(i == 6){
                    _providerInfoObjectUnit.deleteSQLite();
                    _providerInfoSales.deleteSQLite();
                    _providerKolateral.collateralTypeModel.id == "001" ? _providerKolateral.deleteSQLiteCollaOto() : _providerKolateral.deleteSQLiteCollaProp();
                    _providerInfoAlamat.deleteSQLite("6");
                    _providerInfoObjectUnit.saveToSQLite(context);
                    _providerInfoSales.saveToSQLite(context);
                    _providerKolateral.collateralTypeModel.id == "001" ? _providerKolateral.saveToSQLiteOto() : _providerKolateral.saveToSQLiteProperti(context, "6");
                    _providerWMP.deleteSQLite();
                    _providerWMP.saveToSQLite();
                }
                else if(i == 7){
                    _providerKaroseri.deleteSQLite();
                    _providerKaroseri.saveToSQLite();
                }
                else if(i == 8){
                    _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
                    _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
                    _providerMajorInsurance.saveToSQLite();
                    _providerAdditionalInsurance.saveToSQLite();
                }
                else if(i == 9){
                    _providerCreditSubsidy.deleteSQLIte();
                    _providerCreditSubsidy.saveToSQLite();
                }
                else if(i == 10){
                    _providerInfoDocument.deleteSQLite();
                    _providerInfoDocument.saveToSQLite();
                }
                else if(i == 11){
                    _providerTaksasiUnit.deleteSQLite();
                    _providerTaksasiUnit.saveToSQLite(context);
                }
            }
            else{
                isShowDialog(context);
            }
        }
    }

    void isShowDialog(BuildContext context) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans"
                    ),
                    child: AlertDialog(
                        title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Text("Apakah anda yakin ingin kembali ke dashboard?"),
                        actions: <Widget>[
                            FlatButton(
                                onPressed: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
                                },
                                child: Text("Ya",
                                    style: TextStyle(
                                        color: primaryOrange,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            ),
                            FlatButton(
                                onPressed: (){
                                    Navigator.pop(context);
                                },
                                child: Text("Tidak",
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            )
                        ],
                    ),
                );
            }
        );
    }
}
