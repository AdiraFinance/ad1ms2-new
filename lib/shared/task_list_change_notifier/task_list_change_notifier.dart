import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/task_list_model.dart';
import 'package:ad1ms2_dev/screens/SA/sa_job_mayor.dart';
import 'package:ad1ms2_dev/screens/form_AOS/form_AOS.dart';
import 'package:ad1ms2_dev/screens/form_IA/form_IA.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_parent.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TaskListChangeNotifier with ChangeNotifier{
  List<TaskListModel> _taskList = [
    // TaskListModel("001", "Lima Tuju", DateTime(dateNow.year,dateNow.month,dateNow.day - 3), "004","NP"),
    // TaskListModel("002", "Lima Lapan", DateTime(dateNow.year,dateNow.month,dateNow.day - 2), "003","HP"),
    // TaskListModel("003", "Lima Sembilan", DateTime(dateNow.year,dateNow.month,dateNow.day - 1), "002","LP"),
    // TaskListModel("004", "Lima Enam", DateTime(dateNow.year,dateNow.month,dateNow.day), "001","LP"),
  ];

  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<TaskListModel> get taskList => _taskList;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  // void navigateForm(String typeFormId, BuildContext context) async{
  //   if(typeFormId == "SA"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => SAJobMayor()));
  //   }
  //   else if(typeFormId == "AOS"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormAOS()));
  //   }
  //   else if(typeFormId == "IA"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormIA()));
  //   }
  //   else{
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId)));
  //   }
  // }

  void getTaskList() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    this._taskList.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    // try {
      var _body = jsonEncode({
        "P_NIK": _preferences.getString("username"),
      });
      print(_body);

      final _response = await _http.post(
        "${BaseUrl.urlPublic}api/tasklist/get-tasklist-ms2",
          // "https://103.110.89.34/public/ms2dev/api/tasklist/get-tasklist-ms2",
          body: _body,
          headers: {"Content-Type":"application/json"}
      );
      print(_response.statusCode);

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result.isEmpty) {
          showSnackBar("Task List tidak ditemukan");
          loadData = false;
        } else {
          for(int i=0; i < _result.length; i++){
            var _date = DateTime.parse(_result[i]['ORDER_DATE'].toString().replaceAll("T", " "));
            this._taskList.add(
              TaskListModel(
                  _result[i]['ORDER_NO'],
                  _date,
                  _result[i]['CUST_NAME'],
                  _result[i]['CUST_TYPE'],
                  _result[i]['LAST_KNOWN_STATE'],
                  _result[i]['LAST_KNOWN_HANDLED_BY'],
                  _result[i]['IDX'],
                  _result[i]['SOURCE_APPLICATION'],
                  _result[i]['PRIORITY']
              )
            );
          }
          loadData = false;
        }
      } else {
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    // } catch(e) {
    //   showSnackBar("Error $e");
    //   this._loadData = false;
    // }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}