import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/material.dart';

import 'constants.dart';

class DatePickerShared {
  Future<DateTime> selectStartDate(BuildContext context, DateTime initialDate,
      {bool canAccessNextDay}) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: DateTime(1900, 1, 1),
        lastDate: canAccessNextDay
            ? DateTime(dateNow.year + 2, dateNow.month, dateNow.day)
            : DateTime(dateNow.year, dateNow.month, dateNow.day));
    if (picked != null) {
      return picked;
    } else {
      return null;
    }
  }
}
