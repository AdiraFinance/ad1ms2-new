
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/coverage1_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

Future<List<CoverageModel>> getInsuranceType(BuildContext context) async{

  print("di eksekusi");
  List<CoverageModel> _listCoverage = [];
  try{
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_INSR_COMPANY_ID" : "02",
      "P_INSR_LEVEL" : "2",
      "P_INSR_PRODUCT_ID" :"005",
      "P_PARA_OBJECT_ID" : "004"
//      Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id
    });

    final _response = await _http.post(
        "${BaseUrl.urlPublic}api/asuransi/get-insurance-type",
        body: _body,
        headers: {"Content-Type":"application/json"}
    ).timeout(Duration(seconds: 10));

    final _result = jsonDecode(_response.body);
    print(_result);
    List _data = _result['data'];
    if(_data.isNotEmpty){
      for(int i=0; i < _data.length; i++){
        _listCoverage.add(CoverageModel(_data[i]['PARENT_KODE'], _data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
      return _listCoverage;
    }
    else{
      throw Exception("Data tidak ditemukan");
    }
  }
  on TimeoutException catch(_){
    throw Exception("Timeout connection");
  }
  catch (e) {
    throw Exception("${e.toString()}");
  }
}