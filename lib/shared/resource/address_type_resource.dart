import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:http/io_client.dart';

 Future<List<JenisAlamatModel>> getAddressType(int flag) async{
     List<JenisAlamatModel> _listJenisAlamatAPI = [];
     final ioc = new HttpClient();
     ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
     final _http = IOClient(ioc);
     final _response = await _http.get("${BaseUrl.urlPublic}api/address/get-address-type");
     final _data = jsonDecode(_response.body);

//     identitas
     if(flag == 1){
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] == "03"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }
//     domisili
     else if(flag == 2){
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] == "01"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }
     else if(flag == 10){
         for(int i=0; i < _data['data'].length;i++){
             _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
         }
     }
     else {
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] != "03"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }

     return _listJenisAlamatAPI;
 }