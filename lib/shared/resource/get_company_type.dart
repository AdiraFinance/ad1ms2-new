import 'dart:convert';
import 'dart:io';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:http/io_client.dart';

class GetCompanyType{
  Future<Map> getCompanyTypeData() async{
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
      // "${BaseUrl.unit}api/parameter/get-group-object",
        "${BaseUrl.urlPublic}jenis-rehab/get_jenis_perusahaan",
        headers: {"Content-Type":"application/json"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      return _result;
    }
    else{
      throw Exception("Failed get data error ${_response.statusCode}");
    }
  }
}