import 'dart:convert';
import 'dart:io';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:http/io_client.dart';

Future<Map> getAsuransiPerluasan(String  body) async{
  try{
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    final _response = await _http.post(
        "${BaseUrl.urlPublic}api/asuransi/get-rate-asuransi-utama",
        body: body,
        headers: {"Content-Type":"application/json"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      print("result nih $_result");
      List _data = _result['data'];
      if(_data.isNotEmpty){
        return _data[0];
      }
      else{
        throw "Data tidak ditemukan";
      }
    }
    else{
      throw "Error response ${_response.statusCode}";
    }
  }
  catch (e) {
    throw "Error ${e.toString()}";
  }
}