import 'dart:collection';

import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/screens/map_page.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/address_type_resource.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import '../main.dart';
import 'form_m_company_alamat_change_notif.dart';
import 'form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';

class FormMAddAddressCompanyChangeNotifier with ChangeNotifier {
    bool _autoValidate = false;
    bool _isSameWithIdentity = false;
    bool _isCorrespondence = false;
    TextEditingController _controllerAlamat = TextEditingController();
    TextEditingController _controllerRT = TextEditingController();
    TextEditingController _controllerRW = TextEditingController();
    TextEditingController _controllerKelurahan = TextEditingController();
    TextEditingController _controllerKecamatan = TextEditingController();
    TextEditingController _controllerKota = TextEditingController();
    TextEditingController _controllerProvinsi = TextEditingController();
    TextEditingController _controllerTelephone1Area = TextEditingController();
    TextEditingController _controllerTelephone1 = TextEditingController();
    TextEditingController _controllerTelephone2Area = TextEditingController();
    TextEditingController _controllerTelephone2 = TextEditingController();
    TextEditingController _controllerFaxArea = TextEditingController();
    TextEditingController _controllerFax = TextEditingController();
    TextEditingController _controllerPostalCode = TextEditingController();
    TextEditingController _controllerAddressFromMap = TextEditingController();
    JenisAlamatModel _jenisAlamatSelected;
    JenisAlamatModel _jenisAlamatSelectedTemp;
    KelurahanModel _kelurahanSelected;
    KelurahanModel _kelurahanSelectedTemp;
    AddressGuarantorModelCompany _addressModelTemp;
    String _alamatTemp,
        _rtTemp,
        _rwTemp,
        _kelurahanTemp,
        _kecamatanTemp,
        _kotaTemp,
        _provinsiTemp,
        _phone1AreaTemp,
        _phone1Temp,
        _phone2AreaTemp,
        _phone2Temp,
        _faxAreaTemp,
        _faxTemp,
        _postalCodeTemp;
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    bool _enableTfAddress = true;
    bool _enableTfRT = true;
    bool _enableTfRW = true;
    bool _enableTfKelurahan = true;
    bool _enableTfKecamatan = true;
    bool _enableTfKota = true;
    bool _enableTfProv = true;
    bool _enableTfPostalCode = true;
    bool _enableTfTelephone1Area = true;
    bool _enableTfTelephone2Area = true;
    bool _enableTfPhone1 = true;
    bool _enableTfPhone2 = true;
    bool _enableTfFaxArea = true;
    bool _enableTfFax = true;
    Map _addressFromMap;

    List<JenisAlamatModel> _listJenisAlamat = [];

    Future<void> addDataListJenisAlamat(AddressGuarantorModelCompany data, BuildContext context, int flag,
        bool isSameWithIdentity,int index, int typeAddress) async{
        var _provider = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: false);
        var _providerAlamatCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        var _providerSahamLembaga = Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context, listen: false);
        try {
            this._listJenisAlamat.clear();
            if (flag == 0) {
                print("create baru");
                // add
                if(typeAddress == 1){
                    if (_provider.listGuarantorAddress.isEmpty) {
                        this._listJenisAlamat = await getAddressType(1);
                    }
                    else {
                        this._listJenisAlamat = await getAddressType(3);
                        // remove used type
                        for(int x=0; x<_provider.listGuarantorAddress.length; x++){
                            for(int i=0; i < _listJenisAlamat.length;i++){
                                if(_listJenisAlamat[i].KODE == _provider.listGuarantorAddress[x].jenisAlamatModel.KODE){
                                    _listJenisAlamat.removeAt(i);
                                }
                            }
                        }
                    }
                }
                if(typeAddress == 2){
                    if (_providerAlamatCompany.listAlamatKorespondensi.isEmpty) {
                        this._listJenisAlamat = await getAddressType(1);
                    }
                    else {
                        this._listJenisAlamat = await getAddressType(3);
                        // remove used type
                        for(int x=0; x<_providerAlamatCompany.listAlamatKorespondensi.length; x++){
                            for(int i=0; i < _listJenisAlamat.length;i++){
                                if(_listJenisAlamat[i].KODE == _providerAlamatCompany.listAlamatKorespondensi[x].jenisAlamatModel.KODE){
                                    _listJenisAlamat.removeAt(i);
                                }
                            }
                        }
                    }
                }
                if(typeAddress == 3){
                    if (_providerSahamLembaga.listPemegangSahamKelembagaanAddress.isEmpty) {
                        this._listJenisAlamat = await getAddressType(1);
                    }
                    else {
                        this._listJenisAlamat = await getAddressType(3);
                        // remove used type
                        for(int x=0; x<_providerSahamLembaga.listPemegangSahamKelembagaanAddress.length; x++){
                            for(int i=0; i < _listJenisAlamat.length;i++){
                                if(_listJenisAlamat[i].KODE == _providerSahamLembaga.listPemegangSahamKelembagaanAddress[x].jenisAlamatModel.KODE){
                                    _listJenisAlamat.removeAt(i);
                                }
                            }
                        }
                    }
                }
            }
            else {
                // edit
                if(typeAddress == 1){
                    print("edit 1");
                    if (_provider.listGuarantorAddress[index].jenisAlamatModel.KODE == '03') {
                        this._listJenisAlamat = await getAddressType(1);
                    } else if (_provider.listGuarantorAddress[index].jenisAlamatModel.KODE == '01'){
                        this._listJenisAlamat = await getAddressType(2);
                    }
                    else {
                        this._listJenisAlamat = await getAddressType(3);
                    }
                    // remove used type
                    for(int x=0; x<_provider.listGuarantorAddress.length; x++){
                        for(int i=0; i < _listJenisAlamat.length;i++){
                            if(_listJenisAlamat[i].KODE == _provider.listGuarantorAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                                _listJenisAlamat.removeAt(i);
                            }
                        }
                    }
                }
                if(typeAddress == 2){
                    print("edit 2");
                    if (_providerAlamatCompany.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '03') {
                        this._listJenisAlamat = await getAddressType(1);
                    } else if (_providerAlamatCompany.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '01'){
                        this._listJenisAlamat = await getAddressType(2);
                    }
                    else {
                        this._listJenisAlamat = await getAddressType(3);
                    }
                    // remove used type
                    for(int x=0; x<_providerAlamatCompany.listAlamatKorespondensi.length; x++){
                        for(int i=0; i < _listJenisAlamat.length;i++){
                            if(_listJenisAlamat[i].KODE == _providerAlamatCompany.listAlamatKorespondensi[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                                _listJenisAlamat.removeAt(i);
                            }
                        }
                    }
                }
                if(typeAddress == 3){
                    print("edit 2");
                    if (_providerSahamLembaga.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.KODE == '03') {
                        this._listJenisAlamat = await getAddressType(1);
                    } else if (_providerSahamLembaga.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.KODE == '01'){
                        this._listJenisAlamat = await getAddressType(2);
                    }
                    else {
                        this._listJenisAlamat = await getAddressType(3);
                    }
                    // remove used type
                    for(int x=0; x<_providerSahamLembaga.listPemegangSahamKelembagaanAddress.length; x++){
                        for(int i=0; i < _listJenisAlamat.length;i++){
                            if(_listJenisAlamat[i].KODE == _providerSahamLembaga.listPemegangSahamKelembagaanAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                                _listJenisAlamat.removeAt(i);
                            }
                        }
                    }
                }
                setValueForEdit(data, context);
            }
            notifyListeners();
        } catch (e) {
            print(e);
        }
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    TextEditingController get controllerPostalCode => _controllerPostalCode;

    TextEditingController get controllerTelephone1 => _controllerTelephone1;

    TextEditingController get controllerTelephone1Area =>
        _controllerTelephone1Area;

    TextEditingController get controllerProvinsi => _controllerProvinsi;

    TextEditingController get controllerKota => _controllerKota;

    TextEditingController get controllerKecamatan => _controllerKecamatan;

    TextEditingController get controllerKelurahan => _controllerKelurahan;

    TextEditingController get controllerRT => _controllerRT;

    TextEditingController get controllerRW => _controllerRW;

    TextEditingController get controllerAlamat => _controllerAlamat;

    TextEditingController get controllerTelephone2Area =>
        _controllerTelephone2Area;

    TextEditingController get controllerTelephone2 => _controllerTelephone2;

    TextEditingController get controllerFax => _controllerFax;

    TextEditingController get controllerFaxArea => _controllerFaxArea;

    JenisAlamatModel get jenisAlamatSelected => _jenisAlamatSelected;

    TextEditingController get controllerAddressFromMap => _controllerAddressFromMap;

    set jenisAlamatSelected(JenisAlamatModel value) {
        this._jenisAlamatSelected = value;
        notifyListeners();
    }

    KelurahanModel get kelurahanSelected => _kelurahanSelected;

    set kelurahanSelected(KelurahanModel value) {
        this._kelurahanSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<JenisAlamatModel> get listJenisAlamat {
        return UnmodifiableListView(this._listJenisAlamat);
    }

    bool get isCorrespondence => _isCorrespondence;

    set isCorrespondence(bool value) {
        this._isCorrespondence = value;
        notifyListeners();
    }

    bool get isSameWithIdentity => _isSameWithIdentity;

    set isSameWithIdentity(bool value) {
        this._isSameWithIdentity = value;
        notifyListeners();
    }

    bool get enableTfPhone1 => _enableTfPhone1;

    set enableTfPhone1(bool value) {
        this._enableTfPhone1 = value;
    }

    bool get enableTfTelephone1Area => _enableTfTelephone1Area;

    set enableTfTelephone1Area(bool value) {
        this._enableTfTelephone1Area = value;
    }

    bool get enableTfTelephone2Area => _enableTfTelephone2Area;

    set enableTfTelephone2Area(bool value) {
        this._enableTfTelephone2Area = value;
    }

    bool get enableTfPhone2 => _enableTfPhone2;

    set enableTfPhone2(bool value) {
        this._enableTfPhone2 = value;
    }

    bool get enableTfFax => _enableTfFax;

    set enableTfFax(bool value) {
        this._enableTfFax = value;
    }

    bool get enableTfFaxArea => _enableTfFaxArea;

    set enableTfFaxArea(bool value) {
        this._enableTfFaxArea = value;
    }

    bool get enableTfPostalCode => _enableTfPostalCode;

    set enableTfPostalCode(bool value) {
        this._enableTfPostalCode = value;
    }

    bool get enableTfProv => _enableTfProv;

    set enableTfProv(bool value) {
        this._enableTfProv = value;
    }

    bool get enableTfKota => _enableTfKota;

    set enableTfKota(bool value) {
        this._enableTfKota = value;
    }

    bool get enableTfKecamatan => _enableTfKecamatan;

    set enableTfKecamatan(bool value) {
        this._enableTfKecamatan = value;
    }

    bool get enableTfKelurahan => _enableTfKelurahan;

    set enableTfKelurahan(bool value) {
        this._enableTfKelurahan = value;
    }

    bool get enableTfRW => _enableTfRW;

    set enableTfRW(bool value) {
        this._enableTfRW = value;
    }

    bool get enableTfRT => _enableTfRT;

    set enableTfRT(bool value) {
        this._enableTfRT = value;
    }

    bool get enableTfAddress => _enableTfAddress;

    set enableTfAddress(bool value) {
        this._enableTfAddress = value;
    }

    void isShowDialog(BuildContext context, int flag, int index, int typeAddress) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans"
                    ),
                    child: AlertDialog(
                        title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Text("Apakah domisili sama dengan identitas?"),
                        actions: <Widget>[
                            FlatButton(
                                onPressed: (){
                                    _isSameWithIdentity = true;
                                    if(typeAddress == 1){
                                        Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                                            listen: true)
                                            .addGuarantorAddress(AddressGuarantorModelCompany(
                                            this._jenisAlamatSelected,
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));

                                        Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                                            listen: true)
                                            .addGuarantorAddress(AddressGuarantorModelCompany(
                                            JenisAlamatModel("01", "DOMISILI"),
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));
                                    }
                                    else if(typeAddress == 2){
                                        Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addAlamatKorespondensi(AddressGuarantorModelCompany(
                                            this._jenisAlamatSelected,
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));

                                        Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addAlamatKorespondensi(AddressGuarantorModelCompany(
                                            JenisAlamatModel("01", "DOMISILI"),
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));
                                    }
                                    else if(typeAddress == 3){
                                        Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addPemegangSahamKelembagaanAddress(AddressGuarantorModelCompany(
                                            this._jenisAlamatSelected,
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));

                                        Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addPemegangSahamKelembagaanAddress(AddressGuarantorModelCompany(
                                            JenisAlamatModel("01", "DOMISILI"),
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));
                                    }
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                },
                                child: Text("Ya",
                                    style: TextStyle(
                                        color: primaryOrange,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            ),
                            FlatButton(
                                onPressed: (){
                                    //bikin identitas
                                    if(typeAddress == 1){
                                        Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                                            listen: true)
                                            .addGuarantorAddress(AddressGuarantorModelCompany(
                                            this._jenisAlamatSelected,
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));
                                    }
                                    else if(typeAddress == 2){
                                        Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addAlamatKorespondensi(AddressGuarantorModelCompany(
                                            this._jenisAlamatSelected,
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));
                                    }
                                    else if(typeAddress == 3){
                                        Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addPemegangSahamKelembagaanAddress(AddressGuarantorModelCompany(
                                            this._jenisAlamatSelected,
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence));
                                    }
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                },
                                child: Text("Tidak",
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            )
                        ],
                    ),
                );
            }
        );
    }

    void check(BuildContext context, int flag, int index, int typeAddress) {
//  typeAddres 1 = list alamat guarantor addres individu
//  typeAddres 2 = list alamat company
//  typeAddres 3 = list saham lembaga
        final _form = _key.currentState;
        if (flag == 0) {
            if (_form.validate()) {
                print(typeAddress);
                if(typeAddress == 1){
                    print("list alamat company new");
                    if(jenisAlamatSelected.KODE == "03"){
                        this._isSameWithIdentity = true;
                        isShowDialog(context, flag, index, typeAddress);
                    } else {
                        Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                            listen: true)
                            .addGuarantorAddress(AddressGuarantorModelCompany(
                            this._jenisAlamatSelected,
                            this._kelurahanSelected,
                            this._controllerAlamat.text,
                            this._controllerRT.text,
                            this._controllerRW.text,
                            this._controllerTelephone1Area.text,
                            this._controllerTelephone1.text,
                            this._controllerTelephone2Area.text,
                            this._controllerTelephone2.text,
                            this._controllerFaxArea.text,
                            this._controllerFax.text,
                            this._isSameWithIdentity,
                            this._addressFromMap,
                            this._isCorrespondence));
                        Navigator.pop(context);
                    }
                }
                else if(typeAddress == 2){
                    print("list alamat company new2");
                    if(jenisAlamatSelected.KODE == "03"){
                        this._isSameWithIdentity = true;
                        isShowDialog(context, flag, index, typeAddress);
                    } else {
                        Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                            listen: true)
                            .addAlamatKorespondensi(AddressGuarantorModelCompany(
                            this._jenisAlamatSelected,
                            this._kelurahanSelected,
                            this._controllerAlamat.text,
                            this._controllerRT.text,
                            this._controllerRW.text,
                            this._controllerTelephone1Area.text,
                            this._controllerTelephone1.text,
                            this._controllerTelephone2Area.text,
                            this._controllerTelephone2.text,
                            this._controllerFaxArea.text,
                            this._controllerFax.text,
                            this._isSameWithIdentity,
                            this._addressFromMap,
                            this._isCorrespondence));
                        Navigator.pop(context);
                    }
                }
                else if(typeAddress == 3){
                    print("list alamat company new2");
                    if(jenisAlamatSelected.KODE == "03"){
                        this._isSameWithIdentity = true;
                        isShowDialog(context, flag, index, typeAddress);
                    } else {
                        Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context,
                            listen: true)
                            .addPemegangSahamKelembagaanAddress(AddressGuarantorModelCompany(
                            this._jenisAlamatSelected,
                            this._kelurahanSelected,
                            this._controllerAlamat.text,
                            this._controllerRT.text,
                            this._controllerRW.text,
                            this._controllerTelephone1Area.text,
                            this._controllerTelephone1.text,
                            this._controllerTelephone2Area.text,
                            this._controllerTelephone2.text,
                            this._controllerFaxArea.text,
                            this._controllerFax.text,
                            this._isSameWithIdentity,
                            this._addressFromMap,
                            this._isCorrespondence));
                        Navigator.pop(context);
                    }
                }
            } else {
                autoValidate = true;
            }
        } else {
            if (_form.validate()) {
                if(typeAddress == 1){
                    print("list alamat guarantor company edit");
                    Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                        listen: true)
                        .updateGuarantorAddress(
                        AddressGuarantorModelCompany(
                            this._jenisAlamatSelected,
                            this._kelurahanSelected,
                            this._controllerAlamat.text,
                            this._controllerRT.text,
                            this._controllerRW.text,
                            this._controllerTelephone1Area.text,
                            this._controllerTelephone1.text,
                            this._controllerTelephone2Area.text,
                            this._controllerTelephone2.text,
                            this._controllerFaxArea.text,
                            this._controllerFax.text,
                            this._isSameWithIdentity,
                            this._addressFromMap,
                            this._isCorrespondence),
                        index);
                }
                else if (typeAddress == 2){
                    print("list alamat company edit");
                    Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                        listen: true)
                        .updateAlamatKorespondensi(
                        AddressGuarantorModelCompany(
                            this._jenisAlamatSelected,
                            this._kelurahanSelected,
                            this._controllerAlamat.text,
                            this._controllerRT.text,
                            this._controllerRW.text,
                            this._controllerTelephone1Area.text,
                            this._controllerTelephone1.text,
                            this._controllerTelephone2Area.text,
                            this._controllerTelephone2.text,
                            this._controllerFaxArea.text,
                            this._controllerFax.text,
                            this._isSameWithIdentity,
                            this._addressFromMap,
                            this._isCorrespondence),
                        index);
                }
                else if (typeAddress == 3){
                    print("list alamat company edit");
                    Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context,
                        listen: true)
                        .updatePemegangSahamKelembagaanAddress(
                        AddressGuarantorModelCompany(
                            this._jenisAlamatSelected,
                            this._kelurahanSelected,
                            this._controllerAlamat.text,
                            this._controllerRT.text,
                            this._controllerRW.text,
                            this._controllerTelephone1Area.text,
                            this._controllerTelephone1.text,
                            this._controllerTelephone2Area.text,
                            this._controllerTelephone2.text,
                            this._controllerFaxArea.text,
                            this._controllerFax.text,
                            this._isSameWithIdentity,
                            this._addressFromMap,
                            this._isCorrespondence),
                        index);
                }
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        }
    }

//    void setValueIsSameWithIdentity(bool value, BuildContext context, AddressGuarantorModelCompany model) {
//        var _provider = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
//            listen: false);
//        AddressGuarantorModelCompany data;
//        if (_addressModelTemp == null) {
//            if (value) {
//                for (int i = 0; i < _provider.listGuarantorAddress.length; i++) {
//                    if (_provider.listGuarantorAddress[i].jenisAlamatModel.KODE == "01") {
//                        data = _provider.listGuarantorAddress[i];
//                    }
//                }
//                if (model != null) {
//                    for (int i = 0; i < _listJenisAlamat.length; i++) {
//                        if (model.jenisAlamatModel.KODE == _listJenisAlamat[i].KODE) {
//                            this._jenisAlamatSelected = _listJenisAlamat[i];
//                            this._jenisAlamatSelectedTemp = this._jenisAlamatSelected;
//                            this._isSameWithIdentity = value;
//                        }
//                    }
//                }
//                this._controllerAlamat.text = data.address;
//                this._controllerRT.text = data.rt;
//                this._controllerRW.text = data.rw;
//                this._kelurahanSelected = data.kelurahanModel;
//                this._controllerAlamat.text = data.address;
//                this._alamatTemp = this._controllerAlamat.text;
//                this._controllerRT.text = data.rt;
//                this._rtTemp = this._controllerRT.text;
//                this._controllerRW.text = data.rw;
//                this._rwTemp = this._controllerRW.text;
//                this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
//                this._kelurahanTemp = this._controllerKelurahan.text;
//                this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
//                this._kecamatanTemp = this._controllerKecamatan.text;
//                this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
//                this._kotaTemp = this._controllerKota.text;
//                this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
//                this._provinsiTemp = this._controllerProvinsi.text;
//
//                this._controllerTelephone1Area.text = data.phoneArea1;
//                this._phone1AreaTemp = this._controllerTelephone1Area.text;
//
//                this._controllerTelephone1.text = data.phone1;
//                this._phone1Temp = this._controllerTelephone1.text;
//
//                this._controllerTelephone2Area.text = data.phoneArea2;
//                this._phone2AreaTemp = this._controllerTelephone2Area.text;
//
//                this._controllerTelephone2.text = data.phone2;
//                this._phone2Temp = this._controllerTelephone2.text;
//
//                this._controllerFaxArea.text = data.faxArea;
//                this._faxAreaTemp = this._controllerFaxArea.text;
//
//                this._controllerFax.text = data.fax;
//                this._faxTemp = this._controllerFax.text;
//
//                this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
//                this._postalCodeTemp = this._controllerPostalCode.text;
//                enableTfAddress = !value;
//                enableTfRT = !value;
//                enableTfRW = !value;
//                enableTfKelurahan = !value;
//                enableTfKecamatan = !value;
//                enableTfKota = !value;
//                enableTfProv = !value;
//                enableTfPostalCode = !value;
//                enableTfTelephone1Area = !value;
//                enableTfPhone1 = !value;
//                enableTfTelephone2Area = !value;
//                enableTfPhone2 = !value;
//                enableTfFaxArea = !value;
//                enableTfFax = !value;
//            } else {
//                this._controllerAlamat.clear();
//                this._controllerRT.clear();
//                this._controllerRW.clear();
//                this._kelurahanSelected = null;
//                this._controllerAlamat.clear();
////        this._alamatTemp = "";
//                this._controllerRT.clear();
////        this._rtTemp = "";
//                this._controllerRW.clear();
////        this._rwTemp = "";
//                this._controllerKelurahan.clear();
////        this._kelurahanTemp = "";
//                this._controllerKecamatan.clear();
////        this._kecamatanTemp = "";
//                this._controllerKota.clear();
////        this._kotaTemp = "";
//                this._controllerProvinsi.clear();
////        this._provinsiTemp = "";
//                this._controllerTelephone1Area.clear();
////        this._areaCodeTemp = "";
//                this._controllerTelephone1.clear();
////        this._phoneTemp = "";
//                this._controllerPostalCode.clear();
////        this._postalCodeTemp = "";
//                this._controllerTelephone1Area.clear();
//                this._controllerTelephone1.clear();
//
//                this._controllerTelephone2Area.clear();
//                this._controllerTelephone2.clear();
//
//                this._controllerFaxArea.clear();
//                this._controllerFax.clear();
//
//                enableTfAddress = !value;
//                enableTfRT = !value;
//                enableTfRW = !value;
//                enableTfKelurahan = !value;
//                enableTfKecamatan = !value;
//                enableTfKota = !value;
//                enableTfProv = !value;
//                enableTfPostalCode = !value;
//                enableTfTelephone1Area = !value;
//                enableTfPhone1 = !value;
//                enableTfTelephone2Area = !value;
//                enableTfPhone2 = !value;
//                enableTfFaxArea = !value;
//                enableTfFax = !value;
//            }
//        } else {
//            if (value) {
//                for (int i = 0; i < _provider.listGuarantorAddress.length; i++) {
//                    if (_provider.listGuarantorAddress[i].jenisAlamatModel.KODE == "01") {
//                        data = _provider.listGuarantorAddress[i];
//                    }
//                }
//                this._controllerAlamat.text = data.address;
//                this._controllerRT.text = data.rt;
//                this._controllerRW.text = data.rw;
//                this._kelurahanSelected = data.kelurahanModel;
//                this._controllerAlamat.text = data.address;
////      this._alamatTemp = this._controllerAlamat.text;
//                this._controllerRT.text = data.rt;
////      this._rtTemp = this._controllerRT.text;
//                this._controllerRW.text = data.rw;
////      this._rwTemp = this._controllerRW.text;
//                this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
////      this._kelurahanTemp = this._controllerKelurahan.text;
//                this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
////      this._kecamatanTemp = this._controllerKecamatan.text;
//                this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
////      this._kotaTemp = this._controllerKota.text;
//                this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
////      this._provinsiTemp = this._controllerProvinsi.text;
//                this._controllerTelephone1Area.text = data.phoneArea1;
////      this._areaCodeTemp = this._controllerKodeArea.text;
//                this._controllerTelephone1.text = data.phone1;
////      this._phoneTemp = this._controllerTlpn.text;
//
//                this._controllerTelephone2Area.text = data.phoneArea2;
//                this._controllerTelephone2.text = data.phone2;
//
//                this._controllerFaxArea.text = data.faxArea;
//                this._controllerFax.text = data.fax;
//
//                this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
////      this._postalCodeTemp = this._controllerPostalCode.text;
//                enableTfAddress = !value;
//                enableTfRT = !value;
//                enableTfRW = !value;
//                enableTfKelurahan = !value;
//                enableTfKecamatan = !value;
//                enableTfKota = !value;
//                enableTfProv = !value;
//                enableTfPostalCode = !value;
//                enableTfTelephone1Area = !value;
//                enableTfPhone1 = !value;
//                enableTfTelephone2Area = !value;
//                enableTfPhone2 = !value;
//                enableTfFaxArea = !value;
//                enableTfFax = !value;
//            } else {
//                this._controllerAlamat.clear();
//                this._controllerRT.clear();
//                this._controllerRW.clear();
//                this._kelurahanSelected = null;
//                this._controllerAlamat.clear();
////        this._alamatTemp = "";
//                this._controllerRT.clear();
////        this._rtTemp = "";
//                this._controllerRW.clear();
////        this._rwTemp = "";
//                this._controllerKelurahan.clear();
////        this._kelurahanTemp = "";
//                this._controllerKecamatan.clear();
////        this._kecamatanTemp = "";
//                this._controllerKota.clear();
////        this._kotaTemp = "";
//                this._controllerProvinsi.clear();
////        this._provinsiTemp = "";
//                this._controllerTelephone1Area.clear();
////        this._areaCodeTemp = "";
//                this._controllerTelephone1.clear();
////        this._phoneTemp = "";
//                this._controllerPostalCode.clear();
////        this._postalCodeTemp = "";
//                this._controllerTelephone1Area.clear();
//                this._controllerTelephone1.clear();
//
//                this._controllerTelephone2Area.clear();
//                this._controllerTelephone2.clear();
//
//                this._controllerFaxArea.clear();
//                this._controllerFax.clear();
//
//                enableTfAddress = !value;
//                enableTfRT = !value;
//                enableTfRW = !value;
//                enableTfKelurahan = !value;
//                enableTfKecamatan = !value;
//                enableTfKota = !value;
//                enableTfProv = !value;
//                enableTfPostalCode = !value;
//                enableTfTelephone1Area = !value;
//                enableTfPhone1 = !value;
//                enableTfTelephone2Area = !value;
//                enableTfPhone2 = !value;
//                enableTfFaxArea = !value;
//                enableTfFax = !value;
//            }
//        }
//    }

    setValueForEdit(AddressGuarantorModelCompany data, BuildContext context){
//        addDataListAddressType(context, index);
//        if (isSameWithIdentity) {
//            setValueIsSameWithIdentity(isSameWithIdentity, context, data);
//        } else {
//            _addressModelTemp = data;
            for (int i = 0; i < this._listJenisAlamat.length; i++) {
                if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
                    this._jenisAlamatSelected = this._listJenisAlamat[i];
                    this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
                }
            }
            this._kelurahanSelected = data.kelurahanModel;
            this._controllerAlamat.text = data.address;
            this._alamatTemp = this._controllerAlamat.text;
            this._controllerRT.text = data.rt;
            this._rtTemp = this._controllerRT.text;
            this._controllerRW.text = data.rw;
            this._rwTemp = this._controllerRW.text;
            this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
            this._kelurahanTemp = this._controllerKelurahan.text;
            this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
            this._kecamatanTemp = this._controllerKecamatan.text;
            this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
            this._kotaTemp = this._controllerKota.text;
            this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
            this._provinsiTemp = this._controllerProvinsi.text;
            this._controllerTelephone1Area.text = data.phoneArea1;
            this._phone1AreaTemp = this._controllerTelephone1Area.text;
            this._controllerTelephone1.text = data.phone1;
            this._phone1Temp = this._controllerTelephone1.text;
            this._controllerTelephone2Area.text = data.phoneArea2;
            this._phone2AreaTemp = this._controllerTelephone2Area.text;
            this._controllerTelephone2.text = data.phone2;
            this._phone2Temp = this._controllerTelephone2.text;
            this._controllerFaxArea.text = data.faxArea;
            this._faxAreaTemp = this._controllerFaxArea.text;
            this._controllerFax.text = data.fax;
            this._faxTemp = this._controllerFax.text;
            this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
            this._postalCodeTemp = this._controllerPostalCode.text;
//        }
    }

    void checkValidCodeArea1(String value) {
        if (value == "08") {
            Future.delayed(Duration(milliseconds: 100), () {
                this._controllerTelephone1Area.clear();
            });
        } else {
            return;
        }
    }

    void checkValidCodeArea2(String value) {
        if (value == "08") {
            Future.delayed(Duration(milliseconds: 100), () {
                this._controllerTelephone2Area.clear();
            });
        } else {
            return;
        }
    }

    void checkValidCodeAreaFax(String value) {
        if (value == "08") {
            Future.delayed(Duration(milliseconds: 100), () {
                this._controllerFaxArea.clear();
            });
        } else {
            return;
        }
    }

    void searchKelurahan(BuildContext context) async {
        KelurahanModel data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchKelurahanChangeNotif(),
                    child: SearchKelurahan())));
        if (data != null) {
            kelurahanSelected = data;
            this._controllerKelurahan.text = data.KEL_NAME;
            this._controllerKecamatan.text = data.KEC_NAME;
            this._controllerKota.text = data.KABKOT_NAME;
            this._controllerProvinsi.text = data.PROV_NAME;
            this._controllerPostalCode.text = data.ZIPCODE;
            notifyListeners();
        } else {
            return;
        }
    }

    void setLocationAddressByMap(BuildContext context) async{
        Map _result = await Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage()));
        if(_result != null){
            _addressFromMap = _result;
            this._controllerAddressFromMap.text = _result["address"];
            notifyListeners();
        }
    }

    get phone2Temp => _phone2Temp;

    get faxAreaTemp => _faxAreaTemp;

    get faxTemp => _faxTemp;

    get postalCodeTemp => _postalCodeTemp;

    get phone1AreaTemp => _phone1AreaTemp;

    get provinsiTemp => _provinsiTemp;

    get kotaTemp => _kotaTemp;

    get kecamatanTemp => _kecamatanTemp;

    get kelurahanTemp => _kelurahanTemp;

    get rwTemp => _rwTemp;

    get rtTemp => _rtTemp;

    get phone1Temp => _phone1Temp;

    get phone2AreaTemp => _phone2AreaTemp;

    String get alamatTemp => _alamatTemp;

    JenisAlamatModel get jenisAlamatSelectedTemp => _jenisAlamatSelectedTemp;

    KelurahanModel get kelurahanSelectedTemp => _kelurahanSelectedTemp;

    GlobalKey<FormState> get key => _key;
}
