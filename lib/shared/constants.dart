import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/recources_survey_info_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/models/type_offer_model.dart';
import 'package:ad1ms2_dev/screens/SA/sa_job_mayor.dart';
import 'package:ad1ms2_dev/screens/form_AOS/form_AOS.dart';
import 'package:ad1ms2_dev/screens/form_IA/form_IA.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_parent.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:shared_preferences/shared_preferences.dart';

const textInputDecoration = InputDecoration(
//  fillColor: Colors.white,
//  filled: true,
//  enabledBorder: OutlineInputBorder(
//    borderSide: BorderSide(color: Colors.white, width: 2.0),
//  ),
//  focusedBorder: OutlineInputBorder(
//    borderSide: BorderSide(color: Colors.blue, width: 2.0),
//  ),
    );

class TitlePopUpMenuButton {
  static const String Delete = "Delete";
  static const String Detail = "Detail";

  static const List<String> choices = <String>[Detail, Delete];
}

class LabelPopUpMenuButtonSaveToDraftBackToHome{
  static const String SAVE_TO_DRAFT = "Save to draft";
  static const String BACK_TO_HOME = "Back to home";

  static const List<String> choices = [SAVE_TO_DRAFT,BACK_TO_HOME];
}

class PilihOIDMenuButton {
  static const String PilihOID = "Pilih OID";
  static const List<String> choices = <String>[PilihOID];
}

DateFormat dateFormat = DateFormat("dd-MM-yyyy");
DateFormat dateFormat2 = DateFormat("dd-MMM-yyyy");
DateTime dateNow = DateTime.now();

class RelationshipStatusList {
  List<RelationshipStatusModel> relationshipStatusItems = [
    RelationshipStatusModel("01", "SUAMI"),
    RelationshipStatusModel("02", "ISTRI"),
    RelationshipStatusModel("03", "ANAK KANDUNG"),
    RelationshipStatusModel("04", "AYAH KANDUNG"),
    RelationshipStatusModel("05", "IBU KANDUNG"),
    RelationshipStatusModel("89", "KAKAK KANDUNG"),
    RelationshipStatusModel("98", "LAINNYA"),
    RelationshipStatusModel("N8", "ADIK KANDUNG"),
  ];
}

class RelationshipStatusModel {
  final String PARA_FAMILY_TYPE_ID;
  final String PARA_FAMILY_TYPE_NAME;

  RelationshipStatusModel(this.PARA_FAMILY_TYPE_ID, this.PARA_FAMILY_TYPE_NAME);
}

class TypeInstitutionList {
  List<TypeInstitutionModel> typeInstitutionItems = [
    TypeInstitutionModel("001", "PRIVATE"),
    TypeInstitutionModel("002", "PMA"),
    TypeInstitutionModel("003", "PMDN"),
    TypeInstitutionModel("004", "PMDA"),
    TypeInstitutionModel("005", "GOVERNMENT"),
    TypeInstitutionModel("006", "BUMN"),
    TypeInstitutionModel("007", "KOPERASI"),
    TypeInstitutionModel("008", "YAYASAN"),
    TypeInstitutionModel("009", "PT"),
    TypeInstitutionModel("010", "CV"),
    TypeInstitutionModel("011", "PT, TBK"),
    TypeInstitutionModel("013", "BUMD"),
  ];
}

class TypeInstitutionModel {
  final String PARA_ID;
  final String PARA_NAME;

  TypeInstitutionModel(this.PARA_ID, this.PARA_NAME);
}

class TypeOfFinancingList {
  List<FinancingTypeModel> financingTypeList = [
    FinancingTypeModel("1", "KONVENSIONAL"),
    FinancingTypeModel("2", "SYARIAH"),
  ];
}

class ResourceInfoSurveyList {
  List<ResourcesInfoSurveyModel> resourcesInfoSurveyList = [
    ResourcesInfoSurveyModel("001", "TETANGGA"),
    ResourcesInfoSurveyModel("002", "ATASAN KERJA"),
    ResourcesInfoSurveyModel("003", "HRD"),
    ResourcesInfoSurveyModel("004", "REKAN KERJA"),
    ResourcesInfoSurveyModel("005", "LAINNYA"),
  ];
}

class ElectricityType{
  List<ElectricityTypeModel> electricityTypeList = [
    ElectricityTypeModel("01", "450 WATT"),
    ElectricityTypeModel("02", "900 WATT"),
    ElectricityTypeModel("03", "1300 WATT"),
    ElectricityTypeModel("04", "2200 WATT"),
    ElectricityTypeModel("05", "3300 WATT"),
    ElectricityTypeModel("06", "4400 WATT"),
    ElectricityTypeModel("07", ">4400 WATT"),
  ];
}

class IdentityType{
  List<IdentityModel> lisIdentityModel = [
    IdentityModel("01", "KTP"),
    IdentityModel("03", "PASSPORT"),
    IdentityModel("04", "SIM"),
    IdentityModel("05", "KTP Sementara"),
    IdentityModel("06", "Resi KTP"),
    IdentityModel("07", "Ket. Domisili"),
  ];
}

class ListTypeOffer{
  List<TypeOfferModel> listTypeOffer = [
    TypeOfferModel("01", "VOUCHER"),
    TypeOfferModel("02", "NO VOUCHER"),
    TypeOfferModel("03", "CL")
  ];
}

class BaseUrl{
  //Exist dipakai
  static String urlPublic = "https://103.110.89.34/dev/public/ms2/";
  static String urlAcction = "http://10.81.3.21:9080/";
  static String urlLme = "http://10.161.16.207:31814/lmeServiceDEV/";
  static String urlDedup = "http://10.81.3.137:99/DedupAPI/api/";

  //
  // static String urlDedup ="http://10.81.3.137:99/";
  static String urlAddress = "http://10.61.27.14:9016/";
  static String urlOccupation = "http://10.61.27.14:9016/";
  static String unit = "http://10.61.27.14:9093/";
}


String formatDateDedup(DateTime initialDate){
  DateFormat _formatDateDedup = DateFormat("dd-MMM-yyyy");
  return _formatDateDedup.format(initialDate);
}

String formatDateListOID(DateTime initialDate){
  DateFormat _formatDateDedup = DateFormat("dd-MM-yyyy");
  return _formatDateDedup.format(initialDate);
}

Future<DateTime> selectDate(BuildContext context, DateTime initialDate) async{
  var datePicked = await DatePicker.showSimpleDatePicker(
    context,
    initialDate: initialDate,
    firstDate: DateTime(DateTime.now().year-100),
    lastDate: DateTime(DateTime.now().year+100),
    dateFormat: "dd-MMMM-yyyy",
    locale: DateTimePickerLocale.en_us,
    looping: true,
  );
  return datePicked;
}

void navigateForm(String typeFormId, BuildContext context,String orderNo,String orderDate,String custName, String custType) async{
  SharedPreferences _preferences = await SharedPreferences.getInstance();
  _preferences.setString("order_no", orderNo);
  _preferences.setString("order_date",orderDate);
  _preferences.setString("cust_name",custName);
  _preferences.setString("last_known_state",typeFormId);
  _preferences.setString("cust_type",custType);
  if(typeFormId == "SA"){
    Navigator.push(context, MaterialPageRoute(builder: (context) => SAJobMayor()));
  }
  else if(typeFormId == "AOS"){
    Navigator.push(context, MaterialPageRoute(builder: (context) => FormAOS()));
  }
  else if(typeFormId == "IA"){
    Navigator.push(context, MaterialPageRoute(builder: (context) => FormIA()));
  }
  else{
    Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,)));
  }
}

// class SetSSL{
//   var sccontext = SecurityContext.defaultContext;
//   void setSSLFromAsset() async{
//     // String data = await rootBundle.loadString("assets/ad1ms2newdev.pem");
//     try{
//       String data = await rootBundle.loadString("assets/ad1ms2newdev.pem");
//       print(data);
//
// //it can be "cert.crt" as well.
//       List bytes = utf8.encode(data);
//       sccontext.setTrustedCertificatesBytes(bytes);
//     }
//     catch(e){
//       print(e.toString());
//     }
//   }
// }
