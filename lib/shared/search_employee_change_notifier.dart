import 'dart:collection';

import 'package:ad1ms2_dev/models/employee_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class SearchEmployeeChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();

  List<EmployeeModel> _listEmployeeModel = [
    EmployeeModel("100083908", "MUHAMMAD BAYU SAPUTRA"),
    EmployeeModel("100083909", "AGUS SETIAWAN"),
    EmployeeModel("100083910", "SUGENG BUDIMAN")
  ];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<EmployeeModel> get listEmployeeModel {
    return UnmodifiableListView(this._listEmployeeModel);
  }
}
