import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_com_model.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants.dart';

class FormMCompanyPemegangSahamKelembagaanChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  TypeInstitutionModel _typeInstitutionSelected;
  TextEditingController _controllerInstitutionName = TextEditingController();
  TextEditingController _controllerDateOfEstablishment = TextEditingController();
  DateTime _initialDateForDateOfEstablishment = DateTime(dateNow.year, dateNow.month, dateNow.day);
  TextEditingController _controllerNPWP = TextEditingController();
  TextEditingController _controllerPercentShare = TextEditingController();
  DbHelper _dbHelper = DbHelper();

  bool get autoValidate => _autoValidate;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<TypeInstitutionModel> _listTypeInstitution = TypeInstitutionList().typeInstitutionItems;

  // Jenis Lembaga
  UnmodifiableListView<TypeInstitutionModel> get listTypeInstitution {
    return UnmodifiableListView(this._listTypeInstitution);
  }

  TypeInstitutionModel get typeInstitutionSelected => _typeInstitutionSelected;

  set typeInstitutionSelected(TypeInstitutionModel value) {
    this._typeInstitutionSelected = value;
    notifyListeners();
  }

  // Nama Lembaga
  TextEditingController get controllerInstitutionName {
    return this._controllerInstitutionName;
  }

  set controllerInstitutionName(value) {
    this._controllerInstitutionName = value;
    this.notifyListeners();
  }

  // Tanggal Pendirian
  TextEditingController get controllerDateOfEstablishment => _controllerDateOfEstablishment;

  DateTime get initialDateForBirthOfDate => _initialDateForDateOfEstablishment;

  void selectDateOfEstablishment(BuildContext context) async {
    DatePickerShared _datePickerShared = DatePickerShared();
    var _datePickerSelected = await _datePickerShared.selectStartDate(
        context, this._initialDateForDateOfEstablishment,
        canAccessNextDay: false);
    if (_datePickerSelected != null) {
      this._controllerDateOfEstablishment.text = dateFormat.format(_datePickerSelected);
      this._initialDateForDateOfEstablishment = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  // NPWP
  TextEditingController get controllerNPWP {
    return this._controllerNPWP;
  }

  set controllerNPWP(value) {
    this._controllerNPWP = value;
    this.notifyListeners();
  }

  // % Share
  TextEditingController get controllerPercentShare {
    return this._controllerPercentShare;
  }

  set controllerPercentShare(value) {
    this._controllerPercentShare = value;
    this.notifyListeners();
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  // Future<void> clearDataPemegangSahamKelembagaan() async {
  void clearDataPemegangSahamKelembagaan() {
    this._autoValidate = false;
    this._typeInstitutionSelected = null;
    this._controllerInstitutionName.clear();
    this._controllerDateOfEstablishment.clear();
    this._controllerNPWP.clear();
    this._controllerPercentShare.clear();

  }

  void saveToSQLite(BuildContext context) async{
    var _provider = Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false);
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _dbHelper.insertMS2CustShrhldrCom(
        MS2CustShrhldrComModel(
            "22", this._typeInstitutionSelected, null, null, this._controllerInstitutionName.text,
            null, null, null, null, null, null, this._controllerDateOfEstablishment.text, 0,
            null, null, null, null, this._controllerNPWP.text, null, null,
            this._controllerNPWP.text != "" ? 1 : 0, _provider.signPKPSelected, null,
            null, null, null, null, null, null, null, null, null, null, null,
            int.parse(this._controllerPercentShare.text), null, DateTime.now().toString(),
            _preferences.getString("username"), null, null, 1)
    );
  }

  void deleteSQLite(){
    _dbHelper.deleteMS2CustShrhldrCom();
  }
}
