import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/screens/map_page.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/address_type_resource.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

import '../main.dart';
import 'form_m_add_guarantor_individual_change_notifier.dart';
import 'form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'form_m_occupation_change_notif.dart';

class FormMAddAddressIndividuChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerAddressFromMap = TextEditingController();
  Map _addressFromMap;
  bool _isSameWithIdentity = false;
  JenisAlamatModel _jenisAlamatSelected;
  JenisAlamatModel _jenisAlamatSelectedTemp;
  KelurahanModel _kelurahanSelected;
  KelurahanModel _kelurahanSelectedTemp;
  String _alamatTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provinsiTemp,
      _areaCodeTemp,
      _phoneTemp,
      _postalCodeTemp;
  bool _enableTfAddress = true;
  bool _enableTfRT = true;
  bool _enableTfRW = true;
  bool _enableTfKelurahan = true;
  bool _enableTfKecamatan = true;
  bool _enableTfKota = true;
  bool _enableTfProv = true;
  bool _enableTfPostalCode = true;
  bool _enableTfAreaCode = true;
  bool _enableTfPhone = true;
  bool _isCorrespondence;
  DbHelper _dbHelper = DbHelper();

  GlobalKey<FormState> _key = GlobalKey<FormState>();

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<JenisAlamatModel> _listJenisAlamat = [];

  // Jenis Alamat
  UnmodifiableListView<JenisAlamatModel> get listJenisAlamat {
    return UnmodifiableListView(this._listJenisAlamat);
  }

  JenisAlamatModel get jenisAlamatSelected => _jenisAlamatSelected;

  set jenisAlamatSelected(JenisAlamatModel value) {
    this._jenisAlamatSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerProvinsi => _controllerProvinsi;
  TextEditingController get controllerKota => _controllerKota;
  TextEditingController get controllerKecamatan => _controllerKecamatan;
  TextEditingController get controllerKelurahan => _controllerKelurahan;
  TextEditingController get controllerRW => _controllerRW;
  TextEditingController get controllerRT => _controllerRT;
  TextEditingController get controllerAlamat => _controllerAlamat;
  TextEditingController get controllerKodeArea => _controllerKodeArea;
  TextEditingController get controllerTlpn => _controllerTlpn;
  TextEditingController get controllerPostalCode => _controllerPostalCode;
  TextEditingController get controllerAddressFromMap => _controllerAddressFromMap;

  // Kelurahan
  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanSelected = value;
    notifyListeners();
  }

  // Sama Dengan Identitas
  bool get isSameWithIdentity => _isSameWithIdentity;

  set isSameWithIdentity(bool value) {
    this._isSameWithIdentity = value;
    notifyListeners();
  }

  JenisAlamatModel get jenisAlamatSelectedTemp => _jenisAlamatSelectedTemp;

  KelurahanModel get kelurahanSelectedTemp => _kelurahanSelectedTemp;

  Future<void> addDataListJenisAlamat(AddressModel data, BuildContext context, int flag,
      bool isSameWithIdentity,int index, int typeAddress) async{
    var _provider = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    var _providerOccupation = Provider.of<FormMOccupationChangeNotif>(context, listen: false);
    var _providerPenjaminPribadi = Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false);
    var _providerManajemenPIC = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
    var _providerSahamPribadi = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false);
    try {
      this._listJenisAlamat.clear();
//      final ioc = new HttpClient();
//      ioc.badCertificateCallback =
//          (X509Certificate cert, String host, int port) => true;
//
//      final _http = IOClient(ioc);
//      final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/address/get-address-type");
//      final _data = jsonDecode(_response.body);
      if (flag == 0) {
        print("create baru");
        // add
        if(typeAddress == 1){
          if (_provider.listAlamatKorespondensi.isEmpty) {
            this._listJenisAlamat = await getAddressType(1);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
            // remove used type
            for(int x=0; x<_provider.listAlamatKorespondensi.length; x++){
              for(int i=0; i < _listJenisAlamat.length;i++){
                if(_listJenisAlamat[i].KODE == _provider.listAlamatKorespondensi[x].jenisAlamatModel.KODE){
                  _listJenisAlamat.removeAt(i);
                }
              }
            }
          }
        }
        else if(typeAddress == 2){
          if (_providerOccupation.listOccupationAddress.isEmpty) {
            this._listJenisAlamat = await getAddressType(1);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
            // remove used type
            for(int x=0; x<_providerOccupation.listOccupationAddress.length; x++){
              for(int i=0; i < _listJenisAlamat.length;i++){
                if(_listJenisAlamat[i].KODE == _providerOccupation.listOccupationAddress[x].jenisAlamatModel.KODE){
                  _listJenisAlamat.removeAt(i);
                }
              }
            }
          }
        }
        else if(typeAddress == 3){
          if (_providerPenjaminPribadi.listGuarantorAddress.isEmpty) {
            this._listJenisAlamat = await getAddressType(1);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
            // remove used type
            for(int x=0; x<_providerPenjaminPribadi.listGuarantorAddress.length; x++){
              for(int i=0; i < _listJenisAlamat.length;i++){
                if(_listJenisAlamat[i].KODE == _providerPenjaminPribadi.listGuarantorAddress[x].jenisAlamatModel.KODE){
                  _listJenisAlamat.removeAt(i);
                }
              }
            }
          }
        }
        else if(typeAddress == 4){
          if (_providerManajemenPIC.listManajemenPICAddress.isEmpty) {
            this._listJenisAlamat = await getAddressType(1);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
            // remove used type
            for(int x=0; x<_providerManajemenPIC.listManajemenPICAddress.length; x++){
              for(int i=0; i < _listJenisAlamat.length;i++){
                if(_listJenisAlamat[i].KODE == _providerManajemenPIC.listManajemenPICAddress[x].jenisAlamatModel.KODE){
                  _listJenisAlamat.removeAt(i);
                }
              }
            }
          }
        }
        else if(typeAddress == 5){
          if (_providerSahamPribadi.listPemegangSahamPribadiAddress.isEmpty) {
            this._listJenisAlamat = await getAddressType(1);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
            // remove used type
            for(int x=0; x<_providerSahamPribadi.listPemegangSahamPribadiAddress.length; x++){
              for(int i=0; i < _listJenisAlamat.length;i++){
                if(_listJenisAlamat[i].KODE == _providerSahamPribadi.listPemegangSahamPribadiAddress[x].jenisAlamatModel.KODE){
                  _listJenisAlamat.removeAt(i);
                }
              }
            }
          }
        }
      }
      else {
        // edit
        if(typeAddress == 1){
          print("edit 1");
          if (_provider.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '03') {
            this._listJenisAlamat = await getAddressType(1);
          } else if (_provider.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '01'){
            this._listJenisAlamat = await getAddressType(2);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
          }
          // remove used type
          for(int x=0; x<_provider.listAlamatKorespondensi.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _provider.listAlamatKorespondensi[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if (typeAddress == 2){
          print("edit 2");
          if (_providerOccupation.listOccupationAddress[index].jenisAlamatModel.KODE == '03') {
            this._listJenisAlamat = await getAddressType(1);
          } else if (_providerOccupation.listOccupationAddress[index].jenisAlamatModel.KODE == '01'){
            this._listJenisAlamat = await getAddressType(2);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
          }
          // remove used type
          for(int x=0; x<_providerOccupation.listOccupationAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerOccupation.listOccupationAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if (typeAddress == 3){
          print("edit 3");
          if (_providerPenjaminPribadi.listGuarantorAddress[index].jenisAlamatModel.KODE == '03') {
            this._listJenisAlamat = await getAddressType(1);
          } else if (_providerPenjaminPribadi.listGuarantorAddress[index].jenisAlamatModel.KODE == '01'){
            this._listJenisAlamat = await getAddressType(2);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
          }
          // remove used type
          for(int x=0; x<_providerPenjaminPribadi.listGuarantorAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerPenjaminPribadi.listGuarantorAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if (typeAddress == 4){
          print("edit 4");
          if (_providerManajemenPIC.listManajemenPICAddress[index].jenisAlamatModel.KODE == '03') {
            this._listJenisAlamat = await getAddressType(1);
          } else if (_providerManajemenPIC.listManajemenPICAddress[index].jenisAlamatModel.KODE == '01'){
            this._listJenisAlamat = await getAddressType(2);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
          }
          // remove used type
          for(int x=0; x<_providerManajemenPIC.listManajemenPICAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerManajemenPIC.listManajemenPICAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if (typeAddress == 5){
          print("edit 4");
          if (_providerSahamPribadi.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE == '03') {
            this._listJenisAlamat = await getAddressType(1);
          } else if (_providerSahamPribadi.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE == '01'){
            this._listJenisAlamat = await getAddressType(2);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
          }
          // remove used type
          for(int x=0; x<_providerSahamPribadi.listPemegangSahamPribadiAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerSahamPribadi.listPemegangSahamPribadiAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        if(flag == 1) setValueForEdit(data, context, isSameWithIdentity);
      }
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  void searchKelurahan(BuildContext context) async {
    KelurahanModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchKelurahanChangeNotif(),
                child: SearchKelurahan(
//                  flag: 0,
                    ))));
    if (data != null) {
      kelurahanSelected = data;
      this._controllerKelurahan.text = data.KEL_NAME;
      this._controllerKecamatan.text = data.KEC_NAME;
      this._controllerKota.text = data.KABKOT_NAME;
      this._controllerProvinsi.text = data.PROV_NAME;
      this._controllerPostalCode.text = data.ZIPCODE;
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<FormState> get key => _key;

  void isShowDialog(BuildContext context, int flag, int index, int typeAddress) {
      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  primaryColor: Colors.black,
                  accentColor: myPrimaryColor,
                  fontFamily: "NunitoSans",
                  primarySwatch: primaryOrange
              ),
              child: AlertDialog(
                title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Text("Apakah domisili sama dengan identitas?"),
                actions: <Widget>[
                  FlatButton(
                      onPressed: (){
                        _isSameWithIdentity = true;
                        if(typeAddress == 1){
                          Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
                              .addAlamatKorespondensi(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));

                          Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
                              .addAlamatKorespondensi(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));
                        }
                        else if(typeAddress == 2){
                          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
                              .addOccupationAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));

                          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
                              .addOccupationAddress(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));
                        }
                        else if(typeAddress == 3){
                          Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
                              .addGuarantorAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));

                          Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
                              .addGuarantorAddress(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));
                        }
                        else if(typeAddress == 4){
                          Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                              .addManajemenPICAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));

                          Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                              .addManajemenPICAddress(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));
                        }
                        else if(typeAddress == 5){
                          Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false)
                              .addPemegangSahamPribadiAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));

                          Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false)
                              .addPemegangSahamPribadiAddress(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,false
                          ));
                        }
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      child: Text("Ya",
                          style: TextStyle(
                              color: primaryOrange,
                          )
                      )
                  ),
                  FlatButton(
                      onPressed: (){
                        //bikin identitas
                        if(typeAddress == 1){
                          Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
                              .addAlamatKorespondensi(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              false,
                              this._addressFromMap,false
                          ));
                        }
                        else if(typeAddress == 2){
                          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
                              .addOccupationAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              false,
                              this._addressFromMap,false
                          ));
                        }
                        else if(typeAddress == 3){
                          Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
                              .addGuarantorAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              false,
                              this._addressFromMap,false
                          ));
                        }
                        else if(typeAddress == 4){
                          Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                              .addManajemenPICAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false
                          ));
                        }
                        else if(typeAddress == 5){
                          Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false)
                              .addPemegangSahamPribadiAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false
                          ));
                        }
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      child: Text("Tidak",
                          style: TextStyle(
                              color: Colors.grey
                          )
                      )
                  )
                ],
              ),
            );
          }
      );
  }

  void check(BuildContext context, int flag, int index, int typeAddress) {
//  typeAddres 1 = list alamat korespondensi
//  typeAddres 2 = list occupation address
//  typeAddres 3 = list penjamin pribadi
//  typeAddres 4 = list manajemen pic
//  typeAddres 5 = list saham pribadi
    final _form = _key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        if(typeAddress == 1){
          print("list alamat korespondensi new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            isShowDialog(context, flag, index, typeAddress);
          } else {
            Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
                .addAlamatKorespondensi(AddressModel(
                this._jenisAlamatSelected,
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,false
            ));
            Navigator.pop(context);
          }
        }
        else if(typeAddress == 2){
          print("list occupation address new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            isShowDialog(context, flag, index, typeAddress);
          } else {
            Provider.of<FormMOccupationChangeNotif>(context, listen: false)
                .addOccupationAddress(AddressModel(
                this._jenisAlamatSelected,
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,false
            ));
            Navigator.pop(context);
          }
        }
        else if(typeAddress == 3){
          print("list penjamin pribadi new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            isShowDialog(context, flag, index, typeAddress);
          } else {
            Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
                .addGuarantorAddress(AddressModel(
                this._jenisAlamatSelected,
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,false
            ));
            Navigator.pop(context);
          }
        }
        else if(typeAddress == 4){
          print("list manajemen pic new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            isShowDialog(context, flag, index, typeAddress);
          } else {
            Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                .addManajemenPICAddress(AddressModel(
                this._jenisAlamatSelected,
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,false
            ));
            Navigator.pop(context);
          }
        }
        else if(typeAddress == 5){
          print("list saham pribadi new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            isShowDialog(context, flag, index, typeAddress);
          } else {
            Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false)
                .addPemegangSahamPribadiAddress(AddressModel(
                this._jenisAlamatSelected,
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,false
            ));
            Navigator.pop(context);
          }
        }
      } else {
        autoValidate = true;
      }
    } else {
      if (_form.validate()) {
        if(typeAddress == 1){
          print("list alamat korespondensi edit");
          Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
              .updateAlamatKorespondensi(AddressModel(
              this._jenisAlamatSelected,
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence
          ), index);
        }
        else if(typeAddress == 2){
          print("list occupation address edit");
          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
              .updateOccupationAddress(AddressModel(
              this._jenisAlamatSelected,
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence
          ), index);
        }
        else if(typeAddress == 3){
          print("list penjamin pribadi edit");
          Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
              .updateGuarantorAddress(AddressModel(
              this._jenisAlamatSelected,
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence
          ), index);
        }
        else if(typeAddress == 4){
          print("list manajemen pic edit");
          Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
              .updateManajemenPICAddress(AddressModel(
              this._jenisAlamatSelected,
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence
          ), index);
        }
        else if(typeAddress == 5){
          print("list saham pribadi edit");
          Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false)
              .updatePemegangSahamPribadiAddress(AddressModel(
              this._jenisAlamatSelected,
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence
          ), index);
        }
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    }
  }

  void checkValidCOdeArea(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerKodeArea.clear();
      });
    } else {
      return;
    }
  }

  setValueForEdit(AddressModel data, BuildContext context,bool isSameWithIdentity){
//    if (isSameWithIdentity) {
//      setValueIsSameWithIdentity(isSameWithIdentity, context, data);
//    }
//    else {
      for (int i = 0; i < this._listJenisAlamat.length; i++) {
        if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
          this._jenisAlamatSelected = this._listJenisAlamat[i];
          this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
        }
      }
      this._kelurahanSelected = data.kelurahanModel;
      this._controllerAlamat.text = data.address;
      this._alamatTemp = this._controllerAlamat.text;
      this._controllerRT.text = data.rt;
      this._rtTemp = this._controllerRT.text;
      this._controllerRW.text = data.rw;
      this._rwTemp = this._controllerRW.text;
      this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
      this._kelurahanTemp = this._controllerKelurahan.text;
      this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
      this._kecamatanTemp = this._controllerKecamatan.text;
      this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
      this._kotaTemp = this._controllerKota.text;
      this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
      this._provinsiTemp = this._controllerProvinsi.text;
      this._controllerKodeArea.text = data.areaCode;
      this._areaCodeTemp = this._controllerKodeArea.text;
      this._controllerTlpn.text = data.phone;
      this._phoneTemp = this._controllerTlpn.text;
      this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
      this._postalCodeTemp = this._controllerPostalCode.text;
      this._isCorrespondence = data.isCorrespondence;
      this._addressFromMap = data.addressLatLong;
      this._controllerAddressFromMap.text = this._addressFromMap['address'];
//    }
  }

  get postalCodeTemp => _postalCodeTemp;

  get areaCOdeTemp => _areaCodeTemp;

  get provinsiTemp => _provinsiTemp;

  get kotaTemp => _kotaTemp;

  get kecamatanTemp => _kecamatanTemp;

  get kelurahanTemp => _kelurahanTemp;

  get rwTemp => _rwTemp;

  get rtTemp => _rtTemp;

  get phoneTemp => _phoneTemp;

  String get alamatTemp => _alamatTemp;

  void setValueIsSameWithIdentity(bool value, BuildContext context, AddressModel model) {
    var _provider =
    Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    AddressModel data;
    if (_alamatTemp == null) {
      if (value) {
        for (int i = 0; i < _provider.listAlamatKorespondensi.length; i++) {
          if (_provider.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
            data = _provider.listAlamatKorespondensi[i];
          }
        }
        if (model != null) {
          for (int i = 0; i < _listJenisAlamat.length; i++) {
            if (model.jenisAlamatModel.KODE == _listJenisAlamat[i].KODE) {
              this._jenisAlamatSelected = _listJenisAlamat[i];
              this._jenisAlamatSelectedTemp = this._jenisAlamatSelected;
              this._isSameWithIdentity = value;
            }
          }
        }
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._kelurahanSelected = data.kelurahanModel;
        this._controllerAlamat.text = data.address;
        this._alamatTemp = this._controllerAlamat.text;
        this._controllerRT.text = data.rt;
        this._rtTemp = this._controllerRT.text;
        this._controllerRW.text = data.rw;
        this._rwTemp = this._controllerRW.text;
        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
        this._kelurahanTemp = this._controllerKelurahan.text;
        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
        this._kecamatanTemp = this._controllerKecamatan.text;
        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
        this._kotaTemp = this._controllerKota.text;
        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
        this._provinsiTemp = this._controllerProvinsi.text;
        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
        this._postalCodeTemp = this._controllerPostalCode.text;

        this._controllerKodeArea.text = data.areaCode;
        this._areaCodeTemp = this._controllerKodeArea.text;
        this._controllerTlpn.text = data.phone;
        this._phoneTemp = this._controllerTlpn.text;
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      } else {
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._kelurahanSelected = null;
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._controllerKelurahan.clear();
        this._controllerKecamatan.clear();
        this._controllerKota.clear();
        this._controllerProvinsi.clear();
        this._controllerPostalCode.clear();
        this._controllerKodeArea.clear();
        this._controllerTlpn.clear();
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      }
    } else {
      if (value) {
        for (int i = 0; i < _provider.listAlamatKorespondensi.length; i++) {
          if (_provider.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
            data = _provider.listAlamatKorespondensi[i];
          }
        }
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._kelurahanSelected = data.kelurahanModel;
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
        this._controllerKodeArea.text = data.areaCode;
        this._controllerTlpn.text = data.phone;
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      } else {
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._kelurahanSelected = null;
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._controllerKelurahan.clear();
        this._controllerKecamatan.clear();
        this._controllerKota.clear();
        this._controllerProvinsi.clear();
        this._controllerPostalCode.clear();
        this._controllerKodeArea.clear();
        this._controllerTlpn.clear();
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      }
    }
  }

  bool get enableTfPhone => _enableTfPhone;

  set enableTfPhone(bool value) {
    this._enableTfPhone = value;
  }

  bool get enableTfAreaCode => _enableTfAreaCode;

  set enableTfAreaCode(bool value) {
    this._enableTfAreaCode = value;
  }

  bool get enableTfPostalCode => _enableTfPostalCode;

  set enableTfPostalCode(bool value) {
    this._enableTfPostalCode = value;
  }

  bool get enableTfProv => _enableTfProv;

  set enableTfProv(bool value) {
    this._enableTfProv = value;
  }

  bool get enableTfKota => _enableTfKota;

  set enableTfKota(bool value) {
    this._enableTfKota = value;
  }

  bool get enableTfKecamatan => _enableTfKecamatan;

  set enableTfKecamatan(bool value) {
    this._enableTfKecamatan = value;
  }

  bool get enableTfKelurahan => _enableTfKelurahan;

  set enableTfKelurahan(bool value) {
    this._enableTfKelurahan = value;
  }

  bool get enableTfRW => _enableTfRW;

  set enableTfRW(bool value) {
    this._enableTfRW = value;
  }

  bool get enableTfRT => _enableTfRT;

  set enableTfRT(bool value) {
    this._enableTfRT = value;
  }

  bool get enableTfAddress => _enableTfAddress;

  set enableTfAddress(bool value) {
    this._enableTfAddress = value;
  }

  get areaCodeTemp => _areaCodeTemp;

  set areaCodeTemp(value) {
    this._areaCodeTemp = value;
  }

  void setLocationAddressByMap(BuildContext context) async{
    Map _result = await Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage()));
    if(_result != null){
      _addressFromMap = _result;
      this._controllerAddressFromMap.text = _result["address"];
      notifyListeners();
    }
  }

}
