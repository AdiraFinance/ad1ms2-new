import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_ind_occupation_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_business_field.dart';
import 'package:ad1ms2_dev/screens/form_m/search_pep.dart';
import 'package:ad1ms2_dev/screens/form_m/search_sector_economi.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/get_company_type.dart';
import 'package:ad1ms2_dev/shared/search_business_field_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_pep_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_sector_economic_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class FormMOccupationChangeNotif with ChangeNotifier {
  int _oldListSize = 0;
  bool _autoValidate = false;
  int _selectedIndex = -1;
  OccupationModel _occupationSelected;
  TextEditingController _controllerEstablishedDate = TextEditingController();
  TextEditingController _controllerBusinessName = TextEditingController();
  TextEditingController _controllerTotalEstablishedDate =
      TextEditingController();
  TextEditingController _controllerEmployeeTotal = TextEditingController();
  TextEditingController _controllerSectorEconomic = TextEditingController();
  TextEditingController _controllerBusinessField = TextEditingController();
  TextEditingController _controllerCompanyName = TextEditingController();
  TextEditingController _controllerJenisPEP = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TypeOfBusinessModel _typeOfBusinessModelSelected;
  StatusLocationModel _statusLocationModelSelected;
  BusinessLocationModel _businessLocationModelSelected;
  ProfessionTypeModel _professionTypeModelSelected;
  CompanyTypeModel _companyTypeModelSelected;
  EmployeeStatusModel _employeeStatusModelSelected;
  SectorEconomicModel _sectorEconomicModelSelected;
  BusinessFieldModel _businessFieldModelSelected;
  PEPModel _pepModelSelected;
  String _flagChangeOccupation = "";
  List<OccupationModel> _listOccupation = [];
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  DbHelper _dbHelper = DbHelper();

  List<TypeOfBusinessModel> _listTypeOfBusiness = [
//    TypeOfBusinessModel("001", "PRIVATE"),
//    TypeOfBusinessModel("001", "PMA"),
//    TypeOfBusinessModel("001", "PMDN"),
//    TypeOfBusinessModel("001", "PMDA"),
//    TypeOfBusinessModel("001", "GOVERMENT"),
//    TypeOfBusinessModel("001", "BUMN"),
//    TypeOfBusinessModel("001", "KOPERASI"),
//    TypeOfBusinessModel("001", "YAYASAN"),
//    TypeOfBusinessModel("001", "PT"),
//    TypeOfBusinessModel("001", "CV"),
//    TypeOfBusinessModel("001", "PT,TBK"),
//    TypeOfBusinessModel("001", "BUMD"),
  ];

  List<StatusLocationModel> _listStatusLocation = [
//    StatusLocationModel("01", "RUMAH"),
//    StatusLocationModel("02", "PERTOKOAN"),
//    StatusLocationModel("03", "PASAR"),
//    StatusLocationModel("04", "INDUSTRI"),
//    StatusLocationModel("05", "PERKANTORAN"),
//    StatusLocationModel("06", "LAINNYA")
  ];

  List<BusinessLocationModel> _listBusinessLocation = [
//    BusinessLocationModel("01", "STRATEGIS"),
//    BusinessLocationModel("02", "BIASA"),
//    BusinessLocationModel("03", "TIDAK STRATEGIS"),
//    BusinessLocationModel("04", "LAINNYA")
  ];

  List<ProfessionTypeModel> _listProfessionType = [
//    ProfessionTypeModel("01", "GURU"),
//    ProfessionTypeModel("02", "DOKTER"),
//    ProfessionTypeModel("03", "LAWYER"),
//    ProfessionTypeModel("04", "LAINNYA")
  ];

  List<CompanyTypeModel> _listCompanyType = [
//    CompanyTypeModel("001", "PRIVATE"),
//    CompanyTypeModel("001", "PMA"),
//    CompanyTypeModel("001", "PMDN"),
//    CompanyTypeModel("001", "PMDA"),
//    CompanyTypeModel("001", "GOVERMENT"),
//    CompanyTypeModel("001", "BUMN"),
//    CompanyTypeModel("001", "KOPERASI"),
//    CompanyTypeModel("001", "YAYASAN"),
//    CompanyTypeModel("001", "PT"),
//    CompanyTypeModel("001", "CV"),
//    CompanyTypeModel("001", "PT,TBK"),
//    CompanyTypeModel("001", "BUMD"),
  ];
  List<EmployeeStatusModel> _listEmployeeStatus = [
    EmployeeStatusModel("001", "PEGAWAI TETAP"),
    EmployeeStatusModel("002", "PEGAWAI KONTRAK"),
  ];
  List<AddressModel> _listOccupationAddress = [];

  int get oldListSize => _oldListSize;

  set oldListSize(int value) {
    this._oldListSize = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  OccupationModel get occupationSelected => _occupationSelected;

  set occupationSelected(OccupationModel value) {
    this._occupationSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerEstablishedDate =>
      _controllerEstablishedDate;

  TextEditingController get controllerBusinessName => _controllerBusinessName;

  TextEditingController get controllerTotalEstablishedDate =>
      _controllerTotalEstablishedDate;

  TextEditingController get controllerEmployeeTotal => _controllerEmployeeTotal;

  TextEditingController get controllerSectorEconomic =>
      _controllerSectorEconomic;

  TextEditingController get controllerBusinessField => _controllerBusinessField;

  TypeOfBusinessModel get typeOfBusinessModelSelected =>
      _typeOfBusinessModelSelected;

  TextEditingController get controllerCompanyName => _controllerCompanyName;

  TextEditingController get controllerJenisPEP => _controllerJenisPEP;

  TextEditingController get controllerAddress => _controllerAddress;

  TextEditingController get controllerAddressType => _controllerAddressType;

  TextEditingController get controllerRT => _controllerRT;

  set typeOfBusinessModelSelected(TypeOfBusinessModel value) {
    this._typeOfBusinessModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<OccupationModel> get listOccupation {
    return UnmodifiableListView(this._listOccupation);
  }

  UnmodifiableListView<TypeOfBusinessModel> get listTypeOfBusiness {
    return UnmodifiableListView(this._listTypeOfBusiness);
  }

  UnmodifiableListView<StatusLocationModel> get listStatusLocation {
    return UnmodifiableListView(this._listStatusLocation);
  }

  UnmodifiableListView<BusinessLocationModel> get listBusinessLocation {
    return UnmodifiableListView(this._listBusinessLocation);
  }

  StatusLocationModel get statusLocationModelSelected =>
      _statusLocationModelSelected;

  set statusLocationModelSelected(StatusLocationModel value) {
    this._statusLocationModelSelected = value;
    notifyListeners();
  }

  BusinessLocationModel get businessLocationModelSelected =>
      _businessLocationModelSelected;

  set businessLocationModelSelected(BusinessLocationModel value) {
    this._businessLocationModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<ProfessionTypeModel> get listProfessionType {
    return UnmodifiableListView(this._listProfessionType);
  }

  ProfessionTypeModel get professionTypeModelSelected =>
      _professionTypeModelSelected;

  set professionTypeModelSelected(ProfessionTypeModel value) {
    this._professionTypeModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<CompanyTypeModel> get listCompanyType {
    return UnmodifiableListView(this._listCompanyType);
  }

  CompanyTypeModel get companyTypeModelSelected => _companyTypeModelSelected;

  set companyTypeModelSelected(CompanyTypeModel value) {
    this._companyTypeModelSelected = value;
    notifyListeners();
  }

  EmployeeStatusModel get employeeStatusModelSelected =>
      _employeeStatusModelSelected;

  set employeeStatusModelSelected(EmployeeStatusModel value) {
    this._employeeStatusModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<EmployeeStatusModel> get listEmployeeStatus {
    return UnmodifiableListView(this._listEmployeeStatus);
  }

  SectorEconomicModel get sectorEconomicModelSelected =>
      _sectorEconomicModelSelected;

  set sectorEconomicModelSelected(SectorEconomicModel value) {
    this._sectorEconomicModelSelected = value;
  }

  void searchSectorEconomic(BuildContext context) async {
    SectorEconomicModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchSectorEconomicChangeNotif(),
                child: SearchSectorEconomic())));
    if (data != null) {
      sectorEconomicModelSelected = data;
      this._controllerSectorEconomic.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  BusinessFieldModel get businessFieldModelSelected =>
      _businessFieldModelSelected;

  set businessFieldModelSelected(BusinessFieldModel value) {
    this._businessFieldModelSelected = value;
  }

  void searchBusinesField(BuildContext context) async {
    BusinessFieldModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBusinessFieldChangeNotif(),
                child: SearchBusinessField(id: this._sectorEconomicModelSelected.KODE,))));
    if (data != null) {
      businessFieldModelSelected = data;
      this._controllerBusinessField.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  PEPModel get pepModelSelected => _pepModelSelected;

  set pepModelSelected(PEPModel value) {
    this._pepModelSelected = value;
    notifyListeners();
  }

  void searchPEP(BuildContext context) async {
    PEPModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>SearchPEP()));
    if (data != null) {
      pepModelSelected = data;
      this._controllerJenisPEP.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  String get flagChangeOccupation => _flagChangeOccupation;

  set flagChangeOccupation(String value) {
    this._flagChangeOccupation = value;
    notifyListeners();
  }

  void clearOccupationSelected() {
    if (this._flagChangeOccupation != "") {
      if (this._flagChangeOccupation == "OC005" ||
          this._flagChangeOccupation == "OC006") {
        this._controllerBusinessName.clear();
        typeOfBusinessModelSelected = null;
        this._controllerSectorEconomic.clear();
        sectorEconomicModelSelected = null;
        this._controllerBusinessField.clear();
        businessFieldModelSelected = null;
        statusLocationModelSelected = null;
        businessLocationModelSelected = null;
        this._controllerEmployeeTotal.clear();
        this._controllerTotalEstablishedDate.clear();
      } else if (this._flagChangeOccupation == "OC007") {
        this._professionTypeModelSelected = null;
        this._controllerSectorEconomic.clear();
        sectorEconomicModelSelected = null;
        this._controllerBusinessField.clear();
        businessFieldModelSelected = null;
        statusLocationModelSelected = null;
        businessLocationModelSelected = null;
        this._controllerTotalEstablishedDate.clear();
      } else {
        this._controllerCompanyName.clear();
        companyTypeModelSelected = null;
        this._controllerSectorEconomic.clear();
        sectorEconomicModelSelected = null;
        this._controllerBusinessField.clear();
        businessFieldModelSelected = null;
        this._controllerEmployeeTotal.clear();
        this._controllerTotalEstablishedDate.clear();
        employeeStatusModelSelected = null;
        this._controllerJenisPEP.clear();
        pepModelSelected = null;
      }
      flagChangeOccupation = this._occupationSelected.KODE;
    } else {
      flagChangeOccupation = this._occupationSelected.KODE;
    }
  }

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerProvinsi => _controllerProvinsi;

  TextEditingController get controllerPostalCode => _controllerPostalCode;

  TextEditingController get controllerKodeArea => _controllerKodeArea;

  TextEditingController get controllerTlpn => _controllerTlpn;

  List<AddressModel> get listOccupationAddress => _listOccupationAddress;

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  void addOccupationAddress(AddressModel value) {
    this._listOccupationAddress.add(value);
    notifyListeners();
  }

  void updateOccupationAddress(AddressModel value, int index) {
    this._listOccupationAddress[index] = value;
    notifyListeners();
  }

  void deleteListOccupationAddress(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listOccupationAddress.removeAt(index);
                    if (selectedIndex == index) {
                      selectedIndex = -1;
                    }
                    this._controllerAddress.clear();
                    this._controllerAddressType.clear();
                    this._controllerRT.clear();
                    this._controllerRW.clear();
                    this._controllerKelurahan.clear();
                    this._controllerKecamatan.clear();
                    this._controllerKota.clear();
                    this._controllerProvinsi.clear();
                    this._controllerPostalCode.clear();
                    this._controllerKodeArea.clear();
                    this._controllerTlpn.clear();
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void setCorrespondenceAddress(AddressModel value,int index) {
    this._controllerAddress.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerKodeArea.text = value.areaCode;
    this._controllerTlpn.text = value.phone;
    for(int i=0; i < this._listOccupationAddress.length; i++){
      if(this._listOccupationAddress[i].isCorrespondence){
        this._listOccupationAddress[i].isCorrespondence = false;
      }
    }
    this._listOccupationAddress[index].isCorrespondence = true;
    notifyListeners();
  }

  void iconShowDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    "∙ Tekan 1x untuk edit",
                  ),
                  Text(
                    "∙ Tekan lama untuk memilih alamat korespondensi",
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height/37,),
                  Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void isShowDialog(BuildContext context) {
    if(this.listOccupationAddress.length == 1  || this.listOccupationAddress.length == 2) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  fontFamily: "NunitoSans"
              ),
              child: AlertDialog(
                title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "∙ Tekan 1x untuk edit",
                    ),
                    Text(
                      "∙ Tekan lama untuk memilih alamat korespondensi",
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/37,),
                    Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                        // _updateStatusShowDialogSimilarity();
                      },
                      child: Text(
                          "CLOSE",
                          style: TextStyle(
                              color: primaryOrange,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25
                          )
                      )
                  )
                ],
              ),
            );
          }
      );
    }
  }

  Future<void> getListOccupation(BuildContext context) async{
    try{
      _listOccupation.clear();
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/occupation/get-jenis-pekerjaan");
      final _data = jsonDecode(_response.body);
      for(int i=0; i < _data.length;i++){
        _listOccupation.add(OccupationModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
      // setValueEdit(context);
    } catch (e) {
      print(e);
    }
  }

  void getDefaultPEP(BuildContext context){
    var _providerPEP = Provider.of<SearchPEPChangeNotifier>(context, listen: false);
    if(_providerPEP.listPEP.isNotEmpty){
      if(this._controllerJenisPEP.text == ''){
        for(int i=0; i < _providerPEP.listPEP.length; i++){
          if(_providerPEP.listPEP[i].KODE == "MHN"){
            this._controllerJenisPEP.text = "${_providerPEP.listPEP[i].KODE} - ${_providerPEP.listPEP[i].DESKRIPSI}";
          }
        }
      }
    }
    else{
      _providerPEP.getPEP();
      if(_providerPEP.listPEP.isNotEmpty){
        for(int i=0; i < _providerPEP.listPEP.length; i++){
          if(_providerPEP.listPEP[i].KODE == "MHN"){
            this._controllerJenisPEP.text = "${_providerPEP.listPEP[i].KODE} - ${_providerPEP.listPEP[i].DESKRIPSI}";
          }
        }
      }
    }
    // notifyListeners();
  }

  void clearForm(){
    this._controllerEstablishedDate.clear();
    this._controllerBusinessName.clear();
    this._controllerTotalEstablishedDate.clear();
    this._controllerEmployeeTotal.clear();
    this._controllerSectorEconomic.clear();
    this._controllerBusinessField.clear();
    this._controllerCompanyName.clear();
    this._controllerJenisPEP.clear();
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerKodeArea.clear();
    this._controllerTlpn.clear();
    this._listOccupationAddress.clear();
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerKodeArea.clear();
    this._controllerTlpn.clear();
    this._typeOfBusinessModelSelected = null;
    this._statusLocationModelSelected = null;
   this._businessLocationModelSelected = null;
   this._professionTypeModelSelected = null;
   this._companyTypeModelSelected = null;
   this._employeeStatusModelSelected = null;
   this._sectorEconomicModelSelected = null;
   this._businessFieldModelSelected = null;
   this._pepModelSelected = null;
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: primaryOrange,
                          size: 22.0,
                        ),
                        SizedBox(width: 12.0),
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              selectedIndex = index;
                              setCorrespondenceAddress(listOccupationAddress[index],index);
                              Navigator.pop(context);
                            },
                            child: Text(
                              "Pilih sebagai Alamat Korespondensi",
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                        )
                      ]
                  ),
                  SizedBox(height: 12.0),
                  listOccupationAddress[index].jenisAlamatModel.KODE != "03"
                      ? listOccupationAddress[index].isSameWithIdentity
                      ? SizedBox()
                      : Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                          size: 22.0,
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                            deleteListOccupationAddress(context, index);
                          },
                          child: Text(
                            "Hapus",
                            style: TextStyle(fontSize: 14.0, color: Colors.red),
                          ),
                        )
                      ]
                  )
                      : SizedBox(),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }


  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getCompanyType() async{
    this._listCompanyType.clear();
    loadData = true;
    try{
      var _result = await GetCompanyType().getCompanyTypeData();
      for(int i=0; i <_result['data'].length; i++){
        this._listCompanyType.add(
            CompanyTypeModel(_result[i]['kode'], _result[i]['deskripsi'])
        );
      }
      loadData = false;
    }
    catch(e){
      loadData = false;
      showSnackBar(e);
    }
    notifyListeners();
  }

  void getLocationStatus() async{
    this._listStatusLocation.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
      // "${BaseUrl.unit}api/parameter/get-group-object",
        "https://103.110.89.34/public/ms2dev/jenis-rehab/get_stat_lokasi",
        headers: {"Content-Type":"application/json"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      for(int i=0; i < _data.length;i++){
        this._listStatusLocation.add(StatusLocationModel(_data[i]['kode'], _data[i]['deskripsi']));
      }
      loadData = false;
    }
    else{
      loadData = false;
      showSnackBar("Error get status lokasi error ${_response.statusCode}");
    }
    notifyListeners();
  }

  void getBusinessLocation() async{
    this._listBusinessLocation.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
      // "${BaseUrl.unit}api/parameter/get-group-object",
        "${BaseUrl.urlPublic}/jenis-rehab/get_lok_usaha",
        headers: {"Content-Type":"application/json"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      for(int i=0; i < _data.length;i++){
        this._listBusinessLocation.add(BusinessLocationModel(_data[i]['kode'], _data[i]['deskripsi']));
      }
      loadData = false;
    }
    else{
      loadData = false;
      showSnackBar("Error get status lokasi error ${_response.statusCode}");
    }
    notifyListeners();
  }

  void getProfessionType() async{
    this._listProfessionType.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
      // "${BaseUrl.unit}api/parameter/get-group-object",
        "https://103.110.89.34/public/ms2dev/jenis-rehab/get_jenis_profesi",
        headers: {"Content-Type":"application/json"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      for(int i=0; i < _data.length;i++){
        this._listProfessionType.add(ProfessionTypeModel(_data[i]['kode'], _data[i]['deskripsi']));
      }
      loadData = false;
    }
    else{
      loadData = false;
      showSnackBar("Error get status lokasi error ${_response.statusCode}");
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  // Future<void> clearDataOccupation() async {
  void clearDataOccupation() {
    this._autoValidate = false;
    this._occupationSelected = null;
    this._controllerEstablishedDate.clear();
    // this._controllerBusinessName.clear();
    // this._typeOfBusinessModelSelected = null;
    this._controllerSectorEconomic.clear();
    // this._controllerBusinessField.clear();
    // this._statusLocationModelSelected = null;
    // this._businessLocationModelSelected = null
    // this._controllerEmployeeTotal.clear();
    this._controllerTotalEstablishedDate.clear();
    // this._professionTypeModelSelected = null;
    this._businessLocationModelSelected = null;
    // Pekerjaan Lainnya
    this._controllerCompanyName.clear();
    this._controllerJenisPEP.clear();
    this._employeeStatusModelSelected = null;
    // Alamat
    this._selectedIndex = -1;
    this._listOccupationAddress = [];
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerKodeArea.clear();
    this._controllerTlpn.clear();
  }

  void saveToSQLite(BuildContext context, String type){
    var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    // _dbHelper.insertMS2CustIndOccupation(MS2CustIndOccupationModel("123", occupation.occupationSelected.KODE,
    //     occupation.occupationSelected.DESKRIPSI, _controllerBusinessName.text, _controllerBusinessField.text,
    //     _pepModelSelected.KODE, _pepModelSelected.DESKRIPSI, _employeeStatusModelSelected.id, _employeeStatusModelSelected.desc,
    //     _professionTypeModelSelected.id, _professionTypeModelSelected.desc, _sectorEconomicModelSelected.KODE,
    //     _sectorEconomicModelSelected.DESKRIPSI, null, null, _statusLocationModelSelected.id, _statusLocationModelSelected.desc,
    //     _businessLocationModelSelected.id, _businessLocationModelSelected.desc, int.parse(_controllerEmployeeTotal.text),
    //     int.parse(_controllerEstablishedDate.text), int.parse(_controllerTotalEstablishedDate.text),
    //     null, null, null, null, 1, null, null, null));
    print(occupation.occupationSelected.KODE);
    if(occupation.occupationSelected.KODE == "05" || occupation.occupationSelected.KODE == "07"){
      print("data A");
      _dbHelper.insertMS2CustIndOccupation(
          MS2CustIndOccupationModel(
              "123",
              occupation.occupationSelected != null ? occupation.occupationSelected.KODE :"",
              occupation.occupationSelected != null ? occupation.occupationSelected.DESKRIPSI:"",
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              _sectorEconomicModelSelected != null ? _sectorEconomicModelSelected.KODE:"",
              _sectorEconomicModelSelected != null ? _sectorEconomicModelSelected.DESKRIPSI:"",
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              _controllerEstablishedDate.text != "" ? int.parse(_controllerEstablishedDate.text):null,
              _controllerTotalEstablishedDate.text != "" ?  int.parse(_controllerTotalEstablishedDate.text):null,
              null,
              null,
              null,
              null,
              1,
              null,
              null,
              null
          )
      );
    } else if(occupation.occupationSelected.KODE == "08"){
      print("data B");
      _dbHelper.insertMS2CustIndOccupation(MS2CustIndOccupationModel("123", occupation.occupationSelected.KODE,
          occupation.occupationSelected.DESKRIPSI, null, null, null, null, null, null, null, null,
          _sectorEconomicModelSelected.KODE, _sectorEconomicModelSelected.DESKRIPSI, null, null, _statusLocationModelSelected.id,
          _statusLocationModelSelected.desc, null, null, null, int.parse(_controllerEstablishedDate.text),
          int.parse(_controllerTotalEstablishedDate.text), null, null, null, null, 1, null, null, null));
    } else {
      print("data C");
      _dbHelper.insertMS2CustIndOccupation(MS2CustIndOccupationModel("123", occupation.occupationSelected.KODE,
          occupation.occupationSelected.DESKRIPSI, null, null, _pepModelSelected.KODE, _pepModelSelected.DESKRIPSI,
          _employeeStatusModelSelected.id, _employeeStatusModelSelected.desc, null, null, null, null, null, null, null,
          null, null, null, null, int.parse(_controllerEstablishedDate.text), int.parse(_controllerTotalEstablishedDate.text),
          null, null, null, null, 1, null, null, null));
    }

    List<MS2CustAddrModel> _listAddress = [];
    for(int i=0; i<_listOccupationAddress.length; i++){
      _listAddress.add(MS2CustAddrModel("123", _listOccupationAddress[i].isCorrespondence.toString(), type,
          _listOccupationAddress[i].address, _listOccupationAddress[i].rt, null, _listOccupationAddress[i].rw, null,
          _listOccupationAddress[i].kelurahanModel.PROV_ID, _listOccupationAddress[i].kelurahanModel.PROV_NAME,
          _listOccupationAddress[i].kelurahanModel.KABKOT_ID, _listOccupationAddress[i].kelurahanModel.KABKOT_NAME,
          _listOccupationAddress[i].kelurahanModel.KEC_ID, _listOccupationAddress[i].kelurahanModel.KEC_NAME,
          _listOccupationAddress[i].kelurahanModel.KEL_ID, _listOccupationAddress[i].kelurahanModel.KEL_NAME,
          _listOccupationAddress[i].kelurahanModel.ZIPCODE, null, _listOccupationAddress[i].phone, _listOccupationAddress[i].areaCode,
          null, null, null, null, _listOccupationAddress[i].jenisAlamatModel.KODE,
          _listOccupationAddress[i].jenisAlamatModel.DESKRIPSI, _listOccupationAddress[i].addressLatLong['latitude'].toString(),
          _listOccupationAddress[i].addressLatLong['longitude'].toString(),
          _listOccupationAddress[i].addressLatLong['address'].toString(), null, null, null, null, 1));

      // _dbHelper.insertMS2CustAddr(MS2CustAddrModel("123", _listOccupationAddress[i].isCorrespondence.toString(), type,
      //     _listOccupationAddress[i].address, _listOccupationAddress[i].rt, null, _listOccupationAddress[i].rw, null,
      //     _listOccupationAddress[i].kelurahanModel.PROV_ID, _listOccupationAddress[i].kelurahanModel.PROV_NAME,
      //     _listOccupationAddress[i].kelurahanModel.KABKOT_ID, _listOccupationAddress[i].kelurahanModel.KABKOT_NAME,
      //     _listOccupationAddress[i].kelurahanModel.KEC_ID, _listOccupationAddress[i].kelurahanModel.KEC_NAME,
      //     _listOccupationAddress[i].kelurahanModel.KEL_ID, _listOccupationAddress[i].kelurahanModel.KEL_NAME,
      //     _listOccupationAddress[i].kelurahanModel.ZIPCODE, null, _listOccupationAddress[i].phone, _listOccupationAddress[i].areaCode,
      //     null, null, null, null, _listOccupationAddress[i].jenisAlamatModel.KODE,
      //     _listOccupationAddress[i].jenisAlamatModel.DESKRIPSI, _listOccupationAddress[i].addressLatLong['latitude'].toString(),
      //     _listOccupationAddress[i].addressLatLong['longitude'].toString(),
      //     _listOccupationAddress[i].addressLatLong['address'].toString(), null, null, null, null, 1));
    }
    _dbHelper.insertMS2CustAddr(_listAddress);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustIndOccupation();
  }

  void setDataFromSQLite(type) async{
    //pekerjaan
    var _occupation = await _dbHelper.selectMS2CustIndOccupation();
    print(_occupation);
    if(_occupation.isNotEmpty){
      this._occupationSelected = OccupationModel(_occupation[0]['occupation'], _occupation[0]['occupation_desc']);
      this._controllerEstablishedDate.text =_occupation[0]['no_of_year_work'].toString();
      // this._controllerBusinessName.clear();
      // this._typeOfBusinessModelSelected = null;
      if(_occupation[0]['sector_economic'] != null){
        this._controllerSectorEconomic.text = _occupation[0]['sector_economic_desc'];
        this._sectorEconomicModelSelected = SectorEconomicModel(_occupation[0]['sector_economic'], _occupation[0]['sector_economic_desc']);
      }
      // this._controllerBusinessField.clear();
      // this._statusLocationModelSelected = null;
      // this._businessLocationModelSelected = null
      // this._controllerEmployeeTotal.clear();
      this._controllerTotalEstablishedDate.text = _occupation[0]['no_of_year_buss'].toString();
      // this._professionTypeModelSelected = null;
      if(_occupation[0]['buss_location'] != null) {
        this._businessLocationModelSelected = BusinessLocationModel(_occupation[0]['buss_location'], _occupation[0]['buss_location_desc']);
      }
      // Pekerjaan Lainnya
      // this._controllerCompanyName.clear();
      if(_occupation[0]['pep_type'] != "null") {
        this._controllerJenisPEP.text = _occupation[0]['pep_desc'];
        this._pepModelSelected = PEPModel(_occupation[0]['pep_type'], _occupation[0]['pep_desc']);
      }
      if(_occupation[0]['emp_status'] != "null") {
        this._employeeStatusModelSelected = EmployeeStatusModel(_occupation[0]['emp_status'], _occupation[0]['emp_status_desc']);
      }
    }
    //alamat
    var _check = await _dbHelper.selectMS2CustAddr(type);
    if(_check.isNotEmpty){
      for(int i=0; i<_check.length; i++){
        var jenisAlamatModel = JenisAlamatModel(_check[i]['addr_type'], _check[i]['addr_desc']);
        var kelurahanModel = KelurahanModel(_check[i]['kelurahan'], _check[i]['kelurahan_desc'],
            _check[i]['kecamatan'], _check[i]['kecamatan_desc'], _check[i]['kabkot'], _check[i]['kabkot_desc'],
            _check[i]['provinsi'], _check[i]['provinsi_desc'], _check[i]['zip_code']);
        var _addressFromMap = {
          "latitude": _check[i]['latitude'],
          "longitude": _check[i]['longitude']
        };
        _listOccupationAddress.add(AddressModel(
            jenisAlamatModel,
            kelurahanModel,
            _check[i]['address'],
            _check[i]['rt'],
            _check[i]['rw'],
            _check[i]['phone1_area'],
            _check[i]['phone1'],
            _check[i]['koresponden'] == "true" ? true : false,
            _addressFromMap,
            false
        ));
        if(_check[i]['koresponden'] == "true"){
          selectedIndex = i;
          setCorrespondenceAddress(listOccupationAddress[i],i);
        }
      }
    }
    notifyListeners();
  }
}
