import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/check_mpl_activation_model.dart';
import 'package:ad1ms2_dev/models/reference_number_cl_model.dart';
import 'package:ad1ms2_dev/models/reference_number_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_credit_limit_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class SearchReferenceNumberCLChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<ReferenceNumberCLModel> _listReferenceNumberCL = [];
  List<CheckMplActivationModel> _listCheckMplActivation = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController get controllerSearch => _controllerSearch;
  String token;

  bool get showClear => _showClear;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ReferenceNumberCLModel> get listReferenceNumberCL {
    return UnmodifiableListView(this._listReferenceNumberCL);
  }

  UnmodifiableListView<CheckMplActivationModel> get listCheckMplActivation {
    return UnmodifiableListView(this._listCheckMplActivation);
  }

  void getToken(BuildContext context, String query) async {
    this._listReferenceNumberCL.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "grant_type": "client_credentials",
    });

    String _username = 'ad1gate';
    String _password = 'ad1gatePassw0rd';
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$_username:$_password'));

    final _response = await _http.post(
        "${BaseUrl.urlLme}oauth/token",
        body: _body,
        headers: {
          "Content-Type":"application/x-www-form-urlencoded",
          'Authorization': basicAuth
        }
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      print(_result);
      if(_result.isNotEmpty){
        token = _result['access_token'];
        loadData = false;
        getReferenceNumberCL(context, query);
      }
      else{
        showSnackBar("Token gagal didapatkan");
        loadData = false;
      }
    }
    else{
      showSnackBar("Error get token response ${_response.statusCode}");
      loadData = false;
    }
  }

  void getReferenceNumberCL(BuildContext context, String query) async{
    var dataTypeOffer = Provider.of<FormMCreditLimitChangeNotifier>(context, listen: false).typeOfferSelected.KODE;
    if(dataTypeOffer == "01") {
      // Inq Voucher
      if(query.length < 3) {
        showSnackBar("Input minimal 3 karakter");
      } else {
        this._listReferenceNumberCL.clear();
        SharedPreferences _preference = await SharedPreferences.getInstance();

        loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);

        var _body = jsonEncode({
          "oid" : "",
          "source_reff_id" : "MS2${_preference.getString("username")}",
          "req_date" : DateTime.now(),
          "source_channel_id" : "Ad1ms2"
        });

        final _response = await _http.post(
            "${BaseUrl.urlLme}InqService/inqVoucher",
            body: _body,
            headers: {
              "Content-Type":"application/json",
              "Authorization":"Bearer $token"
            }
        );

        if(_response.statusCode == 200){
          final _result = jsonDecode(_response.body);
          print(_result);
          if(_result.isNotEmpty){
            for(int i = 0; i < _result.length; i++){
              this._listReferenceNumberCL.add(ReferenceNumberCLModel(
                _result[i]['reff_id'],
                _result[i]['source_reff_id'],
                _result[i]['oid'],
                _result[i]['source_channel_id'],
                _result[i]['response_code'],
                _result[i]['response_desc'],
                _result[i]['response_date'],
                _result[i]['req_date']
              ));
            }
            loadData = false;
          }
          else{
            showSnackBar("No reference tidak ditemukan");
            loadData = false;
          }
        }
        else{
          showSnackBar("Error get no reference response ${_response.statusCode}");
          loadData = false;
        }
      }
    } else if(dataTypeOffer == "03") {
      // Check Mpl Activation
      if(query.length < 3) {
        showSnackBar("Input minimal 3 karakter");
      } else {
        this._listCheckMplActivation.clear();
        SharedPreferences _preference = await SharedPreferences.getInstance();

        loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);

        var _body = jsonEncode({
          "oid" : "",
          "lme_id" : "",
          "branch_code" : "",
          "source_reff_id" : "MS2${_preference.getString("username")}",
          "req_date" : DateTime.now(),
          "source_channel_id" : "Ad1ms2"
        });

        final _response = await _http.post(
            "${BaseUrl.urlLme}InqService/checkMplActivation",
            body: _body,
            headers: {
              "Content-Type":"application/json",
              "Authorization":"Bearer $token"
            }
        );

        if(_response.statusCode == 200){
          final _result = jsonDecode(_response.body);
          print(_result);
          if(_result.isNotEmpty){
            for(int i = 0; i < _result.length; i++){
              this._listCheckMplActivation.add(CheckMplActivationModel(
                _result[i]['branch_code'],
                _result[i]['source_reff_id'],
                _result[i]['reff_id'],
                _result[i]['source_channel_id'],
                _result[i]['oid'],
                _result[i]['lme_id'],
                _result[i]['mpl_type'],
                _result[i]['jenis_kegiatan_usaha'],
                _result[i]['mpl_status'],
                _result[i]['disbursement_number'],
                _result[i]['capacity_limit_mpl'],
                _result[i]['capacity_ph_mpl'],
                _result[i]['activation_date'],
                _result[i]['remaining_tenor'],
                _result[i]['contract_reference'],
                _result[i]['activation_branch_code'],
                _result[i]['activation_channel'],
                _result[i]['response_code'],
                _result[i]['response_desc'],
                _result[i]['req_date'],
              ));
            }
            loadData = false;
          }
          else{
            showSnackBar("No reference tidak ditemukan");
            loadData = false;
          }
        }
        else{
          showSnackBar("Error get no reference response ${_response.statusCode}");
          loadData = false;
        }
      }
    }

    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
