import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';

class SearchSourceOrderNameChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<SourceOrderNameModel> _listSourceOrderName = [
//    SourceOrderNameModel("07040000", "MAKASSAR DRB"),
//    SourceOrderNameModel("07050000", "KENDARI-AHMAD YANI"),
//    SourceOrderNameModel("07060000", "PALU-EMMY SAELAN"),
//    SourceOrderNameModel("07070000", "MANADO-AHMAD YANI"),
//    SourceOrderNameModel("07080000", "GORONTALO-NANI WARTABONE"),
//    SourceOrderNameModel("07090000", "KOTAMUBAGU"),
//    SourceOrderNameModel("07120000", "MAKASSAR 2 CAR-PANAKUKANG"),
//    SourceOrderNameModel("07160000", "SENGKANG-WAJO-PANGGARU"),
//    SourceOrderNameModel("07200000", "MAMUJU-URIP SUMOHARJO"),
  ];

  List<SourceOrderNameModel> _listSourceOrderNameTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderName {
    return UnmodifiableListView(this._listSourceOrderName);
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameTemp {
    return UnmodifiableListView(this._listSourceOrderNameTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getSourceOrderName() async{
    this._listSourceOrderName.clear();
    this._listSourceOrderNameTemp.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
      // "${BaseUrl.unit}api/parameter/get-group-object",
        "${BaseUrl.urlPublic}pihak-ketiga/get_sumberorder_name",
        headers: {"Content-Type":"application/json"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      print(_result);
      if(_result['data'] == null){
        showSnackBar("Program tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result['data'].length; i++){
          this._listSourceOrderName.add(
              SourceOrderNameModel(
                  _result['data'][i]['kode'],
                  _result['data'][i]['deskripsi']
                  // _result[i]['sentraId'],
                  // _result[i]['sentraName'],
                  // _result[i]['unitId'],
                  // _result[i]['unitName']
              )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchSourceOrderName(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    }
    else {
      _listSourceOrderNameTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listSourceOrderName.forEach((dataProgram) {
        if (dataProgram.kode.contains(query) || dataProgram.deskripsi.contains(query)) {
          this._listSourceOrderNameTemp.add(dataProgram);
        }
      });
      notifyListeners();
    }
  }

  void clearSearchTemp() {
    _listSourceOrderNameTemp.clear();
  }
}
