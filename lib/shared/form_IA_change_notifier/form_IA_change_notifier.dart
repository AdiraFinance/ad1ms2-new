import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class FormIAChangeNotifier with ChangeNotifier{
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAppNumber = TextEditingController();
  TextEditingController _controllerModelObject = TextEditingController();
  int _radioValueIsSignPK = -1;

  TextEditingController get controllerName => _controllerName;

  TextEditingController get controllerAddress => _controllerAddress;

  TextEditingController get controllerAppNumber => _controllerAppNumber;

  TextEditingController get controllerModelObject => _controllerModelObject;

  int get radioValueIsSignPK => _radioValueIsSignPK;

  set radioValueIsSignPK(int value) {
    this._radioValueIsSignPK = value;
    notifyListeners();
  }
}