import 'dart:collection';

import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_info_keluarga_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_keluarga.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'constants.dart';
import 'date_picker.dart';

class AddInfoKeluargaChangeNotif with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  List<RelationshipStatusModel> _listRelationShipStatus = [];
  List<RelationshipStatusModel> _listRelationShipStatusTemp = [];
  bool _autoValidate = false;
  RelationshipStatusModel _relationshipStatusSelected;
  RelationshipStatusModel _relationshipStatusSelectedTemp;
  IdentityModel _identitySelected;
  IdentityModel _identitySelectedTemp;
  TextEditingController _controllerNoIdentitas = TextEditingController();
  TextEditingController _controllerNamaLengkapIdentitas =
      TextEditingController();
  TextEditingController _controllerNamaLengkap = TextEditingController();
  TextEditingController _controllerTglLahir = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitas =
      TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitasLOV = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TextEditingController _controllerNoHp = TextEditingController();
  String _radioValueGender = "01";
  String _noIdentitas;
  String _namaLengkapIdentitas;
  String _namaLengkap;
  String _birthdate;
  String _birthPlace;
  String _birthPlaceLOV;
  String _areaCode;
  String _phoneNumber;
  String _cellPhoneNumber;
  String _radioValueGenderTemp = "01";
  BirthPlaceModel _birthPlaceSelected;

  List<IdentityModel> _listIdentityType = IdentityType().lisIdentityModel;
//  [
//    IdentityModel("01", "KTP"),
//    IdentityModel("03", "PASSPORT"),
//    IdentityModel("04", "SIM"),
//    IdentityModel("05", "KTP Sementara"),
//    IdentityModel("06", "Resi KTP"),
//    IdentityModel("07", "Ket. Domisili"),
//  ];

  DateTime _initialDateForTglLahir =
      DateTime(dateNow.year, dateNow.month, dateNow.day);

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  String get radioValueGender => _radioValueGender;

  set radioValueGender(String value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  RelationshipStatusModel get relationshipStatusSelected =>
      _relationshipStatusSelected;

  set relationshipStatusSelected(RelationshipStatusModel value) {
    this._relationshipStatusSelected = value;
    if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01") {
      radioValueGender = "01";
    } else if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02") {
      radioValueGender = "02";
    } else if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04") {
      radioValueGender = "01";
    }
    notifyListeners();
  }

  IdentityModel get identitySelected => _identitySelected;

  set identitySelected(IdentityModel value) {
    this._identitySelected = value;
    this._controllerNoIdentitas.clear();
    notifyListeners();
  }

  TextEditingController get controllerNoIdentitas => _controllerNoIdentitas;

  TextEditingController get controllerTempatLahirSesuaiIdentitasLOV =>
      _controllerTempatLahirSesuaiIdentitasLOV;

  UnmodifiableListView<RelationshipStatusModel> get listRelationShipStatus {
    return UnmodifiableListView(this._listRelationShipStatus);
  }

  UnmodifiableListView<RelationshipStatusModel> get listRelationShipStatusTemp {
    return UnmodifiableListView(this._listRelationShipStatusTemp);
  }

  UnmodifiableListView<IdentityModel> get listIdentityType {
    return UnmodifiableListView(this._listIdentityType);
  }

  void selectBirthDate(BuildContext context) async {
    // DatePickerShared _datePickerShared = DatePickerShared();
    // var _datePickerSelected = await _datePickerShared.selectStartDate(
    //     context, this._initialDateForTglLahir,
    //     canAccessNextDay: false);
    // if (_datePickerSelected != null) {
    //   this._controllerTglLahir.text = dateFormat.format(_datePickerSelected);
    //   this._initialDateForTglLahir = _datePickerSelected;
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _datePickerSelected = await selectDate(context, _initialDateForTglLahir);
    if (_datePickerSelected != null) {
      this._controllerTglLahir.text = dateFormat.format(_datePickerSelected);
      this._initialDateForTglLahir = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  void checkValidCOdeArea(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerKodeArea.clear();
      });
    } else {
      return;
    }
  }

  void checkValidNotZero(String value) {
    if (value == "0") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerNoHp.clear();
      });
    } else {
      return;
    }
  }

  void check(BuildContext context, int flag, int index) {
    final _form = _key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false)
            .addListInfoKel(FormMInfoKelModel(
                this._identitySelected,
                this._relationshipStatusSelected,
                this._controllerNoIdentitas.text,
                this.controllerNamaLengkapIdentitas.text,
                this._controllerNamaLengkap.text,
                this._controllerTglLahir.text,
                this._radioValueGender,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this.controllerTempatLahirSesuaiIdentitas.text,
                this._controllerNoHp.text,this._birthPlaceSelected));
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    } else {
      if (_form.validate()) {
        Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false)
            .updateListInfoKel(
                FormMInfoKelModel(
                    this._identitySelected,
                    this._relationshipStatusSelected,
                    this._controllerNoIdentitas.text,
                    this.controllerNamaLengkapIdentitas.text,
                    this._controllerNamaLengkap.text,
                    this._controllerTglLahir.text,
                    this._radioValueGender,
                    this._controllerKodeArea.text,
                    this._controllerTlpn.text,
                    this.controllerTempatLahirSesuaiIdentitas.text,
                    this._controllerNoHp.text,this._birthPlaceSelected),
                context,
                index);
      } else {
        autoValidate = true;
      }
    }
  }

  TextEditingController get controllerNamaLengkapIdentitas =>
      _controllerNamaLengkapIdentitas;

  TextEditingController get controllerNamaLengkap => _controllerNamaLengkap;

  TextEditingController get controllerTglLahir => _controllerTglLahir;

  TextEditingController get controllerTempatLahirSesuaiIdentitas =>
      _controllerTempatLahirSesuaiIdentitas;

  TextEditingController get controllerKodeArea => _controllerKodeArea;

  TextEditingController get controllerTlpn => _controllerTlpn;

  TextEditingController get controllerNoHp => _controllerNoHp;

  GlobalKey<FormState> get key => _key;

  RelationshipStatusModel get relationshipStatusSelectedTemp =>
      _relationshipStatusSelectedTemp;

  IdentityModel get identitySelectedTemp => _identitySelectedTemp;

  String get noIdentitas => _noIdentitas;


  String get birthPlaceLOV => _birthPlaceLOV;

  get namaLengkapIdentitas => _namaLengkapIdentitas;

  get namaLengkap => _namaLengkap;

  get birthdate => _birthdate;

  get birthPlace => _birthPlace;

  get areCode => _areaCode;

  get phoneNumber => _phoneNumber;

  get cellPhoneNumber => _cellPhoneNumber;

  String get radioValueGenderTemp => _radioValueGenderTemp;

  Future<void> setValueForEditInfoKeluarga(BuildContext context, FormMInfoKelModel data,int flag) async {
    relationshipFamily(context);
    print(flag);

    if(flag == 1){
      if(data.relationshipStatusModel != null){
        for (int i = 0; i < this._listRelationShipStatus.length; i++) {
          if (data.relationshipStatusModel.PARA_FAMILY_TYPE_ID == this._listRelationShipStatus[i].PARA_FAMILY_TYPE_ID) {
            this._relationshipStatusSelected = _listRelationShipStatus[i];
            this._relationshipStatusSelectedTemp = _listRelationShipStatus[i];
          }
        }
      }


      if(data.identityModel != null){
        for (int j = 0; j < this._listIdentityType.length; j++) {
          if (data.identityModel.id == _listIdentityType[j].id) {
            this._identitySelected = this._listIdentityType[j];
            this._identitySelectedTemp = this._listIdentityType[j];
          }
        }
      }

      print(" tgl ${data.birthDate}");
      this._controllerNoIdentitas.text = data.noIdentitas;
      this._noIdentitas = this._controllerNoIdentitas.text;
      this._controllerNamaLengkapIdentitas.text = data.namaLengkapSesuaiIdentitas;
      this._namaLengkapIdentitas = this._controllerNamaLengkapIdentitas.text;
      this._controllerNamaLengkap.text = data.namaLengkap;
      this._namaLengkap = this._controllerNamaLengkap.text;
//      this._initialDateForTglLahir = DateTime.parse(data.birthDate);
//      print(_initialDateForTglLahir);
      this._controllerTglLahir.text = data.birthDate;
          dateFormat.format(this._initialDateForTglLahir);
      this._birthdate = this._controllerTglLahir.text;
      this._controllerTempatLahirSesuaiIdentitas.text =
          data.tmptLahirSesuaiIdentitas;
      if(data.birthPlaceModel != null){
        this._birthPlaceSelected = data.birthPlaceModel;
        this._controllerTempatLahirSesuaiIdentitasLOV.text = "${data.birthPlaceModel.KABKOT_ID} -${data.birthPlaceModel.KABKOT_NAME}";
        this._birthPlaceLOV = this._controllerTempatLahirSesuaiIdentitasLOV.text;
      }
      this._birthPlace = this._controllerTempatLahirSesuaiIdentitas.text;
      this._radioValueGender = data.gender;
      this._radioValueGenderTemp = this._radioValueGender;
      this._controllerKodeArea.text = data.kodeArea;
      this._areaCode = this._controllerKodeArea.text;
      this._controllerTlpn.text = data.noTlpn;
      this._phoneNumber = this._controllerTlpn.text;
      this._controllerNoHp.text = data.noHp;
      this._cellPhoneNumber = this._controllerNoHp.text;
    }
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerTempatLahirSesuaiIdentitasLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  void relationshipFamily(BuildContext context) {
    this._relationshipStatusSelected = null;
    this._listRelationShipStatus.clear();

    // menghilangkan ibu
    for(int i = 0; i < RelationshipStatusList().relationshipStatusItems.length; i++) {
      if(RelationshipStatusList().relationshipStatusItems[i].PARA_FAMILY_TYPE_ID != "05") {
        _listRelationShipStatusTemp.add(RelationshipStatusList().relationshipStatusItems[i]);
      }
    }

    // menghilangan suami, istri apabila belum kawin
    for(int j = 0; j < _listRelationShipStatusTemp.length; j++) {
      if(Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).maritalStatusSelected.id == "02") {
        // kawin
        _listRelationShipStatus.add(_listRelationShipStatusTemp[j]);
      } else if(Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).maritalStatusSelected.id == "04") {
        // duda/janda dengan anak
        if(_listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "01" && _listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "02") {
          _listRelationShipStatus.add(_listRelationShipStatusTemp[j]);
        }
      } else {
        // single
        if(_listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "01" && _listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "02" && _listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "03") {
          _listRelationShipStatus.add(_listRelationShipStatusTemp[j]);
        }
      }
    }
  }

  bool isEmptyAll(){
    if(this._relationshipStatusSelected == null && this._identitySelected == null
        && this._controllerNoIdentitas.text == "" && this._controllerNamaLengkapIdentitas.text == ""
        && this._controllerNamaLengkap.text == "" && this._controllerTglLahir.text == ""
        && this._controllerTempatLahirSesuaiIdentitas.text == "" && this._controllerTempatLahirSesuaiIdentitasLOV.text == ""
        && this._controllerKodeArea.text == "" && this._controllerTlpn.text == "" && this._controllerNoHp.text == ""
    ){
      return true;
    }
    else{
      return false;
    }
  }
}
