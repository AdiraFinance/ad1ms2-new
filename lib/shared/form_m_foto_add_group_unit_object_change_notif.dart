import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/screens/detail_image.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';

import '../main.dart';
import 'group_unit_object_model.dart';

class FormMFotoAddGroupUnitObject with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _setData = false;
  bool _autoValidateAddGroupUnitObject = false;
  final _imagePicker = ImagePicker();
  GroupObjectUnit _groupObjectUnitSelected;
  GroupObjectUnit _groupObjectUnitTemp;
  List<ObjectUnit> _listObjectUnit = [];
  ObjectUnit _objectUnitSelected;
  ObjectUnit _objectUnitTemp;
  List<ImageFileModel> _listFotoGroupObjectUnit = [];
  int _lengthListFileImage = 0;
  String _pathFile;


  String get pathFile => _pathFile;

  List<GroupObjectUnit> _listGroupObjectUnit = [
    GroupObjectUnit("01", "Mobil"),
    GroupObjectUnit("02", "Motor"),
    GroupObjectUnit("03", "Jasa")
  ];

  bool get autoValidateAddGroupUnitObject => _autoValidateAddGroupUnitObject;

  set autoValidateAddGroupUnitObject(bool value) {
    this._autoValidateAddGroupUnitObject = value;
    notifyListeners();
  }

  GroupObjectUnit get groupObjectUnitSelected => _groupObjectUnitSelected;

  set groupObjectUnitSelected(GroupObjectUnit value) {
    this._groupObjectUnitSelected = value;
    notifyListeners();
  }

  void setDataListObjectUnit() {
    if (this._groupObjectUnitSelected.id != "03") {
      _listObjectUnit = [
        // ObjectUnit("011", "Baru"),
        ObjectUnit("012", "Bekas"),
      ];
    } else {
      _listObjectUnit = [
        ObjectUnit("013", "Jasa"),
      ];
    }
  }

  void addFile() async {
    var _image = await _imagePicker.getImage(
        source: ImageSource.camera,
        maxHeight: 1920.0,
        maxWidth: 1080.0,
        imageQuality: 50);
    savePhoto(_image);
  }

  ObjectUnit get objectUnitSelected => _objectUnitSelected;

  set objectUnitSelected(ObjectUnit value) {
    this._objectUnitSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<ObjectUnit> get listObjectUnit {
    List<ObjectUnit> _emptyList = [];
    if (this._groupObjectUnitSelected != null) {
      return UnmodifiableListView(this._listObjectUnit);
    } else {
      return UnmodifiableListView(_emptyList);
    }
  }

  UnmodifiableListView<GroupObjectUnit> get listGroupObjectUnit {
    return UnmodifiableListView(this._listGroupObjectUnit);
  }

  GlobalKey<FormState> get key => _key;

  List<ImageFileModel> get listFotoGroupObjectUnit => _listFotoGroupObjectUnit;

  bool check() {
    final _form = this._key.currentState;
    if (_form.validate()) {
      if (this._objectUnitSelected.id != "011") {
        if (this._listFotoGroupObjectUnit.isEmpty) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  void actionImageSelected(String actionSelected, int index,BuildContext context) {
    if (actionSelected == TitlePopUpMenuButton.Delete) {
      deleteFile(index);
      this._listFotoGroupObjectUnit.removeAt(index);
      notifyListeners();
    } else if (actionSelected == TitlePopUpMenuButton.Detail) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: this._listFotoGroupObjectUnit[index].imageFile),));
    }
  }

  bool get setData => _setData;

  set setData(bool value) {
    this._setData = value;
    notifyListeners();
  }

  get groupObjectUnitTemp => _groupObjectUnitTemp;

  set groupObjectUnitTemp(GroupObjectUnit value) {
    this._groupObjectUnitTemp = value;
  }

  Future<void> setValueForEdit(
      GroupUnitObjectModel groupUnitObjectModel) async {
    for (int i = 0; i < this._listGroupObjectUnit.length; i++) {
      if (groupUnitObjectModel.groupObjectUnit.id ==
          this._listGroupObjectUnit[i].id) {
        this._groupObjectUnitSelected = this._listGroupObjectUnit[i];
      }
    }

    this._groupObjectUnitTemp = groupUnitObjectModel.groupObjectUnit;
    this._objectUnitTemp = groupUnitObjectModel.objectUnit;
    setDataListObjectUnit();

    for (int i = 0; i < this._listObjectUnit.length; i++) {
      if (groupUnitObjectModel.objectUnit.id == this._listObjectUnit[i].id) {
        this._objectUnitSelected = this._listObjectUnit[i];
      }
    }
    if (groupUnitObjectModel.listImageGroupObjectUnit.isNotEmpty) {
      for (int i = 0;
          i < groupUnitObjectModel.listImageGroupObjectUnit.length;
          i++) {
        this
            ._listFotoGroupObjectUnit
            .add(groupUnitObjectModel.listImageGroupObjectUnit[i]);
      }
      this._lengthListFileImage =
          groupUnitObjectModel.listImageGroupObjectUnit.length;
    }
  }

  get objectUnitTemp => _objectUnitTemp;

  set objectUnitTemp(ObjectUnit value) {
    this._objectUnitTemp = value;
  }

  int get lengthListFileImage => _lengthListFileImage;

  void savePhoto(PickedFile data) async{
    try{
      if (data != null) {
        File _imageFile = File(data.path);
        List<String> split = data.path.split("/");
        String fileName = split[split.length-1];
        File _path = await _imageFile.copy("$globalPath/$fileName");
        _pathFile = _path.path;
        Position _position = await Geolocator().getCurrentPosition(
            desiredAccuracy: LocationAccuracy.bestForNavigation);
        this._listFotoGroupObjectUnit.add(ImageFileModel(_imageFile, _position.latitude, _position.longitude, this._pathFile));
        if (this._autoValidateAddGroupUnitObject)
          this._autoValidateAddGroupUnitObject = false;
        notifyListeners();
      } else {
        return;
      }
//      Uint8List bytes = this._fileDocumentUnitObject['file'].readAsBytesSync();
//      await file.writeAsBytes(bytes);
    }
    catch(e){
      print(e);
    }
  }

  void deleteFile(int index) async{
    final file =  File("${this._listFotoGroupObjectUnit[index].path}");
    await file.delete();
  }

}
