import 'package:intl/intl.dart';

class FormatCurrency{

  NumberFormat _oCcy = NumberFormat("#,##0.00", "en_US");
  String formatCurrency(String value) {
    String _newValue;
    if(value != ""){
      if (value.contains(",")) {
        _newValue = value.replaceAll(",", "");
        if(_newValue.contains("-")){
          _newValue = _newValue.replaceAll("-", "");
        }
      }
      else if(value.contains("-")){
        _newValue = _newValue.replaceAll("-", "");
      }
      else {
        _newValue = value;
      }
      double _number = double.parse(_newValue);
      double _numberFormat = double.parse((_number).toStringAsFixed(2));
      String _currency = _oCcy.format(_numberFormat);
      return _currency;
    }
    else{
      return "";
    }
  }
}