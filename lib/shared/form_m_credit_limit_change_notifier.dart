import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/check_limit_model.dart';
import 'package:ad1ms2_dev/models/check_mpl_activation_model.dart';
import 'package:ad1ms2_dev/models/list_oid_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_detail_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_model.dart';
import 'package:ad1ms2_dev/models/reference_number_cl_model.dart';
import 'package:ad1ms2_dev/models/type_offer_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_reference_number_cl.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_cl_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'constants.dart';

class FormMCreditLimitChangeNotifier with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autoValidate = false;
  bool _flag = false;
  bool _loadData = false;
  int _radioProceedProcess = 0;
  TextEditingController _controllerConsumerName = TextEditingController();
  TextEditingController _controllerTelephoneNumber = TextEditingController();
  TextEditingController _controllerConsumerAddress = TextEditingController();
  TextEditingController _controllerGrading = TextEditingController();
  TextEditingController _controllerOfferType = TextEditingController();
  TextEditingController _controllerReferenceNumberCL = TextEditingController();
  TextEditingController _controllerCustomerType = TextEditingController();
  TextEditingController _controllerNotes = TextEditingController();

  TextEditingController _controllerMaxPH = TextEditingController();
  TextEditingController _controllerMaxInstallment = TextEditingController();
  // TextEditingController _controllerBiddingType = TextEditingController();
  TextEditingController _controllerPortofolio = TextEditingController();
  TextEditingController _controllerTenor = TextEditingController();
  // TextEditingController _controllerReference = TextEditingController();
  TextEditingController _controllerMultidisburseOption = TextEditingController();
  TextEditingController _controllerSearchingFrom = TextEditingController();
  List<TypeOfferModel> _listTypeOffer = ListTypeOffer().listTypeOffer;
  CheckLimitModel _checkLimitSelected;
  TypeOfferModel _typeOfferSelected;
  List<String> _referenceNumberList = [];
  ReferenceNumberCLModel _referenceNumberCLSelected;
  CheckMplActivationModel _checkMplActivationCLSelected;
  String token;
  DbHelper _dbHelper = DbHelper();

  GlobalKey<FormState> get keyForm => _key;

  // Auto Validate
  bool get autoValidate {
    return _autoValidate;
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Load Data
  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  // Flag
  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  CheckMplActivationModel get checkMplActivationCLSelected =>
      _checkMplActivationCLSelected;

  set checkMplActivationCLSelected(CheckMplActivationModel value) {
    this._checkMplActivationCLSelected = value;
    notifyListeners();
  }

  // Notes
  TextEditingController get controllerNotes => _controllerNotes;

  set controllerNotes(TextEditingController value) {
    this._controllerNotes = value;
    notifyListeners();
  }

  // Jenis Nasabah
  TextEditingController get controllerCustomerType => _controllerCustomerType;

  set controllerCustomerType(TextEditingController value) {
    this._controllerCustomerType = value;
    notifyListeners();
  }

  // No Referensi
  TextEditingController get controllerReferenceNumberCL => _controllerReferenceNumberCL;

  set controllerReferenceNumber(TextEditingController value) {
    this._controllerReferenceNumberCL = value;
    notifyListeners();
  }

  // Jenis Penawaran
  TextEditingController get controllerOfferType => _controllerOfferType;

  set controllerOfferType(TextEditingController value) {
    this._controllerOfferType = value;
    notifyListeners();
  }

  // Grading
  TextEditingController get controllerGrading =>
      _controllerGrading;

  set controllerGrading(TextEditingController value) {
    this._controllerGrading = value;
    notifyListeners();
  }

  // Alamat Konsumen
  TextEditingController get controllerConsumerAddress =>
      _controllerConsumerAddress;

  set controllerConsumerAddress(TextEditingController value) {
    this._controllerConsumerAddress = value;
    notifyListeners();
  }

  // Nomer Telepon
  TextEditingController get controllerTelephoneNumber =>
      _controllerTelephoneNumber;

  set controllerTelephoneNumber(TextEditingController value) {
    this._controllerTelephoneNumber = value;
    notifyListeners();
  }

  // Nama Konsumen
  TextEditingController get controllerConsumerName => _controllerConsumerName;

  set controllerConsumerName(TextEditingController value) {
    this._controllerConsumerName = value;
    notifyListeners();
  }

  // Apakah akan melanjutkan proses penginputan atau tidak?
  int get radioProceedProcess => _radioProceedProcess;

  set radioProceedProcess(int value) {
    this._radioProceedProcess = value;
    notifyListeners();
  }

  // Pencarian ke
  TextEditingController get controllerSearchingFrom => _controllerSearchingFrom;

  set controllerSearchingFrom(TextEditingController value) {
    this._controllerSearchingFrom = value;
    notifyListeners();
  }

  // Opsi Multidisburse
  TextEditingController get controllerMultidisburseOption =>
      _controllerMultidisburseOption;

  set controllerMultidisburseOption(TextEditingController value) {
    this._controllerMultidisburseOption = value;
    notifyListeners();
  }

  // No Reference (Informasi Awal)
  // TextEditingController get controllerReference => _controllerReference;
  //
  // set controllerReference(TextEditingController value) {
  //   this._controllerReference = value;
  //   notifyListeners();
  // }

  // Tenor
  TextEditingController get controllerTenor => _controllerTenor;

  set controllerTenor(TextEditingController value) {
    this._controllerTenor = value;
    notifyListeners();
  }

  // Portofolio
  TextEditingController get controllerPortofolio => _controllerPortofolio;

  set controllerPortofolio(TextEditingController value) {
    this._controllerPortofolio = value;
    notifyListeners();
  }

  // Tipe Penawaran
  // TextEditingController get controllerBiddingType => _controllerBiddingType;
  //
  // set controllerBiddingType(TextEditingController value) {
  //   this._controllerBiddingType = value;
  //   notifyListeners();
  // }

  // Max Installment
  TextEditingController get controllerMaxInstallment =>
      _controllerMaxInstallment;

  set controllerMaxInstallment(TextEditingController value) {
    this._controllerMaxInstallment = value;
    notifyListeners();
  }

  // Max PH
  TextEditingController get controllerMaxPH => _controllerMaxPH;

  set controllerMaxPH(TextEditingController value) {
    this._controllerMaxPH = value;
    notifyListeners();
  }

  TypeOfferModel get typeOfferSelected => _typeOfferSelected;

  set typeOfferSelected(TypeOfferModel value) {
    this._typeOfferSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<TypeOfferModel> get listTypeOffer {
    return UnmodifiableListView(this._listTypeOffer);
  }

  // UnmodifiableListView<CheckLimitModel> get listCheckLimit {
  //   return UnmodifiableListView(this._listCheckLimit);
  // }

  CheckLimitModel get checkLimitSelected => _checkLimitSelected;

  set checkLimitSelected(CheckLimitModel value) {
    this._checkLimitSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<String> get referenceNumberList {
    return UnmodifiableListView(this._referenceNumberList);
  }

  ReferenceNumberCLModel get referenceNumberCLSelected => _referenceNumberCLSelected;

  set referenceNumberSelected(ReferenceNumberCLModel value) {
    this._referenceNumberCLSelected = value;
    notifyListeners();
  }

  void searchReferenceNumberCL(BuildContext context) async{
    if(typeOfferSelected.KODE == "01") {
      ReferenceNumberCLModel data = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChangeNotifierProvider(
                  create: (context) => SearchReferenceNumberCLChangeNotifier(),
                  child: SearchReferenceNumberCL())));
      if (data != null) {
        this._referenceNumberCLSelected = data;
        this._controllerReferenceNumberCL.text = "${data.reff_id}";
        notifyListeners();
      } else {
        return;
      }
    } else if(typeOfferSelected.KODE == "03") {
      CheckMplActivationModel data = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChangeNotifierProvider(
                  create: (context) => SearchReferenceNumberCLChangeNotifier(),
                  child: SearchReferenceNumberCL())));
      if (data != null) {
        this._checkMplActivationCLSelected = data;
        this._controllerReferenceNumberCL.text = "${data.reff_id}";
        // disini nanti isi field informasi awal dari response API checkMplActivation
        // ...
        notifyListeners();
      } else {
        return;
      }
    }
  }

  void setValue(ListOidModel data){
    this._controllerConsumerName.text = data.AC_CUST_NAME;
    this._controllerConsumerAddress.text = data.AC_ADDRESS;
  }

  void getToken(BuildContext context) async {
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "grant_type": "client_credentials",
    });

    String _username = 'ad1gate';
    String _password = 'ad1gatePassw0rd';
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$_username:$_password'));

    final _response = await _http.post(
        "http://10.161.16.207:31814/lmeServiceDEV/oauth/token",
        body: _body,
        headers: {
          "Content-Type":"application/x-www-form-urlencoded",
          "Authorization": basicAuth
        }
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isNotEmpty){
        token = _result['access_token'];
        loadData = false;
        getCheckLimit(context);
      }
      else{
        showSnackBar("Token gagal didapatkan");
        loadData = false;
      }
    }
    else{
      showSnackBar("Error get token response ${_response.statusCode}");
      loadData = false;
    }
  }

  void getCheckLimit(BuildContext context) async {
    var date = DateTime.now().toString();
    var splitData = date.split(".");
    var splitData2 = splitData[0].split(" ");
    var reqDate = splitData2[0] + "T" + splitData2[1];
    SharedPreferences _preference = await SharedPreferences.getInstance();

    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "oid" : Provider.of<ListOidChangeNotifier>(context, listen: false).listOIdPersonalSelected.AC_CUST_ID,
      "lme_id" : "",
      "source_reff_id" : "MS2${_preference.getString("username")}",
      "source_channel_id" : "Ad1ms2",
      "disburse_type" : "",
      "product_id" : "",
      "contract_no" : "",
      "installment_amount" : "",
      "ph_amount" : "",
      "voucher_code" : "",
      "req_date" : reqDate,
      "tenor" : "",
      "cust_id" : Provider.of<ListOidChangeNotifier>(context, listen: false).listOIdPersonalSelected.AC_CUST_ID,
      "branch_code" : "",
      "appl_no" : "",
      "flag_book" : "",
      "jenis_kegiatan_usaha" : ""
    });

    final _response = await _http.post(
      "http://10.161.16.207:31814/lmeServiceDEV/InqService/checkLimit",
      body: _body,
      headers: {
        "Content-Type":"application/json",
        "Authorization":"Bearer $token"
      }
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      this._checkLimitSelected = CheckLimitModel(
        _result['reff_id'],
        _result['oid'],
        _result['lme_id'],
        _result['source_reff_id'],
        _result['source_channel_id'],
        _result['disburse_type'],
        _result['product_id'],
        _result['contract_no'],
        _result['voucher_code'],
        _result['tenor'],
        _result['cust_id'],
        _result['elligible_status'],
        _result['branch_code'],
        _result['appl_no'],
        _result['flag_book'],
        _result['jenis_kegiatan_usaha'],
        _result['disbursement_no'],
        _result['response_code'],
        _result['response_desc'],
        _result['response_date'],
        _result['req_date']
      );

      if(_typeOfferSelected.KODE == "01") {
        this._controllerGrading.text = this._checkLimitSelected.elligible_status;
        this._controllerTenor.text = this._checkLimitSelected.tenor.toString();
        this._controllerMultidisburseOption.text = this._checkLimitSelected.disbursement_no.toString();
      }

      loadData = false;
    }
    else{
      showSnackBar("Error get no reference response ${_response.statusCode}");
      loadData = false;
    }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void saveToSQLiteMS2LME(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.insertMS2LME(MS2LMEModel(
      Provider.of<ListOidChangeNotifier>(context, listen: false).listOIdPersonalSelected.AC_CUST_ID,
      null,
      this._checkLimitSelected != null ? this._checkLimitSelected.appl_no : "",
      this._checkLimitSelected != null ? this._checkLimitSelected.elligible_status : "",
      this._checkLimitSelected != null ? this._checkLimitSelected.disburse_type : "", // 5
      this._checkLimitSelected != null ? this._checkLimitSelected.product_id : "",
      null,
      null,
      null,
      this._checkLimitSelected != null ? this._checkLimitSelected.voucher_code : "", // 10
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
      1,
      this._controllerGrading.text != "" ? this._controllerGrading.text : "",
      this._typeOfferSelected != null ? this._typeOfferSelected.KODE : "",
      this._typeOfferSelected != null ? this._typeOfferSelected.DESCRIPTION : "",
      null,
      null,
      null,
      null,
      this._controllerTenor.text != "" ? double.parse(this._controllerTenor.text) : 0,
      this._controllerReferenceNumberCL.text != "" ? this._controllerReferenceNumberCL.text : 0,
      this._controllerMaxInstallment.text != "" ? double.parse(this._controllerMaxInstallment.text) : 0,
      this._controllerPortofolio.text != "" ? this._controllerPortofolio.text : "",
    ));
  }

  void saveToSQLiteMS2LMEDetail(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.insertMS2LMEDetail(MS2LMEDetailModel(
      this._checkLimitSelected != null ? this._checkLimitSelected.source_reff_id : "",
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
      1,
    ));
  }
}
