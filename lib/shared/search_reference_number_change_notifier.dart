import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/reference_number_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchReferenceNumberChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<ReferenceNumberModel> _listReferenceNumber = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ReferenceNumberModel> get listReferenceNumber {
    return UnmodifiableListView(this._listReferenceNumber);
  }

  void getReferenceNumber(String query,BuildContext context) async{
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      this._listReferenceNumber.clear();
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);

      var _body = jsonEncode({
        "refOne": "${Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).rehabTypeSelected.id}",
        "refTwo": "$query"
      });

      final _response = await _http.post(
          "${BaseUrl.urlPublic}no-refference/get_no_reff",
          body: _body,
          headers: {"Content-Type":"application/json"}
      );

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print(_result);
        if(_result.isNotEmpty){
          for(int i = 0; i < _result.length; i++){
            _listReferenceNumber.add(ReferenceNumberModel(_result[i]['noPK'], _result[i]['custName']));
          }
          loadData = false;
        }
        else{
          showSnackBar("No reference tidak ditemukan");
          loadData = false;
        }
      }
      else{
        showSnackBar("Error get no reference response ${_response.statusCode}");
        loadData = false;
      }
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
