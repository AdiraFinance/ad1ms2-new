import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';

class SearchBirthPlaceChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<BirthPlaceModel> _listBirthPlace = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<BirthPlaceModel> get listBirthPlaceModel {
    return UnmodifiableListView(this._listBirthPlace);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getBirthPlace(String query) async{
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      this._listBirthPlace.clear();
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);

      var _body = jsonEncode({
        "P_FLAG":"2",
        "P_SEARCH": query
      });

      final _response = await _http.post(
          "${BaseUrl.urlPublic}api/address/get-alamat-full",
          body: _body,
          headers: {"Content-Type":"application/json"}
      );

      List<BirthPlaceModel> _listTemp = [];
      _listTemp.clear();

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result['data'];
        if(_data.isNotEmpty){
          for(int i = 0; i < _data.length; i++){
            _listTemp.add(BirthPlaceModel(_data[i]['KABKOT_ID'], _data[i]['KABKOT_NAME']));
          }
          var _uniqueKode = _listTemp.map((e) => e.KABKOT_ID.trim()).toSet().toList();
          var _uniqueDescription = _listTemp.map((e) => e.KABKOT_NAME.trim()).toSet().toList();

          for(var i =0; i<_uniqueKode.length; i++){
            BirthPlaceModel _myData = BirthPlaceModel(_uniqueKode[i], _uniqueDescription[i]);
            this._listBirthPlace.add(_myData);
          }
          loadData = false;
        }
        else{
          showSnackBar("Kota tidak ditemukan");
          loadData = false;
        }
      }
      else{
        showSnackBar("Error get kota response ${_response.statusCode}");
        loadData = false;
      }
    }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
