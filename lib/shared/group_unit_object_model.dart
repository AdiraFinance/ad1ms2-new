import 'package:ad1ms2_dev/models/form_m_foto_model.dart';

class GroupUnitObjectModel {
  final GroupObjectUnit groupObjectUnit;
  final ObjectUnit objectUnit;
  final List<ImageFileModel> listImageGroupObjectUnit;

  GroupUnitObjectModel(
      this.groupObjectUnit, this.objectUnit, this.listImageGroupObjectUnit);
}

class GroupObjectUnit {
  final String id;
  final String groupObject;

  GroupObjectUnit(this.id, this.groupObject);
}

class ObjectUnit {
  final String id;
  final String objectUnit;

  ObjectUnit(this.id, this.objectUnit);
}
