import 'dart:collection';
import 'dart:io';
import 'dart:typed_data';

import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/screens/search_document_type.dart';
import 'package:ad1ms2_dev/shared/search_document_type_change_notifier.dart';
import 'package:file_utils/file_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart' as handler;
import 'package:provider/provider.dart';

import '../main.dart';
import 'choose_image_picker.dart';
import 'constants.dart';
import 'date_picker.dart';
import 'document_unit_model.dart';
import 'form_m_foto_change_notif.dart';

class FormMFotoAddDocumentObjectUnit with ChangeNotifier {
  TextEditingController _controllerReceiveDocumentDate = TextEditingController();
  bool _autoValidate = false;
  bool _autoValidateFile = false;
  DateTime _initialDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  String _tglTerimaDocumentTemp;
  String _fileName;
  String _pathFile;
  JenisDocument _jenisDocumentSelected, _jenisDocumentTemp;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  ChooseImagePicker _chooseImagePicker = ChooseImagePicker();
  Map _fileDocumentUnitObject, _fileDocumentUnitObjectTemp;
  List<JenisDocument> _listJenisDocument = [
    JenisDocument("01", "KTP"),
    JenisDocument("02", "STNK"),
    JenisDocument("03", "BPKB"),
    JenisDocument("04", "SERTIPIKAT"),
  ];
  TextEditingController _controllerDocumentType = TextEditingController();
  InfoDocumentTypeModel _documentTypeSelected;

  DateTime get initialDate => _initialDate;

  set initialDate(DateTime value) {
    this._initialDate = value;
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get autoValidateFile => _autoValidateFile;

  set autoValidateFile(bool value) {
    this._autoValidateFile = value;
    notifyListeners();
  }

  JenisDocument get jenisDocumentSelected => _jenisDocumentSelected;

  set jenisDocumentSelected(JenisDocument value) {
    this._jenisDocumentSelected = value;
    notifyListeners();
  }

  GlobalKey<FormState> get key => _key;

  TextEditingController get controllerReceiveDocumentDate =>
      _controllerReceiveDocumentDate;

  UnmodifiableListView<JenisDocument> get listJenisDocument {
    return UnmodifiableListView(this._listJenisDocument);
  }

  void showDatePicker(BuildContext context) async {
    // DatePickerShared _datePicker = DatePickerShared();
    // var _dateSelected = await _datePicker.selectStartDate(context, _initialDate,
    //     canAccessNextDay: true);
    // if (_dateSelected != null) {
    //   _initialDate = _dateSelected;
    //   _controllerReceiveDocumentDate.text = dateFormat.format(_dateSelected);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _dateSelected = await selectDate(context, _initialDate);
    if (_dateSelected != null) {
      _initialDate = _dateSelected;
      _controllerReceiveDocumentDate.text = dateFormat.format(_dateSelected);
      notifyListeners();
    } else {
      return;
    }
  }

  void showBottomSheetChooseFile(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Theme(
            data: ThemeData(fontFamily: "NunitoSans"),
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        _imageFromCamera();
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.photo_camera,
                              color: myPrimaryColor,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Camera",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                  FlatButton(
                      onPressed: () {
                        _fileFormGallery();
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.photo,
                              color: myPrimaryColor,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Gallery",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                ],
              ),
            ),
          );
        });
  }

  Map get fileDocumentUnitObject => _fileDocumentUnitObject;

  set fileDocumentUnitObject(Map value) {
    this._fileDocumentUnitObject = value;
    notifyListeners();
  }

  void _imageFromCamera() async {
    Map _imagePicker = await _chooseImagePicker.imageFromCamera();
    if (_imagePicker != null) {
      _fileDocumentUnitObject = _imagePicker;
      if (this._autoValidateFile) this._autoValidateFile = false;
      notifyListeners();
    } else {
      return;
    }
  }

  void _fileFormGallery() async {
    Map _file = await _chooseImagePicker.fileFromGallery();
    if (_file != null) {
      _fileDocumentUnitObject = _file;
      if (this._autoValidateFile) this._autoValidateFile = false;
      notifyListeners();
    } else {
      return;
    }
  }

  void check(BuildContext context, int flag, int index) {
    final _form = this._key.currentState;
    if (flag == 0) {
      if (!_form.validate()) {
        this._autoValidate = true;
        notifyListeners();
      } else {
        if (this._fileDocumentUnitObject == null) {
          this._autoValidateFile = true;
          notifyListeners();
        } else {
          saveData(context, flag, index);
        }
      }
    } else {
      if (!_form.validate()) {
        this._autoValidate = true;
        notifyListeners();
      } else {
        if (this._fileDocumentUnitObject == null) {
          this._autoValidateFile = true;
          notifyListeners();
        } else {
          saveData(context, flag, index);
        }
      }
    }
  }

  Future<void> setValueForEditDocument(DocumentUnitModel documentUnitModel,int flag) async {
    if(flag != 0){
      for (int i = 0; i < _listJenisDocument.length; i++) {
        if (documentUnitModel.jenisDocument.id == _listJenisDocument[i].id) {
          _jenisDocumentSelected = _listJenisDocument[i];
          _jenisDocumentTemp = _listJenisDocument[i];
        }
      }

      this._initialDate = documentUnitModel.dateTime;
      this._controllerReceiveDocumentDate.text =
          dateFormat.format(documentUnitModel.dateTime);
      this._tglTerimaDocumentTemp = this._controllerReceiveDocumentDate.text;
      this._fileDocumentUnitObject = documentUnitModel.fileDocumentUnitObject;
      this._fileDocumentUnitObjectTemp = this._fileDocumentUnitObject;
      this._fileName = this._fileDocumentUnitObject['file_name'];
      this._pathFile = documentUnitModel.path;
    }
  }

  get jenisDocumentTemp => _jenisDocumentTemp;

  String get fileName => _fileName;

  String get pathFile => _pathFile;

  String get tglTerimaDocumentTemp => _tglTerimaDocumentTemp;

  get fileDocumentUnitObjectTemp => _fileDocumentUnitObjectTemp;

  void searchDocumentType(BuildContext context) async{
    InfoDocumentTypeModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchDocumentTypeChangeNotifier(),
                child: SearchDocumentType())));
    if (data != null) {
      this._documentTypeSelected = data;
      this._controllerDocumentType.text = "${data.docTypeId} - ${data.docTypeName}";
      notifyListeners();
    } else {
      return;
    }
  }

  InfoDocumentTypeModel get documentTypeSelected => _documentTypeSelected;

  set documentTypeSelected(InfoDocumentTypeModel value) {
    this._documentTypeSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerDocumentType => _controllerDocumentType;

 void saveData(BuildContext context, int flag, int index) async{
   try{
     File _myFile = this._fileDocumentUnitObject['file'];
     File _path = await _myFile.copy("$globalPath/${this._fileDocumentUnitObject['file_name']}");
     _pathFile = _path.path;
     print(_pathFile);
     Position _position = await Geolocator().getCurrentPosition(
         desiredAccuracy: LocationAccuracy.bestForNavigation);
     if(flag == 0){
       Provider.of<FormMFotoChangeNotifier>(context, listen: false)
           .addListDocument(
           DocumentUnitModel(this._jenisDocumentSelected,
               this._initialDate, this._fileDocumentUnitObject, this._pathFile, _position.longitude, _position.longitude),
           context);
     } else {
       Provider.of<FormMFotoChangeNotifier>(context, listen: false)
           .updateListDocumentObjectUnit(
           DocumentUnitModel(this._jenisDocumentSelected,
               this._initialDate, this._fileDocumentUnitObject, this._pathFile, _position.longitude, _position.longitude),
           context,
           index);
     }
//      Uint8List bytes = this._fileDocumentUnitObject['file'].readAsBytesSync();
//      await file.writeAsBytes(bytes);
   }
   catch(e){
     print(e);
   }
 }

 void deleteFile() async{
   final file =  File("$globalPath/${this._fileDocumentUnitObjectTemp['file_name']}");
   await file.delete();
 }
}
