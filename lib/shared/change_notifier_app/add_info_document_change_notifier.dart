import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/info_document_detail_model.dart';
import 'package:ad1ms2_dev/models/info_document_model.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:ad1ms2_dev/screens/search_document_type.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_document_type_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import '../choose_image_picker.dart';
import '../constants.dart';
import '../date_picker.dart';

class AddInfoDocumentChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    bool _autoValidate = false;
    bool _autoValidateFile = false;
    List<InfoDocumentModel> _listDocument = [];
    InfoDocumentDetailModel _documentDetailSelected;
    InfoDocumentTypeModel _documentTypeSelected;
    TextEditingController _controllerDocumentType = TextEditingController();
    TextEditingController _controllerDateDocument = TextEditingController();
    DateTime _initialDateDocument = DateTime(dateNow.year, dateNow.month, dateNow.day);
    ChooseImagePicker _chooseImagePicker = ChooseImagePicker();
    Map _fileDocumentUnitObject;
    String _pathFile;

    List<InfoDocumentModel> get listDocument => _listDocument;

    // List<InfoDocumentTypeModel> _listDocumentType = [
    //     InfoDocumentTypeModel("A01", "KTP PEMOHON"),
    //     InfoDocumentTypeModel("A02", "KARTU KELUARGA"),
    //     InfoDocumentTypeModel("A03", "SKCK")
    // ];
    List<InfoDocumentTypeModel> _listDocumentType = [];

    InfoDocumentTypeModel get documentTypeSelected {
        return this._documentTypeSelected;
    }

    set documentTypeSelected(InfoDocumentTypeModel value) {
        this._documentTypeSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<InfoDocumentTypeModel> get listDocumentType {
        return UnmodifiableListView(this._listDocumentType);
    }

    InfoDocumentDetailModel get documentDetailSelected => _documentDetailSelected;

    set documentDetailSelected(InfoDocumentDetailModel value) {
        this._documentDetailSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerDateDocument {
        return this._controllerDateDocument;
    }

    set controllerDateDocument(TextEditingController value) {
        this._controllerDateDocument = value;
        this.notifyListeners();
    }

    Map get fileDocumentUnitObject => _fileDocumentUnitObject;

    set fileDocumentUnitObject(Map value) {
        this._fileDocumentUnitObject = value;
        notifyListeners();
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    bool get autoValidateFile => _autoValidateFile;

    set autoValidateFile(bool value) {
        this._autoValidateFile = value;
        notifyListeners();
    }

    TextEditingController get controllerDocumentType => _controllerDocumentType;

    set controllerDocumentType(TextEditingController value) {
        this._controllerDocumentType = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    void selectDocumentDate(BuildContext context) async {
        // DatePickerShared _datePickerShared = DatePickerShared();
        // var _datePickerSelected = await _datePickerShared.selectStartDate(
        //     context, this._initialDateDocument,
        //     canAccessNextDay: false);
        // if (_datePickerSelected != null) {
        //     this.controllerDateDocument.text = dateFormat.format(_datePickerSelected);
        //     this._initialDateDocument = _datePickerSelected;
        //     notifyListeners();
        // } else {
        //     return;
        // }
        var _datePickerSelected = await selectDate(context, _initialDateDocument);
        if (_datePickerSelected != null) {
            this.controllerDateDocument.text = dateFormat.format(_datePickerSelected);
            this._initialDateDocument = _datePickerSelected;
            notifyListeners();
        } else {
            return;
        }
    }

    void _imageFromCamera() async {
        Map _imagePicker = await _chooseImagePicker.imageFromCamera();
        if (_imagePicker != null) {
            _fileDocumentUnitObject = _imagePicker;
            if (this._autoValidateFile) this._autoValidateFile = false;
            Position _position = await Geolocator().getCurrentPosition(
                desiredAccuracy: LocationAccuracy.bestForNavigation);
            _fileDocumentUnitObject['latitude'] = _position.latitude;
            _fileDocumentUnitObject['longitude'] = _position.longitude;
            print(_fileDocumentUnitObject);
            notifyListeners();
        } else {
            return;
        }
    }

    void _fileFormGallery() async {
        Map _file = await _chooseImagePicker.fileFromGallery();
        if (_file != null) {
            _fileDocumentUnitObject = _file;
            if (this._autoValidateFile) this._autoValidateFile = false;
            Position _position = await Geolocator().getCurrentPosition(
                desiredAccuracy: LocationAccuracy.bestForNavigation);
            _fileDocumentUnitObject['latitude'] = _position.latitude;
            _fileDocumentUnitObject['longitude'] = _position.longitude;
            notifyListeners();
        } else {
            return;
        }
    }

    String splitTypeFile(String file){
        List split = file.split(".");
        String typeFile = split[split.length-1];
        return typeFile.replaceAll("'", "");
    }

    void showBottomSheetChooseFile(context) {
        showModalBottomSheet(context: context,
            builder: (BuildContext bc) {
                return Theme(
                    data: ThemeData(fontFamily: "NunitoSans"),
                    child: Container(
                        child: new Wrap(
                            children: <Widget>[
                                FlatButton(
                                    onPressed: () {
                                        _imageFromCamera();
                                        Navigator.pop(context);
                                    },
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                            Icon(Icons.photo_camera, color: myPrimaryColor, size: 22.0),
                                            SizedBox(width: 12.0),
                                            Text("Camera",style: TextStyle(fontSize: 18.0),
                                            )
                                        ])),
                                FlatButton(
                                    onPressed: () {
                                        _fileFormGallery();
                                        Navigator.pop(context);
                                    },
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                            Icon(Icons.photo, color: myPrimaryColor, size: 22.0),
                                            SizedBox(width: 12.0),
                                            Text("Gallery",style: TextStyle(fontSize: 18.0),
                                            )
                                        ])
                                ),
                            ],
                        ),
                    ),
                );
            });
    }

    void check(BuildContext context, int flag, int index) async{
        final _form = _key.currentState;
        if (flag == 0) {
            if (this._fileDocumentUnitObject == null) {
                this._autoValidateFile = true;
                notifyListeners();
            }
            if (_form.validate()) {
                File _myFile = this._fileDocumentUnitObject['file'];
                File _path = await _myFile.copy("$globalPath/${this._fileDocumentUnitObject['file_name']}");
                _pathFile = _path.path;
                _documentDetailSelected = InfoDocumentDetailModel(_fileDocumentUnitObject['file_name'], _fileDocumentUnitObject['file']);
                Provider.of<InfoDocumentChangeNotifier>(context, listen: false)
                    .addListInfoDocument(InfoDocumentModel(
                    this._documentTypeSelected,
                    // InfoDocumentTypeModel("a", "b", 1,2,3),
                    // this._fileDocumentUnitObject['file_name'],
                    this._documentDetailSelected,
                    this._controllerDateDocument.text,
                    _pathFile,
                    _fileDocumentUnitObject['latitude'],
                    _fileDocumentUnitObject['longitude'])
                );
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
//            if (_form.validate()) {
//                Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false)
//                    .updateListInfoAdditionalInsurance(AdditionalInsuranceModel(
//                    this._insuranceTypeSelected,
//                    this._companySelected,
//                    this._productSelected,), context, index);
//                Navigator.pop(context);
//            } else {
//                autoValidate = true;
//            }
        }
    }

    Future<void> setValueForEditDocument(InfoDocumentModel data) async {
//        for (int i = 0; i < this._listDocumentType.length; i++) {
//            if (data.documentType.docTypeId == this._listDocumentType[i].docTypeId) {
//                this._documentTypeSelected = this._listDocumentType[i];
//            }
//        }
        this._documentTypeSelected = data.documentType;
        this._controllerDocumentType.text = "${data.documentType.docTypeId} - ${data.documentType.docTypeName}";
        this._controllerDateDocument.text = data.date;
        this._documentDetailSelected = data.documentDetail;
    }

    void searchDocumentType(BuildContext context) async{
        InfoDocumentTypeModel data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchDocumentTypeChangeNotifier(),
                    child: SearchDocumentType())));
        if (data != null) {
            this._documentTypeSelected = data;
            this._controllerDocumentType.text = "${data.docTypeId} - ${data.docTypeName}";
            notifyListeners();
        } else {
            return;
        }
    }
}