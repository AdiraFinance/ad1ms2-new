import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/brand_object_model.dart';
import 'package:ad1ms2_dev/models/brand_type_model_genre_model.dart';
import 'package:ad1ms2_dev/models/certificate_type_model.dart';
import 'package:ad1ms2_dev/models/collateral_type_model.dart';
import 'package:ad1ms2_dev/models/collateral_usage_model.dart';
import 'package:ad1ms2_dev/models/floor_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/foundation_type_model.dart';
import 'package:ad1ms2_dev/models/group_object_model.dart';
import 'package:ad1ms2_dev/models/model_object_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_oto_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_prop_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/object_model.dart';
import 'package:ad1ms2_dev/models/object_type_model.dart';
import 'package:ad1ms2_dev/models/object_usage_model.dart';
import 'package:ad1ms2_dev/models/product_type_model.dart';
import 'package:ad1ms2_dev/models/property_type_model.dart';
import 'package:ad1ms2_dev/models/street_type_model.dart';
import 'package:ad1ms2_dev/models/type_of_roof_model.dart';
import 'package:ad1ms2_dev/models/wall_type_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/screens/map_page.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/screens/search_brand_object.dart';
import 'package:ad1ms2_dev/screens/search_collateral_type.dart';
import 'package:ad1ms2_dev/screens/search_collateral_usage.dart';
import 'package:ad1ms2_dev/screens/search_group_object.dart';
import 'package:ad1ms2_dev/screens/search_model_object.dart';
import 'package:ad1ms2_dev/screens/search_object.dart';
import 'package:ad1ms2_dev/screens/search_object_type.dart';
import 'package:ad1ms2_dev/screens/search_product_type.dart';
import 'package:ad1ms2_dev/screens/search_usage_object.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/search_collateral_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:ad1ms2_dev/shared/resource/address_type_resource.dart';
import 'package:ad1ms2_dev/shared/resource/get_product_matrix.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_brand_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_collateral_usage_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_group_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_model_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_usage_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../search_kelurahan_change_notif.dart';
import '../search_object_change_notifier.dart';
import '../search_product_type_change_notifier.dart';

class InformationCollateralChangeNotifier with ChangeNotifier{
  CollateralTypeModel _collateralTypeModel;
  TextEditingController _controllerCollateralType = TextEditingController();
  FormatCurrency _formatCurrency = FormatCurrency();
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');
  String _prodMatrixId;
  DbHelper _dbHelper = DbHelper();
  bool _loadData = false;

  // Jenis Atap
  TypeOfRoofModel _typeOfRoofModelSelected;

  TypeOfRoofModel get typeOfRoodModelSelected => _typeOfRoofModelSelected;

  set typeOfRoodModelSelected(TypeOfRoofModel value) {
    this._typeOfRoofModelSelected = value;
    notifyListeners();
  }

  // Jenis Dinding
  WallTypeModel _wallTypeModelSelected;

  WallTypeModel get wallTypeModelSelected => _wallTypeModelSelected;

  set wallTypeModelSelected(WallTypeModel value) {
    this._wallTypeModelSelected = value;
    notifyListeners();
  }

  // Jenis Lantai
  FloorTypeModel _floorTypeModelSelected;

  FloorTypeModel get floorTypeModelSelected => _floorTypeModelSelected;

  set floorTypeModelSelected(FloorTypeModel value) {
    this._floorTypeModelSelected = value;
    notifyListeners();
  }

  // Jenis Fondasi
  FoundationTypeModel _foundationTypeModelSelected;

  FoundationTypeModel get foundationTypeModelSelected => _foundationTypeModelSelected;

  set foundationTypeModelSelected(FoundationTypeModel value) {
    this._foundationTypeModelSelected = value;
    notifyListeners();
  }

  // Tipe Jalan
  StreetTypeModel _streetTypeModelSelected;

  StreetTypeModel get streetTypeModelSelected => _streetTypeModelSelected;

  set streetTypeModelSelected(StreetTypeModel value) {
    this._streetTypeModelSelected = value;
    notifyListeners();
  }

  //automotive
  TextEditingController _controllerGroupObject = TextEditingController();
  TextEditingController _controllerObject = TextEditingController();
  TextEditingController _controllerTypeProduct = TextEditingController();
  TextEditingController _controllerBrandObject = TextEditingController();
  TextEditingController _controllerObjectType = TextEditingController();
  TextEditingController _controllerModelObject = TextEditingController();
  TextEditingController _controllerUsageObjectModel = TextEditingController();
  TextEditingController _controllerUsageCollateralOto = TextEditingController();
  TextEditingController _controllerPoliceNumber = TextEditingController();
  TextEditingController _controllerFrameNumber = TextEditingController();
  TextEditingController _controllerMachineNumber = TextEditingController();
  TextEditingController _controllerBPKPNumber = TextEditingController();
  TextEditingController _controllerDpGuarantee = TextEditingController();

  TextEditingController _controllerGradeUnit = TextEditingController();
  TextEditingController _controllerFasilitasUTJ = TextEditingController();
  TextEditingController _controllerNamaBidder = TextEditingController();
  TextEditingController _controllerHargaJualShowroom = TextEditingController();
  TextEditingController _controllerPerlengkapanTambahan = TextEditingController();
  TextEditingController _controllerMPAdira = TextEditingController();
  TextEditingController _controllerHargaPasar = TextEditingController();
  TextEditingController _controllerRekondisiFisik = TextEditingController();
  TextEditingController _controllerPHMaxAutomotive = TextEditingController();
  TextEditingController _controllerTaksasiPriceAutomotive = TextEditingController();
  TextEditingController _controllerIdentityNumberAuto = TextEditingController();
  TextEditingController _controllerNameOnCollateralAuto = TextEditingController();
  TextEditingController _controllerBirthDateAuto = TextEditingController();
  TextEditingController _controllerBirthPlaceValidWithIdentityAuto = TextEditingController();
  TextEditingController _controllerBirthPlaceValidWithIdentityLOVAuto = TextEditingController();

  TextEditingController get controllerGradeUnit => _controllerGradeUnit;
  TextEditingController get controllerFasilitasUTJ => _controllerFasilitasUTJ;
  TextEditingController get controllerNamaBidder => _controllerNamaBidder;
  TextEditingController get controllerHargaJualShowroom => _controllerHargaJualShowroom;
  TextEditingController get controllerPerlengkapanTambahan => _controllerPerlengkapanTambahan;
  TextEditingController get controllerMPAdira => _controllerMPAdira;
  TextEditingController get controllerHargaPasar => _controllerHargaPasar;
  TextEditingController get controllerRekondisiFisik => _controllerRekondisiFisik;
  TextEditingController get controllerPHMaxAutomotive => _controllerPHMaxAutomotive;
  TextEditingController get controllerTaksasiPriceAutomotive => _controllerTaksasiPriceAutomotive;
  TextEditingController get controllerIdentityNumberAuto => _controllerIdentityNumberAuto;
  TextEditingController get controllerNameOnCollateralAuto => _controllerNameOnCollateralAuto;
  TextEditingController get controllerBirthDateAuto => _controllerBirthDateAuto;
  TextEditingController get controllerBirthPlaceValidWithIdentityAuto => _controllerBirthPlaceValidWithIdentityAuto;
  TextEditingController get controllerBirthPlaceValidWithIdentityLOVAuto => _controllerBirthPlaceValidWithIdentityLOVAuto;
  TextEditingController get controllerPoliceNumber => _controllerPoliceNumber;
  TextEditingController get controllerFrameNumber => _controllerFrameNumber;
  TextEditingController get controllerMachineNumber => _controllerMachineNumber;
  TextEditingController get controllerBPKPNumber => _controllerBPKPNumber;
  TextEditingController get controllerDpGuarantee => _controllerDpGuarantee;

  //property
  TextEditingController _controllerIdentityType = TextEditingController();
  TextEditingController _controllerIdentityNumber = TextEditingController();
  TextEditingController _controllerNameOnCollateral = TextEditingController();
  TextEditingController _controllerBirthPlaceValidWithIdentity1 = TextEditingController();
  TextEditingController _controllerBirthPlaceValidWithIdentityLOV = TextEditingController();
  TextEditingController _controllerCertificateNumber = TextEditingController();
  TextEditingController _controllerBuildingArea = TextEditingController();
  TextEditingController _controllerSurfaceArea = TextEditingController();

  TextEditingController _controllerTaksasiPrice = TextEditingController();
  TextEditingController _controllerSifatJaminan = TextEditingController();
  TextEditingController _controllerBuktiKepemilikan = TextEditingController();
  TextEditingController _controllerNamaPemegangHak = TextEditingController();
  TextEditingController _controllerNoSuratUkur = TextEditingController();
  TextEditingController _controllerDPJaminan = TextEditingController();
  TextEditingController _controllerPHMax = TextEditingController();
  TextEditingController _controllerJarakFasumPositif = TextEditingController();
  TextEditingController _controllerJarakFasumNegatif = TextEditingController();
  TextEditingController _controllerHargaTanah = TextEditingController();
  TextEditingController _controllerHargaNJOP = TextEditingController();
  TextEditingController _controllerHargaBangunan = TextEditingController();
  TextEditingController _controllerJumlahRumahDalamRadius = TextEditingController();
  TextEditingController _controllerMasaHakBerlaku = TextEditingController();
  TextEditingController _controllerNoIMB = TextEditingController();
  TextEditingController _controllerLuasBangunanIMB = TextEditingController();
  TextEditingController _controllerLTV = TextEditingController();
  TextEditingController _controllerBirthDateProp = TextEditingController();


  TextEditingController get controllerTaksasiPrice => _controllerTaksasiPrice;
  TextEditingController get controllerSifatJaminan => _controllerSifatJaminan;
  TextEditingController get controllerBuktiKepemilikan =>_controllerBuktiKepemilikan;
  TextEditingController get controllerNamaPemegangHak =>_controllerNamaPemegangHak;
  TextEditingController get controllerNoSuratUkur => _controllerNoSuratUkur;
  TextEditingController get controllerDPJaminan => _controllerDPJaminan;
  TextEditingController get controllerPHMax => _controllerPHMax;
  TextEditingController get controllerJarakFasumPositif =>_controllerJarakFasumPositif;
  TextEditingController get controllerJarakFasumNegatif =>_controllerJarakFasumNegatif;
  TextEditingController get controllerHargaTanah => _controllerHargaTanah;
  TextEditingController get controllerHargaNJOP => _controllerHargaNJOP;
  TextEditingController get controllerHargaBangunan => _controllerHargaBangunan;
  TextEditingController get controllerJumlahRumahDalamRadius =>_controllerJumlahRumahDalamRadius;
  TextEditingController get controllerMasaHakBerlaku =>_controllerMasaHakBerlaku;
  TextEditingController get controllerNoIMB =>_controllerNoIMB;
  TextEditingController get controllerLuasBangunanIMB =>_controllerLuasBangunanIMB;
  TextEditingController get controllerLTV =>_controllerLTV;
  TextEditingController get controllerBirthDateProp => _controllerBirthDateProp;

  TextEditingController _controllerCertificateReleaseDate = TextEditingController();
  TextEditingController _controllerCertificateReleaseYear = TextEditingController();
  TextEditingController _controllerDateOfMeasuringLetter = TextEditingController();
  TextEditingController _controllerCertificateOfMeasuringLetter = TextEditingController();
  TextEditingController _controllerDateOfIMB = TextEditingController();
  TextEditingController _controllerUsageCollateralProperty = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProv = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerAddressFromMap = TextEditingController();

  //automotive
  int _radioValueIsCollaNameSameWithApplicantOto = 1;
  int _radioValueIsCollateralSameWithUnitOto = 1;
  int _radioValueYellowPlat = 0;
  int _radioValueBuiltUpNonATPM = 0;
  int _radioValueWorthyOrUnworthy = 0;
  int _radioValueForAllUnitOto = 1;
  IdentityModel _identityTypeSelectedAuto;
  GroupObjectModel _groupObjectSelected;
  ObjectModel _objectSelected;
  ProductTypeModel _productTypeSelected;
  BrandObjectModel _brandObjectSelected;
  ObjectTypeModel _objectTypeSelected;
  ModelObjectModel _modelObjectSelected;
  ObjectUsageModel _objectUsageSelected;
  CollateralUsageModel _collateralUsageOtoSelected;
  String _yearProductionSelected;
  String _yearRegistrationSelected;

  //property
  int _radioValueIsCollateralSameWithApplicantProperty = 1;
  int _radioValueAccessCar = 0;
  int _radioValueForAllUnitProperty = 0;
  CertificateTypeModel _certificateTypeSelected;
  PropertyTypeModel _propertyTypeSelected;
  DateTime _initialDateCertificateRelease = DateTime(dateNow.year,dateNow.month,dateNow.day);
  DateTime _initialDateOfMeasuringLetter = DateTime(dateNow.year,dateNow.month,dateNow.day);
  DateTime _initialDateOfIMB = DateTime(dateNow.year,dateNow.month,dateNow.day);
  TypeOfRoofModel _typeOfRoofSelected;
  WallTypeModel _wallTypeSelected;
  FoundationTypeModel _foundationTypeSelected;
  FloorTypeModel _floorTypeSelected;
  StreetTypeModel _streetTypeSelected;
  CollateralUsageModel _collateralUsagePropertySelected;
  JenisAlamatModel _addressTypeSelected;
  bool _enableTf = true;
  KelurahanModel _kelurahanModel;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _keyAuto = GlobalKey<FormState>();
  GlobalKey<FormState> _keyProp = GlobalKey<FormState>();
  bool _autoValidateAuto = false;
  bool _autoValidateProp = false;

  bool _flag = false;
  BirthPlaceModel _birthPlaceSelected;
  BirthPlaceModel _birthPlaceAutoSelected;
  Map _addressFromMap;

  List<IdentityModel> _listIdentity = IdentityType().lisIdentityModel;
  IdentityModel _identityModel;
//  [
//    IdentityModel("01", "KTP"),
//    IdentityModel("03", "PASSPORT"),
//    IdentityModel("04", "SIM"),
//    IdentityModel("05", "KTP Sementara"),
//    IdentityModel("06", "Resi KTP"),
//    IdentityModel("07", "Ket. Domisili"),
//  ];

  // Load Data
  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  List<String> _listProductionYear = [];
  List<String> _listRegistrationYear = [];

  List<CertificateTypeModel> _listCertificateType = [
    CertificateTypeModel("001", "SHM"),
    CertificateTypeModel("002", "SHGB"),
    CertificateTypeModel("003", "HAK PAKAI"),
    CertificateTypeModel("004", "HAK GUNA USAHA"),
  ];

  List<PropertyTypeModel> _listPropertyType = [
    PropertyTypeModel("R63", "RUMAH"),
    PropertyTypeModel("434", "RUKAN"),
    PropertyTypeModel("SX9", "RUKO"),
    PropertyTypeModel("2AR", "APARTEMEN"),
    PropertyTypeModel("2VP", "RUSUN"),
  ];

  List<TypeOfRoofModel> _listTypeOfRoof = [
    // TypeOfRoofModel("001", "JENIS ATAP 1"),
    // TypeOfRoofModel("002", "JENIS ATAP 2"),
    // TypeOfRoofModel("003", "JENIS ATAP 3")
  ];

  List<WallTypeModel> _listWallType = [
    // WallTypeModel("001", "JENIS DINDING A"),
    // WallTypeModel("002", "JENIS DINDING B"),
    // WallTypeModel("003", "JENIS DINDING C")
  ];

  List<FoundationTypeModel> _listFoundationType = [
    // FoundationTypeModel("001", "JENIS PONDASI 1"),
    // FoundationTypeModel("002", "JENIS PONDASI 2"),
    // FoundationTypeModel("003", "JENIS PONDASI 3")
  ];

  List<FloorTypeModel> _listFloorType = [
    // FloorTypeModel("001", "JENIS LANTAI A"),
    // FloorTypeModel("002", "JENIS LANTAI B"),
    // FloorTypeModel("003", "JENIS LANTAI C")
  ];

  List<StreetTypeModel> _listStreetType = [
    StreetTypeModel("001", "TIPE JALAN 1"),
    StreetTypeModel("002", "TIPE JALAN 2"),
    StreetTypeModel("003", "TIPE JALAN 3")
  ];

  List<JenisAlamatModel> _listAddressType = [];
  List<AddressModel> _listAddress = [];

  // Kelurahan
  KelurahanModel get kelurahanSelected => _kelurahanModel;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanModel = value;
    notifyListeners();
  }

  bool get autoValidateAuto => _autoValidateAuto;

  set autoValidateAuto(bool value) {
    this._autoValidateAuto = value;
    notifyListeners();
  }

  bool get autoValidateProp => _autoValidateProp;

  set autoValidateProp(bool value) {
    this._autoValidateProp = value;
    notifyListeners();
  }

  CollateralTypeModel get collateralTypeModel => _collateralTypeModel;

  set collateralTypeModel(CollateralTypeModel value) {
    this._collateralTypeModel = value;
  }

  int get radioValueIsCollateralSameWithUnitOto =>
      _radioValueIsCollateralSameWithUnitOto;

  set radioValueIsCollateralSameWithUnitOto(int value) {
    this._radioValueIsCollateralSameWithUnitOto = value;
    notifyListeners();
  }

  IdentityModel get identityTypeSelectedAuto => _identityTypeSelectedAuto;

  set identityTypeSelectedAuto(IdentityModel value) {
    this._identityTypeSelectedAuto = value;
    notifyListeners();
  }

  TextEditingController get controllerCollateralType => _controllerCollateralType;
  TextEditingController get controllerGroupObject => _controllerGroupObject;
  TextEditingController get controllerObject => _controllerObject;
  TextEditingController get controllerTypeProduct => _controllerTypeProduct;
  TextEditingController get controllerBrandObject => _controllerBrandObject;
  TextEditingController get controllerObjectType => _controllerObjectType;
  TextEditingController get controllerModelObject => _controllerModelObject;
  TextEditingController get controllerUsageObjectModel => _controllerUsageObjectModel;
  TextEditingController get controllerUsageCollateralOto => _controllerUsageCollateralOto;

  UnmodifiableListView<IdentityModel> get listIdentityType => UnmodifiableListView(_listIdentity);

  GroupObjectModel get groupObjectSelected => _groupObjectSelected;

  set groupObjectSelected(GroupObjectModel value) {
    this._groupObjectSelected = value;
  }

  ObjectModel get objectSelected => _objectSelected;

  set objectSelected(ObjectModel value) {
    _objectSelected = value;
    notifyListeners();
  }

  ProductTypeModel get productTypeSelected => _productTypeSelected;

  set productTypeSelected(ProductTypeModel value) {
    _productTypeSelected = value;
    notifyListeners();
  }

  BrandObjectModel get brandObjectSelected => _brandObjectSelected;

  set brandObjectSelected(BrandObjectModel value) {
    this._brandObjectSelected = value;
  }

  ObjectTypeModel get objectTypeSelected => _objectTypeSelected;

  set objectTypeSelected(ObjectTypeModel value) {
    this._objectTypeSelected = value;
  }

  ModelObjectModel get modelObjectSelected => _modelObjectSelected;

  set modelObjectSelected(ModelObjectModel value) {
    this._modelObjectSelected = value;
  }

  ObjectUsageModel get objectUsageSelected => _objectUsageSelected;

  set objectUsageSelected(ObjectUsageModel value) {
    this._objectUsageSelected = value;
  }

  String get yearProductionSelected => _yearProductionSelected;

  set yearProductionSelected(String value) {
    this._yearProductionSelected = value;
    notifyListeners();
  }

  String get yearRegistrationSelected => _yearRegistrationSelected;

  set yearRegistrationSelected(String value) {
    this._yearRegistrationSelected = value;
    notifyListeners();
  }

  int get radioValueYellowPlat => _radioValueYellowPlat;

  set radioValueYellowPlat(int value) {
    this._radioValueYellowPlat = value;
    notifyListeners();
  }

  int get radioValueBuiltUpNonATPM => _radioValueBuiltUpNonATPM;

  set radioValueBuiltUpNonATPM(int value) {
    this._radioValueBuiltUpNonATPM = value;
    notifyListeners();
  }

  int get radioValueWorthyOrUnworthy => _radioValueWorthyOrUnworthy;

  set radioValueWorthyOrUnworthy(int value) {
    this._radioValueWorthyOrUnworthy = value;
    notifyListeners();
  }

  CollateralUsageModel get collateralUsageOtoSelected => _collateralUsageOtoSelected;

  set collateralUsageOtoSelected(CollateralUsageModel value) {
    this._collateralUsageOtoSelected = value;
    notifyListeners();
  }

  int get radioValueForAllUnitOto => _radioValueForAllUnitOto;

  set radioValueForAllUnitOto(int value) {
    this._radioValueForAllUnitOto = value;
    notifyListeners();
  }

  bool get enableTf => _enableTf;

  set enableTf(bool value) {
    this._enableTf = value;
    notifyListeners();
  }

  int get radioValueIsCollaNameSameWithApplicantOto =>
      _radioValueIsCollaNameSameWithApplicantOto;

  set radioValueIsCollaNameSameWithApplicantOto(int value) {
    this._radioValueIsCollaNameSameWithApplicantOto = value;
    notifyListeners();
  }

  UnmodifiableListView<String> get listProductionYear => UnmodifiableListView(_listProductionYear);

  UnmodifiableListView<String> get listRegistrationYear => UnmodifiableListView(_listRegistrationYear);

  void getTypeOfRoof(BuildContext context) async {
    this._typeOfRoofModelSelected = this._listTypeOfRoof[0];
    _listTypeOfRoof.clear();
    this._typeOfRoofSelected = null;
    loadData = true;

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/colaproperties/get-jenis-atap");
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listTypeOfRoof.add(TypeOfRoofModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      loadData = false;
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void getWallType(BuildContext context) async {
    // this._wallTypeModelSelected = this._listWallType[0];
    _listWallType.clear();
    this._wallTypeSelected = null;
    loadData = true;

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/colaproperties/get-jenis-dinding");
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listWallType.add(WallTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      loadData = false;
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void getFloorType(BuildContext context) async {
    // this._floorTypeModelSelected = this._listFloorType[0];
    _listFloorType.clear();
    this._floorTypeSelected = null;
    loadData = true;

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/colaproperties/get-jenis-lantai");
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listFloorType.add(FloorTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      loadData = false;
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void getFoundationType(BuildContext context) async {
    // this._foundationTypeModelSelected = this._listFoundationType[0];
    _listFoundationType.clear();
    this._foundationTypeSelected = null;
    loadData = true;

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/colaproperties/get-jenis-fondasi");
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listFoundationType.add(FoundationTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      loadData = false;
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void getStreetType(BuildContext context) async {
    // this._streetTypeModelSelected = this._listStreetType[0];
    _listStreetType.clear();
    this._streetTypeSelected = null;
    loadData = true;

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/jenis-rehab/get_street_type");
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listStreetType.add(StreetTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi'].trim()));
      }
      loadData = false;
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void searchCollateralType(BuildContext context) async{
    CollateralTypeModel _data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchCollateralTypeChangeNotifier(),
                child: SearchCollateralType()
            )
        )
    );
    if(_data != null){
      clearDataInfoCollateral();
      collateralTypeModel = _data;
      this._controllerCollateralType.text = "${_data.id} - ${_data.name}";
      if(_data.id == "001"){
        _addYearProductionAndRegistration();
      }
      notifyListeners();
    }
  }

  void searchKelurahan(BuildContext context) async {
    KelurahanModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchKelurahanChangeNotif(),
                child: SearchKelurahan(
//                  flag: 0,
                ))));
    if (data != null) {
      kelurahanSelected = data;
      this._controllerKelurahan.text = data.KEL_NAME;
      this._controllerKecamatan.text = data.KEC_NAME;
      this._controllerKota.text = data.KABKOT_NAME;
      this._controllerProv.text = data.PROV_NAME;
      this._controllerPostalCode.text = data.ZIPCODE;
      notifyListeners();
    } else {
      return;
    }
  }

  void _addYearProductionAndRegistration(){
    this._listProductionYear.clear();
    this._listRegistrationYear.clear();
    int _yearNow = DateTime.now().year;
    int _lastYear = _yearNow - 10;
    for(int i=_lastYear; i <= _yearNow; i++){
      this._listProductionYear.add(i.toString());
      this._listRegistrationYear.add(i.toString());
    }
    notifyListeners();
  }

  void setSameWithApplicantAutomotive(BuildContext context){
    var _provider = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    if(this._radioValueIsCollateralSameWithUnitOto == 0){
      groupObjectSelected = _provider.groupObjectSelected;
      objectSelected = _provider.objectSelected;
      productTypeSelected = _provider.productTypeSelected;
      modelObjectSelected = _provider.modelObjectSelected;
      brandObjectSelected = _provider.brandObjectSelected;
      objectTypeSelected = _provider.objectTypeSelected;
      objectUsageSelected = _provider.objectUsageModel;
      this._controllerGroupObject.text = "${groupObjectSelected.KODE} - ${groupObjectSelected.DESKRIPSI}";
      this._controllerObject.text = "${objectSelected.id} - ${objectSelected.name}";
      this._controllerTypeProduct.text = "${productTypeSelected.id} - ${productTypeSelected.name}";
      this._controllerBrandObject.text = "${brandObjectSelected.id} - ${brandObjectSelected.name}";
      this._controllerObjectType.text = "${objectTypeSelected.id} - ${objectTypeSelected.name}";
      this._controllerModelObject.text = "${modelObjectSelected.id} - ${modelObjectSelected.name}";
      this._controllerUsageObjectModel.text = "${objectUsageSelected.id} - ${objectUsageSelected.name}";
      notifyListeners();
    }
    else{
      groupObjectSelected = null;
      objectSelected = null;
      productTypeSelected = null;
      modelObjectSelected = null;
      brandObjectSelected = null;
      objectTypeSelected = null;
      objectUsageSelected = null;
      this._controllerGroupObject.clear();
      this._controllerObject.clear();
      this._controllerTypeProduct.clear();
      this._controllerBrandObject.clear();
      this._controllerObjectType.clear();
      this._controllerModelObject.clear();
      this._controllerUsageObjectModel.clear();
      notifyListeners();
    }
  }

  void searchModelObject(BuildContext context,String flag,int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchModelObjectChangeNotifier(),
                child: SearchModelObject(flag: flag,flagByBrandModelType: flagByBrandModelType))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageSelected = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageSelected.id} - ${_objectUsageSelected.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchGroupObject(BuildContext context, String flag) async {
    GroupObjectModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchGroupObjectChangeNotifier(),
                child: SearchGroupObject(flag: flag,))));
    if (data != null) {
      this._groupObjectSelected = data;
      this._controllerGroupObject.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObject(BuildContext context) async {
    ObjectModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectChangeNotifier(),
                child: SearchObject(kodeObject: this._groupObjectSelected.KODE))));
    if (data != null) {
      this._objectSelected = data;
      this._controllerObject.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchProductType(BuildContext context,String flag) async {
    ProductTypeModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchProductTypeChangeNotifier(),
                child: SearchProductType(flag: flag,))));
    if (data != null) {
      this._productTypeSelected = data;
      this._controllerTypeProduct.text = "${data.id} - ${data.name}";
      notifyListeners();
      var _getProdMatrixID = await getProdMatrixID(context, flag);
      if(_getProdMatrixID['status']){
        _prodMatrixId = _getProdMatrixID['value'];
      } else {
        showSnackBar(_getProdMatrixID['message']);
      }
      // _getProdMatrixID(context, flag);
    } else {
      return;
    }
  }

  void searchBrandObject(BuildContext context,String flag,int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBrandObjectChangeNotifier(),
                child: SearchBrandObject(flag: flag,flagByBrandModelType: flagByBrandModelType))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageSelected = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageSelected.id} - ${_objectUsageSelected.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObjectType(BuildContext context,String flag,int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectTypeChangeNotifier(),
                child: SearchObjectType(flagByBrandModelType: flagByBrandModelType,flag: flag))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageSelected = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageSelected.id} - ${_objectUsageSelected.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObjectUsage(BuildContext context) async {
    ObjectUsageModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectUsageChangeNotifier(),
                child: SearchUsageObject())));
    if (data != null) {
      this._objectUsageSelected = data;
      this._controllerUsageObjectModel.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchUsageCollateral(BuildContext context,int flag) async {
    CollateralUsageModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchCollateralUsageChangeNotifier(),
                child: SearchCollateralUsage())));
    if (data != null) {
      if(flag == 1){
        this._collateralUsageOtoSelected = data;
        this._controllerUsageCollateralOto.text = "${data.id} - ${data.name}";
      }
      else{
        this._collateralUsagePropertySelected = data;
        this._controllerUsageCollateralProperty.text = "${data.id} - ${data.name}";
      }
      notifyListeners();
    } else {
      return;
    }
  }

  int get radioValueIsCollateralSameWithApplicantProperty =>
      _radioValueIsCollateralSameWithApplicantProperty;

  set radioValueIsCollateralSameWithApplicantProperty(int value) {
    this._radioValueIsCollateralSameWithApplicantProperty = value;
    notifyListeners();
  }

  int get radioValueAccessCar => _radioValueAccessCar;

  set radioValueAccessCar(int value) {
    this._radioValueAccessCar = value;
    notifyListeners();
  }

  int get radioValueForAllUnitProperty => _radioValueForAllUnitProperty;

  set radioValueForAllUnitProperty(int value) {
    this._radioValueForAllUnitProperty = value;
    notifyListeners();
  }

  TextEditingController get controllerBirthPlaceValidWithIdentityLOV =>
      _controllerBirthPlaceValidWithIdentityLOV;

  TextEditingController get controllerBirthPlaceValidWithIdentity1 =>
      _controllerBirthPlaceValidWithIdentity1;

  TextEditingController get controllerNameOnCollateral =>
      _controllerNameOnCollateral;

  TextEditingController get controllerIdentityNumber =>
      _controllerIdentityNumber;

  TextEditingController get controllerIdentityType => _controllerIdentityType;

  TextEditingController get controllerCertificateNumber =>
      _controllerCertificateNumber;

  CertificateTypeModel get certificateTypeSelected => _certificateTypeSelected;

  TextEditingController get controllerBuildingArea => _controllerBuildingArea;

  TextEditingController get controllerSurfaceArea => _controllerSurfaceArea;

  TextEditingController get controllerCertificateReleaseDate =>
      _controllerCertificateReleaseDate;

  TextEditingController get controllerCertificateReleaseYear =>
      _controllerCertificateReleaseYear;

  TextEditingController get controllerDateOfMeasuringLetter =>
      _controllerDateOfMeasuringLetter;

  TextEditingController get controllerCertificateOfMeasuringLetter =>
      _controllerCertificateOfMeasuringLetter;

  TextEditingController get controllerDateOfIMB => _controllerDateOfIMB;

  TextEditingController get controllerUsageCollateralProperty =>
      _controllerUsageCollateralProperty;

  TextEditingController get controllerAddress => _controllerAddress;

  TextEditingController get controllerRT => _controllerRT;

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerProv => _controllerProv;

  TextEditingController get controllerPostalCode => _controllerPostalCode;


  TextEditingController get controllerAddressFromMap =>
      _controllerAddressFromMap;

  set certificateTypeSelected(CertificateTypeModel value) {
    this._certificateTypeSelected = value;
    notifyListeners();
  }

  PropertyTypeModel get propertyTypeSelected => _propertyTypeSelected;

  set propertyTypeSelected(PropertyTypeModel value) {
    this._propertyTypeSelected = value;
    notifyListeners();
  }

  TypeOfRoofModel get typeOfRoofSelected => _typeOfRoofSelected;

  set typeOfRoofSelected(TypeOfRoofModel value) {
    this._typeOfRoofSelected = value;
    notifyListeners();
  }

  StreetTypeModel get streetTypeSelected => _streetTypeSelected;

  set streetTypeSelected(StreetTypeModel value) {
    this._streetTypeSelected = value;
    notifyListeners();
  }

  FloorTypeModel get floorTypeSelected => _floorTypeSelected;

  set floorTypeSelected(FloorTypeModel value) {
    this._floorTypeSelected = value;
    notifyListeners();
  }

  FoundationTypeModel get foundationTypeSelected => _foundationTypeSelected;

  set foundationTypeSelected(FoundationTypeModel value) {
    this._foundationTypeSelected = value;
    notifyListeners();
  }

  WallTypeModel get wallTypeSelected => _wallTypeSelected;

  set wallTypeSelected(WallTypeModel value) {
    this._wallTypeSelected = value;
    notifyListeners();
  }

  CollateralUsageModel get collateralUsagePropertySelected =>
      _collateralUsagePropertySelected;

  set collateralUsagePropertySelected(CollateralUsageModel value) {
    this._collateralUsagePropertySelected = value;
    notifyListeners();
  }


  JenisAlamatModel get addressTypeSelected => _addressTypeSelected;

  set addressTypeSelected(JenisAlamatModel value) {
    this._addressTypeSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<CertificateTypeModel> get listCertificateType => UnmodifiableListView(_listCertificateType);

  UnmodifiableListView<PropertyTypeModel> get listPropertyType => UnmodifiableListView(_listPropertyType);

  UnmodifiableListView<TypeOfRoofModel> get listTypeOfRoof => UnmodifiableListView(_listTypeOfRoof);

  DateTime get initialDateCertificateRelease => _initialDateCertificateRelease;

  UnmodifiableListView<WallTypeModel> get listWallType => UnmodifiableListView(_listWallType);

  UnmodifiableListView<FoundationTypeModel> get listFoundationType => UnmodifiableListView(_listFoundationType);

  UnmodifiableListView<FloorTypeModel> get listFloorType => UnmodifiableListView(_listFloorType);

  UnmodifiableListView<StreetTypeModel> get listStreetType => UnmodifiableListView(_listStreetType);


  List<JenisAlamatModel> get listAddressType => _listAddressType;

  void sameWithApplicantProperty(BuildContext context){
    var _provider = Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false);
    if(this._radioValueIsCollateralSameWithApplicantProperty == 0){
      this._initialDateForBirthDateProp = _provider.initialDateForTglLahir;
      this._controllerIdentityType.text = _provider.identitasModel.name;
      this._controllerIdentityNumber.text = _provider.controllerNoIdentitas.text;
      this._controllerBirthDateProp.text = dateFormat.format(_provider.initialDateForTglLahir);
      this._controllerBirthPlaceValidWithIdentity1.text = _provider.controllerTempatLahirSesuaiIdentitas.text;
      this._controllerNameOnCollateral.text = _provider.controllerNamaLengkapSesuaiIdentitas.text;
      notifyListeners();
    }
    else{
      this._controllerIdentityType.clear();
      this._controllerIdentityNumber.clear();
      this._controllerBirthDateProp.clear();
      this._controllerBirthPlaceValidWithIdentity1.clear();
      this._controllerBirthPlaceValidWithIdentityLOV.clear();
      this._addressTypeSelected = null;
      this._controllerAddress.clear();
      this._controllerRT.clear();
      this._controllerRW.clear();
      this._kelurahanModel = null;
      this._controllerKelurahan.clear();
      this._controllerKecamatan.clear();
      this._controllerKota.clear();
      this._controllerProv.clear();
      this._controllerPostalCode.clear();
      this._enableTf = true;
      addListAddressType();
      notifyListeners();
    }
    addDataListJenisAlamat(context);
  }

  void sameWithApplicantAutomotive(BuildContext context){
    var _provider = Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false);
    if(this._radioValueIsCollaNameSameWithApplicantOto == 0){
      // for(int i = 0; i < this._listIdentity.length; i++){
      //   if(this._listIdentity[i].id == _provider.identitasModel.id){
      //     this._identityTypeSelectedAuto = this._listIdentity[i];
      //   }
      // }
      this._listIdentity.clear();
      this._listIdentity.add(_provider.identitasModel);
      this._identityTypeSelectedAuto = this._listIdentity[0];

      this._controllerIdentityNumberAuto.text = _provider.controllerNoIdentitas.text;
      this._controllerNameOnCollateralAuto.text = _provider.controllerNamaLengkapSesuaiIdentitas.text;
      this._controllerBirthDateAuto.text = _provider.controllerTglLahir.text;
      this._controllerBirthPlaceValidWithIdentityAuto.text = _provider.controllerTempatLahirSesuaiIdentitas.text;
      this._controllerBirthPlaceValidWithIdentityLOVAuto.text = _provider.controllerTempatLahirSesuaiIdentitasLOV.text;
      this._birthPlaceAutoSelected = _provider.birthPlaceSelected;
    }
    else{
      _listIdentity = IdentityType().lisIdentityModel;
      this._controllerIdentityNumberAuto.clear();
      this._controllerNameOnCollateralAuto.clear();
      this._controllerBirthDateAuto.clear();
      this._controllerBirthPlaceValidWithIdentityAuto.clear();
      this._controllerBirthPlaceValidWithIdentityLOVAuto.clear();
      this._birthPlaceAutoSelected = null;
      this._identityTypeSelectedAuto = null;
    }
    notifyListeners();
  }

  void selectCertificateReleaseDate(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateCertificateRelease,
    //     firstDate: DateTime(
    //         DateTime.now().year-50, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateCertificateRelease = _picked;
    //   this._controllerCertificateReleaseDate.text = dateFormat.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context, _initialDateCertificateRelease);
    if (_picked != null) {
      this._initialDateCertificateRelease = _picked;
      this._controllerCertificateReleaseDate.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  void selectDateOfMeasuringLetter(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateOfMeasuringLetter,
    //     firstDate: DateTime(
    //         DateTime.now().year-50, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateOfMeasuringLetter = _picked;
    //   this._controllerDateOfMeasuringLetter.text = dateFormat.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context, _initialDateOfMeasuringLetter);
    if (_picked != null) {
      this._initialDateOfMeasuringLetter = _picked;
      this._controllerDateOfMeasuringLetter.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  void selectDateOfIMB(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateOfIMB,
    //     firstDate: DateTime(
    //         DateTime.now().year-5, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year + 5, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateOfIMB = _picked;
    //   this._controllerDateOfIMB.text = dateFormat.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context, _initialDateOfIMB);
    if (_picked != null) {
      this._initialDateOfIMB = _picked;
      this._controllerDateOfIMB.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  void addListAddressType() async{
    this._listAddressType.clear();
    this._listAddressType = await getAddressType(10);
    if(addressTypeSelected != null){
      for (int i = 0; i < this._listAddressType.length; i++) {
        if (this.addressTypeSelected.KODE == this._listAddressType[i].KODE) {
          this.addressTypeSelected = this._listAddressType[i];
        }
      }
    }
    notifyListeners();
  }

  void addDataListJenisAlamat(BuildContext context) async {
    var _provider =
    Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    this._listAddress.clear();
    this._listAddressType.clear();
    if(this._radioValueIsCollateralSameWithApplicantProperty == 0){
      for(int i=0; i < _provider.listAlamatKorespondensi.length; i++){
        this._listAddressType.add(_provider.listAlamatKorespondensi[i].jenisAlamatModel);
        this._listAddress.add(_provider.listAlamatKorespondensi[i]);
      }
      setValueAddress(0);
    }
    else{
      addListAddressType();
//        this._listAddressType = await getAddressType(10);
//      _listAddressType = [
//        JenisAlamatModel("01", "Identitas"),
//        JenisAlamatModel("02", "Domisili 1"),
//        JenisAlamatModel("03", "Domisili 2"),
//        JenisAlamatModel("04", "Domisili 3"),
//        JenisAlamatModel("05", "Kantor 1"),
//        JenisAlamatModel("05", "Kantor 2"),
//        JenisAlamatModel("07", "Kantor 3")
//      ];
    }
  }

  void setValueAddress(int flag){
    if(flag == 0){
      for(int i=0; i < this._listAddress.length; i++){
        if(this._listAddress[i].isCorrespondence){
          addressTypeSelected = this._listAddress[i].jenisAlamatModel;
          this._controllerAddress.text = this._listAddress[i].address;
          this._controllerRT.text = this._listAddress[i].rt;
          this._controllerRW.text = this._listAddress[i].rw;
          this._kelurahanModel = this._listAddress[i].kelurahanModel;
          this._controllerKelurahan.text = this._kelurahanModel.KEL_NAME;
          this._controllerKecamatan.text = this._kelurahanModel.KEC_NAME;
          this._controllerKota.text = this._kelurahanModel.KABKOT_NAME;
          this._controllerProv.text = this._kelurahanModel.PROV_NAME;
          this._controllerPostalCode.text = this._kelurahanModel.ZIPCODE;
          enableTf = false;
        }
      }
    }
    else{
      for(int i=0; i < this._listAddress.length; i++){
        if(this._addressTypeSelected.KODE == this._listAddress[i].jenisAlamatModel.KODE){
          this._controllerAddress.text = this._listAddress[i].address;
          this._controllerRT.text = this._listAddress[i].rt;
          this._controllerRW.text = this._listAddress[i].rw;
          this._kelurahanModel = this._listAddress[i].kelurahanModel;
          this._controllerKelurahan.text = this._kelurahanModel.KEL_NAME;
          this._controllerKecamatan.text = this._kelurahanModel.KEC_NAME;
          this._controllerKota.text = this._kelurahanModel.KABKOT_NAME;
          this._controllerProv.text = this._kelurahanModel.PROV_NAME;
          this._controllerPostalCode.text = this._kelurahanModel.ZIPCODE;
          enableTf = false;
        }
      }
    }
  }

  GlobalKey<FormState> get keyFormAuto => _keyAuto;

  GlobalKey<FormState> get keyFormProp => _keyProp;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context, int type) {
    var _form;
    if(type == 1){
      autoValidateProp = false;
      _form = _keyAuto.currentState;
      if (_form.validate() && collateralTypeModel != null) {
        flag = true;
        autoValidateAuto = false;
        Navigator.pop(context);
      } else {
        flag = false;
        autoValidateAuto = true;
      }
    } else {
      autoValidateAuto = false;
      _form = _keyProp.currentState;
      if (_form.validate() && collateralTypeModel != null) {
        flag = true;
        autoValidateProp = false;
        Navigator.pop(context);
      } else {
        flag = false;
        autoValidateProp = true;
      }
    }
  }

  Future<bool> onBackPress() async{
    var _form;
    if(collateralTypeModel == null){
      return true;
    }
    if(collateralTypeModel.id == "001"){
      autoValidateProp = false;
      _form = _keyAuto.currentState;
      if (_form.validate() && collateralTypeModel != null) {
        flag = true;
        autoValidateAuto = false;
      } else {
        flag = false;
        autoValidateAuto = true;
      }
    } else {
      autoValidateAuto = false;
      _form = _keyProp.currentState;
      if (_form.validate() && collateralTypeModel != null) {
        flag = true;
        autoValidateProp = false;
      } else {
        flag = false;
        autoValidateProp = true;
      }
    }
    return true;
  }


  void clearDataInfoCollateral(){
    _autoValidateAuto = false;
    _autoValidateProp = false;

//    AUTOMOTIVE
    _collateralTypeModel = null;
    _controllerCollateralType.text = "";
    _radioValueIsCollateralSameWithUnitOto = 1;
    _controllerGroupObject.text = "";
    _controllerBrandObject.text = "";
    _controllerObjectType.text = "";
    _controllerModelObject.text = "";
    _controllerUsageObjectModel.text = "";
    _yearProductionSelected = null;
    _yearRegistrationSelected = null;
    _radioValueYellowPlat = 0;
    _radioValueBuiltUpNonATPM = 0;
    _radioValueWorthyOrUnworthy = 0;
    _controllerUsageCollateralOto.text = "";
    _radioValueForAllUnitOto = 0;
    this._identityTypeSelectedAuto = null;
    this._controllerIdentityNumberAuto.clear();
    this._controllerNameOnCollateralAuto.clear();
    this._initialDateForBirthDateAuto = DateTime(dateNow.year, dateNow.month, dateNow.day);
    this._controllerBirthDateAuto.clear();
    this._controllerBirthPlaceValidWithIdentityAuto.clear();
    this._controllerBirthPlaceValidWithIdentityLOVAuto.clear();
    this._birthPlaceAutoSelected = null;

    _controllerGradeUnit.text = "";
    _controllerFasilitasUTJ.text = "";
    _controllerNamaBidder.text = "";
    _controllerHargaJualShowroom.text = "";
    _controllerPerlengkapanTambahan.text = "";
    _controllerMPAdira.text = "";
    _controllerHargaPasar.text = "";
    _controllerRekondisiFisik.text = "";
    _controllerPHMaxAutomotive.text = "";
    _controllerTaksasiPriceAutomotive.text = "";

//    _certificateTypeSelected = null;
//    _propertyTypeSelected = null;
//    _typeOfRoofSelected = null;
//    _wallTypeSelected = null;
//    _foundationTypeSelected = null;
//    _floorTypeSelected = null;
//    _streetTypeSelected = null;
//    _collateralUsagePropertySelected = null;
//    _addressTypeSelected = null;
//    _enableTf = true;
//    _kelurahanModel = null;

//    END AUTOMOTIVE

//  PROPERTI
    _radioValueIsCollateralSameWithApplicantProperty = 1;
    _controllerIdentityType.text = "";
    _controllerIdentityNumber.text = "";
    _controllerNameOnCollateral.text = "";
    _controllerBirthPlaceValidWithIdentity1.text = "";
    _controllerBirthPlaceValidWithIdentityLOV.text = "";
    this._controllerBirthDateProp.clear();
    this._initialDateForBirthDateProp = DateTime(dateNow.year, dateNow.month, dateNow.day);
    _controllerCertificateNumber.text = "";
    _certificateTypeSelected = null;
    _propertyTypeSelected = null;
    _controllerBuildingArea.text = "";
    _controllerSurfaceArea.text = "";
    _controllerCertificateReleaseDate.text = "";
    _controllerCertificateReleaseYear.text = "";
    _controllerDateOfMeasuringLetter.text = "";
    _controllerCertificateOfMeasuringLetter.text = "";
    _typeOfRoofSelected = null;
    _wallTypeSelected = null;
    _floorTypeSelected = null;
    _foundationTypeSelected = null;
    _streetTypeSelected = null;
    _radioValueAccessCar = 0;
    _controllerDateOfIMB.text = "";
    _collateralUsagePropertySelected = null;
    _controllerUsageCollateralProperty.text = "";
    _radioValueForAllUnitProperty = 0;
    _addressTypeSelected = null;
    _controllerAddress.text = "";
    _controllerRT.text = "";
    _controllerRW.text = "";
    _controllerKelurahan.text = "";
    _controllerKecamatan.text = "";
    _controllerKota.text = "";
    _controllerProv.text = "";
    _controllerPostalCode.text = "";


    _controllerTaksasiPrice.text = "";
    _controllerSifatJaminan.text = "";
    _controllerBuktiKepemilikan.text = "";
    _controllerNamaPemegangHak.text = "";
    _controllerNoSuratUkur.text = "";
    _controllerDPJaminan.text = "";
    _controllerPHMax.text = "";
    _controllerJarakFasumPositif.text = "";
    _controllerJarakFasumNegatif.text = "";
    _controllerHargaTanah.text = "";
    _controllerHargaNJOP.text = "";
    _controllerHargaBangunan.text = "";
    _controllerJumlahRumahDalamRadius.text = "";
    _controllerMasaHakBerlaku.text = "";
    _controllerNoIMB.text = "";
    _controllerLuasBangunanIMB.text = "";
    _controllerLTV.text = "";
//  END PROPERTI
  }

  FormatCurrency get formatCurrency => _formatCurrency;

  RegExInputFormatter get amountValidator => _amountValidator;

  void searchBirthPlace(BuildContext context,int flag) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      if(flag == 0){
        this._birthPlaceAutoSelected = data;
        this._controllerBirthPlaceValidWithIdentityLOVAuto.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
        notifyListeners();
      }
     else{
        this._birthPlaceSelected = data;
        this._controllerBirthPlaceValidWithIdentityLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
        notifyListeners();
     }
    }
    else {
      return;
    }
  }

  IdentityModel get identityModel => _identityModel;

  set identityModel(IdentityModel value) {
    this._identityModel = value;
    notifyListeners();
  }

  DateTime _initialDateForBirthDateAuto = DateTime(dateNow.year, dateNow.month, dateNow.day);
  DateTime _initialDateForBirthDateProp = DateTime(dateNow.year, dateNow.month, dateNow.day);

  DateTime get initialDateForBirthDateAuto => _initialDateForBirthDateAuto;
  DateTime get initialDateForBirthDateProp => _initialDateForBirthDateProp;

  void selectBirthDate(BuildContext context,int flag) async {
    // final DateTime picked = await showDatePicker(
    //     context: context,
    //     initialDate: flag == 0 ? _initialDateForBirthDateAuto : _initialDateForBirthDateProp,
    //     firstDate: DateTime(1920, 1, 1),
    //     lastDate:DateTime(dateNow.year, dateNow.month, dateNow.day));
    // if (picked != null) {
    //   if(flag == 0){
    //     this._controllerBirthDateAuto.text = dateFormat.format(picked);
    //     this._initialDateForBirthDateAuto = picked;
    //   }
    //   else{
    //     this._controllerBirthDateProp.text = dateFormat.format(picked);
    //     this._initialDateForBirthDateProp = picked;
    //   }
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var picked = await selectDate(context, flag == 0 ? _initialDateForBirthDateAuto : _initialDateForBirthDateProp);
    if (picked != null) {
      if(flag == 0){
        this._controllerBirthDateAuto.text = dateFormat.format(picked);
        this._initialDateForBirthDateAuto = picked;
      }
      else{
        this._controllerBirthDateProp.text = dateFormat.format(picked);
        this._initialDateForBirthDateProp = picked;
      }
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void saveToSQLiteOto() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.insertMS2ApplObjtCollOto(MS2ApplObjtCollOtoModel(
      null,
      1,
      this._radioValueIsCollateralSameWithUnitOto,
      this._radioValueIsCollaNameSameWithApplicantOto,
      this._identityTypeSelectedAuto != null ? this._identityTypeSelectedAuto.id : "",
      this._identityTypeSelectedAuto != null ? this._identityTypeSelectedAuto.name : "",
      this._controllerIdentityNumberAuto.text != "" ? this._controllerIdentityNumberAuto.text : "",
      this._controllerNameOnCollateralAuto.text != "" ? this._controllerNameOnCollateralAuto.text : "",
      this._controllerBirthDateAuto.text != "" ? this._controllerBirthDateAuto.text : "",
      this._controllerBirthPlaceValidWithIdentityAuto.text != "" ? this._controllerBirthPlaceValidWithIdentityAuto.text : "",
      this._birthPlaceAutoSelected != null ? this._birthPlaceAutoSelected.KABKOT_ID : "",
      this._birthPlaceAutoSelected != null ? this._birthPlaceAutoSelected.KABKOT_NAME : "",
      this._groupObjectSelected != null ? this._groupObjectSelected.KODE : "",
      this._groupObjectSelected != null ? this._groupObjectSelected.DESKRIPSI : "",
      this._objectSelected != null ? this._objectSelected.id : "",
      this._objectSelected != null ? this._objectSelected.name : "",
      this._brandObjectSelected != null ? this._brandObjectSelected.id : "",
      this._brandObjectSelected != null ? this._brandObjectSelected.name : "",
      this._objectTypeSelected != null ? this._objectTypeSelected.id : "",
      this._objectTypeSelected != null ? this._objectTypeSelected.name : "",
      this._objectUsageSelected != null ? this._objectUsageSelected.id : "",
      this._objectUsageSelected != null ? this._objectUsageSelected.name : "",
      null,
      null,
      this._yearProductionSelected != null ? int.parse(this._yearProductionSelected) : 0,
      this._yearRegistrationSelected != null ? int.parse(this._yearRegistrationSelected) : 0,
      this._radioValueYellowPlat,
      null,
      null,
      this._controllerFrameNumber.text != "" ? this._controllerFrameNumber.text : "",
      this._controllerMachineNumber.text != "" ? this._controllerMachineNumber.text : "",
      this._controllerPoliceNumber.text != "" ? this._controllerPoliceNumber.text : "",
      this._controllerGradeUnit.text != "" ? this._controllerGradeUnit.text : "",
      this._controllerFasilitasUTJ.text != "" ? this._controllerFasilitasUTJ.text : "",
      this._controllerNamaBidder.text != "" ? this._controllerNamaBidder.text : "",
      this._controllerHargaJualShowroom.text != "" ? int.parse(this._controllerHargaJualShowroom.text) : "",
      this._controllerPerlengkapanTambahan.text != "" ? this._controllerPerlengkapanTambahan.text : "",
      this._controllerMPAdira.text != "" ? int.parse(this._controllerMPAdira.text) : "",
      this._controllerRekondisiFisik.text != "" ? int.parse(this._controllerRekondisiFisik.text) : "",
      this._radioValueWorthyOrUnworthy,
      null,
      this._controllerDpGuarantee.text != "" ? int.parse(this._controllerDpGuarantee.text) : 0,
      this._controllerPHMaxAutomotive.text != "" ? int.parse(this._controllerPHMaxAutomotive.text) : 0,
      this._controllerTaksasiPriceAutomotive.text != "" ? int.parse(this._controllerTaksasiPriceAutomotive.text) : 0,
      this._collateralUsageOtoSelected != null ? this._collateralUsageOtoSelected.id : "",
      this._collateralUsageOtoSelected != null ? this._collateralUsageOtoSelected.name : "",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      1,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
      null,
      null,
      null
    ));
  }

  Future<bool> deleteSQLiteCollaOto() async{
    return await _dbHelper.deleteApplCollaOto();
  }

  void saveToSQLiteProperti(BuildContext context,String type) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerDetailCustomer = Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false);
    List<MS2CustAddrModel> _listAddress = [];

    _dbHelper.insertMS2ApplObjtCollProp(MS2ApplObjtCollPropModel(
      null,
      null,
      this._radioValueIsCollateralSameWithApplicantProperty,
      this._radioValueIsCollateralSameWithApplicantProperty == 1 ? this._identityModel != null ? this._identityModel.id : null : _providerDetailCustomer.identitasModel.id ,
      this._radioValueIsCollateralSameWithApplicantProperty == 1 ? this._identityModel != null ? this._identityModel.name : null : _providerDetailCustomer.identitasModel.name,
      this._controllerIdentityNumber.text != "" ? this._controllerIdentityNumber.text : "",
      this._controllerNameOnCollateral.text != "" ? this._controllerNameOnCollateral.text : "",
      this._controllerBirthDateProp.text != "" ? this._controllerBirthDateProp.text : "",
      this._controllerBirthPlaceValidWithIdentity1.text != "" ? this._controllerBirthPlaceValidWithIdentity1.text : "",
      this._birthPlaceSelected != null ? this._birthPlaceSelected.KABKOT_ID : null,
      this._birthPlaceSelected != null ? this._birthPlaceSelected.KABKOT_NAME : null,
      this._controllerCertificateNumber.text != "" ? this._controllerCertificateNumber.text : "",
      this._certificateTypeSelected != null ? this._certificateTypeSelected.id:"",
      this._certificateTypeSelected != null ? this._certificateTypeSelected.name:"",
      this._propertyTypeSelected != null ? this._propertyTypeSelected.id : "",
      this._propertyTypeSelected != null ? this._propertyTypeSelected.name : "",
      this._controllerBuildingArea.text != "" ? double.parse(this._controllerBuildingArea.text.replaceAll(",", "")) : 0,
      this._controllerSurfaceArea.text != "" ? double.parse(this._controllerSurfaceArea.text.replaceAll(",", "")) : 0,
      this._controllerDPJaminan.text != "" ? double.parse(this._controllerDPJaminan.text.replaceAll(",", "")) : 0,
      this._controllerPHMax.text != "" ? double.parse(this._controllerPHMax.text.replaceAll(",", "")) : "",
      this._controllerPHMax.text != "" ? double.parse(this._controllerTaksasiPrice.text.replaceAll(",", "")) : "",
      this._collateralUsagePropertySelected != null ? this._collateralUsagePropertySelected.id : "",
      this._collateralUsagePropertySelected != null ? this._collateralUsagePropertySelected.name : "",
      this._controllerJarakFasumPositif.text != "" ? double.parse(this._controllerJarakFasumPositif.text.replaceAll(",", "")) : 0,
      this._controllerJarakFasumNegatif.text != "" ? double.parse(this._controllerJarakFasumNegatif.text.replaceAll(",", "")) : 0,
      this._controllerHargaTanah.text != "" ? double.parse(this._controllerHargaTanah.text.replaceAll(",", "")) : 0,
      this._controllerHargaNJOP.text != "" ? double.parse(this._controllerHargaNJOP.text.replaceAll(",", "")) : 0,
      this._controllerHargaBangunan.text != "" ? double.parse(this._controllerHargaBangunan.text.replaceAll(",", "")) : 0,
      this._typeOfRoofSelected != null ? this._typeOfRoofSelected.id : "",
      this._typeOfRoofSelected != null ? this._typeOfRoofSelected.name : "",
      this._wallTypeSelected != null ? this._wallTypeSelected.id : "",
      this._wallTypeSelected != null ? this._wallTypeSelected.name : "",
      this._floorTypeSelected != null ? this._floorTypeSelected.id : "",
      this._floorTypeSelected != null ? this._floorTypeSelected.name : "",
      this._foundationTypeSelected != null ? this._foundationTypeSelected.id:"",
      this._foundationTypeSelected != null ? this._foundationTypeSelected.name:"",
      this._streetTypeSelected != null ? this._streetTypeSelected.id : "",
      this._streetTypeSelected != null ? this._streetTypeSelected.name : "",
      this._radioValueAccessCar,
      this._controllerJumlahRumahDalamRadius.text != "" ? int.parse(this._controllerJumlahRumahDalamRadius.text.replaceAll(",", "")) : 0,
      this._controllerSifatJaminan.text != "" ? this._controllerSifatJaminan.text : "",
      this._controllerBuktiKepemilikan.text != "" ? this._controllerBuktiKepemilikan.text : "",
      this._controllerCertificateReleaseDate.text != "" ? this._controllerCertificateReleaseDate.text : "",
      this._controllerCertificateReleaseYear.text != "" ? int.parse(this._controllerCertificateReleaseYear.text.replaceAll(",", "")) : 0,
      this._controllerNamaPemegangHak.text != "" ? this._controllerNamaPemegangHak.text : "",
      this._controllerNoSuratUkur.text != "" ? this._controllerNoSuratUkur.text : "",
      this._controllerDateOfMeasuringLetter.text != "" ? this._controllerDateOfMeasuringLetter.text : "",
      this._controllerCertificateOfMeasuringLetter.text != "" ? this._controllerCertificateOfMeasuringLetter.text : "",
      this._controllerMasaHakBerlaku.text != "" ? this._controllerMasaHakBerlaku.text : "",
      this._controllerNoIMB.text != "" ? this._controllerNoIMB.text : "",
      this._controllerDateOfIMB.text != "" ? this._controllerDateOfIMB.text : "",
      this._controllerLuasBangunanIMB.text != "" ? double.parse(this._controllerLuasBangunanIMB.text.replaceAll(",", "")) : 0,
      this._controllerLTV.text != "" ? double.parse(this._controllerLTV.text) : 0,
      null,
      1,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
    ));

    _listAddress.add(MS2CustAddrModel(
        "000101",
        "0",
        type,
        this._controllerAddress.text,
        this._controllerRT.text,
        null,
        this._controllerRW.text,
        null,
        this._kelurahanModel != null ? this._kelurahanModel.PROV_ID : "",
        this._kelurahanModel != null ? this._kelurahanModel.PROV_NAME : "",
        this._kelurahanModel != null ? this._kelurahanModel.KABKOT_ID : "",
        this._kelurahanModel != null ? this._kelurahanModel.KABKOT_NAME:"",
        this._kelurahanModel != null ? this._kelurahanModel.KEC_ID:"",
        this._kelurahanModel != null ? this._kelurahanModel.KEC_NAME: "",
        this._kelurahanModel != null ? this._kelurahanModel.KEL_ID : "",
        this._kelurahanModel != null ? this._kelurahanModel.KEL_NAME: "",
        this._kelurahanModel != null ? this._kelurahanModel.ZIPCODE : "" ,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        this._addressTypeSelected != null ? this._addressTypeSelected.KODE : "",
        this._addressTypeSelected != null ? this._addressTypeSelected.DESKRIPSI : "",
        this._addressFromMap != null ? this._addressFromMap['latitude'] : "",
        this._addressFromMap != null ? this._addressFromMap['longitude'] : "",
        this._addressFromMap != null ? this._addressFromMap['address'] : "",
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
        1
    ));
    _dbHelper.insertMS2CustAddr(_listAddress);
  }

  Future<bool> deleteSQLiteCollaProp() async{
    return await _dbHelper.deleteApplCollaProp();
  }

  Map get addressFromMap => _addressFromMap;

  set addressFromMap(Map value) {
    this._addressFromMap = value;
    notifyListeners();
  }

  void setLocationAddressByMap(BuildContext context) async{
    Map _result = await Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage()));
    if(_result != null){
      _addressFromMap = _result;
      this._controllerAddressFromMap.text = _result["address"];
      notifyListeners();
    }
  }

  void formatting() {
    _controllerHargaJualShowroom.text = formatCurrency.formatCurrency(_controllerHargaJualShowroom.text);
    _controllerHargaPasar.text = formatCurrency.formatCurrency(_controllerHargaPasar.text);
    _controllerDpGuarantee.text = formatCurrency.formatCurrency(_controllerDpGuarantee.text);
    _controllerTaksasiPriceAutomotive.text = formatCurrency.formatCurrency(_controllerTaksasiPriceAutomotive.text);
  }
}