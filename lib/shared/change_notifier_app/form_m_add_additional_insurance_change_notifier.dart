import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/additional_insurance_model.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/models/company_model.dart';
import 'package:ad1ms2_dev/screens/search_company.dart';
import 'package:ad1ms2_dev/screens/search_product.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/search_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_product_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import 'info_additional_insurance_change_notifier.dart';

class FormMAddAdditionalInsuranceChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    List<AdditionalInsuranceModel> _listAdditionalInsurance = [];
    bool _autoValidate = false;
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    InsuranceTypeModel _insuranceTypeSelected;
    CompanyModel _companySelected;
    TextEditingController _controllerCompany = TextEditingController();
    ProductModel _productSelected;
    TextEditingController _controllerProduct = TextEditingController();
    TextEditingController _controllerPeriodType = TextEditingController();
    TextEditingController _controllerCoverageType = TextEditingController();
    TextEditingController _controllerCoverageValue = TextEditingController();
    TextEditingController _controllerUpperLimit = TextEditingController();
    TextEditingController _controllerLowerLimit = TextEditingController();
    TextEditingController _controllerUpperLimitRate = TextEditingController();
    TextEditingController _controllerLowerLimitRate = TextEditingController();
    TextEditingController _controllerPriceCash = TextEditingController();
    TextEditingController _controllerPriceCredit = TextEditingController();
    TextEditingController _controllerTotalPrice = TextEditingController();
    TextEditingController _controllerTotalPriceRate = TextEditingController();

    List<AdditionalInsuranceModel> get listFormAdditionalInsuranceObject => _listAdditionalInsurance;

    List<InsuranceTypeModel> _listInsuranceType = [
        // InsuranceTypeModel("01", "Kesehatan"),
        // InsuranceTypeModel("02", "Kecelakaan"),
        // InsuranceTypeModel("03", "Keamanan")
    ];

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    InsuranceTypeModel get insuranceTypeSelected {
        return this._insuranceTypeSelected;
    }

    set insuranceTypeSelected(InsuranceTypeModel value) {
        this._insuranceTypeSelected = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    UnmodifiableListView<InsuranceTypeModel> get listInsurance {
        return UnmodifiableListView(this._listInsuranceType);
    }

    void searchCompany(BuildContext context) async {
        CompanyModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchCompanyChangeNotifier(),
                    child: SearchCompany("1"))));
        if (_data != null) {
            this._companySelected = _data;
            this._controllerCompany.text = "${_data.id} - ${_data.text}";
            notifyListeners();
        }
    }

    CompanyModel get companySelected => _companySelected;

    set companySelected(CompanyModel value) {
        _companySelected = value;
        notifyListeners();
    }

    TextEditingController get controllerCompany {
        return this._controllerCompany;
    }

    set controllerCompany(TextEditingController value) {
        _controllerCompany = value;
    }

    void searchProduct(BuildContext context) async {
        ProductModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchProductChangeNotifier(),
                    child: SearchProduct())));
        if (_data != null) {
            this._productSelected = _data;
            this._controllerProduct.text = "${_data.KODE} - ${_data.DESKRIPSI}";
            notifyListeners();
        }
    }

    ProductModel get productSelected => _productSelected;

    set productSelected(ProductModel value) {
        _productSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerProduct {
        return this._controllerProduct;
    }

    set controllerProduct(TextEditingController value) {
        _controllerProduct = value;
    }

    TextEditingController get controllerPeriodType {
        return this._controllerPeriodType;
    }

    set controllerPeriodType(TextEditingController value) {
        _controllerPeriodType = value;
    }

    TextEditingController get controllerCoverageType {
        return this._controllerCoverageType;
    }

    set controllerCoverageType(TextEditingController value) {
        _controllerCoverageType = value;
    }

    TextEditingController get controllerCoverageValue {
        return this._controllerCoverageValue;
    }

    set controllerCoverageValue(TextEditingController value) {
        _controllerCoverageValue = value;
    }

    TextEditingController get controllerUpperLimit {
        return this._controllerUpperLimit;
    }

    set controllerUpperLimit(TextEditingController value) {
        _controllerUpperLimit = value;
    }

    TextEditingController get controllerLowerLimit {
        return this._controllerLowerLimit;
    }

    set controllerLowerLimit(TextEditingController value) {
        _controllerLowerLimit = value;
    }

    TextEditingController get controllerUpperLimitRate {
        return this._controllerUpperLimitRate;
    }

    set controllerUpperLimitRate(TextEditingController value) {
        _controllerUpperLimitRate = value;
    }

    TextEditingController get controllerLowerLimitRate {
        return this._controllerLowerLimitRate;
    }

    set controllerLowerLimitRate(TextEditingController value) {
        _controllerLowerLimitRate = value;
    }

    TextEditingController get controllerPriceCash {
        return this._controllerPriceCash;
    }

    set controllerPriceCash(TextEditingController value) {
        _controllerPriceCash = value;
    }

    TextEditingController get controllerPriceCredit{
        return this._controllerPriceCredit;
    }

    set controllerPriceCredit(TextEditingController value) {
        _controllerPriceCredit = value;
    }

    TextEditingController get controllerTotalPrice{
        return this._controllerTotalPrice;
    }

    set controllerTotalPrice(TextEditingController value) {
        _controllerTotalPrice = value;
    }

    TextEditingController get controllerTotalPriceRate{
        return this._controllerTotalPriceRate;
    }

    set controllerTotalPriceRate(TextEditingController value) {
        _controllerTotalPriceRate = value;
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    void check(BuildContext context, int flag, int index) {
        final _form = _key.currentState;
        if (flag == 0) {
            if (_form.validate()) {
                Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false)
                    .addListInfoAdditionalInsurance(AdditionalInsuranceModel(
                    this._insuranceTypeSelected,
                    this._companySelected,
                    this._productSelected,
                    this._controllerPeriodType.text,
                    this._controllerCoverageType.text,
                    this._controllerCoverageValue.text,
                    this._controllerUpperLimit.text,
                    this._controllerLowerLimit.text,
                    this.controllerUpperLimitRate.text,
                    this.controllerLowerLimitRate.text,
                    this._controllerPriceCash.text,
                    this._controllerPriceCredit.text,
                    this.controllerTotalPriceRate.text,
                    this._controllerTotalPrice.text,));
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
            if (_form.validate()) {
                Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false)
                    .updateListInfoAdditionalInsurance(AdditionalInsuranceModel(
                    this._insuranceTypeSelected,
                    this._companySelected,
                    this._productSelected,
                    this._controllerPeriodType.text,
                    this._controllerCoverageType.text,
                    this._controllerCoverageValue.text,
                    this._controllerUpperLimit.text,
                    this._controllerLowerLimit.text,
                    this.controllerUpperLimitRate.text,
                    this.controllerLowerLimitRate.text,
                    this._controllerPriceCash.text,
                    this._controllerPriceCredit.text,
                    this.controllerTotalPriceRate.text,
                    this._controllerTotalPrice.text), context, index);
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        }
    }

    Future<void> setValueForEditAdditionalInsurance(AdditionalInsuranceModel data) async {
        for (int i = 0; i < this._listInsuranceType.length; i++) {
            if (data.insuranceType.KODE == this._listInsuranceType[i].KODE) {
                this._insuranceTypeSelected = this._listInsuranceType[i];
                // this._jenisAlamatSelectedTemp = this._listInsuranceType[i];
            }
        }
        this._companySelected = data.company;
        this._controllerCompany.text = data.company.id +" - "+data.company.text;
        this._productSelected = data.product;
        this._controllerProduct.text = data.product.KODE +" - "+data.product.DESKRIPSI;
        this._controllerPeriodType.text = data.periodType;
        this._controllerCoverageType.text = data.coverageType;
        this._controllerCoverageValue.text = data.coverageValue;
        this._controllerUpperLimit.text = data.upperLimit;
        this._controllerLowerLimit.text = data.lowerLimit;
        this._controllerUpperLimitRate.text = data.upperLimitRate;
        this._controllerLowerLimitRate.text = data.lowerLimitRate;
        this._controllerPriceCash.text = data.priceCash;
        this._controllerPriceCredit.text = data.priceCredit;
        this._controllerTotalPriceRate.text = data.totalPriceRate;
        this._controllerTotalPrice.text = data.totalPrice;
    }

    void getListInsuranceType(BuildContext context) async{
        _listInsuranceType.clear();
        this._insuranceTypeSelected = null;
        loadData = true;

        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            var _body = jsonEncode({
                "P_INSR_COMPANY_CRIT_HDR_ID": "37",
                "P_INSR_LEVEL": "1",
                "P_PARA_OBJECT_ID": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).controllerObject.text
            });

            final _response = await _http.post(
                "${BaseUrl.urlPublic}api/paramcredcalc/get-insurance-type",
                body: _body,
                headers: {"Content-Type":"application/json"}
            );

            final _data = jsonDecode(_response.body);
            for(int i=0; i < _data['data'].length;i++){
                _listInsuranceType.add(InsuranceTypeModel(_data['data'][i]['PARENT_KODE'], _data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI'].trim()));
            }
            loadData = false;
        } catch (e) {
            loadData = false;
        }
        notifyListeners();
    }
}