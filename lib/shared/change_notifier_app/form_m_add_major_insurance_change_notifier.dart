import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/assurance_model.dart';
import 'package:ad1ms2_dev/models/coverage1_model.dart';
import 'package:ad1ms2_dev/models/coverage2_model.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/models/major_insurance_model.dart';
import 'package:ad1ms2_dev/models/company_model.dart';
import 'package:ad1ms2_dev/models/type_model.dart';
import 'package:ad1ms2_dev/screens/search_assurance.dart';
import 'package:ad1ms2_dev/screens/search_company.dart';
import 'package:ad1ms2_dev/screens/search_coverage1.dart';
import 'package:ad1ms2_dev/screens/search_coverage2.dart';
import 'package:ad1ms2_dev/screens/search_coverage_type.dart';
import 'package:ad1ms2_dev/screens/search_product.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:ad1ms2_dev/shared/resource/get_rate_asuransi_perluasan.dart';
import 'package:ad1ms2_dev/shared/search_assurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_coverage1_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_coverage2_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_coverage_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_product_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormMAddMajorInsuranceChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    List<MajorInsuranceModel> _listMajorInsurance = [];
    bool _autoValidate = false;
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    InsuranceTypeModel _insuranceTypeSelected;
    String _numberOfCollateral;
    AssuranceModel _assuranceSelected;
    TextEditingController _controllerAssurance = TextEditingController();
    CompanyModel _companySelected;
    TextEditingController _controllerCompany = TextEditingController();
    ProductModel _productSelected;
    TextEditingController _controllerProduct = TextEditingController();
    String _typeSelected;
    TextEditingController _controllerPeriodType = TextEditingController();
    CoverageModel _coverage1Selected;
    TextEditingController _controllerCoverage1 = TextEditingController();
    CoverageModel _coverage2Selected;
    TextEditingController _controllerCoverage2 = TextEditingController();
    TextEditingController _controllerCoverageType = TextEditingController();
    TextEditingController _controllerCoverageValue = TextEditingController();
    TextEditingController _controllerUpperLimit = TextEditingController();
    TextEditingController _controllerLowerLimit = TextEditingController();
    TextEditingController _controllerUpperLimitRate = TextEditingController();
    TextEditingController _controllerLowerLimitRate = TextEditingController();
    TextEditingController _controllerTotalPriceSplit = TextEditingController();
    TextEditingController _controllerPriceCash = TextEditingController();
    TextEditingController _controllerPriceCredit = TextEditingController();
    TextEditingController _controllerTotalPrice = TextEditingController();
    TextEditingController _controllerTotalPriceRate = TextEditingController();
    TextEditingController _controllerType = TextEditingController();
    FormatCurrency _formatCurrency = FormatCurrency();
    RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');

    List<MajorInsuranceModel> get listFormMajorInsuranceObject => _listMajorInsurance;

    List<InsuranceTypeModel> _listInsuranceType = [];

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    InsuranceTypeModel get insuranceTypeSelected {
        return this._insuranceTypeSelected;
    }

    set insuranceTypeSelected(InsuranceTypeModel value) {
        this._insuranceTypeSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<InsuranceTypeModel> get listInsurance {
        return UnmodifiableListView(this._listInsuranceType);
    }

    void searchAssurance(BuildContext context) async {
        AssuranceModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchAssuranceChangeNotifier(),
                    child: SearchAssurance())));
        if (_data != null) {
            this._assuranceSelected = _data;
            this._controllerAssurance.text = "${_data.id} - ${_data.text}";
            notifyListeners();
        }
    }

    AssuranceModel get assuranceSelected => _assuranceSelected;

    set assuranceSelected(AssuranceModel value) {
        _assuranceSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerAssurance {
        return this._controllerAssurance;
    }

    set controllerAssurance(TextEditingController value) {
        _controllerAssurance = value;
    }

    void searchCompany(BuildContext context) async {
        CompanyModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchCompanyChangeNotifier(),
                    child: SearchCompany("2"))));
        if (_data != null) {
            this._companySelected = _data;
            this._controllerCompany.text = "${_data.id} - ${_data.text}";
            notifyListeners();
        }
    }

    CompanyModel get companySelected => _companySelected;

    set companySelected(CompanyModel value) {
        _companySelected = value;
        notifyListeners();
    }

    TextEditingController get controllerCompany {
        return this._controllerCompany;
    }

    set controllerCompany(TextEditingController value) {
        _controllerCompany = value;
    }

    void searchProduct(BuildContext context) async {
        ProductModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchProductChangeNotifier(),
                    child: SearchProduct())));
        if (_data != null) {
            this._productSelected = _data;
            this._controllerProduct.text = "${_data.KODE} - ${_data.DESKRIPSI}";
            notifyListeners();
        }
    }

    ProductModel get productSelected => _productSelected;

    set productSelected(ProductModel value) {
        _productSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerProduct {
        return this._controllerProduct;
    }

    set controllerProduct(TextEditingController value) {
        _controllerProduct = value;
    }

    TextEditingController get controllerPeriodType {
        return this._controllerPeriodType;
    }

    set controllerPeriodType(TextEditingController value) {
        _controllerPeriodType = value;
    }

    List<String> _listType = [];

    String get typeSelected {
        return this._typeSelected;
    }

    set typeSelected(String value) {
        this._typeSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<String> get listType {
        return UnmodifiableListView(this._listType);
    }

    void searchCoverage(BuildContext context,int flag) async {
        if(flag == 1){
            CoverageModel _data = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider(
                        create: (context) => SearchCoverage1ChangeNotifier(),
                        child: SearchCoverage1(flag: 1,))));
            if (_data != null) {
                this._coverage1Selected = _data;
                this._controllerCoverage1.text = "${_data.PARENT_KODE} - ${_data.DESKRIPSI}";
                _getRateAsuransiPerluasan(context);
                notifyListeners();
            }
        }
        else{
            CoverageModel _data = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider(
                        create: (context) => SearchCoverage1ChangeNotifier(),
                        child: SearchCoverage1(flag: 2,))));
            if (_data != null) {
                this._coverage2Selected = _data;
                this._controllerCoverage2.text = "${_data.PARENT_KODE} - ${_data.DESKRIPSI}";
                notifyListeners();
            }
        }
    }

    CoverageModel get coverage1Selected => _coverage1Selected;

    set coverage1Selected(CoverageModel value) {
        _coverage1Selected = value;
        notifyListeners();
    }

    TextEditingController get controllerCoverage1 {
        return this._controllerCoverage1;
    }

//    void searchCoverage2(BuildContext context) async {
//        Coverage2Model _data = await Navigator.push(
//            context,
//            MaterialPageRoute(
//                builder: (context) => ChangeNotifierProvider(
//                    create: (context) => SearchCoverage2ChangeNotifier(),
//                    child: SearchCoverage2())));
//        if (_data != null) {
//            this._coverage2Selected = _data;
//            this._controllerCoverage2.text = "${_data.id} - ${_data.text}";
//            notifyListeners();
//        }
//    }

    CoverageModel get coverage2Selected => _coverage2Selected;

    set coverage2Selected(CoverageModel value) {
        _coverage2Selected = value;
        notifyListeners();
    }

    TextEditingController get controllerCoverage2 {
        return this._controllerCoverage2;
    }

    TextEditingController get controllerCoverageType {
        return this._controllerCoverageType;
    }

    set controllerCoverageType(TextEditingController value) {
        _controllerCoverageType = value;
    }

    TextEditingController get controllerCoverageValue {
        return this._controllerCoverageValue;
    }

    set controllerCoverageValue(TextEditingController value) {
        _controllerCoverageValue = value;
    }

    TextEditingController get controllerUpperLimit {
        return this._controllerUpperLimit;
    }

    set controllerUpperLimit(TextEditingController value) {
        _controllerUpperLimit = value;
    }

    TextEditingController get controllerLowerLimit {
        return this._controllerLowerLimit;
    }

    set controllerLowerLimit(TextEditingController value) {
        _controllerLowerLimit = value;
    }

    TextEditingController get controllerUpperLimitRate {
        return this._controllerUpperLimitRate;
    }

    set controllerUpperLimitRate(TextEditingController value) {
        _controllerUpperLimitRate = value;
    }

    TextEditingController get controllerLowerLimitRate {
        return this._controllerLowerLimitRate;
    }

    set controllerLowerLimitRate(TextEditingController value) {
        _controllerLowerLimitRate = value;
    }

    TextEditingController get controllerPriceCash {
        return this._controllerPriceCash;
    }

    set controllerPriceCash(TextEditingController value) {
        _controllerPriceCash = value;
    }

    TextEditingController get controllerPriceCredit{
        return this._controllerPriceCredit;
    }

    set controllerPriceCredit(TextEditingController value) {
        _controllerPriceCredit = value;
    }

    TextEditingController get controllerTotalPriceSplit {
        return this._controllerTotalPriceSplit;
    }

    set controllerTotalPriceSplit(TextEditingController value) {
        _controllerTotalPriceSplit = value;
    }

    TextEditingController get controllerTotalPrice{
        return this._controllerTotalPrice;
    }

    TextEditingController get controllerType => _controllerType;

  set controllerTotalPrice(TextEditingController value) {
        _controllerTotalPrice = value;
    }

    TextEditingController get controllerTotalPriceRate{
        return this._controllerTotalPriceRate;
    }

    set controllerTotalPriceRate(TextEditingController value) {
        _controllerTotalPriceRate = value;
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    String get numberOfCollateral => _numberOfCollateral;

    set numberOfCollateral(String value) {
        this._numberOfCollateral = value;
        notifyListeners();
    }

    List<String> _listNumberOfColla = [];

    UnmodifiableListView<String> get listNumberOfColla {
        return UnmodifiableListView(this._listNumberOfColla);
    }

    void check(BuildContext context, int flag, int index) {
        final _form = _key.currentState;
        if (flag == 0) {
            if (_form.validate()) {
                Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false)
                    .addListInfoMajorInsurance(MajorInsuranceModel(
                    this._insuranceTypeSelected,
                    this._numberOfCollateral,
                    this._companySelected,
                    this._productSelected,
                    this._controllerPeriodType.text,
                    this._switchType ? this._controllerType.text : this._typeSelected,
                    this._coverage1Selected,
                    this._coverage2Selected,
                    this._controllerCoverageType.text,
                    this._controllerCoverageValue.text,
                    this._controllerUpperLimit.text,
                    this._controllerLowerLimit.text,
                    this.controllerUpperLimitRate.text,
                    this.controllerLowerLimitRate.text,
                    this._controllerPriceCash.text,
                    this._controllerPriceCredit.text,
                    this._controllerTotalPriceSplit.text,
                    this.controllerTotalPriceRate.text,
                    this._controllerTotalPrice.text,));
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
            if (_form.validate()) {
                Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false)
                    .updateListInfoMajorInsurance(MajorInsuranceModel(
                    this._insuranceTypeSelected,
                    this._numberOfCollateral,
                    this._companySelected,
                    this._productSelected,
                    this._controllerPeriodType.text,
                    this._switchType ? this._controllerType.text : this._typeSelected,
                    this._coverage1Selected,
                    this._coverage2Selected,
                    this._controllerCoverageType.text,
                    this._controllerCoverageValue.text,
                    this._controllerUpperLimit.text,
                    this._controllerLowerLimit.text,
                    this.controllerUpperLimitRate.text,
                    this.controllerLowerLimitRate.text,
                    this._controllerPriceCash.text,
                    this._controllerPriceCredit.text,
                    this._controllerTotalPriceSplit.text,
                    this.controllerTotalPriceRate.text,
                    this._controllerTotalPrice.text), context, index);
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        }
    }

    void setValueForEditMajorInsurance(MajorInsuranceModel data)  {
//        this._insuranceTypeSelected = data.insuranceType;
        for (int i = 0; i < this._listInsuranceType.length; i++) {
            if (data.insuranceType.KODE == this._listInsuranceType[i].KODE) {
                this._insuranceTypeSelected = this._listInsuranceType[i];
//                this._jenisAlamatSelectedTemp = this._listInsuranceType[i];
            }
        }

//        for(int i=0; i < _listNumberOfColla.length; i++){
//            if(data.numberOfColla == _listNumberOfColla[i]){
//                this._numberOfCollateral = _listNumberOfColla[i];
//            }
//        }

        calculatePeriod(1,data);
        this._controllerAssurance.text = data.numberOfColla;
        this._companySelected = data.company;
        this._controllerCompany.text = data.company.id +" - "+data.company.text;
        this._productSelected = data.product;
        this._controllerProduct.text = data.product.KODE +" - "+data.product.DESKRIPSI;
        this._controllerPeriodType.text = data.periodType;
//        this._typeSelected = data.type;
        for (int i = 0; i < this._listType.length; i++) {
            if (data.type == this._listType[i]) {
                this._typeSelected = this._listType[i];
//                this._jenisAlamatSelectedTemp = this._listInsuranceType[i];
            }
        }
        this._coverage1Selected = data.coverage1;
        this._controllerCoverage1.text = data.coverage1.KODE +" - "+data.coverage1.DESKRIPSI;
        this._coverage2Selected = data.coverage2;
        this._controllerCoverage2.text = data.coverage2.PARENT_KODE +" - "+data.coverage2.DESKRIPSI;
        this._controllerCoverageType.text = data.coverageType;
        this._controllerCoverageValue.text = data.coverageValue;
        this._controllerUpperLimit.text = data.upperLimit;
        this._controllerLowerLimit.text = data.lowerLimit;
        this._controllerUpperLimitRate.text = data.upperLimitRate;
        this._controllerLowerLimitRate.text = data.lowerLimitRate;
        this._controllerPriceCash.text = data.priceCash;
        this._controllerPriceCredit.text = data.priceCredit;
        this._controllerTotalPriceSplit.text = data.totalPriceSplit;
        this._controllerTotalPriceRate.text = data.totalPriceRate;
        this._controllerTotalPrice.text = data.totalPrice;
    }

    Future<void> addData(BuildContext context,MajorInsuranceModel data,int flag) async{
        _getListInsuranceType(context,data,flag);
        this._controllerCoverageValue.text = "${Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).controllerObjectPrice.text}";
    }

    void _getListInsuranceType(BuildContext context,MajorInsuranceModel data,int flag) async{
        _listInsuranceType.clear();
        this._insuranceTypeSelected = null;
        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            var _body = jsonEncode({
                "P_INSR_COMPANY_ID" : "02",
                "P_INSR_LEVEL" : "2",
                "P_INSR_PRODUCT_ID" :"005",
                "P_PARA_OBJECT_ID" :"004"
//                Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id
            });

            final _response = await _http.post(
                "${BaseUrl.urlPublic}api/asuransi/get-insurance-type",
                body: _body,
                headers: {"Content-Type":"application/json"}
            );
            print(_response.statusCode);
            final _data = jsonDecode(_response.body);
            for(int i=0; i < _data['data'].length;i++){
                _listInsuranceType.add(InsuranceTypeModel(_data['data'][i]['PARENT_KODE'], _data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
            }
            if(flag != 0){
                setValueForEditMajorInsurance(data);
            }
        }
        catch (e) {}
        _getType();
        _insertListNumberOfCOlla(context,data,flag);
    }

    void _getType(){
        for(int i=1; i <= 36; i++){
            this._listType.add(i.toString());
        }
//        notifyListeners();
    }

    void _insertListNumberOfCOlla(BuildContext context,MajorInsuranceModel data,int flag){
        var _provider = Provider.of<InfoAppChangeNotifier>(context,listen: false);
        var _providerInfoMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context,listen: false);
        int _numberColla = int.parse(_provider.controllerTotalObject.text);
        if(flag == 0){
            if(_providerInfoMajorInsurance.listFormMajorInsurance.isNotEmpty){
                for(int i=0; i < _numberColla; i++){
                    _listNumberOfColla.add((i+1).toString());
                }

                for(int i=0; i < _providerInfoMajorInsurance.listFormMajorInsurance.length; i++){
                    for(int j=0; j < _listNumberOfColla.length; j++){
                        if(_listNumberOfColla[j] == _providerInfoMajorInsurance.listFormMajorInsurance[i].numberOfColla){
                            _listNumberOfColla.removeAt(j);
                        }
                    }
                }
            }
            else{
                for(int i=0; i < _numberColla; i++){
                    _listNumberOfColla.add((i+1).toString());
                }
            }
        }
        else{
            this._listNumberOfColla.add(data.numberOfColla);
            this._numberOfCollateral = this._listNumberOfColla[0];
        }
        notifyListeners();
    }

    bool _switchType = false;


    bool get switchType => _switchType;

    set switchType(bool value) {
      this._switchType = value;
      notifyListeners();
    }

  void calculatePeriod(int flag,MajorInsuranceModel data){
        if(flag == 0){
            int _valueType;
            if(switchType){
                _valueType = int.parse(this._controllerType.text);
                for(int i =0; i< _listType.length; i++){
                    if(_valueType.toString() == _listType[i]){
                        this._typeSelected = _listType[i];
                    }
                }
            }
            else{
                _valueType = int.parse(this._typeSelected);
                this._controllerType.clear();
                this._controllerPeriodType.clear();
            }

            if(_valueType < 36){
                if(_valueType <= 6){
                    this._controllerPeriodType.text = 6.toString();
                }
                else if(_valueType > 6 && _valueType <= 12){
                    this._controllerPeriodType.text = 12.toString();
                }
                else if(_valueType > 12 && _valueType <= 18){
                    this._controllerPeriodType.text = 18.toString();
                }
                else if(_valueType > 18 && _valueType <= 24){
                    this._controllerPeriodType.text = 24.toString();
                }
                else if(_valueType > 24 && _valueType <= 30){
                    this._controllerPeriodType.text = 30.toString();
                }
                else {
                    this._controllerPeriodType.text = 36.toString();
                }
                switchType = false;
                notifyListeners();
            }
            else{
                switchType = true;
                this._controllerPeriodType.clear();
                if(this._controllerType.text != ""){
                    if(_valueType >= 36){
                        if(_valueType >= 36 && _valueType <= 48){
                            this._controllerPeriodType.text = 48.toString();
                        }
                        else if(_valueType > 48 && _valueType <= 60){
                            this._controllerPeriodType.text = 60.toString();
                        }
                        else{
                            this._controllerType.text = 60.toString();
                            this._controllerPeriodType.text = 60.toString();
                        }
                    }
                    else{
                        for(int i=0; i < _listType.length; i++){
                            if(this._controllerType.text == _listType[i]){
                                this._typeSelected = _listType[i];
                            }
                        }
                        switchType = false;
                        calculatePeriod(0,null);
                        this._controllerType.clear();
                    }
                    notifyListeners();
                }
            }
        }
        else{
            if(int.parse(data.type) < 36){
                this._typeSelected = data.type;
                this._controllerPeriodType.text = data.periodType;
                switchType = false;
            }
            else{
                this._controllerType.text = data.type;
                this._controllerPeriodType.text = data.periodType;
                switchType = true;
            }

        }
  }

  RegExInputFormatter get amountValidator => _amountValidator;

  FormatCurrency get formatCurrency => _formatCurrency;

  void _getRateAsuransiPerluasan(BuildContext context) async{
      var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
      var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
      var _providerListOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
      var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
      var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
      SharedPreferences _preference = await SharedPreferences.getInstance();
      this._loadData = true;
      var _body = jsonEncode(
          {
              "P_APPLICATION_DATE": _providerInfoAppl.controllerOrderDate.text,
              "P_INSR_COMPANY_ID": "02",
              "P_INSR_COV_1": this._coverage1Selected != null ? "${this._coverage1Selected.PARENT_KODE}" : "",
              "P_INSR_COV_2": this._coverage2Selected != null ? "${this._coverage2Selected.PARENT_KODE}" : "",
              "P_INSR_FIN_TYPE_ID": _providerListOID.customerType != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
              "P_INSR_JENIS": "${this._typeSelected}",
              "P_INSR_MAX_OTR": "${double.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "")).round()}", // harga object dr inf structure credit
              "P_INSR_OBJT_PURPOSE_ID": "${_providerInfoUnitObject.objectPurposeSelected.id}",
              "P_INSR_OBJT_TYPE_ID": "${_providerInfoUnitObject.objectTypeSelected.id}",
              "P_INSR_PERIOD": "${this._controllerPeriodType.text}",
              "P_INSR_PRODUCT_ID": "${this._productSelected.KODE}",
              "P_PARA_CP_CENTRA_ID": "${_preference.getString("SentraD")}",// sentra D
              "P_PROD_TYPE_ID": "${_providerInfoUnitObject.productTypeSelected.id}",
          });
      print(_body);
      try{
          var _result = await getAsuransiPerluasan(_body);
          _countLimitLowerUpper(_result);
          this._loadData = false;
      }
      catch(e){
          this._loadData = false;
          print("erro ${e.toString()}");
      }
      notifyListeners();
  }

  void formatting() {
      _controllerPriceCash.text = formatCurrency.formatCurrency(_controllerPriceCash.text);
      _controllerPriceCredit.text = formatCurrency.formatCurrency(_controllerPriceCredit.text);
  }

  void _countLimitLowerUpper(Map data){
      double _valueUpperLimit = double.parse(this._controllerUpperLimitRate.text = data['PARA_UPPER_LIMIT']);
      double _valueLowerLimit = double.parse(this._controllerUpperLimitRate.text = data['PARA_LOWER_LIMIT']);
      double _valueCoverage = double.parse(this._controllerCoverageValue.text.replaceAll(",", ""));
      double _resultValueUpperLimit = _valueUpperLimit * _valueCoverage / 100;
      double _resultValueLowerLimit = _valueLowerLimit * _valueCoverage / 100;
      this._controllerUpperLimit.text = this._formatCurrency.formatCurrency(_resultValueUpperLimit.toString());
      this._controllerLowerLimit.text = this._formatCurrency.formatCurrency(_resultValueLowerLimit.toString());
  }

    void searchCoverageType(BuildContext context) async {
        Map data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchCoverageTypeChangeNotifier(),
                    child: SearchCoverageType(coverage1: this._coverage1Selected,coverage2: this._coverage2Selected,periodType: this._controllerPeriodType.text,product: this._productSelected,type: this._typeSelected))));
        if (data != null) {
            // this._sourceOrderNameSelected = data;
            this._controllerCoverageType.text = "${data['']} - ${data['']}";
            notifyListeners();
        } else {
            return;
        }
    }
}