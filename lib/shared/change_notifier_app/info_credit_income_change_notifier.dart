import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoCreditIncomeChangeNotifier with ChangeNotifier{
  bool _autoValidate = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _controllerDebtComparison = TextEditingController();
  TextEditingController _controllerDSC = TextEditingController();
  TextEditingController _controllerIncomeComparison = TextEditingController();
  TextEditingController _controllerIRR = TextEditingController();
  DbHelper _dbHelper = DbHelper();
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');

  RegExInputFormatter get amountValidator => _amountValidator;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  set amountValidator(RegExInputFormatter value) {
    this._amountValidator = value;
    notifyListeners();
  }

  TextEditingController get controllerDebtComparison =>
      _controllerDebtComparison;

  TextEditingController get controllerDSC => _controllerDSC;

  TextEditingController get controllerIncomeComparison =>
      _controllerIncomeComparison;

  TextEditingController get controllerIRR => _controllerIRR;


  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List mappingCustomerIncomeWiraswastaBelumMenikah(BuildContext context) {
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);

    // WIraswasta Belum Menikah
    var _customerIncome = [];
    for(int i=0; i < 12; i++) {
      _customerIncome.add(
        {
          "customerIncomeID": "NEW",
          "incomeFrmlType": i == 0
              ? "S" : i == 1
              ? "+" : i == 2
              ? "=" : i == 3
              ? "-" : i == 4
              ? "=" : i == 5
              ? "-" : i == 6
              ? "-" : i == 7
              ? "=" : i == 8
              ? "-" : i == 9
              ? "=" : i == 10
              ? "-" : i == 11
              ? "="
              : "",
          "incomeType": i == 0
              ? "001" : i == 1
              ? "002" : i == 2
              ? "003" : i == 3
              ? "004" : i == 4
              ? "005" : i == 5
              ? "006" : i == 6
              ? "007" : i == 7
              ? "008" : i == 8
              ? "009" : i == 9
              ? "010" : i == 10
              ? "011" : i == 11
              ? "017"
              : "",
          "incomeValue": i == 0
              ? _providerIncome.controllerMonthlyIncome.text : i == 1
              ? _providerIncome.controllerOtherIncome.text : i == 2
              ? _providerIncome.controllerTotalIncome.text : i == 3
              ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
              ? _providerIncome.controllerGrossProfit.text : i == 5
              ? _providerIncome.controllerOperatingCosts.text : i == 6
              ? _providerIncome.controllerOtherCosts.text : i == 7
              ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
              ? _providerIncome.controllerTax.text : i == 9
              ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
              ? _providerIncome.controllerCostOfLiving.text : i == 11
              ? _providerIncome.controllerRestIncome.text
              : _providerIncome.controllerOtherInstallments.text,
          "dataStatus": "ACTIVE",
          "creationalSpecification": {
            "createdAt": 1605192713593,
            "createdBy": "SYSTEM",
            "modifiedAt": null,
            "modifiedBy": null
          }
        });
    }

    return _customerIncome;
  }

  List mappingCustomerIncomeWiraswastaMenikah(BuildContext context) {
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);

    // WIraswasta Menikah
    var _customerIncome = [];
    for(int i=0; i < 13; i++) {
      _customerIncome.add(
        {
          "customerIncomeID": "NEW",
          "incomeFrmlType": i == 0
            ? "S" : i == 1
            ? "+" : i == 2
            ? "=" : i == 3
            ? "-" : i == 4
            ? "=" : i == 5
            ? "-" : i == 6
            ? "-" : i == 7
            ? "=" : i == 8
            ? "-" : i == 9
            ? "=" : i == 10
            ? "-" : i == 11
            ? "=" : i == 12
            ? ""
            : "",
          "incomeType": i == 0
            ? "001" : i == 1
            ? "002" : i == 2
            ? "003" : i == 3
            ? "004" : i == 4
            ? "005" : i == 5
            ? "006" : i == 6
            ? "007" : i == 7
            ? "008" : i == 8
            ? "009" : i == 9
            ? "010" : i == 10
            ? "011" : i == 11
            ? "017" : i == 12
            ? ""
            : "",
          "incomeValue": i == 0
            ? _providerIncome.controllerMonthlyIncome.text : i == 1
            ? _providerIncome.controllerOtherIncome.text : i == 2
            ? _providerIncome.controllerTotalIncome.text : i == 3
            ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
            ? _providerIncome.controllerGrossProfit.text : i == 5
            ? _providerIncome.controllerOperatingCosts.text : i == 6
            ? _providerIncome.controllerOtherCosts.text : i == 7
            ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
            ? _providerIncome.controllerTax.text : i == 9
            ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
            ? _providerIncome.controllerCostOfLiving.text : i == 11
            ? _providerIncome.controllerRestIncome.text : i == 12
            ? _providerIncome.controllerSpouseIncome.text
            : _providerIncome.controllerOtherInstallments.text,
          "dataStatus": "ACTIVE",
          "creationalSpecification": {
            "createdAt": 1605192713593,
            "createdBy": "SYSTEM",
            "modifiedAt": null,
            "modifiedBy": null
          }
        }
      );
    }

    return _customerIncome;
  }

  List mappingCustomerIncomeProfesionalBelumMenikah(BuildContext context) {
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);

    var _customerIncome = [];
    // Profesional Belum Menikah
    for(int i=0; i < 5; i++) {
      _customerIncome.add(
        {
          "customerIncomeID": "NEW",
          "incomeFrmlType": i == 0
            ? "S" : i == 1
            ? "+" : i == 2
            ? "=" : i == 3
            ? "-" : i == 4
            ? "="
            : "",
          "incomeType": i == 0
            ? "001" : i == 1
            ? "002" : i == 2
            ? "003" : i == 3
            ? "011" : i == 4
            ? "017"
            : "",
          "incomeValue": i == 0
            ? _providerIncome.controllerIncome.text : i == 1
            ? _providerIncome.controllerOtherIncome.text : i == 2
            ? _providerIncome.controllerTotalIncome.text : i == 3
            ? _providerIncome.controllerCostOfLiving.text : i == 4
            ? _providerIncome.controllerRestIncome.text
            : _providerIncome.controllerOtherInstallments.text,
          "dataStatus": "ACTIVE",
          "creationalSpecification": {
            "createdAt": 1605192713593,
            "createdBy": "SYSTEM",
            "modifiedAt": null,
            "modifiedBy": null
          }
        }
      );
    }

    return _customerIncome;
  }

  List mappingCustomerIncomeProfesionalMenikah(BuildContext context) {
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);

    var _customerIncome = [];
    // Profesional Menikah
    for(int i=0; i < 6; i++) {
      _customerIncome.add(
        {
          "customerIncomeID": "NEW",
          "incomeFrmlType": i == 0
            ? "S" : i == 1
            ? "+" : i == 2
            ? "+" : i == 3
            ? "=" : i == 4
            ? "-" : i == 5
            ? "="
            : "",
          "incomeType": i == 0
            ? "001" : i == 1
            ? "002" : i == 2
            ? "?" : i == 3
            ? "003" : i == 4
            ? "011" : i == 5
            ? "017"
            : "",
          "incomeValue": i == 0
            ? _providerIncome.controllerIncome.text : i == 1
            ? _providerIncome.controllerSpouseIncome.text : i == 2
            ? _providerIncome.controllerOtherIncome.text : i == 3
            ? _providerIncome.controllerTotalIncome.text : i == 4
            ? _providerIncome.controllerCostOfLiving.text : i == 5
            ? _providerIncome.controllerRestIncome.text
            : _providerIncome.controllerOtherInstallments.text,
          "dataStatus": "ACTIVE",
          "creationalSpecification": {
            "createdAt": 1605192713593,
            "createdBy": "SYSTEM",
            "modifiedAt": null,
            "modifiedBy": null
          }
        }
      );
    }

    return _customerIncome;
  }

  List mappingCustomerIncomeCompany (BuildContext context) {
    var _providerIncome = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);

    var _customerIncome = [];
    for(int i=0; i < 10; i++) {
      _customerIncome.add(
          {
            "customerIncomeID": "NEW",
            "incomeFrmlType": i == 0
                ? "S" : i == 1
                ? "+" : i == 2
                ? "=" : i == 3
                ? "-" : i == 4
                ? "=" : i == 5
                ? "-" : i == 6
                ? "-" : i == 7
                ? "=" : i == 8
                ? "-" : i == 9
                ? "="
                : "",
            "incomeType": i == 0
                ? "001" : i == 1
                ? "002" : i == 2
                ? "003" : i == 3
                ? "004" : i == 4
                ? "005" : i == 5
                ? "006" : i == 6
                ? "007" : i == 7
                ? "008" : i == 8
                ? "" : i == 9
                ? ""
                : "",
            "incomeValue": i == 0
                ? _providerIncome.controllerMonthlyIncome.text : i == 1
                ? _providerIncome.controllerOtherIncome.text : i == 2
                ? _providerIncome.controllerTotalIncome.text : i == 3
                ? _providerIncome.controllerCostOfRevenue.text : i == 4
                ? _providerIncome.controllerGrossProfit.text : i == 5
                ? _providerIncome.controllerOperatingCosts.text : i == 6
                ? _providerIncome.controllerOtherCosts.text : i == 7
                ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
                ? _providerIncome.controllerTax.text : i == 9
                ? _providerIncome.controllerNetProfitAfterTax.text
                : _providerIncome.controllerOtherInstallments.text,
            "dataStatus": "ACTIVE",
            "creationalSpecification": {
              "createdAt": 1605192713593,
              "createdBy": "SYSTEM",
              "modifiedAt": null,
              "modifiedBy": null
            }
          }
      );
    }

    return _customerIncome;
  }

  void getDSR(BuildContext context) async {
    var _providerListOID = Provider.of<ListOidChangeNotifier>(context, listen: false);
    var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerRincian = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);

    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "customerType": _providerListOID.customerType,
      "occupationType": _providerListOID.customerType != "COM" ? _providerPhoto.occupationSelected.KODE : "",
      "sumInstallment": int.parse(_providerInfCreditStructure.controllerInstallment.text),
      "customerIncomes": [
        if(_providerListOID.customerType != "COM") {
          if(_providerPhoto.occupationSelected.KODE == "05" || _providerPhoto.occupationSelected.KODE == "07") {
            if(_providerRincian.maritalStatusSelected.id != "02") {
              mappingCustomerIncomeWiraswastaBelumMenikah(context)
            } else {
              mappingCustomerIncomeWiraswastaMenikah(context)
            }
          } else {
            if(_providerRincian.maritalStatusSelected.id != "02") {
              mappingCustomerIncomeProfesionalBelumMenikah(context)
            } else {
              mappingCustomerIncomeProfesionalMenikah(context)
            }
          }
        } else {
          mappingCustomerIncomeCompany(context)
        }
      ],
      "aoInstallment": 0,
      "obligorID": "",
      "statusAORO": "0"
    });

    final _response = await _http.post(
        "${BaseUrl.urlAcction}adira-acction/acction/service/dsr/calculate",
        body: _body,
        headers: {"Content-Type":"application/json"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result == null){
        showSnackBar("Data tidak ditemukan");
        loadData = false;
      } else{
        this._controllerDebtComparison.clear();
        this._controllerIncomeComparison.clear();
        this._controllerIRR.clear();
        this._controllerDSC.clear();
        this._controllerDebtComparison.text =  _result['dsr'];
        this._controllerIncomeComparison.text =  _result['dir'];
        this._controllerIRR.text = _result['irr'];
        this._controllerDSC.text = _result['dsc'];
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  clearDataInfoCreditIncome(){
    this._autoValidate = false;
    this._controllerDebtComparison.clear();
    this._controllerDSC.clear();
    this._controllerIncomeComparison.clear();
    this._controllerIRR.clear();
  }

  void updateMS2ApplObjectInfStrukturKreditIncome() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturKreditIncome(MS2ApplObjectModel(
      "1",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._controllerDebtComparison.text != "" ? int.parse(this._controllerDebtComparison.text) : 0,
      this._controllerIncomeComparison.text != "" ? int.parse(this._controllerIncomeComparison.text) : 0,
      this._controllerDebtComparison.text != "" ? int.parse(this._controllerDebtComparison.text) : 0,
      this._controllerIRR.text != "" ? int.parse(this._controllerIRR.text) : 0,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ));
  }
}