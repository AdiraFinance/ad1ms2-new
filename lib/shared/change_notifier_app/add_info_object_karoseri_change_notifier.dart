import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/screens/search_company.dart';
import 'package:ad1ms2_dev/screens/search_karoseri.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:ad1ms2_dev/shared/search_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_karoseri_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:ad1ms2_dev/models/company_model.dart';
import 'package:ad1ms2_dev/models/karoseri_model.dart';
import 'package:flutter/material.dart';
import 'package:ad1ms2_dev/models/karoseri_object_model.dart';
import 'package:provider/provider.dart';

class AddInfoObjectKaroseriChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    List<KaroseriObjectModel> _listKaroseriObject = [];
    int _radioPKSKaroseri = 0;
    FormatCurrency _formatCurrency = FormatCurrency();
    CompanyTypeModel _companySelected;
    TextEditingController _controllerCompany = TextEditingController();
    KaroseriModel _karoseriSelected;
    TextEditingController _controllerKaroseri = TextEditingController();
    TextEditingController _controllerJumlahKaroseri = TextEditingController();
    TextEditingController _controllerPrice = TextEditingController();
    TextEditingController _controllerTotalPrice = TextEditingController();
    bool _autoValidate = false;
    RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');

    List<KaroseriObjectModel> get listFormKaroseriObject => _listKaroseriObject;

    int get radioValuePKSKaroseri {
        return _radioPKSKaroseri;
    }

    set radioValuePKSKaroseri(int value) {
        this._radioPKSKaroseri = value;
        notifyListeners();
    }

    void searchCompany(BuildContext context) async {
        CompanyTypeModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchCompanyChangeNotifier(),
                    child: SearchCompany("1"))));
        if (_data != null) {
            this._companySelected = _data;
            this._controllerCompany.text = "${_data.id} - ${_data.desc}";
            notifyListeners();
        }
    }

    CompanyTypeModel get companyModelSelected => _companySelected;

    set companyModelSelected(CompanyTypeModel value) {
      _companySelected = value;
      notifyListeners();
    }

    TextEditingController get controllerCompany {
        return this._controllerCompany;
    }

    set controllerCompany(TextEditingController value) {
        this._controllerCompany = value;
        this.notifyListeners();
    }

    void searchKaroseri(BuildContext context) async {
        KaroseriModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchKaroseriChangeNotifier(),
                    child: SearchKaroseri())));
        if (_data != null) {
            this._karoseriSelected = _data;
            this._controllerKaroseri.text = "${_data.kode} - ${_data.deskripsi}";
            notifyListeners();
        }
    }

    KaroseriModel get karoseriSelected => _karoseriSelected;

    set karoseriSelected(KaroseriModel value) {
        _karoseriSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerKaroseri {
        return this._controllerKaroseri;
    }

    set controllerKaroseri(TextEditingController value) {
        this._controllerKaroseri = value;
        this.notifyListeners();
    }

    TextEditingController get controllerJumlahKaroseri {
        return this._controllerJumlahKaroseri;
    }

    set controllerJumlahKaroseri(TextEditingController value) {
        this._controllerJumlahKaroseri = value;
        this.notifyListeners();
    }

    TextEditingController get controllerPrice {
        return this._controllerPrice;
    }

    set controllerPrice(TextEditingController value) {
        this._controllerPrice = value;
        this.notifyListeners();
    }

    TextEditingController get controllerTotalPrice {
        return this._controllerTotalPrice;
    }

    set controllerTotalPrice(TextEditingController value) {
        this._controllerTotalPrice = value;
        this.notifyListeners();
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    void check(BuildContext context, int flag, int index) {
        final _form = _key.currentState;
        if (flag == 0) {
            if (_form.validate()) {
                Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false)
                    .addListInfoObjKaroseri(KaroseriObjectModel(
                    this.radioValuePKSKaroseri,
                    this._companySelected,
                    this._karoseriSelected,
                    this._controllerJumlahKaroseri.text,
                    this._controllerPrice.text,
                    this._controllerTotalPrice.text,));
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
            if (_form.validate()) {
                Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false)
                    .updateListInfoObjKaroseri(KaroseriObjectModel(
                    this.radioValuePKSKaroseri,
                    this._companySelected,
                    this._karoseriSelected,
                    this._controllerJumlahKaroseri.text,
                    this._controllerPrice.text,
                    this._controllerTotalPrice.text), context, index);
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        }
    }

    void calculatedTotalPrice() {
        var _jumlahKaroseri = _controllerJumlahKaroseri.text.isNotEmpty ? double.parse(_controllerJumlahKaroseri.text.replaceAll(",", "")) : 0.0;
        var _price = _controllerPrice.text.isNotEmpty ? double.parse(_controllerPrice.text.replaceAll(",", "")) : 0.0;
        var _totalPrice = _price * _jumlahKaroseri;
        _controllerTotalPrice.text = _formatCurrency.formatCurrency(_totalPrice.toString());
    }

    Future<void> setValueForEditInfoObjKaroseri(KaroseriObjectModel data) async {
        this._radioPKSKaroseri = data.PKSKaroseri;
        this._companySelected = data.company;
        this._controllerCompany.text = data.company.id +" - "+data.company.desc;
        this._karoseriSelected = data.karoseri;
        this._controllerKaroseri.text = data.karoseri.kode +" - "+data.karoseri.deskripsi;
        this._controllerJumlahKaroseri.text = data.jumlahKaroseri;
        this._controllerPrice.text = data.price;
        this._controllerTotalPrice.text = data.totalPrice;
    }

    FormatCurrency get formatCurrency => _formatCurrency;

    RegExInputFormatter get amountValidator => _amountValidator;
}