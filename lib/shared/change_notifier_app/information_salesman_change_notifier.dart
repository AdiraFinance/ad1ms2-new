import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/infromation_sales_model.dart';
import 'package:ad1ms2_dev/models/ms2_objek_sales_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InformationSalesmanChangeNotifier with ChangeNotifier {
  List<InformationSalesModel> _listInfoSales = [];
  bool _autoValidate = false;
  DbHelper _dbHelper = DbHelper();

  List<InformationSalesModel> get listInfoSales => _listInfoSales;

  void addInfoSales(InformationSalesModel value) {
    this._listInfoSales.add(value);
    notifyListeners();
  }

  void deleteListInfoSales(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus salesman ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listInfoSales.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void updateListInfoSales(int index, InformationSalesModel value) {
    this._listInfoSales[index] = value;
    notifyListeners();
  }

  void clearDataInfoSales(){
    this._autoValidate = false;
    this._listInfoSales.clear();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    _autoValidate = value;
    notifyListeners();
  }

  Future<bool> onBackPress() async{
    if (_listInfoSales.length != 0) {
      autoValidate = false;
    } else {
      autoValidate = true;
    }
    return true;
  }

  void saveToSQLite(BuildContext context) async {
    List<MS2ObjekSalesModel> _listObjekSales = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var noReference = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).controllerReferenceNumber.text;
    for(int i=0; i<_listInfoSales.length; i++){
      _listObjekSales.add(MS2ObjekSalesModel(
        "1",
        _preferences.getString("username"),
        this._listInfoSales[i].salesmanTypeModel != null ? this._listInfoSales[i].salesmanTypeModel.id : "",
        this._listInfoSales[i].salesmanTypeModel != null ? this._listInfoSales[i].salesmanTypeModel.salesmanType : "",
        "",
        this._listInfoSales[i].employeeModel != null ? this._listInfoSales[i].employeeModel.id : "",
        this._listInfoSales[i].employeeModel != null ? this._listInfoSales[i].employeeModel.name : "",
        noReference,
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
        this._listInfoSales[i].employeeHeadModel != null ? this._listInfoSales[i].employeeHeadModel.id : "",
        this._listInfoSales[i].employeeHeadModel != null ? this._listInfoSales[i].employeeHeadModel.employeeHeadName : "",
      ));
    }
    _dbHelper.insertMS2ObjekSales(_listObjekSales);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2ObjekSales();
  }
}
