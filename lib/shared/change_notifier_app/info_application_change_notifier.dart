import 'dart:collection';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/ms2_application_model.dart';
import 'package:ad1ms2_dev/models/proportional_type_of_insurance_model.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';

class InfoAppChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  DateTime _initialDateOrder =
  DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  DateTime _initialSurveyDate =
  DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  TextEditingController _controllerOrderDate = TextEditingController();
  TextEditingController _controllerSurveyAppointmentDate =
  TextEditingController();
  TextEditingController _controllerTotalObject = TextEditingController();
  DateFormat _formatter = DateFormat("dd-MM-yyyy");
  bool _isSignedPK = false;
  JenisKonsepModel _conceptTypeModelSelected;
  ProportionalTypeOfInsuranceModel _proportionalTypeOfInsuranceModelSelected;
  String _numberOfUnitSelected;
  DbHelper _dbHelper = DbHelper();

  List<JenisKonsepModel> _listConceptType = [
    JenisKonsepModel("01", "SATU UNIT MULTI JAMINAN"),
    JenisKonsepModel("02", "MULTI UNIT SATU  JAMINAN"),
    JenisKonsepModel("03", "MULTI UNIT DENGAN JAMINAN DAN TANPA JAMINAN")
  ];

  List<ProportionalTypeOfInsuranceModel> _listProportionalTypeOfInsurance = [
    ProportionalTypeOfInsuranceModel("01", "PRORATE"),
    ProportionalTypeOfInsuranceModel("02", "UNIT"),
  ];

  List<String> _listNumberUnit = [];

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  TextEditingController get controllerOrderDate => _controllerOrderDate;

  TextEditingController get controllerSurveyAppointmentDate =>
      _controllerSurveyAppointmentDate;

  TextEditingController get controllerTotalObject => _controllerTotalObject;

  bool get isSignedPK => _isSignedPK;

  set isSignedPK(bool value) {
    this._isSignedPK = value;
    notifyListeners();
  }

  JenisKonsepModel get conceptTypeModelSelected => _conceptTypeModelSelected;

  set conceptTypeModelSelected(JenisKonsepModel value) {
    this._conceptTypeModelSelected = value;
    notifyListeners();
  }

  ProportionalTypeOfInsuranceModel
  get proportionalTypeOfInsuranceModelSelected =>
      _proportionalTypeOfInsuranceModelSelected;

  set proportionalTypeOfInsuranceModelSelected(
      ProportionalTypeOfInsuranceModel value) {
    this._proportionalTypeOfInsuranceModelSelected = value;
    notifyListeners();
  }

  List<String> get listNumberUnit => _listNumberUnit;

  String get numberOfUnitSelected => _numberOfUnitSelected;

  set numberOfUnitSelected(String value) {
    this._numberOfUnitSelected = value;
    notifyListeners();
  }

  void addNumberOfUnitList() {
    Future.delayed(Duration.zero, () {
      if (this._controllerTotalObject.text == "0") {
        this._controllerTotalObject.clear();
      }
      this._listNumberUnit.clear();
      this._numberOfUnitSelected = null;
      if (this._controllerTotalObject.text != "") {
        int _lengthList = int.parse(this._controllerTotalObject.text);
        for (int i = 0; i < _lengthList; i++) {
          this._listNumberUnit.add("${i + 1}");
        }
        this._numberOfUnitSelected = _listNumberUnit[0];
      }
      notifyListeners();
    });
  }

  UnmodifiableListView<JenisKonsepModel> get listConceptType {
    return UnmodifiableListView(this._listConceptType);
  }

  UnmodifiableListView<ProportionalTypeOfInsuranceModel>
  get listProportionalTypeOfInsurance {
    return UnmodifiableListView(this._listProportionalTypeOfInsurance);
  }

  DateTime get initialDateOrder => _initialDateOrder;

  set initialDateOrder(DateTime value) {
    this._initialDateOrder = value;
  }

  void selectDateOrder(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateOrder,
    //     firstDate: DateTime(
    //         DateTime.now().year, DateTime.now().month - 1, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year + 2, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateOrder = _picked;
    //   this._controllerOrderDate.text = _formatter.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context, _initialDateOrder);
    if (_picked != null) {
      this._initialDateOrder = _picked;
      this._controllerOrderDate.text = _formatter.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  void selectSurveyDate(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialSurveyDate,
    //     firstDate: DateTime(
    //         DateTime.now().year, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year + 2, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialSurveyDate = _picked;
    //   this._controllerSurveyAppointmentDate.text = _formatter.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context, _initialSurveyDate);
    if (_picked != null) {
      this._initialSurveyDate = _picked;
      this._controllerSurveyAppointmentDate.text = _formatter.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  Future<void> clearDataInfoApp() async {
      _autoValidate = false;
      _controllerOrderDate.clear();
      _controllerSurveyAppointmentDate.clear();
      _isSignedPK = false;
      _conceptTypeModelSelected = null;
      _controllerTotalObject.clear();
      _proportionalTypeOfInsuranceModelSelected = null;
  }

  bool _isProporsionalEnable = true;

  bool get isProporsionalEnable => _isProporsionalEnable;

  set isProporsionalEnable(bool value) {
    this._isProporsionalEnable = value;
  }

  void setDefaultValue(BuildContext context){
    if(Provider.of<ListOidChangeNotifier>(context).customerType == "COM"){
      if(this._conceptTypeModelSelected != null){
        if(this._conceptTypeModelSelected.id == "03"){
          isProporsionalEnable = false;
          this._proportionalTypeOfInsuranceModelSelected = this._listProportionalTypeOfInsurance[1];
        }
        else{
          isProporsionalEnable = true;
          this._proportionalTypeOfInsuranceModelSelected = this._listProportionalTypeOfInsurance[1];
        }
      }
      else{
        print("Masuk else");
      }
    }
    else{
      if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).jenisKonsepSelected.id == "03"){
        isProporsionalEnable = false;
        this._proportionalTypeOfInsuranceModelSelected = this._listProportionalTypeOfInsurance[1];
      }
      else{
        isProporsionalEnable = true;
        this._proportionalTypeOfInsuranceModelSelected = this._listProportionalTypeOfInsurance[1];
      }
    }
  }

  void saveToSQLite(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);

    _dbHelper.insertMS2Application(MS2ApplicationModel(
      null,
      null,
      null,
      null,
      this._controllerOrderDate.text != "" ? this._controllerOrderDate.text : "",
      this._initialDateOrder.toString(),
      this._controllerSurveyAppointmentDate.text != "" ? this._controllerSurveyAppointmentDate.text : "",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      _isSignedPK ? 0 : 1,
      null,
      null,
      _listOID.customerType == "COM" ? this.conceptTypeModelSelected.id : _providerFoto.jenisKonsepSelected.id,
      _listOID.customerType == "COM" ? this.conceptTypeModelSelected.text : _providerFoto.jenisKonsepSelected.text,
      this._controllerTotalObject.text != "" ? int.parse(this._controllerTotalObject.text) : 0,
      this.proportionalTypeOfInsuranceModelSelected != null ? this.proportionalTypeOfInsuranceModelSelected.kode : "",
      this.proportionalTypeOfInsuranceModelSelected != null ? this.proportionalTypeOfInsuranceModelSelected.description : "",
      int.parse(this._numberOfUnitSelected),
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      _preferences.getString("username"),
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ));
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2Application();
  }

  void setDataFromSQLite() async{
    var _check = await _dbHelper.selectMS2Application();
    if(_check.isNotEmpty){
      this._controllerOrderDate.text = _check[0]['order_date'];
      this._controllerSurveyAppointmentDate.text = _check[0]['svy_appointment_date'];
      // _check[0]['sign_pk'] == "0" ? this.isSignedPK = true : this.isSignedPK = false;
      this.isSignedPK = _check[0]['sign_pk'] == "0";
      this._controllerTotalObject.text = _check[0]['objt_qty'].toString();
      for(int i=0; i<_listProportionalTypeOfInsurance.length; i++){
        if(_check[0]['prop_insr_type'] == _listProportionalTypeOfInsurance[i].kode){
          _proportionalTypeOfInsuranceModelSelected = _listProportionalTypeOfInsurance[i];
        }
      }
      for(int i=0; i<_listNumberUnit.length; i++){
        if(_check[0]['unit'] == _listNumberUnit[i]){
          this._numberOfUnitSelected = _listNumberUnit[i];
        }
      }
    }
    notifyListeners();
  }
}
