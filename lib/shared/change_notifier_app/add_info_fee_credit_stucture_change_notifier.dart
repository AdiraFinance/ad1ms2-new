import 'dart:collection';

import 'package:ad1ms2_dev/models/fee_type_model.dart';
import 'package:ad1ms2_dev/models/info_fee_credit_structure_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';

class AddInfoFeeCreditStructureChangeNotifier with ChangeNotifier{
  FeeTypeModel _feeTypeSelected;
  FeeTypeModel _feeTypeTemp;
  bool _autoValidate = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  TextEditingController _controllerCashCost = TextEditingController();
  TextEditingController _controllerCreditCost = TextEditingController();
  TextEditingController _controllerTotalCost = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  String _cashCostTemp,_creditCostTemp,_totalCostTemp;

  List<FeeTypeModel> _listFeeType = [
    FeeTypeModel("01", "BIAYA ADMIN"),
    FeeTypeModel("02", "BIAYA PROVISI"),
  ];

  FeeTypeModel get feeTypeSelected => _feeTypeSelected;

  set feeTypeSelected(FeeTypeModel value) {
    this._feeTypeSelected = value;
    notifyListeners();
  }

  FormatCurrency get formatCurrency => _formatCurrency;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  TextEditingController get controllerCashCost => _controllerCashCost;

  TextEditingController get controllerCreditCost => _controllerCreditCost;

  TextEditingController get controllerTotalCost => _controllerTotalCost;

  GlobalKey<FormState> get key => _key;

  UnmodifiableListView<FeeTypeModel> get listFeeType {
    return UnmodifiableListView(this._listFeeType);
  }

  void check(BuildContext context,int flag,int index){
    var _provider =
    Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    final _form = this._key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        _provider.addInfoFeeCreditStructure(
            InfoFeeCreditStructureModel(
                this._feeTypeSelected,
                this._controllerCashCost.text,
                this._controllerCreditCost.text,
                this._controllerTotalCost.text
            )
        );
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    }
    else {
      if (_form.validate()) {
        _provider.updateListInfoFeeCreditStructure(
            index,
            InfoFeeCreditStructureModel(
                this._feeTypeSelected,
                this._controllerCashCost.text,
                this._controllerCreditCost.text,
                this._controllerTotalCost.text
            )
        );
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    }
  }

  Future<void> setValue(InfoFeeCreditStructureModel value) async {
    for (int i = 0; i < this._listFeeType.length; i++) {
      if (value.feeTypeModel.id == this._listFeeType[i].id) {
        this._feeTypeSelected = this._listFeeType[i];
        this._feeTypeTemp = this._listFeeType[i];
      }
    }
    this._controllerTotalCost.text = "${value.totalCost}";
    this._totalCostTemp = this._controllerTotalCost.text;
    this._controllerCreditCost.text = "${value.creditCost}";
    this._creditCostTemp = this._controllerCreditCost.text;
    this._controllerCashCost.text = "${value.cashCost}";
    this._cashCostTemp = this._controllerCashCost.text;
  }

  get totalCostTemp => _totalCostTemp;

  get creditCostTemp => _creditCostTemp;

  String get cashCostTemp => _cashCostTemp;

  FeeTypeModel get feeTypeTemp => _feeTypeTemp;

  void formatting() {
    _controllerCashCost.text = formatCurrency.formatCurrency(_controllerCashCost.text);
    _controllerCreditCost.text = formatCurrency.formatCurrency(_controllerCreditCost.text);
  }

  void calculateTotalCost() {
    var _cashCost = _controllerCashCost.text.isNotEmpty ? double.parse(_controllerCashCost.text.replaceAll(",", "")) : 0.0;
    var _creditCost = _controllerCreditCost.text.isNotEmpty ? double.parse(_controllerCreditCost.text.replaceAll(",", "")) : 0.0;
    var _totalCost = _cashCost + _creditCost;
    _controllerTotalCost.text = _formatCurrency.formatCurrency(_totalCost.toString());
  }
}