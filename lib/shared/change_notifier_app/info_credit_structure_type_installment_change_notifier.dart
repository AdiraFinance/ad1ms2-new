import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/gp_type_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_floating_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_gp_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_irregular_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_stepping_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_loan_detail.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoCreditStructureTypeInstallmentChangeNotifier with ChangeNotifier{
  List<InfoCreditStructureFloatingModel> _listInfoCreditStructureFloatingModel = [];
  List<String> _listInstallment = [];
  String _installmentSelected;
  bool _autoValidateDialogFloating = false;
  TextEditingController _controllerInterestRate = TextEditingController();
  GlobalKey<FormState> _keyFloating = GlobalKey<FormState>();
  DbHelper _dbHelper = DbHelper();

  List<InfoCreditStructureFloatingModel>
      get listInfoCreditStructureFloatingModel =>
          _listInfoCreditStructureFloatingModel;

  void checkFloating(int flag,InfoCreditStructureFloatingModel value,int index,BuildContext context){
    final _form = _keyFloating.currentState;
    if(_form.validate()){
      if(flag == 0){
        addInfoCreditStructureFloating(value,context);
      }
      else{
        updateInfoCreditStructureFloating(value,index,context);
      }
    }
    else{
      autoValidateDialogFloating = true;
    }
  }

  void addInfoCreditStructureFloating(InfoCreditStructureFloatingModel value,BuildContext context){
    this._listInfoCreditStructureFloatingModel.add(value);
    if(autoValidateDialogFloating) autoValidateDialogFloating = false;
    notifyListeners();
    Navigator.pop(context);
  }

  void updateInfoCreditStructureFloating(InfoCreditStructureFloatingModel value,int index,BuildContext context){
    this._listInfoCreditStructureFloatingModel[index] = value;
    if(autoValidateDialogFloating) autoValidateDialogFloating = false;
    notifyListeners();
    Navigator.pop(context);
  }

  void deleteInfoCreditStructureFloating(int index){
    this._listInfoCreditStructureFloatingModel.removeAt(index);
    notifyListeners();
  }

  List<String> get listInstallment => _listInstallment;


  String get installmentSelected => _installmentSelected;

  set installmentSelected(String value) {
    this._installmentSelected = value;
    notifyListeners();
  }

  int _tenor;

  void setListInstallment(BuildContext context){
    var _provider = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    _tenor = int.parse(_provider.periodOfTimeSelected);
    this._listInstallment.clear();
    if(_provider.installmentTypeSelected.id== "05"){
      for(int i = 1; i <= _tenor -1; i++){
        this._listInstallment.add(i.toString());
      }
    }
    else{
      for(int i = 1; i <= _tenor; i++){
        this._listInstallment.add(i.toString());
      }
    }
  }

  bool get autoValidateDialogFloating => _autoValidateDialogFloating;

  set autoValidateDialogFloating(bool value) {
    this._autoValidateDialogFloating = value;
    notifyListeners();
  }

  TextEditingController get controllerInterestRate => _controllerInterestRate;

  GlobalKey<FormState> get keyFloating => _keyFloating;

  void clearDialogFloating(){
    this._installmentSelected = null;
    this._controllerInterestRate.clear();
  }

  void setValueForEditFloating(InfoCreditStructureFloatingModel value){
    this._installmentSelected = value.installment;
    this._controllerInterestRate.text = value.interestRateEffFloat;
  }

  /*
  * GP
  **/
  GPTypeModel _gpTypeSelected;
  GlobalKey<FormState> _keyGP = GlobalKey<FormState>();
  GPTypeModel get gpTypeSelected => _gpTypeSelected;
  TextEditingController _controllerNewTenor = TextEditingController();
  bool _autoValidateDialogGP = false;
  List<InfoCreditStructureGPModel> _listInfoCreditStructureGPModel = [];

  set gpTypeSelected(GPTypeModel value) {
    this._gpTypeSelected = value;
    notifyListeners();
  }

  List<GPTypeModel> _listGPType = [
    GPTypeModel("01", "TETAP"),
    GPTypeModel("02", "MUNDUR")
  ];

  List<GPTypeModel> get listGPType => _listGPType;

  GlobalKey<FormState> get keyGP => _keyGP;

  TextEditingController get controllerNewTenor => _controllerNewTenor;

  bool get autoValidateDialogGP => _autoValidateDialogGP;

  set autoValidateDialogGP(bool value) {
    this._autoValidateDialogGP = value;
    notifyListeners();
  }

  List<InfoCreditStructureGPModel> get listInfoCreditStructureGPModel =>
      _listInfoCreditStructureGPModel;

  void addInfoCreditStructureGPModel(InfoCreditStructureGPModel value,BuildContext context){
    this._listInfoCreditStructureGPModel.add(value);
    if(autoValidateDialogGP) autoValidateDialogGP = false;
    notifyListeners();
    Navigator.pop(context);
  }

  void updateInfoCreditStructureGPModel(InfoCreditStructureGPModel value,int index,BuildContext context){
    this._listInfoCreditStructureGPModel[index] = value;
    if(autoValidateDialogGP) autoValidateDialogGP = false;
    notifyListeners();
    Navigator.pop(context);
  }

  void deleteInfoCreditStructureGPModel(BuildContext context, int index){
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus dokumen unit object ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listInfoCreditStructureGPModel.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void checkGP(int flag,InfoCreditStructureGPModel value,int index,BuildContext context){
    final _form = _keyGP.currentState;
    if(_form.validate()){
      if(flag == 0){
        addInfoCreditStructureGPModel(value,context);
      }
      else{
        updateInfoCreditStructureGPModel(value,index,context);
      }
    }
    else{
      autoValidateDialogGP = true;
    }
  }

  void clearDialogGP(){
    this._gpTypeSelected = null;
    this._installmentSelected = null;
    this._controllerNewTenor.clear();
    notifyListeners();
  }

  void setValueForEditGP(InfoCreditStructureGPModel value){
    this._gpTypeSelected = value.gpTypeModel;
    this._installmentSelected = value.installment;
    this._controllerNewTenor.text = value.tenor;
  }

  void setValueNewTenor(){
      if(_gpTypeSelected.id == "01"){
        this._controllerNewTenor.text = _tenor.toString();
      }
      else{
        this._controllerNewTenor.text = (_tenor+this._listInstallment.length).toString();
      }
      notifyListeners();
  }


  /*
  *Irregular
  **/
  int _valuePeriodOfTimeSelected = 0;

  int get valuePeriodOfTimeSelected => _valuePeriodOfTimeSelected;
  List<InfoCreditStructureIrregularModel> _listInfoCreditStructureIrregularModel = [];

  set valuePeriodOfTimeSelected(int value) {
    this._valuePeriodOfTimeSelected = value;
    notifyListeners();
  }

  List<InfoCreditStructureIrregularModel>
      get listInfoCreditStructureIrregularModel =>
          _listInfoCreditStructureIrregularModel;

  Future<void> setValue(BuildContext context) async{
    _valuePeriodOfTimeSelected = int.parse(Provider.of<InfoCreditStructureChangeNotifier>
      (context,listen: false).periodOfTimeSelected);
    this._listInfoCreditStructureIrregularModel.clear();
    for(int i=0; i < _valuePeriodOfTimeSelected; i++){
      _listInfoCreditStructureIrregularModel.add(
          InfoCreditStructureIrregularModel(TextEditingController(),true,false,GlobalKey<FormState>())
      );
    }
  }

  void checkIrregular(int index){
    final _form = this._listInfoCreditStructureIrregularModel[index].keyIrregular.currentState;
    if(_form.validate()){
      setDisableTf(index);
    }
    else{
      this._listInfoCreditStructureIrregularModel[index].autoValidate = true;
      notifyListeners();
    }
  }

  void setEnableTf(int index){
    this._listInfoCreditStructureIrregularModel[index].isEnable = false;
    notifyListeners();
  }

  void setDisableTf(int index){
    print(this._listInfoCreditStructureIrregularModel[index].controller.text);
    this._listInfoCreditStructureIrregularModel[index].isEnable = true;
    notifyListeners();
  }

  /*
  *BP
  **/
  int _radioValueBalloonPHOrBalloonInstallment = 0;
  bool _autoValidateBP = false;
  bool _visibleTfPercentageInstallment = true;

  int get radioValueBalloonPHOrBalloonInstallment =>
      _radioValueBalloonPHOrBalloonInstallment;

  set radioValueBalloonPHOrBalloonInstallment(int value) {
    this._radioValueBalloonPHOrBalloonInstallment = value;
    setVisibleTfPercentageInstallment(value);
    notifyListeners();
  }

  TextEditingController _controllerInstallmentPercentage = TextEditingController();

  TextEditingController _controllerInstallmentValue = TextEditingController();


  TextEditingController get controllerInstallmentPercentage =>
      _controllerInstallmentPercentage;

  TextEditingController get controllerInstallmentValue =>
      _controllerInstallmentValue;

  bool get autoValidateBP => _autoValidateBP;

  set autoValidateBP(bool value) {
    this._autoValidateBP = value;
    notifyListeners();
  }

  bool get visibleTfPercentageInstallment => _visibleTfPercentageInstallment;

  set visibleTfPercentageInstallment(bool value) {
    this._visibleTfPercentageInstallment = value;
    notifyListeners();
  }

  void setVisibleTfPercentageInstallment(int flag){
    if(flag == 0){
      visibleTfPercentageInstallment = true;
    }
    else{
      visibleTfPercentageInstallment = false;
    }
  }

  /*
  *Stepping
  **/
  List<String> _lisTotalStepping = [];
  String _totalSteppingSelected;
  List<InfoCreditStructureSteppingModel> _listInfoCreditStructureStepping = [];

  List<String> get lisTotalStepping => _lisTotalStepping;

  String get totalSteppingSelected => _totalSteppingSelected;

  set totalSteppingSelected(String value) {
    this._totalSteppingSelected = value;
    notifyListeners();
  }

  List<InfoCreditStructureSteppingModel> get listInfoCreditStructureStepping =>
      _listInfoCreditStructureStepping;

  set listInfoCreditStructureStepping(
      List<InfoCreditStructureSteppingModel> value) {
    this._listInfoCreditStructureStepping = value;
    notifyListeners();
  }

  Future<void> setValueTotalStepping(BuildContext context) async{
    int _totalStepping = int.parse(Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false)
        .periodOfTimeSelected);

    this._lisTotalStepping.clear();
    for(int i = 1; i <= _totalStepping; i++){
      if(_totalStepping % i == 0){
        _lisTotalStepping.add(i.toString());
      }
    }
  }

  void setListDataTotalStepping(String value,BuildContext context){
    int _totalStepping = int.parse(Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false)
        .periodOfTimeSelected);
    int _dataTotalStepping = int.parse(value);
    String _value1 = "";
    int _firstValue = 1;
    double _secondValue = _totalStepping/_dataTotalStepping;
    this._listInfoCreditStructureStepping.clear();
    for(int i=1; i <= _dataTotalStepping; i++){
      if(_totalStepping/_dataTotalStepping == 1){
        _value1 = (i).toString();
      }
      else{
        if(i==1){
          _value1 = "$_firstValue-${_secondValue.floor()}";
        }
        else{
          _value1 = "${_firstValue+=(_totalStepping/_dataTotalStepping).floor()}-${(_secondValue +=(_totalStepping/_dataTotalStepping)).floor()}";
        }
      }
      _listInfoCreditStructureStepping.add(InfoCreditStructureSteppingModel(TextEditingController(), TextEditingController(), true, _value1));
    }
  }

  void clearStepping(){
    this._listInfoCreditStructureStepping.clear();
    this._totalSteppingSelected = null;
  }

  void clearData(){
    this._listInfoCreditStructureFloatingModel.clear();
    this._listInfoCreditStructureGPModel.clear();
    clearStepping();
    this._listInfoCreditStructureIrregularModel.clear();
    this. _controllerInstallmentPercentage.clear();
    this._controllerInstallmentValue.clear();
  }

  void updateMS2ApplObjectInfStrukturKreditGP() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturKreditGP(MS2ApplObjectModel(
      "1",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._gpTypeSelected != null ? int.parse(this._gpTypeSelected.id) : 0,
      this._controllerNewTenor.text,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ));
  }

  void saveToSQLiteInfStrukturKreditStepping() async {
    List<MS2ApplLoanDetailModel> _listApplLoanDetail = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    for(int i=0; i<_listInfoCreditStructureStepping.length; i++){
      _listApplLoanDetail.add(MS2ApplLoanDetailModel(
        "1",
        null,
        this._listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? int.parse(this._listInfoCreditStructureStepping[i].controllerPercentage.text) : 0,
        this._listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(this._listInfoCreditStructureStepping[i].controllerValue.text) : 0,
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
      ));
    }
    _dbHelper.insertMS2ApplLoanDetailStepping(_listApplLoanDetail);
  }

  void saveToSQLiteInfStrukturKreditIrreguler() async {
    List<MS2ApplLoanDetailModel> _listApplLoanDetail = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    for(int i=0; i<_listInfoCreditStructureIrregularModel.length; i++){
      _listApplLoanDetail.add(MS2ApplLoanDetailModel(
        "1",
        i+1,
        null,
        this._listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(this._listInfoCreditStructureIrregularModel[i].controller.text) : 0,
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
      ));
    }
    _dbHelper.insertMS2ApplLoanDetailIrreguler(_listApplLoanDetail);
  }

  void updateMS2ApplObjectInfStrukturKreditBP() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturKreditBP(MS2ApplObjectModel(
      "1",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._radioValueBalloonPHOrBalloonInstallment.toString(),
      this._controllerInstallmentPercentage.text != "" ? this._controllerInstallmentPercentage.text.replaceAll(",", "") : 0,
      double.parse(this._controllerInstallmentValue.text),
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ));
  }

  void updateMS2ApplObjectInfStrukturStepping() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturStepping(MS2ApplObjectModel(
      "1",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this.totalSteppingSelected != null ? int.parse(this.totalSteppingSelected) : 0,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ));
  }
}