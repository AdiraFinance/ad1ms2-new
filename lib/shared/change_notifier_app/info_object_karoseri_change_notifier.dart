import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/ms2_objt_karoseri_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ad1ms2_dev/models/karoseri_object_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoObjectKaroseriChangeNotifier with ChangeNotifier{
    List<KaroseriObjectModel> _listKaroseriObject = [];
    bool _autoValidate = false;
    DbHelper _dbHelper = DbHelper();
    List<KaroseriObjectModel> get listFormKaroseriObject => _listKaroseriObject;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    void addListInfoObjKaroseri(KaroseriObjectModel value) {
        _listKaroseriObject.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void deleteListInfoObjKaroseri(BuildContext context, int index) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans",
                        primaryColor: Colors.black,
                        primarySwatch: primaryOrange,
                        accentColor: myPrimaryColor
                    ),
                    child: AlertDialog(
                        title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                                Text("Apakah kamu yakin menghapus karoseri ini?",),
                            ],
                        ),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    this._listKaroseriObject.removeAt(index);
                                    notifyListeners();
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                );
            }
        );
    }

    void updateListInfoObjKaroseri(
        KaroseriObjectModel mInfoObjKaroseri, BuildContext context, int index) {
        this._listKaroseriObject[index] = mInfoObjKaroseri;
        notifyListeners();
    }

    void clearDataInfoObjectKaroseri(){
        this._autoValidate = false;
        this._listKaroseriObject.clear();
    }

    void saveToSQLite() async {
        List<MS2ObjtKaroseriModel> _listObjtKaroseri = [];
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        for(int i=0; i<_listKaroseriObject.length; i++){
            _listObjtKaroseri.add(MS2ObjtKaroseriModel(
                "1",
                this._listKaroseriObject[i].PKSKaroseri != null ? this._listKaroseriObject[i].PKSKaroseri : "",
                this._listKaroseriObject[i].company != null ? this._listKaroseriObject[i].company.id : "",
                this._listKaroseriObject[i].company != null ? this._listKaroseriObject[i].company.desc : "",
                this._listKaroseriObject[i].karoseri != null ? this._listKaroseriObject[i].karoseri.kode : "",
                this._listKaroseriObject[i].karoseri != null ? this._listKaroseriObject[i].karoseri.deskripsi : "",
                this._listKaroseriObject[i].price != null ? double.parse(this._listKaroseriObject[i].price.replaceAll(",", "")) : 0,
                this._listKaroseriObject[i].jumlahKaroseri != null ? int.parse(this._listKaroseriObject[i].jumlahKaroseri) : 0,
                this._listKaroseriObject[i].totalPrice != null ? double.parse(this._listKaroseriObject[i].totalPrice.replaceAll(",", "")) : 0,
                DateTime.now().toString(),
                _preferences.getString("username"),
                null,
                null,
                1,
                null,
                0,
            ));
        }
        _dbHelper.insertMS2ObjtKaroseri(_listObjtKaroseri);
    }

    Future<bool> deleteSQLite() async{
        return await _dbHelper.deleteMS2ObjtKaroseri();
    }
}