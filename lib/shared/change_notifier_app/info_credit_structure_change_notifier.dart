import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/info_fee_credit_structure_model.dart';
import 'package:ad1ms2_dev/models/installment_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_fee_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/models/payment_method_model.dart';
import 'package:ad1ms2_dev/models/period_of_time_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoCreditStructureChangeNotifier with ChangeNotifier{
  bool _autoValidate = false;
  bool _autoValidateInfoFeeCreditStruture = false;
  InstallmentTypeModel _installmentTypeSelected;
  String _periodOfTimeSelected;
  String _dpFlag;
  PaymentMethodModel _paymentMethodSelected;
  TextEditingController _controllerInterestRateEffective = TextEditingController();
  TextEditingController _controllerInterestRateFlat = TextEditingController();
  TextEditingController _controllerObjectPrice = TextEditingController();
  TextEditingController _controllerKaroseriTotalPrice = TextEditingController();
  TextEditingController _controllerTotalPrice = TextEditingController();
  TextEditingController _controllerNetDP = TextEditingController();
  TextEditingController _controllerBranchDP = TextEditingController();
  TextEditingController _controllerGrossDP = TextEditingController();
  TextEditingController _controllerTotalLoan = TextEditingController();
  TextEditingController _controllerInstallment = TextEditingController();
  TextEditingController _controllerLTV = TextEditingController();
  bool _isVisible = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');
  DbHelper _dbHelper = DbHelper();
  bool _loadData = false;

  bool get autoValidate => _autoValidate;
  bool _flag = false;

  GlobalKey<FormState> _key = GlobalKey<FormState>();

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get autoValidateInfoFeeCreditStructure =>
      _autoValidateInfoFeeCreditStruture;

  set autoValidateInfoFeeCreditStructure(bool value) {
    this._autoValidateInfoFeeCreditStruture = value;
    notifyListeners();
  }

  InstallmentTypeModel get installmentTypeSelected => _installmentTypeSelected;

  set installmentTypeSelected(InstallmentTypeModel value) {
    this._installmentTypeSelected = value;
    // setIsVisible(this._installmentTypeSelected.id);
    notifyListeners();
  }

  String get periodOfTimeSelected => _periodOfTimeSelected;

  set periodOfTimeSelected(String value) {
    this._periodOfTimeSelected = value;
    notifyListeners();
  }

  PaymentMethodModel get paymentMethodSelected => _paymentMethodSelected;

  set paymentMethodSelected(PaymentMethodModel value) {
    this._paymentMethodSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerInterestRateEffective =>
      _controllerInterestRateEffective;

  TextEditingController get controllerInterestRateFlat =>
      _controllerInterestRateFlat;

  TextEditingController get controllerObjectPrice => _controllerObjectPrice;

  TextEditingController get controllerKaroseriTotalPrice =>
      _controllerKaroseriTotalPrice;

  TextEditingController get controllerTotalPrice => _controllerTotalPrice;

  TextEditingController get controllerNetDP => _controllerNetDP;

  TextEditingController get controllerBranchDP => _controllerBranchDP;

  TextEditingController get controllerGrossDP => _controllerGrossDP;

  TextEditingController get controllerTotalLoan => _controllerTotalLoan;

  TextEditingController get controllerInstallment => _controllerInstallment;

  TextEditingController get controllerLTV => _controllerLTV;

  List<InstallmentTypeModel> _listInstallmentType = [
    InstallmentTypeModel("01", "ANNUITY"),
    InstallmentTypeModel("03", "DECLINE-N"),
    InstallmentTypeModel("04", "SEASONAL"),
    InstallmentTypeModel("05", "GRACE PERIODE"),
    InstallmentTypeModel("06", "STEPPING"),
    InstallmentTypeModel("07", "IRREGULAR"),
    InstallmentTypeModel("08", "BALLON PAYMENT"),
    InstallmentTypeModel("09", "STRAIGHTLINE"),
    InstallmentTypeModel("10", "FLOATING"),
  ];

  List<String> _listPeriodOfTime = [];

  List<PaymentMethodModel> _listPaymentMethod = [
    PaymentMethodModel("01", "ADVANCE"),
    PaymentMethodModel("02", "ARREAR"),
  ];

  List<InfoFeeCreditStructureModel> _listInfoFeeCreditStructure = [];


  List<InfoFeeCreditStructureModel> get listInfoFeeCreditStructure =>
      _listInfoFeeCreditStructure;

  UnmodifiableListView<InstallmentTypeModel> get listInstallmentType {
    return UnmodifiableListView(this._listInstallmentType);
  }

  UnmodifiableListView<String> get listPeriodOfTime {
    return UnmodifiableListView(this._listPeriodOfTime);
  }

  UnmodifiableListView<PaymentMethodModel> get listPaymentMethod {
    return UnmodifiableListView(this._listPaymentMethod);
  }

  void addInfoFeeCreditStructure(InfoFeeCreditStructureModel value){
    this._listInfoFeeCreditStructure.add(value);
    if(autoValidateInfoFeeCreditStructure) autoValidateInfoFeeCreditStructure = false;
    notifyListeners();
  }

  void updateListInfoFeeCreditStructure(int index,InfoFeeCreditStructureModel value){
    this._listInfoFeeCreditStructure[index] = value;
    notifyListeners();
  }


  bool get isVisible => _isVisible;

  set isVisible(bool value) {
    this._isVisible = value;
    notifyListeners();
  }

  void setIsVisible(String id){
    if(id != null){
      if(id == "05" || id == "06" || id == "07" || id == "08" || id == "10"){
        isVisible = true;
      }
      else{
        isVisible =  false;
      }
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void clearInfoCreditStructure(){
    this._autoValidate = false;
    this._controllerInterestRateEffective.clear();
    this._controllerInterestRateFlat.clear();
    this._controllerObjectPrice.clear();
    this._controllerKaroseriTotalPrice.clear();
    this._controllerTotalPrice.clear();
    this._controllerNetDP.clear();
    this._controllerBranchDP.clear();
    this._controllerGrossDP.clear();
    this._controllerTotalLoan.clear();
    this._controllerInstallment.clear();
    this._controllerLTV.clear();
    this._installmentTypeSelected = null;
    this._periodOfTimeSelected = null;
    this._listInfoFeeCreditStructure.clear();
  }

  RegExInputFormatter get amountValidator => _amountValidator;

  FormatCurrency get formatCurrency => _formatCurrency;

  void updateMS2ApplObjectInfStrukturKredit() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturKredit(MS2ApplObjectModel(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._installmentTypeSelected != null ? this._installmentTypeSelected.id : "",
      this._installmentTypeSelected != null ? this._installmentTypeSelected.name : "",
      this._periodOfTimeSelected != null ? int.parse(this._periodOfTimeSelected) : 0,
      this._paymentMethodSelected != null ? this._paymentMethodSelected.id : "",
      this._paymentMethodSelected != null ? this._paymentMethodSelected.name : "",
      this._controllerInterestRateEffective.text != "" ? int.parse(this._controllerInterestRateEffective.text) : 0,
      this._controllerInterestRateFlat.text != "" ? int.parse(this._controllerInterestRateFlat.text) : 0,
      this._controllerObjectPrice.text != "" ? int.parse(this._controllerObjectPrice.text) : 0,
      this._controllerKaroseriTotalPrice.text != "" ? double.parse(this._controllerKaroseriTotalPrice.text) : 0,
      this._controllerTotalPrice.text != "" ? int.parse(this._controllerTotalPrice.text) : 0,
      this._controllerNetDP.text != "" ? int.parse(this._controllerNetDP.text) : 0,
      null, // installment decline n
      null,
      this._controllerBranchDP.text != "" ? int.parse(this._controllerBranchDP.text) : 0,
      this._controllerGrossDP.text != "" ? int.parse(this._controllerGrossDP.text) : 0,
      this._controllerTotalLoan.text != "" ? int.parse(this._controllerTotalLoan.text) : 0,
      this._controllerInstallment.text != "" ? int.parse(this._controllerInstallment.text) : 0,
      this._controllerLTV.text != "" ? int.parse(this._controllerLTV.text) : 0, // ltv
      null,
      null,
      null,
      null,
      null,
      null, // gp_type
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ));
  }

  void saveToSQLiteInfStrukturKreditBiaya() async {
    List<MS2ApplFeeModel> _listApplFee = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    for(int i=0; i<_listInfoFeeCreditStructure.length; i++){
      _listApplFee.add(MS2ApplFeeModel(
        this._listInfoFeeCreditStructure[i].feeTypeModel != null ? this._listInfoFeeCreditStructure[i].feeTypeModel.id : "",
        this._listInfoFeeCreditStructure[i].feeTypeModel != null ? this._listInfoFeeCreditStructure[i].feeTypeModel.name : "",
        this._listInfoFeeCreditStructure[i].cashCost != null ? int.parse(this._listInfoFeeCreditStructure[i].cashCost) : 0,
        this._listInfoFeeCreditStructure[i].creditCost != null ? int.parse(this._listInfoFeeCreditStructure[i].creditCost) : 0,
        this._listInfoFeeCreditStructure[i].totalCost != null ? int.parse(this._listInfoFeeCreditStructure[i].totalCost) : 0,
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
      ));
    }
    _dbHelper.insertMS2ApplFee(_listApplFee);
  }

  void deleteSQLite(){
    _dbHelper.deleteMS2ApplFee();
  }

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  String get dpFlag => _dpFlag;

  set dpFlag(String value) {
    this._dpFlag = value;
    notifyListeners();
  }

  void getDataTenor(BuildContext context) async{
    var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    this._listPeriodOfTime.clear();
    this._periodOfTimeSelected = null;
    loadData = true;
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      var _body = jsonEncode(
        {
          "P_OBJECT_GROUP_ID": "${_providerUnitObject.groupObjectSelected.KODE}",
          "P_OBJECT_ID": "${_providerUnitObject.objectSelected.id}",
          "P_OJK_BUSS_DETAIL_ID" : _listOID.customerType == "COM" ? "${_providerUnitObject.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
          "P_OJK_BUSS_ID" : _listOID.customerType == "COM" ? "${_providerUnitObject.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}"
        }
      );
      print(_body);
      final _response = await _http.post(
        // "${BaseUrl.urlOccupation}api/occupation/get-jenis-pekerjaan"
          "${BaseUrl.urlPublic}api/paramcredcalc/get-tenor",
          body: _body,
          headers: {"Content-Type":"application/json"}
      );
      print(_response.statusCode);
      final _result = jsonDecode(_response.body);
      List _data = _result['data'];
      for(int i=0; i < _data.length;i++){
        _listPeriodOfTime.add(_data[i].toString());
      }
      this._loadData = false;
    } catch (e) {
      print(e);
      showSnackBar(e.toString());
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void formatting() {
    _controllerObjectPrice.text = formatCurrency.formatCurrency(_controllerObjectPrice.text);
    _controllerNetDP.text = formatCurrency.formatCurrency(_controllerNetDP.text);
    _controllerBranchDP.text = formatCurrency.formatCurrency(_controllerBranchDP.text);
    _controllerGrossDP.text = formatCurrency.formatCurrency(_controllerGrossDP.text);
  }

  void getDPFlag(BuildContext context) async {
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_OBJECT_GROUP_ID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
    });

    try{
      final _response = await _http.post(
          "${BaseUrl.urlPublic}api/paramcredcalc/get-flag-dp",
          body: _body,
          headers: {"Content-Type":"application/json"}
      );

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result == null) {
          showSnackBar(_result['data']['P_MSG']);
          loadData = false;
        } else {
          _dpFlag = _result['data']['P_FLAG'];
        }
      } else {
        showSnackBar("Error get response ${_response.statusCode}");
        this._loadData = false;
      }
    } catch(e){
      showSnackBar("Error $e");
      this._loadData = false;
    }
    notifyListeners();
  }

  void getMinDP(BuildContext context, String flag) async {
    var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);

    var _rateValue;
    if(flag == "EFF_RATE") {
      _rateValue = double.parse(_controllerInterestRateEffective.text);
    } else if(flag == "FLAT_RATE") {
      _rateValue = double.parse(_controllerInterestRateFlat.text);
    } else {
      _rateValue = 0;
    }

    // SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_OBJECT_GROUP_ID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
      "P_OBJECT_ID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
      "P_OJK_BUSS_DETAIL_ID": _listOID.customerType != "COM"
          ? _providerFoto.jenisKegiatanUsahaSelected != null
            ? _providerFoto.jenisKegiatanUsahaSelected.id : ""
          : _providerUnitObject.businessActivitiesTypeModelSelected != null
            ? _providerUnitObject.businessActivitiesTypeModelSelected.id : "",
      "P_OJK_BUSS_ID": _listOID.customerType != "COM"
          ? _providerFoto.kegiatanUsahaSelected != null
          ? _providerFoto.kegiatanUsahaSelected.id : ""
          : _providerUnitObject.businessActivitiesModelSelected != null
          ? _providerUnitObject.businessActivitiesModelSelected.id : ""
    });

    try{
      final _response = await _http.post(
        "${BaseUrl.urlPublic}api/paramcredcalc/get-minimum-dp",
        body: _body,
        headers: {"Content-Type":"application/json"}
      );

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result == null) {
          showSnackBar(_result['data']['P_MSG']);
          loadData = false;
        } else {
          _controllerNetDP = _result['data']['P_MIN_DP'];
          _controllerGrossDP = _result['data']['P_MIN_DP'];
          calculateCredit(context, flag, _rateValue, _result['data']['P_MIN_DP']);
        }
      } else {
        showSnackBar("Error get response ${_response.statusCode}");
        this._loadData = false;
      }
    } catch(e){
      showSnackBar("Error $e");
      this._loadData = false;
    }
    notifyListeners();
  }

  void calculateCredit(BuildContext context, String flag, String rate, String dp) async {
    var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    // var _providerCollaOto = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context,listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context,listen: false);
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context,listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context,listen: false);
    var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
    var _providerSubsidyDetail = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
    var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);

    SharedPreferences _preferences = await SharedPreferences.getInstance();

    var _rateValue = double.parse(rate);
    var _dpValue = double.parse(dp);
    var _collaType = _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "";

    var _dateFormat = DateFormat("dd-MM-yyyy HH:mm:ss");
    var _dateTime = _dateFormat.format(DateTime.now());

    var orderProductSaleses = [];
    if(_providerSales.listInfoSales.length > 0) {
      for(int i=0; i < _providerSales.listInfoSales.length; i++) {
        orderProductSaleses.add(
          {
            "orderProductSalesID": "NEW",
            "salesType": _providerSales.listInfoSales[i].salesmanTypeModel != null ? _providerSales.listInfoSales[i].salesmanTypeModel.id : "",
            "employeeJob": _providerSales.listInfoSales[i].employeeModel != null ? _providerSales.listInfoSales[i].employeeModel.name : "",
            "employeeID": _providerSales.listInfoSales[i].employeeModel != null ? _providerSales.listInfoSales[i].employeeModel.id : "",
            "employeeHeadID": _providerSales.listInfoSales[i].employeeHeadModel != null ? _providerSales.listInfoSales[i].employeeHeadModel.id : "",
            "referalContractNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
            "creationalSpecification": {
              "createdAt": _dateTime,
              "createdBy": _preferences.getString("username"),
              "modifiedAt": _dateTime,
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          }
        );
      }
    }

    var orderKaroseris = [];
    if(_providerKaroseri.listFormKaroseriObject.length > 0) {
      for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
        orderKaroseris.add(
            {
              "orderKaroseriID": "NEW",
              "isPksKaroseri": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri != -1 ? _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri == 1 ? true : false : false,
              "karoseriCompanyID": _providerKaroseri.listFormKaroseriObject[i].company != null ? _providerKaroseri.listFormKaroseriObject[i].company.id : "",
              "karoseriID": _providerKaroseri.listFormKaroseriObject[i].karoseri != null ? _providerKaroseri.listFormKaroseriObject[i].karoseri.kode : "",
              "karoseriPrice": _providerKaroseri.listFormKaroseriObject[i].price != null ? int.parse(_providerKaroseri.listFormKaroseriObject[i].price) : 0,
              "karoseriQty": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri != null ? int.parse(_providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri) : 0,
              "karoseriTotalPrice": _providerKaroseri.listFormKaroseriObject[i].totalPrice != null ? int.parse(_providerKaroseri.listFormKaroseriObject[i].totalPrice) : 0,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var orderProductInsurances = [];
    if(_providerMajorInsurance.listFormMajorInsurance.length > 0) {
      for(int i=0; i < _providerMajorInsurance.listFormMajorInsurance.length; i++) {
        orderProductInsurances.add(
          {
            "orderProductInsuranceID": "NEW",
            "insuranceCollateralReferenceSpecification": {
              "collateralID": "NEW",
              "collateralTypeID": ""
            },
            "insuranceTypeID": _providerMajorInsurance.listFormMajorInsurance[i].insuranceType != null ? _providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE : "",
            "insuranceCompanyID": _providerMajorInsurance.listFormMajorInsurance[i].company != null ? _providerMajorInsurance.listFormMajorInsurance[i].company.id : "",
            "insuranceProductID": _providerMajorInsurance.listFormMajorInsurance[i].product != null ? _providerMajorInsurance.listFormMajorInsurance[i].product.KODE : "",
            "period": _providerMajorInsurance.listFormMajorInsurance[i].periodType != null ? _providerMajorInsurance.listFormMajorInsurance[i].periodType : 0,
            "type": "",
            "type1": "",
            "type2": "",
            "subType": "",
            "subTypeCoverage": "",
            "insuranceSourceTypeID": "",
            "insuranceValue": _providerMajorInsurance.listFormMajorInsurance[i].coverageValue != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].coverageValue) : 0,
            "upperLimitPercentage": _providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate != null ? int.parse( _providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate) : 0,
            "upperLimitAmount": _providerMajorInsurance.listFormMajorInsurance[i].upperLimit != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].upperLimit) : 0,
            "lowerLimitPercentage": _providerMajorInsurance.listFormMajorInsurance[i].lowerLimitRate != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].lowerLimitRate) : 0,
            "lowerLimitAmount": _providerMajorInsurance.listFormMajorInsurance[i].lowerLimit != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].lowerLimit) : 0,
            "insuranceCashFee": _providerMajorInsurance.listFormMajorInsurance[i].priceCash != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPrice) : 0,
            "insuranceCreditFee": _providerMajorInsurance.listFormMajorInsurance[i].priceCredit != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].priceCredit) : 0,
            "totalSplitFee": _providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit) : 0,
            "totalInsuranceFeePercentage": _providerMajorInsurance.listFormMajorInsurance[i].totalPriceRate != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPriceRate) : 0,
            "totalInsuranceFeeAmount": _providerMajorInsurance.listFormMajorInsurance[i].totalPrice != null ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPrice) : 0,
            "creationalSpecification": {
              "createdAt": _dateTime,
              "createdBy": _preferences.getString("username"),
              "modifiedAt": _dateTime,
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE",
            "insurancePaymentType": "CASH"
          }
        );
      }
    }

    var installmentDetails = [];
    if(_installmentTypeSelected.id == "06") {
      // Stepping
      if(_providerCreditStructureTypeInstallment.listInfoCreditStructureStepping.length > 0) {
        for(int i=0; i < _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping.length; i++) {
          installmentDetails.add(
            {
              "installmentID": "NEW",
              "installmentNumber": 0,
              "percentage": _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerPercentage != null ? _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerPercentage : 0,
              "amount": _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerValue != null ? _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerValue : 0,
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
            }
          );
        }
      }
    } else if(_installmentTypeSelected.id == "07") {
      // Irreguler
      if(_providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel.length > 0) {
        for(int i=0; i < _providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel.length; i++) {
          installmentDetails.add(
            {
              "installmentID": "NEW",
              "installmentNumber": i+1,
              "percentage": 0,
              "amount": _providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel[i] != null ? _providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel[i].controller : 0,
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
            }
          );
        }
      }
    }

    var orderFees = [];
    if(_providerCreditStructure.listInfoFeeCreditStructure.length > 0) {
      for(int i=0; i < _providerCreditStructure.listInfoFeeCreditStructure.length; i++) {
        orderFees.add(
          {
            "orderFeeID": "NEW",
            "feeTypeID": _providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel != null ? _providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id : "",
            "cashFee": _providerCreditStructure.listInfoFeeCreditStructure[i].cashCost != null ? double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].cashCost) : 0,
            "creditFee": _providerCreditStructure.listInfoFeeCreditStructure[i].creditCost != null ? double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].creditCost) : 0,
            "totalFee": _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost != null ? double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].totalCost) : 0,
            "creationalSpecification": {
              "createdAt": _dateTime,
              "createdBy": _preferences.getString("username"),
              "modifiedAt": _dateTime,
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          }
        );
      }
    }

    var orderSubsidies = [];
    if(_providerSubsidy.listInfoCreditSubsidy.length > 0) {
      for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
        orderSubsidies.add(
          {
            "orderSubsidyID": "NEW",
            "subsidyProviderID": "1",
            "subsidyTypeID": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
            "subsidyMethodTypeID": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "",
            "refundAmount": _providerSubsidy.listInfoCreditSubsidy[i].claimValue != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].claimValue) : 0,
            "effectiveRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff) : 0,
            "flatRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat) : 0,
            "dpReal": 1,
            "totalInstallment": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].totalInstallment) : 0,
            "interestRate": 0,
            "creationalSpecification": {
              "createdAt": _dateTime,
              "createdBy": _preferences.getString("username"),
              "modifiedAt": _dateTime,
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE",
            "orderSubsidyDetails": [
              for(int i=0; i < _providerSubsidyDetail.listDetail.length; i++) {
                {
                  "orderSubsidyDetailID": "NEW",
                  "installmentNumber": _providerSubsidyDetail.listDetail[i].installmentIndex != null ? int.parse(_providerSubsidyDetail.listDetail[i].installmentIndex) : 0,
                  "installmentAmount": _providerSubsidyDetail.listDetail[i].installmentSubsidy != null ? double.parse(_providerSubsidyDetail.listDetail[i].installmentSubsidy) : 0,
                  "creationalSpecification": {
                    "createdAt": _dateTime,
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": _dateTime,
                    "modifiedBy": _preferences.getString("username"),
                  },
                  "status": "ACTIVE"
                }
              }
            ]
          }
        );
      }
    }

    loadData = true;
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      var _body = jsonEncode(
        {
          "orderProductID": "NEW",
          "orderProductSpecification": {
            "financingTypeID":  _listOID.customerType != "COM" ? "${_providerFoto.typeOfFinancingModelSelected.financingTypeId}" : "${_providerFoto.typeOfFinancingModelSelected.financingTypeId}",
            "ojkBusinessTypeID": "",
            "ojkBussinessDetailID": _listOID.customerType == "COM" ? _providerUnitObject.businessActivitiesTypeModelSelected.id : _providerFoto.jenisKegiatanUsahaSelected.id,
            "orderUnitSpecification": {
              "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
              "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
              "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
              "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
              "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
              "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
              "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "",
              "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : ""
            },
            "salesGroupID": "00003",
            "orderSourceID": "1",
            "orderSourceName": null,
            "orderProductDealerSpecification": {
              "isThirdParty": true,
              "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
              "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
              "thirdPartyActivity": "1",
              "sentradID": _preferences.getString("SentraD"),
              "unitdID": _preferences.getString("UnitD")
            },
            "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
            "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
            "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
            "applicationUnitContractNumber": null,
            "orderProductSaleses": [orderProductSaleses],
            "orderKaroseris": [orderKaroseris],
            "orderProductInsurances": [orderProductInsurances],
            "orderWmps": [
              {
                "orderWmpID": "NEW",
                "wmpNumber": "",
                "wmpType": "",
                "wmpJob": "",
                "wmpSubsidyTypeID": "",
                "maxRefundAmount": 0,
                "status": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": _dateTime,
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": _dateTime,
                  "modifiedBy": _preferences.getString("username"),
                },
              },
              {
                "orderWmpID": "NEW",
                "wmpNumber": "",
                "wmpType": "",
                "wmpJob": "",
                "wmpSubsidyTypeID": "",
                "maxRefundAmount": 0,
                "status": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": _dateTime,
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": _dateTime,
                  "modifiedBy": _preferences.getString("username"),
                },
              }
            ]
          },
          "orderCreditStructure": {
            "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "",
            "tenor": _providerCreditStructure.periodOfTimeSelected != null ? _providerCreditStructure.periodOfTimeSelected : 0,
            "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "",
            "effectiveRate": _providerCreditStructure.controllerInterestRateEffective != null ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text) : 0,
            "flatRate": _providerCreditStructure.controllerInterestRateFlat != null ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text) : 0,
            "objectPrice": _providerCreditStructure.controllerObjectPrice != null ? double.parse(_providerCreditStructure.controllerObjectPrice.text) : 0,
            "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice != null ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text) : 0,
            "totalPrice": _providerCreditStructure.controllerTotalPrice != null ? double.parse(_providerCreditStructure.controllerTotalPrice.text) : 0,
            "nettDownPayment": _providerCreditStructure.controllerNetDP != null ? double.parse(_providerCreditStructure.controllerNetDP.text) : 0,
            "declineNInstallment": 0,
            "paymentOfYear": 0,
            "branchDownPayment": _providerCreditStructure.controllerBranchDP != null ? double.parse(_providerCreditStructure.controllerBranchDP.text) : 0,
            "grossDownPayment": _providerCreditStructure.controllerGrossDP != null ? double.parse(_providerCreditStructure.controllerGrossDP.text) : 0,
            "totalLoan": _providerCreditStructure.controllerTotalLoan != null ? double.parse(_providerCreditStructure.controllerTotalLoan.text) : 0,
            "installmentAmount": _providerCreditStructure.controllerInstallment != null ? double.parse(_providerCreditStructure.controllerInstallment.text) : 0,
            "ltvRatio": _providerCreditStructure.controllerLTV != null ? double.parse(_providerCreditStructure.controllerLTV.text) : 0,
            "interestAmount": 0,
            "pencairan": 0,
            "realDSR": null,
            "gpType": _providerCreditStructureTypeInstallment.gpTypeSelected != null ? _providerCreditStructureTypeInstallment.gpTypeSelected.id : "",
            "newTenor": _providerCreditStructureTypeInstallment.controllerNewTenor != null ? int.parse(_providerCreditStructureTypeInstallment.controllerNewTenor.text) : 0,
            "totalStepping": _providerCreditStructureTypeInstallment.totalSteppingSelected != null ? _providerCreditStructureTypeInstallment.totalSteppingSelected : 0,
            "installmentBalloonPayment": {
              "balloonType": _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment != -1 ? _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 1 ? true : false : false,
              "lastInstallmentPercentage": _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 1 ? double.parse(_providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text) : 0,
              "lastInstallmentValue": _providerCreditStructureTypeInstallment.controllerInstallmentValue != null ? double.parse(_providerCreditStructureTypeInstallment.controllerInstallmentValue.text) : 0
            },
            "installmentDetails": [installmentDetails],
            "installmentSchedule": {
              "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "",
              "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "",
              "roundingValue": null,
              "installment": 0,
              "installmentRound": 0,
              "installmentMethod": 0,
              "lastKnownOutstanding": 0,
              "minInterest": 0,
              "maxInterest": 0,
              "futureValue": 0,
              "isInstallment": false,
              "balloonInstallment": 0,
              "gracePeriode": 0,
              "gracePeriodes": [],
              "seasonal": 1,
              "steppings": [],
              "installmentScheduleDetails": []
            },
            "firstInstallment": null,
            "dsr": _providerCreditIncome.controllerDebtComparison != null ? double.parse(_providerCreditIncome.controllerDebtComparison.text) : 0,
            "dir": _providerCreditIncome.controllerIncomeComparison != null ? double.parse(_providerCreditIncome.controllerIncomeComparison.text) : 0,
            "dsc": _providerCreditIncome.controllerDSC != null ? double.parse(_providerCreditIncome.controllerDSC.text) : 0,
            "irr": _providerCreditIncome.controllerIRR != null ? double.parse(_providerCreditIncome.controllerIRR.text) : 0
          },
          "orderFees": [orderFees],
          "orderSubsidies": [orderSubsidies],
          "isWithoutColla": false,
          "applSendFlag": null,
          "pelunsanaAmount": null,
          "collateralAutomotives": [
            {
              "collateralID": _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
              "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto == 0 ? true : false,
              "isMultiUnitCollateral": true,
              "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? true : false,
              "identitySpecification": {
                "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityNumber": _providerKolateral.controllerIdentityNumberAuto.text != "" ? _providerKolateral.controllerIdentityNumberAuto.text : "",
                "identityName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : null,
                "fullName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.controllerNamaLengkap : _providerKolateral.controllerNameOnCollateralAuto.text,
                "alias": null,
                "title": null,
                "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text : "",
                "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                "placeOfBirthKabKota": _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text : "",
                "gender": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.radioValueGender == "01" ? "Laki-Laki" : "Perempuan}" : null,
                "identityActiveStart": null,
                "identityActiveEnd": null,
                "isLifetime": false,
                "religion": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.religionSelected.id : null,
                "occupationID": _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                "positionID": null
              },
              "collateralAutomotiveSpecification": {
                "orderUnitSpecification": {
                  "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : "",
                  "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : "",
                  "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : "",
                  "objectBrandID": _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : "",
                  "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : "",
                  "objectModelID": _providerKolateral.controllerUsageObjectModel.text != "" ? _providerKolateral.controllerUsageObjectModel.text : "",
                  "objectUsageID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : "",
                  "objectPurposeID": ""
                },
                "productionYear": _providerKolateral.yearProductionSelected,
                "registrationYear": _providerKolateral.yearRegistrationSelected,
                "isYellowPlate": _providerKolateral.radioValueYellowPlat == 0 ? true : false,
                "isBuiltUp": null,
                "utjSpecification": {
                  "bpkbNumber": null,
                  "chassisNumber": null,
                  "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : "",
                  "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : ""
                },
                "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : "",
                "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text : "",
                "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : "",
                "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? _providerKolateral.controllerHargaJualShowroom.text : "",
                "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                "mpAdira": _providerKolateral.controllerMPAdira.text != "" ? _providerKolateral.controllerMPAdira.text : "",
                "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? int.parse(_providerKolateral.controllerRekondisiFisik.text) : 0,
                "isProper": _providerKolateral.radioValueWorthyOrUnworthy == 0 ? true : false,
                "ltvRatio": _providerKolateral.controllerLTV != null ? double.parse(_providerKolateral.controllerLTV.text) : 0,
                "appraisalSpecification": {
                  "collateralDp": _providerKolateral.controllerDPJaminan.text != "" ? double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")) : 0,
                  "maximumPh": _providerKolateral.controllerPHMax.text != "" ? double.parse(_providerKolateral.controllerPHMax.text.replaceAll(",", "")) : 0,
                  "appraisalPrice": _providerKolateral.controllerTaksasiPriceAutomotive.text != "" ? double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")) : 0,
                  "collateralUtilizationID": ""
                },
                "collateralAutomotiveAdditionalSpecification": {
                  "capacity": 0,
                  "color": null,
                  "manufacturer": null,
                  "serialNumber": null,
                  "invoiceNumber": null,
                  "stnkActiveDate": null,
                  "bpkbAddress": null,
                  "bpkbIdentityTypeID": null
                },
              },
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              }
            }
          ],
          "collateralProperties": [
            {
              "isMultiUnitCollateral": true,
              "isCollaNameSameWithCustomerName": true,
              "collateralID": "NEW",
              "identitySpecification": {
                "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityNumber": _providerKolateral.controllerIdentityNumberAuto.text != "" ? _providerKolateral.controllerIdentityNumberAuto.text : "",
                "identityName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1 ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : null,
                "fullName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1 ? _providerInfoNasabah.controllerNamaLengkap : _providerKolateral.controllerNameOnCollateral.text,
                "alias": null,
                "title": null,
                "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text : "",
                "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                "placeOfBirthKabKota": _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text : "",
                "gender": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1 ? _providerInfoNasabah.radioValueGender == "01" ? "Laki-Laki" : "Perempuan" : null,
                "identityActiveStart": null,
                "identityActiveEnd": null,
                "isLifetime": false,
                "religion": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1 ? _providerInfoNasabah.religionSelected.id : null,
                "occupationID": _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                "positionID": null
              },
              "detailAddresses": [
                {
                  "addressID": "NEW",
                  "foreignBusinessID": "NEW",
                  "addressSpecification": {
                    "koresponden": "1",
                    "matrixAddr": "1",
                    "address": _providerKolateral.controllerAddress != null ? _providerKolateral.controllerAddress : "",
                    "rt": _providerKolateral.controllerRT != null ? _providerKolateral.controllerRT : "",
                    "rw": _providerKolateral.controllerRW != null ? _providerKolateral.controllerRW : "",
                    "provinsiID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.PROV_ID : "",
                    "kabkotID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KABKOT_ID : "",
                    "kecamatanID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KEC_ID : "",
                    "kelurahanID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KEL_ID : "",
                    "zipcode": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.ZIPCODE : "",
                  },
                  "contactSpecification": {
                    "telephoneArea1": "",
                    "telephone1": "",
                    "telephoneArea2": "",
                    "telephone2": "",
                    "faxArea": "",
                    "fax": "",
                    "handphone": "",
                    "email": ""
                  },
                  "addressType": _providerKolateral.addressTypeSelected != null ? _providerKolateral.addressTypeSelected.KODE : "",
                  "creationalSpecification": {
                    "createdAt": _dateTime,
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": _dateTime,
                    "modifiedBy": _preferences.getString("username"),
                  },
                  "status": "ACTIVE"
                }
              ],
              "collateralPropertySpecification": {
                "certificateNumber": _providerKolateral.controllerCertificateNumber != null ? _providerKolateral.controllerCertificateNumber : "",
                "certificateTypeID": _providerKolateral.certificateTypeSelected != null ? _providerKolateral.certificateTypeSelected.id : "",
                "propertyTypeID": _providerKolateral.propertyTypeSelected != null ? _providerKolateral.productTypeSelected.id : "",
                "buildingArea": _providerKolateral.controllerBuildingArea != null ? int.parse(_providerKolateral.controllerBuildingArea.text) : 0,
                "landArea": _providerKolateral.controllerSurfaceArea != null ? int.parse(_providerKolateral.controllerSurfaceArea.text) : 0,
                "appraisalSpecification": {
                  "collateralDp": 1,
                  "maximumPh": _providerKolateral.controllerPHMax != null ? int.parse(_providerKolateral.controllerPHMax.text) : 0,
                  "appraisalPrice": 0,
                  "collateralUtilizationID": "1"
                },
                "positiveFasumDistance": _providerKolateral.controllerJarakFasumPositif != null ? int.parse(_providerKolateral.controllerJarakFasumPositif.text) : 0,
                "negativeFasumDistance": _providerKolateral.controllerJarakFasumNegatif != null ? int.parse(_providerKolateral.controllerJarakFasumNegatif.text) : 0,
                "landPrice": _providerKolateral.controllerHargaTanah != null ? double.parse(_providerKolateral.controllerHargaTanah.text) : 0,
                "njopPrice": _providerKolateral.controllerHargaNJOP != null ? int.parse(_providerKolateral.controllerHargaNJOP.text) : 0,
                "buildingPrice": _providerKolateral.controllerHargaBangunan != null ? double.parse(_providerKolateral.controllerHargaBangunan.text) : 0,
                "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : "",
                "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : "",
                "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : "",
                "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : "",
                "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : "",
                "isCarPassable": _providerKolateral.radioValueAccessCar != -1 ? _providerKolateral.radioValueAccessCar == 0 ? true : false : false,
                "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius != null ? int.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text) : 0,
                "sifatJaminan": _providerKolateral.controllerSifatJaminan != null ? _providerKolateral.controllerSifatJaminan.text : "",
                "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan != null ? _providerKolateral.controllerBuktiKepemilikan.text : "",
                "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate != null ? _providerKolateral.controllerCertificateReleaseDate.text : "",
                "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear != null ? int.parse(_providerKolateral.controllerCertificateReleaseYear.text) : 0,
                "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak != null ? _providerKolateral.controllerNamaPemegangHak.text : "",
                "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur != null ? _providerKolateral.controllerNoSuratUkur.text : "",
                "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter != null ? _providerKolateral.controllerDateOfMeasuringLetter.text : "",
                "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter != null ? _providerKolateral.controllerCertificateOfMeasuringLetter.text : "",
                "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku != null ? _providerKolateral.controllerMasaHakBerlaku.text : "",
                "noImb": _providerKolateral.controllerNoIMB != null ? _providerKolateral.controllerNoIMB.text : "",
                "tgglImb": _providerKolateral.controllerDateOfIMB != null ? _providerKolateral.controllerDateOfIMB.text : "",
                "luasBangunanImb": _providerKolateral.controllerLuasBangunanIMB != null ? int.parse(_providerKolateral.controllerLuasBangunanIMB.text) : 0,
                "ltvRatio": _providerKolateral.controllerLTV != null ? int.parse(_providerKolateral.controllerLTV.text) : 0,
                "letakTanah": "",
                "propertyGroupObjtID": "",
                "propertyObjtID": ""
              },
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
            }
          ],
          "orderProductAdditionalSpecification": {
            "virtualAccountID": null,
            "virtualAccountValue": 0,
            "cashingPurposeID": null,
            "cashingTypeID": null
          },
          "autoDebitSpecification": {
            "isAutoDebit": null,
            "bankID": "",
            "accountNumber": "",
            "accountBehalf": "",
            "accountPurposeTypeID": "",
            "downPaymentSourceID": ""
          },
          "orderProductProcessingCompletion": {
            "lastKnownOrderProductState": "APPLICATION_UNIT",
            "nextKnownOrderProductState": "APPLICATION_KAROSERI"
          },
          "tacSpesification": {
            "tacActual": 0,
            "tacMax": 0
          },
          "isSaveKaroseri": true,
          "creationalSpecification": {
            "createdAt": _dateTime,
            "createdBy": _preferences.getString("username"),
            "modifiedAt": _dateTime,
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        }
      );
      print(_body);

      final _response = await _http.post(
        // "${BaseUrl.urlOccupation}api/occupation/get-jenis-pekerjaan"
          "${BaseUrl.urlAcction}adira-acction/acction/service/installment/calculate/structure/credit/rate=$_rateValue&rateType=$flag&downPayment=$_dpValue&collateralType=$_collaType",
          body: _body,
          headers: {"Content-Type":"application/json"}
      );

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        this._controllerInterestRateEffective.text = _result['effectiveRate'];
        this._controllerInterestRateFlat.text = _result['flatRate'];
        this._controllerGrossDP.text = _result['grossDownPayment'];
        this._controllerObjectPrice = _result['objectPrice'];
        this._controllerKaroseriTotalPrice = _result['karoseriTotalPrice'];
        this._controllerTotalPrice = _result['totalPrice'];
        this._controllerNetDP = _result['nettDownPayment'];
        this._controllerBranchDP = _result['branchDownPayment'];
        this._controllerTotalLoan = _result['totalLoan'];
        this._controllerInstallment = _result['installmentAmount'];
        this._controllerLTV = _result['ltvRatio'];
      }
      else{
        showSnackBar("Error ${_response.statusCode}");
        this._loadData = false;
      }
      // final _result = jsonDecode(_response.body);
      // List _data = _result['data'];
      // for(int i=0; i < _data.length;i++){
      //   _listPeriodOfTime.add(_data[i].toString());
      // }
    } catch (e) {
      showSnackBar(e.toString());
      this._loadData = false;
    }
    notifyListeners();
  }
}