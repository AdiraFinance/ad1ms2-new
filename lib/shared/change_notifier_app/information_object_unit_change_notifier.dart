import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/activities_model.dart';
import 'package:ad1ms2_dev/models/brand_object_model.dart';
import 'package:ad1ms2_dev/models/brand_type_model_genre_model.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/group_object_model.dart';
import 'package:ad1ms2_dev/models/group_sales_model.dart';
import 'package:ad1ms2_dev/models/matriks_dealer_model.dart';
import 'package:ad1ms2_dev/models/model_object_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/models/object_model.dart';
import 'package:ad1ms2_dev/models/object_purpose_model.dart';
import 'package:ad1ms2_dev/models/object_type_model.dart';
import 'package:ad1ms2_dev/models/object_usage_model.dart';
import 'package:ad1ms2_dev/models/product_type_model.dart';
import 'package:ad1ms2_dev/models/program_model.dart';
import 'package:ad1ms2_dev/models/reference_number_model.dart';
import 'package:ad1ms2_dev/models/rehab_type_model.dart';
import 'package:ad1ms2_dev/models/source_order_model.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/models/third_party_model.dart';
import 'package:ad1ms2_dev/models/third_party_type_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/screens/search_activity.dart';
import 'package:ad1ms2_dev/screens/search_brand_object.dart';
import 'package:ad1ms2_dev/screens/search_group_object.dart';
import 'package:ad1ms2_dev/screens/search_group_sales.dart';
import 'package:ad1ms2_dev/screens/search_matriks_dealer.dart';
import 'package:ad1ms2_dev/screens/search_model_object.dart';
import 'package:ad1ms2_dev/screens/search_object.dart';
import 'package:ad1ms2_dev/screens/search_object_purpose.dart';
import 'package:ad1ms2_dev/screens/search_object_type.dart';
import 'package:ad1ms2_dev/screens/search_product_type.dart';
import 'package:ad1ms2_dev/screens/search_program.dart';
import 'package:ad1ms2_dev/screens/search_reference_number.dart';
import 'package:ad1ms2_dev/screens/search_rehab_type.dart';
import 'package:ad1ms2_dev/screens/search_source_order.dart';
import 'package:ad1ms2_dev/screens/search_source_order_name.dart';
import 'package:ad1ms2_dev/screens/search_third_party.dart';
import 'package:ad1ms2_dev/screens/search_third_party_type.dart';
import 'package:ad1ms2_dev/screens/search_usage_object.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_activity_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_brand_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_group_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_group_sales_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_matriks_dealer_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_model_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_purpose_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_usage_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_product_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_program_change_notfier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_rehab_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_third_party_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_third_party_type_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InformationObjectUnitChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  List<FinancingTypeModel> _listTypeOfFinancing =
      TypeOfFinancingList().financingTypeList;
  FinancingTypeModel _typeOfFinancingModelSelected;
  KegiatanUsahaModel _businessActivitiesModelSelected;
  JenisKegiatanUsahaModel _businessActivitiesTypeModelSelected;
  ModelObjectModel _modelObjectSelected;
  GroupObjectModel _groupObjectSelected;
  ObjectModel _objectSelected;
  ProductTypeModel _productTypeSelected;
  BrandObjectModel _brandObjectSelected;
  ObjectTypeModel _objectTypeSelected;
  ObjectPurposeModel _objectPurposeSelected;
  GroupSalesModel _groupSalesSelected;
  SourceOrderModel _sourceOrderSelected;
  SourceOrderNameModel _sourceOrderNameSelected;
  ThirdPartyTypeModel _thirdPartyTypeSelected;
  ThirdPartyModel _thirdPartySelected;
  MatriksDealerModel _matriksDealerSelected;
  ProgramModel _programSelected;
  RehabTypeModel _rehabTypeSelected;
  ReferenceNumberModel _referenceNumberModel;
  ObjectUsageModel _objectUsageModel;
  ActivitiesModel _activitiesModel;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _prodMatrixId;
  DbHelper _dbHelper = DbHelper();

  List<KegiatanUsahaModel> _listBusinessActivities = [
    // KegiatanUsahaModel(1, "Investasi"),
    // KegiatanUsahaModel(2, "Multiguna"),
    // KegiatanUsahaModel(3, "Fasilitas Dana"),
  ];

  List<JenisKegiatanUsahaModel> _listBusinessActivitiesType = [
    // JenisKegiatanUsahaModel(3, "PPSA Pendapatan Non Tetap"),
    // JenisKegiatanUsahaModel(3, "PPSA Kelembagaan"),
    // JenisKegiatanUsahaModel(3, "Fin lease Pendapatan Non Tetap"),
    // JenisKegiatanUsahaModel(3, "Fin Lease Kelembagaan")
  ];

  TextEditingController _controllerGroupObject = TextEditingController();
  TextEditingController _controllerObject = TextEditingController();
  TextEditingController _controllerModelObject = TextEditingController();
  TextEditingController _controllerObjectType = TextEditingController();
  TextEditingController _controllerBrandObject = TextEditingController();
  TextEditingController _controllerTypeProduct = TextEditingController();
  TextEditingController _controllerObjectPurpose = TextEditingController();
  TextEditingController _controllerGroupSales = TextEditingController();
  TextEditingController _controllerSourceOrder = TextEditingController();
  TextEditingController _controllerSourceOrderName = TextEditingController();
  TextEditingController _controllerThirdPartyType = TextEditingController();
  TextEditingController _controllerThirdParty = TextEditingController();
  TextEditingController _controllerMatriksDealer = TextEditingController();
  TextEditingController _controllerDetailModel = TextEditingController();
  TextEditingController _controllerUsageObjectModel = TextEditingController();
  TextEditingController _controllerProgram = TextEditingController();
  TextEditingController _controllerRehabType = TextEditingController();
  TextEditingController _controllerReferenceNumber = TextEditingController();
  TextEditingController _controllerSentraD = TextEditingController();
  TextEditingController _controllerUnitD = TextEditingController();

  TextEditingController _controllerKegiatan = TextEditingController();
  TextEditingController _controllerWMP = TextEditingController();


  TextEditingController get controllerKegiatan => _controllerKegiatan;
  TextEditingController get controllerWMP => _controllerWMP;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  MatriksDealerModel get matriksDealerSelected => _matriksDealerSelected;

  set matriksDealerSelected(MatriksDealerModel value) {
    this._matriksDealerSelected = value;
    notifyListeners();
  }

  FinancingTypeModel get typeOfFinancingModelSelected =>
      _typeOfFinancingModelSelected;

  set typeOfFinancingModelSelected(FinancingTypeModel value) {
    this._typeOfFinancingModelSelected = value;
    notifyListeners();
  }

  KegiatanUsahaModel get businessActivitiesModelSelected =>
      _businessActivitiesModelSelected;

  set businessActivitiesModelSelected(KegiatanUsahaModel value) {
    this._businessActivitiesModelSelected = value;
    notifyListeners();
  }

  JenisKegiatanUsahaModel get businessActivitiesTypeModelSelected =>
      _businessActivitiesTypeModelSelected;

  set businessActivitiesTypeModelSelected(JenisKegiatanUsahaModel value) {
    this._businessActivitiesTypeModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<FinancingTypeModel> get listTypeOfFinancing {
    return UnmodifiableListView(this._listTypeOfFinancing);
  }

  UnmodifiableListView<KegiatanUsahaModel> get listBusinessActivities {
    return UnmodifiableListView(this._listBusinessActivities);
  }

  UnmodifiableListView<JenisKegiatanUsahaModel>
      get listBusinessActivitiesType {
    return UnmodifiableListView(this._listBusinessActivitiesType);
  }

  TextEditingController get controllerGroupObject => _controllerGroupObject;

  TextEditingController get controllerObject => _controllerObject;

  TextEditingController get controllerTypeProduct => _controllerTypeProduct;

  TextEditingController get controllerBrandObject => _controllerBrandObject;

  TextEditingController get controllerObjectType => _controllerObjectType;

  TextEditingController get controllerModelObject => _controllerModelObject;

  TextEditingController get controllerObjectPurpose => _controllerObjectPurpose;

  TextEditingController get controllerGroupSales => _controllerGroupSales;

  TextEditingController get controllerSourceOrder => _controllerSourceOrder;

  TextEditingController get controllerSourceOrderName =>
      _controllerSourceOrderName;

  TextEditingController get controllerThirdPartyType =>
      _controllerThirdPartyType;

  TextEditingController get controllerThirdParty => _controllerThirdParty;

  TextEditingController get controllerDetailModel => _controllerDetailModel;

  TextEditingController get controllerProgram => _controllerProgram;

  TextEditingController get controllerRehabType => _controllerRehabType;

  TextEditingController get controllerReferenceNumber =>
      _controllerReferenceNumber;

  TextEditingController get controllerSentraD => _controllerSentraD;

  TextEditingController get controllerUnitD => _controllerUnitD;

  TextEditingController get controllerUsageObjectModel =>
      _controllerUsageObjectModel;

  set groupObjectSelected(GroupObjectModel value) {
    this._groupObjectSelected = value;
  }

  GroupObjectModel get groupObjectSelected => _groupObjectSelected;

  ModelObjectModel get modelObjectSelected => _modelObjectSelected;

  ProductTypeModel get productTypeSelected => _productTypeSelected;

  BrandObjectModel get brandObjectSelected => _brandObjectSelected;

  ObjectTypeModel get objectTypeSelected => _objectTypeSelected;

  ObjectModel get objectSelected => _objectSelected;

  ObjectUsageModel get objectUsageModel => _objectUsageModel;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  ReferenceNumberModel get referenceNumberModel => _referenceNumberModel;

  set referenceNumberModel(ReferenceNumberModel value) {
    this._referenceNumberModel = value;
  }

  void searchModelObject(BuildContext context,String flag,int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchModelObjectChangeNotifier(),
                child: SearchModelObject(flag: flag,flagByBrandModelType: flagByBrandModelType,))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      // this._objectUsageModel = data.objectUsageModel;
      // this._controllerUsageObjectModel.text = "${_objectUsageModel.id} - ${_objectUsageModel.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchGroupObject(BuildContext context,String flag) async {
    GroupObjectModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchGroupObjectChangeNotifier(),
                child: SearchGroupObject(flag: flag,))));
    if (data != null) {
      this._groupObjectSelected = data;
      this._controllerGroupObject.text = "${data.KODE} - ${data.DESKRIPSI}";
      // print("test ${data.KODE}");
      Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).setIsVisible(data.KODE);
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObject(BuildContext context) async {
    ObjectModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectChangeNotifier(),
                child: SearchObject(kodeObject: this._groupObjectSelected.KODE))));
    if (data != null) {
      this._objectSelected = data;
      this._controllerObject.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  String get prodMatrixId => _prodMatrixId;

  set prodMatrixId(String value) {
    this._prodMatrixId = value;
  }

  void searchProductType(BuildContext context,String flag) async {
    ProductTypeModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchProductTypeChangeNotifier(),
                child: SearchProductType(flag: flag,))));
    if (data != null) {
      this._productTypeSelected = data;
      this._controllerTypeProduct.text = "${data.id} - ${data.name}";
      notifyListeners();
      _getProdMatrixID(context, flag);
    } else {
      return;
    }
  }

  void searchBrandObject(BuildContext context,String flag,int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBrandObjectChangeNotifier(),
                child: SearchBrandObject(flag: flag,flagByBrandModelType: flagByBrandModelType,))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      // this._objectUsageModel = data.objectUsageModel;
      // this._controllerUsageObjectModel.text = "${_objectUsageModel.id} - ${_objectUsageModel.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObjectType(BuildContext context,String flag,int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectTypeChangeNotifier(),
                child: SearchObjectType(flagByBrandModelType: flagByBrandModelType,flag: flag,))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      // this._objectUsageModel = data.objectUsageModel;
      // this._controllerUsageObjectModel.text = "${_objectUsageModel.id} - ${_objectUsageModel.name}";
      notifyListeners();
    } else {
      return;
    }
  }


  ObjectPurposeModel get objectPurposeSelected => _objectPurposeSelected;

  set objectPurposeSelected(ObjectPurposeModel value) {
    this._objectPurposeSelected = value;
    notifyListeners();
  }

  void searchObjectPurpose(BuildContext context) async {
    ObjectPurposeModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectPurposeChangeNotifier(),
                child: SearchObjectPurpose())));
    if (data != null) {
      this._objectPurposeSelected = data;
      this._controllerObjectPurpose.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchGroupSales(BuildContext context) async {
    GroupSalesModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchGroupSalesChangeNotifier(),
                child: SearchGroupSales())));
    if (data != null) {
      this._groupSalesSelected = data;
      this._controllerGroupSales.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  SourceOrderModel get sourceOrderSelected => _sourceOrderSelected;

  set sourceOrderSelected(SourceOrderModel value) {
    this._sourceOrderSelected = value;
    notifyListeners();
  }

  void searchSourceOrder(BuildContext context) async {
    SourceOrderModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchSourceOrderChangeNotifier(),
                child: SearchSourceOrder())));
    if (data != null) {
      this._sourceOrderSelected = data;
      this._controllerSourceOrder.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }


  GroupSalesModel get groupSalesSelected => _groupSalesSelected;

  ActivitiesModel get activitiesModel => _activitiesModel;

  SourceOrderNameModel get sourceOrderNameSelected => _sourceOrderNameSelected;

  set sourceOrderNameSelected(SourceOrderNameModel value) {
    this._sourceOrderNameSelected = value;
  }

  void searchSourceOrderName(BuildContext context) async {
    SourceOrderNameModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchSourceOrderNameChangeNotifier(),
                child: SearchSourceOrderName())));
    if (data != null) {
      this._sourceOrderNameSelected = data;
      this._controllerSourceOrderName.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }


  ThirdPartyTypeModel get thirdPartyTypeSelected => _thirdPartyTypeSelected;

  set thirdPartyTypeSelected(ThirdPartyTypeModel value) {
    this._thirdPartyTypeSelected = value;
  }

  void searchThirdPartyType(BuildContext context) async {
    ThirdPartyTypeModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchThirdPartyTypeChangeNotifier(),
                child: SearchThirdPartyType())));
    if (data != null) {
      this._thirdPartyTypeSelected = data;
      this._controllerThirdPartyType.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  ThirdPartyModel get thirdPartySelected => _thirdPartySelected;

  set thirdPartySelected(ThirdPartyModel value) {
    this._thirdPartySelected = value;
  }

  void searchThirdParty(BuildContext context) async {
    ThirdPartyModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchThirdPartyChangeNotifier(),
                child: SearchThirdParty())));
    if (data != null) {
      this._thirdPartySelected = data;
      this._controllerThirdParty.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
      getMatriksDealer(context);
    } else {
      return;
    }
  }

  void searchMatriksDealer(BuildContext context) async {
    MatriksDealerModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchMatriksDealerChangeNotifier(),
                child: SearchMatriksDealer())));
    if (data != null) {
      this._matriksDealerSelected = data;
      this._controllerMatriksDealer.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchProgram(BuildContext context) async {
    ProgramModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchProgramChangeNotifier(),
                child: SearchProgram())));
    if (data != null) {
      this._programSelected = data;
      this._controllerProgram.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchRehabType(BuildContext context) async {
    RehabTypeModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchRehabTypeChangeNotifier(),
                child: SearchRehabType())));
    if (data != null) {
      this._rehabTypeSelected = data;
      this._controllerRehabType.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchReferenceNumber(BuildContext context) async {
    ReferenceNumberModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchReferenceNumberChangeNotifier(),
                child: SearchReferenceNumber())));
    if (data != null) {
      this._referenceNumberModel = data;
      this._controllerReferenceNumber.text = "${data.noPK}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchUsageObject(BuildContext context) async {
    ObjectUsageModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectUsageChangeNotifier(),
                child: SearchUsageObject())));
    if (data != null) {
      this._objectUsageModel = data;
      this._controllerUsageObjectModel.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchActivities(BuildContext context) async {
    ActivitiesModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchActivityChangeNotifier(),
                child: SearchActivity())));
    if (data != null) {
      this._activitiesModel = data;
      this._controllerKegiatan.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void clearDataInfoUnitObject(){
      _autoValidate = false;
      _typeOfFinancingModelSelected = null;
      _businessActivitiesModelSelected = null;
      _businessActivitiesTypeModelSelected = null;
      _controllerGroupObject.clear();
      _controllerObject.clear();
      _controllerTypeProduct.clear();
      _controllerBrandObject.clear();
      _controllerObjectType.clear();
      _controllerModelObject.clear();
      _controllerDetailModel.clear();
      _controllerUsageObjectModel.clear();
      _controllerObjectPurpose.clear();
      _controllerGroupSales.clear();
      _controllerSourceOrder.clear();
      _controllerSourceOrderName.clear();
      _controllerThirdPartyType.clear();
      _controllerThirdParty.clear();
      _controllerSentraD.clear();
      _controllerUnitD.clear();
      _controllerProgram.clear();
      _controllerRehabType.clear();
      _controllerReferenceNumber.clear();

      _controllerKegiatan.clear();
      _controllerWMP.clear();
  }

  void getListBusinessActivitiesType() async{
    try{
      this._listBusinessActivitiesType.clear();
      this._businessActivitiesTypeModelSelected = null;
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      var _body = jsonEncode({
        "P_FIN_TYPE" : this._typeOfFinancingModelSelected.financingTypeId
      });
      final _response = await _http.post(
          "${BaseUrl.urlPublic}api/parameter/get-jenis-kegiatan-usaha",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.length > 0){
        for(int i=0; i<_data.length; i++){
          _listBusinessActivitiesType.add(JenisKegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
        }
        loadData = false;
      }
      else{
        loadData = false;
        showSnackBar("Jenis kegiatan usaha tidak ditemukan");
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      showSnackBar("Timeout Connection");
    }
    on SocketException catch(_){
      loadData = false;
      showSnackBar("Please check connection or contact server");
    }
    catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }

  void getBusinessActivities(String flag,BuildContext context) async{
    this._listBusinessActivities.clear();
    this._businessActivitiesModelSelected = null;
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_FIN_TYPE" : flag == "COM"
          ? this._typeOfFinancingModelSelected.financingTypeId
          : Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId,
    });
    try{
      final _response = await _http.post(
          "${BaseUrl.urlPublic}api/parameter/get-kegiatan-usaha",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.length > 0){
        for(int i=0; i < _data.length; i++){
          this._listBusinessActivities.add(KegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
        }
        loadData = false;
      }
      else{
        loadData = false;
        showSnackBar("Kegiatan usaha data tidak ditemukan");
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      showSnackBar("Timeout Connection");
    }
    on SocketException catch(_){
      loadData = false;
      showSnackBar("Please check connection or contact server");
    }
    catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void _getProdMatrixID(BuildContext context,String flag) async{
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode(
        {
          "P_JENIS_PRODUK_ID": "${this._productTypeSelected.id}",//Jenis produk
          "P_OBJECT_GROUP_ID": "${this._groupObjectSelected.KODE}",// grup objek
          "P_OBJECT_ID": "${this._objectSelected.id}",// objek
          "P_OJK_BUSS_DETAIL_ID" : flag == "COM" ? "${_providerObjectUnit.businessActivitiesModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
          "P_OJK_BUSS_ID" : flag == "COM" ? "${_providerObjectUnit.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}"
        }
    );
    try{
      final _response = await _http.post(
          "${BaseUrl.urlPublic}api/parameter/get-product-matrix",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 30));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result['data'];
        if(_data.isEmpty){
          showSnackBar("Gagal mendapat Prod Matrix ID");
        }
        else{
          print(_result['data']);
          this._prodMatrixId = _data[0]['PROD_MATRIX_ID'];
          notifyListeners();
        }
      }else{
        showSnackBar("Error ${_response.statusCode}");
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Timeout connection");
    }
    catch(e){
      showSnackBar("Error ${e.toString()}");
    }
  }

  ProgramModel get programSelected => _programSelected;

  set programSelected(ProgramModel value) {
    this._programSelected = value;
  }

  RehabTypeModel get rehabTypeSelected => _rehabTypeSelected;

  set rehabTypeSelected(RehabTypeModel value) {
    this._rehabTypeSelected = value;
  }

  void saveToSQLite(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);

    _dbHelper.insertMS2ApplObject(MS2ApplObjectModel(
      "1",
      _listOID.customerType == "COM" ? this._typeOfFinancingModelSelected.financingTypeId : _providerFoto.typeOfFinancingModelSelected.financingTypeId,
      _listOID.customerType == "COM" ? this._businessActivitiesModelSelected.id.toString() : _providerFoto.kegiatanUsahaSelected.id.toString(),
      _listOID.customerType == "COM" ? this._businessActivitiesModelSelected.text : _providerFoto.kegiatanUsahaSelected.text,
      _listOID.customerType == "COM" ? this._businessActivitiesTypeModelSelected.id.toString() : _providerFoto.jenisKegiatanUsahaSelected.id.toString(),
      _listOID.customerType == "COM" ? this._businessActivitiesTypeModelSelected.text : _providerFoto.jenisKegiatanUsahaSelected.text,
      this._groupObjectSelected != null ? this._groupObjectSelected.KODE : "",
      this._groupObjectSelected != null ? this._groupObjectSelected.DESKRIPSI : "",
      this._objectSelected != null ? this._objectSelected.id : "",
      this._objectSelected != null ? this._objectSelected.name : "",
      this._productTypeSelected != null ? this._productTypeSelected.id : "",
      this._productTypeSelected != null ? this._productTypeSelected.name : "",
      this._brandObjectSelected != null ? this._brandObjectSelected.id : "",
      this._brandObjectSelected != null ? this._brandObjectSelected.name : "",
      this._objectTypeSelected != null ? this._objectTypeSelected.id : "",
      this._objectTypeSelected != null ? this._objectTypeSelected.name : "",
      this._modelObjectSelected != null ? this._modelObjectSelected.id : "",
      this._modelObjectSelected != null ? this._modelObjectSelected.name : "",
      this._objectUsageModel != null ? this._objectUsageModel.id : "",
      this._objectUsageModel != null ? this._objectUsageModel.name : "",
      this._objectPurposeSelected != null ? this._objectPurposeSelected.id : "",
      this._objectPurposeSelected != null ? this._objectPurposeSelected.name : "",
      this._groupSalesSelected != null ? this._groupSalesSelected.kode : "",
      this._groupSalesSelected != null ? this._groupSalesSelected.deskripsi : "",
      this._sourceOrderSelected != null ? this._sourceOrderSelected.kode : "",
      this._sourceOrderSelected != null ? this._sourceOrderSelected.deskripsi : "",
      null,
      this._thirdPartyTypeSelected != null ? this._thirdPartyTypeSelected.kode : "",
      this._thirdPartyTypeSelected != null ? this._thirdPartyTypeSelected.deskripsi : "",
      this._thirdPartySelected != null ? this._thirdPartySelected.kode : "",
      this._activitiesModel != null ? this._activitiesModel.kode : "",
      this._activitiesModel != null ? this._activitiesModel.deskripsi : "",
      null,
      null,
      null,
      null,
      this._programSelected != null ? this._programSelected.kode : "",
      this._programSelected != null ? this._programSelected.deskripsi : "",
      this._rehabTypeSelected != null ? this._rehabTypeSelected.id : "",
      this._rehabTypeSelected != null ? this._rehabTypeSelected.name : "",
      this._referenceNumberModel != null ? this._referenceNumberModel.noPK : null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ));
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2ApplObject();
  }

  TextEditingController get controllerMatriksDealer => _controllerMatriksDealer;

  set controllerMatriksDealer(TextEditingController value) {
    this._controllerMatriksDealer = value;
    notifyListeners();
  }

  void getMatriksDealer(BuildContext context) async {
    try {
      // this._listMatriksDealer.clear();
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);

      var _body = jsonEncode({
        "refOne": thirdPartySelected.kode,
        "refTwo": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id,
        "refThree": Provider.of<InfoAppChangeNotifier>(context,listen: false).controllerOrderDate.text
      });

      final _response = await _http.post(
        // "${BaseUrl.unit}api/parameter/get-group-object",
          "${BaseUrl.urlPublic}pihak-ketiga/get_info_dealermatrix",
          body: _body,
          headers: {"Content-Type":"application/json"}
      );
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result.isEmpty){
          showSnackBar("Matriks Dealer tidak ditemukan");
          loadData = false;
        } else{
          this._matriksDealerSelected = MatriksDealerModel(_result[0]['kode'], _result[0]['deskripsi']);
          this._controllerMatriksDealer.text = "${_result[0]['kode']} - ${_result[0]['deskripsi']}";
          notifyListeners();
          // for(int i=0; i <_result.length; i++){
          //   this._listMatriksDealer.add(
          //       MatriksDealerModel(_result[i]['kode'], _result[i]['deskripsi'])
          //   );
          // }
          loadData = false;
        }
      }
      else{
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    } catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }
}
