import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/add_detail_installment_credit_subsidy.dart';
import 'package:ad1ms2_dev/models/credit_subsidy_model.dart';
import 'package:ad1ms2_dev/models/cutting_method_model.dart';
import 'package:ad1ms2_dev/models/installment_index_model.dart';
import 'package:ad1ms2_dev/models/subsidy_type_model.dart';
import 'package:ad1ms2_dev/models/type_model.dart';
import 'package:ad1ms2_dev/screens/app/add_installment_credit_subsidy.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import '../format_currency.dart';
import 'add_installment_credit_subsidy_change_notifier.dart';

class AddCreditSubsidyChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    List<SubsidyTypeModel> _listType = [];
    List<AddInstallmentDetailModel> _listDetail = [];
    List<CreditSubsidyModel> _listCreditSubsidy = [];
    String _radioGiver = "00";
    SubsidyTypeModel _typeSelected;
    String _setVisibility;
    CuttingMethodModel _cuttingMethodSelected;
    TextEditingController _controllerValue = TextEditingController();
    TextEditingController _controllerClaimValue = TextEditingController();
    TextEditingController _controllerRateBeforeEff = TextEditingController();
    TextEditingController _controllerRateBeforeFlat = TextEditingController();
    TextEditingController _controllerTotalInstallment = TextEditingController();
    String _installmentIndexSelected;
    bool _autoValidate = false;
    int _totalInstallmentIndex;
    RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');
    FormatCurrency _formatCurrency = FormatCurrency();


    RegExInputFormatter get amountValidator => _amountValidator;

    FormatCurrency get formatCurrency => _formatCurrency;
    bool _loadData = false;

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    List<CreditSubsidyModel> get listCreditSubsidy => _listCreditSubsidy;

    List<AddInstallmentDetailModel> get listDetail => _listDetail;

    String get radioValueGiver {
        return _radioGiver;
    }

    set radioValueGiver(String value) {
        this._radioGiver = value;
        notifyListeners();
    }

    SubsidyTypeModel get typeSelected {
        return this._typeSelected;
    }

    set typeSelected(SubsidyTypeModel value) {
        changeType();
        this._typeSelected = value;
        _setVisibility = value.id;
        notifyListeners();
    }

    UnmodifiableListView<SubsidyTypeModel> get listType {
        return UnmodifiableListView(this._listType);
    }

    List<CuttingMethodModel> _listCuttingMethod = [
        // CuttingMethodModel("01", "Potong Pencairan"),
        // CuttingMethodModel("02", "Klaim"),
        // CuttingMethodModel("03", "Potong Pencairan & Klaim")
    ];

    CuttingMethodModel get cuttingMethodSelected {
        return this._cuttingMethodSelected;
    }

    set cuttingMethodSelected(CuttingMethodModel value) {
        this._cuttingMethodSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<CuttingMethodModel> get listCuttingMethod {
        return UnmodifiableListView(this._listCuttingMethod);
    }

    TextEditingController get controllerValue => _controllerValue;

    set controllerValue(TextEditingController value) {
        _controllerValue = value;
        notifyListeners();
    }

    TextEditingController get controllerClaimValue => _controllerClaimValue;

    set controllerClaimValue(TextEditingController value) {
        _controllerClaimValue = value;
        notifyListeners();
    }

    TextEditingController get controllerRateBeforeEff => _controllerRateBeforeEff;

    set controllerRateBeforeEff(TextEditingController value) {
        _controllerRateBeforeEff = value;
        notifyListeners();
    }

    TextEditingController get controllerRateBeforeFlat =>
        _controllerRateBeforeFlat;

    set controllerRateBeforeFlat(TextEditingController value) {
        _controllerRateBeforeFlat = value;
        notifyListeners();
    }

    TextEditingController get controllerTotalInstallment =>
      _controllerTotalInstallment;

    set controllerTotalInstallment(TextEditingController value) {
        _controllerTotalInstallment = value;
        notifyListeners();
    }

    List<String> _listInstallmentIndex = [];

    String get installmentIndexSelected {
        return this._installmentIndexSelected;
    }

    set installmentIndexSelected(String value) {
        this._installmentIndexSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<String> get listInstallmentIndexSelected {
        return UnmodifiableListView(this._listInstallmentIndex);
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    void addDetailInstallment(BuildContext context) async {
        // AddInstallmentDetailModel _data = await Navigator.push(context,
        //     MaterialPageRoute(
        //         builder: (context) => ChangeNotifierProvider(
        //             create: (context) => AddInstallmentCreditSubsidyChangeNotifier(),
        //             child: AddInstallment(),
        //         ),
        //         fullscreenDialog: true
        //     ));
        // if (_data != null) {
        //     this._listDetail.add(_data);
        //     notifyListeners();
        // }
        final _form = _key.currentState;
        if (_form.validate() && installmentIndexSelected != null && _controllerTotalInstallment.text != "") {
            this._listDetail.add(AddInstallmentDetailModel(
                _installmentIndexSelected, _controllerTotalInstallment.text));
            listInstallmentIndex();
            notifyListeners();
        } else {
            autoValidate = true;
        }
    }

    void listInstallmentIndex(){
        this._controllerTotalInstallment.text = "";
        this._installmentIndexSelected = null;
        this._listInstallmentIndex.clear();
        for(int i=1; i<=_totalInstallmentIndex; i++){
            _listInstallmentIndex.add(i.toString());
        }
        for(int j=0;j<_listDetail.length;j++){
            for(int i=0; i<_listInstallmentIndex.length; i++){
                if(_listInstallmentIndex[i] == _listDetail[j].installmentIndex){
                    _listInstallmentIndex.removeAt(i);
                }
            }
        }
    }

    void deleteIndex(BuildContext context, index){
        this._listDetail.removeAt(index);
        listInstallmentIndex();
        notifyListeners();
    }

    void changeType(){
         this._typeSelected = null;
         this._cuttingMethodSelected = null;
         this._controllerValue.text = "";
         this._controllerClaimValue.text = "";
         this._controllerRateBeforeEff.text = "";
         this._controllerRateBeforeFlat.text = "";
         this._controllerTotalInstallment.text = "";
         this._installmentIndexSelected = null;
         this._listDetail.clear();
    }

    void check(BuildContext context, int flag, int index) {
        final _form = _key.currentState;
        if (flag == 0) {
            if (_form.validate()) {
                if(this._typeSelected.id != "05"){
                    Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false)
                        .addListInfoCreditSubsidy(CreditSubsidyModel(
                        this.radioValueGiver,
                        this._typeSelected,
                        this._cuttingMethodSelected,
                        this._controllerValue.text,
                        this._controllerClaimValue.text,
                        this._controllerRateBeforeEff.text,
                        this._controllerRateBeforeFlat.text,
                        null,
                        null,
                        null));
                } else {
                    Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false)
                        .addListInfoCreditSubsidy(CreditSubsidyModel(
                        this.radioValueGiver,
                        this._typeSelected,
                        this._cuttingMethodSelected,
                        this._controllerValue.text,
                        null,
                        null,
                        null,
                        this._controllerTotalInstallment.text,
                        this._installmentIndexSelected,
                        this._listDetail));
                }
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
            if (_form.validate()) {
                if(this._typeSelected.id != "05"){
                    Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false)
                        .updateListInfoCreditSubsidy(CreditSubsidyModel(
                        this.radioValueGiver,
                        this._typeSelected,
                        this._cuttingMethodSelected,
                        this._controllerValue.text,
                        this._controllerClaimValue.text,
                        this._controllerRateBeforeEff.text,
                        this._controllerRateBeforeFlat.text,
                        null,
                        null,
                        null), context, index);
                } else {
                    Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false)
                        .updateListInfoCreditSubsidy(CreditSubsidyModel(
                        this.radioValueGiver,
                        this._typeSelected,
                        this._cuttingMethodSelected,
                        this._controllerValue.text,
                        null,
                        null,
                        null,
                        this._controllerTotalInstallment.text,
                        this._installmentIndexSelected,
                        this._listDetail), context, index);
                }
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        }
    }

    Future<void> setValueForEditInfoCreditSubsidy(CreditSubsidyModel data, int flag, int periodTime) async {
        _totalInstallmentIndex = periodTime;
        if(flag != 0){
            getListType(data, flag);
        }
        listInstallmentIndex();
    }

    void getListType(CreditSubsidyModel data, int flag) async{
        _listType.clear();
        this._typeSelected = null;

        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            var _body = jsonEncode({
                "refOne": _radioGiver
            });

            final _response = await _http.post(
                "${BaseUrl.urlPublic}jenis-rehab/get_jenis_subsidi",
                body: _body,
                headers: {"Content-Type":"application/json"}
            );
            final _data = jsonDecode(_response.body);
            for(int i=0; i < _data.length;i++){
                _listType.add(SubsidyTypeModel(_data[i]['kode'], _data[i]['deskripsi']));
            }
            getListCuttingMethod(data, flag);
        } catch (e) {
        }
        if(flag == 0){
            notifyListeners();
        }
    }

    void getListCuttingMethod(CreditSubsidyModel data, int flag) async{
        _listCuttingMethod.clear();
        this._cuttingMethodSelected = null;

        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            final _response = await _http.get(
                "${BaseUrl.urlPublic}jenis-rehab/get_method_subsidi"
            );
            final _data = jsonDecode(_response.body);
            for(int i=0; i < _data['data'].length;i++){
                _listCuttingMethod.add(CuttingMethodModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
            }
            setDataEdit(data, flag);
        } catch (e) {
        }
    }

    void setDataEdit(CreditSubsidyModel data, int flag){
        if(flag != 0){
            this.radioValueGiver = data.giver;
            if(data.type.id != "05"){
                for (int i = 0; i < this._listType.length; i++) {
                    if (data.type.id == this._listType[i].id) {
                        this._typeSelected = this._listType[i];
                    }
                }
                for (int i = 0; i < this._listCuttingMethod.length; i++) {
                    if (data.cuttingMethod.id == this._listCuttingMethod[i].id) {
                        this._cuttingMethodSelected = this._listCuttingMethod[i];
                    }
                }
                this._controllerValue.text = data.value;
                this._controllerClaimValue.text = data.claimValue;
                this._controllerRateBeforeEff.text = data.rateBeforeEff;
                this._controllerRateBeforeFlat.text = data.rateBeforeFlat;
            }
            else {
                for (int i = 0; i < this._listType.length; i++) {
                    if (data.type.id == this._listType[i].id) {
                        this._typeSelected = this._listType[i];
                    }
                }
                for (int i = 0; i < this._listCuttingMethod.length; i++) {
                    if (data.cuttingMethod.id == this._listCuttingMethod[i].id) {
                        this._cuttingMethodSelected = this._listCuttingMethod[i];
                    }
                }
                this._controllerValue.text = data.value;
                this._controllerTotalInstallment.text = data.totalInstallment;
                for (int i = 0; i < this._listInstallmentIndex.length; i++) {
                    if (data.installmentIndex == this._listInstallmentIndex[i]) {
                        this._installmentIndexSelected = this._listInstallmentIndex[i];
                    }
                }
                this._listDetail = data.installmentDetail;
            }
        }
        notifyListeners();
    }
}