import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/ms2_appl_insurance_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ad1ms2_dev/models/major_insurance_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoMajorInsuranceChangeNotifier with ChangeNotifier{
    List<MajorInsuranceModel> _listMajorInsurance = [];
    bool _autoValidate = false;
    bool _flag = false;
    DbHelper _dbHelper = DbHelper();
    List<MajorInsuranceModel> get listFormMajorInsurance => _listMajorInsurance;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    void deleteListInfoMajorInsurance(BuildContext context, int index) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans",
                        primaryColor: Colors.black,
                        primarySwatch: primaryOrange,
                        accentColor: myPrimaryColor
                    ),
                    child: AlertDialog(
                        title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                                Text("Apakah kamu yakin menghapus asuransi utama ini?",),
                            ],
                        ),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    this._listMajorInsurance.removeAt(index);
                                    notifyListeners();
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                );
            }
        );
    }

    void updateListInfoMajorInsurance(
        MajorInsuranceModel mInfoMajorInsurance, BuildContext context, int index) {
        this._listMajorInsurance[index] = mInfoMajorInsurance;
        notifyListeners();
    }

    void addListInfoMajorInsurance(MajorInsuranceModel value) {
        _listMajorInsurance.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void clearMajorInsurance(){
        this._autoValidate = false;
        this._listMajorInsurance.clear();
    }

    bool get flag => _flag;

    set flag(bool value) {
        this._flag = value;
        notifyListeners();
    }

    Future<bool> onBackPress() async{
        if (_listMajorInsurance.length != 0) {
            flag = false;
        } else {
            flag = true;
        }
        return true;
    }

    void saveToSQLite() async {
        List<MS2ApplInsuranceModel> _listApplInsurance = [];
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        if(_listMajorInsurance.isNotEmpty) {
            for(int i=0; i<_listMajorInsurance.length; i++){
                _listApplInsurance.add(MS2ApplInsuranceModel(
                    "1",
                    null,
                    this._listMajorInsurance[i].insuranceType != null ? this._listMajorInsurance[i].insuranceType.KODE : "",
                    this._listMajorInsurance[i].company != null ? this._listMajorInsurance[i].company.id : "",
                    this._listMajorInsurance[i].product != null ? this._listMajorInsurance[i].product.KODE : "",
                    this._listMajorInsurance[i].periodType != "" ? int.parse(this._listMajorInsurance[i].periodType) : 0,
                    this._listMajorInsurance[i].type != null ?  this._listMajorInsurance[i].type : "",
                    null,
                    null,
                    null,
                    null,
                    this._listMajorInsurance[i].coverageType != "" ? double.parse(this._listMajorInsurance[i].coverageType) : 0,
                    this._listMajorInsurance[i].coverageValue != "" ? double.parse(this._listMajorInsurance[i].coverageValue) : 0,
                    this._listMajorInsurance[i].upperLimitRate != "" ? double.parse(this._listMajorInsurance[i].upperLimitRate) : 0,
                    this._listMajorInsurance[i].upperLimit != "" ? double.parse(this._listMajorInsurance[i].upperLimit) : 0,
                    this._listMajorInsurance[i].lowerLimitRate != "" ? double.parse(this._listMajorInsurance[i].lowerLimitRate) : 0,
                    this._listMajorInsurance[i].lowerLimit != "" ? double.parse(this._listMajorInsurance[i].lowerLimit) : 0,
                    this._listMajorInsurance[i].priceCash != "" ? double.parse(this._listMajorInsurance[i].priceCash) : 0,
                    this._listMajorInsurance[i].priceCredit != "" ? double.parse(this._listMajorInsurance[i].priceCredit) : 0,
                    this._listMajorInsurance[i].totalPriceSplit != "" ? double.parse(this._listMajorInsurance[i].totalPriceSplit) : 0,
                    this._listMajorInsurance[i].totalPriceRate != "" ? double.parse(this._listMajorInsurance[i].totalPriceRate) : 0,
                    this._listMajorInsurance[i].totalPrice != "" ? double.parse(this._listMajorInsurance[i].totalPrice) : 0,
                    DateTime.now().toString(),
                    _preferences.getString("username"),
                    null,
                    null,
                    1,
                    i+1
                ));
            }
        }
        _dbHelper.insertMS2ApplInsuranceMain(_listApplInsurance);
    }
}