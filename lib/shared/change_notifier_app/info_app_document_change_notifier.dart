import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/info_document_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class InfoDocumentChangeNotifier with ChangeNotifier{
    List<InfoDocumentModel> _listDocument = [];
    bool _autoValidate = false;
    DbHelper _dbHelper = DbHelper();

    List<InfoDocumentModel> get listInfoDocument => _listDocument;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    void deleteListInfoDocument(int index) {
        deleteFile(index);
        this._listDocument.removeAt(index);
        notifyListeners();
    }

    void updateListInfoDocument(
        InfoDocumentModel mInfoDocument, BuildContext context, int index) {
        this._listDocument[index] = mInfoDocument;
        notifyListeners();
    }

    void addListInfoDocument(InfoDocumentModel value) {
        _listDocument.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void clearInfoDocument(){
        this._autoValidate = false;
        this._listDocument.clear();
    }

    void saveToSQLite(){
        List<MS2DocumentModel> _listData = [];
        for(int i=0; i<_listDocument.length; i++){
            _listData.add(MS2DocumentModel(
                "a",
                "b",
                "c",
                _listDocument[i].path,
                null,
                null,
                null,
                null,
                _listDocument[i].documentType.docTypeId,
                _listDocument[i].documentType.docTypeName,
                _listDocument[i].latitude.toString(),
                _listDocument[i].longitude.toString()
            ));
        }
        _dbHelper.insertMS2Document(_listData);
    }

    void deleteFile(int index) async{
        var file = File("${this._listDocument[index].path}");
        await file.delete();
    }

    Future<bool> deleteSQLite() async{
        return await _dbHelper.deleteMS2Document();
    }
}