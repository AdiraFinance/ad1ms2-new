import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class MarketingNotesChangeNotifier with ChangeNotifier{
  bool _autoValidate = false;
  TextEditingController _controllerMarketingNotes = TextEditingController();

  TextEditingController get controllerMarketingNotes =>
      _controllerMarketingNotes;

  void clearMarketingNotes(){
    this._autoValidate = false;
    this._controllerMarketingNotes.clear();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }
}