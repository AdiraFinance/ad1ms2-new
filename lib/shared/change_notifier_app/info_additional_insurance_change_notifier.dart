import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/additional_insurance_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_insurance_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoAdditionalInsuranceChangeNotifier with ChangeNotifier{
    List<AdditionalInsuranceModel> _listAdditionalInsurance = [];
    bool _autoValidate = false;
    bool _flag = false;
    DbHelper _dbHelper = DbHelper();
    List<AdditionalInsuranceModel> get listFormAdditionalInsurance => _listAdditionalInsurance;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    void deleteListInfoAdditionalInsurance(int index) {
        this._listAdditionalInsurance.removeAt(index);
        notifyListeners();
    }

    void updateListInfoAdditionalInsurance(
        AdditionalInsuranceModel mInfoAdditionalInsurance, BuildContext context, int index) {
        this._listAdditionalInsurance[index] = mInfoAdditionalInsurance;
        notifyListeners();
    }

    void addListInfoAdditionalInsurance(AdditionalInsuranceModel value) {
        _listAdditionalInsurance.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void clearListAdditionalInsurance(){
        this._autoValidate = false;
        this._listAdditionalInsurance.clear();
    }

    bool get flag => _flag;

    set flag(bool value) {
        this._flag = value;
        notifyListeners();
    }

    Future<bool> onBackPress() async{
        if (_listAdditionalInsurance.length != 0) {
            flag = false;
        } else {
            flag = true;
        }
        return true;
    }

    void saveToSQLite() async {
        List<MS2ApplInsuranceModel> _listApplInsurance = [];
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        if(_listAdditionalInsurance.isNotEmpty) {
            for(int i=0; i<_listAdditionalInsurance.length; i++){
                _listApplInsurance.add(MS2ApplInsuranceModel(
                    "1",
                    null,
                    this._listAdditionalInsurance[i].insuranceType != null ? this._listAdditionalInsurance[i].insuranceType.KODE : "",
                    this._listAdditionalInsurance[i].company != null ? this._listAdditionalInsurance[i].company.id : "",
                    this._listAdditionalInsurance[i].product != null ? this._listAdditionalInsurance[i].product.KODE : "",
                    this._listAdditionalInsurance[i].periodType != "" ? int.parse(this._listAdditionalInsurance[i].periodType) : 0,
                    null,
                    null,
                    null,
                    null,
                    null,
                    this._listAdditionalInsurance[i].coverageType != "" ? double.parse(this._listAdditionalInsurance[i].coverageType) : 0,
                    this._listAdditionalInsurance[i].coverageValue != "" ? double.parse(this._listAdditionalInsurance[i].coverageValue) : 0,
                    this._listAdditionalInsurance[i].upperLimitRate != "" ? double.parse(this._listAdditionalInsurance[i].upperLimitRate) : 0,
                    this._listAdditionalInsurance[i].upperLimit != "" ? double.parse(this._listAdditionalInsurance[i].upperLimit) : 0,
                    this._listAdditionalInsurance[i].lowerLimitRate != "" ? double.parse(this._listAdditionalInsurance[i].lowerLimitRate) : 0,
                    this._listAdditionalInsurance[i].lowerLimit != "" ? double.parse(this._listAdditionalInsurance[i].lowerLimit) : 0,
                    this._listAdditionalInsurance[i].priceCash != "" ? double.parse(this._listAdditionalInsurance[i].priceCash) : 0,
                    this._listAdditionalInsurance[i].priceCredit != "" ? double.parse(this._listAdditionalInsurance[i].priceCredit) : 0,
                    null,
                    this._listAdditionalInsurance[i].totalPriceRate != "" ? double.parse(this._listAdditionalInsurance[i].totalPriceRate) : 0,
                    this._listAdditionalInsurance[i].totalPrice != "" ? double.parse(this._listAdditionalInsurance[i].totalPrice) : 0,
                    DateTime.now().toString(),
                    _preferences.getString("username"),
                    null,
                    null,
                    1,
                    i+1
                ));
            }
        }
        _dbHelper.insertMS2ApplInsuranceAdditional(_listApplInsurance);
    }
}