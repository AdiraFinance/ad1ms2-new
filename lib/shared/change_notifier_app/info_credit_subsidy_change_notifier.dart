import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/credit_subsidy_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_refund_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoCreditSubsidyChangeNotifier with ChangeNotifier{
    List<CreditSubsidyModel> _listCreditSubsidy = [];
    bool _autoValidate = false;
    DbHelper _dbHelper = DbHelper();

    List<CreditSubsidyModel> get listInfoCreditSubsidy => _listCreditSubsidy;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    void deleteListInfoCreditSubsidy(int index) {
        this._listCreditSubsidy.removeAt(index);
        notifyListeners();
    }

    void updateListInfoCreditSubsidy(
        CreditSubsidyModel mInfoCreditSubsidy, BuildContext context, int index) {
        this._listCreditSubsidy[index] = mInfoCreditSubsidy;
        notifyListeners();
    }

    void addListInfoCreditSubsidy(CreditSubsidyModel value) {
        _listCreditSubsidy.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void clearInfoCreditSubsidy(){
        this._autoValidate = false;
        this._listCreditSubsidy.clear();
    }

    void saveToSQLite() async {
        List<MS2ApplRefundModel> _listApplRefund = [];
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        // for(int i=0; i<_listCreditSubsidy.length; i++){
        //     _listApplRefund.add(MS2ApplRefundModel(
        //         "1",
        //         this._listCreditSubsidy[i].giver.toString(),
        //         this._listCreditSubsidy[i].type.id,
        //         this._listCreditSubsidy[i].type.text,
        //         this._listCreditSubsidy[i].cuttingMethod.id,
        //         this._listCreditSubsidy[i].cuttingMethod.text,
        //         null, // refund_amt
        //         int.parse(this._listCreditSubsidy[i].rateBeforeEff),
        //         int.parse(this._listCreditSubsidy[i].rateBeforeFlat),
        //         null,
        //         int.parse(this._listCreditSubsidy[i].totalInstallment),
        //         1,
        //         DateTime.now().toString(),
        //         _preferences.getString("username"),
        //         null,
        //         null,
        //         null,
        //         null
        //     ));
        // }
        if(_listCreditSubsidy.isNotEmpty) {
            for(int i=0; i<_listCreditSubsidy.length; i++){
                _listCreditSubsidy[i].type.id == "01"
                    ?
                _listApplRefund.add(MS2ApplRefundModel(
                    "123",
                    this._listCreditSubsidy[i].giver.toString(),
                    this._listCreditSubsidy[i].type != null ? this._listCreditSubsidy[i].type.id : "",
                    this._listCreditSubsidy[i].type != null ? this._listCreditSubsidy[i].type.text : "",
                    this._listCreditSubsidy[i].cuttingMethod != null ? this._listCreditSubsidy[i].cuttingMethod.id : "",
                    this._listCreditSubsidy[i].cuttingMethod != null ? this._listCreditSubsidy[i].cuttingMethod.text : "",
                    double.parse(this._listCreditSubsidy[i].value.replaceAll(",", "")), // refund_amt
                    null, //bunga
                    null, //bunga
                    null,
                    null,
                    1,
                    DateTime.now().toString(),
                    _preferences.getString("username"),
                    null,
                    null,
                    null,
                    null, //claim val
                    null
                ))
                    :
                _listApplRefund.add(MS2ApplRefundModel(
                    "123",
                    this._listCreditSubsidy[i].giver.toString(),
                    this._listCreditSubsidy[i].type != null ? this._listCreditSubsidy[i].type.id : "",
                    this._listCreditSubsidy[i].type != null ? this._listCreditSubsidy[i].type.text : "",
                    this._listCreditSubsidy[i].cuttingMethod != null ? this._listCreditSubsidy[i].cuttingMethod.id : "",
                    this._listCreditSubsidy[i].cuttingMethod != null ? this._listCreditSubsidy[i].cuttingMethod.text : "",
                    double.parse(this._listCreditSubsidy[i].value.replaceAll(",", "")), // refund_amt
                    null, //bunga
                    null, //bunga
                    null,
                    null,
                    1,
                    DateTime.now().toString(),
                    _preferences.getString("username"),
                    null,
                    null,
                    null,
                    null, //claim val
                    this._listCreditSubsidy[i].installmentDetail != null ? this._listCreditSubsidy[i].installmentDetail : ""
                ));
            }
        }
        _dbHelper.insertMS2ApplRefund(_listApplRefund);
    }

    Future<bool> deleteSQLIte() async{
        return await _dbHelper.deleteMS2ApplRefundHeader();
    }
}