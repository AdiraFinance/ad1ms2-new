import 'dart:collection';

import 'package:ad1ms2_dev/models/collateral_type_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class SearchCollateralTypeChangeNotifier with ChangeNotifier{
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();

  List<CollateralTypeModel> _listCollateralType = [
    CollateralTypeModel("001", "AUTOMOTIVE"),
    CollateralTypeModel("002", "PROPERTI"),
  ];

  UnmodifiableListView<CollateralTypeModel> get listCollateralType {
    return UnmodifiableListView(this._listCollateralType);
  }

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }
}