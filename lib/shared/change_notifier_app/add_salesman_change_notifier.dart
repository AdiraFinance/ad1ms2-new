import 'dart:collection';

import 'package:ad1ms2_dev/models/employee_head_model.dart';
import 'package:ad1ms2_dev/models/employee_model.dart';
import 'package:ad1ms2_dev/models/infromation_sales_model.dart';
import 'package:ad1ms2_dev/models/position_model.dart';
import 'package:ad1ms2_dev/models/salesman_type_model.dart';
import 'package:ad1ms2_dev/screens/search_employee.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_employee_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddSalesmanChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  TextEditingController _controllerEmployee = TextEditingController();
  SalesmanTypeModel _salesmanTypeModelSelected;
  SalesmanTypeModel _salesmanTypeModelTemp;
  PositionModel _positionModelSelected;
  PositionModel _positionModeltemp;
  EmployeeHeadModel _employeeHeadModelSelected;
  EmployeeHeadModel _employeeHeadModelTemp;
  EmployeeModel _employeeModelSelected;
  String _employeeTemp;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  List<SalesmanTypeModel> _listSalesmanType = [
    SalesmanTypeModel("001", "SALES INTERNAL"),
    SalesmanTypeModel("002", "SALES EXTERNAL"),
    SalesmanTypeModel("003", "SALES REFFERAL"),
    SalesmanTypeModel("004", "RECIPIENT")
  ];

  List<PositionModel> _listPosition = [
    PositionModel("01", "JABATAN 1"),
    PositionModel("02", "JABATAN 2"),
    PositionModel("03", "JABATAN 3")
  ];

  List<EmployeeHeadModel> _listEmployeeHead = [
    EmployeeHeadModel("01", "ATASAN 1"),
    EmployeeHeadModel("02", "ATASAN 2"),
    EmployeeHeadModel("03", "ATASAN 3")
  ];

  SalesmanTypeModel get salesmanTypeModelSelected => _salesmanTypeModelSelected;

  TextEditingController get controllerEmployee => _controllerEmployee;

  set salesmanTypeModelSelected(SalesmanTypeModel value) {
    this._salesmanTypeModelSelected = value;
    notifyListeners();
  }

  PositionModel get positionModelSelected => _positionModelSelected;

  set positionModelSelected(PositionModel value) {
    this._positionModelSelected = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  EmployeeHeadModel get employeeHeadModelSelected => _employeeHeadModelSelected;

  set employeeHeadModelSelected(EmployeeHeadModel value) {
    this._employeeHeadModelSelected = value;
    notifyListeners();
  }

  GlobalKey<FormState> get key => _key;

  UnmodifiableListView<SalesmanTypeModel> get listSalesmanType {
    return UnmodifiableListView(this._listSalesmanType);
  }

  UnmodifiableListView<PositionModel> get listPosition {
    return UnmodifiableListView(this._listPosition);
  }

  UnmodifiableListView<EmployeeHeadModel> get listEmployeeHead {
    return UnmodifiableListView(this._listEmployeeHead);
  }

  void searchEmployee(BuildContext context) async {
    EmployeeModel _data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchEmployeeChangeNotifier(),
                child: SearchEmployee())));
    if (_data != null) {
      this._employeeModelSelected = _data;
      this._controllerEmployee.text = "${_data.id} - ${_data.name}";
      notifyListeners();
    }
  }

  void check(BuildContext context, int flag, int index) {
    var _provider =
        Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
    final _form = this._key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        _provider.addInfoSales(InformationSalesModel(
            this._salesmanTypeModelSelected,
            this._positionModelSelected,
            this._employeeModelSelected,
            this._employeeHeadModelSelected));
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    } else {
      if (_form.validate()) {
        print(index);
        Provider.of<InformationSalesmanChangeNotifier>(context, listen: false)
            .updateListInfoSales(
                index,
                InformationSalesModel(
                    this._salesmanTypeModelSelected,
                    this._positionModelSelected,
                    this._employeeModelSelected,
                    this._employeeHeadModelSelected));
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    }
  }

  SalesmanTypeModel get salesmanTypeModelTemp => _salesmanTypeModelTemp;

  PositionModel get positionModeltemp => _positionModeltemp;

  EmployeeHeadModel get employeeHeadModelTemp => _employeeHeadModelTemp;

  String get employeeTemp => _employeeTemp;

  Future<void> setValue(InformationSalesModel value) async {
    for (int i = 0; i < this._listSalesmanType.length; i++) {
      if (value.salesmanTypeModel.id == this._listSalesmanType[i].id) {
        this._salesmanTypeModelSelected = this._listSalesmanType[i];
        this._salesmanTypeModelTemp = this._listSalesmanType[i];
      }
    }

    for (int i = 0; i < this._listPosition.length; i++) {
      if (value.positionModel.id == this._listPosition[i].id) {
        this._positionModelSelected = this._listPosition[i];
        this._positionModeltemp = this._listPosition[i];
      }
    }

    for (int i = 0; i < this._listEmployeeHead.length; i++) {
      if (value.employeeHeadModel.id == this._listEmployeeHead[i].id) {
        this._employeeHeadModelSelected = this._listEmployeeHead[i];
        this._employeeHeadModelTemp = this._listEmployeeHead[i];
      }
    }

    this._employeeModelSelected = value.employeeModel;
    this._controllerEmployee.text =
        "${value.employeeModel.id} - ${value.employeeModel.name}";
    this._employeeTemp = this._controllerEmployee.text;
  }
}
