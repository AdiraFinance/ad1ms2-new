import 'dart:collection';
import 'dart:ffi';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_company_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_business_field.dart';
import 'package:ad1ms2_dev/screens/form_m/search_sector_economi.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_business_field_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_sector_economic_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormMCompanyRincianChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  TypeInstitutionModel _typeInstitutionSelected;
  ProfilModel _profilSelected;
  TextEditingController _controllerInstitutionName = TextEditingController();
  int _radioValueIsHaveNPWP = 1;
  TextEditingController _controllerNPWP = TextEditingController();
  TextEditingController _controllerFullNameNPWP = TextEditingController();
  TypeNPWPModel _typeNPWPSelected;
  SignPKPModel _signPKPSelected;
  TextEditingController _controllerNPWPAddress = TextEditingController();
  TextEditingController _controllerDateEstablishment = TextEditingController();
  DateTime _initialDateForDateEstablishment = DateTime(dateNow.year, dateNow.month, dateNow.day);

  TextEditingController _controllerSectorEconomic = TextEditingController();
  SectorEconomicModel _sectorEconomicModelSelected;

  TextEditingController _controllerBusinessField = TextEditingController();
  BusinessFieldModel _businessFieldModelSelected;

  LocationStatusModel _locationStatusSelected;
  BusinessLocationModel _businessLocationSelected;
  TextEditingController _controllerTotalEmployees = TextEditingController();
  TextEditingController _controllerLamaUsahaBerjalan = TextEditingController();
  TextEditingController _controllerTotalLamaUsahaBerjalan = TextEditingController();
  int _radioValueOpenAccount = 0;
  TextEditingController _controllerFormNumber = TextEditingController();

  List<TypeInstitutionModel> _listTypeInstitution = TypeInstitutionList().typeInstitutionItems;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  bool _isEnableFieldCompanyName = false;
  bool _isEnableFieldNumberNPWP = false;
  bool _isEnableFieldEstablishDate = false;
  DbHelper _dbHelper = DbHelper();

  List<ProfilModel> _listProfil = [
    ProfilModel("01", "PT"),
    ProfilModel("02", "CV"),
    ProfilModel("03", "FOUNDATION"),
    ProfilModel("05", "KOPERASI"),
  ];

  List<TypeNPWPModel> _listTypeNPWP = [
    TypeNPWPModel("JN01", "BADAN USAHA"),
    TypeNPWPModel("JN01", "PERORANGAN"),
  ];

  List<SignPKPModel> _listSignPKP = [
    SignPKPModel("1", "PKP"),
    SignPKPModel("2", "NON PKP"),
  ];

  List<SectorEconomicModel> _listSectorEconomic = [
    SectorEconomicModel("01", "A"),
    SectorEconomicModel("01", "B"),
  ];

  List<BusinessFieldModel> _listBusinessField = [
    BusinessFieldModel("01", "A"),
    BusinessFieldModel("01", "B"),
  ];

  List<LocationStatusModel> _listLocationStatus = [
    LocationStatusModel("01", "RUMAH"),
    LocationStatusModel("02", "PERTOKOAN"),
    LocationStatusModel("03", "PASAR"),
    LocationStatusModel("04", "INDUSTRI"),
    LocationStatusModel("05", "PERKANTORAN"),
    LocationStatusModel("06", "LAINNYA"),
  ];

  List<BusinessLocationModel> _listBusinessLocation = [
    BusinessLocationModel("01", "STRATEGIS"),
    BusinessLocationModel("02", "BIASA"),
    BusinessLocationModel("03", "TIDAK STRATEGIS"),
    BusinessLocationModel("04", "LAINNYA"),
  ];

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Jenis Lembaga
  UnmodifiableListView<TypeInstitutionModel> get listTypeInstitution {
    return UnmodifiableListView(this._listTypeInstitution);
  }

  TypeInstitutionModel get typeInstitutionSelected => _typeInstitutionSelected;

  set typeInstitutionSelected(TypeInstitutionModel value) {
    this._typeInstitutionSelected = value;
    notifyListeners();
  }

  // Profil
  UnmodifiableListView<ProfilModel> get listProfil {
    return UnmodifiableListView(this._listProfil);
  }

  ProfilModel get profilSelected => _profilSelected;

  set profilSelected(ProfilModel value) {
    this._profilSelected = value;
    notifyListeners();
  }

  // Nama Lembaga
  TextEditingController get controllerInstitutionName {
    return this._controllerInstitutionName;
  }

  set controllerInstitutionName(value) {
    this._controllerInstitutionName = value;
    this.notifyListeners();
  }

  // Punya NPWP
  int get radioValueIsHaveNPWP => _radioValueIsHaveNPWP;

  set radioValueIsHaveNPWP(int value) {
    this._radioValueIsHaveNPWP = value;
    notifyListeners();
  }

  // NPWP
  TextEditingController get controllerNPWP {
    return this._controllerNPWP;
  }

  set controllerNPWP(value) {
    this._controllerNPWP = value;
    this.notifyListeners();
  }

  // NPWP
  TextEditingController get controllerFullNameNPWP {
    return this._controllerFullNameNPWP;
  }

  set controllerFullNameNPWP(value) {
    this._controllerFullNameNPWP = value;
    this.notifyListeners();
  }

  // Jenis NPWP
  UnmodifiableListView<TypeNPWPModel> get listTypeNPWP {
    return UnmodifiableListView(this._listTypeNPWP);
  }

  TypeNPWPModel get typeNPWPSelected {
    return this._typeNPWPSelected;
  }

  set typeNPWPSelected(TypeNPWPModel value) {
    this._typeNPWPSelected = value;
    notifyListeners();
  }

  // Tanda PKP
  UnmodifiableListView<SignPKPModel> get listSignPKP {
    return UnmodifiableListView(this._listSignPKP);
  }

  SignPKPModel get signPKPSelected {
    return this._signPKPSelected;
  }

  set signPKPSelected(SignPKPModel value) {
    this._signPKPSelected = value;
    notifyListeners();
  }

  // Alamat NPWP
  TextEditingController get controllerNPWPAddress {
    return this._controllerNPWPAddress;
  }

  set controllerNPWPAddress(value) {
    this._controllerNPWPAddress = value;
    this.notifyListeners();
  }

  // Tanggal Pendirian
  TextEditingController get controllerDateEstablishment => _controllerDateEstablishment;


  DateTime get initialDateForDateEstablishment =>
      _initialDateForDateEstablishment;


  set initialDateForDateEstablishment(DateTime value) {
    this._initialDateForDateEstablishment = value;
  }

  void selectDateEstablishment(BuildContext context) async {
    DatePickerShared _datePickerShared = DatePickerShared();
    var _datePickerSelected = await _datePickerShared.selectStartDate(
        context, this._initialDateForDateEstablishment,
        canAccessNextDay: false);
    if (_datePickerSelected != null) {
      this.controllerDateEstablishment.text = dateFormat.format(_datePickerSelected);
      this._initialDateForDateEstablishment = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  // Sektor Ekonomi
  TextEditingController get controllerSectorEconomic =>
      _controllerSectorEconomic;

  SectorEconomicModel get sectorEconomicModelSelected =>
      _sectorEconomicModelSelected;

  set sectorEconomicModelSelected(SectorEconomicModel value) {
    this._sectorEconomicModelSelected = value;
  }

  void searchSectorEconomic(BuildContext context) async {
    SectorEconomicModel data = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ChangeNotifierProvider(
          create: (context) => SearchSectorEconomicChangeNotif(),
          child: SearchSectorEconomic())));
    if (data != null) {
      sectorEconomicModelSelected = data;
      this._controllerSectorEconomic.text = "${data.KODE} - ${data.DESKRIPSI}";
      businessFieldModelSelected = null;
      this._controllerBusinessField.text = "";
      notifyListeners();
    } else {
      return;
    }
  }

  // Lapangan Usaha
  TextEditingController get controllerBusinessField =>
      _controllerBusinessField;

  BusinessFieldModel get businessFieldModelSelected =>
      _businessFieldModelSelected;

  set businessFieldModelSelected(BusinessFieldModel value) {
    this._businessFieldModelSelected = value;
  }

  void searchBusinesField(BuildContext context) async {
    BusinessFieldModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBusinessFieldChangeNotif(),
                child: SearchBusinessField(id: this._sectorEconomicModelSelected.KODE))));
    if (data != null) {
      businessFieldModelSelected = data;
      this._controllerBusinessField.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  // Status Lokasi
  UnmodifiableListView<LocationStatusModel> get listLocationStatus {
    return UnmodifiableListView(this._listLocationStatus);
  }

  LocationStatusModel get locationStatusSelected {
    return this._locationStatusSelected;
  }

  set locationStatusSelected(LocationStatusModel value) {
    this._locationStatusSelected = value;
    notifyListeners();
  }

  // Lokasi Usaha
  UnmodifiableListView<BusinessLocationModel> get listBusinessLocation {
    return UnmodifiableListView(this._listBusinessLocation);
  }

  BusinessLocationModel get businessLocationSelected {
    return this._businessLocationSelected;
  }

  set businessLocationSelected(BusinessLocationModel value) {
    this._businessLocationSelected = value;
    notifyListeners();
  }

  // Total Pegawai
  TextEditingController get controllerTotalEmployees {
    return this._controllerTotalEmployees;
  }

  set controllerTotalEmployees(value) {
    this._controllerTotalEmployees = value;
    this.notifyListeners();
  }

  // Lama Usaha/Kerja Berjalan (bln)
  TextEditingController get controllerLamaUsahaBerjalan {
    return this._controllerLamaUsahaBerjalan;
  }

  set controllerLamaUsahaBerjalan(value) {
    this._controllerLamaUsahaBerjalan = value;
    this.notifyListeners();
  }

  // Total Lama Usaha/Kerja Berjalan (bln)
  TextEditingController get controllerTotalLamaUsahaBerjalan {
    return this._controllerTotalLamaUsahaBerjalan;
  }

  set controllerTotalLamaUsahaBerjalan(value) {
    this._controllerTotalLamaUsahaBerjalan = value;
    this.notifyListeners();
  }

  // Buka Rekening
  int get radioValueOpenAccount => _radioValueOpenAccount;

  set radioValueOpenAccount(int value) {
    this._radioValueOpenAccount = value;
    notifyListeners();
  }

  // Nomor Form
  TextEditingController get controllerFormNumber {
    return this._controllerFormNumber;
  }

  set controllerFormNumber(value) {
    this._controllerFormNumber = value;
    this.notifyListeners();
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      saveToSQLite(context);
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  // Future<void> clearDataRincian() async {
  void clearDataRincian() {
    this._autoValidate = false;
    this._flag = false;
    this._typeInstitutionSelected = null;
    this._profilSelected = null;
//    this._controllerInstitutionName.clear();
//    this._radioValueIsHaveNPWP = 0;
//    this._controllerNPWP.clear();
    this._controllerFullNameNPWP.clear();
    // this._typeNPWPSelected = null;
    // this._signPKPSelected = null;
    // this._controllerNPWPAddress.clear();
//    this._controllerDateEstablishment.clear();
    this._controllerSectorEconomic.clear();
    this._controllerBusinessField.clear();
    // this._locationStatusSelected = null;
    // this._businessLocationSelected = null;
    this._controllerTotalEmployees.clear();
    this._controllerLamaUsahaBerjalan.clear();
    this._controllerTotalLamaUsahaBerjalan.clear();
    // this._radioValueOpenAccount = 0;
    // this._controllerFormNumber.clear();
  }

  bool get isEnableFieldEstablishDate => _isEnableFieldEstablishDate;

  set isEnableFieldEstablishDate(bool value) {
    this._isEnableFieldEstablishDate = value;
  }

  bool get isEnableFieldNumberNPWP => _isEnableFieldNumberNPWP;

  set isEnableFieldNumberNPWP(bool value) {
    this._isEnableFieldNumberNPWP = value;
  }

  bool get isEnableFieldCompanyName => _isEnableFieldCompanyName;

  set isEnableFieldCompanyName(bool value) {
    this._isEnableFieldCompanyName = value;
  }

  void saveToSQLite(BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    _dbHelper.insertMS2CustomerCompany(
        MS2CustomerCompanyModel(
            "000001",
            _typeInstitutionSelected,
            _profilSelected,
            _controllerInstitutionName.text,
            null,
            null,
            null,
            null,
            null,
            null,
            _initialDateForDateEstablishment.toString(),
            "1",
            null,
            null,
            _sectorEconomicModelSelected,
            _businessFieldModelSelected,
            _controllerTotalEmployees.text,
            _controllerLamaUsahaBerjalan.text,
            _controllerTotalLamaUsahaBerjalan.text,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            DateTime.now().toString(),
            _preferences.getString("username"),
            null,
            null,
            1,
            null
        )
    );
    _dbHelper.insertMS2Customer(MS2CustomerModel("00001", "00011", "${_providerOID.customerType}", null, _controllerFormNumber.text, _controllerNPWP.text, null, null, null, null, null, null, null, null, DateTime.now().toString(), _preferences.getString("username"), null, null, null));
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustomerCompany();
  }
}
