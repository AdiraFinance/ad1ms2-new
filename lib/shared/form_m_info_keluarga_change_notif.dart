import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_info_keluarga_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_family_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class FormMInfoKeluargaChangeNotif with ChangeNotifier {
  List<FormMInfoKelModel> _listFormInfoKel = [];
  bool _autoValidate = false;
  bool _flag = false;
  DbHelper _dbHelper = DbHelper();

  List<FormMInfoKelModel> get listFormInfoKel => _listFormInfoKel;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  void addListInfoKel(FormMInfoKelModel value) {
    _listFormInfoKel.add(value);
    if (this._autoValidate) autoValidate = false;
    notifyListeners();
  }

  void deleteListInfoKel(BuildContext context, int index) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return Theme(
          data: ThemeData(
              fontFamily: "NunitoSans",
              primaryColor: Colors.black,
              primarySwatch: primaryOrange,
              accentColor: myPrimaryColor
          ),
          child: AlertDialog(
            title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Apakah kamu yakin menghapus data ini?",),
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  this._listFormInfoKel.removeAt(index);
                  notifyListeners();
                  Navigator.pop(context);
                },
                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak'),
              ),
            ],
          ),
        );
      }
    );
  }

  void updateListInfoKel(
      FormMInfoKelModel mInfoKelModel, BuildContext context, int index) {
    this._listFormInfoKel[index] = mInfoKelModel;
    notifyListeners();
    Navigator.pop(context);
  }

  bool get flag => _flag;

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  Future<bool> onBackPress() async{
    if (_listFormInfoKel.length != 0) {
      flag = false;
      saveToSQLite();
    } else {
      flag = true;
    }
    return true;
  }

  // Future<void> clearDataKeluarga() async {
  void clearDataKeluarga() {
    this._listFormInfoKel = [];
  }

  void saveToSQLite(){
    for(int i=0; i<_listFormInfoKel.length; i++){
      _dbHelper.insertMS2CustFamily(
          MS2CustFamilyModel(
              "123",
              _listFormInfoKel[i].relationshipStatusModel != null ? _listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID : "",
              _listFormInfoKel[i].relationshipStatusModel != null ? _listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_NAME : "",
              _listFormInfoKel[i].namaLengkapSesuaiIdentitas,
              _listFormInfoKel[i].namaLengkap,
              _listFormInfoKel[i].noIdentitas,
              _listFormInfoKel[i].birthDate,
              _listFormInfoKel[i].tmptLahirSesuaiIdentitas,
              _listFormInfoKel[i].birthPlaceModel != null ? _listFormInfoKel[i].birthPlaceModel.KABKOT_ID : "",
              _listFormInfoKel[i].birthPlaceModel != null ? _listFormInfoKel[i].birthPlaceModel.KABKOT_NAME : "",
              _listFormInfoKel[i].gender,
              _listFormInfoKel[i].gender == "01"? "Laki laki" : "Perempuan",
              null,
              _listFormInfoKel[i].identityModel != null ? _listFormInfoKel[i].identityModel.id:"",
              _listFormInfoKel[i].identityModel != null ? _listFormInfoKel[i].identityModel.name:"",
              null,
              null,
              null,
              null,
              1,
              null,
              _listFormInfoKel[i].noTlpn,
              _listFormInfoKel[i].kodeArea,
              _listFormInfoKel[i].noHp
          )
      );
    }
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustFamily();
  }

  void setDataFromSQLite() async{
    List _data =  await _dbHelper.selectDataInfoKeluarga("");
    if(_data.isNotEmpty){
      for(int i=0; i <_data.length; i++){
        this._listFormInfoKel.add(
            FormMInfoKelModel(
                IdentityModel(_data[i]['id_type'], _data[i]['id_desc']),
                RelationshipStatusModel(_data[i]['relation_stauts'], _data[i]['relation_status_desc']),
                _data[i]['id_no'],
                _data[i]['full_name_id'],
                _data[i]['full_name'],
                _data[i]['date_of_birth'],
                _data[i]['gender'],
                _data[i]['phone1_area'],
                _data[i]['phone1'],
                _data[i]['place_of_birth'],
                _data[i]['handphone_no'],
                BirthPlaceModel(_data[i]['place_of_birth_kabkota'], _data[i]['place_of_birth_kabkota_desc']
                )
            )
        );
      }
    }
    notifyListeners();
  }
}
