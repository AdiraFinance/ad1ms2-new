import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_pemegang_saham_pribadi_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_ind_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormMCompanyPemegangSahamPribadiChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  RelationshipStatusModel _relationshipStatusSelected;
//  TypeIdentityModel _typeIdentitySelected;
  IdentityModel _typeIdentitySelected;
  TextEditingController _controllerIdentityNumber = TextEditingController();
  TextEditingController _controllerFullNameIdentity = TextEditingController();
  TextEditingController _controllerFullName = TextEditingController();
  TextEditingController _controllerAlias = TextEditingController();
  TextEditingController _controllerDegree = TextEditingController();
  TextEditingController _controllerBirthOfDate = TextEditingController();
  DateTime _initialDateForBirthOfDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  TextEditingController _controllerPlaceOfBirthIdentity = TextEditingController();
  TextEditingController _controllerBirthPlaceIdentityLOV = TextEditingController();
  String _radioValueGender = "01";
  TextEditingController _controllerPercentShare = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  BirthPlaceModel _birthPlaceSelected;
  DbHelper _dbHelper = DbHelper();

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<RelationshipStatusModel> _listRelationshipStatus = RelationshipStatusList().relationshipStatusItems;

  List<IdentityModel> _listTypeIdentity = IdentityType().lisIdentityModel;

//  List<TypeIdentityModel> _listTypeIdentity = [
////    TypeIdentityModel("01", "KTP"),
////    TypeIdentityModel("02", "NPWP"),
//    TypeIdentityModel("01", "KTP"),
//    TypeIdentityModel("03", "PASSPORT"),
//    TypeIdentityModel("04", "SIM"),
//    TypeIdentityModel("05", "KTP Sementara"),
//    TypeIdentityModel("06", "Resi KTP"),
//    TypeIdentityModel("07", "Ket. Domisili"),
//  ];

  // Status Hubungan
  UnmodifiableListView<RelationshipStatusModel> get listRelationshipStatus {
    return UnmodifiableListView(this._listRelationshipStatus);
  }

  RelationshipStatusModel get relationshipStatusSelected =>
      _relationshipStatusSelected;

  set relationshipStatusSelected(RelationshipStatusModel value) {
    this._relationshipStatusSelected = value;
    if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" || this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04") {
      radioValueGender = "01";
    } else if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" || this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "05") {
      radioValueGender = "02";
    }
    notifyListeners();
  }

  // Jenis Identitas
  UnmodifiableListView<IdentityModel> get listTypeIdentity {
    return UnmodifiableListView(this._listTypeIdentity);
  }

  IdentityModel get typeIdentitySelected => _typeIdentitySelected;

  set typeIdentitySelected(IdentityModel value) {
    this._typeIdentitySelected = value;
    this._controllerIdentityNumber.clear();
    notifyListeners();
  }

  // No Identitas
  TextEditingController get controllerIdentityNumber {
    return this._controllerIdentityNumber;
  }

  set controllerIdentityNumber(value) {
    this._controllerIdentityNumber = value;
    this.notifyListeners();
  }

  // Nama Lengkap Sesuai Identitas
  TextEditingController get controllerFullNameIdentity {
    return this._controllerFullNameIdentity;
  }

  set controllerFullNameIdentity(value) {
    this._controllerFullNameIdentity = value;
    this.notifyListeners();
  }

  // Nama Lengkap
  TextEditingController get controllerFullName {
    return this._controllerFullName;
  }

  set controllerFullName(value) {
    this._controllerFullName = value;
    this.notifyListeners();
  }

  // Alias
  TextEditingController get controllerAlias {
    return this._controllerAlias;
  }

  set controllerAlias(value) {
    this._controllerAlias = value;
    this.notifyListeners();
  }

  // Gelar
  TextEditingController get controllerDegree {
    return this._controllerDegree;
  }

  set controllerDegree(value) {
    this._controllerDegree = value;
    this.notifyListeners();
  }

  // Tanggal Lahir
  TextEditingController get controllerBirthOfDate => _controllerBirthOfDate;

  DateTime get initialDateForBirthOfDate => _initialDateForBirthOfDate;

  void selectBirthDate(BuildContext context) async {
    DatePickerShared _datePickerShared = DatePickerShared();
    var _datePickerSelected = await _datePickerShared.selectStartDate(
        context, this._initialDateForBirthOfDate,
        canAccessNextDay: false);
    if (_datePickerSelected != null) {
      this.controllerBirthOfDate.text = dateFormat.format(_datePickerSelected);
      this._initialDateForBirthOfDate = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  // Tempat Lahir Sesuai Identitas
  TextEditingController get controllerPlaceOfBirthIdentity {
    return this._controllerPlaceOfBirthIdentity;
  }

  set controllerPlaceOfBirthIdentity(value) {
    this._controllerPlaceOfBirthIdentity = value;
    this.notifyListeners();
  }

  // Tempat Lahir Sesuai Identitas LOV
  TextEditingController get controllerBirthPlaceIdentityLOV =>
      _controllerBirthPlaceIdentityLOV;

  set controllerBirthPlaceIdentityLOV(TextEditingController value) {
    this._controllerBirthPlaceIdentityLOV = value;
    notifyListeners();
  }

  // Jenis Kalmin
  String get radioValueGender {
    return _radioValueGender;
  }

  set radioValueGender(String value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  // % Share
  TextEditingController get controllerPercentShare {
    return this._controllerPercentShare;
  }

  set controllerPercentShare(value) {
    this._controllerPercentShare = value;
    this.notifyListeners();
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  BirthPlaceModel get birthPlaceSelected => _birthPlaceSelected;

  set birthPlaceSelected(BirthPlaceModel value) {
    this._birthPlaceSelected = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerBirthPlaceIdentityLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  // Future<void> clearDataPemegangSahamPribadi() async {
  void clearDataPemegangSahamPribadi() {
    this._autoValidate = false;
    this._relationshipStatusSelected = null;
    this._typeIdentitySelected = null;
    this._controllerIdentityNumber.clear();
    this._controllerFullNameIdentity.clear();
    this._controllerFullName.clear();
    // _controllerAlias.clear();
    // _controllerDegree.clear();
    // _controllerBirthOfDate.clear();
    // _controllerPlaceOfBirthIdentity.clear();
    // _controllerBirthPlaceIdentityLOV.clear();
    // _radioValueGender = "01";
    // _controllerPercentShare.clear();
  }

  void saveToSQLite(){
    _dbHelper.insertMS2CustShrhldrInd(MS2CustShrhldrIndModel(
      "123",
      null,
      0,
      _relationshipStatusSelected.PARA_FAMILY_TYPE_ID,
      _relationshipStatusSelected.PARA_FAMILY_TYPE_NAME,
      _typeIdentitySelected.id,
      _typeIdentitySelected.name,
      _controllerIdentityNumber.text,
      _controllerFullNameIdentity.text,
      _controllerFullName.text,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    )
    );
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustShrhldrInd();
  }
}
