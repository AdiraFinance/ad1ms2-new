import 'package:ad1ms2_dev/models/branch_marketing_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class SAJobMayorChangeNotifier with ChangeNotifier{
  TextEditingController _controllerAppNo = TextEditingController();
  TextEditingController _controllerNumberOrder = TextEditingController();
  TextEditingController _controllerCustomerName = TextEditingController();
  TextEditingController _controllerCellPhoneNumber = TextEditingController();
  TextEditingController _controllerPhoneNumber = TextEditingController();
  List<BranchMarketingModel> _listBranchMarketing = [
    BranchMarketingModel("001", "MARKETING CABANG 1"),
    BranchMarketingModel("002", "MARKETING CABANG 2"),
    BranchMarketingModel("003", "MARKETING CABANG 3"),
  ];
  TextEditingController _controllerAppointmentSurveyDate = TextEditingController();
  bool _autoValidate = false;
  BranchMarketingModel _branchMarketingSelected;
  DateTime _initialAppointmentSurveyDate = DateTime(dateNow.year,dateNow.month,dateNow.day);

  TextEditingController get controllerAppNo => _controllerAppNo;

  TextEditingController get controllerNumberOrder => _controllerNumberOrder;

  TextEditingController get controllerCustomerName => _controllerCustomerName;

  TextEditingController get controllerCellPhoneNumber =>
      _controllerCellPhoneNumber;

  TextEditingController get controllerPhoneNumber => _controllerPhoneNumber;

  TextEditingController get controllerAppointmentSurveyDate =>
      _controllerAppointmentSurveyDate;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<BranchMarketingModel> get listBranchMarketing => _listBranchMarketing;

  BranchMarketingModel get branchMarketingSelected => _branchMarketingSelected;

  set branchMarketingSelected(BranchMarketingModel value) {
    this._branchMarketingSelected = value;
    notifyListeners();
  }

  void selectAppointmentSurveyDate(BuildContext context) async {
    final DateTime _picked = await showDatePicker(
        context: context,
        initialDate: _initialAppointmentSurveyDate,
        firstDate: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day),
        lastDate: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day));
    if (_picked != null) {
      this._initialAppointmentSurveyDate = _picked;
      this._controllerAppointmentSurveyDate.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }
}