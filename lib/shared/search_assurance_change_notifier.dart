import 'dart:collection';

import 'package:ad1ms2_dev/models/assurance_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class SearchAssuranceChangeNotifier with ChangeNotifier {
    bool _showClear = false;
    TextEditingController _controllerSearch = TextEditingController();

    List<AssuranceModel> _listAssurance = [
        AssuranceModel("A", "1"),
        AssuranceModel("B", "11"),
        AssuranceModel("C", "12"),
        AssuranceModel("D", "13")
    ];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<AssuranceModel> get listAssuranceModel {
        return UnmodifiableListView(this._listAssurance);
    }
}
