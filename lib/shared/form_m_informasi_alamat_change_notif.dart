import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FormMInfoAlamatChangeNotif with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  int _oldListSize = 0;
  int _selectedIndex = -1;
  bool _autoValidate = false;
  List<AddressModel> _listAlamatKorespondensi = [];
  TextEditingController _controllerInfoAlamat = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTeleponArea = TextEditingController();
  TextEditingController _controllerTelepon = TextEditingController();
  KelurahanModel _kelurahanSelected;
  DbHelper _dbHelper = DbHelper();

  int get oldListSize => _oldListSize;

  set oldListSize(int value) {
    this._oldListSize = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  List<AddressModel> get listAlamatKorespondensi => _listAlamatKorespondensi;

  // Alamat
  TextEditingController get controllerInfoAlamat => _controllerInfoAlamat;

  // Jenis Alamat
  TextEditingController get controllerAddressType => _controllerAddressType;

  // RT
  TextEditingController get controllerRT => _controllerRT;

  // RW
  TextEditingController get controllerRW => _controllerRW;

  // Kelurahan
  TextEditingController get controllerKelurahan => _controllerKelurahan;

  // Kecamatan
  TextEditingController get controllerKecamatan => _controllerKecamatan;

  // Kabupaten/Kota
  TextEditingController get controllerKota => _controllerKota;

  // Provinsi
  TextEditingController get controllerProvinsi => _controllerProvinsi;

  // Kode Pos
  TextEditingController get controllerPostalCode => _controllerPostalCode;

  // Telepon 1 Area
  TextEditingController get controllerTeleponArea => _controllerTeleponArea;

  // Telepon 1
  TextEditingController get controllerTelepon => _controllerTelepon;

  void addAlamatKorespondensi(AddressModel value) {
    this._listAlamatKorespondensi.add(value);
    notifyListeners();
  }

  void updateAlamatKorespondensi(AddressModel value, int index) {
    this._listAlamatKorespondensi[index] = value;
    notifyListeners();
  }

  void deleteAlamatKorespondensi(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listAlamatKorespondensi.removeAt(index);
                    if (selectedIndex == index) {
                      selectedIndex = -1;
                    }
                    this._controllerInfoAlamat.clear();
                    this._controllerAddressType.clear();
                    this._controllerRT.clear();
                    this._controllerRW.clear();
                    this._controllerKelurahan.clear();
                    this._controllerKecamatan.clear();
                    this._controllerKota.clear();
                    this._controllerProvinsi.clear();
                    this._controllerPostalCode.clear();
                    this._controllerTeleponArea.clear();
                    this._controllerTelepon.clear();
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void setAddress(AddressModel value) {
    kelurahanSelected = value.kelurahanModel;
    this._controllerInfoAlamat.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerTeleponArea.text = value.areaCode;
    this._controllerTelepon.text = value.phone;
    notifyListeners();
  }

  void setCorrespondence(int index){
    for(int i=0; i < this._listAlamatKorespondensi.length; i++){
      if(this._listAlamatKorespondensi[i].isCorrespondence){
        this._listAlamatKorespondensi[i].isCorrespondence = false;
      }
    }
    this._listAlamatKorespondensi[index].isCorrespondence = true;
    notifyListeners();
  }

  void iconShowDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    "∙ Tekan 1x untuk edit",
                  ),
                  Text(
                    "∙ Tekan lama untuk memilih alamat korespondensi",
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height/37,),
                  Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25,
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void isShowDialog(BuildContext context) {
    if(this.listAlamatKorespondensi.length == 1 || this.listAlamatKorespondensi.length == 2) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  fontFamily: "NunitoSans"
              ),
              child: AlertDialog(
                title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "∙ Tekan 1x untuk edit",
                    ),
                    Text(
                      "∙ Tekan lama untuk memilih alamat korespondensi",
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/37,),
                    Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                        // _updateStatusShowDialogSimilarity();
                      },
                      child: Text(
                          "CLOSE",
                          style: TextStyle(
                              color: primaryOrange,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25
                          )
                      )
                  )
                ],
              ),
            );
          }
      );
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }


  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return Theme(
          data: ThemeData(
              fontFamily: "NunitoSans"
          ),
          child: AlertDialog(
            title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.location_on,
                        color: primaryOrange,
                        size: 22.0,
                      ),
                      SizedBox(width: 12.0),
                      Expanded(
                        child: GestureDetector(
                          onTap: (){
                            selectedIndex = index;
                            controllerInfoAlamat.clear();
                            setAddress(listAlamatKorespondensi[index]);
                            setCorrespondence(index);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Pilih sebagai Alamat Korespondensi",
                            style: TextStyle(fontSize: 14.0),
                          ),
                        ),
                      )
                    ]
                ),
                SizedBox(height: 12.0),
                listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                    ? listAlamatKorespondensi[index].isSameWithIdentity
                    ? SizedBox()
                    : Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.delete,
                        color: Colors.red,
                        size: 22.0,
                      ),
                      SizedBox(
                        width: 12.0,
                      ),
                      GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                          deleteAlamatKorespondensi(context, index);
                        },
                        child: Text(
                          "Hapus",
                          style: TextStyle(fontSize: 14.0, color: Colors.red),
                        ),
                      )
                    ]
                )
                    : SizedBox(),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: (){
                    Navigator.pop(context);
                    // _updateStatusShowDialogSimilarity();
                  },
                  child: Text(
                      "CLOSE",
                      style: TextStyle(
                          color: primaryOrange,
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.25
                      )
                  )
              )
            ],
          ),
        );
      }
    );
  }

  void moreOption(context, index) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Theme(
            data: ThemeData(fontFamily: "NunitoSans"),
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.close,
                              color: Colors.black,
                              size: 22.0,
                            ),
                          ])),
                  FlatButton(
                      onPressed: () {
                        selectedIndex = index;
                        controllerInfoAlamat.clear();
                        setAddress(listAlamatKorespondensi[index]);
                        setCorrespondence(index);
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              color: primaryOrange,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Pilih sebagai Alamat Korespondensi",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                  listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                  ? listAlamatKorespondensi[index].isSameWithIdentity
                      ? SizedBox()
                      : FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                        deleteAlamatKorespondensi(context, index);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.delete,
                              color: Colors.red,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Hapus",
                              style: TextStyle(fontSize: 18.0, color: Colors.red),
                            )
                          ]
                      )
                  )
                  : SizedBox(),
                ],
              ),
            ),
          );
        }
    );
  }

  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanSelected = value;
  }

  // Future<void> clearDataAlamat() async {
  void clearDataAlamat() {
    this._flag = false;
    this._autoValidate = false;
    this._selectedIndex = -1;
    this._listAlamatKorespondensi = [];
    this._controllerInfoAlamat.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerTeleponArea.clear();
    this._controllerTelepon.clear();
  }

  void saveToSQLite(String type){
    List<MS2CustAddrModel> _listAddress = [];
    for(int i=0; i<_listAlamatKorespondensi.length; i++){
      _listAddress.add(MS2CustAddrModel("123", _listAlamatKorespondensi[i].isCorrespondence.toString(), type,
          _listAlamatKorespondensi[i].address, _listAlamatKorespondensi[i].rt, null, _listAlamatKorespondensi[i].rw, null,
          _listAlamatKorespondensi[i].kelurahanModel.PROV_ID, _listAlamatKorespondensi[i].kelurahanModel.PROV_NAME,
          _listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID, _listAlamatKorespondensi[i].kelurahanModel.KABKOT_NAME,
          _listAlamatKorespondensi[i].kelurahanModel.KEC_ID, _listAlamatKorespondensi[i].kelurahanModel.KEC_NAME,
          _listAlamatKorespondensi[i].kelurahanModel.KEL_ID, _listAlamatKorespondensi[i].kelurahanModel.KEL_NAME,
          _listAlamatKorespondensi[i].kelurahanModel.ZIPCODE, null, _listAlamatKorespondensi[i].phone, _listAlamatKorespondensi[i].areaCode,
          null, null, null, null, _listAlamatKorespondensi[i].jenisAlamatModel.KODE,
          _listAlamatKorespondensi[i].jenisAlamatModel.DESKRIPSI, _listAlamatKorespondensi[i].addressLatLong['latitude'].toString(),
          _listAlamatKorespondensi[i].addressLatLong['longitude'].toString(),
          _listAlamatKorespondensi[i].addressLatLong['address'].toString(), null, null, null, null, 1));

      // _dbHelper.insertMS2CustAddr(MS2CustAddrModel("123", _listAlamatKorespondensi[i].isCorrespondence.toString(), type,
      //     _listAlamatKorespondensi[i].address, _listAlamatKorespondensi[i].rt, null, _listAlamatKorespondensi[i].rw, null,
      //     _listAlamatKorespondensi[i].kelurahanModel.PROV_ID, _listAlamatKorespondensi[i].kelurahanModel.PROV_NAME,
      //     _listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID, _listAlamatKorespondensi[i].kelurahanModel.KABKOT_NAME,
      //     _listAlamatKorespondensi[i].kelurahanModel.KEC_ID, _listAlamatKorespondensi[i].kelurahanModel.KEC_NAME,
      //     _listAlamatKorespondensi[i].kelurahanModel.KEL_ID, _listAlamatKorespondensi[i].kelurahanModel.KEL_NAME,
      //     _listAlamatKorespondensi[i].kelurahanModel.ZIPCODE, null, _listAlamatKorespondensi[i].phone, _listAlamatKorespondensi[i].areaCode,
      //     null, null, null, null, _listAlamatKorespondensi[i].jenisAlamatModel.KODE,
      //     _listAlamatKorespondensi[i].jenisAlamatModel.DESKRIPSI, _listAlamatKorespondensi[i].addressLatLong['latitude'].toString(),
      //     _listAlamatKorespondensi[i].addressLatLong['longitude'].toString(),
      //     _listAlamatKorespondensi[i].addressLatLong['address'].toString(), null, null, null, null, 1));
    }
    _dbHelper.insertMS2CustAddr(_listAddress);
  }

  Future<bool> deleteSQLite(String type) async{
    print("jalan bro info alamat");
    return await _dbHelper.deleteMS2CustAddr(type);
  }

  void setDataFromSQLite(String type) async{
    var _check = await _dbHelper.selectMS2CustAddr(type);
    print(_check);
    if(_check.isNotEmpty){
      for(int i=0; i<_check.length; i++){
        var jenisAlamatModel = JenisAlamatModel(_check[i]['addr_type'], _check[i]['addr_desc']);
        var kelurahanModel = KelurahanModel(_check[i]['kelurahan'], _check[i]['kelurahan_desc'],
            _check[i]['kecamatan'], _check[i]['kecamatan_desc'], _check[i]['kabkot'], _check[i]['kabkot_desc'],
            _check[i]['provinsi'], _check[i]['provinsi_desc'], _check[i]['zip_code']);
        var _addressFromMap = {
          "latitude": _check[i]['latitude'],
          "longitude": _check[i]['longitude']
        };
        _listAlamatKorespondensi.add(AddressModel(
            jenisAlamatModel,
            kelurahanModel,
            _check[i]['address'],
            _check[i]['rt'],
            _check[i]['rw'],
            _check[i]['phone1_area'],
            _check[i]['phone1'],
            _check[i]['koresponden'] == "true" ? true : false,
            _addressFromMap,
            false
        ));
        if(_check[i]['koresponden'] == "true"){
          selectedIndex = i;
          controllerInfoAlamat.clear();
          setAddress(listAlamatKorespondensi[i]);
          setCorrespondence(i);
        }
      }
    }
    notifyListeners();
  }
}
