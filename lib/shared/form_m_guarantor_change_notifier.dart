import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_comp_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_ind_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class FormMGuarantorChangeNotifier with ChangeNotifier {
  int _radioValueIsWithGuarantor = 0;
  bool _autoValidate = false;
  List<GuarantorIndividualModel> _listGuarantorIndividual = [];

  List<GuarantorCompanyModel> _listGuarantorCompany = [];
  DbHelper _dbHelper = DbHelper();

  int get radioValueIsWithGuarantor => _radioValueIsWithGuarantor;

  set radioValueIsWithGuarantor(int value) {
    this._radioValueIsWithGuarantor = value;
    notifyListeners();
  }

  List<GuarantorIndividualModel> get listGuarantorIndividual => _listGuarantorIndividual;

  List<GuarantorCompanyModel> get listGuarantorCompany => _listGuarantorCompany;

  void addGuarantorIndividual(GuarantorIndividualModel model) {
    this._listGuarantorIndividual.add(model);
    if(this._autoValidate)autoValidate = false;
    notifyListeners();
  }

  void addGuarantorCompany(GuarantorCompanyModel model) {
    this._listGuarantorCompany.add(model);
    if(this._autoValidate)autoValidate = false;
    notifyListeners();
  }

  void deleteListGuarantorIndividual(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus penjamin ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listGuarantorIndividual.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void updateListGuarantorIndividual(GuarantorIndividualModel value, int index) {
    this._listGuarantorIndividual[index] = value;
    notifyListeners();
  }

  void updateListGuarantorCompany(GuarantorCompanyModel value, int index) {
    this._listGuarantorCompany[index] = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Future<void> clearDataGuarantor() async {
  void clearDataGuarantor() {
    this._autoValidate = false;
    this._radioValueIsWithGuarantor = 1;
    this._listGuarantorIndividual = [];
    this._listGuarantorCompany = [];
  }


  void saveToSQLite(){
    List<MS2GrntrIndModel> _listDataIndividu = [];
    List<MS2GrntrCompModel> _listDataCompany = [];
    for(int i=0; i<_listGuarantorIndividual.length;i++){
      _listDataIndividu.add(MS2GrntrIndModel(
         "123",
          _listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
          _listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_NAME,
         0,
          _listGuarantorIndividual[i].identityModel.id,
          _listGuarantorIndividual[i].identityModel.name,
          _listGuarantorIndividual[i].identityNumber,
          _listGuarantorIndividual[i].fullNameIdentity,
          _listGuarantorIndividual[i].fullName,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null
        )
      );
    }
    for(int i=0; i<_listGuarantorCompany.length;i++){
      _listDataCompany.add(MS2GrntrCompModel(
        "123",
        0,
        _listGuarantorCompany[i].typeInstitutionModel.PARA_ID,
        _listGuarantorCompany[i].typeInstitutionModel.PARA_NAME,
        null,
        null,
        _listGuarantorCompany[i].institutionName,
        null,
        null,
        null,
        null,
        null,
        null,
        _listGuarantorCompany[i].establishDate,
        null,
        null,
        null,
        null,
        null,
        _listGuarantorCompany[i].npwp,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
      )
      );
    }
    _dbHelper.insertMS2GrntrInd(_listDataIndividu);
    _dbHelper.insertMS2GrntrComp(_listDataCompany);
  }

  void saveToSQLiteAddressGuarantorInd(String type){
    List<MS2CustAddrModel> _listAddressIndividu = [];
    List<MS2CustAddrModel> _listAddressCompany = [];
    for(int i=0; i<_listGuarantorIndividual.length; i++){
      for(int j=0; j<_listGuarantorIndividual[i].listAddressGuarantorModel.length; i++){
        _listAddressIndividu.add(MS2CustAddrModel(
            "123",
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence.toString(),
            type,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
            null,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
            null,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_NAME,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_NAME,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_NAME,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_NAME,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE,
            null,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
            null,
            null,
            null,
            null,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.DESKRIPSI,
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['latitude'].toString(),
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['longitude'].toString(),
            _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['address'].toString(),
            null,
            null,
            null,
            null,
            1
        ));
      }
    }

    for(int i=0; i<_listGuarantorCompany.length; i++){
      for(int j=0; j<_listGuarantorCompany[i].listAddressGuarantorModel.length; i++){
        _listAddressCompany.add(MS2CustAddrModel(
            "123",
            _listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence.toString(),
            type,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].address,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
            null,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
            null,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_NAME,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_NAME,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_NAME,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_NAME,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE,
            null,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.DESKRIPSI,
            _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['latitude'].toString(),
            _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['longitude'].toString(),
            _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['address'].toString(),
            null,
            null,
            null,
            null,
            1
        ));
      }
    }
    _dbHelper.insertMS2CustAddr(_listAddressIndividu);
    _dbHelper.insertMS2CustAddr(_listAddressCompany);
  }

  void deleteSQLite(){
    _dbHelper.deleteMS2GrntrComp();
    _dbHelper.deleteMS2GrntrInd();
  }
}
