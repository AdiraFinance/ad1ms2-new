import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_photo.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/screens/detail_image.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/document_unit_model.dart';
import 'package:ad1ms2_dev/shared/group_unit_object_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/io_client.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';

import '../main.dart';

class FormMFotoChangeNotifier with ChangeNotifier {
  List<ImageFileModel> _listFotoTempatTinggal = [];
  bool _autoValidate = false;
  bool _autoValidateFotoTempatTinggal = false;
  bool _autoValidateGroupObjectUnit = false;
  bool _autoValidateDocumentObjectUnit = false;
  bool _autoValidateTempatUsaha = false;
  final _imagePicker = ImagePicker();
  OccupationModel _occupationSelected;
  List<ImageFileModel> _listFotoTempatUsaha = [];
  KegiatanUsahaModel _kegiatanUsahaSelected;
  JenisKegiatanUsahaModel _jenisKegiatanUsahaSelected;
  JenisKonsepModel _jenisKonsepSelected;
  List<GroupUnitObjectModel> _listGroupUnitObject = [];
  List<DocumentUnitModel> _listDocument = [];

  List<OccupationModel> _listOccupation = [];
  List<FinancingTypeModel> _listTypeOfFinancing = TypeOfFinancingList().financingTypeList;
  String _pathFile;
  DbHelper _dbHelper = DbHelper();

  // Future<void> getListOccupation(BuildContext context) async{
  //   this._typeOfFinancingModelSelected = this._listTypeOfFinancing[0];
  //   try{
  //     _listOccupation.clear();
  //     final ioc = new HttpClient();
  //     ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  //     final _http = IOClient(ioc);
  //     final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/occupation/get-jenis-pekerjaan");
  //     final _data = jsonDecode(_response.body);
  //     for(int i=0; i < _data.length;i++){
  //       _listOccupation.add(OccupationModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
  //     }
  //     setValueEdit(context);
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  void getListOccupation(BuildContext context) async{
    this._typeOfFinancingModelSelected = this._listTypeOfFinancing[0];
    _listOccupation.clear();
    this._occupationSelected = null;
    loadData = true;
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get(
          "${BaseUrl.urlPublic}api/occupation/get-jenis-pekerjaan"
          // "https://103.110.89.34/public/ms2dev/api/occupation/get-jenis-pekerjaan"
      );
      final _data = jsonDecode(_response.body);
      for(int i=0; i < _data.length;i++){
        _listOccupation.add(OccupationModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
      loadData = false;
      getBusinessActivities(context);
      // setValueEdit(context);
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  List<JenisKegiatanUsahaModel> _listJenisKegiatanUsaha = [];

  // Future<void> getListKegiatanUsaha() async{
  //   try{
  //       _listJenisKegiatanUsaha.clear();
  //       final ioc = new HttpClient();
  //       ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  //       final _http = IOClient(ioc);
  //       var _body = jsonEncode({
  //           "P_KODE" : _kegiatanUsahaSelected.id
  //       });
  //       final _response = await _http.post(
  //         "https://103.110.89.34/public/ms2dev/api/parameter/get-jenis-kegiatan-usaha-name",
  //         body: _body,
  //         headers: {"Content-Type":"application/json"}
  //       );
  //
  //       final _result = jsonDecode(_response.body);
  //       final _data = _result['data'];
  //       if(_response.statusCode == 200){
  //         for(int i=0; i<_data.length; i++){
  //           _listJenisKegiatanUsaha.add(JenisKegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
  //         }
  //       }
  //       notifyListeners();
  //   } catch(e){
  //     print(e);
  //   }
  // }

  void setDefaultValue() {
    this._jenisKonsepSelected = this._listJenisKonsep[2];
  }

  setValueEdit(BuildContext context){
    var data = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
//    occupation
    if(data.occupationSelected != null){
      for (int i = 0; i < this._listOccupation.length; i++) {
        if (data.occupationSelected.KODE == this._listOccupation[i].KODE) {
          this._occupationSelected = this._listOccupation[i];
        }
      }
    }
//    jenis kegiatan usaha
    if(data.jenisKegiatanUsahaSelected != null){
      for (int i = 0; i < this._listJenisKegiatanUsaha.length; i++) {
        if (data.jenisKegiatanUsahaSelected.id == this._listJenisKegiatanUsaha[i].id) {
          this._jenisKegiatanUsahaSelected = this._listJenisKegiatanUsaha[i];
        }
      }
    }
  }

  List<KegiatanUsahaModel> _listKegiatanUsaha = [
    // KegiatanUsahaModel(1, "Investasi"),
    // KegiatanUsahaModel(2, "Multiguna"),
    // KegiatanUsahaModel(3, "Modal Kerja"),
  ];

  List<JenisKonsepModel> _listJenisKonsep = [
    JenisKonsepModel("01", "SATU UNIT MULTI JAMINAN"),
    JenisKonsepModel("02", "MULTI UNIT SATU  JAMINAN"),
    JenisKonsepModel("03", "MULTI UNIT DENGAN JAMINAN DAN TANPA JAMINAN")
  ];

  List<ImageFileModel> get listFotoTempatTinggal => _listFotoTempatTinggal;

  void addFotoTempatTinggal(ImageFileModel data) {
    this._listFotoTempatTinggal.add(data);
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get autoValidateFotoTempatTinggal => _autoValidateFotoTempatTinggal;

  set autoValidateFotoTempatTinggal(bool value) {
    this._autoValidateFotoTempatTinggal = value;
    notifyListeners();
  }

  bool get autoValidateGroupObjectUnit => _autoValidateGroupObjectUnit;

  set autoValidateGroupObjectUnit(bool value) {
    this._autoValidateGroupObjectUnit = value;
    notifyListeners();
  }

  bool get autoValidateDocumentObjectUnit => _autoValidateDocumentObjectUnit;

  set autoValidateDocumentObjectUnit(bool value) {
    this._autoValidateDocumentObjectUnit = value;
    notifyListeners();
  }

  bool get autoValidateTempatUsaha => _autoValidateTempatUsaha;

  set autoValidateTempatUsaha(bool value) {
    this._autoValidateTempatUsaha = value;
    notifyListeners();
  }

  void addFile(int idImageArray) async {
    var _image = await _imagePicker.getImage(
        source: ImageSource.camera,
        maxHeight: 1920.0,
        maxWidth: 1080.0,
        imageQuality: 50);
    savePhoto(_image, idImageArray);
  }

  UnmodifiableListView<OccupationModel> get listOccupation {
    return UnmodifiableListView(this._listOccupation);
  }

  OccupationModel get occupationSelected => _occupationSelected;

  String get pathFile => _pathFile;

  set occupationSelected(OccupationModel value) {
    this._occupationSelected = value;
    notifyListeners();
  }

  List<ImageFileModel> get listFotoTempatUsaha => _listFotoTempatUsaha;

  UnmodifiableListView<KegiatanUsahaModel> get listKegiatanUsaha {
    return UnmodifiableListView(this._listKegiatanUsaha);
  }

  KegiatanUsahaModel get kegiatanUsahaSelected => _kegiatanUsahaSelected;

  set kegiatanUsahaSelected(KegiatanUsahaModel value) {
    this._kegiatanUsahaSelected = value;
    this._jenisKegiatanUsahaSelected = null;
    // getListKegiatanUsaha();
    notifyListeners();
  }

  UnmodifiableListView<JenisKegiatanUsahaModel> get listJenisKegiatanUsaha {
      return UnmodifiableListView(this._listJenisKegiatanUsaha);
  }

  JenisKegiatanUsahaModel get jenisKegiatanUsahaSelected =>
      _jenisKegiatanUsahaSelected;

  set jenisKegiatanUsahaSelected(JenisKegiatanUsahaModel value) {
    this._jenisKegiatanUsahaSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<JenisKonsepModel> get listJenisKonsep {
    return UnmodifiableListView(this._listJenisKonsep);
  }

  JenisKonsepModel get jenisKonsepSelected => _jenisKonsepSelected;

  set jenisKonsepSelected(JenisKonsepModel value) {
    this._jenisKonsepSelected = value;
    notifyListeners();
  }

  List<GroupUnitObjectModel> get listGroupUnitObject => _listGroupUnitObject;

  void removeListGroupUnitObject(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus group unit object ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listGroupUnitObject.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void addToListGroupUnitObject(
      GroupObjectUnit groupObjectUnit,
      ObjectUnit objectUnit,
      List<ImageFileModel> imageFiles,
      BuildContext context) {
    this
        ._listGroupUnitObject
        .add(GroupUnitObjectModel(groupObjectUnit, objectUnit, imageFiles));
    if (this._autoValidateGroupObjectUnit) {
      autoValidateGroupObjectUnit = false;
    }
    notifyListeners();
    Navigator.pop(context, true);
  }

  void updateToListGroupUnitObject(
      GroupObjectUnit groupObjectUnit,
      ObjectUnit objectUnit,
      List<ImageFileModel> imageFiles,
      BuildContext context,
      int index) {
    this._listGroupUnitObject[index] =
        GroupUnitObjectModel(groupObjectUnit, objectUnit, imageFiles);
    notifyListeners();
    Navigator.pop(context, true);
  }

  List<DocumentUnitModel> get listDocument => _listDocument;

  void addListDocument(
      DocumentUnitModel documentUnitModel, BuildContext context) {
    this._listDocument.add(documentUnitModel);
    if (this._autoValidateDocumentObjectUnit) {
      autoValidateDocumentObjectUnit = false;
    }
    notifyListeners();
    Navigator.pop(context);
  }

  void updateListDocumentObjectUnit(
      DocumentUnitModel documentUnitModel, BuildContext context, int index) {
    this._listDocument[index] = documentUnitModel;
    notifyListeners();
    Navigator.pop(context);
  }

  void deleteListDocumentObjectUnit(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus dokumen unit object ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    deleteFileDocument(index);
                    this._listDocument.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void deleteFileDocument(int index) async{
    final file =  File("${this._listDocument[index].path}");
    await file.delete();
  }

  void actionDeleteDetailImageResidence(String action,int index,BuildContext context){
    if(TitlePopUpMenuButton.Delete == action){
      deleteFile(index, "tinggal");
      this._listFotoTempatTinggal.removeAt(index);
      notifyListeners();
    }
    else{
      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: this._listFotoTempatTinggal[index].imageFile,),));
    }
  }

  void actionDeleteDetailImageBusinessPlace(String action,int index,BuildContext context){
    if(TitlePopUpMenuButton.Delete == action){
      deleteFile(index, "usaha");
      this._listFotoTempatUsaha.removeAt(index);
      notifyListeners();
    }
    else{
      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: this._listFotoTempatUsaha[index].imageFile,),));
    }
  }

  UnmodifiableListView<FinancingTypeModel> get listTypeOfFinancing {
    return UnmodifiableListView(this._listTypeOfFinancing);
  }

  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getListBusinessActivitiesType(BuildContext context) async{
    Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected = null;
    Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).installmentTypeSelected = null;
    try{
      this._listJenisKegiatanUsaha.clear();
      this._jenisKegiatanUsahaSelected = null;
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      var _body = jsonEncode({
        "P_FIN_TYPE" : this._typeOfFinancingModelSelected.financingTypeId
      });
      final _response = await _http.post(
        "${BaseUrl.urlPublic}api/parameter/get-jenis-kegiatan-usaha",
          // "https://103.110.89.34/public/ms2dev/api/parameter/get-jenis-kegiatan-usaha",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.length > 0){
        for(int i=0; i<_data.length; i++){
          _listJenisKegiatanUsaha.add(JenisKegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
        }
        loadData = false;
      }
      else{
        loadData = false;
        showSnackBar("Jenis kegiatan usaha tidak ditemukan");
      }
      setDataFromSQLite();
    }
    on TimeoutException catch(_){
      loadData = false;
      showSnackBar("Timeout Connection");
    }
    on SocketException catch(_){
      loadData = false;
      showSnackBar("Please check connection or contact server");
    }
    catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }

  FinancingTypeModel _typeOfFinancingModelSelected;

  FinancingTypeModel get typeOfFinancingModelSelected =>
      _typeOfFinancingModelSelected;

  set typeOfFinancingModelSelected(FinancingTypeModel value) {
    this._typeOfFinancingModelSelected = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getBusinessActivities(BuildContext context) async{
    loadData = true;
    Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected = null;
    Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).installmentTypeSelected = null;
    this._listKegiatanUsaha.clear();
    this._kegiatanUsahaSelected = null;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_FIN_TYPE" : this._typeOfFinancingModelSelected.financingTypeId
    });
    try{
      final _response = await _http.post(
          "${BaseUrl.urlPublic}api/parameter/get-kegiatan-usaha",
          // "https://103.110.89.34/public/ms2dev/api/parameter/get-kegiatan-usaha",
          body: _body,
          headers: {"Content-Type":"application/json"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.length > 0){
        for(int i=0; i < _data.length; i++){
          this._listKegiatanUsaha.add(KegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
        }
        loadData = false;
      }
      else{
        loadData = false;
        showSnackBar("Kegiatan usaha data tidak ditemukan");
      }
      getListBusinessActivitiesType(context);
    }
    on TimeoutException catch(_){
      loadData = false;
      showSnackBar("Timeout Connection");
    }
    on SocketException catch(_){
      loadData = false;
      showSnackBar("Please check connection or contact server");
    }
    catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void clearFormMFoto(){
    this._autoValidate = false;
    this._listFotoTempatTinggal.clear();
    this._occupationSelected = null;
    this._listOccupation.clear();
    this._listFotoTempatUsaha.clear();
    this._typeOfFinancingModelSelected = null;
    this._listKegiatanUsaha.clear();
    this._kegiatanUsahaSelected = null;
    this._jenisKegiatanUsahaSelected = null;
    this._listJenisKegiatanUsaha.clear();
    this._jenisKonsepSelected = null;
    this._listGroupUnitObject.clear();
    this._listDocument.clear();
  }

  void savePhoto(PickedFile data, int idImageArray) async{
    try{
      if (data != null) {
        File _imageFile = File(data.path);
        List<String> split = data.path.split("/");
        String fileName = split[split.length-1];
        File _path = await _imageFile.copy("$globalPath/$fileName");
        _pathFile = _path.path;
        Position _position = await Geolocator().getCurrentPosition(
            desiredAccuracy: LocationAccuracy.bestForNavigation);
        switch (idImageArray) {
          case 0:
            this._listFotoTempatTinggal.add(ImageFileModel(
                _imageFile, _position.latitude, _position.longitude, this._pathFile));
            if (this._autoValidateFotoTempatTinggal) {
              autoValidateFotoTempatTinggal = false;
            }
            notifyListeners();
            break;
          case 1:
            this._listFotoTempatUsaha.add(ImageFileModel(
                _imageFile, _position.latitude, _position.longitude, this._pathFile));
            if (this._autoValidateTempatUsaha) {
              autoValidateTempatUsaha = false;
            }
            notifyListeners();
            break;
        }
      } else {
        return;
      }
//      Uint8List bytes = this._fileDocumentUnitObject['file'].readAsBytesSync();
//      await file.writeAsBytes(bytes);
    }
    catch(e){
      print(e);
    }
  }

  void deleteFile(int index, String type) async{
    var file;
    if(type == "usaha"){
      file =  File("${this._listFotoTempatUsaha[index].path}");
    }
    else {
      file =  File("${this._listFotoTempatTinggal[index].path}");
    }
    await file.delete();
  }

  void saveToSQLite(){
    print("save");
    _dbHelper.insertMS2PhotoHeader(MS2ApplPhotoModel(this._listFotoTempatTinggal, this._occupationSelected, this._listFotoTempatUsaha,
        this._typeOfFinancingModelSelected, this._kegiatanUsahaSelected, this._jenisKegiatanUsahaSelected,
        this._jenisKonsepSelected, this._listGroupUnitObject, this._listDocument)
    );
  }

  void setDataFromSQLite() async{
    print("get data from sqlite");
    var _check = await _dbHelper.selectMS2PhotoHeader();
    var _check1 = await _dbHelper.selectMS2PhotoDetail();
    var _check2 = await _dbHelper.selectMS2PhotoDetail2();
    //insert header
    if(_check.isNotEmpty){
      for(int i=0; i<_listOccupation.length;i++){
        if(_listOccupation[i].KODE == _check[0]['work']){
          _occupationSelected = _listOccupation[i];
        }
      }
      for(int i=0; i<_listTypeOfFinancing.length; i++){
        if(_listTypeOfFinancing[i].financingTypeId == _check[0]['financing_type']){
          _typeOfFinancingModelSelected = _listTypeOfFinancing[i];
        }
      }
      for(int i=0; i<_listKegiatanUsaha.length; i++){
        if(_listKegiatanUsaha[i].id.toString() == _check[0]['business_activities']){
          _kegiatanUsahaSelected = _listKegiatanUsaha[i];
        }
      }
      for(int i=0; i<_listJenisKegiatanUsaha.length; i++){
        if(_listJenisKegiatanUsaha[i].id.toString() == _check[0]['business_activities_type']){
          _jenisKegiatanUsahaSelected = _listJenisKegiatanUsaha[i];
        }
      }
      for(int i=0; i<_listJenisKonsep.length; i++){
        if(_listJenisKonsep[i].id.toString() == _check[0]['concept_type']){
          _jenisKonsepSelected = _listJenisKonsep[i];
        }
      }
      // this._jenisKonsepSelected = JenisKonsepModel(_check[0]['concept_type'], _check[0]['concept_type_desc']);
      notifyListeners();
    }
    //insert foto tempat tinggal dan usaha
    if(_check2.isNotEmpty){
      for(int i=0; i<_check2.length; i++){
        if(_check2[i]['flag'] == "1"){
          File _imageFile = File(_check2[i]['path_photo']);
          _listFotoTempatTinggal.add(ImageFileModel(_imageFile, double.parse(_check2[i]['latitude']), double.parse(_check2[i]['longitude']), _check2[i]['path_photo']));
        }
        if(_check2[i]['flag'] == "2"){
          File _imageFile = File(_check2[i]['path_photo']);
          _listFotoTempatUsaha.add(ImageFileModel(_imageFile, double.parse(_check2[i]['latitude']), double.parse(_check2[i]['longitude']), _check2[i]['path_photo']));
        }
      }
    }
    //detail list
    if(_check1.isNotEmpty){
      for(int i=0; i<_check1.length; i++){
        //list group unit object
        if(_check1[i]['flag'] == "1"){
          var groupObjectSelected = GroupObjectUnit(_check1[i]['group_object'], _check1[i]['group_object_desc']);
          var unitObject = ObjectUnit(_check1[i]['unit_object'], _check1[i]['unit_object_desc']);
          List<ImageFileModel> listImageGroupObjectUnit = [];
          if(_check1[i]['unit_object'] == "011"){
            print("no foto");
            _listGroupUnitObject.add(GroupUnitObjectModel(groupObjectSelected, unitObject, null));
          }
          else {
            print("pake foto");
            for(int j=0; j<_check2.length; j++){
              if(_check1[i]['detail_id'] == _check2[j]['detail_id']){
                File _imageFile = File(_check2[j]['path_photo']);
                listImageGroupObjectUnit.add(ImageFileModel(_imageFile, double.parse(_check2[j]['latitude']), double.parse(_check2[j]['longitude']), _check2[j]['path_photo']));
              }
            }
            _listGroupUnitObject.add(GroupUnitObjectModel(groupObjectSelected, unitObject, listImageGroupObjectUnit));
          }
        }
        else {
          for(int j=0; j<_check2.length; j++){
            if(_check1[i]['detail_id'] == _check2[j]['detail_id']){
              if(_check2[j]['flag'] == "4"){
                File _imageFile = File(_check2[j]['path_photo']);
                List _splitFileName = _check2[j]['path_photo'].split("/");
                int _idFileName = _splitFileName.length - 1;
                var _fileDocument = {
                  "file":_imageFile,
                  "file_name": _splitFileName[_idFileName],
                };
                var jenisDokumen = JenisDocument(_check1[i]['document_type'], _check1[i]['document_type_desc']);
                var dateReceive = DateTime.parse(_check1[i]['date_receive']);
                _listDocument.add(DocumentUnitModel(
                    jenisDokumen,
                    dateReceive,
                    _fileDocument,
                    _pathFile,
                    double.parse(_check2[j]['latitude']),
                    double.parse(_check2[j]['longitude']))
                );
              }
            }
          }
        }
      }
    }
  }

  Future<bool> deleteSQLite(){
    return _dbHelper.deleteMS2PhotoHeader();
  }
}
