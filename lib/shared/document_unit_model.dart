class DocumentUnitModel{
  final JenisDocument jenisDocument;
  final DateTime dateTime;
  final Map fileDocumentUnitObject;
  final String path;
  final double latitude;
  final double longitude;

  DocumentUnitModel(this.jenisDocument, this.dateTime, this.fileDocumentUnitObject, this.path, this.latitude, this.longitude);
}

class JenisDocument{
  final String id;
  final String name;

  JenisDocument(this.id, this.name);
}