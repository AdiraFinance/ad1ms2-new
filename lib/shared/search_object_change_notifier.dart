import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/object_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchObjectChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<ObjectModel> _listObjectModel = [];
  List<ObjectModel> _listObjectTemp = [];

  void setDataListObject(String id) {
    if (id == "001") {
      _listObjectModel.add(ObjectModel("001", "MOTOR BARU"));
      _listObjectModel.add(ObjectModel("002", "MOTOR BEKAS"));
    } else if (id == "002") {
      _listObjectModel.add(ObjectModel("003", "MOBIL BARU"));
      _listObjectModel.add(ObjectModel("004", "MOBIL BEKAS"));
    } else if (id == "003") {
      _listObjectModel.add(ObjectModel("005", "DURABLE"));
      _listObjectModel.add(ObjectModel("018", "DURABLE GOODS"));
    } else if (id == "007") {
      _listObjectModel.add(ObjectModel("014", "JASA"));
    }
//    notifyListeners();
  }

  List<ObjectModel> get listObjectModel => _listObjectModel;

  UnmodifiableListView<ObjectModel> get listObjectTemp {
    return UnmodifiableListView(this._listObjectTemp);
  }

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getObject(BuildContext context) async{
    this._listObjectModel.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_KODE": "${Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE}"
    });

    try{
      final _response = await _http.post(
        // "${BaseUrl.unit}api/parameter/get-object",
          "${BaseUrl.urlPublic}api/parameter/get-object",
          body: _body,
          headers: {"Content-Type":"application/json"}
      );
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        List _data = _result['data'];
        if(_data.isEmpty){
          showSnackBar("Address not found");
          loadData = false;
        }
        else{
          for(int i=0; i <_data.length; i++){
            this._listObjectModel.add(
                ObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
            );
          }
          loadData = false;
        }
      }
      else{
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    }
    catch(e){
      showSnackBar("Error ${e.toString()}");
      this._loadData = false;
    }

    notifyListeners();
  }

  void searchObject(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listObjectTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listObjectModel.forEach((dataGroupObject) {
        if (dataGroupObject.id.contains(query) || dataGroupObject.name.contains(query)) {
          _listObjectTemp.add(dataGroupObject);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listObjectTemp.clear();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
