import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormMParentChangeNotifierOld with ChangeNotifier {
  List<GlobalKey<FormState>> formKeys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>()
  ];

  VoidCallback _onStepContinue;
  VoidCallback _onStepCancel;

  int _currentStep = 0;
  bool _flagStatusForm1 = false;
  bool _flagStatusForm2 = false;
  bool _flagStatusForm3 = false;
  bool _flagStatusForm4 = false;
  bool _flagStatusForm5 = false;
  bool _flagStatusForm6 = false;
  bool _flagStatusForm7 = false;
  bool _flagStatusForm8 = false;
  bool _enableTapForm2 = false;
  bool _enableTapForm3 = false;
  bool _enableTapForm4 = false;
  bool _enableTapForm5 = false;
  bool _enableTapForm6 = false;
  bool _enableTapForm7 = false;
  bool _enableTapForm8 = false;

  int get currentStep => _currentStep;

  set currentStep(int value) {
    this._currentStep = value;
    notifyListeners();
  }

  VoidCallback get onStepCancel => _onStepCancel;

  set onStepCancel(VoidCallback value) {
    this._onStepCancel = value;
//    notifyListeners();
  }

  VoidCallback get onStepContinue => _onStepContinue;

  set onStepContinue(VoidCallback value) {
    this._onStepContinue = value;
//    notifyListeners();
  }

  bool get flagStatusForm1 => _flagStatusForm1;

  set flagStatusForm1(bool value) {
    this._flagStatusForm1 = value;
    notifyListeners();
  }

  bool get flagStatusForm2 => _flagStatusForm2;

  set flagStatusForm2(bool value) {
    this._flagStatusForm2 = value;
    notifyListeners();
  }

  bool get enableTapForm2 => _enableTapForm2;

  set enableTapForm2(bool value) {
    this._enableTapForm2 = value;
    notifyListeners();
  }

  bool get enableTapForm3 => _enableTapForm3;

  set enableTapForm3(bool value) {
    this._enableTapForm3 = value;
    notifyListeners();
  }

  bool get flagStatusForm3 => _flagStatusForm3;

  set flagStatusForm3(bool value) {
    this._flagStatusForm3 = value;
    notifyListeners();
  }

  bool get flagStatusForm4 => _flagStatusForm4;

  set flagStatusForm4(bool value) {
    this._flagStatusForm4 = value;
    notifyListeners();
  }

  bool get enableTapForm4 => _enableTapForm4;

  set enableTapForm4(bool value) {
    this._enableTapForm4 = value;
    notifyListeners();
  }

  bool get enableTapForm5 => _enableTapForm5;

  set enableTapForm5(bool value) {
    this._enableTapForm5 = value;
    notifyListeners();
  }

  bool get flagStatusForm5 => _flagStatusForm5;

  set flagStatusForm5(bool value) {
    this._flagStatusForm5 = value;
    notifyListeners();
  }

  bool get flagStatusForm6 => _flagStatusForm6;

  set flagStatusForm6(bool value) {
    this._flagStatusForm6 = value;
    notifyListeners();
  }

  bool get enableTapForm6 => _enableTapForm6;

  set enableTapForm6(bool value) {
    this._enableTapForm6 = value;
    notifyListeners();
  }

  bool get enableTapForm7 => _enableTapForm7;

  set enableTapForm7(bool value) {
    this._enableTapForm7 = value;
    notifyListeners();
  }

  bool get flagStatusForm7 => _flagStatusForm7;

  set flagStatusForm7(bool value) {
    this._flagStatusForm7 = value;
    notifyListeners();
  }

  bool get enableTapForm8 => _enableTapForm8;

  set enableTapForm8(bool value) {
    this._enableTapForm8 = value;
    notifyListeners();
  }

  bool get flagStatusForm8 => _flagStatusForm8;

  set flagStatusForm8(bool value) {
    this._flagStatusForm8 = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final form = formKeys[this._currentStep].currentState;
    var _providerFormMFotoChangeNotif =
        Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerFormInfoNasabahChangeNotif =
        Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    if (this._currentStep == 0) {
      if (_providerFormMFotoChangeNotif.listFotoTempatTinggal.isEmpty) {
        _providerFormMFotoChangeNotif.autoValidateFotoTempatTinggal = true;
      }
      if (_providerFormMFotoChangeNotif.listGroupUnitObject.isEmpty) {
        _providerFormMFotoChangeNotif.autoValidateGroupObjectUnit = true;
      }
      if (!form.validate()) {
        _providerFormMFotoChangeNotif.autoValidate = true;
      }
      if (_providerFormMFotoChangeNotif.listDocument.isEmpty) {
        _providerFormMFotoChangeNotif.autoValidateDocumentObjectUnit = true;
      }
      if (_providerFormMFotoChangeNotif.occupationSelected != null) {
        if (_providerFormMFotoChangeNotif.occupationSelected.KODE == "OC005" ||
            _providerFormMFotoChangeNotif.occupationSelected.KODE == "OC006" ||
            _providerFormMFotoChangeNotif.occupationSelected.KODE == "OC007") {
          if (_providerFormMFotoChangeNotif.listFotoTempatUsaha.isEmpty) {
            _providerFormMFotoChangeNotif.autoValidateTempatUsaha = true;
          }
        }
      }
      if (form.validate() &&
          _providerFormMFotoChangeNotif.listFotoTempatTinggal.isNotEmpty &&
          _providerFormMFotoChangeNotif.listGroupUnitObject.isNotEmpty &&
          _providerFormMFotoChangeNotif.listDocument.isNotEmpty) {
        if (_providerFormMFotoChangeNotif.occupationSelected.KODE == "OC005" ||
            _providerFormMFotoChangeNotif.occupationSelected.KODE == "OC006" ||
            _providerFormMFotoChangeNotif.occupationSelected.KODE == "OC007") {
          if (_providerFormMFotoChangeNotif.listFotoTempatUsaha.isNotEmpty) {
            this._onStepContinue();
            enableTapForm2 = true;
            flagStatusForm1 = true;
          }
        } else {
          this._onStepContinue();
          enableTapForm2 = true;
          flagStatusForm1 = true;
        }
      }
    } else if (this._currentStep == 1) {
      if (form.validate()) {
        enableTapForm3 = true;
        flagStatusForm2 = true;
        this._onStepContinue();
      } else {
        _providerFormInfoNasabahChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 2) {
      if (form.validate()) {
        enableTapForm4 = true;
        flagStatusForm3 = true;
        this._onStepContinue();
      } else {
        Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
            .autoValidate = true;
      }
    } else if (this._currentStep == 3) {
      if (form.validate()) {
        enableTapForm5 = true;
        flagStatusForm4 = true;
        this._onStepContinue();
      } else {
        Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context,
                listen: false)
            .autoValidate = true;
      }
    } else if (this._currentStep == 4) {
      var _provider =
          Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
      if (_provider.listFormInfoKel.isNotEmpty) {
        enableTapForm6 = true;
        flagStatusForm5 = true;
        this._onStepContinue();
      } else {
        _provider.autoValidate = true;
      }
    } else if (this._currentStep == 5) {
      var _provider =
          Provider.of<FormMOccupationChangeNotif>(context, listen: false);
      if (form.validate()) {
        enableTapForm7 = true;
        flagStatusForm6 = true;
        this._onStepContinue();
      } else {
        _provider.autoValidate = true;
      }
    } else if (this._currentStep == 6) {
      var _provider =
          Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
      if (form.validate()) {
        enableTapForm8 = true;
        flagStatusForm7 = true;
        this._onStepContinue();
      } else {
        _provider.autoValidate = true;
      }
    }
  }

  void onTapCheck(int step, BuildContext context) {
    final form = formKeys[this._currentStep].currentState;
    var _provider =
        Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    if (this._currentStep == 0) {
      if (_provider.listFotoTempatTinggal.isEmpty) {
        _provider.autoValidateFotoTempatTinggal = true;
      }
      if (_provider.listGroupUnitObject.isEmpty) {
        _provider.autoValidateGroupObjectUnit = true;
      }
      if (!form.validate()) {
        _provider.autoValidate = true;
      }
      if (_provider.listDocument.isEmpty) {
        _provider.autoValidateDocumentObjectUnit = true;
      }
      if (_provider.occupationSelected != null) {
        if (_provider.occupationSelected.KODE == "OC004" ||
            _provider.occupationSelected.KODE == "OC005") {
          if (_provider.listFotoTempatUsaha.isEmpty) {
            _provider.autoValidateTempatUsaha = true;
          }
        }
      }
      if (form.validate() &&
          _provider.listFotoTempatTinggal.isNotEmpty &&
          _provider.listGroupUnitObject.isNotEmpty &&
          _provider.listDocument.isNotEmpty) {
        if (_provider.occupationSelected.KODE == "OC004" ||
            _provider.occupationSelected.KODE == "OC005") {
          if (_provider.listFotoTempatUsaha.isNotEmpty) {
            currentStep = step;
          }
        } else {
          currentStep = step;
        }
      }
    } else if (this._currentStep == 1) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep == 2) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep == 3) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    }
  }
}
