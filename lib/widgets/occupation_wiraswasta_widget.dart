import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OccupationWiraswastaWidget extends StatefulWidget {
  @override
  _OccupationWiraswastaWidgetState createState() =>
      _OccupationWiraswastaWidgetState();
}

class _OccupationWiraswastaWidgetState
    extends State<OccupationWiraswastaWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMOccupationChangeNotif>(
      builder: (context, formMOccupationChangeNotif, _) {
        return Column(
          children: [
//            TextFormField(
//              autovalidate: formMOccupationChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
//              controller: formMOccupationChangeNotif.controllerBusinessName,
//              style: new TextStyle(color: Colors.black),
//              decoration: new InputDecoration(
//                  labelText: 'Nama Usaha',
//                  labelStyle: TextStyle(color: Colors.black),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(8))),
//              keyboardType: TextInputType.text,
//              textCapitalization: TextCapitalization.characters,
//            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            DropdownButtonFormField<TypeOfBusinessModel>(
//                autovalidate: formMOccupationChangeNotif.autoValidate,
//                validator: (e) {
//                  if (e == null) {
//                    return "Silahkan pilih jenis badan usaha";
//                  } else {
//                    return null;
//                  }
//                },
//                value: formMOccupationChangeNotif.typeOfBusinessModelSelected,
//                onChanged: (value) {
//                  formMOccupationChangeNotif.typeOfBusinessModelSelected =
//                      value;
//                },
//                onTap: () {
//                  FocusManager.instance.primaryFocus.unfocus();
//                },
//                decoration: InputDecoration(
//                  labelText: "Jenis Badan Usaha",
//                  border: OutlineInputBorder(),
//                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                ),
//                items:
//                    formMOccupationChangeNotif.listTypeOfBusiness.map((value) {
//                  return DropdownMenuItem<TypeOfBusinessModel>(
//                    value: value,
//                    child: Text(
//                      value.text,
//                      overflow: TextOverflow.ellipsis,
//                    ),
//                  );
//                }).toList()),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              autovalidate: formMOccupationChangeNotif.autoValidate,
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              readOnly: true,
              onTap: () {
                FocusManager.instance.primaryFocus.unfocus();
                formMOccupationChangeNotif.searchSectorEconomic(context);
              },
              controller:
              formMOccupationChangeNotif.controllerSectorEconomic,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Sektor Ekonomi',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            TextFormField(
//              autovalidate: formMOccupationChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
//              readOnly: true,
//              onTap: () {
//                FocusManager.instance.primaryFocus.unfocus();
//                formMOccupationChangeNotif.searchBusinesField(context);
//              },
//              controller:
//              formMOccupationChangeNotif.controllerBusinessField,
//              style: new TextStyle(color: Colors.black),
//              decoration: new InputDecoration(
//                  labelText: 'Lapangan Usaha',
//                  labelStyle: TextStyle(color: Colors.black),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(8))),
//              enabled: formMOccupationChangeNotif.controllerSectorEconomic.text == "" ? false : true,
//            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            DropdownButtonFormField<StatusLocationModel>(
//                autovalidate: formMOccupationChangeNotif.autoValidate,
//                validator: (e) {
//                  if (e == null) {
//                    return "Silahkan pilih status lokasi";
//                  } else {
//                    return null;
//                  }
//                },
//                value: formMOccupationChangeNotif.statusLocationModelSelected,
//                onChanged: (value) {
//                  formMOccupationChangeNotif.statusLocationModelSelected =
//                      value;
//                },
//                onTap: () {
//                  FocusManager.instance.primaryFocus.unfocus();
//                },
//                decoration: InputDecoration(
//                  labelText: "Status Lokasi",
//                  border: OutlineInputBorder(),
//                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                ),
//                items:
//                    formMOccupationChangeNotif.listStatusLocation.map((value) {
//                  return DropdownMenuItem<StatusLocationModel>(
//                    value: value,
//                    child: Text(
//                      value.desc,
//                      overflow: TextOverflow.ellipsis,
//                    ),
//                  );
//                }).toList()),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            DropdownButtonFormField<BusinessLocationModel>(
//                autovalidate: formMOccupationChangeNotif.autoValidate,
//                validator: (e) {
//                  if (e == null) {
//                    return "Silahkan pilih lokasi usaha";
//                  } else {
//                    return null;
//                  }
//                },
//                value: formMOccupationChangeNotif.businessLocationModelSelected,
//                onChanged: (value) {
//                  formMOccupationChangeNotif.businessLocationModelSelected =
//                      value;
//                },
//                onTap: () {
//                  FocusManager.instance.primaryFocus.unfocus();
//                },
//                decoration: InputDecoration(
//                  labelText: "Lokasi Usaha",
//                  border: OutlineInputBorder(),
//                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                ),
//                items: formMOccupationChangeNotif.listBusinessLocation
//                    .map((value) {
//                  return DropdownMenuItem<BusinessLocationModel>(
//                    value: value,
//                    child: Text(
//                      value.desc,
//                      overflow: TextOverflow.ellipsis,
//                    ),
//                  );
//                }).toList()),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            TextFormField(
//              autovalidate: formMOccupationChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
//              controller: formMOccupationChangeNotif.controllerEmployeeTotal,
//              style: new TextStyle(color: Colors.black),
//              decoration: new InputDecoration(
//                  labelText: 'Total Pegawai',
//                  labelStyle: TextStyle(color: Colors.black),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(8))),
//              keyboardType: TextInputType.number,
//            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              autovalidate: formMOccupationChangeNotif.autoValidate,
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              controller:
                  formMOccupationChangeNotif.controllerTotalEstablishedDate,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Total Lama Usaha/Kerja (bln)',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
              keyboardType: TextInputType.number,
            ),
          ],
        );
      },
    );
  }
}
