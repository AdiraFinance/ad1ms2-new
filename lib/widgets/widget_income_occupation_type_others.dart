import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WidgetIncomeOccupationTypeOther extends StatefulWidget {
  final String maritalStatusKode;

  const WidgetIncomeOccupationTypeOther({Key key, this.maritalStatusKode}) : super(key: key);
  @override
  _WidgetIncomeOccupationTypeOtherState createState() =>
      _WidgetIncomeOccupationTypeOtherState();
}

class _WidgetIncomeOccupationTypeOtherState extends State<WidgetIncomeOccupationTypeOther> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<FormMIncomeChangeNotifier>(context, listen: false).setIsMarried(context);
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMIncomeChangeNotifier>(
      builder: (context, formMIncomeChangeNotif, _) {
        return SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width/37,
            vertical: MediaQuery.of(context).size.height/57,
          ),
          child: Column(
            children: [
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerIncome,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Pendapatan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                  formMIncomeChangeNotif.controllerIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                },
                onTap: () {
                  formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                  formMIncomeChangeNotif.formattingProfessionalOther();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              formMIncomeChangeNotif.isMarried == true
              ? SizedBox(height: MediaQuery.of(context).size.height / 47)
              : SizedBox(),
              Visibility(
                visible: formMIncomeChangeNotif.isMarried,
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerSpouseIncome,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Pendapatan Pasangan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.controllerSpouseIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  },
                  onTap: () {
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.formattingProfessionalOther();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerOtherIncome,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Pendapatan Lainnya',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                  formMIncomeChangeNotif.controllerOtherIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                },
                onTap: () {
                  formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                  formMIncomeChangeNotif.formattingProfessionalOther();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                enabled: false,
                autovalidate: formMIncomeChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
                controller: formMIncomeChangeNotif.controllerTotalIncome,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Total Pendapatan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                textAlign: TextAlign.end,
                keyboardType: TextInputType.number,
                textCapitalization: TextCapitalization.characters,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerCostOfLiving,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Biaya Hidup',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  formMIncomeChangeNotif.controllerCostOfLiving.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                },
                onTap: () {
                  formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  formMIncomeChangeNotif.formattingProfessionalOther();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                enabled: false,
                autovalidate: formMIncomeChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
                controller: formMIncomeChangeNotif.controllerRestIncome,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Sisa Pendapatan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                textAlign: TextAlign.end,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerOtherInstallments,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Angsuran Lainnya',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerOtherInstallments.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingProfessionalOther();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
            ],
          ),
        );
      },
    );
  }
}
