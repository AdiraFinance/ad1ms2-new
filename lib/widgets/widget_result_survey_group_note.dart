import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/recommendation_surveyor_model.dart';
import 'package:ad1ms2_dev/models/result_survey_model.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WidgetResultGroupSurveyNotes extends StatefulWidget {
  @override
  _WidgetResultGroupSurveyNotesState createState() => _WidgetResultGroupSurveyNotesState();
}

class _WidgetResultGroupSurveyNotesState extends State<WidgetResultGroupSurveyNotes> {

  @override
  void initState() {
    super.initState();
    if( Provider.of<ResultSurveyChangeNotifier>(context,listen: false).listRecomSurvey.isEmpty
        &&  Provider.of<ResultSurveyChangeNotifier>(context,listen: false).listResultSurvey.isEmpty){
      Provider.of<ResultSurveyChangeNotifier>(context,listen: false).getRecomSurvey();
      Provider.of<ResultSurveyChangeNotifier>(context,listen: false).getStatusSurvey();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<ResultSurveyChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Text(
              "Hasil Survey-Group Catatan",
              style: TextStyle(color: Colors.black)
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width / 27,
              vertical: MediaQuery.of(context).size.height / 57),
          child: Consumer<ResultSurveyChangeNotifier>(
            builder: (context, value, _) {
              return Form(
                onWillPop: value.onBackPressResultSurveyGroupNotes,
                key: value.keyResultSurveyGroupNotes,
                child: Column(
                  children: [
                    DropdownButtonFormField<ResultSurveyModel>(
                        autovalidate: value.autoValidateResultSurveyGroupNote,
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih jenis identitas";
                          } else {
                            return null;
                          }
                        },
                        value: value.resultSurveySelected,
                        onChanged: (data) {
                          value.resultSurveySelected = data;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Hasil Survey",
                          border: OutlineInputBorder(),
                          contentPadding:
                          EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: value
                            .listResultSurvey
                            .map((value) {
                          return DropdownMenuItem<ResultSurveyModel>(
                            value: value,
                            child: Text(
                              value.DESKRIPSI,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList()
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    DropdownButtonFormField<RecommendationSurveyorModel>(
                        autovalidate: value.autoValidateResultSurveyGroupNote,
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih jenis identitas";
                          } else {
                            return null;
                          }
                        },
                        value: value.recommendationSurveySelected,
                        onChanged: (data) {
                          value.recommendationSurveySelected = data;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Rekomendasi Survey",
                          border: OutlineInputBorder(),
                          contentPadding:
                          EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: value
                            .listRecomSurvey
                            .map((value) {
                          return DropdownMenuItem<RecommendationSurveyorModel>(
                            value: value,
                            child: Text(
                              value.DESKRIPSI,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList()
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        autovalidate: value.autoValidateResultSurveyGroupNote,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: value.controllerNote,
                        decoration: new InputDecoration(
                            labelText: 'Catatan',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        )
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        autovalidate: value.autoValidateResultSurveyGroupNote,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        onTap: (){
                          value.selectDateResultSurvey(context);
                        },
                        readOnly: true,
                        controller: value.controllerResultSurveyDate,
                        decoration: new InputDecoration(
                            labelText: 'Tanggal Hasil Survey',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        )
                    ),
                  ],
                ),
              );
            },
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Consumer<ResultSurveyChangeNotifier>(
                builder: (context, resultSurveyChangeNotifier, _) {
                  return RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                        resultSurveyChangeNotifier.checkResultSurveyGroupNotes(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("DONE",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25))
                        ],
                      ));
                },
              )),
        ),
      ),
    );
  }
}
