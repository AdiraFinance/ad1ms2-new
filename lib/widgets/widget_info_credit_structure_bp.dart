import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetInfoCreditStructureBP extends StatefulWidget {
  @override
  _WidgetInfoCreditStructureBPState createState() => _WidgetInfoCreditStructureBPState();
}

class _WidgetInfoCreditStructureBPState extends State<WidgetInfoCreditStructureBP> {

  var _tenor;

  @override
  void initState() {
    super.initState();
    _tenor = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected;
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor
      ),
      child: Scaffold(
        appBar: AppBar(
            title:
            Text("Struktur Kredit BP", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
          builder: (context, value, child) {
            return Container(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 47,
                  horizontal: MediaQuery.of(context).size.width / 27),
              child: Column(
                children: [
                  Row(
                    children: [
                      Row(
                        children: [
                          Radio(
                              value: 0,
                              activeColor: primaryOrange,
                              groupValue: value.radioValueBalloonPHOrBalloonInstallment,
                              onChanged: (data){
                                value.radioValueBalloonPHOrBalloonInstallment = data;
                              }
                          ),
                          SizedBox(width: 8),
                          Text("Balloon PH")
                        ],
                      ),
                      Row(
                        children: [
                          Radio(
                              value: 1,
                              activeColor: primaryOrange,
                              groupValue: value.radioValueBalloonPHOrBalloonInstallment,
                              onChanged: (data){
                                value.radioValueBalloonPHOrBalloonInstallment = data;
                              }
                          ),
                          SizedBox(width: 8),
                          Text("Balloon Angsuran")
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Visibility(
                    visible: value.visibleTfPercentageInstallment,
                    child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        keyboardType: TextInputType.number,
                        controller: value.controllerInstallmentPercentage,
                        autovalidate: value.autoValidateBP,
                        decoration: new InputDecoration(
                            labelText: 'Presentase Angsuran Ke - $_tenor',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)
                            )
                        )
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      keyboardType: TextInputType.number,
                      controller: value.controllerInstallmentValue,
                      autovalidate: value.autoValidateBP,
                      decoration: new InputDecoration(
                          labelText: 'Nilai Angsuran Ke - $_tenor',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)
                          )
                      )
                  )
                ],
              ),
            );
          },
        ),
        bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
                      builder: (context, infoCreditStructureTypeInstallmentChangeNotifier, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                  Navigator.pop(context);
//                        infoCreditStructureTypeInstallmentChangeNotifier.check(context);
                              },
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                      Text("DONE",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: 1.25))
                                  ],
                              ));
                      },
                  )),
          ),
      ),
    );
  }
}
