import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetInfoCollateralAutomotive extends StatelessWidget {
  final String objectSelected;
  final String flag;

  const WidgetInfoCollateralAutomotive({this.objectSelected, this.flag});
  @override
  Widget build(BuildContext context) {
    return Consumer<InformationCollateralChangeNotifier>(
      builder: (context,infoCollateralChangeNotifier,_){
        return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Text(
                    "Nama Jaminan = Pemohon",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                    ),
                ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                Row(
                    children: [
                        Row(
                            children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 0,
                                    groupValue: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto,
                                    onChanged: (value){
                                        infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto = value;
                                        infoCollateralChangeNotifier.sameWithApplicantAutomotive(context);
                                    }
                                ),
                                Text("Ya")
                            ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width / 27),
                        Row(
                            children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 1,
                                    groupValue: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto,
                                    onChanged: (value){
                                        infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto = value;
                                        infoCollateralChangeNotifier.sameWithApplicantAutomotive(context);
                                    }
                                ),
                                Text("Tidak")
                            ],
                        ),
                    ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                  child: DropdownButtonFormField<IdentityModel>(
                      value: infoCollateralChangeNotifier.identityTypeSelectedAuto,
                      // autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      // validator: (e) {
                      //     if (e == null) {
                      //         return "Tidak boleh kosong";
                      //     } else {
                      //         return null;
                      //     }
                      // },
                      decoration: InputDecoration(
                          labelText: "Jenis Identitas",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: infoCollateralChangeNotifier.listIdentityType.map((data) {
                          return DropdownMenuItem<IdentityModel>(
                              value: data,
                              child: Text(
                                  data.name,
                                  overflow: TextOverflow.ellipsis,
                              ),
                          );
                      }).toList(),
                      onChanged: (newVal) {
                          infoCollateralChangeNotifier.identityTypeSelectedAuto = newVal;
                      },
                      onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                      },
                  ),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                  child: TextFormField(
                      readOnly: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? true:false,
                      // autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      // validator: (e) {
                      //     if (e.isEmpty) {
                      //         return "Tidak boleh kosong";
                      //     } else {
                      //         return null;
                      //     }
                      // },
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                      ],
                      controller: infoCollateralChangeNotifier.controllerIdentityNumberAuto,
                      decoration: new InputDecoration(
                          labelText: 'No Identitas',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))
                      )
                  ),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                  child: TextFormField(
                      readOnly: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? true:false,
                      validator: (e) {
                          if (e.isEmpty) {
                              return "Tidak boleh kosong";
                          } else {
                              return null;
                          }
                      },
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      controller: infoCollateralChangeNotifier.controllerNameOnCollateralAuto,
                      autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      decoration: new InputDecoration(
                          labelText: 'Nama Pada Jaminan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))
                      )
                  ),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                  child: TextFormField(
                      onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                          infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ?
                          null : infoCollateralChangeNotifier.selectBirthDate(context,0);
                      },
                      controller: infoCollateralChangeNotifier.controllerBirthDateAuto,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Tanggal Lahir',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      // autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      // validator: (e) {
                      //     if (e.isEmpty) {
                      //         return "Tidak boleh kosong";
                      //     } else {
                      //         return null;
                      //     }
                      // },
                      readOnly: true,
                  ),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                  child: TextFormField(
                      readOnly: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? true:false,
                      // autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      // validator: (e) {
                      //     if (e.isEmpty) {
                      //         return "Tidak boleh kosong";
                      //     } else {
                      //         return null;
                      //     }
                      // },
                      // readOnly: true,
                      textCapitalization: TextCapitalization.characters,
                      controller: infoCollateralChangeNotifier.controllerBirthPlaceValidWithIdentityAuto,
                      decoration: new InputDecoration(
                          labelText: 'Tempat Lahir Sesuai Identitas',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)))),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                  child: TextFormField(
                      validator: (e) {
                          if (e.isEmpty) {
                              return "Tidak boleh kosong";
                          } else {
                              return null;
                          }
                      },
                      readOnly: true,
                      onTap: (){
                          infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0
                          ? null
                          : infoCollateralChangeNotifier.searchBirthPlace(context,0);
                      },
                      controller: infoCollateralChangeNotifier.controllerBirthPlaceValidWithIdentityLOVAuto,
                      autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      decoration: new InputDecoration(
                          labelText: 'Tempat Lahir Sesuai Identitas',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)))),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Text(
                    "Collateral = Unit",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                    ),
                ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                Row(
                    children: [
                        Row(
                            children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 0,
                                    groupValue: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto,
                                    onChanged: (value){
                                        infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto = value;
                                        infoCollateralChangeNotifier.setSameWithApplicantAutomotive(context);
                                    }
                                ),
                                Text("Ya")
                            ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width / 27),
                        Row(
                            children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 1,
                                    groupValue: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto,
                                    onChanged: (value){
                                        infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto = value;
                                        infoCollateralChangeNotifier.setSameWithApplicantAutomotive(context);
                                    }
                                ),
                                Text("Tidak")
                            ],
                        )
                    ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                // DropdownButtonFormField<IdentityModel>(
                //     autovalidate:
                //     infoCollateralChangeNotifier.autoValidateAuto,
                //     validator: (e) {
                //         if (e == null) {
                //             return "Silahkan pilih jenis identitas";
                //         } else {
                //             return null;
                //         }
                //     },
                //     value: infoCollateralChangeNotifier
                //         .identityTypeSelected,
                //     onChanged: (value) {
                //         infoCollateralChangeNotifier
                //             .identityTypeSelected = value;
                //     },
                //     onTap: () {
                //         FocusManager.instance.primaryFocus.unfocus();
                //     },
                //     decoration: InputDecoration(
                //         labelText: "Jenis Identitas",
                //         border: OutlineInputBorder(),
                //         contentPadding:
                //         EdgeInsets.symmetric(horizontal: 10),
                //     ),
                //     items: infoCollateralChangeNotifier
                //         .listIdentityType
                //         .map((value) {
                //         return DropdownMenuItem<IdentityModel>(
                //             value: value,
                //             child: Text(
                //                 value.name,
                //                 overflow: TextOverflow.ellipsis,
                //             ),
                //         );
                //     }).toList()),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                  child: TextFormField(
                      validator: (e) {
                          if (e.isEmpty) {
                              return "Tidak boleh kosong";
                          } else {
                              return null;
                          }
                      },
                      readOnly: true,
                      onTap: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto != 0
                          ?
                          () {infoCollateralChangeNotifier.searchGroupObject(context, flag);}
                          : null,
                      controller: infoCollateralChangeNotifier.controllerGroupObject,
                      autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      decoration: new InputDecoration(
                          labelText: 'Grup Objek',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)))),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                  child: TextFormField(
                      validator: (e) {
                          if (e.isEmpty) {
                              return "Tidak boleh kosong";
                          } else {
                              return null;
                          }
                      },
                      readOnly: true,
                      onTap: infoCollateralChangeNotifier.controllerGroupObject.text != ""
                          ? () {
                          infoCollateralChangeNotifier.searchObject(context);
                      }
                          : null,
                      controller: infoCollateralChangeNotifier.controllerObject,
                      autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      decoration: new InputDecoration(
                          labelText: 'Objek',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)))),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                  child: TextFormField(
                      validator: (e) {
                          if (e.isEmpty) {
                              return "Tidak boleh kosong";
                          } else {
                              return null;
                          }
                      },
                      readOnly: true,
                      onTap: infoCollateralChangeNotifier.controllerGroupObject.text != ""
                          ? (){infoCollateralChangeNotifier.searchProductType(context,flag);}
                          : null,
                      controller: infoCollateralChangeNotifier.controllerTypeProduct,
                      autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      decoration: new InputDecoration(
                          labelText: 'Jenis Produk',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)))),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                  child: TextFormField(
                      validator: (e) {
                          if (e.isEmpty) {
                              return "Tidak boleh kosong";
                          } else {
                              return null;
                          }
                      },
                      readOnly: true,
                      onTap: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto != 0 && infoCollateralChangeNotifier.controllerTypeProduct.text != ""
                          ?
                          () {infoCollateralChangeNotifier.searchBrandObject(context,flag,1);}
                          : null,
                      controller: infoCollateralChangeNotifier.controllerBrandObject,
                      autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      decoration: new InputDecoration(
                          labelText: 'Merk Objek',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)))),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                  child: TextFormField(
                      validator: (e) {
                          if (e.isEmpty) {
                              return "Tidak boleh kosong";
                          } else {
                              return null;
                          }
                      },
                      readOnly: true,
                      onTap: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto != 0 && infoCollateralChangeNotifier.controllerTypeProduct.text != ""
                          ?
                          () {infoCollateralChangeNotifier.searchObjectType(context,flag,2);}
                          : null,
                      controller: infoCollateralChangeNotifier.controllerObjectType,
                      autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      decoration: new InputDecoration(
                          labelText: 'Jenis Objek',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)))),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                  child: TextFormField(
                      validator: (e) {
                          if (e.isEmpty) {
                              return "Tidak boleh kosong";
                          } else {
                              return null;
                          }
                      },
                      readOnly: true,
                      onTap: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto != 0 && infoCollateralChangeNotifier.controllerTypeProduct.text != ""
                          ?
                          () {infoCollateralChangeNotifier.searchModelObject(context,flag,3);}
                          : null,
                      controller: infoCollateralChangeNotifier.controllerModelObject,
                      autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                      decoration: new InputDecoration(
                          labelText: 'Model Objek',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)))),
                ),
                Visibility(
                    visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                // Visibility(
                //   visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                //   child: TextFormField(
                //       validator: (e) {
                //           if (e.isEmpty) {
                //               return "Tidak boleh kosong";
                //           } else {
                //               return null;
                //           }
                //       },
                //       readOnly: true,
                //       onTap: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto != 0 && infoCollateralChangeNotifier.controllerTypeProduct.text != ""
                //           ?
                //           () {infoCollateralChangeNotifier.searchObjectUsage(context);}
                //           : null,
                //       controller: infoCollateralChangeNotifier.controllerUsageObjectModel,
                //       autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                //       decoration: InputDecoration(
                //           labelText: 'Pemakaian Objek',
                //           labelStyle: TextStyle(color: Colors.black),
                //           border: OutlineInputBorder(
                //               borderRadius: BorderRadius.circular(8))
                //       )
                //   ),
                // ),
                // Visibility(
                //     visible: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false:true,
                //     child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                // ),
                DropdownButtonFormField<String>(
                    autovalidate:
                    infoCollateralChangeNotifier.autoValidateAuto,
                    validator: (e) {
                        if (e == null) {
                            return "Silahkan pilih jenis identitas";
                        } else {
                            return null;
                        }
                    },
                    value: infoCollateralChangeNotifier
                        .yearProductionSelected,
                    onChanged: (value) {
                        infoCollateralChangeNotifier
                            .yearProductionSelected = value;
                    },
                    onTap: () {
                        FocusManager.instance.primaryFocus.unfocus();
                    },
                    decoration: InputDecoration(
                        labelText: "Tahun Pembuatan",
                        border: OutlineInputBorder(),
                        contentPadding:
                        EdgeInsets.symmetric(horizontal: 10),
                    ),
                    items: infoCollateralChangeNotifier
                        .listProductionYear
                        .map((value) {
                        return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                                value,
                                overflow: TextOverflow.ellipsis,
                            ),
                        );
                    }).toList()),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                DropdownButtonFormField<String>(
                    autovalidate:
                    infoCollateralChangeNotifier.autoValidateAuto,
                    validator: (e) {
                        if (e == null) {
                            return "Silahkan pilih jenis identitas";
                        } else {
                            return null;
                        }
                    },
                    value: infoCollateralChangeNotifier
                        .yearRegistrationSelected,
                    onChanged: (value) {
                        infoCollateralChangeNotifier
                            .yearRegistrationSelected = value;
                    },
                    onTap: () {
                        FocusManager.instance.primaryFocus.unfocus();
                    },
                    decoration: InputDecoration(
                        labelText: "Tahun Registrasi",
                        border: OutlineInputBorder(),
                        contentPadding:
                        EdgeInsets.symmetric(horizontal: 10),
                    ),
                    items: infoCollateralChangeNotifier
                        .listRegistrationYear
                        .map((value) {
                        return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                                value,
                                overflow: TextOverflow.ellipsis,
                            ),
                        );
                    }).toList()),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Text(
                    "Plat Kuning",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                    ),
                ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                Row(
                    children: [
                        Row(
                            children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 0,
                                    groupValue: infoCollateralChangeNotifier.radioValueYellowPlat,
                                    onChanged: (value){
                                        infoCollateralChangeNotifier.radioValueYellowPlat = value;
                                    }
                                ),
                                Text("Ya")
                            ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width / 27),
                        Row(
                            children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 1,
                                    groupValue: infoCollateralChangeNotifier.radioValueYellowPlat,
                                    onChanged: (value){
                                        infoCollateralChangeNotifier.radioValueYellowPlat = value;
                                    }
                                ),
                                Text("Tidak")
                            ],
                        ),
                    ],
                ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                // Text(
                //     "Built Up Non ATPM",
                //     style: TextStyle(
                //         fontSize: 16,
                //         fontWeight: FontWeight.bold
                //     ),
                // ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
//                 Row(
//                     children: [
//                         Row(
//                             children: [
//                                 Radio(
//                                     activeColor: primaryOrange,
//                                     value: 0,
//                                     groupValue: infoCollateralChangeNotifier.radioValueBuiltUpNonATPM,
//                                     onChanged: (value){
//                                         infoCollateralChangeNotifier.radioValueBuiltUpNonATPM = value;
//                                     }
//                                 ),
//                                 Text("Ya")
//                             ],
//                         ),
//                         SizedBox(height: MediaQuery.of(context).size.width / 27),
//                         Row(
//                             children: [
//                                 Radio(
//                                     activeColor: primaryOrange,
//                                     value: 1,
//                                     groupValue: infoCollateralChangeNotifier.radioValueBuiltUpNonATPM,
//                                     onChanged: (value){
//                                         infoCollateralChangeNotifier.radioValueBuiltUpNonATPM = value;
//                                     }
//                                 ),
//                                 Text("Tidak")
//                             ],
//                         ),
//                     ],
//                 ),
                objectSelected == "001" || objectSelected == "003"
                    ?
                SizedBox(height: MediaQuery.of(context).size.height / 47)
                    :
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Column(
                    children: [
                        TextFormField(
                            // validator: (e) {
                            //   if (e.isEmpty) {
                            //     return "Tidak boleh kosong";
                            //   } else {
                            //     return null;
                            //   }
                            // },
                            // autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            controller: infoCollateralChangeNotifier.controllerPoliceNumber,
                            decoration: new InputDecoration(
                                labelText: 'No Polisi',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))
                            ),
                            textCapitalization: TextCapitalization.characters,
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width / 27),
                        TextFormField(
                            // validator: (e) {
                            //   if (e.isEmpty) {
                            //     return "Tidak boleh kosong";
                            //   } else {
                            //     return null;
                            //   }
                            // },
                            // autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            controller: infoCollateralChangeNotifier.controllerFrameNumber,
                            decoration: new InputDecoration(
                                labelText: 'No Rangka',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))
                            ),
                            textCapitalization: TextCapitalization.characters,
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width / 27),
                        TextFormField(
                            // validator: (e) {
                            //   if (e.isEmpty) {
                            //     return "Tidak boleh kosong";
                            //   } else {
                            //     return null;
                            //   }
                            // },
                            // autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            controller: infoCollateralChangeNotifier.controllerMachineNumber,
                            decoration: new InputDecoration(
                                labelText: 'No Mesin',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))
                            ),
                          textCapitalization: TextCapitalization.characters,
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width / 27),
//                         TextFormField(
// //                            validator: (e) {
// //                              if (e.isEmpty) {
// //                                return "Tidak boleh kosong";
// //                              } else {
// //                                return null;
// //                              }
// //                            },
// //                            controller: infoCollateralChangeNotifier.controllerBPKPNumber,
//                             autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
//                             decoration: new InputDecoration(
//                                 labelText: 'No BPKB',
//                                 labelStyle: TextStyle(color: Colors.black),
//                                 border: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(8))
//                             )
//                         ),
//                         SizedBox(height: MediaQuery.of(context).size.width / 27),
                    ],
                ),
                TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    controller: infoCollateralChangeNotifier.controllerGradeUnit,
                    decoration: new InputDecoration(
                        labelText: 'Grade Unit',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    controller: infoCollateralChangeNotifier.controllerFasilitasUTJ,
                    decoration: new InputDecoration(
                        labelText: 'Fasilitas UTJ',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    controller: infoCollateralChangeNotifier.controllerNamaBidder,
                    decoration: new InputDecoration(
                        labelText: 'Nama Bidder/harga jual/harga pasar',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.end,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2),
                      infoCollateralChangeNotifier.amountValidator
                    ],
                    onFieldSubmitted: (value){
                      infoCollateralChangeNotifier.controllerHargaJualShowroom.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                    },
                    controller: infoCollateralChangeNotifier.controllerHargaJualShowroom,
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    decoration: new InputDecoration(
                        labelText: 'Harga Jual Showroom / Penjual',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    onTap: () {
                      infoCollateralChangeNotifier.formatting();
                    },
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    controller: infoCollateralChangeNotifier.controllerPerlengkapanTambahan,
                    decoration: new InputDecoration(
                        labelText: 'Perlengkapan Tambahan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    onTap: () {
                      infoCollateralChangeNotifier.formatting();
                    },
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    controller: infoCollateralChangeNotifier.controllerMPAdira,
                    decoration: new InputDecoration(
                        labelText: 'MP Adira',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    onTap: () {
                      infoCollateralChangeNotifier.formatting();
                    },
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.end,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2),
                      infoCollateralChangeNotifier.amountValidator
                    ],
                    onFieldSubmitted: (value){
                      infoCollateralChangeNotifier.controllerHargaPasar.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                    },
                    controller: infoCollateralChangeNotifier.controllerHargaPasar,
                    decoration: new InputDecoration(
                        labelText: 'Harga Pasar',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    onTap: () {
                      infoCollateralChangeNotifier.formatting();
                    },
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    controller: infoCollateralChangeNotifier.controllerRekondisiFisik,
                    decoration: new InputDecoration(
                        labelText: 'Rekondisi Fisik',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    onTap: () {
                      infoCollateralChangeNotifier.formatting();
                    },
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                  controller: infoCollateralChangeNotifier.controllerDpGuarantee,
                  decoration: new InputDecoration(
                      labelText: 'DP Jaminan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))
                  ),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    infoCollateralChangeNotifier.amountValidator
                  ],
                  onFieldSubmitted: (value){
                    infoCollateralChangeNotifier.controllerDpGuarantee.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                  },
                  onTap: () {
                    infoCollateralChangeNotifier.formatting();
                  },
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    controller: infoCollateralChangeNotifier.controllerPHMaxAutomotive,
                    decoration: new InputDecoration(
                        labelText: 'PH Maksimal',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    keyboardType: TextInputType.number,
                    // textCapitalization: TextCapitalization.characters,
                    onFieldSubmitted: (value){
                      infoCollateralChangeNotifier.controllerPHMaxAutomotive.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                    },
                    onTap: () {
                      infoCollateralChangeNotifier.formatting();
                    },
                    textAlign: TextAlign.end,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2),
                      infoCollateralChangeNotifier.amountValidator
                    ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.end,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2),
                      infoCollateralChangeNotifier.amountValidator
                    ],
                    onFieldSubmitted: (value){
                      infoCollateralChangeNotifier.controllerTaksasiPriceAutomotive.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                    },
                    controller: infoCollateralChangeNotifier.controllerTaksasiPriceAutomotive,
                    decoration: new InputDecoration(
                        labelText: 'Harga Taksasi / Appraisal',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    ),
                    onTap: () {
                      infoCollateralChangeNotifier.formatting();
                    },
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Text(
                    "Kesimpulan",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.15),
                ),
                Row(
                    children: [
                        Row(
                            children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 0,
                                    groupValue: infoCollateralChangeNotifier.radioValueWorthyOrUnworthy,
                                    onChanged: (value){
                                        infoCollateralChangeNotifier.radioValueWorthyOrUnworthy = value;
                                    }
                                ),
                                Text("Layak")
                            ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width / 27),
                        Row(
                            children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 1,
                                    groupValue: infoCollateralChangeNotifier.radioValueWorthyOrUnworthy,
                                    onChanged: (value){
                                        infoCollateralChangeNotifier.radioValueWorthyOrUnworthy = value;
                                    }
                                ),
                                Text("Tidak Layak")
                            ],
                        ),
                    ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                    validator: (e) {
                        if (e.isEmpty) {
                            return "Tidak boleh kosong";
                        } else {
                            return null;
                        }
                    },
                    readOnly: true,
                    onTap: (){
                        infoCollateralChangeNotifier.formatting();
                        infoCollateralChangeNotifier.searchUsageCollateral(context,1);
                    },
                    controller: infoCollateralChangeNotifier.controllerUsageCollateralOto,
                    decoration: new InputDecoration(
                        labelText: 'Tujuan Penggunaan Collateral',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))
                    )
                ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                // Text(
                //     "Untuk Semua Unit",
                //     style: TextStyle(
                //         fontSize: 16,
                //         fontWeight: FontWeight.w700,
                //         letterSpacing: 0.15),
                // ),
                // // SizedBox(height: MediaQuery.of(context).size.height / 47),
                // Row(
                //     children: [
                //         Row(
                //             children: [
                //                 Radio(
                //                     activeColor: primaryOrange,
                //                     value: 0,
                //                     groupValue: infoCollateralChangeNotifier.radioValueForAllUnitOto,
                //                     onChanged: (value){
                //                         infoCollateralChangeNotifier.radioValueForAllUnitOto = value;
                //                     }
                //                 ),
                //                 Text("Ya")
                //             ],
                //         ),
                //         SizedBox(height: MediaQuery.of(context).size.width / 27),
                //         Row(
                //             children: [
                //                 Radio(
                //                     activeColor: primaryOrange,
                //                     value: 1,
                //                     groupValue: infoCollateralChangeNotifier.radioValueForAllUnitOto,
                //                     onChanged: (value){
                //                         infoCollateralChangeNotifier.radioValueForAllUnitOto = value;
                //                     }
                //                 ),
                //                 Text("Tidak")
                //             ],
                //         ),
                //     ],
                // ),
            ],
        );
      },
    );
  }
}
