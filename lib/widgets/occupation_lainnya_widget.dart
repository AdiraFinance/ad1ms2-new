import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class OccupationLainnyaWidget extends StatefulWidget {
  @override
  _OccupationLainnyaWidgetState createState() =>
      _OccupationLainnyaWidgetState();
}

class _OccupationLainnyaWidgetState extends State<OccupationLainnyaWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMOccupationChangeNotif>(
      builder: (context, formMChangeNotif, _) {
        return Column(
          children: [
//            TextFormField(
//              autovalidate: formMChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
//              controller: formMChangeNotif.controllerCompanyName,
//              style: new TextStyle(color: Colors.black),
//              decoration: new InputDecoration(
//                  labelText: 'Nama Perusahaan',
//                  labelStyle: TextStyle(color: Colors.black),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(8))),
//              keyboardType: TextInputType.text,
//              textCapitalization: TextCapitalization.characters,
//            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            DropdownButtonFormField<CompanyTypeModel>(
//                autovalidate: formMChangeNotif.autoValidate,
//                validator: (e) {
//                  if (e == null) {
//                    return "Silahkan pilih jenis perusahaan";
//                  } else {
//                    return null;
//                  }
//                },
//                value: formMChangeNotif.companyTypeModelSelected,
//                onChanged: (value) {
//                  formMChangeNotif.companyTypeModelSelected = value;
//                },
//                onTap: () {
//                  FocusManager.instance.primaryFocus.unfocus();
//                },
//                decoration: InputDecoration(
//                  labelText: "Jenis Perusahaan",
//                  border: OutlineInputBorder(),
//                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                ),
//                items: formMChangeNotif.listCompanyType.map((value) {
//                  return DropdownMenuItem<CompanyTypeModel>(
//                    value: value,
//                    child: Text(
//                      value.desc,
//                      overflow: TextOverflow.ellipsis,
//                    ),
//                  );
//                }).toList()),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            TextFormField(
//              autovalidate: formMChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
//              onTap: () {
//                FocusManager.instance.primaryFocus.unfocus();
//                formMChangeNotif.searchSectorEconomic(context);
//              },
//              readOnly: true,
//              controller: formMChangeNotif.controllerSectorEconomic,
//              style: new TextStyle(color: Colors.black),
//              decoration: new InputDecoration(
//                  labelText: 'Sektor Ekonomi',
//                  labelStyle: TextStyle(color: Colors.black),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(8))),
//            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            TextFormField(
//              autovalidate: formMChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
//              readOnly: true,
//              onTap: () {
//                FocusManager.instance.primaryFocus.unfocus();
//                formMChangeNotif.searchBusinesField(context);
//              },
//              controller: formMChangeNotif.controllerBusinessField,
//              style: new TextStyle(color: Colors.black),
//              decoration: new InputDecoration(
//                  labelText: 'Lapangan Usaha',
//                  labelStyle: TextStyle(color: Colors.black),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(8))),
//              enabled: formMChangeNotif.controllerSectorEconomic.text == "" ? false : true,
//            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            TextFormField(
//              autovalidate: formMChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//                // LengthLimitingTextInputFormatter(10),
//              ],
//              controller: formMChangeNotif.controllerEmployeeTotal,
//              style: new TextStyle(color: Colors.black),
//              decoration: new InputDecoration(
//                  labelText: 'Total Pegawai',
//                  labelStyle: TextStyle(color: Colors.black),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(8))),
//              keyboardType: TextInputType.number,
//            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              autovalidate: formMChangeNotif.autoValidate,
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly,
                // LengthLimitingTextInputFormatter(10),
              ],
              controller: formMChangeNotif.controllerTotalEstablishedDate,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Total Lama Usaha/Kerja (bln)',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              autovalidate: formMChangeNotif.autoValidate,
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              readOnly: true,
              onTap: () {
                FocusManager.instance.primaryFocus.unfocus();
                formMChangeNotif.searchPEP(context);
              },
              controller: formMChangeNotif.controllerJenisPEP,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Jenis PEP & High Risk Cust',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<EmployeeStatusModel>(
              autovalidate: formMChangeNotif.autoValidate,
              validator: (e) {
                if (e == null) {
                  return "Silahkan pilih status pegawai";
                } else {
                  return null;
                }
              },
              value: formMChangeNotif.employeeStatusModelSelected,
              onChanged: (value) {
                formMChangeNotif.employeeStatusModelSelected = value;
              },
              onTap: () {
                FocusManager.instance.primaryFocus.unfocus();
              },
              decoration: InputDecoration(
                labelText: "Status Pegawai",
                border: OutlineInputBorder(),
                contentPadding: EdgeInsets.symmetric(horizontal: 10),
              ),
              items: formMChangeNotif.listEmployeeStatus.map((value) {
                return DropdownMenuItem<EmployeeStatusModel>(
                  value: value,
                  child: Text(
                    value.desc,
                    overflow: TextOverflow.ellipsis,
                  ),
                );
              }).toList()
            ),
          ],
        );
      },
    );
  }
}
