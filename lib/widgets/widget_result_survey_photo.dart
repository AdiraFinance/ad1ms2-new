import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/photo_type_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WidgetResultSurveyPhoto extends StatefulWidget {
  @override
  _WidgetResultSurveyPhotoState createState() => _WidgetResultSurveyPhotoState();
}

class _WidgetResultSurveyPhotoState extends State<WidgetResultSurveyPhoto> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              "Tambah Survey Foto",
              style: TextStyle(color: Colors.black)
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
        ),
        body: Consumer<ResultSurveyChangeNotifier>(
          builder: (context, value, child) {
            return SingleChildScrollView(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width / 27,
                  vertical: MediaQuery.of(context).size.height / 57),
              child: Column(
                children: [
                  DropdownButtonFormField<PhotoTypeModel>(
                      autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
                      validator: (e) {
                        if (e == null) {
                          return "Silahkan pilih jenis identitas";
                        } else {
                          return null;
                        }
                      },
                      value: value.photoTypeSelected,
                      onChanged: (data) {
                        value.photoTypeSelected = data;
                        value.clearListPhoto();
                      },
                      onTap: () {
                        FocusManager.instance.primaryFocus.unfocus();
                      },
                      decoration: InputDecoration(
                        labelText: "Jenis Foto",
                        border: OutlineInputBorder(),
                        contentPadding:
                        EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: value
                          .listPhotoType
                          .map((value) {
                        return DropdownMenuItem<PhotoTypeModel>(
                          value: value,
                          child: Text(
                            value.name,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  value.listPhoto.isEmpty
                  ? RaisedButton(
                    onPressed: () {
                      value.addFile();
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius:
                        new BorderRadius.circular(8.0)),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.camera_alt),
                        SizedBox(
                            width:
                            MediaQuery.of(context).size.width /
                                47),
                        Text("ADD")
                      ],
                    ),
                    color: myPrimaryColor,
                  )
                  : Row(
                    children: <Widget>[
                      Row(
                        children: _listWidgetPhoto(value.listPhoto),
                      ),
                      value.listPhoto.length < 4
                        ? IconButton(
                        onPressed: () {
                          value.addFile();
                        },
                        icon: Icon(
                          Icons.add_a_photo,
                          color: myPrimaryColor,
                          size: 33,
                        ))
                        : SizedBox(width: 0.0, height: 0.0)
                    ],
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  List<Widget> _listWidgetPhoto(List<ImageFileModel> data) {
    List<Widget> _newListWidget = [];
    for (int i = 0; i < data.length; i++) {
      _newListWidget.add(Padding(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 57,
            vertical: MediaQuery.of(context).size.height / 57),
        child: PopupMenuButton<String>(
            onSelected: (value) {
              Provider.of<ResultSurveyChangeNotifier>(context,listen: false).deletePhoto(i);
            },
            child: ClipRRect(
              child: Image.file(data[i].imageFile,
                  width: 60,
                  height: 60,
                  fit: BoxFit.cover,
                  filterQuality: FilterQuality.medium),
              borderRadius: BorderRadius.circular(8.0),
            ),
            itemBuilder: (context) {
              return TitlePopUpMenuButton.choices.map((String choice) {
                return PopupMenuItem(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            }),
      ));
    }
    return _newListWidget;
  }
}
