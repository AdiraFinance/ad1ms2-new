import 'package:ad1ms2_dev/models/certificate_type_model.dart';
import 'package:ad1ms2_dev/models/floor_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/foundation_type_model.dart';
import 'package:ad1ms2_dev/models/property_type_model.dart';
import 'package:ad1ms2_dev/models/street_type_model.dart';
import 'package:ad1ms2_dev/models/type_of_roof_model.dart';
import 'package:ad1ms2_dev/models/wall_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class WidgetInfoCollateralProperty extends StatefulWidget {

  @override
  _WidgetInfoCollateralPropertyState createState() => _WidgetInfoCollateralPropertyState();
}

class _WidgetInfoCollateralPropertyState extends State<WidgetInfoCollateralProperty> {
  @override
  void initState() {
    super.initState();
    Provider.of<InformationCollateralChangeNotifier>(context,listen: false).addListAddressType();
    Provider.of<InformationCollateralChangeNotifier>(context, listen: false).getTypeOfRoof(context);
    Provider.of<InformationCollateralChangeNotifier>(context, listen: false).getWallType(context);
    Provider.of<InformationCollateralChangeNotifier>(context, listen: false).getFloorType(context);
    Provider.of<InformationCollateralChangeNotifier>(context, listen: false).getFoundationType(context);
    Provider.of<InformationCollateralChangeNotifier>(context, listen: false).getStreetType(context);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<InformationCollateralChangeNotifier>(
      builder: (context,infoCollateralChangeNotifier,_){
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Nama Jaminan = Pemohon",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold
              ),
            ),
            // SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: [
                Row(
                  children: [
                    Radio(
                        activeColor: primaryOrange,
                        value: 0,
                        groupValue: infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty,
                        onChanged: (value){
                          infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty = value;
                          infoCollateralChangeNotifier.sameWithApplicantProperty(context);
                        }
                    ),
                    Text("Ya")
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.width / 27),
                Row(
                  children: [
                    Radio(
                        activeColor: primaryOrange,
                        value: 1,
                        groupValue: infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty,
                        onChanged: (value){
                          infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty = value;
                          infoCollateralChangeNotifier.sameWithApplicantProperty(context);
                        }
                    ),
                    Text("Tidak")
                  ],
                )
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty == 0
                ?
            TextFormField(
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //   if (e.isEmpty) {
              //     return "Tidak boleh kosong";
              //   } else {
              //     return null;
              //   }
              // },
                enabled: false,
                controller: infoCollateralChangeNotifier.controllerIdentityType,
                decoration: new InputDecoration(
                    labelText: 'Jenis Identitas',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))))
                :
            DropdownButtonFormField<IdentityModel>(
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //   if (e == null) {
              //     return "Silahkan pilih jenis sertifikat";
              //   } else {
              //     return null;
              //   }
              // },
                value: infoCollateralChangeNotifier
                    .identityModel,
                onChanged: (value) {
                  infoCollateralChangeNotifier
                      .identityModel = value;
                },
                decoration: InputDecoration(
                  labelText: "Jenis Identitas",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier
                    .listIdentityType
                    .map((value) {
                  return DropdownMenuItem<IdentityModel>(
                    value: value,
                    child: Text(
                      value.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                readOnly: true,
                controller: infoCollateralChangeNotifier.controllerIdentityNumber,
                decoration: new InputDecoration(
                    labelText: 'No Identitas',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)))),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: infoCollateralChangeNotifier.controllerNameOnCollateral,
                autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                decoration: new InputDecoration(
                    labelText: 'Nama Pada Jaminan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)))),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              onTap: () {
                FocusManager.instance.primaryFocus.unfocus();
                infoCollateralChangeNotifier.selectBirthDate(context,1);
              },
              controller: infoCollateralChangeNotifier.controllerBirthDateProp,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Tanggal Lahir',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //   if (e.isEmpty) {
              //     return "Tidak boleh kosong";
              //   } else {
              //     return null;
              //   }
              // },
              readOnly: true,
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                readOnly: true,
                controller: infoCollateralChangeNotifier.controllerBirthPlaceValidWithIdentity1,
                decoration: new InputDecoration(
                    labelText: 'Tempat Lahir Sesuai Identitas',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)))),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                readOnly: true,
                onTap: (){
                  infoCollateralChangeNotifier.searchBirthPlace(context,1);
                },
                controller: infoCollateralChangeNotifier.controllerBirthPlaceValidWithIdentityLOV,
                decoration: new InputDecoration(
                    labelText: 'Tempat Lahir Sesuai Identitas',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)))),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerCertificateNumber,
                decoration: new InputDecoration(
                    labelText: 'No Sertifikat',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)))),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<CertificateTypeModel>(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e == null) {
                //     return "Silahkan pilih jenis sertifikat";
                //   } else {
                //     return null;
                //   }
                // },
                value: infoCollateralChangeNotifier
                    .certificateTypeSelected,
                onChanged: (value) {
                  infoCollateralChangeNotifier
                      .certificateTypeSelected = value;
                },
                decoration: InputDecoration(
                  labelText: "Jenis Sertifikat",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier
                    .listCertificateType
                    .map((value) {
                  return DropdownMenuItem<CertificateTypeModel>(
                    value: value,
                    child: Text(
                      value.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<PropertyTypeModel>(
                autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                validator: (e) {
                  if (e == null) {
                    return "Silahkan pilih jenis properti";
                  } else {
                    return null;
                  }
                },
                value: infoCollateralChangeNotifier
                    .propertyTypeSelected,
                onChanged: (value) {
                  infoCollateralChangeNotifier
                      .propertyTypeSelected = value;
                },
                decoration: InputDecoration(
                  labelText: "Jenis Properti",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier
                    .listPropertyType
                    .map((value) {
                  return DropdownMenuItem<PropertyTypeModel>(
                    value: value,
                    child: Text(
                      value.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerBuildingArea,
                decoration: new InputDecoration(
                    labelText: 'Luas Bangunan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)))),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerSurfaceArea,
                decoration: new InputDecoration(
                    labelText: 'Luas Tanah',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)))),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              controller: infoCollateralChangeNotifier.controllerTaksasiPrice,
              decoration: new InputDecoration(
                  labelText: 'Harga Taksasi/Appraisal',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))
              ),
              keyboardType: TextInputType.number,
              textAlign: TextAlign.end,
              onFieldSubmitted: (value) {
                infoCollateralChangeNotifier.controllerTaksasiPrice.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
              },
              textInputAction: TextInputAction.done,
              inputFormatters: [
                DecimalTextInputFormatter(decimalRange: 2),
                infoCollateralChangeNotifier.amountValidator
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerSifatJaminan,
                decoration: new InputDecoration(
                    labelText: 'Sifat Jaminan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerBuktiKepemilikan,
                decoration: new InputDecoration(
                    labelText: 'Bukti Kepemilikan Jaminan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                readOnly: true,
                controller: infoCollateralChangeNotifier.controllerCertificateReleaseDate,
                onTap: (){
                  infoCollateralChangeNotifier.selectCertificateReleaseDate(context);
                },
                decoration: new InputDecoration(
                    labelText: 'Tanggal Penerbitan Sertifikat',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerCertificateReleaseYear,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly
                ],
                decoration: new InputDecoration(
                    labelText: 'Tahun Penerbitan Sertifikat',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerNamaPemegangHak,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Nama Pemegang Hak',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerNoSuratUkur,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Nomor Surat Ukur/Gambar Situasi',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                readOnly: true,
                controller: infoCollateralChangeNotifier.controllerDateOfMeasuringLetter,
                onTap: (){
                  infoCollateralChangeNotifier.selectDateOfMeasuringLetter(context);
                },
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Tanggal Surat Ukur/Gambar Situasi',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //   if (e.isEmpty) {
              //     return "Tidak boleh kosong";
              //   } else {
              //     return null;
              //   }
              // },
                controller: infoCollateralChangeNotifier.controllerCertificateOfMeasuringLetter,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Sertifikat dan Surat Ukur/Gambar Situasi Dikeluarkan Oleh',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  infoCollateralChangeNotifier.amountValidator
                ],
                onFieldSubmitted: (value){
                  infoCollateralChangeNotifier.controllerDPJaminan.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                },
                controller: infoCollateralChangeNotifier.controllerDPJaminan,
                decoration: new InputDecoration(
                    labelText: 'DP Jaminan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  infoCollateralChangeNotifier.amountValidator
                ],
                onFieldSubmitted: (value){
                  infoCollateralChangeNotifier.controllerPHMax.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                },
                controller: infoCollateralChangeNotifier.controllerPHMax,
                decoration: new InputDecoration(
                    labelText: 'PH Maksimal',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerJarakFasumPositif,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Jarak Fasum Positif',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerJarakFasumNegatif,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Jarak Fasum Negatif',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //   if (e.isEmpty) {
              //     return "Tidak boleh kosong";
              //   } else {
              //     return null;
              //   }
              // },
              controller: infoCollateralChangeNotifier.controllerHargaTanah,
              decoration: new InputDecoration(
                  labelText: 'Harga Tanah',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))
              ),
              keyboardType: TextInputType.number,
              textAlign: TextAlign.end,
              onFieldSubmitted: (value) {
                infoCollateralChangeNotifier.controllerHargaTanah.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
              },
              textInputAction: TextInputAction.done,
              inputFormatters: [
                DecimalTextInputFormatter(decimalRange: 2),
                infoCollateralChangeNotifier.amountValidator
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerHargaNJOP,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  infoCollateralChangeNotifier.controllerHargaNJOP.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  infoCollateralChangeNotifier.amountValidator
                ],
                decoration: new InputDecoration(
                    labelText: 'Harga NJOP',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  infoCollateralChangeNotifier.amountValidator
                ],
                onFieldSubmitted: (value){
                  infoCollateralChangeNotifier.controllerHargaBangunan.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                },
                controller: infoCollateralChangeNotifier.controllerHargaBangunan,
                decoration: new InputDecoration(
                    labelText: 'Harga Bangunan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<TypeOfRoofModel>(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e == null) {
                //     return "Silahkan pilih jenis atap";
                //   } else {
                //     return null;
                //   }
                // },
                value: infoCollateralChangeNotifier
                    .typeOfRoofSelected,
                onChanged: (value) {
                  infoCollateralChangeNotifier
                      .typeOfRoofSelected = value;
                },
                decoration: InputDecoration(
                  labelText: "Jenis Atap",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier
                  .listTypeOfRoof
                  .map((value) {
                    return DropdownMenuItem<TypeOfRoofModel>(
                      value: value,
                      child: Text(
                        value.name,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<WallTypeModel>(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e == null) {
                //     return "Silahkan pilih jenis dinding";
                //   } else {
                //     return null;
                //   }
                // },
                value: infoCollateralChangeNotifier
                    .wallTypeSelected,
                onChanged: (value) {
                  infoCollateralChangeNotifier
                      .wallTypeSelected = value;
                },
                decoration: InputDecoration(
                  labelText: "Jenis Dinding",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier
                  .listWallType
                  .map((value) {
                    return DropdownMenuItem<WallTypeModel>(
                      value: value,
                      child: Text(
                        value.name,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<FloorTypeModel>(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e == null) {
                //     return "Silahkan pilih jenis lantain";
                //   } else {
                //     return null;
                //   }
                // },
                value: infoCollateralChangeNotifier
                    .floorTypeSelected,
                onChanged: (value) {
                  infoCollateralChangeNotifier
                      .floorTypeSelected = value;
                },
                decoration: InputDecoration(
                  labelText: "Jenis Lantai",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier
                    .listFloorType
                    .map((value) {
                  return DropdownMenuItem<FloorTypeModel>(
                    value: value,
                    child: Text(
                      value.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<FoundationTypeModel>(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e == null) {
                //     return "Silahkan pilih jenis fondasi";
                //   } else {
                //     return null;
                //   }
                // },
                value: infoCollateralChangeNotifier
                    .foundationTypeSelected,
                onChanged: (value) {
                  infoCollateralChangeNotifier
                      .foundationTypeSelected = value;
                },
                decoration: InputDecoration(
                  labelText: "Jenis Fondasi",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier
                    .listFoundationType
                    .map((value) {
                  return DropdownMenuItem<FoundationTypeModel>(
                    value: value,
                    child: Text(
                      value.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<StreetTypeModel>(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e == null) {
                //     return "Silahkan pilih tipe jalan";
                //   } else {
                //     return null;
                //   }
                // },
                value: infoCollateralChangeNotifier
                    .streetTypeSelected,
                onChanged: (value) {
                  infoCollateralChangeNotifier
                      .streetTypeSelected = value;
                },
                decoration: InputDecoration(
                  labelText: "Tipe Jalan",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier
                    .listStreetType
                    .map((value) {
                  return DropdownMenuItem<StreetTypeModel>(
                    value: value,
                    child: Text(
                      value.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Text(
              "Dilewati Mobil",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold
              ),
            ),
            // SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: [
                Row(
                  children: [
                    Radio(
                        activeColor: primaryOrange,
                        value: 0,
                        groupValue: infoCollateralChangeNotifier.radioValueAccessCar,
                        onChanged: (value){
                          infoCollateralChangeNotifier.radioValueAccessCar = value;
                        }
                    ),
                    Text("Ya")
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.width / 27),
                Row(
                  children: [
                    Radio(
                        activeColor: primaryOrange,
                        value: 1,
                        groupValue: infoCollateralChangeNotifier.radioValueAccessCar,
                        onChanged: (value){
                          infoCollateralChangeNotifier.radioValueAccessCar = value;
                        }
                    ),
                    Text("Tidak")
                  ],
                )
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerJumlahRumahDalamRadius,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Jumlah Rumah Dalam Radius',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                controller: infoCollateralChangeNotifier.controllerMasaHakBerlaku,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Masa Berlaku Hak',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerNoIMB,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'No IMB',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                readOnly: true,
                controller: infoCollateralChangeNotifier.controllerDateOfIMB,
                onTap: (){
                  infoCollateralChangeNotifier.selectDateOfIMB(context);
                },
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Tanggal IMB',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerLuasBangunanIMB,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Luas Bangunan Berdasarkan IMB',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                readOnly: true,
                onTap: (){
                  infoCollateralChangeNotifier.searchUsageCollateral(context,2);
                },
                controller: infoCollateralChangeNotifier.controllerUsageCollateralProperty,
                decoration: new InputDecoration(
                    labelText: 'Tujuan Penggunaan Jaminan/Collateral',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: infoCollateralChangeNotifier.controllerLTV,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'LTV',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))
                )
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Text(
              "Untuk Semua Unit",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold
              ),
            ),
            // SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: [
                Row(
                  children: [
                    Radio(
                        activeColor: primaryOrange,
                        value: 0,
                        groupValue: infoCollateralChangeNotifier.radioValueForAllUnitProperty,
                        onChanged: (value){
                          infoCollateralChangeNotifier.radioValueForAllUnitProperty = value;
                        }
                    ),
                    Text("Ya")
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.width / 27),
                Row(
                  children: [
                    Radio(
                        activeColor: primaryOrange,
                        value: 1,
                        groupValue: infoCollateralChangeNotifier.radioValueForAllUnitProperty,
                        onChanged: (value){
                          infoCollateralChangeNotifier.radioValueForAllUnitProperty = value;
                        }
                    ),
                    Text("Tidak")
                  ],
                ),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<JenisAlamatModel>(
                // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                // validator: (e) {
                //   if (e == null) {
                //     return "Silahkan pilih jenis alamat";
                //   } else {
                //     return null;
                //   }
                // },
                value: infoCollateralChangeNotifier.addressTypeSelected,
                onChanged: (value) {
                  infoCollateralChangeNotifier.addressTypeSelected =
                      value;
                  if(infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty == 0){
                    infoCollateralChangeNotifier.setValueAddress(1);
                  }
                },
                decoration: InputDecoration(
                  labelText: "Jenis Alamat",
                  border: OutlineInputBorder(),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 10),
                ),
                items: infoCollateralChangeNotifier.listAddressType
                    .map((value) {
                  return DropdownMenuItem<JenisAlamatModel>(
                    value: value,
                    child: Text(
                      value.DESKRIPSI,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //   if (e.isEmpty) {
              //     return "Tidak boleh kosong";
              //   } else {
              //     return null;
              //   }
              // },
              controller:
              infoCollateralChangeNotifier.controllerAddress,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                  labelText: 'Alamat',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                  filled: !infoCollateralChangeNotifier.enableTf,
                  fillColor: !infoCollateralChangeNotifier.enableTf
                      ? Colors.black12
                      : Colors.white),
              maxLines: 3,
              keyboardType: TextInputType.text,
              textCapitalization: TextCapitalization.characters,
            ),
            SizedBox(
                height:
                MediaQuery.of(context).size.height / 47),
            Row(
              children: [
                Expanded(
                  flex: 5,
                  child: TextFormField(
                    // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      // LengthLimitingTextInputFormatter(10),
                    ],
                    controller:
                    infoCollateralChangeNotifier.controllerRT,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'RT',
                        labelStyle:
                        TextStyle(color: Colors.black),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                        filled: !infoCollateralChangeNotifier.enableTf,
                        fillColor: !infoCollateralChangeNotifier.enableTf
                            ? Colors.black12
                            : Colors.white),
                    keyboardType: TextInputType.number,
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  flex: 5,
                  child: TextFormField(
                    // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      // LengthLimitingTextInputFormatter(10),
                    ],
                    controller:
                    infoCollateralChangeNotifier.controllerRW,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'RW',
                        labelStyle:
                        TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.circular(8)),
                        filled: !infoCollateralChangeNotifier.enableTf,
                        fillColor: !infoCollateralChangeNotifier.enableTf
                            ? Colors.black12
                            : Colors.white),
                    keyboardType: TextInputType.number,
                  ),
                )
              ],
            ),
            SizedBox(
                height:
                MediaQuery.of(context).size.height / 47),
            TextFormField(
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //     if (e.isEmpty) {
              //         return "Tidak boleh kosong";
              //     } else {
              //         return null;
              //     }
              // },
              onTap: () {
                infoCollateralChangeNotifier.searchKelurahan(context);
              },
              controller: infoCollateralChangeNotifier.controllerKelurahan,
              readOnly: true,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Kelurahan',
                  labelStyle:
                  TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius:
                      BorderRadius.circular(8))),
              keyboardType: TextInputType.text,
              textCapitalization: TextCapitalization.characters,
            ),
            SizedBox(
                height:
                MediaQuery.of(context).size.height / 47),
            TextFormField(
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //   if (e.isEmpty) {
              //     return "Tidak boleh kosong";
              //   } else {
              //     return null;
              //   }
              // },
              controller: infoCollateralChangeNotifier.controllerKecamatan,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Kecamatan',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                  filled: !infoCollateralChangeNotifier.enableTf,
                  fillColor: !infoCollateralChangeNotifier.enableTf
                      ? Colors.black12
                      : Colors.white),
              enabled: false,
            ),
            SizedBox(
                height:
                MediaQuery.of(context).size.height / 47),
            TextFormField(
              // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
              // validator: (e) {
              //   if (e.isEmpty) {
              //     return "Tidak boleh kosong";
              //   } else {
              //     return null;
              //   }
              // },
              controller: infoCollateralChangeNotifier.controllerKota,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Kabupaten/Kota',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                  filled: !infoCollateralChangeNotifier.enableTf,
                  fillColor: !infoCollateralChangeNotifier.enableTf
                      ? Colors.black12
                      : Colors.white),
              enabled: false,
            ),
            SizedBox(
                height:
                MediaQuery.of(context).size.height / 47),
            Row(
              children: [
                Expanded(
                  flex: 7,
                  child: TextFormField(
                    // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    controller: infoCollateralChangeNotifier.controllerProv,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Provinsi',
                        labelStyle:
                        TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.circular(8)),
                        filled: !infoCollateralChangeNotifier.enableTf,
                        fillColor: !infoCollateralChangeNotifier.enableTf
                            ? Colors.black12
                            : Colors.white),
                    enabled: false,
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  flex: 3,
                  child: TextFormField(
                    // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    controller: infoCollateralChangeNotifier.controllerPostalCode,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Kode Pos',
                        labelStyle:
                        TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.circular(8)),
                        filled: !infoCollateralChangeNotifier.enableTf,
                        fillColor: !infoCollateralChangeNotifier.enableTf
                            ? Colors.black12
                            : Colors.white),
                    enabled: false,
                  ),
                ),
              ],
            ),
            SizedBox(
                height:
                MediaQuery.of(context).size.height / 47),
            TextFormField(
              // autovalidate:
              // formMAddAlamatKorespon.autoValidate,
              // validator: (e) {
              //     if (e.isEmpty) {
              //         return "Tidak boleh kosong";
              //     } else {
              //         return null;
              //     }
              // },
              readOnly: true,
              onTap: (){
                infoCollateralChangeNotifier.setLocationAddressByMap(context);
              },
              maxLines: 3,
              controller: infoCollateralChangeNotifier.controllerAddressFromMap,
              style: TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Alamat Lokasi',
                  labelStyle:
                  TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius:
                      BorderRadius.circular(8))),
              keyboardType: TextInputType.number,
            ),
          ],
        );
      },
    );
  }
}
