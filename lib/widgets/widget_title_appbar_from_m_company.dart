import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TitleAppBarFormMCompany extends StatelessWidget {
    final int selectedIndex;
    const TitleAppBarFormMCompany({this.selectedIndex});

    @override
    Widget build(BuildContext context) {
        var _provider = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        var _text = "";
        if (selectedIndex == 0) {
            return Text("Rincian Lembaga",style: TextStyle(color: Colors.black));
        } else if (selectedIndex == 1) {
            return Text("Pendapatan",style: TextStyle(color: Colors.black));
        } else if (selectedIndex == 2) {
            return Text("Penjamin",style: TextStyle(color: Colors.black));
        } else if (selectedIndex == 3) {
            return Text("Manajemen PIC",style: TextStyle(color: Colors.black));
        } else if (selectedIndex == 4) {
            return Text("Pemegang Saham",style: TextStyle(color: Colors.black));
        }
        else if (selectedIndex == 5) {
            return Text("Aplikasi",style: TextStyle(color: Colors.black));
        } else if (selectedIndex == 6) {
            return Text("Informasi Objek",style: TextStyle(color: Colors.black));
        } else if (selectedIndex == 7) {
            return Text("Karoseri",style: TextStyle(color: Colors.black));
        } else if (selectedIndex == 8) {
            return Text("Rincian Pinjaman",style: TextStyle(color: Colors.black));
        } else if(selectedIndex == 9){
            return Text("Subsidi",style: TextStyle(color: Colors.black));
        } else if(selectedIndex == 10){
            return Text("Informasi Dokumen",style: TextStyle(color: Colors.black));
        } else if(selectedIndex == 11){
            if(_provider.groupObjectSelected != null){
                if(_provider.groupObjectSelected.KODE == "001"){
                    _text = "Motor";
                }
                else if(_provider.groupObjectSelected.KODE == "001"){
                    _text = "Mobil";
                }
            }
            return Text(
                "Taksasi Unit $_text",
                style: TextStyle(color: Colors.black)
            );
        } else if(selectedIndex == 12){
            return Text("Marketing Notes",style: TextStyle(color: Colors.black));
        } else {
            return Text("Hasil Survey",style: TextStyle(color: Colors.black));
        }
    }
}
