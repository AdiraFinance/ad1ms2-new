import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class WidgetIncomeOccupationTypeEntrepreneur extends StatefulWidget {
  final String maritalStatusKode;

  const WidgetIncomeOccupationTypeEntrepreneur({Key key, this.maritalStatusKode}) : super(key: key);
  @override
  _WidgetIncomeOccupationTypeEntrepreneurState createState() =>
      _WidgetIncomeOccupationTypeEntrepreneurState();
}

class _WidgetIncomeOccupationTypeEntrepreneurState extends State<WidgetIncomeOccupationTypeEntrepreneur> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<FormMIncomeChangeNotifier>(context, listen: false).setIsMarried(context);
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMIncomeChangeNotifier>(
      builder: (context, formMIncomeChangeNotif, _) {
        return SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width/37,
            vertical: MediaQuery.of(context).size.height/57,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerMonthlyIncome,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Pendapatan Perbulan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerMonthlyIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingWiraswasta();
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerOtherIncome,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Pendapatan Lainnya',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerOtherIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingWiraswasta();
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                enabled: false,
                autovalidate: formMIncomeChangeNotif.autoValidate,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: formMIncomeChangeNotif.controllerTotalIncome,
                style: new TextStyle(color: Colors.black),
                textAlign: TextAlign.end,
                decoration: new InputDecoration(
                    labelText: 'Total Pendapatan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerCostOfGoodsSold,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Harga Pokok Pendapatan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerCostOfGoodsSold.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingWiraswasta();
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                enabled: false,
                autovalidate: formMIncomeChangeNotif.autoValidate,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: formMIncomeChangeNotif.controllerGrossProfit,
                style: new TextStyle(color: Colors.black),
                textAlign: TextAlign.end,
                decoration: new InputDecoration(
                    labelText: 'Laba Kotor',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textCapitalization: TextCapitalization.characters,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerOperatingCosts,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Biaya Operasional',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerOperatingCosts.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingWiraswasta();
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerOtherCosts,
                style: TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Biaya Lainnya',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerOtherCosts.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingWiraswasta();
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                enabled: false,
                autovalidate: formMIncomeChangeNotif.autoValidate,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: formMIncomeChangeNotif.controllerNetProfitBeforeTax,
                style: new TextStyle(color: Colors.black),
                textAlign: TextAlign.end,
                decoration: new InputDecoration(
                    labelText: 'Laba Bersih Sebelum Pajak',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textCapitalization: TextCapitalization.characters,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerTax,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Pajak',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerTax.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingWiraswasta();
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                enabled: false,
                autovalidate: formMIncomeChangeNotif.autoValidate,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: formMIncomeChangeNotif.controllerNetProfitAfterTax,
                style: new TextStyle(color: Colors.black),
                textAlign: TextAlign.end,
                decoration: new InputDecoration(
                    labelText: 'Laba Bersih Setelah Pajak',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textCapitalization: TextCapitalization.characters,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
//              enabled: false,
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerCostOfLiving,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Biaya Hidup',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerCostOfLiving.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingWiraswasta();
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                enabled: false,
                autovalidate: formMIncomeChangeNotif.autoValidate,
                // validator: (e) {
                //   if (e.isEmpty) {
                //     return "Tidak boleh kosong";
                //   } else {
                //     return null;
                //   }
                // },
                controller: formMIncomeChangeNotif.controllerRestIncome,
                style: new TextStyle(color: Colors.black),
                textAlign: TextAlign.end,
                decoration: new InputDecoration(
                    labelText: 'Sisa Pendapatan',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textCapitalization: TextCapitalization.characters,
              ),
              formMIncomeChangeNotif.isMarried == true
                  ? SizedBox(height: MediaQuery.of(context).size.height / 47)
                  : SizedBox(),
              Visibility(
                visible: formMIncomeChangeNotif.isMarried,
                child: TextFormField(
//              enabled: false,
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerSpouseIncome,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Pendapatan Pasangan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerSpouseIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
              TextFormField(
                autovalidate: formMIncomeChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                controller: formMIncomeChangeNotif.controllerOtherInstallments,
                style: new TextStyle(color: Colors.black),
                decoration: new InputDecoration(
                    labelText: 'Angsuran Lainnya',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                textAlign: TextAlign.end,
                onFieldSubmitted: (value) {
                  formMIncomeChangeNotif.controllerOtherInstallments.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                onTap: () {
                  formMIncomeChangeNotif.formattingWiraswasta();
                  formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                  formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                  formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                  formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                },
                textInputAction: TextInputAction.done,
                inputFormatters: [
                  DecimalTextInputFormatter(decimalRange: 2),
                  formMIncomeChangeNotif.amountValidator
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
            ],
          ),
        );
      },
    );
  }
}
