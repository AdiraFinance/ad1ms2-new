import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_add_edit_asset.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WidgetResultSurveyCreateEditAsset extends StatefulWidget {
  @override
  _WidgetResultSurveyCreateEditAssetState createState() => _WidgetResultSurveyCreateEditAssetState();
}

class _WidgetResultSurveyCreateEditAssetState extends State<WidgetResultSurveyCreateEditAsset> {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            fontFamily: "NunitoSans",
            accentColor: myPrimaryColor,
            primaryColor: Colors.black,
            primarySwatch: primaryOrange
        ),
        child: Scaffold(
          appBar: AppBar(
            title: Text(
                "Hasil Survey - Aset",
                style: TextStyle(color: Colors.black)
            ),
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(
                color: Colors.black
            ),
          ),
          body: Consumer<ResultSurveyChangeNotifier>(
            builder: (context, value, child) {
              return  ListView.builder(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height / 47,
                    horizontal: MediaQuery.of(context).size.width / 27),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                              WidgetAddEditAsset(
                                  index: index,
                                  model: value.listResultSurveyAssetModel[index],
                                  flag: 1
                              )
                          )
                      );
                    },
                    child: Card(
                      child: Container(
                        padding: EdgeInsets.all(13),
                        child: Stack(
                          children: [
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(child: Text("Jenis Aset"),flex: 5),
                                    Text(" : "),
                                    Expanded(
                                        child: Text(
                                            "${value.listResultSurveyAssetModel[index].assetTypeModel.id} - "
                                                "${value.listResultSurveyAssetModel[index].assetTypeModel.name}"
                                        ),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                Row(
                                  children: [
                                    Expanded(child: Text("Jumlah"),flex: 5),
                                    Text(" : "),
                                    Expanded(
                                        child: Text(
                                            "${value.listResultSurveyAssetModel[index].valueAsset}"
                                        ),
                                        flex: 5
                                    )
                                  ],
                                )
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: IconButton(icon: Icon(Icons.delete,color: Colors.red), onPressed: (){}),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: value.listResultSurveyDetailSurveyModel.length,
              );
            },
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetAddEditAsset(flag: 0,index: null,model: null,)));
            },
            child: Icon(Icons.add),
            backgroundColor: myPrimaryColor,
          ),
        )
    );
  }
}
