import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_add_edit_survey_detail.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class WidgetResultSurveyCreateNewEditSurveyDetail extends StatefulWidget {
  @override
  _WidgetResultSurveyCreateNewEditSurveyDetailState createState() => _WidgetResultSurveyCreateNewEditSurveyDetailState();
}

class _WidgetResultSurveyCreateNewEditSurveyDetailState extends State<WidgetResultSurveyCreateNewEditSurveyDetail> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              "Hasil Survey-Survey Detail",
              style: TextStyle(color: Colors.black)
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
        ),
        body: Consumer<ResultSurveyChangeNotifier>(
          builder: (context, value, _) {
            return ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 47,
                  horizontal: MediaQuery.of(context).size.width / 27),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                              WidgetAddEditSurveyDetail(
                                  index: index,
                                  model: value.listResultSurveyDetailSurveyModel[index],
                                  flag: 1
                              )
                          )
                      );
                    },
                    child: Card(
                      child: Container(
                        padding: EdgeInsets.all(13),
                        child: Stack(
                          children: [
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(child: Text("Informasi Lingkungan"),flex: 5),
                                    Text(" : "),
                                    Expanded(
                                        child: Text(
                                            "${value.listResultSurveyDetailSurveyModel[index].environmentalInformationModel.name}"
                                        ),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                Row(
                                  children: [
                                    Expanded(child: Text("Sumber Informasi"),flex: 5),
                                    Text(" : "),
                                    Expanded(
                                        child: Text(
                                            "${value.listResultSurveyDetailSurveyModel[index].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_NAME}"
                                        ),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                Row(
                                  children: [
                                    Expanded(child: Text("Nama Sumber Informasi"),flex: 5),
                                    Text(" : "),
                                    Expanded(
                                        child: Text(
                                            "${value.listResultSurveyDetailSurveyModel[index].resourceInformationName}"
                                        ),
                                        flex: 5
                                    )
                                  ],
                                ),
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: IconButton(icon: Icon(Icons.delete,color: Colors.red), onPressed: (){}),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              itemCount: value.listResultSurveyDetailSurveyModel.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetAddEditSurveyDetail(index: null,model: null,flag: 0)));
          },
          child: Icon(Icons.add),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}
