import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/survey_type_model.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WidgetResultSurveyGroupLocation extends StatefulWidget {
  @override
  _WidgetResultSurveyGroupLocationState createState() => _WidgetResultSurveyGroupLocationState();
}

class _WidgetResultSurveyGroupLocationState extends State<WidgetResultSurveyGroupLocation> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
          appBar: AppBar(
            title: Text(
                "Hasil Survey - Group Lokasi",
                style: TextStyle(color: Colors.black)
            ),
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(
                color: Colors.black
            ),
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: Consumer<ResultSurveyChangeNotifier>(
              builder: (context, value, _) {
                return Form(
                  onWillPop: value.onBackPressResultSurveyGroupLocation,
                  key: value.keyResultSurveyGroupLocation,
                  child: Column(
                    children: [
                      DropdownButtonFormField<SurveyTypeModel>(
                          autovalidate: value.autoValidateResultSurveyGroupLocation,
                          validator: (e) {
                            if (e == null) {
                              return "Silahkan pilih jenis identitas";
                            } else {
                              return null;
                            }
                          },
                          value: value.surveyTypeSelected,
                          onChanged: (data) {
                            value.surveyTypeSelected = data;
                          },
                          onTap: () {
                            FocusManager.instance.primaryFocus.unfocus();
                          },
                          decoration: InputDecoration(
                            labelText: "Jenis Survey",
                            border: OutlineInputBorder(),
                            contentPadding:
                            EdgeInsets.symmetric(horizontal: 10),
                          ),
                          items: value
                              .listSurveyType
                              .map((value) {
                            return DropdownMenuItem<SurveyTypeModel>(
                              value: value,
                              child: Text(
                                value.name,
                                overflow: TextOverflow.ellipsis,
                              ),
                            );
                          }).toList()
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          autovalidate: value.autoValidateResultSurveyGroupLocation,
                          controller: value.controllerDistanceLocationWithSentraUnit,
                          decoration: new InputDecoration(
                              labelText: 'Jarak Lokasi Dengan Sentra/Unit (Km)',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))
                          )
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          autovalidate: value.autoValidateResultSurveyGroupLocation,
                          controller: value.controllerDistanceLocationWithDealerMerchantAXI,
                          decoration: new InputDecoration(
                              labelText: 'Jarak Lokasi Dengan Dealer/Merchant/AXI (Km)',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))
                          )
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          autovalidate: value.autoValidateResultSurveyGroupLocation,
                          controller: value.controllerDistanceLocationWithUsageObjectWithSentraUnit,
                          decoration: new InputDecoration(
                              labelText: 'Jarak Lokasi Penggunaan Objek dengan Sentra/Unit (Km)',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))
                          )
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Consumer<ResultSurveyChangeNotifier>(
                builder: (context, resultSurveyChangeNotifier, _) {
                  return RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                        resultSurveyChangeNotifier.checkResultSurveyGroupLocation(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("DONE",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25))
                        ],
                      ));
                },
              )),
        ),
      ),
    );
  }
}
