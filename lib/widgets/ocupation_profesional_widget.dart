import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_business_field.dart';
import 'package:ad1ms2_dev/screens/form_m/search_sector_economi.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class OccupationProfesionalWidget extends StatefulWidget {
  @override
  _OccupationProfesionalWidgetState createState() =>
      _OccupationProfesionalWidgetState();
}

class _OccupationProfesionalWidgetState
    extends State<OccupationProfesionalWidget> {

  @override
  void initState() {
    super.initState();
    if(Provider.of<FormMOccupationChangeNotif>(context,listen: false).listBusinessLocation.isEmpty)
    Provider.of<FormMOccupationChangeNotif>(context,listen: false).getBusinessLocation();
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMOccupationChangeNotif>(
      builder: (context, formMOccupationChangeNotif, _) {
        return Column(
          children: [
//            DropdownButtonFormField<ProfessionTypeModel>(
//                autovalidate: formMOccupationChangeNotif.autoValidate,
//                validator: (e) {
//                  if (e == null) {
//                    return "Silahkan pilih pekerjaan";
//                  } else {
//                    return null;
//                  }
//                },
//                value: formMOccupationChangeNotif.professionTypeModelSelected,
//                onChanged: (value) {
//                  formMOccupationChangeNotif.professionTypeModelSelected =
//                      value;
//                },
//                onTap: () {
//                  FocusManager.instance.primaryFocus.unfocus();
//                },
//                decoration: InputDecoration(
//                  labelText: "Jenis Profesi",
//                  border: OutlineInputBorder(),
//                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                ),
//                items:
//                    formMOccupationChangeNotif.listProfessionType.map((value) {
//                  return DropdownMenuItem<ProfessionTypeModel>(
//                    value: value,
//                    child: Text(
//                      value.desc,
//                      overflow: TextOverflow.ellipsis,
//                    ),
//                  );
//                }).toList()),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              autovalidate: formMOccupationChangeNotif.autoValidate,
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              readOnly: true,
              onTap: () {
                FocusManager.instance.primaryFocus.unfocus();
                formMOccupationChangeNotif.searchSectorEconomic(context);
              },
              controller:
              formMOccupationChangeNotif.controllerSectorEconomic,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                  labelText: 'Sektor Ekonomi',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            TextFormField(
//              autovalidate: formMOccupationChangeNotif.autoValidate,
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                } else {
//                  return null;
//                }
//              },
//              readOnly: true,
//              onTap: () {
//                FocusManager.instance.primaryFocus.unfocus();
//                formMOccupationChangeNotif.searchBusinesField(context);
//              },
//              controller:
//              formMOccupationChangeNotif.controllerBusinessField,
//              style: new TextStyle(color: Colors.black),
//              decoration: new InputDecoration(
//                  labelText: 'Lapangan Usaha',
//                  labelStyle: TextStyle(color: Colors.black),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(8))),
//              enabled: formMOccupationChangeNotif.controllerSectorEconomic.text == "" ? false : true,
//            ),
//            SizedBox(height: MediaQuery.of(context).size.height / 47),
//            DropdownButtonFormField<StatusLocationModel>(
//                autovalidate: formMOccupationChangeNotif.autoValidate,
//                validator: (e) {
//                  if (e == null) {
//                    return "Silahkan pilih pekerjaan";
//                  } else {
//                    return null;
//                  }
//                },
//                value: formMOccupationChangeNotif.statusLocationModelSelected,
//                onChanged: (value) {
//                  formMOccupationChangeNotif.statusLocationModelSelected =
//                      value;
//                },
//                onTap: () {
//                  FocusManager.instance.primaryFocus.unfocus();
//                },
//                decoration: InputDecoration(
//                  labelText: "Status Lokasi",
//                  border: OutlineInputBorder(),
//                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                ),
//                items:
//                    formMOccupationChangeNotif.listStatusLocation.map((value) {
//                  return DropdownMenuItem<StatusLocationModel>(
//                    value: value,
//                    child: Text(
//                      value.desc,
//                      overflow: TextOverflow.ellipsis,
//                    ),
//                  );
//                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            DropdownButtonFormField<BusinessLocationModel>(
//                autovalidate: formMOccupationChangeNotif.autoValidate,
//                validator: (e) {
//                  if (e == null) {
//                    return "Silahkan pilih pekerjaan";
//                  } else {
//                    return null;
//                  }
//                },
                value: formMOccupationChangeNotif.businessLocationModelSelected,
                onChanged: (value) {
                  formMOccupationChangeNotif.businessLocationModelSelected =
                      value;
                },
                onTap: () {
                  FocusManager.instance.primaryFocus.unfocus();
                },
                decoration: InputDecoration(
                  labelText: "Lokasi Usaha",
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                ),
                items: formMOccupationChangeNotif.listBusinessLocation
                    .map((value) {
                  return DropdownMenuItem<BusinessLocationModel>(
                    value: value,
                    child: Text(
                      value.desc,
                      overflow: TextOverflow.ellipsis,
                    ),
                  );
                }).toList()),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            TextFormField(
              autovalidate: formMOccupationChangeNotif.autoValidate,
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              controller:
                  formMOccupationChangeNotif.controllerTotalEstablishedDate,
              style: new TextStyle(color: Colors.black),
              decoration: new InputDecoration(
                  labelText: 'Total Lama Usaha/Kerja (bln)',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
              keyboardType: TextInputType.number,
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly,
                // LengthLimitingTextInputFormatter(10),
              ],
            ),
          ],
        );
      },
    );
  }
}
