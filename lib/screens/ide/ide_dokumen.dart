import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class IdeDokumen extends StatefulWidget {
  @override
  _IdeDokumenState createState() => _IdeDokumenState();
}

class _IdeDokumenState extends State<IdeDokumen> {
  File _imageFile;

  Future _getImage(ImageSource _source) async {
    var image = await ImagePicker.pickImage(source: _source);
    setState(() {
      _imageFile = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          _imageFile == null
              ? Container()
              : Container(
                  height: 800,
                  width: 600,
                  child: Image.file(_imageFile),
                ),
          SizedBox(
            height: 12.0,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton(
                onPressed: () {
                  _getImage(ImageSource.gallery);
                },
                heroTag: 'image0',
                tooltip: 'Pick Image from gallery',
                child: const Icon(Icons.photo_library),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: FloatingActionButton(
                  onPressed: () {
                    _getImage(ImageSource.camera);
                  },
                  heroTag: 'image1',
                  tooltip: 'Take a Photo',
                  child: const Icon(Icons.camera_alt),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
