import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_create_edit_asset.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_create_edit_survey_detail.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_group_location.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_group_note.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_photo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ResultSurvey extends StatefulWidget {
  @override
  _ResultSurveyState createState() => _ResultSurveyState();
}

class _ResultSurveyState extends State<ResultSurvey> {
  @override
  Widget build(BuildContext context) {
    var _provider = Provider.of<ResultSurveyChangeNotifier>(context, listen: true);
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
//        appBar: AppBar(
//          title: Text("Hasil Survey"),
//          backgroundColor: myPrimaryColor,
//          iconTheme: IconThemeData(color: Colors.black),
//        ),
        body: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 37,
            vertical: MediaQuery.of(context).size.height / 57,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WidgetResultGroupSurveyNotes()));
                },
                child: Card(
                  elevation: 3.3,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Catatan"),
                        _provider.flagResultSurveyGroupNotes
                            ? Icon(Icons.check, color: primaryOrange)
                            : SizedBox()
                      ],
                    ),
                  ),
                ),
              ),
              _provider.autoValidateResultSurveyGroupNote
                  ? Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 37),
                      child: Text("Data belum lengkap",
                          style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                  : SizedBox(),
              SizedBox(height: MediaQuery.of(context).size.height / 87),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WidgetResultSurveyGroupLocation()));
                },
                child: Card(
                  elevation: 3.3,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Lokasi"),
                        _provider.flagResultSurveyGroupLocation
                            ? Icon(Icons.check, color: primaryOrange)
                            : SizedBox()
                      ],
                    ),
                  ),
                ),
              ),
              _provider.autoValidateResultSurveyGroupLocation
                  ? Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 37),
                      child: Text("Data belum lengkap",
                          style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                  : SizedBox(),
              SizedBox(height: MediaQuery.of(context).size.height / 87),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WidgetResultSurveyCreateNewEditSurveyDetail()));
                },
                child: Card(
                  elevation: 3.3,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Survey Detail"),
                        _provider.flagResultSurveyCreateEditDetailSurvey
                            ? Icon(Icons.check, color: primaryOrange)
                            : SizedBox()
                      ],
                    ),
                  ),
                ),
              ),
              _provider.autoValidateResultSurveyCreateEditDetailSurvey
                  ? Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 37),
                      child: Text("Data belum lengkap",
                          style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                  : SizedBox(),
              SizedBox(height: MediaQuery.of(context).size.height / 87),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WidgetResultSurveyCreateEditAsset()));
                },
                child: Card(
                  elevation: 3.3,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Aset"),
                        _provider.flagResultSurveyAsset
                            ? Icon(Icons.check, color: primaryOrange)
                            : SizedBox()
                      ],
                    ),
                  ),
                ),
              ),
              _provider.autoValidateResultSurveyAssetModel
                  ?
              Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width / 37),
                child: Text("Data belum lengkap",
                    style: TextStyle(color: Colors.red, fontSize: 12)),
              )
                  :
              SizedBox(),
              SizedBox(height: MediaQuery.of(context).size.height / 87),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WidgetResultSurveyPhoto()));
                },
                child: Card(
                  elevation: 3.3,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Foto Survey"),
                        _provider.flagResultSurveyAsset
                            ? Icon(Icons.check, color: primaryOrange)
                            : SizedBox()
                      ],
                    ),
                  ),
                ),
              ),
              _provider.autoValidateResultSurveyPhoto
                  ?
              Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width / 37),
                child: Text("Data belum lengkap",
                    style: TextStyle(color: Colors.red, fontSize: 12)),
              )
                  :
              SizedBox(height: MediaQuery.of(context).size.height / 87),
            ],
          ),
        ),
      ),
    );
  }
}
