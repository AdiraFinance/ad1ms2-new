import 'package:ad1ms2_dev/screens/form_m/form_m_info_nasabah.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_alamat.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_keluarga(ibu).dart';
import 'package:ad1ms2_dev/screens/form_m/list_informasi_keluarga.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class MenuCustomerDetail extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: true);
        var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: true);
        var _providerInfoKeluargaIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: true);
        var _providerInfoKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: true);
        // _providerInfoNasabah.deleteSQLite();
        // _providerInfoAlamat.setDataFromSQLite("5");
        // _providerInfoAlamat.deleteSQLite("5");
        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMInfoNasabah()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Nasabah"),
                                            _providerInfoNasabah.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerInfoNasabah.autoValidate
                        ? Container(
                            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                            child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                        )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMInformasiAlamat()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Alamat"),
                                        _providerInfoAlamat.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerInfoAlamat.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            // _providerInfoKeluargaIbu.deleteSQLite();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMInformasiKeluargaIBU()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Keluarga (Ibu)"),
                                        _providerInfoKeluargaIbu.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerInfoKeluargaIbu.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            // _providerInfoKeluarga.deleteSQLite();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ListInfoKeluarga()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Keluarga"),
                                        _providerInfoKeluarga.listFormInfoKel.length != 0
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
//                    _providerInfoKeluarga.flag
//                        ? Container(
//                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
//                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
//                    )
//                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }
}
