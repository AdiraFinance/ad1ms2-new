import 'package:ad1ms2_dev/models/credit_subsidy_model.dart';
import 'package:ad1ms2_dev/models/cutting_method_model.dart';
import 'package:ad1ms2_dev/models/installment_index_model.dart';
import 'package:ad1ms2_dev/models/subsidy_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddInfoCreditSubsidy extends StatefulWidget {
    final int flag;
    final CreditSubsidyModel formMInfoCreditSubsidyModel;
    final int index;
    const AddInfoCreditSubsidy({Key key, this.flag, this.formMInfoCreditSubsidyModel, this.index}) : super(key: key);

    @override
    _AddInfoCreditSubsidyState createState() => _AddInfoCreditSubsidyState();
}

class _AddInfoCreditSubsidyState extends State<AddInfoCreditSubsidy> {
    Future<void> _loadData;
    @override
    void initState() {
        super.initState();
        // int _periodTime = int.parse(Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false).periodOfTimeSelected);
        // if (widget.flag != 0) {
        _loadData = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false)
                    .setValueForEditInfoCreditSubsidy(widget.formMInfoCreditSubsidyModel, widget.flag, 5);
        // }
    }
    @override
    Widget build(BuildContext context) {
        var _size = MediaQuery.of(context).size;
        return Theme(
            data: ThemeData(
                primaryColor: Colors.black,
                accentColor: myPrimaryColor,
                fontFamily: "NunitoSans",
                primarySwatch: Colors.yellow),
            child: Scaffold(
                appBar: AppBar(
                    backgroundColor: myPrimaryColor,
                    title: Text(
                        widget.flag == 0 ? "Tambah Info Kredit Subsidi" : "Edit Info Kredit Subsidi",
                        style: TextStyle(color: Colors.black)),
                    centerTitle: true,
                    iconTheme: IconThemeData(
                        color: Colors.black,
                    ),
                ),
                body: SingleChildScrollView(
                    padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width / 27,
                        vertical: MediaQuery.of(context).size.height / 57),
                    child:
//                     widget.flag == 0
//                         ?
//                     Consumer<AddCreditSubsidyChangeNotifier>(
//                         builder: (consumerContext, addCreditSubsidyChangeNotifier, _) {
//                             return Form(
//                                 onWillPop: _onWillPop,
//                                 key: addCreditSubsidyChangeNotifier.key,
//                                 child: Column(
//                                     crossAxisAlignment: CrossAxisAlignment.start,
//                                     children: [
//                                         Text(
//                                             "Pemberi",
//                                             style: TextStyle(
//                                                 fontSize: 16,
//                                                 fontWeight: FontWeight.w700,
//                                                 letterSpacing: 0.15),
//                                         ),
//                                         Row(
//                                             children: [
//                                                 Radio(
//                                                     value: 1,
//                                                     groupValue: addCreditSubsidyChangeNotifier
//                                                         .radioValueGiver,
//                                                     onChanged: (value) {
//                                                         addCreditSubsidyChangeNotifier
//                                                             .radioValueGiver = value;
//                                                     }),
//                                                 Text("Adira")
//                                             ],
//                                         ),
//                                         Row(
//                                             children: [
//                                                 Radio(
//                                                     value: 2,
//                                                     groupValue: addCreditSubsidyChangeNotifier
//                                                         .radioValueGiver,
//                                                     onChanged: (value) {
//                                                         addCreditSubsidyChangeNotifier
//                                                             .radioValueGiver = value;
//                                                     }),
//                                                 Text("Dealer/Merchant/AXI/Pemasok")
//                                             ],
//                                         ),
//                                         SizedBox(height: _size.height / 47),
//                                         DropdownButtonFormField<SubsidyTypeModel>(
//                                             autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
//                                             validator: (e) {
//                                                 if (e == null) {
//                                                     return "Tidak boleh kosong";
//                                                 } else {
//                                                     return null;
//                                                 }
//                                             },
//                                             value: addCreditSubsidyChangeNotifier.typeSelected,
//                                             onChanged: (value) {
//                                                 addCreditSubsidyChangeNotifier.typeSelected = value;
//                                             },
//                                             decoration: InputDecoration(
//                                                 labelText: "Jenis",
//                                                 border: OutlineInputBorder(),
//                                                 contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                                             ),
//                                             items: addCreditSubsidyChangeNotifier.listType.map((value) {
//                                                 return DropdownMenuItem<SubsidyTypeModel>(
//                                                     value: value,
//                                                     child: Text(
//                                                         value.text,
//                                                         overflow: TextOverflow.ellipsis,
//                                                     ),
//                                                 );
//                                             }).toList(),
//                                         ),
//                                         SizedBox(height: _size.height / 47),
//                                         DropdownButtonFormField<CuttingMethodModel>(
//                                             autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
//                                             validator: (e) {
//                                                 if (e == null) {
//                                                     return "Tidak boleh kosong";
//                                                 } else {
//                                                     return null;
//                                                 }
//                                             },
//                                             value: addCreditSubsidyChangeNotifier.cuttingMethodSelected,
//                                             onChanged: (value) {
//                                                 addCreditSubsidyChangeNotifier.cuttingMethodSelected = value;
//                                             },
//                                             decoration: InputDecoration(
//                                                 labelText: "Metode Pemotongan",
//                                                 border: OutlineInputBorder(),
//                                                 contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                                             ),
//                                             items: addCreditSubsidyChangeNotifier.listCuttingMethod.map((value) {
//                                                 return DropdownMenuItem<CuttingMethodModel>(
//                                                     value: value,
//                                                     child: Text(
//                                                         value.text,
//                                                         overflow: TextOverflow.ellipsis,
//                                                     ),
//                                                 );
//                                             }).toList(),
//                                         ),
//                                         SizedBox(height: _size.height / 47),
//                                         TextFormField(
//                                             controller: addCreditSubsidyChangeNotifier
//                                                 .controllerValue,
//                                             style: new TextStyle(color: Colors.black),
//                                             decoration: new InputDecoration(
//                                                 labelText: 'Nilai Potongan Pencarian',
//                                                 labelStyle: TextStyle(color: Colors.black),
//                                                 border: OutlineInputBorder(
//                                                     borderRadius: BorderRadius.circular(8))),
//                                             keyboardType: TextInputType.number,
//                                             textCapitalization: TextCapitalization.characters,
//                                             inputFormatters: [
//                                                 WhitelistingTextInputFormatter.digitsOnly
//                                             ],
//                                             autovalidate:
//                                             addCreditSubsidyChangeNotifier.autoValidate,
//                                             validator: (e) {
//                                                 if (e.isEmpty) {
//                                                     return "Tidak boleh kosong";
//                                                 } else {
//                                                     return null;
//                                                 }
//                                             },
//                                         ),
//                                         SizedBox(height: _size.height / 47),
//                                         Visibility(
//                                           visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
//                                           child: TextFormField(
//                                               readOnly: true,
//                                               controller: addCreditSubsidyChangeNotifier
//                                                   .controllerClaimValue,
//                                               style: new TextStyle(color: Colors.black),
//                                               decoration: new InputDecoration(
//                                                   labelText: 'Nilai Klaim',
//                                                   labelStyle: TextStyle(color: Colors.black),
//                                                   border: OutlineInputBorder(
//                                                       borderRadius: BorderRadius.circular(8))),
//                                               keyboardType: TextInputType.number,
//                                               inputFormatters: [
//                                                   WhitelistingTextInputFormatter.digitsOnly
//                                               ],
//                                               textCapitalization: TextCapitalization.characters,
// //                                            autovalidate:
// //                                            addCreditSubsidyChangeNotifier.autoValidate,
// //                                            validator: (e) {
// //                                                if (e.isEmpty) {
// //                                                    return "Tidak boleh kosong";
// //                                                } else {
// //                                                    return null;
// //                                                }
// //                                            },
//                                           ),
//                                         ),
//                                         Visibility(
//                                             visible: addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
//                                             child: SizedBox(height: _size.height / 47)
//                                         ),
//                                         Visibility(
//                                           visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
//                                           child: TextFormField(
//                                               readOnly: true,
//                                               controller: addCreditSubsidyChangeNotifier
//                                                   .controllerRateBeforeEff,
//                                               style: new TextStyle(color: Colors.black),
//                                               decoration: new InputDecoration(
//                                                   labelText: 'Sukuk Bunga Sebelum (Eff)',
//                                                   labelStyle: TextStyle(color: Colors.black),
//                                                   border: OutlineInputBorder(
//                                                       borderRadius: BorderRadius.circular(8))),
//                                               keyboardType: TextInputType.number,
//                                               inputFormatters: [
//                                                   WhitelistingTextInputFormatter.digitsOnly
//                                               ],
//                                               textCapitalization: TextCapitalization.characters,
// //                                            autovalidate:
// //                                            addCreditSubsidyChangeNotifier.autoValidate,
// //                                            validator: (e) {
// //                                                if (e.isEmpty) {
// //                                                    return "Tidak boleh kosong";
// //                                                } else {
// //                                                    return null;
// //                                                }
// //                                            },
//                                           ),
//                                         ),
//                                         Visibility(
//                                             visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
//                                             child: SizedBox(height: _size.height / 47)
//                                         ),
//                                         Visibility(
//                                           visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
//                                           child: TextFormField(
//                                               readOnly: true,
//                                               controller: addCreditSubsidyChangeNotifier
//                                                   .controllerRateBeforeFlat,
//                                               style: new TextStyle(color: Colors.black),
//                                               decoration: new InputDecoration(
//                                                   labelText: 'Sukuk Bunga Sebelum (Flat)',
//                                                   labelStyle: TextStyle(color: Colors.black),
//                                                   border: OutlineInputBorder(
//                                                       borderRadius: BorderRadius.circular(8))),
//                                               keyboardType: TextInputType.number,
//                                               inputFormatters: [
//                                                   WhitelistingTextInputFormatter.digitsOnly
//                                               ],
//                                               textCapitalization: TextCapitalization.characters,
// //                                            autovalidate:
// //                                            addCreditSubsidyChangeNotifier.autoValidate,
// //                                            validator: (e) {
// //                                                if (e.isEmpty) {
// //                                                    return "Tidak boleh kosong";
// //                                                } else {
// //                                                    return null;
// //                                                }
// //                                            },
//                                           ),
//                                         ),
//                                         Visibility(
//                                             visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
//                                             child: SizedBox(height: _size.height / 47)
//                                         ),
//                                         Visibility(
//                                           visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
//                                           child: TextFormField(
//                                               controller: addCreditSubsidyChangeNotifier
//                                                   .controllerTotalInstallment,
//                                               style: new TextStyle(color: Colors.black),
//                                               decoration: new InputDecoration(
//                                                   labelText: 'Jumlah Angsuran',
//                                                   labelStyle: TextStyle(color: Colors.black),
//                                                   border: OutlineInputBorder(
//                                                       borderRadius: BorderRadius.circular(8))),
//                                               keyboardType: TextInputType.number,
//                                               textCapitalization: TextCapitalization.characters,
//                                               inputFormatters: [
//                                                   WhitelistingTextInputFormatter.digitsOnly
//                                               ],
//                                               autovalidate:
//                                               addCreditSubsidyChangeNotifier.autoValidate,
//                                               validator: (e) {
//                                                   if (e.isEmpty) {
//                                                       return "Tidak boleh kosong";
//                                                   } else {
//                                                       return null;
//                                                   }
//                                               },
//                                           ),
//                                         ),
//                                         Visibility(
//                                             visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
//                                             child: SizedBox(height: _size.height / 47)
//                                         ),
//                                         Visibility(
//                                           visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
//                                           child: Row(
//                                             children: [
//                                               Expanded(
//                                                 flex: 4,
//                                                 child: DropdownButtonFormField<InstalmentIndexModel>(
//                                                     autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
//                                                     validator: (e) {
//                                                         if (e == null) {
//                                                             return "Tidak boleh kosong";
//                                                         } else {
//                                                             return null;
//                                                         }
//                                                     },
//                                                     value: addCreditSubsidyChangeNotifier.installmentIndexSelected,
//                                                     onChanged: (value) {
//                                                         addCreditSubsidyChangeNotifier.installmentIndexSelected = value;
//                                                     },
//                                                     decoration: InputDecoration(
//                                                         labelText: "Angsuran Ke",
//                                                         border: OutlineInputBorder(),
//                                                         contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                                                     ),
//                                                     items: addCreditSubsidyChangeNotifier.listInstallmentIndexSelected.map((value) {
//                                                         return DropdownMenuItem<InstalmentIndexModel>(
//                                                             value: value,
//                                                             child: Text(
//                                                                 value.text,
//                                                                 overflow: TextOverflow.ellipsis,
//                                                             ),
//                                                         );
//                                                     }).toList(),
//                                                 ),
//                                               ),
//                                               SizedBox(width: _size.height / 47),
//                                               Expanded(
//                                                     flex: 1,
//                                                     child: RaisedButton(
//                                                         onPressed: (){
//                                                             addCreditSubsidyChangeNotifier.addDetailInstallment(context);
//                                                         },
//                                                         shape: RoundedRectangleBorder(
//                                                             borderRadius: new BorderRadius.circular(8.0)),
//                                                         color: myPrimaryColor,
//                                                         child: Text("Add"),
//                                                     ),
//                                                 )
//                                             ],
//                                           ),
//                                         ),
//                                         Visibility(
//                                             visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
//                                             child: SizedBox(height: _size.height / 47)
//                                         ),
//                                         Visibility(
//                                           visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
//                                           child: Row(
//                                               children: [
//                                                   Expanded(
//                                                       flex: 2,
//                                                       child: Text("Angsuran Ke "),
//                                                   ),
//                                                   SizedBox(width: _size.height / 47),
//                                                   Expanded(
//                                                       flex: 2,
//                                                       child: Text("Subsidi Angsuran "),
//                                                   ),
//                                                   SizedBox(width: _size.height / 47),
//                                                   Expanded(
//                                                       flex: 1,
//                                                       child: Text(""),
//                                                   ),
//                                               ],
//                                           ),
//                                         ),
//                                         Visibility(
//                                             visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
//                                             child: Divider(color: Colors.black)
//                                         ),
//                                         Visibility(
//                                           visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
//                                           child: Container(
//                                               margin: EdgeInsets.only(
//                                                   bottom: MediaQuery.of(context).size.height/77),
//                                               child: Column(
//                                                   crossAxisAlignment:CrossAxisAlignment.center,
//                                                   children: listDetail()),
//                                           ),
//                                         ),
//                                     ],
//                                 ),
//                             );
//                         },
//                     )
//                         :
                    FutureBuilder(
                        future: _loadData,
                        builder: (context, snapshot) {
                            if (snapshot.connectionState == ConnectionState.waiting) {
                                return Center(
                                    child: CircularProgressIndicator(),
                                );
                            }
                            return Consumer<AddCreditSubsidyChangeNotifier>(
                                builder:
                                    (consumerContext, addCreditSubsidyChangeNotifier, _) {
                                        return Form(
                                            // onWillPop: _onWillPop,
                                            key: addCreditSubsidyChangeNotifier.key,
                                            child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                    Text(
                                                        "Pemberi",
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            fontWeight: FontWeight.w700,
                                                            letterSpacing: 0.15),
                                                    ),
                                                    Row(
                                                        children: [
                                                            Radio(
                                                                activeColor: primaryOrange,
                                                                value: "0",
                                                                groupValue: addCreditSubsidyChangeNotifier
                                                                    .radioValueGiver,
                                                                onChanged: (value) {
                                                                    addCreditSubsidyChangeNotifier
                                                                        .radioValueGiver = value;
                                                                    addCreditSubsidyChangeNotifier.getListType(null, 0);
                                                                }),
                                                            Text("Adira")
                                                        ],
                                                    ),
                                                    Row(
                                                        children: [
                                                            Radio(
                                                                activeColor: primaryOrange,
                                                                value: "1",
                                                                groupValue: addCreditSubsidyChangeNotifier
                                                                    .radioValueGiver,
                                                                onChanged: (value) {
                                                                    addCreditSubsidyChangeNotifier
                                                                        .radioValueGiver = value;
                                                                    addCreditSubsidyChangeNotifier.getListType(null, 0);
                                                                }),
                                                            Text("Dealer/Merchant/AXI/Pemasok")
                                                        ],
                                                    ),
                                                    SizedBox(height: _size.height / 47),
                                                    DropdownButtonFormField<SubsidyTypeModel>(
                                                        autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                        validator: (e) {
                                                            if (e == null) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                        value: addCreditSubsidyChangeNotifier.typeSelected,
                                                        onChanged: (value) {
                                                            addCreditSubsidyChangeNotifier.typeSelected = value;
                                                        },
                                                        decoration: InputDecoration(
                                                            labelText: "Jenis",
                                                            border: OutlineInputBorder(),
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                        ),
                                                        items: addCreditSubsidyChangeNotifier.listType.map((value) {
                                                            return DropdownMenuItem<SubsidyTypeModel>(
                                                                value: value,
                                                                child: Text(
                                                                    value.text,
                                                                    overflow: TextOverflow.ellipsis,
                                                                ),
                                                            );
                                                        }).toList(),
                                                    ),
                                                    SizedBox(height: _size.height / 47),
                                                    DropdownButtonFormField<CuttingMethodModel>(
                                                        autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                        validator: (e) {
                                                            if (e == null) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                        value: addCreditSubsidyChangeNotifier.cuttingMethodSelected,
                                                        onChanged: (value) {
                                                            addCreditSubsidyChangeNotifier.cuttingMethodSelected = value;
                                                        },
                                                        decoration: InputDecoration(
                                                            labelText: "Metode Pemotongan",
                                                            border: OutlineInputBorder(),
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                        ),
                                                        items: addCreditSubsidyChangeNotifier.listCuttingMethod.map((value) {
                                                            return DropdownMenuItem<CuttingMethodModel>(
                                                                value: value,
                                                                child: Text(
                                                                    value.text,
                                                                    overflow: TextOverflow.ellipsis,
                                                                ),
                                                            );
                                                        }).toList(),
                                                    ),
                                                    SizedBox(height: _size.height / 47),
                                                    TextFormField(
                                                        controller: addCreditSubsidyChangeNotifier
                                                            .controllerValue,
                                                        style: new TextStyle(color: Colors.black),
                                                        decoration: new InputDecoration(
                                                            labelText: 'Nilai Potongan Pencarian',
                                                            labelStyle: TextStyle(color: Colors.black),
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(8))),
                                                        keyboardType: TextInputType.number,
                                                        textAlign: TextAlign.end,
                                                        inputFormatters: [
                                                            DecimalTextInputFormatter(decimalRange: 2),
                                                            addCreditSubsidyChangeNotifier.amountValidator
                                                        ],
                                                        onFieldSubmitted: (value){
                                                            addCreditSubsidyChangeNotifier.controllerValue.text = addCreditSubsidyChangeNotifier.formatCurrency.formatCurrency(value);
                                                        },
                                                        autovalidate:
                                                        addCreditSubsidyChangeNotifier.autoValidate,
                                                        validator: (e) {
                                                            if (e.isEmpty) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                    ),
                                                    SizedBox(height: _size.height / 47),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
                                                        child: TextFormField(
                                                            enabled: false,
                                                            controller: addCreditSubsidyChangeNotifier
                                                                .controllerClaimValue,
                                                            style: new TextStyle(color: Colors.black),
                                                            decoration: new InputDecoration(
                                                                labelText: 'Nilai Klaim',
                                                                labelStyle: TextStyle(color: Colors.black),
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(8))),
                                                            keyboardType: TextInputType.number,
                                                            textAlign: TextAlign.end,
                                                            inputFormatters: [
                                                                DecimalTextInputFormatter(decimalRange: 2),
                                                                addCreditSubsidyChangeNotifier.amountValidator
                                                            ],
                                                            onFieldSubmitted: (value){
                                                                addCreditSubsidyChangeNotifier.controllerClaimValue.text = addCreditSubsidyChangeNotifier.formatCurrency.formatCurrency(value);
                                                            },
//                                            autovalidate:
//                                            addCreditSubsidyChangeNotifier.autoValidate,
//                                            validator: (e) {
//                                                if (e.isEmpty) {
//                                                    return "Tidak boleh kosong";
//                                                } else {
//                                                    return null;
//                                                }
//                                            },
                                                        ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
                                                        child: TextFormField(
                                                            enabled: false,
                                                            controller: addCreditSubsidyChangeNotifier
                                                                .controllerRateBeforeEff,
                                                            style: new TextStyle(color: Colors.black),
                                                            decoration: new InputDecoration(
                                                                labelText: 'Sukuk Bunga Sebelum (Eff)',
                                                                labelStyle: TextStyle(color: Colors.black),
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(8))),
                                                            keyboardType: TextInputType.number,
                                                            inputFormatters: [
                                                                WhitelistingTextInputFormatter.digitsOnly
                                                            ],
                                                            textCapitalization: TextCapitalization.characters,
//                                            autovalidate:
//                                            addCreditSubsidyChangeNotifier.autoValidate,
//                                            validator: (e) {
//                                                if (e.isEmpty) {
//                                                    return "Tidak boleh kosong";
//                                                } else {
//                                                    return null;
//                                                }
//                                            },
                                                        ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
                                                        child: TextFormField(
                                                            enabled: false,
                                                            controller: addCreditSubsidyChangeNotifier
                                                                .controllerRateBeforeFlat,
                                                            style: new TextStyle(color: Colors.black),
                                                            decoration: new InputDecoration(
                                                                labelText: 'Sukuk Bunga Sebelum (Flat)',
                                                                labelStyle: TextStyle(color: Colors.black),
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(8))),
                                                            keyboardType: TextInputType.number,
                                                            inputFormatters: [
                                                                WhitelistingTextInputFormatter.digitsOnly
                                                            ],
                                                            textCapitalization: TextCapitalization.characters,
//                                            autovalidate:
//                                            addCreditSubsidyChangeNotifier.autoValidate,
//                                            validator: (e) {
//                                                if (e.isEmpty) {
//                                                    return "Tidak boleh kosong";
//                                                } else {
//                                                    return null;
//                                                }
//                                            },
                                                        ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? false : true,
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: TextFormField(
                                                            controller: addCreditSubsidyChangeNotifier
                                                                .controllerTotalInstallment,
                                                            style: new TextStyle(color: Colors.black),
                                                            decoration: new InputDecoration(
                                                                labelText: 'Jumlah Angsuran',
                                                                labelStyle: TextStyle(color: Colors.black),
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(8))),
                                                            keyboardType: TextInputType.number,
                                                            textAlign: TextAlign.end,
                                                            inputFormatters: [
                                                                DecimalTextInputFormatter(decimalRange: 2),
                                                                addCreditSubsidyChangeNotifier.amountValidator
                                                            ],
                                                            onFieldSubmitted: (value){
                                                                addCreditSubsidyChangeNotifier.controllerTotalInstallment.text = addCreditSubsidyChangeNotifier.formatCurrency.formatCurrency(value);
                                                            },
                                                            autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                            // validator: (e) {
                                                            //     if (e.isEmpty) {
                                                            //         return "Tidak boleh kosong";
                                                            //     } else {
                                                            //         return null;
                                                            //     }
                                                            // },
                                                        ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: Row(
                                                            children: [
                                                                Expanded(
                                                                    flex: 4,
                                                                    child: DropdownButtonFormField<String>(
                                                                        autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                                        // validator: (e) {
                                                                        //     if (e == null) {
                                                                        //         return "Tidak boleh kosong";
                                                                        //     } else {
                                                                        //         return null;
                                                                        //     }
                                                                        // },
                                                                        value: addCreditSubsidyChangeNotifier.installmentIndexSelected,
                                                                        onChanged: (value) {
                                                                            addCreditSubsidyChangeNotifier.installmentIndexSelected = value;
                                                                        },
                                                                        decoration: InputDecoration(
                                                                            labelText: "Angsuran Ke",
                                                                            border: OutlineInputBorder(),
                                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                        ),
                                                                        items: addCreditSubsidyChangeNotifier.listInstallmentIndexSelected.map((value) {
                                                                            return DropdownMenuItem<String>(
                                                                                value: value,
                                                                                child: Text(
                                                                                    value,
                                                                                    overflow: TextOverflow.ellipsis,
                                                                                ),
                                                                            );
                                                                        }).toList(),
                                                                    ),
                                                                ),
                                                                SizedBox(width: _size.height / 47),
                                                                Expanded(
                                                                    flex: 1,
                                                                    child: RaisedButton(
                                                                        onPressed: (){
                                                                            addCreditSubsidyChangeNotifier.addDetailInstallment(context);
                                                                        },
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius: new BorderRadius.circular(8.0)),
                                                                        color: myPrimaryColor,
                                                                        child: Text("Add"),
                                                                    ),
                                                                )
                                                            ],
                                                        ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: Row(
                                                            children: [
                                                                Expanded(
                                                                    flex: 2,
                                                                    child: Text("Angsuran Ke "),
                                                                ),
                                                                SizedBox(width: _size.height / 47),
                                                                Expanded(
                                                                    flex: 2,
                                                                    child: Text("Subsidi Angsuran "),
                                                                ),
                                                                SizedBox(width: _size.height / 47),
                                                                Expanded(
                                                                    flex: 1,
                                                                    child: Text(""),
                                                                ),
                                                            ],
                                                        ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: Divider(color: Colors.black)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: Container(
                                                            margin: EdgeInsets.only(
                                                                bottom: MediaQuery.of(context).size.height/77),
                                                            child: Column(
                                                                crossAxisAlignment:CrossAxisAlignment.center,
                                                                children: listDetail()),
                                                        ),
                                                    ),
                                                ],
                                            ),
                                        );
                                },
                            );
                        }),
                ),
                bottomNavigationBar: BottomAppBar(
                    elevation: 0.0,
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Consumer<AddCreditSubsidyChangeNotifier>(
                            builder: (context, addCreditSubsidyChangeNotifier, _) {
                                return RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(8.0)),
                                    color: myPrimaryColor,
                                    onPressed: () {
                                        if (widget.flag == 0) {
                                            addCreditSubsidyChangeNotifier.check(
                                                context, widget.flag, null);
                                        } else {
                                            addCreditSubsidyChangeNotifier.check(
                                                context, widget.flag, widget.index);
                                        }
                                    },
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                            Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: 1.25))
                                        ],
                                    ));
                            },
                        )),
                ),
            ),
        );
    }

    List<Widget> listDetail(){
        var _size = MediaQuery.of(context).size;
        List<Widget> _temp = [];
        var _providerCreditSubsidy = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
        for(var i=0; i<_providerCreditSubsidy.listDetail.length; i++){
            _temp.add(
                Column(
                  children: [
                    Row(
                        children: [
                            Expanded(
                                flex: 2,
                                child: Text(_providerCreditSubsidy.listDetail[i].installmentIndex),
                            ),
                            SizedBox(width: _size.height / 47),
                            Expanded(
                                flex: 2,
                                child: Text(_providerCreditSubsidy.listDetail[i].installmentSubsidy),
                            ),
                            SizedBox(width: _size.height / 47),
                            Expanded(
                                flex: 1,
                                child: IconButton(
                                    icon: Icon(Icons.delete, color: Colors.redAccent),
                                    onPressed: () {
                                        _providerCreditSubsidy.deleteIndex(context, i);
                                    }),
                            ),
                        ],
                    ),
                    Divider(color: Colors.black)
                  ],
                ),
            );
        }
        return _temp;
    }

    Future<bool> _onWillPop() async {
        var _provider = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
        if (widget.flag == 0) {
            if (_provider.radioValueGiver != "0" ||
                _provider.typeSelected != null ||
                _provider.cuttingMethodSelected != null ||
                _provider.controllerValue.text != "") {
                return (await showDialog(
                    context: context,
                    builder: (myContext) => AlertDialog(
                        title: new Text('Warning'),
                        content: new Text('Simpan perubahan?'),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    widget.flag == 0
                                        ?
                                    _provider.check(context, widget.flag, null)
                                        :
                                    _provider.check(context, widget.flag, widget.index);
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya'),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                )) ??
                    false;
            } else {
                return true;
            }
        } else {
            if (widget.formMInfoCreditSubsidyModel.giver != _provider.radioValueGiver ||
                widget.formMInfoCreditSubsidyModel.type.id != _provider.typeSelected.id ||
                widget.formMInfoCreditSubsidyModel.cuttingMethod.id != _provider.cuttingMethodSelected.id ||
                widget.formMInfoCreditSubsidyModel.value != _provider.controllerValue.text) {
                return (await showDialog(
                    context: context,
                    builder: (myContext) => AlertDialog(
                        title: new Text('Warning'),
                        content: new Text('Simpan perubahan?'),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    _provider.check(context, widget.flag, widget.index);
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya'),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                )) ??
                    false;
            } else {
                return true;
            }
        }
    }
}
