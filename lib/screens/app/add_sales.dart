import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/employee_head_model.dart';
import 'package:ad1ms2_dev/models/infromation_sales_model.dart';
import 'package:ad1ms2_dev/models/position_model.dart';
import 'package:ad1ms2_dev/models/salesman_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_salesman_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddSales extends StatefulWidget {
  final int flag;
  final int index;
  final InformationSalesModel model;
  const AddSales({this.flag, this.model, this.index});
  @override
  _AddSalesState createState() => _AddSalesState();
}

class _AddSalesState extends State<AddSales> {
  Future<void> _setValueForEdit;
  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      _setValueForEdit =
          Provider.of<AddSalesmanChangeNotifier>(context, listen: false)
              .setValue(widget.model);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: Colors.black,
          primarySwatch: primaryOrange,
          accentColor: myPrimaryColor),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            "Tambah Salesman",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(16),
          child: widget.flag == 0
              ? Consumer<AddSalesmanChangeNotifier>(
                  builder: (context, addSalesmanChangeNotifier, _) {
                    return Form(
                      key: addSalesmanChangeNotifier.key,
                      onWillPop: _onWillPop,
                      child: Column(
                        children: [
                          DropdownButtonFormField<SalesmanTypeModel>(
                            isExpanded: true,
                            autovalidate:
                                addSalesmanChangeNotifier.autoValidate,
                            validator: (e) {
                              if (e == null) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            value: addSalesmanChangeNotifier
                                .salesmanTypeModelSelected,
                            onChanged: (value) {
                              addSalesmanChangeNotifier
                                  .salesmanTypeModelSelected = value;
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                              labelText: "Jenis Sales",
                              border: OutlineInputBorder(),
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: addSalesmanChangeNotifier.listSalesmanType
                                .map((value) {
                              return DropdownMenuItem<SalesmanTypeModel>(
                                value: value,
                                child: Text(
                                  value.salesmanType,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList(),
                          ),
                          SizedBox(
                              height: MediaQuery.of(context).size.height / 47),
                          DropdownButtonFormField<PositionModel>(
                            isExpanded: true,
                            autovalidate:
                                addSalesmanChangeNotifier.autoValidate,
                            validator: (e) {
                              if (e == null) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            value:
                                addSalesmanChangeNotifier.positionModelSelected,
                            onChanged: (value) {
                              addSalesmanChangeNotifier.positionModelSelected =
                                  value;
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                              labelText: "Jabatan",
                              border: OutlineInputBorder(),
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: addSalesmanChangeNotifier.listPosition
                                .map((value) {
                              return DropdownMenuItem<PositionModel>(
                                value: value,
                                child: Text(
                                  value.positionName,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList(),
                          ),
                          SizedBox(
                              height: MediaQuery.of(context).size.height / 47),
                          TextFormField(
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              readOnly: true,
                              onTap: () {
                                addSalesmanChangeNotifier
                                    .searchEmployee(context);
                              },
                              controller:
                                  addSalesmanChangeNotifier.controllerEmployee,
                              autovalidate:
                                  addSalesmanChangeNotifier.autoValidate,
                              decoration: new InputDecoration(
                                  labelText: 'Pegawai',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)))),
                          SizedBox(
                              height: MediaQuery.of(context).size.height / 47),
                          DropdownButtonFormField<EmployeeHeadModel>(
                            isExpanded: true,
                            autovalidate:
                                addSalesmanChangeNotifier.autoValidate,
                            validator: (e) {
                              if (e == null) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            value: addSalesmanChangeNotifier
                                .employeeHeadModelSelected,
                            onChanged: (value) {
                              addSalesmanChangeNotifier
                                  .employeeHeadModelSelected = value;
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                              labelText: "Atasan",
                              border: OutlineInputBorder(),
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: addSalesmanChangeNotifier.listEmployeeHead
                                .map((value) {
                              return DropdownMenuItem<EmployeeHeadModel>(
                                value: value,
                                child: Text(
                                  value.employeeHeadName,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList(),
                          ),
                          SizedBox(
                              height: MediaQuery.of(context).size.height / 47),
                        ],
                      ),
                    );
                  },
                )
              : FutureBuilder(
                  future: _setValueForEdit,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return Consumer<AddSalesmanChangeNotifier>(
                      builder: (context, addSalesmanChangeNotifier, _) {
                        return Form(
                          onWillPop: _onWillPop,
                          key: addSalesmanChangeNotifier.key,
                          child: Column(
                            children: [
                              DropdownButtonFormField<SalesmanTypeModel>(
                                isExpanded: true,
                                autovalidate:
                                    addSalesmanChangeNotifier.autoValidate,
                                validator: (e) {
                                  if (e == null) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                value: addSalesmanChangeNotifier
                                    .salesmanTypeModelSelected,
                                onChanged: (value) {
                                  addSalesmanChangeNotifier
                                      .salesmanTypeModelSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Sales",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: addSalesmanChangeNotifier
                                    .listSalesmanType
                                    .map((value) {
                                  return DropdownMenuItem<SalesmanTypeModel>(
                                    value: value,
                                    child: Text(
                                      value.salesmanType,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              DropdownButtonFormField<PositionModel>(
                                isExpanded: true,
                                autovalidate:
                                    addSalesmanChangeNotifier.autoValidate,
                                validator: (e) {
                                  if (e == null) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                value: addSalesmanChangeNotifier
                                    .positionModelSelected,
                                onChanged: (value) {
                                  addSalesmanChangeNotifier
                                      .positionModelSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jabatan",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: addSalesmanChangeNotifier.listPosition
                                    .map((value) {
                                  return DropdownMenuItem<PositionModel>(
                                    value: value,
                                    child: Text(
                                      value.positionName,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              TextFormField(
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  readOnly: true,
                                  onTap: () {
                                    addSalesmanChangeNotifier
                                        .searchEmployee(context);
                                  },
                                  controller: addSalesmanChangeNotifier
                                      .controllerEmployee,
                                  autovalidate:
                                      addSalesmanChangeNotifier.autoValidate,
                                  decoration: new InputDecoration(
                                      labelText: 'Pegawai',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8)))),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              DropdownButtonFormField<EmployeeHeadModel>(
                                isExpanded: true,
                                autovalidate:
                                    addSalesmanChangeNotifier.autoValidate,
                                validator: (e) {
                                  if (e == null) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                value: addSalesmanChangeNotifier
                                    .employeeHeadModelSelected,
                                onChanged: (value) {
                                  addSalesmanChangeNotifier
                                      .employeeHeadModelSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Atasan",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: addSalesmanChangeNotifier
                                    .listEmployeeHead
                                    .map((value) {
                                  return DropdownMenuItem<EmployeeHeadModel>(
                                    value: value,
                                    child: Text(
                                      value.employeeHeadName,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                            ],
                          ),
                        );
                      },
                    );
                  }),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: myPrimaryColor,
                onPressed: () {
                  if (widget.flag != 0) {
                    Provider.of<AddSalesmanChangeNotifier>(context,
                            listen: false)
                        .check(context, widget.flag, widget.index);
                  } else {
                    Provider.of<AddSalesmanChangeNotifier>(context,
                            listen: false)
                        .check(context, widget.flag, null);
                  }
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [Text(widget.flag == 0 ? "SAVE" : "UPDATE")])),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider =
        Provider.of<AddSalesmanChangeNotifier>(context, listen: false);
    if (widget.flag == 0) {
      if (_provider.salesmanTypeModelSelected != null ||
          _provider.positionModelSelected != null ||
          _provider.controllerEmployee.text != "" ||
          _provider.employeeHeadModelSelected != null) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dengan simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (_provider.salesmanTypeModelSelected.id !=
              _provider.salesmanTypeModelTemp.id ||
          _provider.employeeTemp != _provider.controllerEmployee.text ||
          _provider.positionModelSelected.id !=
              _provider.positionModeltemp.id ||
          _provider.employeeHeadModelSelected.id !=
              _provider.employeeHeadModelTemp.id) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dengan simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      print(widget.index);
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
  }
}
