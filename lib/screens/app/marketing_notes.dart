import 'package:ad1ms2_dev/shared/change_notifier_app/marketing_notes_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MarketingNotes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MarketingNotesChangeNotifier>(
      builder: (context, value, child) {
        return SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height / 37,
              horizontal: MediaQuery.of(context).size.width / 27),
          child: Column(
            children: [
              TextFormField(
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.characters,
                maxLines: 3,
                controller: value.controllerMarketingNotes,
                decoration: InputDecoration(
                    labelText: 'Marketing Notes',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)
                    )
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
