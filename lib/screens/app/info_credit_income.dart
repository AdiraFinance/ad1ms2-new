import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InfoCreditIncome extends StatefulWidget {
  @override
  _InfoCreditIncomeState createState() => _InfoCreditIncomeState();
}

class _InfoCreditIncomeState extends State<InfoCreditIncome> {
  @override
  void initState() {
    super.initState();
    Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).getDSR(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor,
        primaryColor: Colors.black
      ),
      child: Consumer<InfoCreditIncomeChangeNotifier>(
        builder: (context, value, child) {
          return Scaffold(
            appBar: AppBar(
                title:
                Text("Informasi Kredit Income", style: TextStyle(color: Colors.black)),
                centerTitle: true,
                backgroundColor: myPrimaryColor,
                iconTheme: IconThemeData(color: Colors.black),
            ),
            body: SingleChildScrollView(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 47,
                  horizontal: MediaQuery.of(context).size.width / 27),
              child: Column(
                children: [
                  TextFormField(
                      enabled: false,
                      keyboardType: TextInputType.number,
                      controller: value.controllerDebtComparison,
                      decoration: InputDecoration(
                          labelText: 'Perbandingan Hutang (DSR)%',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)
                          )
                      ),
                      textAlign: TextAlign.end,
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        DecimalTextInputFormatter(decimalRange: 2),
                        value.amountValidator
                      ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 57),
                  TextFormField(
                      enabled: false,
                      keyboardType: TextInputType.number,
                      controller: value.controllerIncomeComparison,
                      decoration: InputDecoration(
                          labelText: 'Perbandingan Pendapatan (DIR)%',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)
                          )
                      ),
                      textAlign: TextAlign.end,
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        DecimalTextInputFormatter(decimalRange: 2),
                        value.amountValidator
                      ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 57),
                  TextFormField(
                      enabled: false,
                      keyboardType: TextInputType.number,
                      controller: value.controllerDSC,
                      decoration: InputDecoration(
                          labelText: 'DSC',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)
                          )
                      ),
                      textAlign: TextAlign.end,
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        DecimalTextInputFormatter(decimalRange: 2),
                        value.amountValidator
                      ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 57),
                  TextFormField(
                      enabled: false,
                      keyboardType: TextInputType.number,
                      controller: value.controllerIRR,
                      decoration: InputDecoration(
                          labelText: 'IRR(%)',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)
                          )
                      ),
                      textAlign: TextAlign.end,
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        DecimalTextInputFormatter(decimalRange: 2),
                        value.amountValidator
                      ],
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
                  elevation: 0.0,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                              Navigator.pop(context);
//                        infoCreditStructureTypeInstallmentChangeNotifier.check(context);
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                  Text("DONE",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25))
                              ],
                          ))),
              ),
          );
        },
      ),
    );
  }
}
