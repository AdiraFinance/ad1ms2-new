import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/installment_type_model.dart';
import 'package:ad1ms2_dev/models/payment_method_model.dart';
import 'package:ad1ms2_dev/models/period_of_time_model.dart';
import 'package:ad1ms2_dev/screens/app/add_info_fee_credit_structure.dart';
import 'package:ad1ms2_dev/screens/app/list_info_fee_credit_structure.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InfoCreditStructure extends StatefulWidget {
  @override
  _InfoCreditStructureState createState() => _InfoCreditStructureState();
}

class _InfoCreditStructureState extends State<InfoCreditStructure> {

  @override
  void initState() {
    super.initState();
    if(Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).listPeriodOfTime.isEmpty){
      Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).getDataTenor(context);
    }
    Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).getDPFlag(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        accentColor: myPrimaryColor,
        primaryColor: Colors.black,
        fontFamily: "NunitoSans"
      ),
      child: Scaffold(
        key: Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
            title:
            Text("Informasi Struktur Kredit", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
        ),
        body:
        // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).loadData
        //     ?
        // Center(child: CircularProgressIndicator())
        //     :
        SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width/37,
            vertical: MediaQuery.of(context).size.height/57,
          ),
          child: Consumer<InfoCreditStructureChangeNotifier>(
            builder: (context,infoCreditStructureChangeNotifier,_){
              return Form(
                key: infoCreditStructureChangeNotifier.keyForm,
                onWillPop: infoCreditStructureChangeNotifier.onBackPress,
                child: Column(
                  children: [
                    DropdownButtonFormField<InstallmentTypeModel>(
                        autovalidate:
                        infoCreditStructureChangeNotifier.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih jenis identitas";
                          } else {
                            return null;
                          }
                        },
                        value: infoCreditStructureChangeNotifier
                            .installmentTypeSelected,
                        onChanged: (value) {
                          infoCreditStructureChangeNotifier
                              .installmentTypeSelected = value;
                          Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).clearData();
                          Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).setIsVisible(value.id);
                        },
                        decoration: InputDecoration(
                          labelText: "Jenis Angsuran",
                          border: OutlineInputBorder(),
                          contentPadding:
                          EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: infoCreditStructureChangeNotifier
                            .listInstallmentType
                            .map((value) {
                          return DropdownMenuItem<InstallmentTypeModel>(
                            value: value,
                            child: Text(
                              value.name,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList()),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    DropdownButtonFormField<String>(
                        autovalidate:
                        infoCreditStructureChangeNotifier.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih jangka waktu";
                          } else {
                            return null;
                          }
                        },
                        value: infoCreditStructureChangeNotifier
                            .periodOfTimeSelected,
                        onChanged: (value) {
                          infoCreditStructureChangeNotifier
                              .periodOfTimeSelected = value;
                          Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).totalSteppingSelected = null;
                          Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).listInfoCreditStructureStepping.clear();
                        },
                        decoration: InputDecoration(
                          labelText: "Jangka Waktu",
                          border: OutlineInputBorder(),
                          contentPadding:
                          EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: infoCreditStructureChangeNotifier
                            .listPeriodOfTime
                            .map((value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                              value,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList()),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    DropdownButtonFormField<PaymentMethodModel>(
                        autovalidate:
                        infoCreditStructureChangeNotifier.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih jenis identitas";
                          } else {
                            return null;
                          }
                        },
                        value: infoCreditStructureChangeNotifier
                            .paymentMethodSelected,
                        onChanged: (value) {
                          infoCreditStructureChangeNotifier
                              .paymentMethodSelected = value;
                        },
                        decoration: InputDecoration(
                          labelText: "Metode Pembayaran",
                          border: OutlineInputBorder(),
                          contentPadding:
                          EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: infoCreditStructureChangeNotifier
                            .listPaymentMethod
                            .map((value) {
                          return DropdownMenuItem<PaymentMethodModel>(
                            value: value,
                            child: Text(
                              value.name,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList()),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
//                _providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId == "1"
                  true
                        ?
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                        ],
                        keyboardType: TextInputType.number,
                        // readOnly: true,
                        // onTap: () {
                        //   // infoCreditStructureChangeNotifier.searchGroupObject(context);
                        //   infoCreditStructureChangeNotifier.calculateCredit(context, flag);
                        // },
                        onFieldSubmitted: (value){
                          // infoCreditStructureChangeNotifier.calculateCredit(context, "EFF_RATE");
                          infoCreditStructureChangeNotifier.getMinDP(context, "EFF_RATE");
                        },
                        controller: infoCreditStructureChangeNotifier.controllerInterestRateEffective,
                        autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Suku Bunga/Tahun (Eff)',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        ),
                        enabled: Provider.of<ListOidChangeNotifier>(context,listen: false).customerType != "COM"
                        ? Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "1"
                          ? infoCreditStructureChangeNotifier.installmentTypeSelected.id == "04" ? false : true
                          : false
                        : Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "1"
                          ? infoCreditStructureChangeNotifier.installmentTypeSelected.id == "04" ? false : true
                          : false
                        // infoCreditStructureChangeNotifier.installmentTypeSelected.id == "04" ? false : true
                      )
                        :
                    SizedBox(),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    infoCreditStructureChangeNotifier.installmentTypeSelected != null
                        ?
                    infoCreditStructureChangeNotifier.installmentTypeSelected.id != "09"
                        ?
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                        ],
                        keyboardType: TextInputType.number,
                        // readOnly: true,
                        // onTap: () {
                        //   // infoCreditStructureChangeNotifier.searchGroupObject(context);
                        //   // infoCreditStructureChangeNotifier.calculateCredit(context, flag);
                        // },
                        onFieldSubmitted: (value){
                          // infoCreditStructureChangeNotifier.calculateCredit(context, "FLAT_RATE");
                          infoCreditStructureChangeNotifier.getMinDP(context, "FLAT_RATE");
                        },
                        controller: infoCreditStructureChangeNotifier.controllerInterestRateFlat,
                        autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                        decoration: new InputDecoration(
                            labelText: true
//                        _providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId == "1"
                                ? 'Suku Bunga/Tahun (Flat)' : 'Margin',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        )
                    )
                        :
                    SizedBox()
                        :
                    SizedBox(),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: infoCreditStructureChangeNotifier.controllerObjectPrice,
                        autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCreditStructureChangeNotifier.amountValidator
                        ],
                        onFieldSubmitted: (value){
                          infoCreditStructureChangeNotifier.controllerObjectPrice.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                          infoCreditStructureChangeNotifier.getMinDP(context, "");
                        },
                        decoration: new InputDecoration(
                            labelText: 'Harga Objek',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        ),
                        onTap: () {
                          infoCreditStructureChangeNotifier.formatting();
                          infoCreditStructureChangeNotifier.getMinDP(context, "");
                        }
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        textAlign: TextAlign.end,
                        enabled: false,
                        controller: infoCreditStructureChangeNotifier.controllerKaroseriTotalPrice,
                        decoration: new InputDecoration(
                            labelText: 'Total Harga Karoseri',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        )
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        enabled: false,
                        controller: infoCreditStructureChangeNotifier.controllerTotalPrice,
                        decoration: new InputDecoration(
                            labelText: 'Total Harga',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        )
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        controller: infoCreditStructureChangeNotifier.controllerNetDP,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCreditStructureChangeNotifier.amountValidator
                        ],
                        onTap: () {
                          infoCreditStructureChangeNotifier.formatting();
                          infoCreditStructureChangeNotifier.getMinDP(context, "");
                        },
                        onFieldSubmitted: (value){
                          infoCreditStructureChangeNotifier.controllerNetDP.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                          infoCreditStructureChangeNotifier.getMinDP(context, "");
                        },
                        decoration: new InputDecoration(
                            labelText: 'Uang Muka (Net)',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        ),
                        enabled: infoCreditStructureChangeNotifier.dpFlag == "0" ? false : true,
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: infoCreditStructureChangeNotifier.controllerBranchDP,
                        autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCreditStructureChangeNotifier.amountValidator
                        ],
                        onTap: () {
                          infoCreditStructureChangeNotifier.formatting();
                        },
                        onFieldSubmitted: (value){
                          infoCreditStructureChangeNotifier.controllerBranchDP.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                        },
                        decoration: new InputDecoration(
                            labelText: 'Uang Muka (Cabang)',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCreditStructureChangeNotifier.amountValidator
                        ],
                        onTap: () {
                          infoCreditStructureChangeNotifier.formatting();
                          infoCreditStructureChangeNotifier.getMinDP(context, "");
                        },
                        onFieldSubmitted: (value){
                          infoCreditStructureChangeNotifier.controllerGrossDP.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                          infoCreditStructureChangeNotifier.getMinDP(context, "");

                        },
                        controller: infoCreditStructureChangeNotifier.controllerGrossDP,
                        autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Uang Muka (Gross)',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        ),
                        enabled: infoCreditStructureChangeNotifier.dpFlag == "1" ? false : true,
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        enabled: false,
                        controller: infoCreditStructureChangeNotifier.controllerTotalLoan,
                        decoration: new InputDecoration(
                            labelText: 'Jumlah Pinjaman',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        )
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        enabled: false,
                        controller: infoCreditStructureChangeNotifier.controllerInstallment,
                        decoration: new InputDecoration(
                            labelText: 'Angsuran',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        )
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        enabled: false,
                        controller: infoCreditStructureChangeNotifier.controllerLTV,
                        decoration: new InputDecoration(
                            labelText: 'LTV',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))
                        )
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ListInfoFeeCreditStructure()));
                      },
                      child: Card(
                        elevation: 3.3,
                        child: Padding(
                          padding: EdgeInsets.all(13.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Jumlah Info Struktur Kredit Biaya : ${infoCreditStructureChangeNotifier.listInfoFeeCreditStructure.length}"),
                              Icon(Icons.add_circle_outline, color: primaryOrange,)
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    infoCreditStructureChangeNotifier.autoValidateInfoFeeCreditStructure
                        ?
                    Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 37),
                      child: Text("Tidak boleh kosong",
                          style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        :
                    SizedBox(),
                  ],
                ),
              );
            },
          ),
        ),
        bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<InfoCreditStructureChangeNotifier>(
                      builder: (context, infoCreditStructureChangeNotifier, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                  infoCreditStructureChangeNotifier.check(context);
                              },
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                      Text("DONE",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: 1.25))
                                  ],
                              ));
                      },
                  )),
          ),
      ),
    );
  }
}
