
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'add_additional_insurence.dart';

class ListAdditionalInsurance extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
            ),
            child: Scaffold(
                appBar: AppBar(
                    title:
                    Text("List Asuransi Tambahan", style: TextStyle(color: Colors.black)),
                    centerTitle: true,
                    backgroundColor: myPrimaryColor,
                    iconTheme: IconThemeData(color: Colors.black),
                ),
                body: Consumer<InfoAdditionalInsuranceChangeNotifier>(
                    builder: (context, infoAdditionalInsuranceChangeNotifier, _) {
                        return Form(
                          onWillPop: infoAdditionalInsuranceChangeNotifier.onBackPress,
                          child: ListView.builder(
                              padding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height / 47,
                                  horizontal: MediaQuery.of(context).size.width / 37),
                              itemBuilder: (context, index) {
                                  return InkWell(
                                      onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => ChangeNotifierProvider(
                                                      create: (context) => FormMAddAdditionalInsuranceChangeNotifier(),
                                                      child: AddAdditionalInsurance(
                                                          flag: 1,
                                                          formMAdditionalInsurance: infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index],
                                                          index: index,
                                                      ))));
                                      },
                                      child: Card(
                                          elevation: 3.3,
                                          child: Stack(
                                              children: [
                                                  Padding(
                                                      padding: const EdgeInsets.all(12.0),
                                                      child: Column(
                                                          children: [
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text(
                                                                              "Tipe Asuransi"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].insuranceType.DESKRIPSI),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Perusahaan"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].company.text),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Produk"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].product.DESKRIPSI),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Biaya Tunai"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].priceCash),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Biaya Kredit"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].priceCredit),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Total Biaya"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].totalPrice),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                          ],
                                                      ),
                                                  ),
                                                  Align(
                                                      alignment: Alignment.topRight,
                                                      child: IconButton(
                                                          icon: Icon(Icons.delete, color: Colors.red),
                                                          onPressed: () {
                                                              infoAdditionalInsuranceChangeNotifier
                                                                  .deleteListInfoAdditionalInsurance(index);
                                                          }),
                                                  ),
                                              ],
                                          )),
                                  );
                              },
                              itemCount: infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance.length),
                        );
                    },
                ),
                floatingActionButton: FloatingActionButton(
                    onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                    create: (context) => FormMAddAdditionalInsuranceChangeNotifier(),
                                    child: AddAdditionalInsurance(
                                        flag: 0,
                                        formMAdditionalInsurance: null,
                                        index: null,
                                    ))));
                    },
                    child: Icon(Icons.add, color: Colors.black),
                    backgroundColor: myPrimaryColor,
                ),
            ),
        );
    }
}

