import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_floating_model.dart';
import 'package:ad1ms2_dev/models/installment_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_info_credit_structure_bp.dart';
import 'package:ad1ms2_dev/widgets/widget_info_credit_structure_floating.dart';
import 'package:ad1ms2_dev/widgets/widget_info_credit_structure_gp.dart';
import 'package:ad1ms2_dev/widgets/widget_info_credit_structure_irregular.dart';
import 'package:ad1ms2_dev/widgets/widget_info_credit_structure_stepping.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InfoCreditStructureTypeInstallment extends StatefulWidget {
  @override
  _InfoCreditStructureTypeInstallmentState createState() => _InfoCreditStructureTypeInstallmentState();
}

class _InfoCreditStructureTypeInstallmentState extends State<InfoCreditStructureTypeInstallment> {

  InstallmentTypeModel _typeInstallment;

  @override
  void initState() {
    super.initState();
    _typeInstallment = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false)
        .installmentTypeSelected;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _typeInstallment.id == "10"
          ?
      WidgetInfoCreditStructureFloating() //list
          :
      _typeInstallment.id == "08"
          ?
      WidgetInfoCreditStructureBP() //form
          :
      _typeInstallment.id == "07"
          ?
      WidgetInfoCreditStructureIrregular() //form
          :
      _typeInstallment.id == "06"
          ?
      WidgetInfoCreditStructureStepping() //form
          :
      _typeInstallment.id == "05"
          ?
      WidgetInfoCreditStructureGP() // list
          :
      SizedBox(),
    );
  }
}
