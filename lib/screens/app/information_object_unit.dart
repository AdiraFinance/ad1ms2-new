import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InformationObjectUnit extends StatefulWidget {
  final String flag;

  const InformationObjectUnit({this.flag});
  @override
  _InformationObjectUnitState createState() => _InformationObjectUnitState();
}

class _InformationObjectUnitState extends State<InformationObjectUnit> {

  @override
  void initState() {
    super.initState();
    print('widget.flag');
    print(widget.flag);
    if(widget.flag == "COM"){
        if(Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).listBusinessActivities.isEmpty){
            Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).getBusinessActivities(widget.flag,context);
        }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        key: Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
            title:
            Text("Informasi Unit Detail/Objek", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        body: Consumer<InformationObjectUnitChangeNotifier>(
          builder: (context, infoObjectUnit, _) {
            return infoObjectUnit.loadData
                ?
            Center(child: CircularProgressIndicator(),)
                :
            Form(
              key: infoObjectUnit.keyForm,
              onWillPop: infoObjectUnit.onBackPress,
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 27,
                    vertical: MediaQuery.of(context).size.height / 47
                ),
                child: Column(
                  children: [
                    Visibility(
                      visible: widget.flag == "COM",
                      child: DropdownButtonFormField<FinancingTypeModel>(
                            isExpanded: true,
                            autovalidate: infoObjectUnit.autoValidate,
                            validator: (e) {
                                if (e == null) {
                                    return "Tidak boleh kosong";
                                } else {
                                    return null;
                                }
                            },
                            value: infoObjectUnit.typeOfFinancingModelSelected,
                            onChanged: (value) {
                                infoObjectUnit.typeOfFinancingModelSelected = value;
                                infoObjectUnit.businessActivitiesModelSelected = null;
                                infoObjectUnit.getListBusinessActivitiesType();
                            },
                            onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                                labelText: "Jenis Pembiayaan",
                                border: OutlineInputBorder(),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: infoObjectUnit.listTypeOfFinancing.map((value) {
                                return DropdownMenuItem<FinancingTypeModel>(
                                    value: value,
                                    child: Text(
                                        value.financingTypeName,
                                        overflow: TextOverflow.ellipsis,
                                    ),
                                );
                            }).toList(),
                        ),
                    ),
                    Visibility(
                        visible: widget.flag == "COM",
                        child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                    ),
                    Visibility(
                      visible: widget.flag == "COM",
                      child: DropdownButtonFormField<KegiatanUsahaModel>(
                        autovalidate: infoObjectUnit.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        value: infoObjectUnit.businessActivitiesModelSelected,
                        onChanged: (value) {
                          infoObjectUnit.businessActivitiesModelSelected = value;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Kegiatan Usaha",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: infoObjectUnit.listBusinessActivities.map((value) {
                          return DropdownMenuItem<KegiatanUsahaModel>(
                            value: value,
                            child: Text(
                              value.text,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: widget.flag == "COM" ? true : false,),
                    Visibility(
                      visible: widget.flag == "COM",
                      child: DropdownButtonFormField<JenisKegiatanUsahaModel>(
                        autovalidate: infoObjectUnit.autoValidate,
                        isExpanded: true,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        value: infoObjectUnit.businessActivitiesTypeModelSelected,
                        onChanged: (value) {
                          infoObjectUnit.businessActivitiesTypeModelSelected =
                              value;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Jenis Kegiatan Usaha",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: infoObjectUnit.listBusinessActivitiesType
                            .map((value) {
                          return DropdownMenuItem<JenisKegiatanUsahaModel>(
                            value: value,
                            child: Text(
                              value.text,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: widget.flag == "COM" ? true : false,),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          if(widget.flag == "COM"){
                            if(infoObjectUnit.typeOfFinancingModelSelected != null){
                              infoObjectUnit.searchGroupObject(context,widget.flag);
                            }
                          }
                          else{
                            infoObjectUnit.searchGroupObject(context,widget.flag);
                          }
                        },
                        // enabled: widget.flag == "COM" ? infoObjectUnit.typeOfFinancingModelSelected != null ? true : false : true,
                        controller: infoObjectUnit.controllerGroupObject,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Grup Objek',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)
                            )
                        )
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: infoObjectUnit.controllerGroupObject.text != ""
                        ? () {
                          infoObjectUnit.searchObject(context);
                        }
                        : null,
                        controller: infoObjectUnit.controllerObject,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Objek',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: infoObjectUnit.controllerGroupObject.text != ""
                        ? (){infoObjectUnit.searchProductType(context,widget.flag);}
                        : null,
                        controller: infoObjectUnit.controllerTypeProduct,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Jenis Produk',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: infoObjectUnit.controllerTypeProduct.text != ""
                        ? () {infoObjectUnit.searchBrandObject(context,widget.flag,1);}
                        : null,
                        controller: infoObjectUnit.controllerBrandObject,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Merk Objek',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchObjectType(context,widget.flag,2);
                        },
                        controller: infoObjectUnit.controllerObjectType,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Jenis Objek',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchModelObject(context,widget.flag,3);
                        },
                        controller: infoObjectUnit.controllerModelObject,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Model Objek',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
//                     TextFormField(
// //                        validator: (e) {
// //                          if (e.isEmpty) {
// //                            return "Tidak boleh kosong";
// //                          } else {
// //                            return null;
// //                          }
// //                        },
//                         readOnly: infoObjectUnit.groupObjectSelected != null
//                         ? infoObjectUnit.groupObjectSelected.KODE == "003"
//                           ? false
//                           : true
//                         : true,
//                         controller: infoObjectUnit.controllerDetailModel,
//                         autovalidate: infoObjectUnit.autoValidate,
//                         decoration: new InputDecoration(
//                             labelText: 'Model Detail',
//                             labelStyle: TextStyle(color: Colors.black),
//                             border: OutlineInputBorder(
//                                 borderRadius: BorderRadius.circular(8)))),
//                     SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: (){
                          infoObjectUnit.searchUsageObject(context);
                        },
                        controller: infoObjectUnit.controllerUsageObjectModel,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: InputDecoration(
                            labelText: 'Pemakaian Objek',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchObjectPurpose(context);
                        },
                        controller: infoObjectUnit.controllerObjectPurpose,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Tujuan Penggunaan Objek',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        autovalidate: infoObjectUnit.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchGroupSales(context);
                        },
                        controller: infoObjectUnit.controllerGroupSales,
                        decoration: new InputDecoration(
                            labelText: 'Grup Sales',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        autovalidate: infoObjectUnit.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchSourceOrder(context);
                        },
                        controller: infoObjectUnit.controllerSourceOrder,
                        decoration: new InputDecoration(
                            labelText: 'Sumber Order',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchSourceOrderName(context);
                        },
                        controller: infoObjectUnit.controllerSourceOrderName,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Nama Sumber Order',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchThirdPartyType(context);
                        },
                        controller: infoObjectUnit.controllerThirdPartyType,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Jenis Pihak Ketiga',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchThirdParty(context);
                        },
                        controller: infoObjectUnit.controllerThirdParty,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Pihak Ketiga',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        // autovalidate: infoObjectUnit.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        enabled: false,
                        // onTap: () {
                        //   infoObjectUnit.searchMatriksDealer(context);
                        // },
                        controller: infoObjectUnit.controllerMatriksDealer,
                        decoration: new InputDecoration(
                            labelText: 'Matriks Dealer',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    // TextFormField(
                    //     // validator: (e) {
                    //     //    if (e.isEmpty) {
                    //     //      return "Tidak boleh kosong";
                    //     //    } else {
                    //     //      return null;
                    //     //    }
                    //     //  },
                    //     readOnly: true,
                    //     onTap: infoObjectUnit.sourceOrderNameSelected != null ? () {
                    //       infoObjectUnit.searchActivities(context);
                    //     }: null,
                    //     controller: infoObjectUnit.controllerKegiatan,
                    //     autovalidate: infoObjectUnit.autoValidate,
                    //     decoration: new InputDecoration(
                    //         labelText: 'Kegiatan',
                    //         labelStyle: TextStyle(color: Colors.black),
                    //         border: OutlineInputBorder(
                    //             borderRadius: BorderRadius.circular(8)))),
                    // SizedBox(height: MediaQuery.of(context).size.height / 47),
//                     TextFormField(
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                         readOnly: true,
//                         onTap: () {},
//                         controller: infoObjectUnit.controllerSentraD,
// //                        autovalidate: infoObjectUnit.autoValidate,
//                         decoration: new InputDecoration(
//                             labelText: 'Sentra D',
//                             labelStyle: TextStyle(color: Colors.black),
//                             border: OutlineInputBorder(
//                                 borderRadius: BorderRadius.circular(8)))),
//                     SizedBox(height: MediaQuery.of(context).size.height / 47),
//                     TextFormField(
//                       validator: (e) {
//                         if (e.isEmpty) {
//                           return "Tidak boleh kosong";
//                         } else {
//                           return null;
//                         }
//                       },
//                       readOnly: true,
//                       onTap: () {},
//                       controller: infoObjectUnit.controllerUnitD,
//                       autovalidate: infoObjectUnit.autoValidate,
//                       decoration: new InputDecoration(
//                           labelText: 'Unit D',
//                           labelStyle: TextStyle(color: Colors.black),
//                           border: OutlineInputBorder(
//                               borderRadius: BorderRadius.circular(8)))),
//                     SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchProgram(context);
                        },
                        controller: infoObjectUnit.controllerProgram,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Program',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchRehabType(context);
                        },
                        controller: infoObjectUnit.controllerRehabType,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'Jenis Rehab',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
                        readOnly: true,
                        onTap: () {
                          infoObjectUnit.searchReferenceNumber(context);
                        },
                        controller: infoObjectUnit.controllerReferenceNumber,
                        autovalidate: infoObjectUnit.autoValidate,
                        decoration: new InputDecoration(
                            labelText: 'No Reference',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)))),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
//                    TextFormField(
////                validator: (e) {
////                  if (e.isEmpty) {
////                    return "Tidak boleh kosong";
////                  } else {
////                    return null;
////                  }
////                },
//                        readOnly: true,
//                        onTap: () {},
//                        controller: infoObjectUnit.controllerWMP,
//                        autovalidate: infoObjectUnit.autoValidate,
//                        decoration: new InputDecoration(
//                            labelText: 'WMP',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)))),
//                     SizedBox(height: MediaQuery.of(context).size.height / 47),
                  ],
                ),
              ),
            );
          },
        ),
        bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<InformationObjectUnitChangeNotifier>(
                      builder: (context, informationObjectUnitChangeNotifier, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                  informationObjectUnitChangeNotifier.check(context);
                              },
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                      Text("DONE",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: 1.25))
                                  ],
                              ));
                      },
                  )),
          ),
      ),
    );
  }
}
