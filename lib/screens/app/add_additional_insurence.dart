import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/additional_insurance_model.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_additional_insurance_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';


class AddAdditionalInsurance extends StatefulWidget {
    final int flag;
    final AdditionalInsuranceModel formMAdditionalInsurance;
    final int index;

    const AddAdditionalInsurance({Key key, this.flag, this.formMAdditionalInsurance, this.index}) : super(key: key);
    @override
    _AddAdditionalInsuranceState createState() => _AddAdditionalInsuranceState();
}

class _AddAdditionalInsuranceState extends State<AddAdditionalInsurance> {
    Future<void> _loadData;
    @override
    void initState() {
        if (widget.flag != 0) {
            _loadData =
                Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false)
                    .setValueForEditAdditionalInsurance(widget.formMAdditionalInsurance);
        }
        super.initState();
        Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false).getListInsuranceType(context);
    }
    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                accentColor: myPrimaryColor,
            ),
            child: Consumer<FormMAddAdditionalInsuranceChangeNotifier>(
                builder: (context, formMAddAdditionalInsuranceChangeNotifier, _) {
                    return Scaffold(
                        appBar: AppBar(
                            backgroundColor: myPrimaryColor,
                            title: Text(
                                widget.flag == 0 ? "Tambah Asuransi Tambahan" : "Edit Asuransi Tambahan",
                                style: TextStyle(color: Colors.black)),
                            centerTitle: true,
                            iconTheme: IconThemeData(
                                color: Colors.black,
                            ),
                        ),
                        body: SingleChildScrollView(
                            padding: EdgeInsets.symmetric(
                                horizontal: MediaQuery.of(context).size.width / 27,
                                vertical: MediaQuery.of(context).size.height / 57),
                            child: widget.flag == 0
                                ? Consumer<FormMAddAdditionalInsuranceChangeNotifier>(
                                builder: (consumerContext, formMaddAdditionalInsuranceChangeNotifier, _){
                                    return Form(
                                        onWillPop: _onWillPop,
                                        key: formMaddAdditionalInsuranceChangeNotifier.key,
                                        child: Column(
                                            children: [
                                                DropdownButtonFormField<InsuranceTypeModel>(
                                                    autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e == null) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    value: formMaddAdditionalInsuranceChangeNotifier.insuranceTypeSelected,
                                                    onChanged: (value) {
                                                        formMaddAdditionalInsuranceChangeNotifier.insuranceTypeSelected = value;
                                                    },
                                                    decoration: InputDecoration(
                                                        labelText: "Tipe Asuransi",
                                                        border: OutlineInputBorder(),
                                                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                    ),
                                                    items: formMaddAdditionalInsuranceChangeNotifier.listInsurance.map((value) {
                                                        return DropdownMenuItem<InsuranceTypeModel>(
                                                            value: value,
                                                            child: Text(
                                                                value.DESKRIPSI,
                                                                overflow: TextOverflow.ellipsis,
                                                            ),
                                                        );
                                                    }).toList(),
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                TextFormField(
                                                    readOnly: true,
                                                    onTap:() {
                                                        formMaddAdditionalInsuranceChangeNotifier.searchCompany(context);
                                                    },
                                                    controller: formMaddAdditionalInsuranceChangeNotifier
                                                        .controllerCompany,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        labelText: 'Perusahaan',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    keyboardType: TextInputType.text,
                                                    textCapitalization: TextCapitalization.characters,
                                                    autovalidate:
                                                    formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                TextFormField(
                                                    readOnly: true,
                                                    onTap:() {
                                                        formMaddAdditionalInsuranceChangeNotifier.searchProduct(context);
                                                    },
                                                    controller: formMaddAdditionalInsuranceChangeNotifier
                                                        .controllerProduct,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        labelText: 'Produk',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    keyboardType: TextInputType.text,
                                                    textCapitalization: TextCapitalization.characters,
                                                    autovalidate:
                                                    formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                TextFormField(
                                                    readOnly: true,
                                                    controller: formMaddAdditionalInsuranceChangeNotifier
                                                        .controllerPeriodType,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        labelText: 'Periode',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    keyboardType: TextInputType.number,
                                                    textCapitalization: TextCapitalization.characters,
                                                    inputFormatters: [
                                                        WhitelistingTextInputFormatter.digitsOnly
                                                    ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                TextFormField(
                                                    controller: formMaddAdditionalInsuranceChangeNotifier
                                                        .controllerCoverageType,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        labelText: 'Jenis Pertanggungan',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    keyboardType: TextInputType.text,
                                                    textCapitalization: TextCapitalization.characters,
                                                    autovalidate:
                                                    formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    enabled: false,
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                TextFormField(
                                                    readOnly: true,
                                                    controller: formMaddAdditionalInsuranceChangeNotifier
                                                        .controllerCoverageValue,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        labelText: 'Nilai Pertanggungan',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    keyboardType: TextInputType.number,
                                                    textCapitalization: TextCapitalization.characters,
                                                    inputFormatters: [
                                                        WhitelistingTextInputFormatter.digitsOnly
                                                    ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                Row(
                                                    children: [
                                                        Expanded(
                                                            flex: 2,
                                                            child: TextFormField(
                                                                readOnly: true,
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerUpperLimitRate,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Batas Atas %',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                            ),
                                                        ),
                                                        SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                        Expanded(
                                                            flex: 3,
                                                            child: TextFormField(
                                                                readOnly: true,
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerUpperLimit,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Batas Atas',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                            ),
                                                        ),
                                                    ],
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                Row(
                                                    children: [
                                                        Expanded(
                                                            flex: 2,
                                                            child: TextFormField(
                                                                readOnly: true,
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerLowerLimitRate,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Batas Bawah %',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                            ),
                                                        ),
                                                        SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                        Expanded(
                                                            flex: 3,
                                                            child: TextFormField(
                                                                readOnly: true,
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerLowerLimit,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Batas Bawah',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                            ),
                                                        ),
                                                    ],
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                TextFormField(
                                                    controller: formMaddAdditionalInsuranceChangeNotifier
                                                        .controllerPriceCash,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        labelText: 'Biaya Tunai',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    keyboardType: TextInputType.number,
                                                    textCapitalization: TextCapitalization.characters,
                                                    inputFormatters: [
                                                        WhitelistingTextInputFormatter.digitsOnly
                                                    ],
                                                autovalidate:
                                                formMAddAdditionalInsuranceChangeNotifier.autoValidate,
                                                validator: (e) {
                                                    if (e.isEmpty) {
                                                        return "Tidak boleh kosong";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                TextFormField(
                                                    controller: formMaddAdditionalInsuranceChangeNotifier
                                                        .controllerPriceCredit,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        labelText: 'Biaya Kredit',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    keyboardType: TextInputType.number,
                                                    textCapitalization: TextCapitalization.characters,
                                                    inputFormatters: [
                                                        WhitelistingTextInputFormatter.digitsOnly
                                                    ],
                                                autovalidate:
                                                formMAddAdditionalInsuranceChangeNotifier.autoValidate,
                                                validator: (e) {
                                                    if (e.isEmpty) {
                                                        return "Tidak boleh kosong";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                Row(
                                                    children: [
                                                        Expanded(
                                                            flex: 2,
                                                            child: TextFormField(
                                                                readOnly: true,
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerTotalPriceRate,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Total Biaya %',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                            ),
                                                        ),
                                                        SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                        Expanded(
                                                            flex: 3,
                                                            child: TextFormField(
                                                                readOnly: true,
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerTotalPrice,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Total Biaya',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                            ),
                                                        ),
                                                    ],
                                                ),
                                            ],
                                        ),
                                    );
                                },
                            )
                                : FutureBuilder(
                                future: _loadData,
                                builder: (context, snapshot) {
                                    if (snapshot.connectionState == ConnectionState.waiting) {
                                        return Center(
                                            child: CircularProgressIndicator(),
                                        );
                                    }
                                    return Consumer<FormMAddAdditionalInsuranceChangeNotifier>(
                                        builder:
                                            (consumerContext, formMaddAdditionalInsuranceChangeNotifier, _) {
                                                return Form(
                                                    onWillPop: _onWillPop,
                                                    key: formMaddAdditionalInsuranceChangeNotifier.key,
                                                    child: Column(
                                                        children: [
                                                            DropdownButtonFormField<InsuranceTypeModel>(
                                                                autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                validator: (e) {
                                                                    if (e == null) {
                                                                        return "Tidak boleh kosong";
                                                                    } else {
                                                                        return null;
                                                                    }
                                                                },
                                                                value: formMaddAdditionalInsuranceChangeNotifier.insuranceTypeSelected,
                                                                onChanged: (value) {
                                                                    formMaddAdditionalInsuranceChangeNotifier.insuranceTypeSelected = value;
                                                                },
                                                                decoration: InputDecoration(
                                                                    labelText: "Tipe Asuransi",
                                                                    border: OutlineInputBorder(),
                                                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                ),
                                                                items: formMaddAdditionalInsuranceChangeNotifier.listInsurance.map((value) {
                                                                    return DropdownMenuItem<InsuranceTypeModel>(
                                                                        value: value,
                                                                        child: Text(
                                                                            value.DESKRIPSI,
                                                                            overflow: TextOverflow.ellipsis,
                                                                        ),
                                                                    );
                                                                }).toList(),
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            TextFormField(
                                                                readOnly: true,
                                                                onTap:() {
                                                                    formMaddAdditionalInsuranceChangeNotifier.searchCompany(context);
                                                                },
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerCompany,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Perusahaan',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.text,
                                                                textCapitalization: TextCapitalization.characters,
                                                                autovalidate:
                                                                formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                validator: (e) {
                                                                    if (e.isEmpty) {
                                                                        return "Tidak boleh kosong";
                                                                    } else {
                                                                        return null;
                                                                    }
                                                                },
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            TextFormField(
                                                                readOnly: true,
                                                                onTap:() {
                                                                    formMaddAdditionalInsuranceChangeNotifier.searchProduct(context);
                                                                },
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerProduct,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Produk',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.text,
                                                                textCapitalization: TextCapitalization.characters,
                                                                autovalidate:
                                                                formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                validator: (e) {
                                                                    if (e.isEmpty) {
                                                                        return "Tidak boleh kosong";
                                                                    } else {
                                                                        return null;
                                                                    }
                                                                },
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            TextFormField(
                                                                readOnly: true,
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerPeriodType,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Periode',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            TextFormField(
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerCoverageType,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Jenis Pertanggungan',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.text,
                                                                textCapitalization: TextCapitalization.characters,
                                                                autovalidate:
                                                                formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                validator: (e) {
                                                                    if (e.isEmpty) {
                                                                        return "Tidak boleh kosong";
                                                                    } else {
                                                                        return null;
                                                                    }
                                                                },
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            TextFormField(
                                                                readOnly: true,
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerCoverageValue,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Nilai Pertanggungan',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        flex: 2,
                                                                        child: TextFormField(
                                                                            readOnly: true,
                                                                            controller: formMaddAdditionalInsuranceChangeNotifier
                                                                                .controllerUpperLimitRate,
                                                                            style: new TextStyle(color: Colors.black),
                                                                            decoration: new InputDecoration(
                                                                                labelText: 'Batas Atas %',
                                                                                labelStyle: TextStyle(color: Colors.black),
                                                                                border: OutlineInputBorder(
                                                                                    borderRadius: BorderRadius.circular(8))),
                                                                            keyboardType: TextInputType.number,
                                                                            textCapitalization: TextCapitalization.characters,
                                                                            inputFormatters: [
                                                                                WhitelistingTextInputFormatter.digitsOnly
                                                                            ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                                        ),
                                                                    ),
                                                                    SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                                    Expanded(
                                                                        flex: 3,
                                                                        child: TextFormField(
                                                                            readOnly: true,
                                                                            controller: formMaddAdditionalInsuranceChangeNotifier
                                                                                .controllerUpperLimit,
                                                                            style: new TextStyle(color: Colors.black),
                                                                            decoration: new InputDecoration(
                                                                                labelText: 'Batas Atas',
                                                                                labelStyle: TextStyle(color: Colors.black),
                                                                                border: OutlineInputBorder(
                                                                                    borderRadius: BorderRadius.circular(8))),
                                                                            keyboardType: TextInputType.number,
                                                                            textCapitalization: TextCapitalization.characters,
                                                                            inputFormatters: [
                                                                                WhitelistingTextInputFormatter.digitsOnly
                                                                            ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                                        ),
                                                                    ),
                                                                ],
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        flex: 2,
                                                                        child: TextFormField(
                                                                            readOnly: true,
                                                                            controller: formMaddAdditionalInsuranceChangeNotifier
                                                                                .controllerLowerLimitRate,
                                                                            style: new TextStyle(color: Colors.black),
                                                                            decoration: new InputDecoration(
                                                                                labelText: 'Batas Bawah %',
                                                                                labelStyle: TextStyle(color: Colors.black),
                                                                                border: OutlineInputBorder(
                                                                                    borderRadius: BorderRadius.circular(8))),
                                                                            keyboardType: TextInputType.number,
                                                                            textCapitalization: TextCapitalization.characters,
                                                                            inputFormatters: [
                                                                                WhitelistingTextInputFormatter.digitsOnly
                                                                            ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                                        ),
                                                                    ),
                                                                    SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                                    Expanded(
                                                                        flex: 3,
                                                                        child: TextFormField(
                                                                            readOnly: true,
                                                                            controller: formMaddAdditionalInsuranceChangeNotifier
                                                                                .controllerLowerLimit,
                                                                            style: new TextStyle(color: Colors.black),
                                                                            decoration: new InputDecoration(
                                                                                labelText: 'Batas Bawah',
                                                                                labelStyle: TextStyle(color: Colors.black),
                                                                                border: OutlineInputBorder(
                                                                                    borderRadius: BorderRadius.circular(8))),
                                                                            keyboardType: TextInputType.number,
                                                                            textCapitalization: TextCapitalization.characters,
                                                                            inputFormatters: [
                                                                                WhitelistingTextInputFormatter.digitsOnly
                                                                            ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                                        ),
                                                                    ),
                                                                ],
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            TextFormField(
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerPriceCash,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Biaya Tunai',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
                                                            autovalidate:
                                                            formMAddAdditionalInsuranceChangeNotifier.autoValidate,
                                                            validator: (e) {
                                                                if (e.isEmpty) {
                                                                    return "Tidak boleh kosong";
                                                                } else {
                                                                    return null;
                                                                }
                                                            },
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            TextFormField(
                                                                controller: formMaddAdditionalInsuranceChangeNotifier
                                                                    .controllerPriceCredit,
                                                                style: new TextStyle(color: Colors.black),
                                                                decoration: new InputDecoration(
                                                                    labelText: 'Biaya Kredit',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8))),
                                                                keyboardType: TextInputType.number,
                                                                textCapitalization: TextCapitalization.characters,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
                                                            autovalidate:
                                                            formMAddAdditionalInsuranceChangeNotifier.autoValidate,
                                                            validator: (e) {
                                                                if (e.isEmpty) {
                                                                    return "Tidak boleh kosong";
                                                                } else {
                                                                    return null;
                                                                }
                                                            },
                                                            ),
                                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        flex: 2,
                                                                        child: TextFormField(
                                                                            readOnly: true,
                                                                            controller: formMaddAdditionalInsuranceChangeNotifier
                                                                                .controllerTotalPriceRate,
                                                                            style: new TextStyle(color: Colors.black),
                                                                            decoration: new InputDecoration(
                                                                                labelText: 'Total Biaya %',
                                                                                labelStyle: TextStyle(color: Colors.black),
                                                                                border: OutlineInputBorder(
                                                                                    borderRadius: BorderRadius.circular(8))),
                                                                            keyboardType: TextInputType.number,
                                                                            textCapitalization: TextCapitalization.characters,
                                                                            inputFormatters: [
                                                                                WhitelistingTextInputFormatter.digitsOnly
                                                                            ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                                        ),
                                                                    ),
                                                                    SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                                    Expanded(
                                                                        flex: 3,
                                                                        child: TextFormField(
                                                                            readOnly: true,
                                                                            controller: formMaddAdditionalInsuranceChangeNotifier
                                                                                .controllerTotalPrice,
                                                                            style: new TextStyle(color: Colors.black),
                                                                            decoration: new InputDecoration(
                                                                                labelText: 'Total Biaya',
                                                                                labelStyle: TextStyle(color: Colors.black),
                                                                                border: OutlineInputBorder(
                                                                                    borderRadius: BorderRadius.circular(8))),
                                                                            keyboardType: TextInputType.number,
                                                                            textCapitalization: TextCapitalization.characters,
                                                                            inputFormatters: [
                                                                                WhitelistingTextInputFormatter.digitsOnly
                                                                            ],
//                                    autovalidate:
//                                    infoMajorInsuranceChangeNotifier.autoValidate,
//                                    validator: (e) {
//                                        if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                        } else {
//                                            return null;
//                                        }
//                                    },
                                                                        ),
                                                                    ),
                                                                ],
                                                            ),
                                                        ],
                                                    ),
                                                );
                                        },
                                    );
                                }),
                        ),
                        bottomNavigationBar: BottomAppBar(
                            elevation: 0.0,
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Consumer<FormMAddAdditionalInsuranceChangeNotifier>(
                                    builder: (context, formMAddAdditionalInsuranceChangeNotifier, _) {
                                        return RaisedButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius: new BorderRadius.circular(8.0)),
                                            color: myPrimaryColor,
                                            onPressed: () {
                                                if (widget.flag == 0) {
                                                    formMAddAdditionalInsuranceChangeNotifier.check(
                                                        context, widget.flag, null);
                                                } else {
                                                    formMAddAdditionalInsuranceChangeNotifier.check(
                                                        context, widget.flag, widget.index);
                                                }
                                            },
                                            child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                    Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight: FontWeight.w500,
                                                            letterSpacing: 1.25))
                                                ],
                                            ));
                                    },
                                )),
                        ),
                    );
                },
            )
        );

    }

    Future<bool> _onWillPop() async {
      var _provider = Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false);
      if (widget.flag == 0) {
        if (_provider.insuranceTypeSelected != null ||
            _provider.controllerCompany.text != "" ||
            _provider.controllerProduct.text != "" ||
            _provider.controllerCoverageType.text != "" ||
            _provider.controllerPriceCredit.text != "" ||
            _provider.controllerPriceCash.text != "") {
            return (await showDialog(
                context: context,
                builder: (myContext) => AlertDialog(
                    title: new Text('Warning'),
                    content: new Text('Keluar dengan simpan perubahan?'),
                    actions: <Widget>[
                        new FlatButton(
                            onPressed: () {
                                widget.flag == 0
                                    ?
                                _provider.check(context, widget.flag, null)
                                    :
                                _provider.check(context, widget.flag, widget.index);
                                Navigator.pop(context);
                            },
                            child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                        ),
                        new FlatButton(
                            onPressed: () => Navigator.of(context).pop(true),
                            child: new Text('Tidak'),
                        ),
                    ],
                ),
            )) ??
                false;
        } else {
            return true;
        }
      } else {
          if (widget.formMAdditionalInsurance.insuranceType.KODE != _provider.insuranceTypeSelected.KODE ||
              widget.formMAdditionalInsurance.company.id != _provider.companySelected.id ||
              widget.formMAdditionalInsurance.product.KODE != _provider.productSelected.DESKRIPSI ||
              widget.formMAdditionalInsurance.coverageType != _provider.controllerCoverageType.text ||
              widget.formMAdditionalInsurance.priceCredit != _provider.controllerPriceCredit.text ||
              widget.formMAdditionalInsurance.priceCash != _provider.controllerPriceCash.text) {
              return (await showDialog(
                  context: context,
                  builder: (myContext) => AlertDialog(
                      title: new Text('Warning'),
                      content: new Text('Keluar dengan simpan perubahan?'),
                      actions: <Widget>[
                          new FlatButton(
                              onPressed: () {
                                  _provider.check(context, widget.flag, widget.index);
                                  Navigator.pop(context);
                              },
                              child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                          ),
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: new Text('Tidak'),
                          ),
                      ],
                  ),
              )) ??
                  false;
          } else {
              return true;
          }
      }
    }
}
