import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/models/major_insurance_model.dart';
import 'package:ad1ms2_dev/models/type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';


class AddMajorInsurance extends StatefulWidget {
    final int flag;
    final MajorInsuranceModel formMMajorInsurance;
    final int index;

    const AddMajorInsurance({Key key, this.flag, this.formMMajorInsurance, this.index}) : super(key: key);
  @override
  _AddMajorInsuranceState createState() => _AddMajorInsuranceState();
}

class _AddMajorInsuranceState extends State<AddMajorInsurance> {
    Future<void> _loadData;
    @override
    void initState() {
        if (widget.flag != 0) {
            _loadData =
                Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false)
                    .addData(context,widget.formMMajorInsurance,1);
        }
        else{
            _loadData = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).addData(context,null,0);
        }
        super.initState();

    }
    @override
    Widget build(BuildContext context) {
      return Theme(
          data: ThemeData(
            fontFamily: "NunitoSans",
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            primarySwatch: primaryOrange,
          ),
          child: Consumer<FormMAddMajorInsuranceChangeNotifier>(
            builder: (context, formMAddMajorInsuranceChangeNotifier, _) {
              return Scaffold(
                appBar: AppBar(
                  backgroundColor: myPrimaryColor,
                  title: Text(
                      widget.flag == 0 ? "Tambah Asuransi Utama" : "Edit Asuransi Utama",
                      style: TextStyle(color: Colors.black)),
                  centerTitle: true,
                  iconTheme: IconThemeData(
                    color: Colors.black,
                  ),
                ),
                body: formMAddMajorInsuranceChangeNotifier.loadData
                    ?
                Center(child: CircularProgressIndicator())
                    :
                FutureBuilder(
                    future: _loadData,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Consumer<FormMAddMajorInsuranceChangeNotifier>(
                        builder: (consumerContext, formMaddMajorInsuranceChangeNotifier, _) {
                          return Form(
                            // onWillPop: _onWillPop,
                            key: formMaddMajorInsuranceChangeNotifier.key,
                            child: SingleChildScrollView(
                              padding: EdgeInsets.symmetric(
                                  horizontal: MediaQuery.of(context).size.width / 27,
                                  vertical: MediaQuery.of(context).size.height / 57),
                              child: Column(
                                children: [
                                  DropdownButtonFormField<InsuranceTypeModel>(
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e == null) {
                                        return "Tidak boleh kosong";
                                      }
                                      else {
                                        return null;
                                      }
                                    },
                                    value: formMAddMajorInsuranceChangeNotifier.insuranceTypeSelected,
                                    onChanged: (value) {
                                      formMAddMajorInsuranceChangeNotifier.insuranceTypeSelected = value;
                                      },
                                    decoration: InputDecoration(
                                      labelText: "Tipe Asuransi",
                                      border: OutlineInputBorder(),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: formMAddMajorInsuranceChangeNotifier.listInsurance.map((value) {
                                      return DropdownMenuItem<InsuranceTypeModel>(
                                        value: value,
                                        child: Text(
                                          value.DESKRIPSI,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  DropdownButtonFormField<String>(
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e == null) {
                                        return "Tidak boleh kosong";
                                      }
                                      else {
                                        return null;
                                      }
                                    },
                                    value: formMAddMajorInsuranceChangeNotifier.numberOfCollateral,
                                    onChanged: (value) {
                                      formMAddMajorInsuranceChangeNotifier.numberOfCollateral = value;
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jaminan Ke",
                                      border: OutlineInputBorder(),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: formMAddMajorInsuranceChangeNotifier.listNumberOfColla.map((value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(
                                          value,
                                          overflow: TextOverflow.ellipsis,
                                              ),
                                          );
                                      }).toList(),
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    readOnly: true,
                                    onTap:() {
                                      formMAddMajorInsuranceChangeNotifier.searchCompany(context);
                                    },
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerCompany,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Perusahaan',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    // validator: (e) {
                                    //   if (e.isEmpty) {
                                    //     return "Tidak boleh kosong";
                                    //   } else {
                                    //     return null;
                                    //   }
                                    // },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    readOnly: true,
                                    onTap:() {
                                      formMAddMajorInsuranceChangeNotifier.searchProduct(context);
                                      },
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerProduct,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Produk',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate:
                                    formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    enabled: false,
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerPeriodType,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Periode',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                    textCapitalization: TextCapitalization.characters,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly
                                    ],
                                    // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    // validator: (e) {
                                    //     if (e.isEmpty) {
                                    //         return "Tidak boleh kosong";
                                    //     } else {
                                    //         return null;
                                    //     }
                                    // },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  formMAddMajorInsuranceChangeNotifier.switchType
                                      ?
                                  TextFormField(
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerType,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Jenis',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                    ],
                                    autovalidate:
                                    formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onFieldSubmitted: (value){
                                      formMAddMajorInsuranceChangeNotifier.calculatePeriod(0,null);
                                    },
                                  )
                                      :
                                  DropdownButtonFormField<String>(
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e == null) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                      },
                                    value: formMAddMajorInsuranceChangeNotifier.typeSelected,
                                    onChanged: (value) {
                                      formMAddMajorInsuranceChangeNotifier.typeSelected = value;
                                      formMAddMajorInsuranceChangeNotifier.calculatePeriod(0,null);
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jenis",
                                      border: OutlineInputBorder(),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: formMAddMajorInsuranceChangeNotifier.listType.map((value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(
                                          value,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    readOnly: true,
                                    onTap:() {
                                      formMAddMajorInsuranceChangeNotifier.searchCoverage(context,1);
                                      },
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerCoverage1,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Coverage 1',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate:
                                    formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                      },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    readOnly: true,
                                    onTap:() {
                                      formMAddMajorInsuranceChangeNotifier.searchCoverage(context,2);
                                      },
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerCoverage2,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Coverage 2',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: formMaddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                      },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    onTap: (){
                                      formMAddMajorInsuranceChangeNotifier.searchCoverageType(context);
                                    },
                                    readOnly: true,
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerCoverageType,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Jenis Pertanggungan',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    // validator: (e) {
                                    //   if (e.isEmpty) {
                                    //     return "Tidak boleh kosong";
                                    //   } else {
                                    //     return null;
                                    //   }
                                    // },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    readOnly: true,
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerCoverageValue,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Nilai Pertanggungan',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                    textCapitalization: TextCapitalization.characters,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly
                                    ],
                                    textAlign: TextAlign.end,
                                    // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    // validator: (e) {
                                    //     if (e.isEmpty) {
                                    //         return "Tidak boleh kosong";
                                    //     } else {
                                    //         return null;
                                    //     }
                                    // },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier
                                              .controllerUpperLimitRate,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              labelText: 'Batas Atas %',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          // validator: (e) {
                                          //     if (e.isEmpty) {
                                          //         return "Tidak boleh kosong";
                                          //     } else {
                                          //         return null;
                                          //     }
                                          // },
                                        ),
                                      ),
                                      SizedBox(width: MediaQuery.of(context).size.width / 47),
                                      Expanded(
                                        flex: 3,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier
                                              .controllerUpperLimit,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              labelText: 'Batas Atas',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          // validator: (e) {
                                          //     if (e.isEmpty) {
                                          //         return "Tidak boleh kosong";
                                          //     } else {
                                          //         return null;
                                          //     }
                                          // },
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier
                                              .controllerLowerLimitRate,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              labelText: 'Batas Bawah %',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          // validator: (e) {
                                          //     if (e.isEmpty) {
                                          //         return "Tidak boleh kosong";
                                          //     } else {
                                          //         return null;
                                          //     }
                                          // },
                                        ),
                                      ),
                                      SizedBox(width: MediaQuery.of(context).size.width / 47),
                                      Expanded(
                                        flex: 3,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier
                                              .controllerLowerLimit,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              labelText: 'Batas Bawah',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          // validator: (e) {
                                          //     if (e.isEmpty) {
                                          //         return "Tidak boleh kosong";
                                          //     } else {
                                          //         return null;
                                          //     }
                                          // },
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerPriceCash,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Biaya Tunai',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.end,
                                    onFieldSubmitted: (value) {
                                      formMAddMajorInsuranceChangeNotifier.controllerPriceCash.text = formMAddMajorInsuranceChangeNotifier.formatCurrency.formatCurrency(value);
                                      },
                                    onTap: () {
                                      formMAddMajorInsuranceChangeNotifier.formatting();
                                      },
                                    textInputAction: TextInputAction.done,
                                    inputFormatters: [
                                      DecimalTextInputFormatter(decimalRange: 2),
                                      formMAddMajorInsuranceChangeNotifier.amountValidator
                                    ],
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                      },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerPriceCredit,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Biaya Kredit',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.end,
                                    onFieldSubmitted: (value) {
                                      formMAddMajorInsuranceChangeNotifier.controllerPriceCredit.text = formMAddMajorInsuranceChangeNotifier.formatCurrency.formatCurrency(value);
                                      },
                                    onTap: () {
                                      formMAddMajorInsuranceChangeNotifier.formatting();
                                      },
                                    textInputAction: TextInputAction.done,
                                    inputFormatters: [
                                      DecimalTextInputFormatter(decimalRange: 2),
                                      formMAddMajorInsuranceChangeNotifier.amountValidator
                                    ],
                                    autovalidate:
                                    formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                      },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  TextFormField(
                                    enabled: false,
                                    controller: formMAddMajorInsuranceChangeNotifier
                                        .controllerTotalPriceSplit,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Total Biaya Split',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                    textCapitalization: TextCapitalization.characters,
                                    textAlign: TextAlign.end,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly
                                    ],
                                    // autovalidate: formMaddMajorInsuranceChangeNotifier.autoValidate,
                                    // validator: (e) {
                                    //   if (e.isEmpty) {
                                    //     return "Tidak boleh kosong";
                                    //   } else {
                                    //     return null;
                                    //   }
                                    // },
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier
                                              .controllerTotalPriceRate,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              labelText: 'Total Biaya %',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          // validator: (e) {
                                          //     if (e.isEmpty) {
                                          //         return "Tidak boleh kosong";
                                          //     } else {
                                          //         return null;
                                          //     }
                                          // },
                                        ),
                                      ),
                                      SizedBox(width: MediaQuery.of(context).size.width / 47),
                                      Expanded(
                                        flex: 3,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier
                                              .controllerTotalPrice,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              labelText: 'Total Biaya',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          // autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          // validator: (e) {
                                          //     if (e.isEmpty) {
                                          //         return "Tidak boleh kosong";
                                          //     } else {
                                          //         return null;
                                          //     }
                                          // },
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                          },
                      );
                    }),
                        bottomNavigationBar: BottomAppBar(
                            elevation: 0.0,
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Consumer<FormMAddMajorInsuranceChangeNotifier>(
                                    builder: (context, formMAddMajorInsuranceChangeNotifier, _) {
                                        return RaisedButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius: new BorderRadius.circular(8.0)),
                                            color: myPrimaryColor,
                                            onPressed: () {
                                                if (widget.flag == 0) {
                                                    formMAddMajorInsuranceChangeNotifier.check(
                                                        context, widget.flag, null);
                                                } else {
                                                    formMAddMajorInsuranceChangeNotifier.check(
                                                        context, widget.flag, widget.index);
                                                }
                                            },
                                            child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                    Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight: FontWeight.w500,
                                                            letterSpacing: 1.25))
                                                ],
                                            ));
                                    },
                                )),
                        ),
                    );
                },
            )
        );

    }

    Future<bool> _onWillPop() async {
      var _provider = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false);
      if (widget.flag == 0) {
        if (_provider.insuranceTypeSelected != null ||
            _provider.controllerAssurance.text != "" ||
            _provider.controllerCompany.text != "" ||
            _provider.controllerProduct.text != "" ||
            _provider.typeSelected != null ||
            _provider.controllerCoverage1.text != "" ||
            _provider.controllerCoverage2.text != "" ||
            _provider.controllerCoverageType.text != "" ||
            _provider.controllerPriceCredit.text != "" ||
            _provider.controllerPriceCash.text != "") {
            return (await showDialog(
                context: context,
                builder: (myContext) => AlertDialog(
                    title: new Text('Warning'),
                    content: new Text('Keluar dengan simpan perubahan?'),
                    actions: <Widget>[
                        new FlatButton(
                            onPressed: () {
                                widget.flag == 0
                                    ?
                                _provider.check(context, widget.flag, null)
                                    :
                                _provider.check(context, widget.flag, widget.index);
                                Navigator.pop(context);
                            },
                            child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                        ),
                        new FlatButton(
                            onPressed: () => Navigator.of(context).pop(true),
                            child: new Text('Tidak'),
                        ),
                    ],
                ),
            )) ??
                false;
        } else {
            return true;
        }
      } else {
          if (widget.formMMajorInsurance.insuranceType.KODE != _provider.insuranceTypeSelected.KODE ||
              widget.formMMajorInsurance.numberOfColla != _provider.numberOfCollateral ||
              widget.formMMajorInsurance.company.id != _provider.companySelected.id ||
              widget.formMMajorInsurance.product.KODE != _provider.productSelected.KODE ||
              widget.formMMajorInsurance.type != _provider.typeSelected ||
              widget.formMMajorInsurance.coverage1.KODE != _provider.coverage1Selected.KODE ||
              widget.formMMajorInsurance.coverage2.PARENT_KODE != _provider.coverage2Selected.PARENT_KODE ||
              widget.formMMajorInsurance.coverageType != _provider.controllerCoverageType.text ||
              widget.formMMajorInsurance.priceCredit != _provider.controllerPriceCredit.text ||
              widget.formMMajorInsurance.priceCash != _provider.controllerPriceCash.text){
              return (await showDialog(
                  context: context,
                  builder: (myContext) => AlertDialog(
                      title: new Text('Warning'),
                      content: new Text('Keluar dengan simpan perubahan?'),
                      actions: <Widget>[
                          new FlatButton(
                              onPressed: () {
                                  _provider.check(context, widget.flag, widget.index);
                                  Navigator.pop(context);
                              },
                              child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                          ),
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: new Text('Tidak'),
                          ),
                      ],
                  ),
              )) ??
                  false;
          } else {
              return true;
          }
      }
    }
}
