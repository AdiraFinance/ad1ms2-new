import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/proportional_type_of_insurance_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InfoApplication extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Provider.of<InfoAppChangeNotifier>(context,listen: false).setDefaultValue(context);
    Provider.of<InfoAppChangeNotifier>(context,listen: false).setDataFromSQLite();
    // Provider.of<InfoAppChangeNotifier>(context,listen: false).deleteSQLite();
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: Consumer<InfoAppChangeNotifier>(
        builder: (context, infoAppChangeNotifier, _) {
          return SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: Column(
              children: [
                TextFormField(
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoAppChangeNotifier.controllerOrderDate,
                    autovalidate: infoAppChangeNotifier.autoValidate,
                    readOnly: true,
                    onTap: () {
                      FocusManager.instance.primaryFocus.unfocus();
                      infoAppChangeNotifier.selectDateOrder(context);
                    },
                    decoration: new InputDecoration(
                        labelText: 'Tanggal Pemesanan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)))),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    autovalidate: infoAppChangeNotifier.autoValidate,
                    controller: infoAppChangeNotifier.controllerSurveyAppointmentDate,
                    readOnly: true,
                    onTap: () {
                      FocusManager.instance.primaryFocus.unfocus();
                      infoAppChangeNotifier.selectSurveyDate(context);
                    },
                    decoration: new InputDecoration(
                        labelText: 'Tanggal Janji Survey',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)))),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Row(
                  children: [
                    Text("PK telah di tanda tangani", style: TextStyle(fontWeight: FontWeight.bold),),
                    Checkbox(
                      onChanged: (bool value) {
                        infoAppChangeNotifier.isSignedPK = value;
                      },
                      value: infoAppChangeNotifier.isSignedPK,
                      activeColor: primaryOrange,
                    )
                  ],
                ),
                Visibility(
                    visible: Provider.of<ListOidChangeNotifier>(context).customerType == "COM",
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                ),
                Visibility(
                  visible: Provider.of<ListOidChangeNotifier>(context).customerType == "COM",
                  child: DropdownButtonFormField<JenisKonsepModel>(
                    isExpanded: true,
                    autovalidate: infoAppChangeNotifier.autoValidate,
                    validator: (e) {
                      if (e == null) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    value: infoAppChangeNotifier.conceptTypeModelSelected,
                    onChanged: (value) {
                      infoAppChangeNotifier.conceptTypeModelSelected = value;
                      infoAppChangeNotifier.setDefaultValue(context);
                    },
                    onTap: () {
                      FocusManager.instance.primaryFocus.unfocus();
                    },
                    decoration: InputDecoration(
                      labelText: "Jenis Konsep",
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    ),
                    items: infoAppChangeNotifier.listConceptType.map((value) {
                      return DropdownMenuItem<JenisKonsepModel>(
                        value: value,
                        child: Text(
                          value.text,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    onChanged: (value) =>
                        infoAppChangeNotifier.addNumberOfUnitList(),
                    keyboardType: TextInputType.number,
                    controller: infoAppChangeNotifier.controllerTotalObject,
                    autovalidate: infoAppChangeNotifier.autoValidate,
                    decoration: new InputDecoration(
                        labelText: 'Jumlah Objek',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)))),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                DropdownButtonFormField<ProportionalTypeOfInsuranceModel>(
                  isExpanded: true,
                  autovalidate: infoAppChangeNotifier.autoValidate,
                  validator: (e) {
                    if (e == null) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  disabledHint: infoAppChangeNotifier.conceptTypeModelSelected != null ?
                  Text(infoAppChangeNotifier.proportionalTypeOfInsuranceModelSelected.description) : Text("Jenis proporsional asuransi"),
                  value: infoAppChangeNotifier
                      .proportionalTypeOfInsuranceModelSelected,
                  onChanged: infoAppChangeNotifier.isProporsionalEnable ?(value) {
                    infoAppChangeNotifier
                        .proportionalTypeOfInsuranceModelSelected = value;
                  } : null,
                  onTap: () {
                    FocusManager.instance.primaryFocus.unfocus();
                  },
                  decoration: InputDecoration(
                    labelText: "Jenis proporsional asuransi",
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  ),
                  items: infoAppChangeNotifier.listProportionalTypeOfInsurance
                      .map((value) {
                    return DropdownMenuItem<ProportionalTypeOfInsuranceModel>(
                      value: value,
                      child: Text(
                        value.description,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }).toList(),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                DropdownButtonFormField<String>(
                  isExpanded: true,
                  autovalidate: infoAppChangeNotifier.autoValidate,
                  validator: (e) {
                    if (e == null) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  value: infoAppChangeNotifier.numberOfUnitSelected,
                  onChanged: (value) {
                    infoAppChangeNotifier.numberOfUnitSelected = value;
                  },
                  onTap: () {
                    FocusManager.instance.primaryFocus.unfocus();
                  },
                  decoration: InputDecoration(
                    labelText: "Unit Ke",
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  ),
                  items: infoAppChangeNotifier.listNumberUnit.map((value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }).toList(),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
