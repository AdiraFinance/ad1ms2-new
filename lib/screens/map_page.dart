import 'package:ad1ms2_dev/main.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {

  var _latitude,_longitude;
  final Set<Marker> _markers = {};
  LatLng _currentPosition;
  var _loadMaps = false;
  String _addressFromMap = "";

  @override
  void initState() {
    super.initState();
    _getLocation();
  }

  _getLocation() async{
    setState(() {
      _loadMaps = true;
    });
    Location location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();
    print(_locationData.latitude);
    print(_locationData.longitude);
    setState(() {
      _currentPosition = LatLng(_locationData.latitude, _locationData.longitude);
      _latitude = _locationData.latitude;
      _longitude = _locationData.longitude;
      _loadMaps = false;
    });
    _markers.add(
      Marker(
        markerId:
        MarkerId("${_locationData.latitude}, ${_locationData.longitude}"),
        icon: BitmapDescriptor.defaultMarker,
        position: _currentPosition,
      ),
    );
    _getCurrentLocation(_locationData.latitude, _locationData.longitude);
  }

  // @override
  // void initState() {
  //   super.initState();
  //   _checkAccuracy();
  // }
  //
  // _checkAccuracy() async{
  //   Location _location = Location();
  //   _location.onLocationChanged.listen((LocationData currentLocation) {
  //     print(currentLocation.accuracy);
  //     print("${currentLocation.latitude},${currentLocation.longitude}");
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Set Location", style: TextStyle(color: Colors.black)),
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: myPrimaryColor,
      ),
      body:
      _loadMaps ? Center(child: CircularProgressIndicator()) :GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: CameraPosition(
            target: _currentPosition,
            zoom: 27.0,
          ),
          markers: _markers,
          indoorViewEnabled: true,
          buildingsEnabled: true,
          compassEnabled: true,
          onTap: (position) {
            setState(() {
              _latitude = position.latitude;
              _longitude = position.longitude;
              _markers.clear();
              _markers.add(
                Marker(
                  markerId:
                      MarkerId("${position.latitude}, ${position.longitude}"),
                  icon: BitmapDescriptor.defaultMarker,
                  position: position,
                )
              );
            });
            _getUserLocation(position.latitude, position.longitude);
          }),
      bottomNavigationBar: BottomAppBar(
        child: Container(
            margin: EdgeInsets.only(left: 8, right: 8, bottom: 8, top: 8),
            child: RaisedButton(
              padding: EdgeInsets.only(top: 8, bottom: 8),
              onPressed: () {
                var _value = {
                  "address": _addressFromMap,
                  "latitude": _latitude,
                  "longitude": _longitude
                };
                // widget.setAddress(_value);
                // Navigator.pop(context,_addressFromMap);
                Navigator.pop(context,_value);
              },
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(8.0)),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('SET LOCATION',
                      style: TextStyle(
                          fontFamily: "NunitoSans", color: Colors.black))
                ],
              ),
              color: myPrimaryColor,
            )),
      ),
    );
  }

  _getUserLocation(double lat, double long) async {
    final coordinates = new Coordinates(lat, long);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;
    setState(() {
      _addressFromMap = first.addressLine;
    });
    _showDialog(first.addressLine,1);
  }

  _getCurrentLocation(double lat, double long) async{
    final coordinates = new Coordinates(lat, long);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;
    setState(() {
      _addressFromMap = first.addressLine;
    });
    _showDialog(_addressFromMap,0);
  }

  void _showDialog(String address,int flag) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(flag == 0 ? "Lokasi saat ini" :"Alamat pilihan anda"),
          content: new Text(address,textAlign: TextAlign.justify,style: TextStyle(fontFamily: "NunitoSans")),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
