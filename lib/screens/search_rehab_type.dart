import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_rehab_type_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchRehabType extends StatefulWidget {
  @override
  _SearchRehabTypeState createState() => _SearchRehabTypeState();
}

class _SearchRehabTypeState extends State<SearchRehabType> {

  @override
  void initState() {
    super.initState();
    Provider.of<SearchRehabTypeChangeNotifier>(context,listen: false).getRehabType(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchRehabTypeChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchRehabTypeChangeNotifier>(
            builder: (context, searchRehabTypeChangeNotifier, _) {
              return TextFormField(
                controller: searchRehabTypeChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchRehabTypeChangeNotifier.searchRehabType(query);
                },
                onChanged: (e) {
                  searchRehabTypeChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Jenis Rehab (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchRehabTypeChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchRehabTypeChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchRehabTypeChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchRehabTypeChangeNotifier>(context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchRehabTypeChangeNotifier>(
          builder: (context, searchRehabTypeChangeNotifier, _) {
            return searchRehabTypeChangeNotifier.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchRehabTypeChangeNotifier.listRehabTYpeTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchRehabTypeChangeNotifier.listRehabTYpeTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchRehabTypeChangeNotifier.listRehabTYpeTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchRehabTypeChangeNotifier.listRehabTYpeTemp[index].id} - "
                              "${searchRehabTypeChangeNotifier.listRehabTYpeTemp[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchRehabTypeChangeNotifier.listRehabTYpe.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchRehabTypeChangeNotifier.listRehabTYpe[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchRehabTypeChangeNotifier.listRehabTYpe[index].id} - "
                          "${searchRehabTypeChangeNotifier.listRehabTYpe[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
