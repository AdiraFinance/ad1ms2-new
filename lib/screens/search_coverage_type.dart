import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/coverage1_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/shared/search_coverage_type_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchCoverageType extends StatefulWidget {
  final CoverageModel coverage1;
  final CoverageModel coverage2;
  final String type;
  final ProductModel product;
  final String periodType;

  const SearchCoverageType({this.coverage1, this.coverage2, this.type, this.product, this.periodType});
  @override
  _SearchCoverageTypeState createState() => _SearchCoverageTypeState();
}

class _SearchCoverageTypeState extends State<SearchCoverageType> {
  @override
  void initState() {
    super.initState();
   Provider.of<SearchCoverageTypeChangeNotifier>(context,listen: false).getCoverageType(context,widget.coverage1,widget.coverage2,widget.type,widget.product,widget.periodType);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     key: Provider.of<SearchCoverageTypeChangeNotifier>(context,listen: false).scaffoldKey,
     appBar: AppBar(
       title: Consumer<SearchCoverageTypeChangeNotifier>(
         builder: (context, searchCoverage1ChangeNotifier, _) {
           return TextFormField(
             controller: searchCoverage1ChangeNotifier.controllerSearch,
             style: TextStyle(color: Colors.black),
             textInputAction: TextInputAction.search,
             onFieldSubmitted: (e) {
//            _getCustomer(e);
             },
             onChanged: (e) {
               searchCoverage1ChangeNotifier.changeAction(e);
             },
             cursorColor: Colors.black,
             decoration: new InputDecoration(
               hintText: "Cari Jenis Pertanggungan",
               hintStyle: TextStyle(color: Colors.black),
               enabledBorder: UnderlineInputBorder(
                 borderSide: BorderSide(color: myPrimaryColor),
               ),
               focusedBorder: UnderlineInputBorder(
                 borderSide: BorderSide(color: myPrimaryColor),
               ),
             ),
             autofocus: true,
           );
         },
       ),
       backgroundColor: myPrimaryColor,
       iconTheme: IconThemeData(color: Colors.black),
       actions: <Widget>[
         Provider.of<SearchCoverageTypeChangeNotifier>(context, listen: true)
             .showClear
             ? IconButton(
             icon: Icon(Icons.clear),
             onPressed: () {
               Provider.of<SearchCoverageTypeChangeNotifier>(context,
                   listen: false)
                   .controllerSearch
                   .clear();
               Provider.of<SearchCoverageTypeChangeNotifier>(context,
                   listen: false)
                   .changeAction(Provider.of<SearchCoverageTypeChangeNotifier>(
                   context,
                   listen: false)
                   .controllerSearch
                   .text);
             })
             : SizedBox(
           width: 0.0,
           height: 0.0,
         )
       ],
     ),
     body:
     Consumer<SearchCoverageTypeChangeNotifier>(
       builder: (context, searchCoverage1ChangeNotifier, _) {
         return searchCoverage1ChangeNotifier.loadData
             ?
         Center(child: CircularProgressIndicator())
             :
         SizedBox();
         //   ListView.separated(
         //   padding: EdgeInsets.symmetric(
         //       vertical: MediaQuery.of(context).size.height / 57,
         //       horizontal: MediaQuery.of(context).size.width / 27),
         //   itemCount: searchCoverage1ChangeNotifier.listCoverage1Model.length,
         //   itemBuilder: (listContext, index) {
         //     return InkWell(
         //       onTap: () {
         //         Navigator.pop(context,
         //             searchCoverage1ChangeNotifier.listCoverage1Model[index]);
         //       },
         //       child: Container(
         //         child: Row(
         //           mainAxisSize: MainAxisSize.max,
         //           children: [
         //             Text(
         //               "${searchCoverage1ChangeNotifier.listCoverage1Model[index].PARENT_KODE} - "
         //                   "${searchCoverage1ChangeNotifier.listCoverage1Model[index].DESKRIPSI} ",
         //               style: TextStyle(fontSize: 16),
         //             )
         //           ],
         //         ),
         //       ),
         //     );
         //   },
         //   separatorBuilder: (context, index) {
         //     return Divider();
         //   },
         // );
       },
     ),
    );
  }
}
