import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/dedup/dedup_comp.dart';
import 'package:ad1ms2_dev/screens/dedup/dedup_indi.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:flutter/material.dart';

class DedupMenu extends StatefulWidget {
  @override
  _DedupMenuState createState() => _DedupMenuState();
}

class _DedupMenuState extends State<DedupMenu> {
  Screen _size;
  @override
  Widget build(BuildContext context) {
    _size = Screen(MediaQuery.of(context).size);
    return Theme(
      data: ThemeData(fontFamily: "NunitoSans", accentColor: myPrimaryColor),
      child: ListView(
        padding: EdgeInsets.symmetric(
            horizontal: _size.wp(3), vertical: _size.hp(2)),
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FormDedupIndividu()));
            },
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              elevation: 3.3,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Image.asset(
                      "img/icon_dedup_individu.webp",
                      width: _size.wp(40),
                      height: _size.hp(16),
                    ),
                  ),
                  Text("Individu", style: TextStyle(fontSize: 20))
                ],
              ),
            ),
          ),
          SizedBox(
            height: _size.hp(1.5),
          ),
          InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FormDedupComp()));
            },
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              elevation: 3.3,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Image.asset(
                      "img/icon_dedup_kelembagaan.webp",
                      width: _size.wp(40),
                      height: _size.hp(16),
                    ),
                  ),
                  Text("Kelembagaan", style: TextStyle(fontSize: 20))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
