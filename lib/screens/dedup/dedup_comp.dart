import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_company_parent_old.dart';
import 'package:ad1ms2_dev/screens/ide/ide_dokumen.dart';
import 'package:ad1ms2_dev/screens/list_oid.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class FormDedupComp extends StatefulWidget {
  @override
  _FormDedupCompState createState() => _FormDedupCompState();
}

class _FormDedupCompState extends State<FormDedupComp> {
  bool _autoValidate = false;
  Screen _size;
  final _controllerTglPendirian = TextEditingController();
  final _controllerNPWPNumber = TextEditingController();
  final _controllerName = TextEditingController();
  final _controllerAddress = TextEditingController();
  DateTime _initialDate;
  var _dateNow = DateTime.now();
  final _key = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _initialDate = DateTime(_dateNow.year, _dateNow.month, _dateNow.day);
  }

  @override
  Widget build(BuildContext context) {
    _size = Screen(MediaQuery.of(context).size);
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: myPrimaryColor,
            centerTitle: true,
            title: Text('Dedup Kelembagaan',
                style: TextStyle(color: Colors.black)),
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                vertical: _size.hp(2.5), horizontal: _size.wp(3)),
            child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    controller: _controllerName,
                    autovalidate: _autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Nama Lembaga',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: _size.hp(2.5)),
                  TextFormField(
                    controller: _controllerNPWPNumber,
                    autovalidate: _autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'NPWP',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      // LengthLimitingTextInputFormatter(16)
                    ],
                  ),
                  SizedBox(height: _size.hp(2.5)),
                  TextFormField(
                    controller: _controllerAddress,
                    autovalidate: _autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Alamat NPWP',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: _size.hp(2.5)),
                  TextFormField(
                    autovalidate: _autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: _controllerTglPendirian,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Tanggal Pendirian',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    onTap: () {
                      _showDatePicker();
                    },
                    readOnly: true,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: _size.hp(3.5)),
                  RaisedButton(
                      onPressed: () {
                        _check();
                      },
                      padding: EdgeInsets.symmetric(vertical: _size.hp(1.5)),
                      color: myPrimaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "SUBMIT",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25),
                            )
                          ]))
                ],
              ),
            ),
          )
//        Container(
//          padding: EdgeInsets.zero,
//          child: ListView(
//            children: <Widget>[
//              SizedBox(
//                height: 5.0,
//              ),
//              TextFormField(
//                decoration: textInputDecoration.copyWith(
//                    hintText: 'Input No NPWP', labelText: 'NPWP'),
//                validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
//                onChanged: (val) {
////                setState(() => email = val);
//                },
//              ),
//              SizedBox(
//                height: 5.0,
//              ),
//              TextFormField(
//                decoration: textInputDecoration.copyWith(
//                    hintText: 'Input Nama Lembaga', labelText: 'Nama Lembaga'),
//                validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
//                onChanged: (val) {
////                setState(() => email = val);
//                },
//              ),
//              SizedBox(
//                height: 5.0,
//              ),
//              TextFormField(
//                decoration: textInputDecoration.copyWith(
//                    hintText: 'DD/MMM/YYYY', labelText: 'Tanggal Pendirian'),
//                validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
//                onChanged: (val) {
////                setState(() => email = val);
//                },
//              ),
//              TextFormField(
//                decoration: textInputDecoration.copyWith(
//                    hintText: 'Input Alamat', labelText: 'Alamat'),
//                validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
//                onChanged: (val) {
////                setState(() => email = val);
//                },
//              ),
//              SizedBox(
//                height: 20.0,
//              ),
//              RaisedButton(
//                color: Colors.yellow[800],
//                child: Text(
//                  'Dedup',
//                  style: TextStyle(
//                    color: Colors.white,
//                  ),
//                ),
//                onPressed: () async {
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(builder: (context) => IdeDokumen()),
//                  );
//                },
//              ),
//            ],
//          ),
//        ),
          ),
    );
  }

  _check() {
    final _form = _key.currentState;
    if (_form.validate()) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => ListOid(flag: "COM",initialDateBirthDate: _initialDate,fullname: _controllerName.text,identityNumber: _controllerNPWPNumber.text,identityAddress: _controllerAddress.text,birthDate: _controllerTglPendirian.text,)));
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  _showDatePicker() async {
    // DatePickerShared _datePicker = DatePickerShared();
    // var _dateSelected = await _datePicker.selectStartDate(context, _initialDate,
    //     canAccessNextDay: false);
    // if (_dateSelected != null) {
    //   setState(() {
    //     _initialDate = _dateSelected;
    //     _controllerTglPendirian.text = dateFormat.format(_dateSelected);
    //   });
    // } else {
    //   return;
    // }
    var _dateSelected = await selectDate(context, _initialDate);
    if (_dateSelected != null) {
      setState(() {
        _initialDate = _dateSelected;
        _controllerTglPendirian.text = dateFormat.format(_dateSelected);
      });
    } else {
      return;
    }
  }
}
