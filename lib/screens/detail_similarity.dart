import 'package:ad1ms2_dev/main.dart';
import 'package:flutter/material.dart';

class DetailSimilarity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(
            color: Colors.black
        ),
        title: Text(
          "Detail Kemiripan",
          style: TextStyle(
              color: Colors.black
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(child: Text("Nomor Identitas"),flex: 4,),
                Text(" : "),
                Expanded(child: Text("35150123495780001"),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Nama Lengkap"),flex: 4,),
                Text(" : "),
                Expanded(child: Text("Wahyu Tri Ramadani"),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Tanggal Lahir"),flex: 4,),
                Text(" : "),
                Expanded(child: Text("22-06-1979"),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Tempat Lahir"),flex: 4,),
                Text(" : "),
                Expanded(child: Text("Sukabumi"),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Nama Gadis Ibu Kandung"),flex: 4,),
                Text(" : "),
                Expanded(child: Text("Gadis"),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Alamat"),flex: 4,),
                Text(" : "),
                Expanded(child: Text("Jl Nanas no 57 Surabaya"),flex: 6,),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
