import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_parent.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/task_list_change_notifier/task_list_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class TaskList extends StatefulWidget {
  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  @override
  void initState() {
    super.initState();
    Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskListChangeNotifier>(
      builder: (context, value, child) {
        return Theme(
          data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            // fontFamily: "NunitoSans",
          ),
          child: Scaffold(
            key: Provider.of<TaskListChangeNotifier>(context,listen: false).scaffoldKey,
            body: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                    right: MediaQuery.of(context).size.width / 37,
                    left: MediaQuery.of(context).size.width / 37,
                    top:  MediaQuery.of(context).size.height / 47,
                    bottom:  MediaQuery.of(context).size.height / 47
                  ),
                  child: TextFormField(
                    onFieldSubmitted: (query){},
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    textInputAction: TextInputAction.search,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                      prefixIcon: Icon(Icons.search,color: Colors.black,),
                        labelText: 'Search task list',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                  ),
                ),
                Expanded(
                  child: value.taskList.isEmpty
                  ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                        // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                        Text("Tidak Ada Task List", style: TextStyle(color: Colors.grey, fontSize: 16),)
                      ],
                    ),
                  )
                  : GridView.builder(
                    itemCount: value.taskList.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 6,
                        mainAxisSpacing: 6,
                        childAspectRatio: 0.90
                    ),
                    padding: EdgeInsets.symmetric(
                        // vertical: MediaQuery.of(context).size.height / 87,
                        horizontal: MediaQuery.of(context).size.width / 37),
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: (){
                          // value.navigateForm(value.taskList[index].LAST_KNOWN_STATE, context);
                          navigateForm(value.taskList[index].LAST_KNOWN_STATE,
                              context,
                              value.taskList[index].ORDER_NO,
                              value.taskList[index].ORDER_DATE.toString(),
                              value.taskList[index].CUST_NAME,
                              value.taskList[index].CUST_TYPE
                          );
                        },
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          color: "${value.taskList[index].LAST_KNOWN_STATE}" == "IDE"
                              ? primaryColorIDE
                              : "${value.taskList[index].LAST_KNOWN_STATE}" == "SA" || "${value.taskList[index].LAST_KNOWN_STATE}" == "IA" || "${value.taskList[index].LAST_KNOWN_STATE}" == "AOS" || "${value.taskList[index].LAST_KNOWN_STATE}" == "SARS"
                              ? primaryColorMiniForm
                              : "${value.taskList[index].LAST_KNOWN_STATE}" == "SVY"
                              ? primaryColorRegulerSurvey
                              : "${value.taskList[index].LAST_KNOWN_STATE}" == "PAC"
                              ? primaryColorPAC
                              : "${value.taskList[index].LAST_KNOWN_STATE}" == "DKR"
                              ? primaryColorDataKoreksi
                              : "${value.taskList[index].LAST_KNOWN_STATE}" == "RSVY"
                              ? primaryColorResurvey
                              : primaryColorIDE,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(13, 8, 13, 0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  // margin: EdgeInsets.only(right: size.wp(4)),
                                  child: Align(
                                    alignment: Alignment.topRight,
                                    child: Text(
                                      "${value.taskList[index].LAST_KNOWN_STATE}",
                                      style: TextStyle(
                                        color: Colors.white70,
                                        letterSpacing: 0.2,
                                        fontSize: 40,
                                        fontWeight: FontWeight.w900,
                                        fontStyle: FontStyle.italic,
                                        fontFamily: "YuGoth"
                                      ),
                                    )
                                  ),
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 177),
                                Text(
                                  value.taskList[index].ORDER_NO,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 0.2,
                                    fontSize: 16,
                                    fontFamily: "NunitoSans",
                                  ),
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 177),
                                Text(
                                  "${value.taskList[index].CUST_NAME}",
                                  style: TextStyle(
                                      fontSize: 15,
                                      letterSpacing: 0.2,
                                      color: Colors.white,
                                      fontFamily: "NunitoSans",
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 177),
                                Text(
                                  "${dateFormat2.format(value.taskList[index].ORDER_DATE)}",
                                  style: TextStyle(
                                    color: Colors.white,
                                    letterSpacing: 0.2,
                                    fontSize: 14,
                                  ),
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 47),
                                Container(
                                  decoration: BoxDecoration(
                                    color: value.taskList[index].PRIORITY == "3"
                                        ?
                                    Color(0xff1B5E20)
                                        :
                                    value.taskList[index].PRIORITY == "2"
                                        ?
                                    Color(0xffE65100)
                                    // Color(0xff46be8a)
                                    // Color(0xffec7f00)
                                    // Color(0xffdc3030)
                                        :
                                    Color(0xffB71C1C),
                                    borderRadius: new BorderRadius.circular(25),
                                    // border: Border.all(color: Colors.white)
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: MediaQuery.of(context).size.width / 37,
                                        vertical: MediaQuery.of(context).size.height / 177
                                    ),
                                    child: Text(value.taskList[index].PRIORITY == "3" ? "Low Priority" : value.taskList[index].PRIORITY == "2" ? "Medium Priority" : "High Priority",
                                      style: TextStyle(
                                          color: Colors.white,
                                          letterSpacing: 0.2,
                                          fontFamily: "NunitoSans",
                                      ),
                                    ),
                                    // child: Text("Low Priority"
                                    //     "${value.taskList[index].PRIORITY}",
                                    //   style: TextStyle(
                                    //       color: Colors.white
                                    //   ),
                                    // ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
