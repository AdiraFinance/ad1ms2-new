import 'dart:async';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/detail_similarity.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_credit_limit.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_parent.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'form_m/form_m_parent_old.dart';
import 'form_m_company/form_m_company_parent.dart';
import 'form_m_company/form_m_company_parent_old.dart';

class ListOid extends StatefulWidget {
  final String flag;
  final String identityNumber;
  final String fullname;
  final String birthDate;
  final String birthPlace;
  final String motherName;
  final String identityAddress;
  final DateTime initialDateBirthDate;

  const ListOid({this.flag, this.identityNumber, this.fullname, this.birthDate, this.birthPlace, this.motherName, this.identityAddress, this.initialDateBirthDate});

  @override
  _ListOidState createState() => _ListOidState();
}

class _ListOidState extends State<ListOid> {
  var _dataDedup;
  // int _selectedIndex = -1;
  // GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // var _listOid = [
  //   {"nama": "Wahyu Tri Ramadan", "nik": "3515013012880001", "score": 95, "tempat_tinggal": "Jakarta", "tanggal_lahir": "30-12-1988", "ibu": "Susi Sujanah", "alamat": "Bandung Raya no.14, Bandung, Jawa Barat"},
  //   {"nama": "Bambang Harianto", "nik": "3215010412900001", "score": 60, "tempat_tinggal": "Bandung", "tanggal_lahir": "04-12-1990", "ibu": "Putri Kamila", "alamat": "Dagoo no.02, Bandung, Jawa Barat"},
  //   {"nama": "Dudi Sukarman", "nik": "3215012407890002", "score": 45, "tempat_tinggal": "Purwakarta", "tanggal_lahir": "30-12-1989", "ibu": "Dhila Izza", "alamat": "Jakarta Raya no.33, Jakarta Pusat, DKI Jakarta"},
  // ];

  @override
  void initState() {
    super.initState();
    Provider.of<ListOidChangeNotifier>(context, listen: false).customerType = widget.flag;
    if(widget.flag == "PER"){
      Provider.of<ListOidChangeNotifier>(context, listen: false)
          .getListOid(
        widget.identityNumber,
        widget.fullname,
        widget.initialDateBirthDate,
        widget.birthPlace,
        widget.motherName,
        widget.identityAddress,
      );
    }
    else{
      Provider.of<ListOidChangeNotifier>(context, listen: false)
          .getListOidCompany(
        widget.identityNumber,
        widget.fullname,
        widget.initialDateBirthDate,
        widget.identityAddress
      );
    }
    _dataDedup = {
      "identity_number":widget.identityNumber,
      "full_name":widget.fullname,
      "birth_place":widget.birthPlace,
      "birth_date":widget.birthDate,
      "mother_name":widget.motherName,
      "initial_date_birth_date":widget.initialDateBirthDate
    };
    // Timer(Duration(milliseconds: 300), () {
    //   _checkAndShowDialogInfoSimilarity();
    // });
  }

  // _checkAndShowDialogInfoSimilarity() async {
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  //   if (_preferences.getString("isFirstTimeDialogShow") == null) {
  //     _showDialog();
  //   } else {
  //     return;
  //   }
  // }

  // _updateStatusShowDialogSimilarity() async {
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  //   _preferences.setString("isFirstTimeDialogShow", "no");
  // }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor,
        primaryColor: myPrimaryColor,
      ),
      child: Consumer<ListOidChangeNotifier>(
        builder: (context, listOidChangeNotif, _) {
          return Scaffold(
            key: listOidChangeNotif.scaffoldKey,
            appBar: AppBar(
              title: Text("Hasil Dedup"),
              centerTitle: true,
              actions: <Widget>[
                // IconButton(
                //     icon: Icon(Icons.info_outline, color: Colors.black),
                //     onPressed: () {
                //       _showDialog();
                //     })
              ],
            ),
            body: listOidChangeNotif.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            widget.flag == "PER"
                ?
            listOidChangeNotif.listOid.isEmpty
                ?
            Center(child: Text("Tidak ada OID", style: TextStyle(color: Colors.grey, fontSize: 16)))
                :
            ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  itemBuilder: (context, index) {
                    Color _color;
                    double _score = double.parse(listOidChangeNotif.listOid[index].SCORE);

                    if (_score > 70) {
                      _color = Colors.green;
                    }
                    if (_score <= 60) {
                      _color = Colors.orange;
                    }
                    if (_score <= 50) {
                      _color = Colors.red;
                    }
                    return InkWell(
                      // onLongPress: () {
                      //   setState(() {
                      //     _selectedIndex = index;
                      //   });
                      // },
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailSimilarity()));
                      },
                      child: Card(
                          elevation: 3.3,
                          shape: (listOidChangeNotif.selectedIndex == index)
                              ? RoundedRectangleBorder(
                              side: BorderSide(color: primaryOrange, width: 2),
                              borderRadius:
                              BorderRadius.all(Radius.circular(4)))
                              : null,
                          child: Padding(
                              padding: EdgeInsets.all(13.0),
                              child: Stack(
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        children: [
                                          Text("${listOidChangeNotif.listOid[index].AC_CUST_NAME} - skor. ", style: TextStyle(fontWeight: FontWeight.bold),),
                                          Text("${listOidChangeNotif.listOid[index].SCORE}", style: TextStyle(color: _color, fontSize: 18, fontWeight: FontWeight.bold),),
                                        ],
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height / 127,),
                                      Text(listOidChangeNotif.listOid[index].AC_ID_NO, style: TextStyle(color: Colors.grey),),
                                      SizedBox(height: MediaQuery.of(context).size.height / 97,),
                                      Row(
                                        children: [
                                          Text(listOidChangeNotif.listOid[index].AC_PLACE_BIRTH, style: TextStyle(color: Colors.grey, fontSize: 13),),
                                          Text(", "+ listOidChangeNotif.listOid[index].AC_DATE_BIRTH, style: TextStyle(color: Colors.grey, fontSize: 13),),
                                        ],
                                      ),
                                      Text("Ibu " +listOidChangeNotif.listOid[index].AC_MOTHER_NAME, style: TextStyle(color: Colors.grey, fontSize: 13),),
                                      Text(listOidChangeNotif.listOid[index].AC_ADDRESS, style: TextStyle(color: Colors.grey, fontSize: 13),),
                                    ],
                                  ),
                                  Align(
                                      alignment: Alignment.topRight,
                                      child: PopupMenuButton<String>(
                                          onSelected: (value) {
                                              listOidChangeNotif.selectedIndex = index;
                                              listOidChangeNotif.listOIdPersonalSelected = listOidChangeNotif.listOid[index];
                                          },
                                          itemBuilder: (context) {
                                            return PilihOIDMenuButton.choices.map((String choice) {
                                              return PopupMenuItem(
                                                value: choice,
                                                child: Text(choice),
                                              );
                                            }).toList();
                                          },
                                          child: Icon(Icons.more_vert, color: Colors.grey)
                                      )
                                  )
                                ],
                              )
                          )),
                    );
                  },
                  itemCount: listOidChangeNotif.listOid.length)
                :
            listOidChangeNotif.listOIDCompany.isEmpty
                ?
            Center(child: Text("Tidak ada OID", style: TextStyle(color: Colors.grey, fontSize: 16)))
                :
            ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                itemBuilder: (context, index) {
                  Color _color;
                  double _score = double.parse(listOidChangeNotif.listOid[index].SCORE);

                  if (_score > 70) {
                    _color = Colors.green;
                  }
                  if (_score <= 60) {
                    _color = Colors.orange;
                  }
                  if (_score <= 50) {
                    _color = Colors.red;
                  }
                  return InkWell(
                    // onLongPress: () {
                    //   setState(() {
                    //     _selectedIndex = index;
                    //   });
                    // },
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailSimilarity()));
                    },
                    child: Card(
                        elevation: 3.3,
                        shape: (listOidChangeNotif.selectedIndex == index)
                            ? RoundedRectangleBorder(
                            side: BorderSide(color: primaryOrange, width: 2),
                            borderRadius:
                            BorderRadius.all(Radius.circular(4)))
                            : null,
                        child: Padding(
                            padding: EdgeInsets.all(13.0),
                            child: Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Text("${listOidChangeNotif.listOIDCompany[index].AC_CUST_NAME} - skor. ", style: TextStyle(fontWeight: FontWeight.bold),),
                                        Text("${listOidChangeNotif.listOIDCompany[index].SCORE}", style: TextStyle(color: _color, fontSize: 18, fontWeight: FontWeight.bold),),
                                      ],
                                    ),
                                    SizedBox(height: MediaQuery.of(context).size.height / 127,),
                                    Text(listOidChangeNotif.listOIDCompany[index].AC_NPWP_NUMBER, style: TextStyle(color: Colors.grey),),
                                    SizedBox(height: MediaQuery.of(context).size.height / 97,),
                                    Row(
                                      children: [
                                        Text(listOidChangeNotif.listOIDCompany[index].AC_PLACE_BIRTH, style: TextStyle(color: Colors.grey, fontSize: 13),),
                                        Text(", "+ listOidChangeNotif.listOIDCompany[index].AC_ESTABILISHED_DATE, style: TextStyle(color: Colors.grey, fontSize: 13),),
                                      ],
                                    ),
                                    Text("Ibu " +listOidChangeNotif.listOid[index].AC_MOTHER_NAME, style: TextStyle(color: Colors.grey, fontSize: 13),),
                                    Text(listOidChangeNotif.listOid[index].AC_ADDRESS, style: TextStyle(color: Colors.grey, fontSize: 13),),
                                  ],
                                ),
                                Align(
                                    alignment: Alignment.topRight,
                                    child: PopupMenuButton<String>(
                                        onSelected: (value) {
                                            listOidChangeNotif.selectedIndex = index;
                                            listOidChangeNotif.listOIdCompanySelected = listOidChangeNotif.listOIDCompany[index];
                                        },
                                        itemBuilder: (context) {
                                          return PilihOIDMenuButton.choices.map((String choice) {
                                            return PopupMenuItem(
                                              value: choice,
                                              child: Text(choice),
                                            );
                                          }).toList();
                                        },
                                        child: Icon(Icons.more_vert, color: Colors.grey)
                                    )
                                )
                              ],
                            )
                        )),
                  );
                },
                itemCount: listOidChangeNotif.listOIDCompany.length) ,
            bottomNavigationBar: BottomAppBar(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0)),
                            color: myPrimaryColor,
                            onPressed: () {
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(flag: widget.flag)));
                              // if (_selectedIndex != -1) {
                                if (widget.flag != "COM") {
                                  Provider.of<ListOidChangeNotifier>(context,listen: false).setValueFromDedup(context, _dataDedup);
                                  listOidChangeNotif.submitNewOid(context, widget.identityNumber,
                                    widget.fullname,
                                    widget.initialDateBirthDate,
                                    widget.birthPlace,
                                    widget.motherName,
                                    widget.identityAddress,
                                    widget.flag);
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(flag: widget.flag)));
                                } else {
                                  Provider.of<ListOidChangeNotifier>(context,listen: false).setValueCompanyFromDedup(context, _dataDedup);
                                  listOidChangeNotif.submitNewOidCompany(context, widget.flag,
                                      widget.identityNumber,
                                      widget.fullname,
                                      widget.initialDateBirthDate,
                                      widget.identityAddress);
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(flag: widget.flag)));
                                }
                              // }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("OID BARU",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25)
                                )
                              ],
                            )),
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width / 37),
                      Expanded(
                        flex: 5,
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0)),
                            color: myPrimaryColor,
                            onPressed: () {
                              if (listOidChangeNotif.selectedIndex != -1) {
                                if (widget.flag == "PER") {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCreditLimit(flag: widget.flag,model: listOidChangeNotif.listOIdPersonalSelected)));
                                } else {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(flag: widget.flag)));
                                }
                              }
                              else {
                                listOidChangeNotif.showSnackBar("Pilih satu OID untuk melanjutkan proses");
                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("NEXT",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25)
                                )
                              ],
                            )
                        ),
                      ),
                    ],
                  )
              ),
            ),
            // bottomNavigationBar: BottomAppBar(
            //   elevation: 0.0,
            //   child: Container(
            //     padding: EdgeInsets.symmetric(
            //         horizontal: MediaQuery.of(context).size.width / 27,
            //         vertical: MediaQuery.of(context).size.height / 57),
            //     child: RaisedButton(
            //         color: myPrimaryColor,
            //         shape: RoundedRectangleBorder(
            //             borderRadius: BorderRadius.circular(8)
            //         ),
            //         onPressed: () {
            //           if (_selectedIndex != -1) {
            //             if (widget.flag == "PER") {
            //               Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCreditLimit(flag: widget.flag)));
            //             } else {
            //               Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(flag: widget.flag)));
            //             }
            //           }
            //           else {
            //             _showSnackBar("Pilih satu OID untuk melanjutkan proses");
            //           }
            //         },
            //         child: Row(
            //             mainAxisAlignment: MainAxisAlignment.center,
            //             children: [Text("NEXT",
            //                 style: TextStyle(
            //                   color: Colors.black,
            //                   fontSize: 14,
            //                   fontWeight: FontWeight.w500,
            //                   letterSpacing: 1.25
            //                 )
            //             )]
            //         )
            //     ),
            //   ),
            // ),
          );
        }
      ),
    );
  }
  // _showDialog() async {
  //   showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext context) {
  //         return Theme(
  //           data: ThemeData(fontFamily: "NunitoSans"),
  //           child: AlertDialog(
  //             title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
  //             content: Column(
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               mainAxisSize: MainAxisSize.min,
  //               children: <Widget>[
  //                 Text(
  //                   "∙ Tekan 1x untuk melihat detail kemiripan",
  //                 ),
  //                 Text(
  //                   "∙ Tekan lama untuk memilih OID",
  //                 ),
  //                 SizedBox(
  //                   height: MediaQuery.of(context).size.height / 37,
  //                 ),
  //                 Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
  //               ],
  //             ),
  //             actions: <Widget>[
  //               FlatButton(
  //                   onPressed: () {
  //                     Navigator.pop(context);
  //                     _updateStatusShowDialogSimilarity();
  //                   },
  //                   child: Text("CLOSE",
  //                       style: TextStyle(
  //                           color: primaryOrange,
  //                           fontSize: 14,
  //                           fontWeight: FontWeight.w500,
  //                           letterSpacing: 1.25)))
  //             ],
  //           ),
  //         );
  //       });
  // }
  //
  // _showSnackBar(String text){
  //   this._scaffoldKey.currentState.showSnackBar(new SnackBar(
  //       content: Text("$text"), behavior: SnackBarBehavior.floating,
  //       backgroundColor: snackbarColor, duration: Duration(seconds: 2))
  //   );
  // }
}
