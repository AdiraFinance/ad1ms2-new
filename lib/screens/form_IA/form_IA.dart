import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/survey/result_survey.dart';
import 'package:ad1ms2_dev/shared/form_IA_change_notifier/form_IA_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormIA extends StatefulWidget {
  @override
  _FormIAState createState() => _FormIAState();
}

class _FormIAState extends State<FormIA> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            "Form Instant Approved",
          style: TextStyle(
              color: Colors.black
          ),
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(
          color: Colors.black
        ),
      ),
      body: Consumer<FormIAChangeNotifier>(
        builder: (context, value, child) {
          return SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
//              autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
//              keyboardType: TextInputType.number,
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//              ],
                  controller: value.controllerName,
                  style: new TextStyle(color: Colors.black),
                  enabled: false,
                  decoration: new InputDecoration(
                      labelText: 'Nama',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
//              autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
//              keyboardType: TextInputType.number,
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//              ],
                  controller: value.controllerAddress,
                  style: new TextStyle(color: Colors.black),
                  enabled: false,
                  decoration: new InputDecoration(
                      labelText: 'Alamat',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
//              autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
//              keyboardType: TextInputType.number,
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//              ],
                  controller: value.controllerAppNumber,
                  style: new TextStyle(color: Colors.black),
                  enabled: false,
                  decoration: new InputDecoration(
                      labelText: 'No Aplikasi',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
//              autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
//              keyboardType: TextInputType.number,
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//              ],
                  controller: value.controllerModelObject,
                  style: new TextStyle(color: Colors.black),
                  enabled: false,
                  decoration: new InputDecoration(
                      labelText: 'Model Objek',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Text("Sudah dilakukan sign PK?"),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Row(
                  children: [
                    Row(
                      children: [
                        Radio(
                            activeColor: primaryOrange,
                            value: 0,
                            groupValue: value.radioValueIsSignPK,
                            onChanged: (data) {
                              value.radioValueIsSignPK = data;
                            }),
                        Text("Ya")
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Row(
                      children: [
                        Radio(
                            activeColor: primaryOrange,
                            value: 1,
                            groupValue:
                            value.radioValueIsSignPK,
                            onChanged: (data) {
                              value.radioValueIsSignPK = data;
                            }),
                        Text("Tidak")
                      ],
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0.0,
        child: Container(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(8.0)),
              color: myPrimaryColor,
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => ResultSurvey()), (Route<dynamic> route) => false);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("SUBMIT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.25))
                ],
              )),
        ),
      ),
    );
  }
}
