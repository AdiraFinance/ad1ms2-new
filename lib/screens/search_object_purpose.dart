import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_object_purpose_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchObjectPurpose extends StatefulWidget {
  final String flag;

  const SearchObjectPurpose({this.flag});
  @override
  _SearchObjectPurposeState createState() => _SearchObjectPurposeState();
}

class _SearchObjectPurposeState extends State<SearchObjectPurpose> {


    @override
    void initState() {
      super.initState();
      Provider.of<SearchObjectPurposeChangeNotifier>(context,listen: false).getObjectPurpose(context,widget.flag);
    }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchObjectPurposeChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchObjectPurposeChangeNotifier>(
            builder: (context, searchObjectPurposeChangeNotifier, _) {
              return TextFormField(
                controller: searchObjectPurposeChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchObjectPurposeChangeNotifier.searchGroupObject(query);
//            _getCustomer(e);
                },
                onChanged: (e) {
                  searchObjectPurposeChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Tujuan Penggunaan Objek (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchObjectPurposeChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchObjectPurposeChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchObjectPurposeChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchObjectPurposeChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchObjectPurposeChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchObjectPurposeChangeNotifier>(
          builder: (context, searchObjectPurposeChangeNotifier, _) {
            return searchObjectPurposeChangeNotifier.loadData
                ?
            Center(child: CircularProgressIndicator(),)
                :
            searchObjectPurposeChangeNotifier.listObjectPurposeTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchObjectPurposeChangeNotifier.listObjectPurposeTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchObjectPurposeChangeNotifier
                            .listObjectPurposeTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchObjectPurposeChangeNotifier.listObjectPurposeTemp[index].id} - "
                              "${searchObjectPurposeChangeNotifier.listObjectPurposeTemp[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
                  searchObjectPurposeChangeNotifier.listObjectPurpose.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchObjectPurposeChangeNotifier
                            .listObjectPurpose[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchObjectPurposeChangeNotifier.listObjectPurpose[index].id} - "
                          "${searchObjectPurposeChangeNotifier.listObjectPurpose[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
