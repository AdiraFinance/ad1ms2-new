import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/list_pemegang_saham_kelembagaan_alamat.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMCompanyPemegangSahamKelembagaanAlamatProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(
      builder: (context, formMCompanyPemegangSahamKelembagaanAlamatChangeNotif, _) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: myPrimaryColor,
              fontFamily: "NunitoSans",
              primarySwatch: Colors.yellow),
          child: Scaffold(
            appBar: AppBar(
              title:
              Text("Pemegang Saham Kelembagaan Alamat", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.keyForm,
              onWillPop: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Column(
                    children: [
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ListPemegangSahamKelembagaanAddress()));
                        },
                        controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerAddress,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Alamat',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        readOnly: true,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerAddressType,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            filled: true,
                            fillColor: Colors.black12,
                            labelText: 'Jenis Alamat',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Row(
                        children: [
                          Expanded(
                            flex: 5,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerRT,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'RT',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                          SizedBox(width: 8),
                          Expanded(
                              flex: 5,
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerRW,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    filled: true,
                                    fillColor: Colors.black12,
                                    labelText: 'RW',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                              ))
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      FocusScope(
                        node: FocusScopeNode(),
                        child: TextFormField(
                          autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerKelurahan,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Kelurahan',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerKecamatan,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            filled: true,
                            fillColor: Colors.black12,
                            labelText: 'Kecamatan',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerKota,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.black12,
                            labelText: 'Kabupaten/Kota',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Row(
                        children: [
                          Expanded(
                            flex: 7,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerProvinsi,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Provinsi',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                          SizedBox(width: 8),
                          Expanded(
                            flex: 3,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller:
                              formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerPostalCode,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Kode Pos',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Row(
                        children: [
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                LengthLimitingTextInputFormatter(10),
                              ],
                              controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerTelepon1Area,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Telepon 1 (Area)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width / 37),
                          Expanded(
                            flex: 6,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                LengthLimitingTextInputFormatter(10),
                              ],
                              controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerTelepon1,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Telepon 1',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Row(
                        children: [
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                LengthLimitingTextInputFormatter(10),
                              ],
                              controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerTelepon2Area,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Telepon 2 (Area)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width / 37),
                          Expanded(
                            flex: 6,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                LengthLimitingTextInputFormatter(10),
                              ],
                              controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerTelepon2,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Telepon 2',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Row(
                        children: [
                          Expanded(
                            flex: 4,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                LengthLimitingTextInputFormatter(10),
                              ],
                              controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerFaxArea,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Fax (Area)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width / 37),
                          Expanded(
                            flex: 6,
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                LengthLimitingTextInputFormatter(10),
                              ],
                              controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerFax,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Fax',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(
                    builder: (context, formMCompanyPemegangSahamKelembagaanAlamatChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          ),
        );
      },
    );
  }
}
