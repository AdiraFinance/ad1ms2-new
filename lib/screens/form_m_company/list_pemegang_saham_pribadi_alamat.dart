import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_pemegang_saham_pribadi_alamat.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListPemegangSahamPribadiAlamat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(fontFamily: "NunitoSans"),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Korespondensi",
            style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
            IconButton(
                icon: Icon(Icons.info_outline),
                onPressed: (){
                  Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: true).iconShowDialog(context);
                })
          ],
        ),
        body: Consumer<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(
          builder: (context, formMCompanyPemegangSahamPribadiAlamatNotif, _) {
            return ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  onLongPress: () {
                    formMCompanyPemegangSahamPribadiAlamatNotif.selectedIndex = index;
                    formMCompanyPemegangSahamPribadiAlamatNotif.controllerAddress.clear();
                    formMCompanyPemegangSahamPribadiAlamatNotif.setAddress(formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index]);
                    Navigator.pop(context);
                  },
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChangeNotifierProvider(
                          create: (context) => FormMAddAddressIndividuChangeNotifier(),
                          child: CompanyAddPemegangSahamPribadiAlamat(
                              flag: 1,
                              index: index,
                              addressModel: formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index],
                              typeAddress: 5,
                          )
                        )
                      )
                    );
                  },
                  child: Card(
                    shape: formMCompanyPemegangSahamPribadiAlamatNotif.selectedIndex == index
                        ? RoundedRectangleBorder(
                          side: BorderSide(color: primaryOrange, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].jenisAlamatModel.DESKRIPSI, style: TextStyle(fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].address}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].kelurahanModel.KEC_NAME}, ${formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].kelurahanModel.KABKOT_NAME}, ${formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].kelurahanModel.PROV_NAME}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].kelurahanModel.ZIPCODE}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].areaCode} ${formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].phone}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),)
                              ],
                            ),
                            Align(
                                alignment: Alignment.topRight,
                                child:
                                // formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                                //     ? formMinfoAlamat.listAlamatKorespondensi[index].isSameWithIdentity
                                //     ? SizedBox()
                                //     :
                                GestureDetector(
                                  // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                    onTap: () {formMCompanyPemegangSahamPribadiAlamatNotif.moreDialog(context, index);},
                                    child: Icon(Icons.more_vert, color: Colors.grey)
                                )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
//                            Align(
//                              alignment: Alignment.topRight,
//                              child: formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE == "03" || formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE == "01"
//                                  ? SizedBox()
//                                  : GestureDetector(
//                                      onTap: () {formMCompanyPemegangSahamPribadiAlamatNotif.deletePemegangSahamPribadiAddress(context, index);},
//                                      child: Icon(Icons.delete, color: Colors.red)
//                                  )
//                            )
                          ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: formMCompanyPemegangSahamPribadiAlamatNotif.listPemegangSahamPribadiAddress.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ChangeNotifierProvider(
                      create: (context) => FormMAddAddressIndividuChangeNotifier(),
                      child: CompanyAddPemegangSahamPribadiAlamat(
                        flag: 0,
                        index: null,
                        addressModel: null,
                        typeAddress: 5,
                      )
                  )
              )
            ).then((value) => Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false).isShowDialog(context));
          },
          backgroundColor: myPrimaryColor,
          child: Icon(Icons.add, color: Colors.black),
        ),
      ),
    );
  }
}
