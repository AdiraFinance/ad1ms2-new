import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormMCompanyPendapatanProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange,
      ),
      child: Consumer<FormMCompanyPendapatanChangeNotifier>(
        builder: (context, formMCompanyPendapatanChangeNotif, _) {
          return Scaffold(
            body: ListView(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height/57,
                  horizontal: MediaQuery.of(context).size.width/37
              ),
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPendapatanChangeNotif.controllerMonthlyIncome,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Pendapatan Perbulan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        formMCompanyPendapatanChangeNotif.controllerMonthlyIncome.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      onTap: () {
                        formMCompanyPendapatanChangeNotif.formattingCompany();
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        formMCompanyPendapatanChangeNotif.amountValidator
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPendapatanChangeNotif.controllerOtherIncome,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Pendapatan Lainnya',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        formMCompanyPendapatanChangeNotif.controllerOtherIncome.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      onTap: () {
                        formMCompanyPendapatanChangeNotif.formattingCompany();
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        formMCompanyPendapatanChangeNotif.amountValidator
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      enabled: false,
                      // autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      // validator: (e) {
                      //   if (e.isEmpty) {
                      //     return "Tidak boleh kosong";
                      //   } else {
                      //     return null;
                      //   }
                      // },
                      controller: formMCompanyPendapatanChangeNotif.controllerTotalIncome,
                      style: new TextStyle(color: Colors.black),
                      textAlign: TextAlign.end,
                      decoration: new InputDecoration(
                          labelText: 'Total Pendapatan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textCapitalization: TextCapitalization.characters,
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPendapatanChangeNotif.controllerCostOfRevenue,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Harga Pokok Penjualan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        formMCompanyPendapatanChangeNotif.controllerCostOfRevenue.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      onTap: () {
                        formMCompanyPendapatanChangeNotif.formattingCompany();
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        formMCompanyPendapatanChangeNotif.amountValidator
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      enabled: false,
                      // autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      // validator: (e) {
                      //   if (e.isEmpty) {
                      //     return "Tidak boleh kosong";
                      //   } else {
                      //     return null;
                      //   }
                      // },
                      controller: formMCompanyPendapatanChangeNotif.controllerGrossProfit,
                      style: new TextStyle(color: Colors.black),
                      textAlign: TextAlign.end,
                      decoration: new InputDecoration(
                          labelText: 'Laba Kotor',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textCapitalization: TextCapitalization.characters,
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPendapatanChangeNotif.controllerOperatingCosts,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Biaya Operasional',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        formMCompanyPendapatanChangeNotif.controllerOperatingCosts.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      onTap: () {
                        formMCompanyPendapatanChangeNotif.formattingCompany();
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        formMCompanyPendapatanChangeNotif.amountValidator
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPendapatanChangeNotif.controllerOtherCosts,
                      style:  TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Biaya Lainnya',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        formMCompanyPendapatanChangeNotif.controllerOtherCosts.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      onTap: () {
                        formMCompanyPendapatanChangeNotif.formattingCompany();
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        formMCompanyPendapatanChangeNotif.amountValidator
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      enabled: false,
                      // autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      // validator: (e) {
                      //   if (e.isEmpty) {
                      //     return "Tidak boleh kosong";
                      //   } else {
                      //     return null;
                      //   }
                      // },
                      controller: formMCompanyPendapatanChangeNotif.controllerNetProfitBeforeTax,
                      style: new TextStyle(color: Colors.black),
                      textAlign: TextAlign.end,
                      decoration: new InputDecoration(
                          labelText: 'Laba Bersih Sebelum Pajak',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textCapitalization: TextCapitalization.characters,
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPendapatanChangeNotif.controllerTax,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Pajak',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        formMCompanyPendapatanChangeNotif.controllerTax.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      onTap: () {
                        formMCompanyPendapatanChangeNotif.formattingCompany();
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        formMCompanyPendapatanChangeNotif.amountValidator
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      enabled: false,
                      // autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      // validator: (e) {
                      //   if (e.isEmpty) {
                      //     return "Tidak boleh kosong";
                      //   } else {
                      //     return null;
                      //   }
                      // },
                      controller: formMCompanyPendapatanChangeNotif.controllerNetProfitAfterTax,
                      style: new TextStyle(color: Colors.black),
                      textAlign: TextAlign.end,
                      decoration: new InputDecoration(
                          labelText: 'Laba Bersih Setelah Pajaks',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textCapitalization: TextCapitalization.characters,
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    TextFormField(
                      autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPendapatanChangeNotif.controllerOtherInstallments,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Angsuran Lainnya',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        formMCompanyPendapatanChangeNotif.controllerOtherInstallments.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      onTap: () {
                        formMCompanyPendapatanChangeNotif.formattingCompany();
                        formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                        formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                        formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        formMCompanyPendapatanChangeNotif.amountValidator
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                  ],
                )
              ],
            ),
          );
        },
      )
    );
  }
}
