import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_manajemen_pic_alamat.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListManajemenPICAlamat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(fontFamily: "NunitoSans"),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Korespondensi",
            style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
            // IconButton(
            //     icon: Icon(Icons.info_outline),
            //     onPressed: (){
            //       Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: true).iconShowDialog(context);
            //     })
          ],
        ),
        body: Consumer<FormMCompanyManajemenPICAlamatChangeNotifier>(
          builder: (context, formMCompanyManajemenPICAlamatNotif, _) {
            return formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress.isEmpty
            ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
            : ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  onLongPress: () {
                    formMCompanyManajemenPICAlamatNotif.selectedIndex = index;
                    formMCompanyManajemenPICAlamatNotif.controllerAddress.clear();
                    formMCompanyManajemenPICAlamatNotif.setAddress(formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index]);
                    Navigator.pop(context);
                  },
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChangeNotifierProvider(
                          create: (context) => FormMAddAddressIndividuChangeNotifier(),
                          child: CompanyAddManajemenPICAlamat(
                              flag: 1,
                              index: index,
                              addressModel : formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index],
                              typeAddress: 4,
                          )
                        )
                      )
                    ).then((value) => formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].isCorrespondence ? formMCompanyManajemenPICAlamatNotif.setAddress(formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index]) : null );
                  },
                  child: Card(
                    shape: formMCompanyManajemenPICAlamatNotif.selectedIndex == index
                        ? RoundedRectangleBorder(
                          side: BorderSide(color: primaryOrange, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].jenisAlamatModel.DESKRIPSI, style: TextStyle(fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].address}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].kelurahanModel.KEC_NAME}, ${formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].kelurahanModel.KABKOT_NAME}, ${formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].kelurahanModel.PROV_NAME}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].kelurahanModel.ZIPCODE}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].areaCode} ${formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].phone}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),)
                              ],
                            ),
                            Align(
                                alignment: Alignment.topRight,
                                child:
                                // formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                                //     ? formMinfoAlamat.listAlamatKorespondensi[index].isSameWithIdentity
                                //     ? SizedBox()
                                //     :
                                GestureDetector(
                                  // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                    onTap: () {formMCompanyManajemenPICAlamatNotif.moreDialog(context, index);},
                                    child: Icon(Icons.more_vert, color: Colors.grey)
                                )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
//                            Align(
//                              alignment: Alignment.topRight,
//                              child: formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].jenisAlamatModel.KODE == "03" || formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress[index].jenisAlamatModel.KODE == "01"
//                                  ? SizedBox()
//                                  : GestureDetector(
//                                      onTap: () {formMCompanyManajemenPICAlamatNotif.deleteManajemenPICAddress(context, index);},
//                                      child: Icon(Icons.delete, color: Colors.red)
//                                  )
//                            )
                          ],
                        )
                    ),
                  ),
                );
              },
              itemCount: formMCompanyManajemenPICAlamatNotif.listManajemenPICAddress.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ChangeNotifierProvider(
                      create: (context) => FormMAddAddressIndividuChangeNotifier(),
                      child: CompanyAddManajemenPICAlamat(
                        flag: 0,
                        index: null,
                        addressModel: null,
                        typeAddress: 4,
                      )
                  )
              )
            );
            // .then((value) => Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false).isShowDialog(context));
          },
          backgroundColor: myPrimaryColor,
          child: Icon(Icons.add, color: Colors.black),
        ),
      ),
    );
  }
}
