import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'form_m_manajemen_pic.dart';
import 'form_m_manajemen_pic_alamat.dart';
import 'form_m_pemegang_saham_kelembagaan.dart';
import 'form_m_pemegang_saham_kelembagaan_alamat.dart';
import 'form_m_pemegang_saham_pribadi.dart';
import 'form_m_pemegang_saham_pribadi_alamat.dart';

class MenuPemegangSaham extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        var _providerSahamPribadi = Provider.of<FormMCompanyPemegangSahamPribadiChangeNotifier>(context, listen: true);
        var _providerSahamPribadiAlamat = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: true);
        var _providerSahamLembaga = Provider.of<FormMCompanyPemegangSahamKelembagaanChangeNotifier>(context, listen: true);
        var _providerSahamLembagaAlamat = Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            _providerSahamPribadi.deleteSQLite();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMCompanyPemegangSahamPribadiProvider()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Pemegang Saham Pribadi"),
                                        _providerSahamPribadi.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerSahamPribadi.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                   //  InkWell(
                   //      onTap: () {
                   //          Navigator.push(
                   //              context,
                   //              MaterialPageRoute(
                   //                  builder: (context) => FormMCompanyPemegangSahamPribadiAlamatProvider()));
                   //      },
                   //      child: Card(
                   //          elevation: 3.3,
                   //          child: Padding(
                   //              padding: const EdgeInsets.all(16.0),
                   //              child: Row(
                   //                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   //                  children: <Widget>[
                   //                      Text(
                   //                          "Pemegang Saham Pribadi Alamat"),
                   //                      _providerSahamPribadiAlamat.flag
                   //                          ? Icon(Icons.check, color: primaryOrange)
                   //                          : SizedBox()
                   //                  ],
                   //              ),
                   //          ),
                   //      ),
                   //  ),
                   //  _providerSahamPribadiAlamat.autoValidate
                   //      ? Container(
                   //      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                   //      child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                   //  )
                   //      : SizedBox(height: MediaQuery.of(context).size.height / 87),
                   // SizedBox(height: MediaQuery.of(context).size.height / 87),
                   // InkWell(
                   //     onTap: () {
                   //         _providerSahamLembaga.deleteSQLite();
                   //         Navigator.push(
                   //             context,
                   //             MaterialPageRoute(
                   //                 builder: (context) => FormMCompanyPemegangSahamKelembagaanProvider()
                   //             )
                   //         );
                   //     },
                   //     child: Card(
                   //         elevation: 3.3,
                   //         child: Padding(
                   //             padding: const EdgeInsets.all(16.0),
                   //             child: Row(
                   //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   //                 children: <Widget>[
                   //                     Text(
                   //                         "Pemegang Saham Kelembagaan"),
                   //                     _providerSahamLembaga.flag
                   //                         ? Icon(Icons.check, color: primaryOrange)
                   //                         : SizedBox()
                   //                 ],
                   //             ),
                   //         ),
                   //     ),
                   // ),
                   // _providerSahamLembaga.autoValidate
                   //     ? Container(
                   //     margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                   //     child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                   // )
                   //     : SizedBox(height: MediaQuery.of(context).size.height / 87),
                   //  SizedBox(height: MediaQuery.of(context).size.height / 87),
                   //  InkWell(
                   //      onTap: () {
                   //          Navigator.push(
                   //              context,
                   //              MaterialPageRoute(
                   //                  builder: (context) => FormMCompanyPemegangSahamKelembagaanAlamatProvider()));
                   //      },
                   //      child: Card(
                   //          elevation: 3.3,
                   //          child: Padding(
                   //              padding: const EdgeInsets.all(16.0),
                   //              child: Row(
                   //                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   //                  children: <Widget>[
                   //                      Text(
                   //                          "Pemegang Saham Kelembagaan Alamat"),
                   //                      _providerSahamLembagaAlamat.flag
                   //                          ? Icon(Icons.check, color: primaryOrange)
                   //                          : SizedBox()
                   //                  ],
                   //              ),
                   //          ),
                   //      ),
                   //  ),
                   //  _providerSahamLembagaAlamat.autoValidate
                   //      ? Container(
                   //      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                   //      child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                   //  )
                   //      : SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }
}
