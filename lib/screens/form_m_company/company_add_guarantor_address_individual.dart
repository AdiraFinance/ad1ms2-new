import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_address_guarantor_individual_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class CompanyAddGuarantorAddressIndividual extends StatefulWidget {
  final int flag;
  final int index;
  final AddressGuarantorModelIndividiual addressModel;

  const CompanyAddGuarantorAddressIndividual(
      {this.flag, this.index, this.addressModel});
  @override
  _CompanyAddGuarantorAddressIndividualState createState() =>
      _CompanyAddGuarantorAddressIndividualState();
}

class _CompanyAddGuarantorAddressIndividualState
    extends State<CompanyAddGuarantorAddressIndividual> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      if (widget.addressModel.isSameWithIdentity) {
        _setValueForEdit = Provider.of<
                    FormMCompanyAddAddressGuarantorIndividualChangeNotifier>(
                context,
                listen: false)
            .setValueForEdit(widget.addressModel, context, widget.index,
                widget.addressModel.isSameWithIdentity);
      } else {
        _setValueForEdit = Provider.of<
                    FormMCompanyAddAddressGuarantorIndividualChangeNotifier>(
                context,
                listen: false)
            .setValueForEdit(widget.addressModel, context, widget.index,
                widget.addressModel.isSameWithIdentity);
      }
    } else {
      Provider.of<FormMCompanyAddAddressGuarantorIndividualChangeNotifier>(
              context,
              listen: false)
          .addDataListAddressType(context, null);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans"),
        child: Scaffold(
          appBar: AppBar(
            title: Text(
                widget.flag == 0
                    ? "Add Alamat Penjamin Pribadi"
                    : "Edit Alamat Penjamin Pribadi",
                style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body: SingleChildScrollView(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width / 27,
                  vertical: MediaQuery.of(context).size.height / 57),
              child: widget.flag == 0
                  ? Consumer<
                      FormMCompanyAddAddressGuarantorIndividualChangeNotifier>(
                      builder:
                          (context, formMAddGuarantorAddressIndividual, _) {
                        return Form(
                          key: formMAddGuarantorAddressIndividual.key,
                          onWillPop: _onWillPop,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DropdownButtonFormField<JenisAlamatModel>(
                                  autovalidate:
                                      formMAddGuarantorAddressIndividual
                                          .autoValidate,
                                  validator: (e) {
                                    if (e == null) {
                                      return "Silahkan pilih jenis alamat";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: formMAddGuarantorAddressIndividual
                                      .jenisAlamatSelected,
                                  onChanged: (value) {
                                    formMAddGuarantorAddressIndividual
                                        .jenisAlamatSelected = value;
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Jenis Alamat",
                                    border: OutlineInputBorder(),
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: formMAddGuarantorAddressIndividual
                                      .listJenisAlamat
                                      .map((value) {
                                    return DropdownMenuItem<JenisAlamatModel>(
                                      value: value,
                                      child: Text(
                                        value.DESKRIPSI,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()),
                              formMAddGuarantorAddressIndividual
                                          .jenisAlamatSelected !=
                                      null
                                  ? formMAddGuarantorAddressIndividual
                                              .jenisAlamatSelected.KODE ==
                                          "02"
                                      ? Row(
                                          children: [
                                            Checkbox(
                                                value:
                                                    formMAddGuarantorAddressIndividual
                                                        .isSameWithIdentity,
                                                onChanged: (value) {
                                                  formMAddGuarantorAddressIndividual
                                                          .isSameWithIdentity =
                                                      value;
                                                  formMAddGuarantorAddressIndividual
                                                      .setValueIsSameWithIdentity(
                                                          value, context, null);
                                                },
                                                activeColor: myPrimaryColor),
                                            SizedBox(width: 8),
                                            Text("Sama dengan identitas")
                                          ],
                                        )
                                      : SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              47)
                                  : SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              47),
                              TextFormField(
                                autovalidate: formMAddGuarantorAddressIndividual
                                    .autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMAddGuarantorAddressIndividual
                                    .controllerAlamat,
                                enabled: formMAddGuarantorAddressIndividual
                                    .enableTfAddress,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    filled: !formMAddGuarantorAddressIndividual
                                        .enableTfAddress,
                                    fillColor:
                                        !formMAddGuarantorAddressIndividual
                                                .enableTfAddress
                                            ? Colors.black12
                                            : Colors.white,
                                    labelText: 'Alamat',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                maxLines: 3,
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 5,
                                    child: TextFormField(
                                      autovalidate:
                                          formMAddGuarantorAddressIndividual
                                              .autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      enabled:
                                          formMAddGuarantorAddressIndividual
                                              .enableTfRT,
                                      controller:
                                          formMAddGuarantorAddressIndividual
                                              .controllerRT,
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                          filled:
                                              !formMAddGuarantorAddressIndividual
                                                  .enableTfRT,
                                          fillColor:
                                              !formMAddGuarantorAddressIndividual
                                                      .enableTfRT
                                                  ? Colors.black12
                                                  : Colors.white,
                                          labelText: 'RT',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly,
                                        // LengthLimitingTextInputFormatter(10),
                                      ],
                                      keyboardType: TextInputType.number,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Expanded(
                                    flex: 5,
                                    child: TextFormField(
                                      autovalidate:
                                          formMAddGuarantorAddressIndividual
                                              .autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      enabled:
                                          formMAddGuarantorAddressIndividual
                                              .enableTfRW,
                                      controller:
                                          formMAddGuarantorAddressIndividual
                                              .controllerRW,
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                          filled:
                                              !formMAddGuarantorAddressIndividual
                                                  .enableTfRW,
                                          fillColor:
                                              !formMAddGuarantorAddressIndividual
                                                      .enableTfRW
                                                  ? Colors.black12
                                                  : Colors.white,
                                          labelText: 'RW',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly,
                                        // LengthLimitingTextInputFormatter(10),
                                      ],
                                      keyboardType: TextInputType.number,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              FocusScope(
                                node: FocusScopeNode(),
                                child: TextFormField(
                                  autovalidate:
                                      formMAddGuarantorAddressIndividual
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                    formMAddGuarantorAddressIndividual
                                        .searchKelurahan(context);
                                  },
                                  enabled: formMAddGuarantorAddressIndividual
                                      .enableTfKelurahan,
                                  controller: formMAddGuarantorAddressIndividual
                                      .controllerKelurahan,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      filled:
                                          !formMAddGuarantorAddressIndividual
                                              .enableTfKelurahan,
                                      fillColor:
                                          !formMAddGuarantorAddressIndividual
                                                  .enableTfKelurahan
                                              ? Colors.black12
                                              : Colors.white,
                                      labelText: 'Kelurahan',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                ),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              TextFormField(
                                autovalidate: formMAddGuarantorAddressIndividual
                                    .autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                // enabled: formMAddGuarantorAddressIndividual.enableTfKecamatan,
                                controller: formMAddGuarantorAddressIndividual
                                    .controllerKecamatan,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    filled: !formMAddGuarantorAddressIndividual
                                        .enableTfKecamatan,
                                    fillColor:
                                        !formMAddGuarantorAddressIndividual
                                                .enableTfKecamatan
                                            ? Colors.black12
                                            : Colors.white,
                                    labelText: 'Kecamatan',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                enabled: false,
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              TextFormField(
                                autovalidate: formMAddGuarantorAddressIndividual
                                    .autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                // enabled: formMAddGuarantorAddressIndividual.enableTfKota,
                                controller: formMAddGuarantorAddressIndividual
                                    .controllerKota,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    filled: !formMAddGuarantorAddressIndividual
                                        .enableTfKota,
                                    fillColor:
                                        !formMAddGuarantorAddressIndividual
                                                .enableTfKota
                                            ? Colors.black12
                                            : Colors.white,
                                    labelText: 'Kabupaten/Kota',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                enabled: false,
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 7,
                                    child: TextFormField(
                                      autovalidate:
                                          formMAddGuarantorAddressIndividual
                                              .autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      // enabled: formMAddGuarantorAddressIndividual.enableTfProv,
                                      controller:
                                          formMAddGuarantorAddressIndividual
                                              .controllerProvinsi,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled:
                                              !formMAddGuarantorAddressIndividual
                                                  .enableTfProv,
                                          fillColor:
                                              !formMAddGuarantorAddressIndividual
                                                      .enableTfProv
                                                  ? Colors.black12
                                                  : Colors.white,
                                          labelText: 'Provinsi',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      enabled: false,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Expanded(
                                    flex: 3,
                                    child: TextFormField(
                                      autovalidate:
                                          formMAddGuarantorAddressIndividual
                                              .autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      // enabled: formMAddGuarantorAddressIndividual.enableTfPostalCode,
                                      controller:
                                          formMAddGuarantorAddressIndividual
                                              .controllerPostalCode,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled:
                                              !formMAddGuarantorAddressIndividual
                                                  .enableTfPostalCode,
                                          fillColor:
                                              !formMAddGuarantorAddressIndividual
                                                      .enableTfPostalCode
                                                  ? Colors.black12
                                                  : Colors.white,
                                          labelText: 'Kode Pos',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      enabled: false,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: TextFormField(
                                      autovalidate:
                                          formMAddGuarantorAddressIndividual
                                              .autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      onChanged: (e) {
                                        formMAddGuarantorAddressIndividual
                                            .checkValidCodeArea(e);
                                      },
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter
                                            .digitsOnly,
                                        LengthLimitingTextInputFormatter(10),
                                      ],
                                      enabled: formMAddGuarantorAddressIndividual.enableTfAreaCode,
                                      controller:
                                          formMAddGuarantorAddressIndividual
                                              .controllerKodeArea,
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                          filled:
                                              !formMAddGuarantorAddressIndividual
                                                  .enableTfAreaCode,
                                          fillColor:
                                              !formMAddGuarantorAddressIndividual
                                                      .enableTfAreaCode
                                                  ? Colors.black12
                                                  : Colors.white,
                                          labelText: 'Telepon (Area)',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                    ),
                                  ),
                                  SizedBox(
                                      width: MediaQuery.of(context).size.width /
                                          37),
                                  Expanded(
                                    flex: 6,
                                    child: TextFormField(
                                      autovalidate:
                                          formMAddGuarantorAddressIndividual
                                              .autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      enabled: formMAddGuarantorAddressIndividual.enableTfPhone,
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter
                                            .digitsOnly,
                                        LengthLimitingTextInputFormatter(10),
                                      ],
                                      controller:
                                          formMAddGuarantorAddressIndividual
                                              .controllerTlpn,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled:
                                              !formMAddGuarantorAddressIndividual
                                                  .enableTfPhone,
                                          fillColor:
                                              !formMAddGuarantorAddressIndividual
                                                      .enableTfPhone
                                                  ? Colors.black12
                                                  : Colors.white,
                                          labelText: 'Telepon',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      },
                    )
                  : FutureBuilder(
                      future: _setValueForEdit,
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return Consumer<
                            FormMCompanyAddAddressGuarantorIndividualChangeNotifier>(
                          builder:
                              (context, formMAddGuarantorAddressIndividual, _) {
                            return Form(
                              key: formMAddGuarantorAddressIndividual.key,
                              onWillPop: _onWillPop,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  DropdownButtonFormField<JenisAlamatModel>(
                                      autovalidate:
                                          formMAddGuarantorAddressIndividual
                                              .autoValidate,
                                      validator: (e) {
                                        if (e == null) {
                                          return "Silahkan pilih jenis alamat";
                                        } else {
                                          return null;
                                        }
                                      },
                                      value: formMAddGuarantorAddressIndividual
                                          .jenisAlamatSelected,
                                      onChanged: (value) {
                                        formMAddGuarantorAddressIndividual
                                            .jenisAlamatSelected = value;
                                      },
                                      onTap: () {
                                        FocusManager.instance.primaryFocus.unfocus();
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Jenis Alamat",
                                        border: OutlineInputBorder(),
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 10),
                                      ),
                                      items: formMAddGuarantorAddressIndividual
                                          .listJenisAlamat
                                          .map((value) {
                                        return DropdownMenuItem<
                                            JenisAlamatModel>(
                                          value: value,
                                          child: Text(
                                            value.DESKRIPSI,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        );
                                      }).toList()),
                                  formMAddGuarantorAddressIndividual
                                              .jenisAlamatSelected !=
                                          null
                                      ? formMAddGuarantorAddressIndividual
                                                  .jenisAlamatSelected.KODE ==
                                              "02"
                                          ? Row(
                                              children: [
                                                Checkbox(
                                                    value:
                                                        formMAddGuarantorAddressIndividual
                                                            .isSameWithIdentity,
                                                    onChanged: (value) {
                                                      formMAddGuarantorAddressIndividual
                                                              .isSameWithIdentity =
                                                          value;
                                                      formMAddGuarantorAddressIndividual
                                                          .setValueIsSameWithIdentity(
                                                              value,
                                                              context,
                                                              null);
                                                    },
                                                    activeColor:
                                                        primaryOrange),
                                                SizedBox(width: 8),
                                                Text("Sama dengan identitas")
                                              ],
                                            )
                                          : SizedBox(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  47)
                                      : SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              47),
                                  TextFormField(
                                    autovalidate:
                                        formMAddGuarantorAddressIndividual
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller:
                                        formMAddGuarantorAddressIndividual
                                            .controllerAlamat,
                                    enabled: formMAddGuarantorAddressIndividual
                                        .enableTfAddress,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled:
                                            !formMAddGuarantorAddressIndividual
                                                .enableTfAddress,
                                        fillColor:
                                            !formMAddGuarantorAddressIndividual
                                                    .enableTfAddress
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Alamat',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    maxLines: 3,
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                  ),
                                  SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              47),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 5,
                                        child: TextFormField(
                                          autovalidate:
                                              formMAddGuarantorAddressIndividual
                                                  .autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          enabled:
                                              formMAddGuarantorAddressIndividual
                                                  .enableTfRT,
                                          controller:
                                              formMAddGuarantorAddressIndividual
                                                  .controllerRT,
                                          style: TextStyle(color: Colors.black),
                                          decoration: InputDecoration(
                                              filled:
                                                  !formMAddGuarantorAddressIndividual
                                                      .enableTfRT,
                                              fillColor:
                                                  !formMAddGuarantorAddressIndividual
                                                          .enableTfRT
                                                      ? Colors.black12
                                                      : Colors.white,
                                              labelText: 'RT',
                                              labelStyle: TextStyle(
                                                  color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8))),
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly,
                                            // LengthLimitingTextInputFormatter(10),
                                          ],
                                          keyboardType: TextInputType.number,
                                        ),
                                      ),
                                      SizedBox(width: 8),
                                      Expanded(
                                        flex: 5,
                                        child: TextFormField(
                                          autovalidate:
                                              formMAddGuarantorAddressIndividual
                                                  .autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          enabled:
                                              formMAddGuarantorAddressIndividual
                                                  .enableTfRW,
                                          controller:
                                              formMAddGuarantorAddressIndividual
                                                  .controllerRW,
                                          style: TextStyle(color: Colors.black),
                                          decoration: InputDecoration(
                                              filled:
                                                  !formMAddGuarantorAddressIndividual
                                                      .enableTfRW,
                                              fillColor:
                                                  !formMAddGuarantorAddressIndividual
                                                          .enableTfRW
                                                      ? Colors.black12
                                                      : Colors.white,
                                              labelText: 'RW',
                                              labelStyle: TextStyle(
                                                  color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8))),
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly,
                                            // LengthLimitingTextInputFormatter(10),
                                          ],
                                          keyboardType: TextInputType.number,
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              47),
                                  FocusScope(
                                    node: FocusScopeNode(),
                                    child: TextFormField(
                                      autovalidate:
                                          formMAddGuarantorAddressIndividual
                                              .autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      onTap: () {
                                        FocusManager.instance.primaryFocus.unfocus();
                                        formMAddGuarantorAddressIndividual
                                            .searchKelurahan(context);
                                      },
                                      enabled:
                                          formMAddGuarantorAddressIndividual
                                              .enableTfKelurahan,
                                      controller:
                                          formMAddGuarantorAddressIndividual
                                              .controllerKelurahan,
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                          filled:
                                              !formMAddGuarantorAddressIndividual
                                                  .enableTfKelurahan,
                                          fillColor:
                                              !formMAddGuarantorAddressIndividual
                                                      .enableTfKelurahan
                                                  ? Colors.black12
                                                  : Colors.white,
                                          labelText: 'Kelurahan',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                    ),
                                  ),
                                  SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              47),
                                  TextFormField(
                                    autovalidate:
                                        formMAddGuarantorAddressIndividual
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    // enabled: formMAddGuarantorAddressIndividual.enableTfKecamatan,
                                    controller:
                                        formMAddGuarantorAddressIndividual
                                            .controllerKecamatan,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled:
                                            !formMAddGuarantorAddressIndividual
                                                .enableTfKecamatan,
                                        fillColor:
                                            !formMAddGuarantorAddressIndividual
                                                    .enableTfKecamatan
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Kecamatan',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                  SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              47),
                                  TextFormField(
                                    autovalidate:
                                        formMAddGuarantorAddressIndividual
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    // enabled: formMAddGuarantorAddressIndividual.enableTfKota,
                                    controller:
                                        formMAddGuarantorAddressIndividual
                                            .controllerKota,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled:
                                            !formMAddGuarantorAddressIndividual
                                                .enableTfKota,
                                        fillColor:
                                            !formMAddGuarantorAddressIndividual
                                                    .enableTfKota
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Kabupaten/Kota',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                  SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              47),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 7,
                                        child: TextFormField(
                                          autovalidate:
                                              formMAddGuarantorAddressIndividual
                                                  .autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          // enabled: formMAddGuarantorAddressIndividual.enableTfProv,
                                          controller:
                                              formMAddGuarantorAddressIndividual
                                                  .controllerProvinsi,
                                          style: new TextStyle(
                                              color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled:
                                                  !formMAddGuarantorAddressIndividual
                                                      .enableTfProv,
                                              fillColor:
                                                  !formMAddGuarantorAddressIndividual
                                                          .enableTfProv
                                                      ? Colors.black12
                                                      : Colors.white,
                                              labelText: 'Provinsi',
                                              labelStyle: TextStyle(
                                                  color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8))),
                                          enabled: false,
                                        ),
                                      ),
                                      SizedBox(width: 8),
                                      Expanded(
                                        flex: 3,
                                        child: TextFormField(
                                          autovalidate:
                                              formMAddGuarantorAddressIndividual
                                                  .autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          // enabled: formMAddGuarantorAddressIndividual.enableTfPostalCode,
                                          controller:
                                              formMAddGuarantorAddressIndividual
                                                  .controllerPostalCode,
                                          style: new TextStyle(
                                              color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled:
                                                  !formMAddGuarantorAddressIndividual
                                                      .enableTfPostalCode,
                                              fillColor:
                                                  !formMAddGuarantorAddressIndividual
                                                          .enableTfPostalCode
                                                      ? Colors.black12
                                                      : Colors.white,
                                              labelText: 'Kode Pos',
                                              labelStyle: TextStyle(
                                                  color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              47),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 4,
                                        child: TextFormField(
                                          autovalidate:
                                              formMAddGuarantorAddressIndividual
                                                  .autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          onChanged: (e) {
                                            formMAddGuarantorAddressIndividual
                                                .checkValidCodeArea(e);
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter
                                                .digitsOnly,
                                            LengthLimitingTextInputFormatter(
                                                10),
                                          ],
                                          enabled: formMAddGuarantorAddressIndividual.enableTfAreaCode,
                                          controller:
                                              formMAddGuarantorAddressIndividual
                                                  .controllerKodeArea,
                                          style: TextStyle(color: Colors.black),
                                          decoration: InputDecoration(
                                              filled:
                                                  !formMAddGuarantorAddressIndividual
                                                      .enableTfAreaCode,
                                              fillColor:
                                                  !formMAddGuarantorAddressIndividual
                                                          .enableTfAreaCode
                                                      ? Colors.black12
                                                      : Colors.white,
                                              labelText: 'Telepon (Area)',
                                              labelStyle: TextStyle(
                                                  color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8))),
                                        ),
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              37),
                                      Expanded(
                                        flex: 6,
                                        child: TextFormField(
                                          autovalidate:
                                              formMAddGuarantorAddressIndividual
                                                  .autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          enabled:
                                              formMAddGuarantorAddressIndividual
                                                  .enableTfPhone,
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter
                                                .digitsOnly,
                                            LengthLimitingTextInputFormatter(
                                                10),
                                          ],
                                          controller:
                                              formMAddGuarantorAddressIndividual
                                                  .controllerTlpn,
                                          style: new TextStyle(
                                              color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled:
                                                  !formMAddGuarantorAddressIndividual
                                                      .enableTfPhone,
                                              fillColor:
                                                  !formMAddGuarantorAddressIndividual
                                                          .enableTfPhone
                                                      ? Colors.black12
                                                      : Colors.white,
                                              labelText: 'Telepon',
                                              labelStyle: TextStyle(
                                                  color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8))),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      })),
          bottomNavigationBar: BottomAppBar(
            elevation: 0.0,
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0)),
                    color: myPrimaryColor,
                    onPressed: () {
                      if (widget.flag == 0) {
                        Provider.of<FormMCompanyAddAddressGuarantorIndividualChangeNotifier>(
                                context,
                                listen: true)
                            .check(context, widget.flag, null);
                      } else {
                        Provider.of<FormMCompanyAddAddressGuarantorIndividualChangeNotifier>(
                                context,
                                listen: true)
                            .check(context, widget.flag, widget.index);
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                letterSpacing: 1.25))
                      ],
                    ))),
          ),
        ));
    ;
  }

  Future<bool> _onWillPop() async {
    var _provider =
        Provider.of<FormMCompanyAddAddressGuarantorIndividualChangeNotifier>(
            context,
            listen: false);
    if (widget.flag == 0) {
      if (_provider.jenisAlamatSelected != null ||
          _provider.controllerAlamat.text != "" ||
          _provider.controllerRT.text != "" ||
          _provider.controllerRW.text != "" ||
          _provider.controllerKelurahan.text != "" ||
          _provider.controllerKecamatan.text != "" ||
          _provider.controllerKota.text != "" ||
          _provider.controllerProvinsi.text != "" ||
          _provider.controllerPostalCode.text != "" ||
          _provider.controllerKodeArea.text != "" ||
          _provider.controllerTlpn.text != "") {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (_provider.jenisAlamatSelectedTemp.KODE !=
              _provider.jenisAlamatSelected.KODE ||
          _provider.alamatTemp != _provider.controllerAlamat.text ||
          _provider.rtTemp != _provider.controllerRT.text ||
          _provider.rwTemp != _provider.controllerRW.text ||
          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
          _provider.kotaTemp != _provider.controllerKota.text ||
          _provider.provinsiTemp != _provider.controllerProvinsi.text ||
          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
          _provider.areaCOdeTemp != _provider.controllerKodeArea.text ||
          _provider.phoneTemp != _provider.controllerTlpn.text) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
  }
}
