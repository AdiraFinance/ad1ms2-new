import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMCompanyPemegangSahamKelembagaanProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        primarySwatch: primaryOrange,
      ),
      child: Consumer<FormMCompanyPemegangSahamKelembagaanChangeNotifier>(
        builder: (context, formMCompanyPemegangSahamKelembagaanChangeNotif, _) {
          return Scaffold(
            appBar: AppBar(
              title:
              Text("Pemegang Saham Kelembagaan", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyPemegangSahamKelembagaanChangeNotif.keyForm,
              onWillPop: formMCompanyPemegangSahamKelembagaanChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DropdownButtonFormField<TypeInstitutionModel>(
                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih jenis lembaga";
                          } else {
                            return null;
                          }
                        },
                        value: formMCompanyPemegangSahamKelembagaanChangeNotif.typeInstitutionSelected,
                        onChanged: (value) {
                          formMCompanyPemegangSahamKelembagaanChangeNotif.typeInstitutionSelected = value;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Jenis Lembaga",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMCompanyPemegangSahamKelembagaanChangeNotif.listTypeInstitution.map((value) {
                          return DropdownMenuItem<TypeInstitutionModel>(
                            value: value,
                            child: Text(
                              value.PARA_NAME,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerInstitutionName,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Nama Lembaga',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      FocusScope(
                          node: FocusScopeNode(),
                          child: TextFormField(
                            autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerDateOfEstablishment,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                labelText: 'Tanggal Pendirian',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                              formMCompanyPemegangSahamKelembagaanChangeNotif.selectDateOfEstablishment(context);
                            },
                          )
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          // LengthLimitingTextInputFormatter(10),
                        ],
                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerNPWP,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'NPWP',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          // LengthLimitingTextInputFormatter(10),
                        ],
                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerPercentShare,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: '% Share',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.number,
                      ),
                    ],
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyPemegangSahamKelembagaanChangeNotifier>(
                    builder: (context, formMCompanyPemegangSahamKelembagaanChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMCompanyPemegangSahamKelembagaanChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          );
        },
      )
    );
  }
}
