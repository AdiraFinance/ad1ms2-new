import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_alamat_korespondensi.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_alamat_korespondensi_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Lembaga",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
            // IconButton(
            //     icon: Icon(Icons.info_outline),
            //     onPressed: (){
            //       Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: true).iconShowDialog(context);
            //     })
          ],
        ),
        body: Consumer<FormMCompanyAlamatChangeNotifier>(
          builder: (context, formMCompanyAlamatNotif, _) {
            return formMCompanyAlamatNotif.listAlamatKorespondensi.isEmpty
            ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
            : ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  onLongPress: () {
                    formMCompanyAlamatNotif.selectedIndex = index;
                    formMCompanyAlamatNotif.controllerAddress.clear();
                    formMCompanyAlamatNotif.setAddress(formMCompanyAlamatNotif.listAlamatKorespondensi[index]);
                    Navigator.pop(context);
                  },
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChangeNotifierProvider(
                          create: (context) => FormMAddAddressCompanyChangeNotifier(),
                          child: CompanyAddAlamatKorespondensi(
                              flag: 1,
                              index: index,
                              addressModel: formMCompanyAlamatNotif.listAlamatKorespondensi[index],
                              typeAddress: 2),
                        )
                      )
                    ).then((value) => formMCompanyAlamatNotif.listAlamatKorespondensi[index].isCorrespondence ? formMCompanyAlamatNotif.setAddress(formMCompanyAlamatNotif.listAlamatKorespondensi[index]) : null );
                  },
                  child: Card(
                    shape: formMCompanyAlamatNotif.selectedIndex == index
                        ? RoundedRectangleBorder(
                            side: BorderSide(color: primaryOrange, width: 2),
                            borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(formMCompanyAlamatNotif.listAlamatKorespondensi[index].jenisAlamatModel.DESKRIPSI, style: TextStyle(fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMCompanyAlamatNotif.listAlamatKorespondensi[index].address}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMCompanyAlamatNotif.listAlamatKorespondensi[index].kelurahanModel.KEC_NAME}, ${formMCompanyAlamatNotif.listAlamatKorespondensi[index].kelurahanModel.KABKOT_NAME}, ${formMCompanyAlamatNotif.listAlamatKorespondensi[index].kelurahanModel.PROV_NAME}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMCompanyAlamatNotif.listAlamatKorespondensi[index].kelurahanModel.ZIPCODE}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMCompanyAlamatNotif.listAlamatKorespondensi[index].phoneArea1} ${formMCompanyAlamatNotif.listAlamatKorespondensi[index].phone1}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),)
                              ],
                            ),
                            Align(
                                alignment: Alignment.topRight,
                                child:
                                // formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                                //     ? formMinfoAlamat.listAlamatKorespondensi[index].isSameWithIdentity
                                //     ? SizedBox()
                                //     :
                                GestureDetector(
                                  // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                    onTap: () {formMCompanyAlamatNotif.moreDialog(context, index);},
                                    child: Icon(Icons.more_vert, color: Colors.grey)
                                )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
//                            Align(
//                              alignment: Alignment.topRight,
//                              child: formMCompanyAlamatNotif.listAlamatKorespondensi[index].jenisAlamatModel.KODE == "03" || formMCompanyAlamatNotif.listAlamatKorespondensi[index].jenisAlamatModel.KODE == "01"
//                                  ? SizedBox()
//                                  : GestureDetector(
//                                      onTap: () {formMCompanyAlamatNotif.deleteAlamatKorespondensi(context, index);},
//                                      child: Icon(Icons.delete, color: Colors.red)
//                                  )
//                            )
                          ],
                        )
                    ),
                  ),
                );
              },
              itemCount: formMCompanyAlamatNotif.listAlamatKorespondensi.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                  create: (context) => FormMAddAddressCompanyChangeNotifier(),
                  child: CompanyAddAlamatKorespondensi(
                    flag: 0,
                    index: null,
                    addressModel: null,
                    typeAddress: 2,
                  )
                )
              )
            );
            // .then((value) => Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false).isShowDialog(context));
          },
          backgroundColor: myPrimaryColor,
          child: Icon(Icons.add, color: Colors.black),
        ),
      ),
    );
  }
}
