import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_model.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class FormMCompanyManajemenPICProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange,
      ),
      child: Consumer<FormMCompanyManajemenPICChangeNotifier>(
        builder: (context, formMCompanyManajemenPICChangeNotif, _) {
          return Scaffold(
            appBar: AppBar(
              title:
              Text("Informasi Manajemen PIC", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyManajemenPICChangeNotif.keyForm,
              onWillPop: formMCompanyManajemenPICChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DropdownButtonFormField<TypeIdentityModel>(
                        autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        value: formMCompanyManajemenPICChangeNotif.typeIdentitySelected,
                        onChanged: (value) {
                          formMCompanyManajemenPICChangeNotif.typeIdentitySelected = value;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Jenis Identitas",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMCompanyManajemenPICChangeNotif.listTypeIdentity.map((value) {
                          return DropdownMenuItem<TypeIdentityModel>(
                            value: value,
                            child: Text(
                              value.text,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        inputFormatters: formMCompanyManajemenPICChangeNotif.typeIdentitySelected != null
                            ? formMCompanyManajemenPICChangeNotif.typeIdentitySelected.id != "03"
                              ? [WhitelistingTextInputFormatter.digitsOnly, LengthLimitingTextInputFormatter(16),]
                              : null
                            : [WhitelistingTextInputFormatter.digitsOnly],
                        controller: formMCompanyManajemenPICChangeNotif.controllerIdentityNumber,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'No Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: formMCompanyManajemenPICChangeNotif.typeIdentitySelected != null
                            ? formMCompanyManajemenPICChangeNotif.typeIdentitySelected.id != "03"
                            ? TextInputType.number
                            : TextInputType.text
                            : TextInputType.number,
                        textCapitalization: TextCapitalization.characters,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCompanyManajemenPICChangeNotif.controllerFullNameIdentity,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Nama Lengkap Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCompanyManajemenPICChangeNotif.controllerFullName,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Nama Lengkap',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   controller: formMCompanyManajemenPICChangeNotif.controllerAlias,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Alias',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   controller: formMCompanyManajemenPICChangeNotif.controllerDegree,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Gelar',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      FocusScope(
                          node: FocusScopeNode(),
                          child: TextFormField(
                            autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMCompanyManajemenPICChangeNotif.controllerBirthOfDate,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                labelText: 'Tanggal Lahir',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                              formMCompanyManajemenPICChangeNotif.selectBirthDate(context);
                            },
                            readOnly: true,
                          )
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyManajemenPICChangeNotif.controllerPlaceOfBirthIdentity,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Tempat Lahir Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        onTap: (){
                          FocusManager.instance.primaryFocus.unfocus();
                          formMCompanyManajemenPICChangeNotif.searchBirthPlace(context);
                        },
                        controller: formMCompanyManajemenPICChangeNotif.controllerBirthPlaceIdentityLOV,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Tempat Lahir Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                      ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   controller: formMCompanyManajemenPICChangeNotif.controllerPosition,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Jabatan',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                      //   validator: formMCompanyManajemenPICChangeNotif.validateEmail,
                      //   controller: formMCompanyManajemenPICChangeNotif.controllerEmail,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Email',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      // ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          // LengthLimitingTextInputFormatter(10),
                        ],
                        controller: formMCompanyManajemenPICChangeNotif.controllerHandphoneNumber,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'No Handphone',
                            labelStyle: TextStyle(color: Colors.black),
                            prefixText: '08',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.number,
                      ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   onTap: () {
                      //     Navigator.push(
                      //       context,
                      //       MaterialPageRoute(builder: (context) => ListManajemenPICAlamat()));
                      //   },
                      //   controller: formMCompanyManajemenPICChangeNotif.controllerManajemenPICAddress,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //     labelText: 'Alamat Korespondensi',
                      //     labelStyle: TextStyle(color: Colors.black),
                      //     border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))
                      //   ),
                      // )
                    ],
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyManajemenPICChangeNotifier>(
                    builder: (context, formMCompanyManajemenPICChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMCompanyManajemenPICChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          );
        },
      )
    );
  }
}
