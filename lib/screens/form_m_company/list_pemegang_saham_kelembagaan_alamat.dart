import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_pemegang_saham_kelembagaan_alamat.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListPemegangSahamKelembagaanAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: Colors.yellow),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Korespondensi",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
            IconButton(
                icon: Icon(Icons.info_outline),
                onPressed: (){
                  Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context, listen: true).iconShowDialog(context);
                })
          ],
        ),
        body: Consumer<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(
          builder: (context, formMCompanyPemegangSahamKelembagaanAlamatNotif, _) {
            return ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  onLongPress: () {
                    formMCompanyPemegangSahamKelembagaanAlamatNotif.selectedIndex = index;
                    formMCompanyPemegangSahamKelembagaanAlamatNotif.controllerAddress.clear();
                    formMCompanyPemegangSahamKelembagaanAlamatNotif.setAddress(formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index]);
                    Navigator.pop(context);
                  },
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangeNotifierProvider(
                                create: (context) => FormMAddAddressCompanyChangeNotifier(),
                                child: CompanyAddPemegangSahamKelembagaanAddress(
                                    flag: 1,
                                    index: index,
                                    addressModel: formMCompanyPemegangSahamKelembagaanAlamatNotif
                                        .listPemegangSahamKelembagaanAddress[index],
                                    typeAddress: 3,
                                )
                            )
                        )
                    );
                  },
                  child: Card(
                    shape: formMCompanyPemegangSahamKelembagaanAlamatNotif.selectedIndex == index
                        ? RoundedRectangleBorder(
                            side: BorderSide(color: primaryOrange, width: 2),
                            borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Stack(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.DESKRIPSI, style: TextStyle(fontWeight: FontWeight.bold),),
                              SizedBox(
                                height:
                                MediaQuery.of(context).size.height /
                                    97,
                              ),
                              Text("${formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].address}",
                                style: TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              Text("${formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].kelurahanModel.KEC_NAME}, ${formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].kelurahanModel.KABKOT_NAME}, ${formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].kelurahanModel.PROV_NAME}",
                                style: TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              Text("${formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].kelurahanModel.ZIPCODE}",
                                style: TextStyle(color: Colors.grey, fontSize: 13),),
                              SizedBox(
                                height:
                                MediaQuery.of(context).size.height /
                                    97,
                              ),
                              Text("${formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].phoneArea1} ${formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].phone1}",
                                style: TextStyle(color: Colors.grey, fontSize: 13),)
                            ],
                          ),
                          Align(
                              alignment: Alignment.topRight,
                              child:
                              // formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                              //     ? formMinfoAlamat.listAlamatKorespondensi[index].isSameWithIdentity
                              //     ? SizedBox()
                              //     :
                              GestureDetector(
                                // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                  onTap: () {formMCompanyPemegangSahamKelembagaanAlamatNotif.moreDialog(context, index);},
                                  child: Icon(Icons.more_vert, color: Colors.grey)
                              )
                            // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                            //     onPressed: () {
                            //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                            //     })
                            //     : SizedBox(),
                          )
//                          Align(
//                            alignment: Alignment.topRight,
//                            child: formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.KODE == "03" || formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.KODE == "01"
//                                ? SizedBox()
//                                : GestureDetector(
//                                onTap: () {formMCompanyPemegangSahamKelembagaanAlamatNotif.deletePemegangSahamKelembagaanAddress(context, index);},
//                                child: Icon(Icons.delete, color: Colors.red)
//                            )
//                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: formMCompanyPemegangSahamKelembagaanAlamatNotif.listPemegangSahamKelembagaanAddress.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                  create: (context) => FormMAddAddressCompanyChangeNotifier(),
                  child: CompanyAddPemegangSahamKelembagaanAddress(
                    flag: 0,
                    index: null,
                    addressModel: null,
                    typeAddress: 3,
                  )
                )
              )
            ).then((value) => Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context, listen: false).isShowDialog(context));
          },
          backgroundColor: myPrimaryColor,
          child: Icon(Icons.add, color: Colors.black),
        ),
      ),
    );
  }
}
