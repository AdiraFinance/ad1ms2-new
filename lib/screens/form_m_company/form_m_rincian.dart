import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMCompanyRincianProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange,
      ),
      child: Consumer<FormMCompanyRincianChangeNotifier>(
        builder: (context, formMCompanyRincianChangeNotif, _) {
          return Scaffold(
            appBar: AppBar(
              title:
              Text("Rincian Lembaga", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyRincianChangeNotif.keyForm,
              onWillPop: formMCompanyRincianChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DropdownButtonFormField<TypeInstitutionModel>(
                        autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        value: formMCompanyRincianChangeNotif.typeInstitutionSelected,
                        onChanged: (value) {
                          formMCompanyRincianChangeNotif.typeInstitutionSelected = value;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Jenis Lembaga",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMCompanyRincianChangeNotif.listTypeInstitution.map((value) {
                          return DropdownMenuItem<TypeInstitutionModel>(
                            value: value,
                            child: Text(
                              value.PARA_NAME,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      DropdownButtonFormField<ProfilModel>(
                        autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        value: formMCompanyRincianChangeNotif.profilSelected,
                        onChanged: (value) {
                          formMCompanyRincianChangeNotif.profilSelected = value;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Profil",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMCompanyRincianChangeNotif.listProfil.map((value) {
                          return DropdownMenuItem<ProfilModel>(
                            value: value,
                            child: Text(
                              value.text,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCompanyRincianChangeNotif.controllerInstitutionName,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Nama Lembaga',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: formMCompanyRincianChangeNotif.isEnableFieldCompanyName,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Text(
                        "Punya NPWP ?",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.15),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 1,
                                  groupValue:
                                  formMCompanyRincianChangeNotif.radioValueIsHaveNPWP,
                                  onChanged: (value) {
                                    formMCompanyRincianChangeNotif.radioValueIsHaveNPWP =
                                        value;
                                  }),
                              Text("Ya")
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 0,
                                  groupValue:
                                  formMCompanyRincianChangeNotif.radioValueIsHaveNPWP,
                                  onChanged: (value) {
                                    formMCompanyRincianChangeNotif.radioValueIsHaveNPWP =
                                        value;
                                  }),
                              Text("Tidak")
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      formMCompanyRincianChangeNotif.radioValueIsHaveNPWP == 0
                          ? SizedBox(height: 0.0)
                          : Column(
                        children: [
                          TextFormField(
                            // autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                            // validator: (e) {
                            //   if (e.isEmpty) {
                            //     return "Tidak boleh kosong";
                            //   } else {
                            //     return null;
                            //   }
                            // },
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              // LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMCompanyRincianChangeNotif.controllerNPWP,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                labelText: 'NPWP',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                            keyboardType: TextInputType.number,
                            enabled: formMCompanyRincianChangeNotif.isEnableFieldNumberNPWP,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          TextFormField(
                            // autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                            // validator: (e) {
                            //   if (e.isEmpty) {
                            //     return "Tidak boleh kosong";
                            //   } else {
                            //     return null;
                            //   }
                            // },
                            controller: formMCompanyRincianChangeNotif.controllerFullNameNPWP,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                labelText: 'Nama Lengkap Sesuai NPWP',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          // DropdownButtonFormField<TypeNPWPModel>(
                          //   autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                          //   validator: (e) {
                          //     if (e == null) {
                          //       return "Tidak boleh kosong";
                          //     } else {
                          //       return null;
                          //     }
                          //   },
                          //   value: formMCompanyRincianChangeNotif.typeNPWPSelected,
                          //   decoration: InputDecoration(
                          //     labelText: "Jenis NPWP",
                          //     border: OutlineInputBorder(),
                          //     contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          //   ),
                          //   items: formMCompanyRincianChangeNotif.listTypeNPWP.map((data) {
                          //     return DropdownMenuItem<TypeNPWPModel>(
                          //       value: data,
                          //       child: Text(
                          //         data.text,
                          //         overflow: TextOverflow.ellipsis,
                          //       ),
                          //     );
                          //   }).toList(),
                          //   onChanged: (newVal) {
                          //     formMCompanyRincianChangeNotif.typeNPWPSelected = newVal;
                          //   },
                          //   onTap: () {
                          //     FocusManager.instance.primaryFocus.unfocus();
                          //   },
                          // ),
                          // SizedBox(height: MediaQuery.of(context).size.height / 47),
                          // DropdownButtonFormField<SignPKPModel>(
                          //   autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                          //   validator: (e) {
                          //     if (e == null) {
                          //       return "Tidak boleh kosong";
                          //     } else {
                          //       return null;
                          //     }
                          //   },
                          //   value: formMCompanyRincianChangeNotif.signPKPSelected,
                          //   decoration: InputDecoration(
                          //     labelText: "Tanda PKP",
                          //     border: OutlineInputBorder(),
                          //     contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          //   ),
                          //   items: formMCompanyRincianChangeNotif.listSignPKP.map((data) {
                          //     return DropdownMenuItem<SignPKPModel>(
                          //       value: data,
                          //       child: Text(
                          //         data.text,
                          //         overflow: TextOverflow.ellipsis,
                          //       ),
                          //     );
                          //   }).toList(),
                          //   onChanged: (newVal) {
                          //     formMCompanyRincianChangeNotif.signPKPSelected = newVal;
                          //   },
                          //   onTap: () {
                          //     FocusManager.instance.primaryFocus.unfocus();
                          //   },
                          // ),
                          // SizedBox(height: MediaQuery.of(context).size.height / 47),
                          // TextFormField(
                          //   autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                          //   validator: (e) {
                          //     if (e.isEmpty) {
                          //       return "Tidak boleh kosong";
                          //     } else {
                          //       return null;
                          //     }
                          //   },
                          //   controller: formMCompanyRincianChangeNotif.controllerNPWPAddress,
                          //   style: new TextStyle(color: Colors.black),
                          //   decoration: new InputDecoration(
                          //       labelText: 'Alamat NPWP',
                          //       labelStyle: TextStyle(color: Colors.black),
                          //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                          //   keyboardType: TextInputType.text,
                          //   textCapitalization: TextCapitalization.characters,
                          // ),
                          // SizedBox(height: MediaQuery.of(context).size.height / 47),
                        ],
                      ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                          formMCompanyRincianChangeNotif.selectDateEstablishment(context);
                        },
                        controller: formMCompanyRincianChangeNotif.controllerDateEstablishment,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Tanggal Pendirian',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        readOnly: true,
                        enabled: formMCompanyRincianChangeNotif.isEnableFieldCompanyName,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      FocusScope(
                          node: FocusScopeNode(),
                          child: TextFormField(
                            // autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                            // validator: (e) {
                            //   if (e.isEmpty) {
                            //     return "Tidak boleh kosong";
                            //   } else {
                            //     return null;
                            //   }
                            // },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                              formMCompanyRincianChangeNotif.searchSectorEconomic(context);
                            },
                            controller:
                            formMCompanyRincianChangeNotif.controllerSectorEconomic,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                labelText: 'Sektor Ekonomi',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                          )),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      FocusScope(
                          node: FocusScopeNode(),
                          child: TextFormField(
                            // autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                            // validator: (e) {
                            //   if (e.isEmpty) {
                            //     return "Tidak boleh kosong";
                            //   } else {
                            //     return null;
                            //   }
                            // },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                              formMCompanyRincianChangeNotif.searchBusinesField(context);
                            },
                            controller:
                            formMCompanyRincianChangeNotif.controllerBusinessField,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                labelText: 'Lapangan Usaha',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: formMCompanyRincianChangeNotif.controllerSectorEconomic.text == "" ? false : true,
                          )),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // DropdownButtonFormField<LocationStatusModel>(
                      //   autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e == null) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   value: formMCompanyRincianChangeNotif.locationStatusSelected,
                      //   decoration: InputDecoration(
                      //     labelText: "Status Lokasi",
                      //     border: OutlineInputBorder(),
                      //     contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      //   ),
                      //   items: formMCompanyRincianChangeNotif.listLocationStatus.map((data) {
                      //     return DropdownMenuItem<LocationStatusModel>(
                      //       value: data,
                      //       child: Text(
                      //         data.text,
                      //         overflow: TextOverflow.ellipsis,
                      //       ),
                      //     );
                      //   }).toList(),
                      //   onChanged: (newVal) {
                      //     formMCompanyRincianChangeNotif.locationStatusSelected = newVal;
                      //   },
                      //   onTap: () {
                      //     FocusManager.instance.primaryFocus.unfocus();
                      //   },
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // DropdownButtonFormField<BusinessLocationModel>(
                      //   autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e == null) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   value: formMCompanyRincianChangeNotif.businessLocationSelected,
                      //   decoration: InputDecoration(
                      //     labelText: "Lokasi Usaha",
                      //     border: OutlineInputBorder(),
                      //     contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      //   ),
                      //   items: formMCompanyRincianChangeNotif.listBusinessLocation.map((data) {
                      //     return DropdownMenuItem<BusinessLocationModel>(
                      //       value: data,
                      //       child: Text(
                      //         data.desc,
                      //         overflow: TextOverflow.ellipsis,
                      //       ),
                      //     );
                      //   }).toList(),
                      //   onChanged: (newVal) {
                      //     formMCompanyRincianChangeNotif.businessLocationSelected = newVal;
                      //   },
                      //   onTap: () {
                      //     FocusManager.instance.primaryFocus.unfocus();
                      //   },
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   inputFormatters: [
                      //     WhitelistingTextInputFormatter.digitsOnly,
                      //     // LengthLimitingTextInputFormatter(10),
                      //   ],
                      //   controller: formMCompanyRincianChangeNotif.controllerTotalEmployees,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Total Pegawai',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.number,
                      // ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          // LengthLimitingTextInputFormatter(10),
                        ],
                        controller: formMCompanyRincianChangeNotif.controllerLamaUsahaBerjalan,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Lama Usaha/Kerja Berjalan (bln)',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          // LengthLimitingTextInputFormatter(10),
                        ],
                        controller: formMCompanyRincianChangeNotif.controllerTotalLamaUsahaBerjalan,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Total Lama Usaha/Kerja Berjalan (bln)',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // Text(
                      //   "Buka Rekening ?",
                      //   style: TextStyle(
                      //       fontSize: 16,
                      //       fontWeight: FontWeight.w700,
                      //       letterSpacing: 0.15),
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // Row(
                      //   children: [
                      //     Row(
                      //       children: [
                      //         Radio(
                      //             activeColor: primaryOrange,
                      //             value: 1,
                      //             groupValue:
                      //             formMCompanyRincianChangeNotif.radioValueOpenAccount,
                      //             onChanged: (value) {
                      //               formMCompanyRincianChangeNotif.radioValueOpenAccount = value;
                      //             }),
                      //         Text("Ya")
                      //       ],
                      //     ),
                      //     Row(
                      //       children: [
                      //         Radio(
                      //             activeColor: primaryOrange,
                      //             value: 0,
                      //             groupValue:
                      //             formMCompanyRincianChangeNotif.radioValueOpenAccount,
                      //             onChanged: (value) {
                      //               formMCompanyRincianChangeNotif.radioValueOpenAccount = value;
                      //             }),
                      //         Text("Tidak")
                      //       ],
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // formMCompanyRincianChangeNotif.radioValueOpenAccount == 0
                      //     ? SizedBox(height: 0.0)
                      //     :
                      // TextFormField(
                      //   autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   inputFormatters: [
                      //     WhitelistingTextInputFormatter.digitsOnly,
                      //     // LengthLimitingTextInputFormatter(10),
                      //   ],
                      //   controller: formMCompanyRincianChangeNotif.controllerFormNumber,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Nomor Form',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.number,
                      // ),
                    ],
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyRincianChangeNotifier>(
                    builder: (context, formMCompanyRincianChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMCompanyRincianChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          );
        },
      )
    );
  }
}
