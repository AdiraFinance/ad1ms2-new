import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_company_pemegang_saham_pribadi_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class FormMCompanyPemegangSahamPribadiProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange,
      ),
      child: Consumer<FormMCompanyPemegangSahamPribadiChangeNotifier>(
        builder: (context, formMCompanyPemegangSahamPribadiChangeNotif, _) {
          return Scaffold(
            appBar: AppBar(
              title:
              Text("Pemegang Saham Pribadi", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyPemegangSahamPribadiChangeNotif.keyForm,
              onWillPop: formMCompanyPemegangSahamPribadiChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DropdownButtonFormField<RelationshipStatusModel>(
                        autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih status hubungan";
                          } else {
                            return null;
                          }
                        },
                        value: formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected,
                        onChanged: (value) {
                          formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected = value;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Status Hubungan",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMCompanyPemegangSahamPribadiChangeNotif.listRelationshipStatus.map((value) {
                          return DropdownMenuItem<RelationshipStatusModel>(
                            value: value,
                            child: Text(
                              value.PARA_FAMILY_TYPE_NAME,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      DropdownButtonFormField<IdentityModel>(
                        autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih jenis identitas";
                          } else {
                            return null;
                          }
                        },
                        value: formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected,
                        onChanged: (value) {
                          formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected = value;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        decoration: InputDecoration(
                          labelText: "Jenis Identitas",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMCompanyPemegangSahamPribadiChangeNotif.listTypeIdentity.map((value) {
                          return DropdownMenuItem<IdentityModel>(
                            value: value,
                            child: Text(
                              value.name,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected != null
                            ? formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected.id != "03"
                            ? [WhitelistingTextInputFormatter.digitsOnly]
                            : null
                            : [WhitelistingTextInputFormatter.digitsOnly],
                        controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerIdentityNumber,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'No Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected != null
                            ? formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected.id != "03"
                            ? TextInputType.number
                            : TextInputType.text
                            : TextInputType.number
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerFullNameIdentity,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Nama Lengkap Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerFullName,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Nama Lengkap',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                      ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerAlias,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Alias',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerDegree,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Gelar',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // FocusScope(
                      //     node: FocusScopeNode(),
                      //     child: TextFormField(
                      //       autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                      //       validator: (e) {
                      //         if (e.isEmpty) {
                      //           return "Tidak boleh kosong";
                      //         } else {
                      //           return null;
                      //         }
                      //       },
                      //       controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerBirthOfDate,
                      //       style: new TextStyle(color: Colors.black),
                      //       decoration: new InputDecoration(
                      //           labelText: 'Tanggal Lahir',
                      //           labelStyle: TextStyle(color: Colors.black),
                      //           border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                      //       onTap: () {
                      //         FocusManager.instance.primaryFocus.unfocus();
                      //         formMCompanyPemegangSahamPribadiChangeNotif.selectBirthDate(context);
                      //       },
                      //       readOnly: true,
                      //     )
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerPlaceOfBirthIdentity,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Tempat Lahir Sesuai Identitas',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   onTap: (){
                      //     formMCompanyPemegangSahamPribadiChangeNotif.searchBirthPlace(context);
                      //   },
                      //   controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerBirthPlaceIdentityLOV,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Tempat Lahir Sesuai Identitas',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      //   readOnly: true,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // Text(
                      //   "Jenis Kelamin",
                      //   style: TextStyle(
                      //       fontSize: 16,
                      //       fontWeight: FontWeight.w700,
                      //       letterSpacing: 0.15),
                      // ),
                      // Row(
                      //   children: [
                      //     Row(
                      //       children: [
                      //         Radio(
                      //             value: "01",
                      //             groupValue: formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender,
                      //             onChanged: formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected != null
                      //                 ? formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" ||
                      //                 formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" ||
                      //                 formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04" ||
                      //                 formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "05"
                      //                 ? (value) {;}
                      //                 : (value) {formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender = value;}
                      //                 : (value) {;}
                      //         ),
                      //         Text("Laki Laki")
                      //       ],
                      //     ),
                      //     Row(
                      //       children: [
                      //         Radio(
                      //             value: "02",
                      //             groupValue: formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender,
                      //             onChanged: formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected != null
                      //                 ? formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" ||
                      //                 formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" ||
                      //                 formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04"||
                      //                 formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "05"
                      //                 ? (value) {;}
                      //                 : (value) {formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender = value;}
                      //                 : (value) {;}
                      //         ),
                      //         Text("Perempuan")
                      //       ],
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   inputFormatters: [
                      //     WhitelistingTextInputFormatter.digitsOnly,
                      //     // LengthLimitingTextInputFormatter(10),
                      //   ],
                      //   controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerPercentShare,
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: '% Share',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.number,
                      // ),
                    ],
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyPemegangSahamPribadiChangeNotifier>(
                    builder: (context, formMCompanyPemegangSahamPribadiChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMCompanyPemegangSahamPribadiChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          );
        },
      )
    );
  }
}
