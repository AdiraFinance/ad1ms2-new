import 'package:ad1ms2_dev/screens/app/info_credit_income.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_structure.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_structure_type_installment.dart';
import 'package:ad1ms2_dev/screens/app/info_wmp.dart';
import 'package:ad1ms2_dev/screens/app/list_additional_insurance.dart';
import 'package:ad1ms2_dev/screens/app/list_major_insurance.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_info_nasabah.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_alamat.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_keluarga(ibu).dart';
import 'package:ad1ms2_dev/screens/form_m/list_informasi_keluarga.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class MenuDetailLoan extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
         var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context, listen: true);

        var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: true);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => InfoCreditStructure()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Struktur Kredit"),
                                        _providerCreditStructure.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerCreditStructure.autoValidate
                    ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                    : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    Visibility(visible: _providerCreditStructure.isVisible,
                        child: SizedBox(height: MediaQuery.of(context).size.height / 87)
                    ),
                    Visibility(
                      visible: _providerCreditStructure.isVisible,
                      child: InkWell(
                          onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => InfoCreditStructureTypeInstallment()));
                          },
                          child: Card(
                              elevation: 3.3,
                              child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                          Text(
                                              "Info Struktur Kredit Jenis Angsuran"),
                                          _providerCreditStructure.flag
                                              ? Icon(Icons.check, color: primaryOrange)
                                              : SizedBox()
                                      ],
                                  ),
                              ),
                          ),
                      ),
                    ),
                    Visibility(
                      visible: _providerCreditStructure.isVisible,
                      child: _providerCreditStructure.autoValidate
                        ? Container(
                          margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                          child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                        )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    ),
                    Visibility(visible: _providerCreditStructure.isVisible,
                        child: SizedBox(height: MediaQuery.of(context).size.height / 87)
                    ),
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ListMajorInsurance()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Asuransi Utama"),
                                        _providerMajorInsurance.listFormMajorInsurance.length != 0
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerMajorInsurance.flag
                    ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                    : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    _providerColla.collateralTypeModel.id == "001"
                    ? _providerColla.groupObjectSelected.KODE == "002"
                        ? InkWell(
                            onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ListAdditionalInsurance()));
                            },
                            child: Card(
                                elevation: 3.3,
                                child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                            Text(
                                                "Informasi Asuransi Tambahan"),
                                            _providerAdditionalInsurance.listFormAdditionalInsurance.length != 0
                                                ? Icon(Icons.check, color: primaryOrange)
                                                : SizedBox()
                                        ],
                                    ),
                                ),
                            ),
                        )
                        : SizedBox()
                    : SizedBox(),
                    _providerColla.collateralTypeModel.id == "001"
                    ? _providerColla.groupObjectSelected.KODE == "002"
                        ? _providerAdditionalInsurance.flag
                            ? Container(
                                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                                child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                            )
                            : SizedBox()
                        : SizedBox()
                    : SizedBox(),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => InfoWMP()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi WMP"),
                                        _providerCreditStructure.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => InfoCreditIncome()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Kredit Income"),
                                        _providerCreditStructure.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
//                    _providerCreditStructure.autoValidate
//                        ? Container(
//                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
//                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
//                    )
//                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }
}
