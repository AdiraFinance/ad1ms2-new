import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/add_address_individu.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListAlamatKorespondensi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(fontFamily: "NunitoSans", accentColor: myPrimaryColor),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Korespondensi",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
            // IconButton(
            //     icon: Icon(Icons.info_outline),
            //     onPressed: (){
            //       Provider.of<FormMInfoAlamatChangeNotif>(context, listen: true).iconShowDialog(context);
            //     })
          ],
        ),
        body: Consumer<FormMInfoAlamatChangeNotif>(
          builder: (context, formMinfoAlamat, _) {
            return formMinfoAlamat.listAlamatKorespondensi.isEmpty
            ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
            : ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  // onLongPress: () {
                  //   formMinfoAlamat.selectedIndex = index;
                  //   formMinfoAlamat.controllerInfoAlamat.clear();
                  //   formMinfoAlamat.setAddress(formMinfoAlamat.listAlamatKorespondensi[index]);
                  //   formMinfoAlamat.setCorrespondence(index);
                  //   Navigator.pop(context);
                  // },
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChangeNotifierProvider(
                          create: (context) => FormMAddAddressIndividuChangeNotifier(),
                          child: AddAddressIndividu(
                              flag: 1,
                              index: index,
                              addressModel: formMinfoAlamat.listAlamatKorespondensi[index],
                              typeAddress: 1
                          ),
                        )
                      )
                    ).then((value) => formMinfoAlamat.listAlamatKorespondensi[index].isCorrespondence ? formMinfoAlamat.setAddress(formMinfoAlamat.listAlamatKorespondensi[index]) : null );
                  },
                  child: Card(
                    shape: formMinfoAlamat.selectedIndex == index
                        ? RoundedRectangleBorder(
                            side: BorderSide(color: primaryOrange, width: 2),
                            borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.DESKRIPSI, style: TextStyle(fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].address}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.KEC_NAME}, ${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.KABKOT_NAME}, ${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.PROV_NAME}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.ZIPCODE}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].areaCode} ${formMinfoAlamat.listAlamatKorespondensi[index].phone}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),)
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child:
                              // formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                              //     ? formMinfoAlamat.listAlamatKorespondensi[index].isSameWithIdentity
                              //     ? SizedBox()
                              //     :
                              GestureDetector(
                                  // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                  onTap: () {formMinfoAlamat.moreDialog(context, index);},
                                  child: Icon(Icons.more_vert, color: Colors.grey)
                              )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
                          ],
                        )
                    ),
                  ),
                );
              },
              itemCount: formMinfoAlamat.listAlamatKorespondensi.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                  create: (context) => FormMAddAddressIndividuChangeNotifier(),
                  child: AddAddressIndividu(
                    flag: 0,
                    index: null,
                    addressModel: null,
                    typeAddress: 1,
                  )
                )
              )
            );
            // .then((value) => Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false).isShowDialog(context))
          },
          backgroundColor: myPrimaryColor,
          child: Icon(Icons.add, color: Colors.black),
        ),
      ),
    );
  }
}
