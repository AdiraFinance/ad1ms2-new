import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_occupation_address_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'add_address_individu.dart';
import 'add_occupation_address.dart';

class ListOccupationAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(fontFamily: "NunitoSans", accentColor: myPrimaryColor, primarySwatch: primaryOrange),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Pekerjaan",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
            // IconButton(
            //     icon: Icon(Icons.info_outline),
            //     onPressed: (){
            //       Provider.of<FormMOccupationChangeNotif>(context, listen: true).iconShowDialog(context);
            //     })
          ],
        ),
        body: Consumer<FormMOccupationChangeNotif>(
          builder: (context, formMOccupationChangeNotif, _) {
            return formMOccupationChangeNotif.listOccupationAddress.isEmpty
            ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Alamat Tempat Pekerjaan", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
            : ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
//                 onLongPress: () {
//                   formMOccupationChangeNotif.selectedIndex = index;
//                   formMOccupationChangeNotif.setCorrespondenceAddress(
//                       formMOccupationChangeNotif.listOccupationAddress[index],index);
//                   Navigator.pop(context);
// //                  formMOccupationChangeNotif.controllerInfoAlamat.text =
// //                      formMOccupationChangeNotif.listOccupationAddress[index].address;
//                 },
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangeNotifierProvider(
                                create: (context) =>
                                    FormMAddAddressIndividuChangeNotifier(),
                                child: AddAddressIndividu(
                                    flag: 1,
                                    index: index,
                                    addressModel: formMOccupationChangeNotif.listOccupationAddress[index],
                                    typeAddress: 2)
                            )
                        )
                    ).then((value) => formMOccupationChangeNotif.listOccupationAddress[index].isCorrespondence ? formMOccupationChangeNotif.setCorrespondenceAddress(formMOccupationChangeNotif.listOccupationAddress[index],index) : null );
                  },
                  child: Card(
                    shape: formMOccupationChangeNotif.selectedIndex == index
                        ? RoundedRectangleBorder(
                            side: BorderSide(color: primaryOrange, width: 2),
                            borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(formMOccupationChangeNotif.listOccupationAddress[index].jenisAlamatModel.DESKRIPSI, style: TextStyle(fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMOccupationChangeNotif.listOccupationAddress[index].address}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMOccupationChangeNotif.listOccupationAddress[index].kelurahanModel.KEC_NAME}, ${formMOccupationChangeNotif.listOccupationAddress[index].kelurahanModel.KABKOT_NAME}, ${formMOccupationChangeNotif.listOccupationAddress[index].kelurahanModel.PROV_NAME}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMOccupationChangeNotif.listOccupationAddress[index].kelurahanModel.ZIPCODE}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMOccupationChangeNotif.listOccupationAddress[index].areaCode} ${formMOccupationChangeNotif.listOccupationAddress[index].phone}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),)
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: GestureDetector(
                                // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                  onTap: () {formMOccupationChangeNotif.moreDialog(context, index);},
                                  child: Icon(Icons.more_vert, color: Colors.grey)
                              )
                              // formMOccupationChangeNotif.listOccupationAddress[index].jenisAlamatModel.KODE == "03"
                              //     ? formMOccupationChangeNotif.listOccupationAddress[index].isSameWithIdentity
                              //     ? SizedBox()
                              //     : GestureDetector(
                              //     onTap: () {formMOccupationChangeNotif.deleteListOccupationAddress(context, index);},
                              //     child: Icon(Icons.delete, color: Colors.red)
                              // )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
                          ],
                        )
                      // child: Stack(
                      //   children: [
                      //     Column(
                      //       children: [
                      //         Row(
                      //           children: [
                      //             Expanded(flex: 4, child: Text("Jenis alamat")),
                      //             Text(" : "),
                      //             Expanded(
                      //                 flex: 6,
                      //                 child: Text(formMOccupationChangeNotif
                      //                     .listOccupationAddress[index]
                      //                     .jenisAlamatModel
                      //                     .DESKRIPSI))
                      //           ],
                      //         ),
                      //         Row(
                      //           children: [
                      //             Expanded(flex: 4, child: Text("Alamat")),
                      //             Text(" : "),
                      //             Expanded(
                      //                 flex: 6,
                      //                 child: Text(
                      //                     "${formMOccupationChangeNotif.listOccupationAddress[index].address}"))
                      //           ],
                      //         ),
                      //       ],
                      //     ),
                      //     Align(
                      //       alignment: Alignment.topRight,
                      //       child: formMOccupationChangeNotif.listOccupationAddress[index].jenisAlamatModel.KODE == "01" || formMOccupationChangeNotif.listOccupationAddress[index].jenisAlamatModel.KODE == "03"
                      //           ? SizedBox()
                      //           : IconButton(
                      //           icon: Icon(Icons.delete, color: Colors.red),
                      //           onPressed: () {
                      //             formMOccupationChangeNotif
                      //                 .deleteListOccupationAddress(index);
                      //           }),
                      //     )
                      //   ],
                      // ),
                    ),
                  ),
                );
              },
              itemCount: formMOccupationChangeNotif.listOccupationAddress.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider(
                        create: (context) =>
                            FormMAddAddressIndividuChangeNotifier(),
                        child: AddAddressIndividu(
                          flag: 0,
                          index: null,
                          addressModel: null,
                          typeAddress: 2,
                        )
                    )
                )
            );
            // .then((value) => Provider.of<FormMOccupationChangeNotif>(context, listen: false).isShowDialog(context));
          },
          child: Icon(Icons.add, color: Colors.black),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}
