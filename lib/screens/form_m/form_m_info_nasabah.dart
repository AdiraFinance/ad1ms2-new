import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMInfoNasabah extends StatefulWidget {
  @override
  _FormMInfoNasabahState createState() => _FormMInfoNasabahState();
}

class _FormMInfoNasabahState extends State<FormMInfoNasabah> {
  @override
  void initState() {
    super.initState();
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).setDataSQLite();
  }
  @override
  Widget build(BuildContext context) {
    var _size = MediaQuery.of(context).size;
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: Scaffold(
            appBar: AppBar(
              title:
                  Text("Informasi Nasabah", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body:
            // Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).setupData
            //     ?
            // Center(child: CircularProgressIndicator())
            //     :
            Consumer<FormMInfoNasabahChangeNotif>(
              builder: (context, formMInfoNasabahChangeNotif, _) {
                formMInfoNasabahChangeNotif.setDefaultValue();
                return Form(
                  onWillPop: formMInfoNasabahChangeNotif.onBackPress,
                  key: formMInfoNasabahChangeNotif.keyForm,
                  child: ListView(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height/57,
                        horizontal: MediaQuery.of(context).size.width/37
                    ),
                    children: [
                      DropdownButtonFormField<GCModel>(
                        value: formMInfoNasabahChangeNotif.gcModel,
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          labelText: "Group Customer",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMInfoNasabahChangeNotif.itemsGC.map((data) {
                          return DropdownMenuItem<GCModel>(
                            value: data,
                            child: Text(
                              data.name,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                        onChanged: (newVal) {
                          formMInfoNasabahChangeNotif.gcModel = newVal;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                      ),
                      SizedBox(height: _size.height / 47),
                      DropdownButtonFormField<IdentityModel>(
                        value: formMInfoNasabahChangeNotif.identitasModel,
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        disabledHint: Text(formMInfoNasabahChangeNotif.identitasModel.name),
                        decoration: InputDecoration(
                          labelText: "Jenis Identitas",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMInfoNasabahChangeNotif.items.map((data) {
                          return DropdownMenuItem<IdentityModel>(
                            value: data,
                            child: Text(
                              data.name,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                        onChanged: null,
//                        (newVal) {
//                      formMInfoNasabahChangeNotif.identitasModel = newVal;
//                    },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                      ),
                      SizedBox(height: _size.height / 47),
                      TextFormField(
                          enabled: formMInfoNasabahChangeNotif.isEnableFieldIdentityNumber,
                          controller: formMInfoNasabahChangeNotif.controllerNoIdentitas,
                          autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              labelText: 'Nomor Identitas',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          keyboardType: formMInfoNasabahChangeNotif.identitasModel != null
                              ? formMInfoNasabahChangeNotif.identitasModel.id != "03"
                              ? TextInputType.number
                              : TextInputType.text
                              : TextInputType.number,
                          textCapitalization: TextCapitalization.characters,
                          inputFormatters: formMInfoNasabahChangeNotif.identitasModel != null
                              ? formMInfoNasabahChangeNotif.identitasModel.id != "03"
                              ? [WhitelistingTextInputFormatter.digitsOnly]
                              : null
                              : [WhitelistingTextInputFormatter.digitsOnly]
                      ),
                      SizedBox(height: _size.height / 47),
//                  FocusScope(
//                      node: FocusScopeNode(),
//                      child: TextFormField(
//                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                        onTap: () {
//                          FocusManager.instance.primaryFocus.unfocus();
//                          formMInfoNasabahChangeNotif.selectTglIdentitas(context);
//                        },
//                        controller:
//                            formMInfoNasabahChangeNotif.controllerTglIdentitas,
//                        style: new TextStyle(color: Colors.black),
//                        decoration: new InputDecoration(
//                            labelText: 'Tanggal Identitas',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8))),
//                        readOnly: true,
//                      )),
//                  formMInfoNasabahChangeNotif.identitasModel != null
//                      ? formMInfoNasabahChangeNotif.identitasModel.id != "01"
//                          ? SizedBox(height: _size.height / 47)
//                          : Row(
//                              children: [
//                                Text("KTP berlaku seumur hidup?", style: TextStyle(fontWeight: FontWeight.bold)),
//                                Checkbox(
//                                  activeColor: primaryOrange,
//                                    value:
//                                        formMInfoNasabahChangeNotif.valueCheckBox,
//                                    onChanged: (value) {
//                                      formMInfoNasabahChangeNotif.valueCheckBox =
//                                          value;
//                                    }),
//                                Text("Ya", style: TextStyle(fontWeight: FontWeight.bold)),
//                              ],
//                            )
//                      : SizedBox(height: _size.height / 47),
//                  !formMInfoNasabahChangeNotif.valueCheckBox
//                      ? FocusScope(
//                          node: FocusScopeNode(),
//                          child: Padding(
//                            padding: EdgeInsets.only(bottom: _size.height / 47),
//                            child: TextFormField(
//                              enabled: !formMInfoNasabahChangeNotif.valueCheckBox,
//                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              onTap: () {
//                                FocusManager.instance.primaryFocus.unfocus();
//                                formMInfoNasabahChangeNotif
//                                    .selectTglIdentitasBerlakuSampai(context);
//                              },
//                              controller: formMInfoNasabahChangeNotif
//                                  .controllerIdentitasBerlakuSampai,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                  labelText: 'Identitas Berlaku Sampai',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8))),
//                              readOnly: true,
//                            ),
//                          ))
//                      : SizedBox(height: _size.height / 47),
//                  // SizedBox(height: _size.height / 47),47
                      TextFormField(
                        controller: formMInfoNasabahChangeNotif
                            .controllerNamaLengkapSesuaiIdentitas,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Nama Lengkap Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                      ),
                      SizedBox(height: _size.height / 47),
                      TextFormField(
                        controller: formMInfoNasabahChangeNotif.controllerNamaLengkap,
                        style: new TextStyle(color: Colors.black),
                        enabled: formMInfoNasabahChangeNotif.isEnableFieldIdentityNumber,
                        decoration: new InputDecoration(
                            labelText: 'Nama Lengkap',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                      ),
                      SizedBox(height: _size.height / 47),
                      TextFormField(
                        onTap: formMInfoNasabahChangeNotif.isEnableFieldBirthDate ? () {
                          FocusManager.instance.primaryFocus.unfocus();
                          formMInfoNasabahChangeNotif.selectBirthDate(context);
                        }:null,
                        controller: formMInfoNasabahChangeNotif.controllerTglLahir,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Tanggal Lahir',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        enabled: formMInfoNasabahChangeNotif.isEnableFieldBirthDate,
                        readOnly: true,
                      ),
                      SizedBox(height: _size.height / 47),
                      TextFormField(
                        enabled: formMInfoNasabahChangeNotif.isEnableFieldBirthPlace,
//                    readOnly: !formMInfoNasabahChangeNotif.isEnableFieldBirthPlace,
                        controller: formMInfoNasabahChangeNotif
                            .controllerTempatLahirSesuaiIdentitas,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Tempat Lahir Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                      ),
                      SizedBox(height: _size.height / 47),
                      TextFormField(
                        readOnly: true,
                        onTap: (){
                          formMInfoNasabahChangeNotif.searchBirthPlace(context);
                        },
                        controller: formMInfoNasabahChangeNotif
                            .controllerTempatLahirSesuaiIdentitasLOV,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Tempat Lahir Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                      ),
                      SizedBox(height: _size.height / 47),
                      Text(
                        "Jenis Kelamin",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.15),
                      ),
                      // SizedBox(height: _size.height / 47),
                      Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: "01",
                                  groupValue: formMInfoNasabahChangeNotif.radioValueGender,
                                  onChanged: (value) {
                                    formMInfoNasabahChangeNotif.radioValueGender = value;
                                  }),
                              Text("Laki Laki")
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: "02",
                                  groupValue: formMInfoNasabahChangeNotif.radioValueGender,
                                  onChanged: (value) {
                                    formMInfoNasabahChangeNotif.radioValueGender = value;
                                  }),
                              Text("Perempuan")
                            ],
                          ),
                        ],
                      ),
//                  SizedBox(height: _size.height / 47),
//                  DropdownButtonFormField<ReligionModel>(
//                    value: formMInfoNasabahChangeNotif.religionSelected,
//                    autovalidate: formMInfoNasabahChangeNotif.autoValidate,
//                    validator: (e) {
//                      if (e == null) {
//                        return "Tidak boleh kosong";
//                      } else {
//                        return null;
//                      }
//                    },
//                    decoration: InputDecoration(
//                      labelText: "Agama",
//                      border: OutlineInputBorder(),
//                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
//                    ),
//                    items: formMInfoNasabahChangeNotif.itemsReligion.map((data) {
//                      return DropdownMenuItem<ReligionModel>(
//                        value: data,
//                        child: Text(
//                          data.text,
//                          overflow: TextOverflow.ellipsis,
//                        ),
//                      );
//                    }).toList(),
//                    onChanged: (newVal) {
//                      formMInfoNasabahChangeNotif.religionSelected = newVal;
//                    },
//                    onTap: () {
//                      FocusManager.instance.primaryFocus.unfocus();
//                    },
//                  ),
                      SizedBox(height: _size.height / 47),
                      DropdownButtonFormField<EducationModel>(
                        value: formMInfoNasabahChangeNotif.educationSelected,
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          labelText: "Pendidikan",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items: formMInfoNasabahChangeNotif.listEducation.map((data) {
                          return DropdownMenuItem<EducationModel>(
                            value: data,
                            child: Text(
                              data.text,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                        onChanged: (newVal) {
                          formMInfoNasabahChangeNotif.educationSelected = newVal;
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                      ),
//                  SizedBox(height: _size.height / 47),
//                  TextFormField(
//                    controller:
//                        formMInfoNasabahChangeNotif.controllerJumlahTanggungan,
//                    style: new TextStyle(color: Colors.black),
//                    decoration: new InputDecoration(
//                        labelText: 'Jumlah Tanggungan',
//                        labelStyle: TextStyle(color: Colors.black),
//                        border: OutlineInputBorder(
//                            borderRadius: BorderRadius.circular(8))),
//                    keyboardType: TextInputType.number,
//                    inputFormatters: [
//                      WhitelistingTextInputFormatter.digitsOnly,
//                      // LengthLimitingTextInputFormatter(10),
//                    ],
//                    autovalidate: formMInfoNasabahChangeNotif.autoValidate,
//                    validator: (e) {
//                      if (e.isEmpty) {
//                        return "Tidak boleh kosong";
//                      } else {
//                        return null;
//                      }
//                    },
//                  ),
                      SizedBox(height: _size.height / 47),
                      DropdownButtonFormField<MaritalStatusModel>(
                        value: formMInfoNasabahChangeNotif.maritalStatusSelected,
                        autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                        validator: (e) {
                          if (e == null) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          labelText: "Status Pernikahan",
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                        items:
                        formMInfoNasabahChangeNotif.listMaritalStatus.map((data) {
                          return DropdownMenuItem<MaritalStatusModel>(
                            value: data,
                            child: Text(
                              data.text,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                        onChanged: (newVal) {
                          formMInfoNasabahChangeNotif.maritalStatusSelected = newVal;
                          formMInfoNasabahChangeNotif.checkNeedGuarantor();
                        },
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                      ),
//                  SizedBox(height: _size.height / 47),
//                  TextFormField(
//                    controller: formMInfoNasabahChangeNotif.controllerEmail,
//                    style: new TextStyle(color: Colors.black),
//                    decoration: new InputDecoration(
//                        labelText: 'Email',
//                        labelStyle: TextStyle(color: Colors.black),
//                        border: OutlineInputBorder(
//                            borderRadius: BorderRadius.circular(8))),
//                    keyboardType: TextInputType.text,
//                    textCapitalization: TextCapitalization.characters,
//                    autovalidate: formMInfoNasabahChangeNotif.autoValidate,
//                    validator: formMInfoNasabahChangeNotif.validateEmail,
//                  ),
                      SizedBox(height: _size.height / 47),
                      Row(
                        children: [
                          Expanded(
                            flex: 7,
                            child: TextFormField(
                              controller: formMInfoNasabahChangeNotif.controllerNoHp,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'No Handphone 1',
                                  labelStyle: TextStyle(color: Colors.black),
                                  prefixText: "08",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              keyboardType: TextInputType.number,
                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp(r"^[1-9][0-9]*$"))
                              ],
                            ),
                          ),
                          Expanded(
                              flex: 3,
                              child: Row(
                                children: [
                                  Checkbox(
                                      activeColor: primaryOrange,
                                      value: formMInfoNasabahChangeNotif.isNoHp1WA,
                                      onChanged: (value) {
                                        formMInfoNasabahChangeNotif.isNoHp1WA = value;
                                      }),
                                  Text(
                                    "WA",
                                    style: TextStyle(
                                        fontSize: 16, fontWeight: FontWeight.bold),
                                  )
                                ],
                              ))
                        ],
                      ),
//                  SizedBox(height: _size.height / 47),
//                  Row(
//                    children: [
//                      Expanded(
//                        flex: 7,
//                        child: TextFormField(
//                          controller: formMInfoNasabahChangeNotif.controllerNoHp2,
//                          style: new TextStyle(color: Colors.black),
//                          decoration: new InputDecoration(
//                              labelText: 'No Handphone 2',
//                              labelStyle: TextStyle(color: Colors.black),
//                              prefixText: "08",
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8))),
//                          keyboardType: TextInputType.number,
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter(RegExp(r"^[1-9][0-9]*$"))
//                          ],
//                        ),
//                      ),
//                      Expanded(
//                          flex: 3,
//                          child: Row(
//                            children: [
//                              Checkbox(
//                                  activeColor: primaryOrange,
//                                  value: formMInfoNasabahChangeNotif.isNoHp2WA,
//                                  onChanged: (value) {
//                                    formMInfoNasabahChangeNotif.isNoHp2WA = value;
//                                  }),
//                              Text("WA",
//                                  style: TextStyle(
//                                      fontSize: 16, fontWeight: FontWeight.bold)),
//                            ],
//                          ))
//                    ],
//                  ),
//                  SizedBox(height: _size.height / 47),
//                  Row(
//                    children: [
//                      Expanded(
//                        flex: 7,
//                        child: TextFormField(
//                          controller: formMInfoNasabahChangeNotif.controllerNoHp3,
//                          style: new TextStyle(color: Colors.black),
//                          decoration: new InputDecoration(
//                              labelText: 'No Handphone 3',
//                              labelStyle: TextStyle(color: Colors.black),
//                              prefixText: "08",
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8))),
//                          keyboardType: TextInputType.number,
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter(RegExp(r"^[1-9][0-9]*$"))
//                          ],
//                        ),
//                      ),
//                      Expanded(
//                          flex: 3,
//                          child: Row(
//                            children: [
//                              Checkbox(
//                                  activeColor: primaryOrange,
//                                  value: formMInfoNasabahChangeNotif.isNoHp3WA,
//                                  onChanged: (value) {
//                                    formMInfoNasabahChangeNotif.isNoHp3WA = value;
//                                  }),
//                              Text("WA",
//                                  style: TextStyle(
//                                      fontSize: 16, fontWeight: FontWeight.bold)),
//                            ],
//                          ))
//                    ],
//                  ),
                      SizedBox(height: _size.height / 47),
                      Text(
                        "Punya NPWP ?",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.15),
                      ),
                      // SizedBox(height: _size.height / 47),
                      Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 1,
                                  groupValue:
                                  formMInfoNasabahChangeNotif.radioValueIsHaveNPWP,
                                  onChanged: (value) {
                                    formMInfoNasabahChangeNotif.radioValueIsHaveNPWP =
                                        value;
                                  }),
                              Text("Ya")
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 0,
                                  groupValue:
                                  formMInfoNasabahChangeNotif.radioValueIsHaveNPWP,
                                  onChanged: (value) {
                                    formMInfoNasabahChangeNotif.radioValueIsHaveNPWP =
                                        value;
                                  }),
                              Text("Tidak")
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: _size.height / 47),
                      formMInfoNasabahChangeNotif.radioValueIsHaveNPWP == 1
                          ? Column(
                        children: [
                          TextFormField(
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              // LengthLimitingTextInputFormatter(10),
                            ],
                            controller:
                            formMInfoNasabahChangeNotif.controllerNoNPWP,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                labelText: 'Nomor NPWP',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            keyboardType: TextInputType.number,
//                              autovalidate:
//                                  formMInfoNasabahChangeNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
                          ),
//                            SizedBox(height: _size.height / 47),
//                            TextFormField(
//                              controller: formMInfoNasabahChangeNotif
//                                  .controllerNamaSesuaiNPWP,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                  labelText: 'Nama Sesuai NPWP',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8))),
//                              keyboardType: TextInputType.text,
//                              textCapitalization: TextCapitalization.characters,
//                              autovalidate:
//                                  formMInfoNasabahChangeNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                            ),
//                            SizedBox(height: _size.height / 47),
//                            DropdownButtonFormField<JenisNPWPModel>(
//                              value: formMInfoNasabahChangeNotif.jenisNPWPSelected,
//                              autovalidate:
//                                  formMInfoNasabahChangeNotif.autoValidate,
//                              validator: (e) {
//                                if (e == null) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              decoration: InputDecoration(
//                                labelText: "Jenis NPWP",
//                                border: OutlineInputBorder(),
//                                contentPadding:
//                                    EdgeInsets.symmetric(horizontal: 10),
//                              ),
//                              items: formMInfoNasabahChangeNotif.listJenisNPWP
//                                  .map((data) {
//                                return DropdownMenuItem<JenisNPWPModel>(
//                                  value: data,
//                                  child: Text(
//                                    data.text,
//                                    overflow: TextOverflow.ellipsis,
//                                  ),
//                                );
//                              }).toList(),
//                              onChanged: (newVal) {
//                                formMInfoNasabahChangeNotif.jenisNPWPSelected =
//                                    newVal;
//                              },
//                              onTap: () {
//                                FocusManager.instance.primaryFocus.unfocus();
//                              },
//                            ),
//                            SizedBox(height: _size.height / 47),
//                            DropdownButtonFormField<TandaPKPModel>(
//                              value: formMInfoNasabahChangeNotif.tandaPKPSelected,
//                              autovalidate:
//                                  formMInfoNasabahChangeNotif.autoValidate,
//                              validator: (e) {
//                                if (e == null) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              decoration: InputDecoration(
//                                labelText: "Tanda PKP",
//                                border: OutlineInputBorder(),
//                                contentPadding:
//                                    EdgeInsets.symmetric(horizontal: 10),
//                              ),
//                              items: formMInfoNasabahChangeNotif.listTandaPKP
//                                  .map((data) {
//                                return DropdownMenuItem<TandaPKPModel>(
//                                  value: data,
//                                  child: Text(
//                                    data.text,
//                                    overflow: TextOverflow.ellipsis,
//                                  ),
//                                );
//                              }).toList(),
//                              onChanged: (newVal) {
//                                formMInfoNasabahChangeNotif.tandaPKPSelected =
//                                    newVal;
//                              },
//                              onTap: () {
//                                FocusManager.instance.primaryFocus.unfocus();
//                              },
//                            ),
                          SizedBox(height: _size.height / 47),
                          TextFormField(
                            controller: formMInfoNasabahChangeNotif
                                .controllerAlamatSesuaiNPWP,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                labelText: 'Alamat Sesuai NPWP',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            autovalidate:
                            formMInfoNasabahChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ],
                      )
                          : SizedBox()
                    ],
                  ),
                );
              },
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMInfoNasabahChangeNotif>(
                    builder: (context, formMInfoNasabahChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMInfoNasabahChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
      )
    );
  }

}
