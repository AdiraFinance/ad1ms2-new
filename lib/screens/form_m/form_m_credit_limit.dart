import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/list_oid_model.dart';
import 'package:ad1ms2_dev/models/type_offer_model.dart';
import 'package:ad1ms2_dev/shared/form_m_credit_limit_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMCreditLimit extends StatefulWidget {
  final String flag;
  final ListOidModel model;
  const FormMCreditLimit({this.flag, this.model});

  @override
  _FormMCreditLimitState createState() => _FormMCreditLimitState();
}

class _FormMCreditLimitState extends State<FormMCreditLimit> {

  @override
  void initState() {
    super.initState();
    Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false).setValue(widget.model);
    Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false).getToken(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
      ),
      child: Consumer<FormMCreditLimitChangeNotifier>(
        builder: (context, formMCreditLimitChangeNotif, _) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: myPrimaryColor,
              centerTitle: true,
              title: Text("Credit Limit", style: TextStyle(color: Colors.black)),
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              onWillPop: formMCreditLimitChangeNotif.onBackPress,
              key: formMCreditLimitChangeNotif.keyForm,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  TextFormField(
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerConsumerName,
                    autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Nama Konsumen',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerTelephoneNumber,
                    autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Nomer Telepon',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      // LengthLimitingTextInputFormatter(10),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerConsumerAddress,
                    autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Alamat Konsumen',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerCustomerType,
                    autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Jenis Nasabah',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  // TextFormField(
                  //   enabled: false,
                  //   controller: formMCreditLimitChangeNotif.controllerNotesAboutConsumer,
                  //   autovalidate: formMCreditLimitChangeNotif.autoValidate,
                  //   validator: (e) {
                  //     if (e.isEmpty) {
                  //       return "Tidak boleh kosong";
                  //     } else {
                  //       return null;
                  //     }
                  //   },
                  //   style: new TextStyle(color: Colors.black),
                  //   decoration: new InputDecoration(
                  //       labelText: 'Catatan Tentang Konsumen',
                  //       labelStyle: TextStyle(color: Colors.black),
                  //       border: OutlineInputBorder(
                  //           borderRadius: BorderRadius.circular(8))),
                  //   keyboardType: TextInputType.text,
                  //   textCapitalization: TextCapitalization.characters,
                  // ),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47),
                  DropdownButtonFormField<TypeOfferModel>(
                      autovalidate:
                      formMCreditLimitChangeNotif.autoValidate,
                      validator: (e) {
                        if (e == null) {
                          return "Silahkan pilih jenis penawaran";
                        } else {
                          return null;
                        }
                      },
                      value: formMCreditLimitChangeNotif.typeOfferSelected,
                      onChanged: (value) {
                        formMCreditLimitChangeNotif.typeOfferSelected = value;
                      },
                      onTap: () {
                        FocusManager.instance.primaryFocus.unfocus();
                      },
                      decoration: InputDecoration(
                        labelText: "Jenis Penawaran",
                        border: OutlineInputBorder(),
                        contentPadding:
                        EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: formMCreditLimitChangeNotif.listTypeOffer
                          .map((value) {
                        return DropdownMenuItem<TypeOfferModel>(
                          value: value,
                          child: Text(
                            value.DESCRIPTION,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  formMCreditLimitChangeNotif.typeOfferSelected != null
                    ? formMCreditLimitChangeNotif.typeOfferSelected.KODE != "02"
                      ? Container(
                          margin: EdgeInsets.only(
                            bottom: MediaQuery.of(context).size.height / 47
                          ),
                          child: TextFormField(
                              autovalidate:
                              formMCreditLimitChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null) {
                                  return "Silahkan pilih nomor referensi";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMCreditLimitChangeNotif.controllerReferenceNumberCL,
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                                formMCreditLimitChangeNotif.searchReferenceNumberCL(context);
                              },
                              decoration: InputDecoration(
                                labelText: "Nomor Referensi",
                                border: OutlineInputBorder(),
                                contentPadding:
                                EdgeInsets.symmetric(horizontal: 10),
                              ),
                          ),
                        )
                      : SizedBox(height: 0.0,width: 0.0)
                    : SizedBox(height: 0.0,width: 0.0),
                  TextFormField(
                    // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerGrading,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Grading',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Text(
                    "Apakah akan melanjutkan proses penginputan atau tidak?",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.15),
                  ),
                  // SizedBox(height: _size.height / 47),
                  Row(
                    children: [
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: 1,
                              groupValue: formMCreditLimitChangeNotif.radioProceedProcess,
                              onChanged: (value) {
                                // formMCreditLimitChangeNotif.getInformation(context);
                                formMCreditLimitChangeNotif.radioProceedProcess = value;
                              }),
                          Text("Ya")
                        ],
                      ),
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: 0,
                              groupValue: formMCreditLimitChangeNotif.radioProceedProcess,
                              onChanged: (value) {
                                formMCreditLimitChangeNotif.radioProceedProcess = value;
                              }),
                          Text("Tidak")
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  formMCreditLimitChangeNotif.radioProceedProcess == 1
                  ? TextFormField(
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerNotes,
                    autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Notes',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    maxLines: 3,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  )
                  : Column(
                    children: [
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        controller: formMCreditLimitChangeNotif.controllerMaxPH,
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Max PH',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerMaxInstallment,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Max Installment',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   controller: formMCreditLimitChangeNotif.controllerBiddingType,
                      //   autovalidate: formMCreditLimitChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'Jenis Penawaran',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      //   enabled: false,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerPortofolio,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Portofolio',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerTenor,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Tenor',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   controller: formMCreditLimitChangeNotif.controllerReference,
                      //   autovalidate: formMCreditLimitChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   style: new TextStyle(color: Colors.black),
                      //   decoration: new InputDecoration(
                      //       labelText: 'No Reference',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      //   enabled: false,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        controller: formMCreditLimitChangeNotif.controllerMultidisburseOption,
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Opsi Multidisburse',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        controller: formMCreditLimitChangeNotif.controllerSearchingFrom,
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Pencarian ke',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      )
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
//                       Expanded(
//                         flex: 5,
//                         child: RaisedButton(
//                             shape: RoundedRectangleBorder(
//                                 borderRadius: new BorderRadius.circular(8.0)),
//                             color: myPrimaryColor,
//                             onPressed: () {
//                               Navigator.push(
//                                   context,
//                                   MaterialPageRoute(builder: (context) => FormMParent(flag: widget.flag)));
// //                          if (_selectedIndex != -1) {
// //                            if (widget.flag != 0) {
// //                              Navigator.push(
// //                                  context,
// //                                  MaterialPageRoute(
// //                                      builder: (context) => FormMParent()));
// //                            } else {
// //                              Navigator.push(
// //                                  context,
// //                                  MaterialPageRoute(
// //                                      builder: (context) => FormMCompanyParent()));
// //                            }
// //                          }
//                         },
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           children: <Widget>[
//                             Text("OID BARU",
//                                 style: TextStyle(
//                                     color: Colors.black,
//                                     fontSize: 14,
//                                     fontWeight: FontWeight.w500,
//                                     letterSpacing: 1.25)
//                             )
//                           ],
//                         )),
//                       ),
//                       SizedBox(width: MediaQuery.of(context).size.width / 37),
                      Expanded(
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0)),
                            color: myPrimaryColor,
                            onPressed: () {
                              formMCreditLimitChangeNotif.check(context);
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(flag: widget.flag)));
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("SUBMIT",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25)
                                )
                              ],
                            )
                        ),
                      ),
                    ],
                  )
              ),
            ),
          );
        },
      ),
    );
  }
}
