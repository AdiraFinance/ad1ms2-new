import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/screens/form_m/list_guarantor_address_company.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_company_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddGuarantorCompany extends StatefulWidget {
  final int flag;
  final int index;
  final GuarantorCompanyModel model;
  const AddGuarantorCompany({this.flag, this.index, this.model});
  @override
  _AddGuarantorCompanyState createState() => _AddGuarantorCompanyState();
}

class _AddGuarantorCompanyState extends State<AddGuarantorCompany> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    // if (widget.flag != 0) {
      _setValueForEdit = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(
              context,
              listen: false)
          .setValueForEdit(widget.model, widget.flag);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        primaryColor: Colors.black,
        fontFamily: "NunitoSans",
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              widget.flag == 0
                  ? "Tambah Penjamin Kelembagaan"
                  : "Edit Penjamin Kelembagaan",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width / 27,
              vertical: MediaQuery.of(context).size.height / 57),
          child:
          // widget.flag == 0
          //     ? Consumer<FormMAddGuarantorCompanyChangeNotifier>(
          //         builder: (context, formMAdGuarantorCompanyChangeNotif, _) {
          //           return Form(
          //             key: formMAdGuarantorCompanyChangeNotif.key,
          //             onWillPop: _onWillPop,
          //             child: Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: [
          //                 DropdownButtonFormField<TypeInstitutionModel>(
          //                   autovalidate:
          //                       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                   validator: (e) {
          //                     if (e == null) {
          //                       return "Tidak boleh kosong";
          //                     } else {
          //                       return null;
          //                     }
          //                   },
          //                   value: formMAdGuarantorCompanyChangeNotif
          //                       .typeInstitutionSelected,
          //                   onChanged: (value) {
          //                     formMAdGuarantorCompanyChangeNotif
          //                         .typeInstitutionSelected = value;
          //                   },
          //                   onTap: () {
          //                     FocusManager.instance.primaryFocus.unfocus();
          //                   },
          //                   decoration: InputDecoration(
          //                     labelText: "Jenis Lembaga",
          //                     border: OutlineInputBorder(),
          //                     contentPadding:
          //                         EdgeInsets.symmetric(horizontal: 10),
          //                   ),
          //                   items: formMAdGuarantorCompanyChangeNotif
          //                       .listTypeInstitution
          //                       .map((value) {
          //                     return DropdownMenuItem<TypeInstitutionModel>(
          //                       value: value,
          //                       child: Text(
          //                         value.PARA_NAME,
          //                         overflow: TextOverflow.ellipsis,
          //                       ),
          //                     );
          //                   }).toList(),
          //                 ),
          //                 SizedBox(
          //                     height: MediaQuery.of(context).size.height / 47),
          //                 DropdownButtonFormField<ProfilModel>(
          //                   autovalidate:
          //                       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                   validator: (e) {
          //                     if (e == null) {
          //                       return "Tidak boleh kosong";
          //                     } else {
          //                       return null;
          //                     }
          //                   },
          //                   value: formMAdGuarantorCompanyChangeNotif
          //                       .profilSelected,
          //                   onChanged: (value) {
          //                     formMAdGuarantorCompanyChangeNotif
          //                         .profilSelected = value;
          //                   },
          //                   onTap: () {
          //                     FocusManager.instance.primaryFocus.unfocus();
          //                   },
          //                   decoration: InputDecoration(
          //                     labelText: "Profil",
          //                     border: OutlineInputBorder(),
          //                     contentPadding:
          //                         EdgeInsets.symmetric(horizontal: 10),
          //                   ),
          //                   items: formMAdGuarantorCompanyChangeNotif.listProfil
          //                       .map((value) {
          //                     return DropdownMenuItem<ProfilModel>(
          //                       value: value,
          //                       child: Text(
          //                         value.text,
          //                         overflow: TextOverflow.ellipsis,
          //                       ),
          //                     );
          //                   }).toList(),
          //                 ),
          //                 SizedBox(
          //                     height: MediaQuery.of(context).size.height / 47),
          //                 TextFormField(
          //                   autovalidate:
          //                       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                   validator: (e) {
          //                     if (e.isEmpty) {
          //                       return "Tidak boleh kosong";
          //                     } else {
          //                       return null;
          //                     }
          //                   },
          //                   controller: formMAdGuarantorCompanyChangeNotif
          //                       .controllerInstitutionName,
          //                   style: new TextStyle(color: Colors.black),
          //                   decoration: new InputDecoration(
          //                       labelText: 'Nama Lembaga',
          //                       labelStyle: TextStyle(color: Colors.black),
          //                       border: OutlineInputBorder(
          //                           borderRadius: BorderRadius.circular(8))),
          //                   keyboardType: TextInputType.text,
          //                   textCapitalization: TextCapitalization.characters,
          //                 ),
          //                 SizedBox(
          //                     height: MediaQuery.of(context).size.height / 47),
          //                 TextFormField(
          //                   autovalidate:
          //                       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                   validator: (e) {
          //                     if (e.isEmpty) {
          //                       return "Tidak boleh kosong";
          //                     } else {
          //                       return null;
          //                     }
          //                   },
          //                   inputFormatters: [
          //                     WhitelistingTextInputFormatter.digitsOnly,
          //                     // LengthLimitingTextInputFormatter(10),
          //                   ],
          //                   controller: formMAdGuarantorCompanyChangeNotif
          //                       .controllerNPWP,
          //                   style: new TextStyle(color: Colors.black),
          //                   decoration: new InputDecoration(
          //                       labelText: 'NPWP',
          //                       labelStyle: TextStyle(color: Colors.black),
          //                       border: OutlineInputBorder(
          //                           borderRadius: BorderRadius.circular(8))),
          //                   keyboardType: TextInputType.number,
          //                 ),
          //                 SizedBox(
          //                     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   onTap: () {
          //                 //     Navigator.push(
          //                 //         context,
          //                 //         MaterialPageRoute(
          //                 //             builder: (context) =>
          //                 //                 ListGuarantorAddressCompany()));
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerAddress,
          //                 //   style: TextStyle(color: Colors.black),
          //                 //   decoration: new InputDecoration(
          //                 //       labelText: 'Alamat',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   readOnly: true,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   readOnly: true,
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerAddressType,
          //                 //   style: new TextStyle(color: Colors.black),
          //                 //   decoration: new InputDecoration(
          //                 //       filled: true,
          //                 //       fillColor: Colors.black12,
          //                 //       labelText: 'Jenis Alamat',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   enabled: false,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 5,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerRT,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'RT',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(width: 8),
          //                 //     Expanded(
          //                 //         flex: 5,
          //                 //         child: TextFormField(
          //                 //           readOnly: true,
          //                 //           autovalidate:
          //                 //               formMAdGuarantorCompanyChangeNotif
          //                 //                   .autoValidate,
          //                 //           validator: (e) {
          //                 //             if (e.isEmpty) {
          //                 //               return "Tidak boleh kosong";
          //                 //             } else {
          //                 //               return null;
          //                 //             }
          //                 //           },
          //                 //           controller:
          //                 //               formMAdGuarantorCompanyChangeNotif
          //                 //                   .controllerRW,
          //                 //           style: new TextStyle(color: Colors.black),
          //                 //           decoration: new InputDecoration(
          //                 //               filled: true,
          //                 //               fillColor: Colors.black12,
          //                 //               labelText: 'RW',
          //                 //               labelStyle:
          //                 //                   TextStyle(color: Colors.black),
          //                 //               border: OutlineInputBorder(
          //                 //                   borderRadius:
          //                 //                       BorderRadius.circular(8))),
          //                 //           enabled: false,
          //                 //         ))
          //                 //   ],
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   readOnly: true,
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerKelurahan,
          //                 //   style: new TextStyle(color: Colors.black),
          //                 //   decoration: new InputDecoration(
          //                 //       filled: true,
          //                 //       fillColor: Colors.black12,
          //                 //       labelText: 'Kelurahan',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   enabled: false,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   readOnly: true,
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerKecamatan,
          //                 //   style: new TextStyle(color: Colors.black),
          //                 //   decoration: new InputDecoration(
          //                 //       filled: true,
          //                 //       fillColor: Colors.black12,
          //                 //       labelText: 'Kecamatan',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   enabled: false,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   readOnly: true,
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerKota,
          //                 //   style: TextStyle(color: Colors.black),
          //                 //   decoration: InputDecoration(
          //                 //       filled: true,
          //                 //       fillColor: Colors.black12,
          //                 //       labelText: 'Kabupaten/Kota',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   enabled: false,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 7,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerProvinsi,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Provinsi',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(width: 8),
          //                 //     Expanded(
          //                 //       flex: 3,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerPostalCode,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Kode Pos',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //   ],
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 4,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerTelephoneArea1,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Telepon 1 (Area)',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(
          //                 //         width:
          //                 //             MediaQuery.of(context).size.width / 37),
          //                 //     Expanded(
          //                 //       flex: 6,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         inputFormatters: [
          //                 //           WhitelistingTextInputFormatter.digitsOnly,
          //                 //           LengthLimitingTextInputFormatter(10),
          //                 //         ],
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerTelephone1,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Telepon 1',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //   ],
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 4,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerTelephoneArea2,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Telepon 2 (Area)',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(
          //                 //         width:
          //                 //             MediaQuery.of(context).size.width / 37),
          //                 //     Expanded(
          //                 //       flex: 6,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         inputFormatters: [
          //                 //           WhitelistingTextInputFormatter.digitsOnly,
          //                 //           LengthLimitingTextInputFormatter(10),
          //                 //         ],
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerTelephone2,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Telepon 2',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //   ],
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 4,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerFaxArea,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Fax (Area)',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(
          //                 //         width:
          //                 //             MediaQuery.of(context).size.width / 37),
          //                 //     Expanded(
          //                 //       flex: 6,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         inputFormatters: [
          //                 //           WhitelistingTextInputFormatter.digitsOnly,
          //                 //           LengthLimitingTextInputFormatter(10),
          //                 //         ],
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerFax,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Fax',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //   ],
          //                 // ),
          //               ],
          //             ),
          //           );
          //         },
          //       )
          //     :
          FutureBuilder(
                  future: _setValueForEdit,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return Consumer<FormMAddGuarantorCompanyChangeNotifier>(
                      builder:
                          (context, formMAdGuarantorCompanyChangeNotif, _) {
                        return Form(
                          key: formMAdGuarantorCompanyChangeNotif.key,
                          onWillPop: _onWillPop,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DropdownButtonFormField<TypeInstitutionModel>(
                                autovalidate: formMAdGuarantorCompanyChangeNotif
                                    .autoValidate,
                                validator: (e) {
                                  if (e == null) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                value: formMAdGuarantorCompanyChangeNotif
                                    .typeInstitutionSelected,
                                onChanged: (value) {
                                  formMAdGuarantorCompanyChangeNotif
                                      .typeInstitutionSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Lembaga",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: formMAdGuarantorCompanyChangeNotif
                                    .listTypeInstitution
                                    .map((value) {
                                  return DropdownMenuItem<TypeInstitutionModel>(
                                    value: value,
                                    child: Text(
                                      value.PARA_NAME,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height / 47),
                              // DropdownButtonFormField<ProfilModel>(
                              //   autovalidate: formMAdGuarantorCompanyChangeNotif
                              //       .autoValidate,
                              //   validator: (e) {
                              //     if (e == null) {
                              //       return "Tidak boleh kosong";
                              //     } else {
                              //       return null;
                              //     }
                              //   },
                              //   value: formMAdGuarantorCompanyChangeNotif
                              //       .profilSelected,
                              //   onChanged: (value) {
                              //     formMAdGuarantorCompanyChangeNotif
                              //         .profilSelected = value;
                              //   },
                              //   onTap: () {
                              //     FocusManager.instance.primaryFocus.unfocus();
                              //   },
                              //   decoration: InputDecoration(
                              //     labelText: "Profil",
                              //     border: OutlineInputBorder(),
                              //     contentPadding:
                              //         EdgeInsets.symmetric(horizontal: 10),
                              //   ),
                              //   items: formMAdGuarantorCompanyChangeNotif
                              //       .listProfil
                              //       .map((value) {
                              //     return DropdownMenuItem<ProfilModel>(
                              //       value: value,
                              //       child: Text(
                              //         value.text,
                              //         overflow: TextOverflow.ellipsis,
                              //       ),
                              //     );
                              //   }).toList(),
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              TextFormField(
                                autovalidate: formMAdGuarantorCompanyChangeNotif
                                    .autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMAdGuarantorCompanyChangeNotif
                                    .controllerInstitutionName,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Nama Lembaga',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                keyboardType: TextInputType.text,
                                textCapitalization:
                                    TextCapitalization.characters,
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 47),
                              TextFormField(
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  style: new TextStyle(color: Colors.black),
                                  controller: formMAdGuarantorCompanyChangeNotif.controllerEstablishDate,
                                  autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                  readOnly: true,
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                    formMAdGuarantorCompanyChangeNotif.selectEstablishDate(context);
                                  },
                                  decoration: new InputDecoration(
                                      labelText: 'Tanggal Pendirian',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))
                                  )
                              ),
                              SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height / 47),
                              TextFormField(
                                autovalidate: formMAdGuarantorCompanyChangeNotif
                                    .autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMAdGuarantorCompanyChangeNotif
                                    .controllerNPWP,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'NPWP',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  // LengthLimitingTextInputFormatter(10),
                                ],
                                keyboardType: TextInputType.number,
                              ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // TextFormField(
                              //   autovalidate: formMAdGuarantorCompanyChangeNotif
                              //       .autoValidate,
                              //   validator: (e) {
                              //     if (e.isEmpty) {
                              //       return "Tidak boleh kosong";
                              //     } else {
                              //       return null;
                              //     }
                              //   },
                              //   onTap: () {
                              //     Navigator.push(
                              //         context,
                              //         MaterialPageRoute(
                              //             builder: (context) =>
                              //                 ListGuarantorAddressCompany()));
                              //   },
                              //   controller: formMAdGuarantorCompanyChangeNotif
                              //       .controllerAddress,
                              //   style: TextStyle(color: Colors.black),
                              //   decoration: new InputDecoration(
                              //       labelText: 'Alamat',
                              //       labelStyle: TextStyle(color: Colors.black),
                              //       border: OutlineInputBorder(
                              //           borderRadius:
                              //               BorderRadius.circular(8))),
                              //   readOnly: true,
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // TextFormField(
                              //   readOnly: true,
                              //   autovalidate: formMAdGuarantorCompanyChangeNotif
                              //       .autoValidate,
                              //   validator: (e) {
                              //     if (e.isEmpty) {
                              //       return "Tidak boleh kosong";
                              //     } else {
                              //       return null;
                              //     }
                              //   },
                              //   controller: formMAdGuarantorCompanyChangeNotif
                              //       .controllerAddressType,
                              //   style: new TextStyle(color: Colors.black),
                              //   decoration: new InputDecoration(
                              //       filled: true,
                              //       fillColor: Colors.black12,
                              //       labelText: 'Jenis Alamat',
                              //       labelStyle: TextStyle(color: Colors.black),
                              //       border: OutlineInputBorder(
                              //           borderRadius:
                              //               BorderRadius.circular(8))),
                              //   enabled: false,
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // Row(
                              //   children: [
                              //     Expanded(
                              //       flex: 5,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerRT,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'RT',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //     SizedBox(width: 8),
                              //     Expanded(
                              //         flex: 5,
                              //         child: TextFormField(
                              //           readOnly: true,
                              //           autovalidate:
                              //               formMAdGuarantorCompanyChangeNotif
                              //                   .autoValidate,
                              //           validator: (e) {
                              //             if (e.isEmpty) {
                              //               return "Tidak boleh kosong";
                              //             } else {
                              //               return null;
                              //             }
                              //           },
                              //           controller:
                              //               formMAdGuarantorCompanyChangeNotif
                              //                   .controllerRW,
                              //           style:
                              //               new TextStyle(color: Colors.black),
                              //           decoration: new InputDecoration(
                              //               filled: true,
                              //               fillColor: Colors.black12,
                              //               labelText: 'RW',
                              //               labelStyle:
                              //                   TextStyle(color: Colors.black),
                              //               border: OutlineInputBorder(
                              //                   borderRadius:
                              //                       BorderRadius.circular(8))),
                              //           enabled: false,
                              //         ))
                              //   ],
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // TextFormField(
                              //   readOnly: true,
                              //   autovalidate: formMAdGuarantorCompanyChangeNotif
                              //       .autoValidate,
                              //   validator: (e) {
                              //     if (e.isEmpty) {
                              //       return "Tidak boleh kosong";
                              //     } else {
                              //       return null;
                              //     }
                              //   },
                              //   controller: formMAdGuarantorCompanyChangeNotif
                              //       .controllerKelurahan,
                              //   style: new TextStyle(color: Colors.black),
                              //   decoration: new InputDecoration(
                              //       filled: true,
                              //       fillColor: Colors.black12,
                              //       labelText: 'Kelurahan',
                              //       labelStyle: TextStyle(color: Colors.black),
                              //       border: OutlineInputBorder(
                              //           borderRadius:
                              //               BorderRadius.circular(8))),
                              //   enabled: false,
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // TextFormField(
                              //   readOnly: true,
                              //   autovalidate: formMAdGuarantorCompanyChangeNotif
                              //       .autoValidate,
                              //   validator: (e) {
                              //     if (e.isEmpty) {
                              //       return "Tidak boleh kosong";
                              //     } else {
                              //       return null;
                              //     }
                              //   },
                              //   controller: formMAdGuarantorCompanyChangeNotif
                              //       .controllerKecamatan,
                              //   style: new TextStyle(color: Colors.black),
                              //   decoration: new InputDecoration(
                              //       filled: true,
                              //       fillColor: Colors.black12,
                              //       labelText: 'Kecamatan',
                              //       labelStyle: TextStyle(color: Colors.black),
                              //       border: OutlineInputBorder(
                              //           borderRadius:
                              //               BorderRadius.circular(8))),
                              //   enabled: false,
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // TextFormField(
                              //   readOnly: true,
                              //   autovalidate: formMAdGuarantorCompanyChangeNotif
                              //       .autoValidate,
                              //   validator: (e) {
                              //     if (e.isEmpty) {
                              //       return "Tidak boleh kosong";
                              //     } else {
                              //       return null;
                              //     }
                              //   },
                              //   controller: formMAdGuarantorCompanyChangeNotif
                              //       .controllerKota,
                              //   style: TextStyle(color: Colors.black),
                              //   decoration: InputDecoration(
                              //       filled: true,
                              //       fillColor: Colors.black12,
                              //       labelText: 'Kabupaten/Kota',
                              //       labelStyle: TextStyle(color: Colors.black),
                              //       border: OutlineInputBorder(
                              //           borderRadius:
                              //               BorderRadius.circular(8))),
                              //   enabled: false,
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // Row(
                              //   children: [
                              //     Expanded(
                              //       flex: 7,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerProvinsi,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'Provinsi',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //     SizedBox(width: 8),
                              //     Expanded(
                              //       flex: 3,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerPostalCode,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'Kode Pos',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //   ],
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // Row(
                              //   children: [
                              //     Expanded(
                              //       flex: 4,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         keyboardType: TextInputType.number,
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerTelephoneArea1,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'Telepon 1 (Area)',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //     SizedBox(
                              //         width: MediaQuery.of(context).size.width /
                              //             37),
                              //     Expanded(
                              //       flex: 6,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         keyboardType: TextInputType.number,
                              //         inputFormatters: [
                              //           WhitelistingTextInputFormatter
                              //               .digitsOnly,
                              //           LengthLimitingTextInputFormatter(10),
                              //         ],
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerTelephone1,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'Telepon 1',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //   ],
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // Row(
                              //   children: [
                              //     Expanded(
                              //       flex: 4,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         keyboardType: TextInputType.number,
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerTelephoneArea2,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'Telepon 2 (Area)',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //     SizedBox(
                              //         width: MediaQuery.of(context).size.width /
                              //             37),
                              //     Expanded(
                              //       flex: 6,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         keyboardType: TextInputType.number,
                              //         inputFormatters: [
                              //           WhitelistingTextInputFormatter
                              //               .digitsOnly,
                              //           LengthLimitingTextInputFormatter(10),
                              //         ],
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerTelephone2,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'Telepon 2',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //   ],
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              // Row(
                              //   children: [
                              //     Expanded(
                              //       flex: 4,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         keyboardType: TextInputType.number,
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerFaxArea,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'Fax (Area)',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //     SizedBox(
                              //         width: MediaQuery.of(context).size.width /
                              //             37),
                              //     Expanded(
                              //       flex: 6,
                              //       child: TextFormField(
                              //         readOnly: true,
                              //         autovalidate:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .autoValidate,
                              //         validator: (e) {
                              //           if (e.isEmpty) {
                              //             return "Tidak boleh kosong";
                              //           } else {
                              //             return null;
                              //           }
                              //         },
                              //         keyboardType: TextInputType.number,
                              //         inputFormatters: [
                              //           WhitelistingTextInputFormatter
                              //               .digitsOnly,
                              //           LengthLimitingTextInputFormatter(10),
                              //         ],
                              //         controller:
                              //             formMAdGuarantorCompanyChangeNotif
                              //                 .controllerFax,
                              //         style: new TextStyle(color: Colors.black),
                              //         decoration: new InputDecoration(
                              //             filled: true,
                              //             fillColor: Colors.black12,
                              //             labelText: 'Fax',
                              //             labelStyle:
                              //                 TextStyle(color: Colors.black),
                              //             border: OutlineInputBorder(
                              //                 borderRadius:
                              //                     BorderRadius.circular(8))),
                              //         enabled: false,
                              //       ),
                              //     ),
                              //   ],
                              // ),
                            ],
                          ),
                        );
                      },
                    );
                  }),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0)),
                  color: myPrimaryColor,
                  onPressed: () {
                    if (widget.flag == 0) {
                      Provider.of<FormMAddGuarantorCompanyChangeNotifier>(
                              context,
                              listen: false)
                          .check(context, widget.flag, null);
                    } else {
                     Provider.of<FormMAddGuarantorCompanyChangeNotifier>(
                         context,
                         listen: false)
                         .check(context, widget.flag, widget.index);
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25))
                    ],
                  ))),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: false);
    if (widget.flag == 0) {
      if (_provider.typeInstitutionSelected != null ||
          _provider.listGuarantorAddress.isNotEmpty ||
          _provider.controllerEstablishDate.text != "" ||
          // _provider.profilSelected != null ||
          _provider.controllerInstitutionName.text != "" ||
          _provider.controllerNPWP.text != "") {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      // Navigator.pop(context, true);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () {
                      _provider.clearData();
                      _provider.autoValidate = false;
                      Navigator.of(context).pop(true);
                    },
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
    else {
      if (_provider.typeInstitutionSelected.PARA_ID != _provider.typeInstitutionTemp.PARA_ID ||
          // _provider.profilSelected.id != _provider.profilTemp.id ||
          _provider.controllerInstitutionName.text != _provider.institutionNameTemp ||
          _provider.npwpTemp != _provider.controllerNPWP.text ||
          _provider.controllerEstablishDate.text != widget.model.establishDate
          // _provider.addressTemp != _provider.controllerAddress.text ||
          // _provider.addressTypeTemp != _provider.controllerAddressType.text ||
          // _provider.rtTemp != _provider.controllerRT.text ||
          // _provider.rwTemp != _provider.controllerRW.text ||
          // _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
          // _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
          // _provider.kotaTemp != _provider.controllerKota.text ||
          // _provider.provTemp != _provider.controllerProvinsi.text ||
          // _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
          // _provider.phoneArea1Temp != _provider.controllerTelephoneArea1.text ||
          // _provider.phone1Temp != _provider.controllerTelephone1.text ||
          // _provider.phoneArea2Temp != _provider.controllerTelephoneArea2.text ||
          // _provider.phone2Temp != _provider.controllerTelephone2.text ||
          // _provider.faxAreaTemp != _provider.controllerFaxArea.text ||
          // _provider.faxTemp != _provider.controllerFax.text
      ) {
          return (await showDialog(
            context: context,
            builder: (myContext) => AlertDialog(
              title: new Text('Warning'),
              content: new Text('Simpan perubahan?'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    _provider.check(context, widget.flag, widget.index);
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                ),
                FlatButton(
                  onPressed: () {
                    _provider.clearData();
                    Navigator.of(context).pop(true);
                  },
                  child: new Text('Tidak'),
                ),
              ],
            ),
          )) ?? false;
      } else {
        return (await showDialog(
          context: context,
          builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text('Keluar dari edit data?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  _provider.clearData();
                  Navigator.pop(context, true);
                },
                child: new Text('Ya'),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Tidak'),
              ),
            ],
          ),
        )) ?? false;
      }
    }
  }
}
