import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/list_alamat_korespondensi.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMInformasiAlamat extends StatefulWidget {
  @override
  _FormMInformasiAlamatState createState() => _FormMInformasiAlamatState();
}

class _FormMInformasiAlamatState extends State<FormMInformasiAlamat> {

  @override
  void initState() {
    super.initState();
    Provider.of<FormMInfoAlamatChangeNotif>(context,listen: false).setDataFromSQLite("5");
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FormMInfoAlamatChangeNotif>(
      builder: (context, formMAlamatChangeNotif, _) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: myPrimaryColor,
              fontFamily: "NunitoSans",
              primarySwatch: Colors.yellow),
          child: Scaffold(
            appBar: AppBar(
              title:
              Text("Informasi Alamat", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              onWillPop: formMAlamatChangeNotif.onBackPress,
              key: formMAlamatChangeNotif.keyForm,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  TextFormField(
                    autovalidate: formMAlamatChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListAlamatKorespondensi()));
                    },
                    controller: formMAlamatChangeNotif.controllerInfoAlamat,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Alamat',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    readOnly: true,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    autovalidate: formMAlamatChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: formMAlamatChangeNotif.controllerAddressType,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        filled: true,
                        fillColor: Colors.black12,
                        labelText: 'Jenis Alamat',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    enabled: false,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: TextFormField(
                          autovalidate: formMAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMAlamatChangeNotif.controllerRT,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'RT',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                          flex: 5,
                          child: TextFormField(
                            autovalidate: formMAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMAlamatChangeNotif.controllerRW,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'RW',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ))
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  FocusScope(
                    node: FocusScopeNode(),
                    child: TextFormField(
                      autovalidate: formMAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMAlamatChangeNotif.controllerKelurahan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kelurahan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    autovalidate: formMAlamatChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: formMAlamatChangeNotif.controllerKecamatan,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        filled: true,
                        fillColor: Colors.black12,
                        labelText: 'Kecamatan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    enabled: false,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    autovalidate: formMAlamatChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: formMAlamatChangeNotif.controllerKota,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.black12,
                        labelText: 'Kabupaten/Kota',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    enabled: false,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Row(
                    children: [
                      Expanded(
                        flex: 7,
                        child: TextFormField(
                          autovalidate: formMAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMAlamatChangeNotif.controllerProvinsi,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Provinsi',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        flex: 3,
                        child: TextFormField(
                          autovalidate: formMAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller:
                          formMAlamatChangeNotif.controllerPostalCode,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Kode Pos',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Row(
                    children: [
                      Expanded(
                      flex: 4,
                        child: TextFormField(
//                          autovalidate: formMAlamatChangeNotif.autoValidate,
//                          validator: (e) {
//                            if (e.isEmpty) {
//                              return "Tidak boleh kosong";
//                            } else {
//                              return null;
//                            }
//                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(10),
                          ],
                          controller: formMAlamatChangeNotif.controllerTeleponArea,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Telepon (Area)',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width / 37),
                      Expanded(
                        flex: 6,
                        child: TextFormField(
//                          autovalidate: formMAlamatChangeNotif.autoValidate,
//                          validator: (e) {
//                            if (e.isEmpty) {
//                              return "Tidak boleh kosong";
//                            } else {
//                              return null;
//                            }
//                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(10),
                          ],
                          controller: formMAlamatChangeNotif.controllerTelepon,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Telepon',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMInfoAlamatChangeNotif>(
                    builder: (context, formMAlamatChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMAlamatChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          ),
        );
      },
    );
  }
}
