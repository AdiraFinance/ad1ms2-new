import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/form_m_credit_limit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_cl_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchReferenceNumberCL extends StatefulWidget {
  @override
  _SearchReferenceNumberCLState createState() => _SearchReferenceNumberCLState();
}

class _SearchReferenceNumberCLState extends State<SearchReferenceNumberCL> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Provider.of<SearchReferenceNumberCLChangeNotifier>(context,listen: false).scaffoldKey,
      appBar: AppBar(
        title: Consumer<SearchReferenceNumberCLChangeNotifier>(
          builder: (context, searchReferenceNumberCLChangeNotifier, _) {
            return TextFormField(
              controller: searchReferenceNumberCLChangeNotifier.controllerSearch,
              style: TextStyle(color: Colors.black),
              textInputAction: TextInputAction.search,
              onFieldSubmitted: (e) {
                searchReferenceNumberCLChangeNotifier.getToken(context, e);
              },
              onChanged: (e) {
                searchReferenceNumberCLChangeNotifier.changeAction(e);
              },
              cursorColor: Colors.black,
              decoration: new InputDecoration(
                hintText: "Cari Reference Number (minimal 3 karakter)",
                hintStyle: TextStyle(color: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
              ),
              textCapitalization: TextCapitalization.characters,
              autofocus: true,
            );
          },
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Provider.of<SearchReferenceNumberChangeNotifier>(context, listen: true)
              .showClear
              ? IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                Provider.of<SearchReferenceNumberCLChangeNotifier>(context, listen: false).controllerSearch.clear();
                Provider.of<SearchReferenceNumberCLChangeNotifier>(context, listen: false)
                    .changeAction(Provider.of<SearchReferenceNumberCLChangeNotifier>(context, listen: false)
                    .controllerSearch.text);
              })
              : SizedBox(
            width: 0.0,
            height: 0.0,
          )
        ],
      ),
      body: Consumer<SearchReferenceNumberCLChangeNotifier>(
        builder: (context, searchReferenceNumberCLChangeNotifier, _) {
          if (searchReferenceNumberCLChangeNotifier.loadData == true) {
            return Center(
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(primaryOrange),
              ),
            );
          }
          else{
            return Provider.of<FormMCreditLimitChangeNotifier>(context, listen: false).typeOfferSelected.KODE == "01"
            ? ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchReferenceNumberCLChangeNotifier.listReferenceNumberCL.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchReferenceNumberCLChangeNotifier.listReferenceNumberCL[index]);
                  },
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${searchReferenceNumberCLChangeNotifier.listReferenceNumberCL[index].reff_id}",
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          "${searchReferenceNumberCLChangeNotifier.listReferenceNumberCL[index].source_reff_id}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
            : ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchReferenceNumberCLChangeNotifier.listCheckMplActivation.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchReferenceNumberCLChangeNotifier.listCheckMplActivation[index]);
                  },
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${searchReferenceNumberCLChangeNotifier.listCheckMplActivation[index].reff_id}",
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          "${searchReferenceNumberCLChangeNotifier.listCheckMplActivation[index].source_reff_id}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          }
        },
      ),
    );
  }
}
