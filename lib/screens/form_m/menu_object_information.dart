import 'package:ad1ms2_dev/screens/app/information_collateral.dart';
import 'package:ad1ms2_dev/screens/app/information_object_unit.dart';
import 'package:ad1ms2_dev/screens/app/information_salesman.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class MenuObjectInformation extends StatefulWidget {
    final String flag;

    const MenuObjectInformation({this.flag});

  @override
  _MenuObjectInformationState createState() => _MenuObjectInformationState();
}

class _MenuObjectInformationState extends State<MenuObjectInformation> {
    @override
    Widget build(BuildContext context) {
        var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: true);
        var _providerSalesman = Provider.of<InformationSalesmanChangeNotifier>(context, listen: true);
        var _providerCollateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => InformationObjectUnit(flag: widget.flag)
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Unit Detail/Objek"),
                                        _providerObjectUnit.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerObjectUnit.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            _providerSalesman.deleteSQLite();
                            // _providerObjectUnit.flag ?
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => InformationSalesman()));
                            // : null;
                        },
                        child: Card(
                            color: _providerObjectUnit.flag ?
                            Colors.white : Colors.black12,
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Sales"),
                                        _providerSalesman.listInfoSales.length != 0
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerSalesman.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            _providerSalesman.listInfoSales.length != 0 ?
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => InformationCollateral(flag: widget.flag)))
                            : null;
                        },
                        child: Card(
                            color: _providerSalesman.listInfoSales.length != 0 ?
                            Colors.white : Colors.black12,
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Jaminan/Colla"),
                                        _providerCollateral.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerCollateral.autoValidateAuto || _providerCollateral.autoValidateProp
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }
}
