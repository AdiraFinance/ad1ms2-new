import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class FormMInformasiKeluargaIBU extends StatefulWidget {

  @override
  _FormMInformasiKeluargaIBUState createState() => _FormMInformasiKeluargaIBUState();
}

class _FormMInformasiKeluargaIBUState extends State<FormMInformasiKeluargaIBU> {

  @override
  void initState() {
    super.initState();
    Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context,listen: false).setDataSQLite();
  }
  @override
  Widget build(BuildContext context) {
    var _size = MediaQuery.of(context).size;
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: Consumer<FormMInformasiKeluargaIBUChangeNotif>(
        builder: (context, formMInfoKelIBUchangeNotif, _) {
          formMInfoKelIBUchangeNotif.setDefaultValue();
          return Scaffold(
            appBar: AppBar(
              title:
              Text("Informasi Keluarga (Ibu)", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              onWillPop: formMInfoKelIBUchangeNotif.onBackPress,
              key: formMInfoKelIBUchangeNotif.keyForm,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  DropdownButtonFormField<RelationshipStatusModel>(
                      autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                      validator: (e) {
                        if (e == null) {
                          return "Silahkan pilih status hubungan";
                        } else {
                          return null;
                        }
                      },
                      disabledHint: Text(formMInfoKelIBUchangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_NAME),
                      value: formMInfoKelIBUchangeNotif.relationshipStatusSelected,
                      onChanged: null,
                      //     (value) {
                      //   // formMInfoKelIBUchangeNotif.relationshipStatusSelected = value;
                      // },
                      onTap: () {
                        FocusManager.instance.primaryFocus.unfocus();
                      },
                      decoration: InputDecoration(
                        labelText: "Status hubungan",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: formMInfoKelIBUchangeNotif.listRelationShipStatus
                          .map((value) {
                        return DropdownMenuItem<RelationshipStatusModel>(
                          value: value,
                          child: Text(
                            value.PARA_FAMILY_TYPE_NAME,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                  SizedBox(height: _size.height / 47),
////                  DropdownButtonFormField<IdentityModel>(
////                    value: formMInfoKelIBUchangeNotif.identitySelected,
////                    autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
////                    validator: (e) {
////                      if (e == null) {
////                        return "Tidak boleh kosong";
////                      } else {
////                        return null;
////                      }
////                    },
////                    decoration: InputDecoration(
////                      labelText: "Jenis Identitas",
////                      border: OutlineInputBorder(),
////                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
////                    ),
////                    items: formMInfoKelIBUchangeNotif.listIdentityType.map((data) {
////                      return DropdownMenuItem<IdentityModel>(
////                        value: data,
////                        child: Text(
////                          data.name,
////                          overflow: TextOverflow.ellipsis,
////                        ),
////                      );
////                    }).toList(),
////                    onChanged: (newVal) {
////                      formMInfoKelIBUchangeNotif.identitySelected = newVal;
////                    },
////                    onTap: () {
////                      FocusManager.instance.primaryFocus.unfocus();
////                    },
////                  ),
////                  SizedBox(height: _size.height / 47),
////                  TextFormField(
////                    controller: formMInfoKelIBUchangeNotif.controllerNoIdentitas,
////                    autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
////                    validator: (e) {
////                      if (e.isEmpty) {
////                        return "Tidak boleh kosong";
////                      } else {
////                        return null;
////                      }
////                    },
////                    style: new TextStyle(color: Colors.black),
////                    decoration: new InputDecoration(
////                        labelText: 'Nomor Identitas',
////                        labelStyle: TextStyle(color: Colors.black),
////                        border: OutlineInputBorder(
////                            borderRadius: BorderRadius.circular(8))),
////                      keyboardType: formMInfoKelIBUchangeNotif.identitySelected != null
////                        ? formMInfoKelIBUchangeNotif.identitySelected.id != "03"
////                          ? TextInputType.number
////                          : TextInputType.text
////                        : TextInputType.number,
////                      textCapitalization: TextCapitalization.characters,
////                      inputFormatters: formMInfoKelIBUchangeNotif.identitySelected != null
////                        ? formMInfoKelIBUchangeNotif.identitySelected.id != "03"
////                          ? [WhitelistingTextInputFormatter.digitsOnly]
////                          : null
////                        : [WhitelistingTextInputFormatter.digitsOnly]
////                  ),
//                  SizedBox(height: _size.height / 47),
                  TextFormField(
                    controller:
                        formMInfoKelIBUchangeNotif.controllerNamaLengkapdentitas,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Nama Lengkap Sesuai Identitas',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                  ),
                  SizedBox(height: _size.height / 47),
                  TextFormField(
                    enabled: formMInfoKelIBUchangeNotif.isEnableFieldFullName,
                    controller: formMInfoKelIBUchangeNotif.controllerNamaIdentitas,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Nama Lengkap',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                  ),
                  SizedBox(height: _size.height / 47),
//                  FocusScope(
//                      node: FocusScopeNode(),
//                      child: TextFormField(
//                        onTap: () {
//                            FocusManager.instance.primaryFocus.unfocus();
//                          formMInfoKelIBUchangeNotif.selectBirthDate(context);
//                        },
//                        controller: formMInfoKelIBUchangeNotif.controllerTglLahir,
//                        style: new TextStyle(color: Colors.black),
//                        decoration: new InputDecoration(
//                            labelText: 'Tanggal Lahir',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8))),
//                        autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//
//                      )),
//                  SizedBox(height: _size.height / 47),
//                  TextFormField(
//                    controller: formMInfoKelIBUchangeNotif
//                        .controllerTempatLahirSesuaiIdentitas,
//                    style: new TextStyle(color: Colors.black),
//                    decoration: new InputDecoration(
//                        labelText: 'Tempat Lahir Sesuai Identitas',
//                        labelStyle: TextStyle(color: Colors.black),
//                        border: OutlineInputBorder(
//                            borderRadius: BorderRadius.circular(8))),
//                    keyboardType: TextInputType.text,
//                    textCapitalization: TextCapitalization.characters,
//                    autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
//                    validator: (e) {
//                      if (e.isEmpty) {
//                        return "Tidak boleh kosong";
//                      } else {
//                        return null;
//                      }
//                    },
//                  ),
//                  SizedBox(height: _size.height / 47),
//                  TextFormField(
//                    controller: formMInfoKelIBUchangeNotif
//                        .controllerTempatLahirSesuaiIdentitasLOV,
//                    style: new TextStyle(color: Colors.black),
//                    decoration: new InputDecoration(
//                        labelText: 'Tempat Lahir Sesuai Identitas',
//                        labelStyle: TextStyle(color: Colors.black),
//                        border: OutlineInputBorder(
//                            borderRadius: BorderRadius.circular(8))),
//                    keyboardType: TextInputType.text,
//                    textCapitalization: TextCapitalization.characters,
//                    autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
//                    validator: (e) {
//                      if (e.isEmpty) {
//                        return "Tidak boleh kosong";
//                      } else {
//                        return null;
//                      }
//                    },
//                    readOnly: true,
//                    onTap: (){
//                      formMInfoKelIBUchangeNotif.searchBirthPlace(context);
//                    },
//                  ),
//                  SizedBox(height: _size.height / 47),
//                  Text(
//                    "Jenis Kelamin",
//                    style: TextStyle(
//                        fontSize: 16,
//                        fontWeight: FontWeight.w700,
//                        letterSpacing: 0.15),
//                  ),
//                  Row(
//                    children: [
//                      Row(
//                        children: [
//                          Radio(
//                              activeColor: primaryOrange,
//                              value: "01",
//                              groupValue: formMInfoKelIBUchangeNotif.radioValueGender,
//                              onChanged: (value) {
//                                // formMInfoKelIBUchangeNotif.radioValueGender = value;
//                              }),
//                          Text("Laki Laki")
//                        ],
//                      ),
//                      Row(
//                        children: [
//                          Radio(
//                              activeColor: primaryOrange,
//                              value: "02",
//                              groupValue: formMInfoKelIBUchangeNotif.radioValueGender,
//                              onChanged: (value) {
//                                formMInfoKelIBUchangeNotif.radioValueGender = value;
//                              }),
//                          Text("Perempuan")
//                        ],
//                      ),
//                    ],
//                  ),
//                  SizedBox(height: _size.height / 47),
//                  Row(
//                    children: [
//                      Expanded(
//                        flex: 4,
//                        child: TextFormField(
//                          autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
//                          validator: (e) {
//                            if (e.isEmpty) {
//                              return "Tidak boleh kosong";
//                            } else {
//                              return null;
//                            }
//                          },
//                          onChanged: (e) {
//                            formMInfoKelIBUchangeNotif.checkValidCodeArea(e);
//                          },
//                          keyboardType: TextInputType.number,
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter.digitsOnly,
//                            LengthLimitingTextInputFormatter(10),
//                          ],
//                          controller: formMInfoKelIBUchangeNotif.controllerKodeArea,
//                          style: new TextStyle(color: Colors.black),
//                          decoration: new InputDecoration(
//                              labelText: 'Kode Area',
//                              labelStyle: TextStyle(color: Colors.black),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8))),
//                        ),
//                      ),
//                      SizedBox(width: MediaQuery.of(context).size.width / 37),
//                      Expanded(
//                        flex: 6,
//                        child: TextFormField(
//                          autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
//                          validator: (e) {
//                            if (e.isEmpty) {
//                              return "Tidak boleh kosong";
//                            } else {
//                              return null;
//                            }
//                          },
//                          keyboardType: TextInputType.number,
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter.digitsOnly,
//                            LengthLimitingTextInputFormatter(10),
//                          ],
//                          controller: formMInfoKelIBUchangeNotif.controllerTlpn,
//                          style: new TextStyle(color: Colors.black),
//                          decoration: new InputDecoration(
//                              labelText: 'Telepon',
//                              labelStyle: TextStyle(color: Colors.black),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8))),
//                        ),
//                      ),
//                    ],
//                  ),
//                  SizedBox(height: _size.height / 47),
//                  TextFormField(
//                    controller: formMInfoKelIBUchangeNotif.controllerNoHp,
//                    style: new TextStyle(color: Colors.black),
//                    decoration: new InputDecoration(
//                        labelText: 'No Handphone',
//                        labelStyle: TextStyle(color: Colors.black),
//                        prefixText: "08",
//                        border: OutlineInputBorder(
//                            borderRadius: BorderRadius.circular(8))),
//                    keyboardType: TextInputType.number,
//                    autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
//                    onChanged: (e) {
//                      formMInfoKelIBUchangeNotif.checkValidNotZero(e);
//                    },
//                    validator: (e) {
//                      if (e.isEmpty) {
//                        return "Tidak boleh kosong";
//                      } else {
//                        return null;
//                      }
//                    },
//                    inputFormatters: [
//                      WhitelistingTextInputFormatter.digitsOnly,
//                      LengthLimitingTextInputFormatter(12)
//                    ],
//                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMInformasiKeluargaIBUChangeNotif>(
                    builder: (context, formMInfoKelIBUchangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMInfoKelIBUchangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          );
        },
      ),
    );
  }
}
