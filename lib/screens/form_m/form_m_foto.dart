import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/screens/form_m/list_document_object_unit.dart';
import 'package:ad1ms2_dev/screens/form_m/list_group_unit_object.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/document_unit_model.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/shared/group_unit_object_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:provider/provider.dart';

class FormMFoto extends StatefulWidget {
  @override
  _FormMFotoState createState() => _FormMFotoState();
}

class _FormMFotoState extends State<FormMFoto> {
  @override
  void initState() {
    super.initState();
    if(Provider.of<FormMFotoChangeNotifier>(context, listen: false).listOccupation.isEmpty)
    Provider.of<FormMFotoChangeNotifier>(context, listen: false).getListOccupation(context);
    // Provider.of<FormMFotoChangeNotifier>(context, listen: false).setDataFromSQLite();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
        ),
        child: Consumer<FormMFotoChangeNotifier>(
          builder: (context, formMFotoChangeNotif, _) {
            formMFotoChangeNotif.setDefaultValue();
            return
              Scaffold(
                key: formMFotoChangeNotif.scaffoldKey,
                body: formMFotoChangeNotif.loadData
                    ?
                Center(child: CircularProgressIndicator())
                    :
                ListView(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height/57,
                      horizontal: MediaQuery.of(context).size.width/37
                  ),
                  children: [
                    Card(
                      elevation: 3.3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height / 57,
                                left: MediaQuery.of(context).size.width / 37,
                                right: MediaQuery.of(context).size.width / 37),
                            child: Text("Foto Tempat Tinggal"),
                          ),
                          formMFotoChangeNotif.listFotoTempatTinggal.isEmpty
                              ?
                          Padding(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height / 57,
                                left: MediaQuery.of(context).size.width / 37,
                                right: MediaQuery.of(context).size.width / 37,
                                bottom:
                                MediaQuery.of(context).size.height / 37),
                            child: RaisedButton(
                              onPressed: () {
                                formMFotoChangeNotif.addFile(0);
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                  new BorderRadius.circular(8.0)),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.camera_alt),
                                  SizedBox(
                                      width:
                                      MediaQuery.of(context).size.width /
                                          47),
                                  Text("ADD")
                                ],
                              ),
                              color: myPrimaryColor,
                            ),
                          )
                              : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    children: _listWidgetImageTempatTinggal(
                                        formMFotoChangeNotif
                                            .listFotoTempatTinggal,
                                        context),
                                  ),
                                  formMFotoChangeNotif.listFotoTempatTinggal.length < 2
                                      ?
                                  IconButton(
                                      onPressed: () {
                                        formMFotoChangeNotif.addFile(0);
                                      },
                                      icon: Icon(
                                        Icons.add_a_photo,
                                        color: myPrimaryColor,
                                        size: 33,
                                      )
                                  )
                                      :
                                  SizedBox(width: 0.0, height: 0.0)
                                ],
                              ),
                            ],
                          ),
                          formMFotoChangeNotif.autoValidateFotoTempatTinggal
                              ? Padding(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width / 37,
                                right: MediaQuery.of(context).size.width / 37,
                                bottom:
                                MediaQuery.of(context).size.height / 37),
                            child: Text(
                              "Tidak boleh kosong",
                              style:
                              TextStyle(fontSize: 12, color: Colors.red),
                            ),
                          )
                              : SizedBox()
                        ],
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    DropdownButtonFormField<OccupationModel>(
                      autovalidate: formMFotoChangeNotif.autoValidate,
                      validator: (e) {
                        if (e == null) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      value: formMFotoChangeNotif.occupationSelected,
                      onChanged: (value) {
                        formMFotoChangeNotif.occupationSelected = value;
                        Provider.of<FormMOccupationChangeNotif>(context,listen: false).clearForm();
                        Provider.of<FormMIncomeChangeNotifier>(context,listen: false).clearData();
                        Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).clearDataInfoUnitObject();
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isOccupationDone = false;
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isIncomeDone = false;
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isMenuObjectInformationDone = false;
                        Provider.of<FormMOccupationChangeNotif>(context,listen: false).selectedIndex = -1;
                        Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).autoValidate = true;
                        Provider.of<InformationSalesmanChangeNotifier>(context,listen: false).autoValidate = true;
                      },
                      decoration: InputDecoration(
                        labelText: "Pekerjaan",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: formMFotoChangeNotif.listOccupation.map((value) {
                        return DropdownMenuItem<OccupationModel>(
                          value: value,
                          child: Text(
                            value.DESKRIPSI,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList(),
                    ),
                    formMFotoChangeNotif.occupationSelected != null
                        ? formMFotoChangeNotif.occupationSelected.KODE == "05" ||
                        formMFotoChangeNotif.occupationSelected.KODE ==
                            "07" ||
                        formMFotoChangeNotif.occupationSelected.KODE ==
                            "08"
                        ? Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height / 97,
                          bottom:
                          MediaQuery.of(context).size.height / 97),
                      child: Card(
                        elevation: 3.3,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                  top:
                                  MediaQuery.of(context).size.height /
                                      57,
                                  left:
                                  MediaQuery.of(context).size.width /
                                      37,
                                  right:
                                  MediaQuery.of(context).size.width /
                                      37),
                              child: Text("Foto Usaha"),
                            ),
                            formMFotoChangeNotif
                                .listFotoTempatUsaha.isEmpty
                                ? Padding(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context)
                                      .size
                                      .height /
                                      57,
                                  left: MediaQuery.of(context)
                                      .size
                                      .width /
                                      37,
                                  right: MediaQuery.of(context)
                                      .size
                                      .width /
                                      37,
                                  bottom: MediaQuery.of(context)
                                      .size
                                      .height /
                                      37),
                              child: RaisedButton(
                                onPressed: () {
                                  formMFotoChangeNotif.addFile(1);
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                    new BorderRadius.circular(
                                        8.0)),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.camera_alt),
                                    SizedBox(
                                        width:
                                        MediaQuery.of(context)
                                            .size
                                            .width /
                                            47),
                                    Text("ADD")
                                  ],
                                ),
                                color: myPrimaryColor,
                              ),
                            )
                                : Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: <Widget>[
                                    Row(
                                      children:
                                      _listWidgetImageTempatUsaha(
                                          formMFotoChangeNotif
                                              .listFotoTempatUsaha,
                                          context),
                                    ),
                                    formMFotoChangeNotif
                                        .listFotoTempatUsaha
                                        .length <
                                        2
                                        ? IconButton(
                                        onPressed: () {
                                          formMFotoChangeNotif
                                              .addFile(1);
                                        },
                                        icon: Icon(
                                          Icons.add_a_photo,
                                          color: myPrimaryColor,
                                          size: 33,
                                        ))
                                        : SizedBox(
                                        width: 0.0, height: 0.0)
                                  ],
                                ),
                              ],
                            ),
                            formMFotoChangeNotif.autoValidateTempatUsaha
                                ? Padding(
                              padding: EdgeInsets.only(
                                  left: MediaQuery.of(context)
                                      .size
                                      .width /
                                      37,
                                  right: MediaQuery.of(context)
                                      .size
                                      .width /
                                      37,
                                  bottom: MediaQuery.of(context)
                                      .size
                                      .height /
                                      37),
                              child: Text(
                                "Tidak boleh kosong",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.red),
                              ),
                            )
                                : SizedBox()
                          ],
                        ),
                      ),
                    )
                        : SizedBox(
                        height: MediaQuery.of(context).size.height / 47)
                        : SizedBox(height: MediaQuery.of(context).size.height / 47),
                    DropdownButtonFormField<FinancingTypeModel>(
                      isExpanded: true,
                      autovalidate: formMFotoChangeNotif.autoValidate,
                      validator: (e) {
                        if (e == null) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      value: formMFotoChangeNotif.typeOfFinancingModelSelected,
                      onChanged: (value) {
                        formMFotoChangeNotif.typeOfFinancingModelSelected = value;
                        formMFotoChangeNotif.getBusinessActivities(context);
                        formMFotoChangeNotif.jenisKegiatanUsahaSelected = null;
                        Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).clearDataInfoUnitObject();
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isMenuObjectInformationDone = false;
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isPhotoDone = false;
                      },
                      onTap: () {
                        FocusManager.instance.primaryFocus.unfocus();
                      },
                      decoration: InputDecoration(
                        labelText: "Jenis Pembiayaan",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: formMFotoChangeNotif.listTypeOfFinancing.map((value) {
                        return DropdownMenuItem<FinancingTypeModel>(
                          value: value,
                          child: Text(
                            value.financingTypeName,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    DropdownButtonFormField<KegiatanUsahaModel>(
                      autovalidate: formMFotoChangeNotif.autoValidate,
                      validator: (e) {
                        if (e == null) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      value: formMFotoChangeNotif.kegiatanUsahaSelected,
                      onChanged: (value) {
                        formMFotoChangeNotif.kegiatanUsahaSelected = value;
                        Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).clearDataInfoUnitObject();
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isMenuObjectInformationDone = false;
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isPhotoDone = false;
                        formMFotoChangeNotif.getListBusinessActivitiesType(context);
                      },
                      decoration: InputDecoration(
                        labelText: "Kegiatan Usaha",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: formMFotoChangeNotif.listKegiatanUsaha.map((value) {
                        return DropdownMenuItem<KegiatanUsahaModel>(
                          value: value,
                          child: Text(
                            value.text,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    DropdownButtonFormField<JenisKegiatanUsahaModel>(
                      isExpanded: true,
                      autovalidate: formMFotoChangeNotif.autoValidate,
                      validator: (e) {
                        if (e == null) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      value: formMFotoChangeNotif.jenisKegiatanUsahaSelected,
                      onChanged: (value) {
                        formMFotoChangeNotif.jenisKegiatanUsahaSelected = value;
                      },
                      decoration: InputDecoration(
                        labelText: "Jenis Kegiatan Usaha",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items:
                      formMFotoChangeNotif.listJenisKegiatanUsaha.map((value) {
                        return DropdownMenuItem<JenisKegiatanUsahaModel>(
                          value: value,
                          child: Text(
                            value.text,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    DropdownButtonFormField<JenisKonsepModel>(
                      isExpanded: true,
                      autovalidate: formMFotoChangeNotif.autoValidate,
                      validator: (e) {
                        if (e == null) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      value: formMFotoChangeNotif.jenisKonsepSelected,
                      onChanged: (value) {
                        formMFotoChangeNotif.jenisKonsepSelected = value;
                      },
                      decoration: InputDecoration(
                        labelText: "Jenis Konsep",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: formMFotoChangeNotif.listJenisKonsep.map((value) {
                        return DropdownMenuItem<JenisKonsepModel>(
                          value: value,
                          child: Text(
                            value.text,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ListGroupUnitObjectProvider()));
                      },
                      child: Card(
                        elevation: 3.3,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                  "Jumlah Group Object Unit : ${formMFotoChangeNotif.listGroupUnitObject.length}"),
                              Icon(Icons.add_circle_outline, color: primaryOrange)
                            ],
                          ),
                        ),
                      ),
                    ),
                    formMFotoChangeNotif.autoValidateGroupObjectUnit
                        ? Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 37),
                      child: Text("Tidak boleh kosong",
                          style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    // SizedBox(height: MediaQuery.of(context).size.height / 47),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ListDocumentObjectUnitProvider()));
                      },
                      child: Card(
                        elevation: 3.3,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                  "Jumlah Dokumen Object Unit : ${formMFotoChangeNotif.listDocument.length}"),
                              Icon(Icons.add_circle_outline, color: primaryOrange)
                            ],
                          ),
                        ),
                      ),
                    ),
                    formMFotoChangeNotif.autoValidateDocumentObjectUnit
                        ? Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 37),
                      child: Text("Tidak boleh kosong",
                          style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                  ],
                ),
              );
          },
        )
        // FutureBuilder(
        //   future: _getDataDropdown,
        //   builder: (context, snapshot) {
        //     if(snapshot.connectionState == ConnectionState.waiting){
        //       return Center(
        //         child: CircularProgressIndicator(),
        //       );
        //     }
        //     return Consumer<FormMFotoChangeNotifier>(
        //       builder: (context, formMFotoChangeNotif, _) {
        //         return
        //         Scaffold(
        //           key: formMFotoChangeNotif.scaffoldKey,
        //           body: formMFotoChangeNotif.loadData
        //               ?
        //           Center(child: CircularProgressIndicator())
        //               :
        //           ListView(
        //             padding: EdgeInsets.symmetric(
        //                 vertical: MediaQuery.of(context).size.height/57,
        //                 horizontal: MediaQuery.of(context).size.width/37
        //             ),
        //             children: [
        //               Card(
        //                 elevation: 3.3,
        //                 child: Column(
        //                   crossAxisAlignment: CrossAxisAlignment.start,
        //                   children: <Widget>[
        //                     Padding(
        //                       padding: EdgeInsets.only(
        //                           top: MediaQuery.of(context).size.height / 57,
        //                           left: MediaQuery.of(context).size.width / 37,
        //                           right: MediaQuery.of(context).size.width / 37),
        //                       child: Text("Foto Tempat Tinggal"),
        //                     ),
        //                     formMFotoChangeNotif.listFotoTempatTinggal.isEmpty
        //                         ?
        //                     Padding(
        //                       padding: EdgeInsets.only(
        //                           top: MediaQuery.of(context).size.height / 57,
        //                           left: MediaQuery.of(context).size.width / 37,
        //                           right: MediaQuery.of(context).size.width / 37,
        //                           bottom:
        //                           MediaQuery.of(context).size.height / 37),
        //                       child: RaisedButton(
        //                         onPressed: () {
        //                           formMFotoChangeNotif.addFile(0);
        //                         },
        //                         shape: RoundedRectangleBorder(
        //                             borderRadius:
        //                             new BorderRadius.circular(8.0)),
        //                         child: Row(
        //                           mainAxisSize: MainAxisSize.max,
        //                           mainAxisAlignment: MainAxisAlignment.center,
        //                           children: <Widget>[
        //                             Icon(Icons.camera_alt),
        //                             SizedBox(
        //                                 width:
        //                                 MediaQuery.of(context).size.width /
        //                                     47),
        //                             Text("ADD")
        //                           ],
        //                         ),
        //                         color: myPrimaryColor,
        //                       ),
        //                     )
        //                         : Column(
        //                       crossAxisAlignment: CrossAxisAlignment.start,
        //                       children: <Widget>[
        //                         Row(
        //                           mainAxisAlignment: MainAxisAlignment.center,
        //                           children: <Widget>[
        //                             Row(
        //                               children: _listWidgetImageTempatTinggal(
        //                                   formMFotoChangeNotif
        //                                       .listFotoTempatTinggal,
        //                                   context),
        //                             ),
        //                             formMFotoChangeNotif
        //                                 .listFotoTempatTinggal.length <
        //                                 2
        //                                 ? IconButton(
        //                                 onPressed: () {
        //                                   formMFotoChangeNotif.addFile(0);
        //                                 },
        //                                 icon: Icon(
        //                                   Icons.add_a_photo,
        //                                   color: myPrimaryColor,
        //                                   size: 33,
        //                                 ))
        //                                 : SizedBox(width: 0.0, height: 0.0)
        //                           ],
        //                         ),
        //                       ],
        //                     ),
        //                     formMFotoChangeNotif.autoValidateFotoTempatTinggal
        //                         ? Padding(
        //                       padding: EdgeInsets.only(
        //                           left: MediaQuery.of(context).size.width / 37,
        //                           right: MediaQuery.of(context).size.width / 37,
        //                           bottom:
        //                           MediaQuery.of(context).size.height / 37),
        //                       child: Text(
        //                         "Tidak boleh kosong",
        //                         style:
        //                         TextStyle(fontSize: 12, color: Colors.red),
        //                       ),
        //                     )
        //                         : SizedBox()
        //                   ],
        //                 ),
        //               ),
        //               SizedBox(height: MediaQuery.of(context).size.height / 47),
        //               DropdownButtonFormField<OccupationModel>(
        //                 autovalidate: formMFotoChangeNotif.autoValidate,
        //                 validator: (e) {
        //                   if (e == null) {
        //                     return "Tidak boleh kosong";
        //                   } else {
        //                     return null;
        //                   }
        //                 },
        //                 value: formMFotoChangeNotif.occupationSelected,
        //                 onChanged: (value) {
        //                   formMFotoChangeNotif.occupationSelected = value;
        //                   Provider.of<FormMOccupationChangeNotif>(context,listen: false).clearForm();
        //                   Provider.of<FormMIncomeChangeNotifier>(context,listen: false).clearData();
        //                 },
        //                 decoration: InputDecoration(
        //                   labelText: "Pekerjaan",
        //                   border: OutlineInputBorder(),
        //                   contentPadding: EdgeInsets.symmetric(horizontal: 10),
        //                 ),
        //                 items: formMFotoChangeNotif.listOccupation.map((value) {
        //                   return DropdownMenuItem<OccupationModel>(
        //                     value: value,
        //                     child: Text(
        //                       value.DESKRIPSI,
        //                       overflow: TextOverflow.ellipsis,
        //                     ),
        //                   );
        //                 }).toList(),
        //               ),
        //               formMFotoChangeNotif.occupationSelected != null
        //                   ? formMFotoChangeNotif.occupationSelected.KODE == "05" ||
        //                   formMFotoChangeNotif.occupationSelected.KODE ==
        //                       "07" ||
        //                   formMFotoChangeNotif.occupationSelected.KODE ==
        //                       "08"
        //                   ? Container(
        //                 margin: EdgeInsets.only(
        //                     top: MediaQuery.of(context).size.height / 97,
        //                     bottom:
        //                     MediaQuery.of(context).size.height / 97),
        //                 child: Card(
        //                   elevation: 3.3,
        //                   child: Column(
        //                     crossAxisAlignment: CrossAxisAlignment.start,
        //                     children: <Widget>[
        //                       Padding(
        //                         padding: EdgeInsets.only(
        //                             top:
        //                             MediaQuery.of(context).size.height /
        //                                 57,
        //                             left:
        //                             MediaQuery.of(context).size.width /
        //                                 37,
        //                             right:
        //                             MediaQuery.of(context).size.width /
        //                                 37),
        //                         child: Text("Foto Usaha"),
        //                       ),
        //                       formMFotoChangeNotif
        //                           .listFotoTempatUsaha.isEmpty
        //                           ? Padding(
        //                         padding: EdgeInsets.only(
        //                             top: MediaQuery.of(context)
        //                                 .size
        //                                 .height /
        //                                 57,
        //                             left: MediaQuery.of(context)
        //                                 .size
        //                                 .width /
        //                                 37,
        //                             right: MediaQuery.of(context)
        //                                 .size
        //                                 .width /
        //                                 37,
        //                             bottom: MediaQuery.of(context)
        //                                 .size
        //                                 .height /
        //                                 37),
        //                         child: RaisedButton(
        //                           onPressed: () {
        //                             formMFotoChangeNotif.addFile(1);
        //                           },
        //                           shape: RoundedRectangleBorder(
        //                               borderRadius:
        //                               new BorderRadius.circular(
        //                                   8.0)),
        //                           child: Row(
        //                             mainAxisSize: MainAxisSize.max,
        //                             mainAxisAlignment:
        //                             MainAxisAlignment.center,
        //                             children: <Widget>[
        //                               Icon(Icons.camera_alt),
        //                               SizedBox(
        //                                   width:
        //                                   MediaQuery.of(context)
        //                                       .size
        //                                       .width /
        //                                       47),
        //                               Text("ADD")
        //                             ],
        //                           ),
        //                           color: myPrimaryColor,
        //                         ),
        //                       )
        //                           : Column(
        //                         crossAxisAlignment:
        //                         CrossAxisAlignment.start,
        //                         children: <Widget>[
        //                           Row(
        //                             mainAxisAlignment:
        //                             MainAxisAlignment.center,
        //                             children: <Widget>[
        //                               Row(
        //                                 children:
        //                                 _listWidgetImageTempatUsaha(
        //                                     formMFotoChangeNotif
        //                                         .listFotoTempatUsaha,
        //                                     context),
        //                               ),
        //                               formMFotoChangeNotif
        //                                   .listFotoTempatUsaha
        //                                   .length <
        //                                   2
        //                                   ? IconButton(
        //                                   onPressed: () {
        //                                     formMFotoChangeNotif
        //                                         .addFile(1);
        //                                   },
        //                                   icon: Icon(
        //                                     Icons.add_a_photo,
        //                                     color: myPrimaryColor,
        //                                     size: 33,
        //                                   ))
        //                                   : SizedBox(
        //                                   width: 0.0, height: 0.0)
        //                             ],
        //                           ),
        //                         ],
        //                       ),
        //                       formMFotoChangeNotif.autoValidateTempatUsaha
        //                           ? Padding(
        //                         padding: EdgeInsets.only(
        //                             left: MediaQuery.of(context)
        //                                 .size
        //                                 .width /
        //                                 37,
        //                             right: MediaQuery.of(context)
        //                                 .size
        //                                 .width /
        //                                 37,
        //                             bottom: MediaQuery.of(context)
        //                                 .size
        //                                 .height /
        //                                 37),
        //                         child: Text(
        //                           "Tidak boleh kosong",
        //                           style: TextStyle(
        //                               fontSize: 12,
        //                               color: Colors.red),
        //                         ),
        //                       )
        //                           : SizedBox()
        //                     ],
        //                   ),
        //                 ),
        //               )
        //                   : SizedBox(
        //                   height: MediaQuery.of(context).size.height / 47)
        //                   : SizedBox(height: MediaQuery.of(context).size.height / 47),
        //               DropdownButtonFormField<FinancingTypeModel>(
        //                 isExpanded: true,
        //                 autovalidate: formMFotoChangeNotif.autoValidate,
        //                 validator: (e) {
        //                   if (e == null) {
        //                     return "Tidak boleh kosong";
        //                   } else {
        //                     return null;
        //                   }
        //                 },
        //                 value: formMFotoChangeNotif.typeOfFinancingModelSelected,
        //                 onChanged: (value) {
        //                   formMFotoChangeNotif.typeOfFinancingModelSelected = value;
        //                   formMFotoChangeNotif.getBusinessActivities();
        //                 },
        //                 onTap: () {
        //                   FocusManager.instance.primaryFocus.unfocus();
        //                 },
        //                 decoration: InputDecoration(
        //                   labelText: "Jenis Pembiayaan",
        //                   border: OutlineInputBorder(),
        //                   contentPadding: EdgeInsets.symmetric(horizontal: 10),
        //                 ),
        //                 items: formMFotoChangeNotif.listTypeOfFinancing.map((value) {
        //                   return DropdownMenuItem<FinancingTypeModel>(
        //                     value: value,
        //                     child: Text(
        //                       value.financingTypeName,
        //                       overflow: TextOverflow.ellipsis,
        //                     ),
        //                   );
        //                 }).toList(),
        //               ),
        //               SizedBox(height: MediaQuery.of(context).size.height / 47),
        //               DropdownButtonFormField<KegiatanUsahaModel>(
        //                 autovalidate: formMFotoChangeNotif.autoValidate,
        //                 validator: (e) {
        //                   if (e == null) {
        //                     return "Tidak boleh kosong";
        //                   } else {
        //                     return null;
        //                   }
        //                 },
        //                 value: formMFotoChangeNotif.kegiatanUsahaSelected,
        //                 onChanged: (value) {
        //                   formMFotoChangeNotif.kegiatanUsahaSelected = value;
        //                   formMFotoChangeNotif.getListBusinessActivitiesType();
        //                 },
        //                 decoration: InputDecoration(
        //                   labelText: "Kegiatan Usaha",
        //                   border: OutlineInputBorder(),
        //                   contentPadding: EdgeInsets.symmetric(horizontal: 10),
        //                 ),
        //                 items: formMFotoChangeNotif.listKegiatanUsaha.map((value) {
        //                   return DropdownMenuItem<KegiatanUsahaModel>(
        //                     value: value,
        //                     child: Text(
        //                       value.text,
        //                       overflow: TextOverflow.ellipsis,
        //                     ),
        //                   );
        //                 }).toList(),
        //               ),
        //               SizedBox(height: MediaQuery.of(context).size.height / 47),
        //               DropdownButtonFormField<JenisKegiatanUsahaModel>(
        //                 isExpanded: true,
        //                 autovalidate: formMFotoChangeNotif.autoValidate,
        //                 validator: (e) {
        //                   if (e == null) {
        //                     return "Tidak boleh kosong";
        //                   } else {
        //                     return null;
        //                   }
        //                 },
        //                 value: formMFotoChangeNotif.jenisKegiatanUsahaSelected,
        //                 onChanged: (value) {
        //                   formMFotoChangeNotif.jenisKegiatanUsahaSelected = value;
        //                 },
        //                 decoration: InputDecoration(
        //                   labelText: "Jenis Kegiatan Usaha",
        //                   border: OutlineInputBorder(),
        //                   contentPadding: EdgeInsets.symmetric(horizontal: 10),
        //                 ),
        //                 items:
        //                 formMFotoChangeNotif.listJenisKegiatanUsaha.map((value) {
        //                   return DropdownMenuItem<JenisKegiatanUsahaModel>(
        //                     value: value,
        //                     child: Text(
        //                       value.text,
        //                       overflow: TextOverflow.ellipsis,
        //                     ),
        //                   );
        //                 }).toList(),
        //               ),
        //               SizedBox(height: MediaQuery.of(context).size.height / 47),
        //               DropdownButtonFormField<JenisKonsepModel>(
        //                 isExpanded: true,
        //                 autovalidate: formMFotoChangeNotif.autoValidate,
        //                 validator: (e) {
        //                   if (e == null) {
        //                     return "Tidak boleh kosong";
        //                   } else {
        //                     return null;
        //                   }
        //                 },
        //                 value: formMFotoChangeNotif.jenisKonsepSelected,
        //                 onChanged: (value) {
        //                   formMFotoChangeNotif.jenisKonsepSelected = value;
        //                 },
        //                 decoration: InputDecoration(
        //                   labelText: "Jenis Konsep",
        //                   border: OutlineInputBorder(),
        //                   contentPadding: EdgeInsets.symmetric(horizontal: 10),
        //                 ),
        //                 items: formMFotoChangeNotif.listJenisKonsep.map((value) {
        //                   return DropdownMenuItem<JenisKonsepModel>(
        //                     value: value,
        //                     child: Text(
        //                       value.text,
        //                       overflow: TextOverflow.ellipsis,
        //                     ),
        //                   );
        //                 }).toList(),
        //               ),
        //               SizedBox(height: MediaQuery.of(context).size.height / 47),
        //               InkWell(
        //                 onTap: () {
        //                   Navigator.push(
        //                       context,
        //                       MaterialPageRoute(
        //                           builder: (context) =>
        //                               ListGroupUnitObjectProvider()));
        //                 },
        //                 child: Card(
        //                   elevation: 3.3,
        //                   child: Padding(
        //                     padding: const EdgeInsets.all(16.0),
        //                     child: Row(
        //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                       children: <Widget>[
        //                         Text(
        //                             "Jumlah Group Object Unit : ${formMFotoChangeNotif.listGroupUnitObject.length}"),
        //                         Icon(Icons.add_circle_outline, color: primaryOrange)
        //                       ],
        //                     ),
        //                   ),
        //                 ),
        //               ),
        //               formMFotoChangeNotif.autoValidateGroupObjectUnit
        //                   ? Container(
        //                 margin: EdgeInsets.only(
        //                     left: MediaQuery.of(context).size.width / 37),
        //                 child: Text("Tidak boleh kosong",
        //                     style: TextStyle(color: Colors.red, fontSize: 12)),
        //               )
        //                   : SizedBox(height: MediaQuery.of(context).size.height / 87),
        //               // SizedBox(height: MediaQuery.of(context).size.height / 47),
        //               InkWell(
        //                 onTap: () {
        //                   Navigator.push(
        //                       context,
        //                       MaterialPageRoute(
        //                           builder: (context) =>
        //                               ListDocumentObjectUnitProvider()));
        //                 },
        //                 child: Card(
        //                   elevation: 3.3,
        //                   child: Padding(
        //                     padding: const EdgeInsets.all(16.0),
        //                     child: Row(
        //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                       children: <Widget>[
        //                         Text(
        //                             "Jumlah Dokumen Object Unit : ${formMFotoChangeNotif.listDocument.length}"),
        //                         Icon(Icons.add_circle_outline, color: primaryOrange)
        //                       ],
        //                     ),
        //                   ),
        //                 ),
        //               ),
        //               formMFotoChangeNotif.autoValidateDocumentObjectUnit
        //                   ? Container(
        //                 margin: EdgeInsets.only(
        //                     left: MediaQuery.of(context).size.width / 37),
        //                 child: Text("Tidak boleh kosong",
        //                     style: TextStyle(color: Colors.red, fontSize: 12)),
        //               )
        //                   : SizedBox(height: MediaQuery.of(context).size.height / 87),
        //             ],
        //           ),
        //         );
        //       },
        //     );
        //   },
        // )
    );

  }

  List<Widget> _listWidgetImageTempatTinggal(List<ImageFileModel> data, BuildContext context) {
    List<Widget> _newListWidget = [];
    for (int i = 0; i < data.length; i++) {
      _newListWidget.add(Padding(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 57,
            vertical: MediaQuery.of(context).size.height / 57),
        child: PopupMenuButton<String>(
            onSelected: (value) {
              Provider.of<FormMFotoChangeNotifier>(context,listen: false).actionDeleteDetailImageResidence(value,i,context);
            },
            child: ClipRRect(
              child: Image.file(data[i].imageFile,
                  width: 150,
                  height: 150,
                  fit: BoxFit.cover,
                  filterQuality: FilterQuality.medium),
              borderRadius: BorderRadius.circular(8.0),
            ),
            itemBuilder: (context) {
              return TitlePopUpMenuButton.choices.map((String choice) {
                return PopupMenuItem(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            }),
      ));
    }
    return _newListWidget;
  }

  List<Widget> _listWidgetImageTempatUsaha(List<ImageFileModel> data, BuildContext context) {
    List<Widget> _newListWidget = [];
    for (int i = 0; i < data.length; i++) {
      _newListWidget.add(Padding(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 57,
            vertical: MediaQuery.of(context).size.height / 57),
        child: PopupMenuButton<String>(
            onSelected: (value) {
              Provider.of<FormMFotoChangeNotifier>(context,listen: false).actionDeleteDetailImageBusinessPlace(value,i,context);
              },
            child: ClipRRect(
              child: Image.file(data[i].imageFile,
                  width: 150,
                  height: 150,
                  fit: BoxFit.cover,
                  filterQuality: FilterQuality.medium),
              borderRadius: BorderRadius.circular(8.0),
            ),
            itemBuilder: (context) {
              return TitlePopUpMenuButton.choices.map((String choice) {
                return PopupMenuItem(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            }),
      ));
    }
    return _newListWidget;
  }
}
