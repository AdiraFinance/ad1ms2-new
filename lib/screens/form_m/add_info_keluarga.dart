import 'package:ad1ms2_dev/models/form_m_info_keluarga_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/add_info_keluarga_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddInfoKeluaraga extends StatefulWidget {
  final int flag;
  final FormMInfoKelModel formMInfoKelModel;
  final int index;

  const AddInfoKeluaraga({this.flag, this.formMInfoKelModel, this.index});

  @override
  _AddInfoKeluaragaState createState() => _AddInfoKeluaragaState();
}

class _AddInfoKeluaragaState extends State<AddInfoKeluaraga> {
  Future<void> _loadData;
  @override
  void initState() {
    if (widget.flag != 0) {
      print("here");
      _loadData =
          Provider.of<AddInfoKeluargaChangeNotif>(context, listen: false)
              .setValueForEditInfoKeluarga(context, widget.formMInfoKelModel,widget.flag);
    } else {
      _loadData =
          Provider.of<AddInfoKeluargaChangeNotif>(context, listen: false)
              .setValueForEditInfoKeluarga(context, widget.formMInfoKelModel,widget.flag);
//      Provider.of<AddInfoKeluargaChangeNotif>(context, listen: false).relationshipFamily(context);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _size = MediaQuery.of(context).size;
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: myPrimaryColor,
          title: Text(
              widget.flag == 0 ? "Tambah Informasi Keluarga" : "Edit Informasi Keluarga",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width / 27,
              vertical: MediaQuery.of(context).size.height / 57),
          child:
//          widget.flag == 0
//              ? Consumer<AddInfoKeluargaChangeNotif>(
//                  builder: (consumerContext, addInfoKeluargaChangeNotif, _) {
//                    return Form(
//                      key: addInfoKeluargaChangeNotif.key,
//                      onWillPop: _onWillPop,
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        children: [
//                          DropdownButtonFormField<RelationshipStatusModel>(
//                              autovalidate:
//                                  addInfoKeluargaChangeNotif.autoValidate,
//                              validator: (e) {
//                                if (e == null) {
//                                  return "Silahkan pilih status hubungan";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              value: addInfoKeluargaChangeNotif
//                                  .relationshipStatusSelected,
//                              onChanged: (value) {
//                                addInfoKeluargaChangeNotif
//                                    .relationshipStatusSelected = value;
//                              },
//                              onTap: () {
//                                FocusManager.instance.primaryFocus.unfocus();
//                              },
//                              decoration: InputDecoration(
//                                labelText: "Status Hubungan",
//                                border: OutlineInputBorder(),
//                                contentPadding:
//                                    EdgeInsets.symmetric(horizontal: 10),
//                              ),
//                              items: addInfoKeluargaChangeNotif
//                                  .listRelationShipStatus
//                                  .map((value) {
//                                return DropdownMenuItem<
//                                    RelationshipStatusModel>(
//                                  value: value,
//                                  child: Text(
//                                    value.PARA_FAMILY_TYPE_NAME,
//                                    overflow: TextOverflow.ellipsis,
//                                  ),
//                                );
//                              }).toList()),
//                          SizedBox(height: _size.height / 47),
//                          DropdownButtonFormField<IdentityModel>(
//                            value: addInfoKeluargaChangeNotif.identitySelected,
//                            autovalidate:
//                                addInfoKeluargaChangeNotif.autoValidate,
//                            validator: (e) {
//                              if (e == null) {
//                                return "Tidak boleh kosong";
//                              } else {
//                                return null;
//                              }
//                            },
//                            decoration: InputDecoration(
//                              labelText: "Jenis Identitas",
//                              border: OutlineInputBorder(),
//                              contentPadding:
//                                  EdgeInsets.symmetric(horizontal: 10),
//                            ),
//                            items: addInfoKeluargaChangeNotif.listIdentityType
//                                .map((data) {
//                              return DropdownMenuItem<IdentityModel>(
//                                value: data,
//                                child: Text(
//                                  data.name,
//                                  overflow: TextOverflow.ellipsis,
//                                ),
//                              );
//                            }).toList(),
//                            onChanged: (newVal) {
//                              addInfoKeluargaChangeNotif.identitySelected =
//                                  newVal;
//                            },
//                            onTap: () {
//                              FocusManager.instance.primaryFocus.unfocus();
//                            },
//                          ),
//                          SizedBox(height: _size.height / 47),
//                          TextFormField(
//                            controller: addInfoKeluargaChangeNotif
//                                .controllerNoIdentitas,
//                            autovalidate:
//                                addInfoKeluargaChangeNotif.autoValidate,
//                            validator: (e) {
//                              if (e.isEmpty) {
//                                return "Tidak boleh kosong";
//                              } else {
//                                return null;
//                              }
//                            },
//                            style: new TextStyle(color: Colors.black),
//                            decoration: new InputDecoration(
//                                labelText: 'Nomor Identitas',
//                                labelStyle: TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius: BorderRadius.circular(8))),
//                            keyboardType: addInfoKeluargaChangeNotif.identitySelected != null
//                                ? addInfoKeluargaChangeNotif.identitySelected.id != "03"
//                                  ? TextInputType.number
//                                  : TextInputType.text
//                                : TextInputType.number,
//                            textCapitalization: TextCapitalization.characters,
//                            inputFormatters: addInfoKeluargaChangeNotif.identitySelected != null
//                              ? addInfoKeluargaChangeNotif.identitySelected.id != "03"
//                                ? [WhitelistingTextInputFormatter.digitsOnly]
//                                : null
//                              : [WhitelistingTextInputFormatter.digitsOnly]
//                          ),
//                          SizedBox(height: _size.height / 47),
//                          TextFormField(
//                            controller: addInfoKeluargaChangeNotif
//                                .controllerNamaLengkapIdentitas,
//                            style: new TextStyle(color: Colors.black),
//                            decoration: new InputDecoration(
//                                labelText: 'Nama Lengkap Sesuai Identitas',
//                                labelStyle: TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius: BorderRadius.circular(8))),
//                            keyboardType: TextInputType.text,
//                            textCapitalization: TextCapitalization.characters,
//                            autovalidate:
//                                addInfoKeluargaChangeNotif.autoValidate,
//                            validator: (e) {
//                              if (e.isEmpty) {
//                                return "Tidak boleh kosong";
//                              } else {
//                                return null;
//                              }
//                            },
//                          ),
//                          SizedBox(height: _size.height / 47),
//                          TextFormField(
//                            controller: addInfoKeluargaChangeNotif
//                                .controllerNamaLengkap,
//                            style: new TextStyle(color: Colors.black),
//                            decoration: new InputDecoration(
//                                labelText: 'Nama Lengkap',
//                                labelStyle: TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius: BorderRadius.circular(8))),
//                            keyboardType: TextInputType.text,
//                            textCapitalization: TextCapitalization.characters,
//                            autovalidate:
//                                addInfoKeluargaChangeNotif.autoValidate,
//                            validator: (e) {
//                              if (e.isEmpty) {
//                                return "Tidak boleh kosong";
//                              } else {
//                                return null;
//                              }
//                            },
//                          ),
//                          SizedBox(height: _size.height / 47),
//                          FocusScope(
//                              node: FocusScopeNode(),
//                              child: TextFormField(
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                  addInfoKeluargaChangeNotif
//                                      .selectBirthDate(context);
//                                },
//                                controller: addInfoKeluargaChangeNotif
//                                    .controllerTglLahir,
//                                style: new TextStyle(color: Colors.black),
//                                decoration: new InputDecoration(
//                                    labelText: 'Tanggal Lahir',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius:
//                                            BorderRadius.circular(8))),
//                                autovalidate:
//                                    addInfoKeluargaChangeNotif.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                readOnly: true,
//                              )),
//                          SizedBox(height: _size.height / 47),
//                          TextFormField(
//                            controller: addInfoKeluargaChangeNotif
//                                .controllerTempatLahirSesuaiIdentitas,
//                            style: new TextStyle(color: Colors.black),
//                            decoration: new InputDecoration(
//                                labelText: 'Tempat Lahir Sesuai Identitas',
//                                labelStyle: TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius: BorderRadius.circular(8))),
//                            keyboardType: TextInputType.text,
//                            textCapitalization: TextCapitalization.characters,
//                            autovalidate:
//                                addInfoKeluargaChangeNotif.autoValidate,
//                            validator: (e) {
//                              if (e.isEmpty) {
//                                return "Tidak boleh kosong";
//                              } else {
//                                return null;
//                              }
//                            },
//                          ),
//                          SizedBox(height: _size.height / 47),
//                          TextFormField(
//                            controller: addInfoKeluargaChangeNotif
//                                .controllerTempatLahirSesuaiIdentitasLOV,
//                            style: new TextStyle(color: Colors.black),
//                            decoration: new InputDecoration(
//                                labelText: 'Tempat Lahir Sesuai Identitas',
//                                labelStyle: TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius: BorderRadius.circular(8))),
//                            keyboardType: TextInputType.text,
//                            textCapitalization: TextCapitalization.characters,
//                            autovalidate:
//                                addInfoKeluargaChangeNotif.autoValidate,
//                            validator: (e) {
//                              if (e.isEmpty) {
//                                return "Tidak boleh kosong";
//                              } else {
//                                return null;
//                              }
//                            },
//                            onTap: (){
//                              addInfoKeluargaChangeNotif.searchBirthPlace(context);
//                            },
//                            readOnly: true,
//                          ),
//                          SizedBox(height: _size.height / 47),
//                          Text(
//                            "Jenis Kelamin",
//                            style: TextStyle(
//                                fontSize: 16,
//                                fontWeight: FontWeight.w700,
//                                letterSpacing: 0.15),
//                          ),
//                          Row(
//                            children: [
//                              Row(
//                                children: [
//                                  Radio(
//                                      activeColor: primaryOrange,
//                                      value: "01",
//                                      groupValue: addInfoKeluargaChangeNotif
//                                          .radioValueGender,
//                                      onChanged: addInfoKeluargaChangeNotif.relationshipStatusSelected != null
//                                          ? addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" || addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" || addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04"
//                                          ? (value) {}
//                                          : (value) {addInfoKeluargaChangeNotif.radioValueGender = value;}
//                                        : (value) {}
//                                  ),
//                                  Text("Laki Laki")
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Radio(
//                                    activeColor: primaryOrange,
//                                    value: "02",
//                                    groupValue: addInfoKeluargaChangeNotif
//                                        .radioValueGender,
//                                    onChanged: addInfoKeluargaChangeNotif.relationshipStatusSelected != null
//                                      ? addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" || addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" || addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04"
//                                        ? (value) {}
//                                        : (value) {addInfoKeluargaChangeNotif.radioValueGender = value;}
//                                      : (value) {}
//                                  ),
//                                  Text("Perempuan")
//                                ],
//                              ),
//                            ],
//                          ),
//                          SizedBox(height: _size.height / 47),
//                          Row(
//                            children: [
//                              Expanded(
//                                flex: 4,
//                                child: TextFormField(
//                                  autovalidate:
//                                      addInfoKeluargaChangeNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  onChanged: (e) {
//                                    addInfoKeluargaChangeNotif
//                                        .checkValidCOdeArea(e);
//                                  },
//                                  keyboardType: TextInputType.number,
//                                  inputFormatters: [
//                                    WhitelistingTextInputFormatter.digitsOnly,
//                                    LengthLimitingTextInputFormatter(10),
//                                  ],
//                                  controller: addInfoKeluargaChangeNotif
//                                      .controllerKodeArea,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                      labelText: 'Kode Area',
//                                      labelStyle:
//                                          TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(8))),
//                                ),
//                              ),
//                              SizedBox(
//                                  width:
//                                      MediaQuery.of(context).size.width / 37),
//                              Expanded(
//                                flex: 6,
//                                child: TextFormField(
//                                  autovalidate:
//                                      addInfoKeluargaChangeNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  keyboardType: TextInputType.number,
//                                  inputFormatters: [
//                                    WhitelistingTextInputFormatter.digitsOnly,
//                                    LengthLimitingTextInputFormatter(10),
//                                  ],
//                                  controller:
//                                      addInfoKeluargaChangeNotif.controllerTlpn,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                      labelText: 'Telepon',
//                                      labelStyle:
//                                          TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(8))),
//                                ),
//                              ),
//                            ],
//                          ),
//                          SizedBox(height: _size.height / 47),
//                          TextFormField(
//                            controller:
//                                addInfoKeluargaChangeNotif.controllerNoHp,
//                            style: new TextStyle(color: Colors.black),
//                            decoration: new InputDecoration(
//                                labelText: 'No Handphone',
//                                labelStyle: TextStyle(color: Colors.black),
//                                prefixText: "08",
//                                border: OutlineInputBorder(
//                                    borderRadius: BorderRadius.circular(8))),
//                            keyboardType: TextInputType.number,
//                            autovalidate:
//                                addInfoKeluargaChangeNotif.autoValidate,
//                            onChanged: (e) {
//                              addInfoKeluargaChangeNotif.checkValidNotZero(e);
//                            },
//                            validator: (e) {
//                              if (e.isEmpty) {
//                                return "Tidak boleh kosong";
//                              } else {
//                                return null;
//                              }
//                            },
//                            inputFormatters: [
//                              WhitelistingTextInputFormatter.digitsOnly,
//                              LengthLimitingTextInputFormatter(12)
//                            ],
//                          )
//                        ],
//                      ),
//                    );
//                  },
//                )
//              :
          FutureBuilder(
                  future: _loadData,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return Consumer<AddInfoKeluargaChangeNotif>(
                      builder:
                          (consumerContext, addInfoKeluargaChangeNotif, _) {
                        return Form(
                          key: addInfoKeluargaChangeNotif.key,
                          onWillPop: _onWillPop,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DropdownButtonFormField<RelationshipStatusModel>(
//                                  autovalidate:
//                                      addInfoKeluargaChangeNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e == null) {
//                                      return "Silahkan pilih pekerjaan";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
                                  value: addInfoKeluargaChangeNotif
                                      .relationshipStatusSelected,
                                  onChanged: (value) {
                                    addInfoKeluargaChangeNotif
                                        .relationshipStatusSelected = value;
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Status Hubungan",
                                    border: OutlineInputBorder(),
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: addInfoKeluargaChangeNotif
                                      .listRelationShipStatus
                                      .map((value) {
                                    return DropdownMenuItem<
                                        RelationshipStatusModel>(
                                      value: value,
                                      child: Text(
                                        value.PARA_FAMILY_TYPE_NAME,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()),
                              SizedBox(height: _size.height / 47),
                              DropdownButtonFormField<IdentityModel>(
                                value:
                                    addInfoKeluargaChangeNotif.identitySelected,
//                                autovalidate:
//                                    addInfoKeluargaChangeNotif.autoValidate,
//                                validator: (e) {
//                                  if (e == null) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Identitas",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: addInfoKeluargaChangeNotif
                                    .listIdentityType
                                    .map((data) {
                                  return DropdownMenuItem<IdentityModel>(
                                    value: data,
                                    child: Text(
                                      data.name,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                                onChanged: (newVal) {
                                  addInfoKeluargaChangeNotif.identitySelected =
                                      newVal;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                              ),
                              SizedBox(height: _size.height / 47),
                              TextFormField(
                                controller: addInfoKeluargaChangeNotif
                                    .controllerNoIdentitas,
//                                autovalidate:
//                                    addInfoKeluargaChangeNotif.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Nomor Identitas',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                  keyboardType: addInfoKeluargaChangeNotif.identitySelected != null
                                    ? addInfoKeluargaChangeNotif.identitySelected.id != "03"
                                      ? TextInputType.number
                                      : TextInputType.text
                                    : TextInputType.number,
                                  textCapitalization: TextCapitalization.characters,
                                  inputFormatters: addInfoKeluargaChangeNotif.identitySelected != null
                                    ? addInfoKeluargaChangeNotif.identitySelected.id != "03"
                                      ? [WhitelistingTextInputFormatter.digitsOnly]
                                      : null
                                    : [WhitelistingTextInputFormatter.digitsOnly]
                              ),
                              SizedBox(height: _size.height / 47),
                              TextFormField(
                                controller: addInfoKeluargaChangeNotif
                                    .controllerNamaLengkapIdentitas,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Nama Lengkap Sesuai Identitas',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                keyboardType: TextInputType.text,
                                textCapitalization:
                                    TextCapitalization.characters,
//                                autovalidate:
//                                    addInfoKeluargaChangeNotif.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
                              ),
                              SizedBox(height: _size.height / 47),
                              TextFormField(
                                controller: addInfoKeluargaChangeNotif
                                    .controllerNamaLengkap,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Nama Lengkap',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                keyboardType: TextInputType.text,
                                textCapitalization:
                                    TextCapitalization.characters,
//                                autovalidate:
//                                    addInfoKeluargaChangeNotif.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
                              ),
                              SizedBox(height: _size.height / 47),
                              TextFormField(
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                  addInfoKeluargaChangeNotif
                                      .selectBirthDate(context);
                                },
                                readOnly: true,
                                controller: addInfoKeluargaChangeNotif
                                    .controllerTglLahir,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Tanggal Lahir',
                                    labelStyle:
                                    TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                        BorderRadius.circular(8))),
//                                    autovalidate:
//                                        addInfoKeluargaChangeNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
                              ),
                              SizedBox(height: _size.height / 47),
                              TextFormField(
                                controller: addInfoKeluargaChangeNotif
                                    .controllerTempatLahirSesuaiIdentitas,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Tempat Lahir Sesuai Identitas',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                keyboardType: TextInputType.text,
                                textCapitalization:
                                    TextCapitalization.characters,
//                                autovalidate:
//                                    addInfoKeluargaChangeNotif.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
                              ),
                              SizedBox(height: _size.height / 47),
                              TextFormField(
                                onTap: (){
                                  addInfoKeluargaChangeNotif.searchBirthPlace(context);
                                },
                                readOnly: true,
                                controller: addInfoKeluargaChangeNotif
                                    .controllerTempatLahirSesuaiIdentitasLOV,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Tempat Lahir Sesuai Identitas',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                keyboardType: TextInputType.text,
                                textCapitalization:
                                    TextCapitalization.characters,
//                                autovalidate:
//                                    addInfoKeluargaChangeNotif.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
                              ),
                              SizedBox(height: _size.height / 47),
                              Text(
                                "Jenis Kelamin",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 0.15),
                              ),
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      Radio(
                                          activeColor: primaryOrange,
                                          value: "01",
                                          groupValue: addInfoKeluargaChangeNotif
                                              .radioValueGender,
                                          onChanged: addInfoKeluargaChangeNotif.relationshipStatusSelected != null
                                            ? addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" || addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" || addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04"
                                              ? (value) {}
                                              : (value) {addInfoKeluargaChangeNotif.radioValueGender = value;}
                                            : (value) {}
                                      ),
                                      Text("Laki Laki")
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Radio(
                                          activeColor: primaryOrange,
                                          value: "02",
                                          groupValue: addInfoKeluargaChangeNotif
                                              .radioValueGender,
                                          onChanged: addInfoKeluargaChangeNotif.relationshipStatusSelected != null
                                            ? addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" || addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" || addInfoKeluargaChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04"
                                              ? (value) {}
                                              : (value) {addInfoKeluargaChangeNotif.radioValueGender = value;}
                                            : (value) {}
                                      ),
                                      Text("Perempuan")
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(height: _size.height / 47),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: TextFormField(
//                                      autovalidate: addInfoKeluargaChangeNotif
//                                          .autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
                                      onChanged: (e) {
                                        addInfoKeluargaChangeNotif
                                            .checkValidCOdeArea(e);
                                      },
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter
                                            .digitsOnly,
                                        LengthLimitingTextInputFormatter(10),
                                      ],
                                      controller: addInfoKeluargaChangeNotif
                                          .controllerKodeArea,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          labelText: 'Kode Area',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                    ),
                                  ),
                                  SizedBox(
                                      width: MediaQuery.of(context).size.width /
                                          37),
                                  Expanded(
                                    flex: 6,
                                    child: TextFormField(
//                                      autovalidate: addInfoKeluargaChangeNotif
//                                          .autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter
                                            .digitsOnly,
                                        LengthLimitingTextInputFormatter(10),
                                      ],
                                      controller: addInfoKeluargaChangeNotif
                                          .controllerTlpn,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          labelText: 'Telepon',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: _size.height / 47),
                              TextFormField(
                                controller:
                                    addInfoKeluargaChangeNotif.controllerNoHp,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'No Handphone 1',
                                    labelStyle: TextStyle(color: Colors.black),
                                    prefixText: "08",
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                                keyboardType: TextInputType.number,
//                                autovalidate:
//                                    addInfoKeluargaChangeNotif.autoValidate,
                                onChanged: (e) {
                                  addInfoKeluargaChangeNotif
                                      .checkValidNotZero(e);
                                },
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  LengthLimitingTextInputFormatter(12)
                                ],
                              )
                            ],
                          ),
                        );
                      },
                    );
                  }),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Consumer<AddInfoKeluargaChangeNotif>(
                builder: (context, addInfoKelChangeNotif, _) {
                  return RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed:
                      !addInfoKelChangeNotif.isEmptyAll() ? ()
                      {
                        if (widget.flag == 0) {
                          addInfoKelChangeNotif.check(
                              context, widget.flag, null);
                        } else {
                          addInfoKelChangeNotif.check(
                              context, widget.flag, widget.index);
                        }
                      } : null,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25))
                        ],
                      ));
                },
              )),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider =
        Provider.of<AddInfoKeluargaChangeNotif>(context, listen: false);
    if (widget.flag == 0) {
      if (_provider.relationshipStatusSelected != null ||
          _provider.identitySelected != null ||
          _provider.controllerNoIdentitas.text != "" ||
          _provider.controllerNamaLengkapIdentitas.text != "" ||
          _provider.controllerNamaLengkap.text != "" ||
          _provider.controllerTglLahir.text != "" ||
          _provider.controllerTempatLahirSesuaiIdentitas.text != "" ||
          _provider.controllerTempatLahirSesuaiIdentitasLOV.text != "" ||
          _provider.controllerKodeArea.text != "" ||
          _provider.controllerTlpn.text != "" ||
          _provider.controllerNoHp.text != "") {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if ((_provider.relationshipStatusSelectedTemp != null
          ? _provider.relationshipStatusSelected.PARA_FAMILY_TYPE_ID
          != _provider.relationshipStatusSelectedTemp.PARA_FAMILY_TYPE_ID
          : false) ||
          (_provider.identitySelectedTemp != null ? _provider.identitySelected.id != _provider.identitySelectedTemp.id : false) ||
          _provider.noIdentitas != _provider.controllerNoIdentitas.text ||
          _provider.namaLengkapIdentitas !=
              _provider.controllerNamaLengkapIdentitas.text ||
          _provider.namaLengkap != _provider.controllerNamaLengkap.text ||
          _provider.birthdate != _provider.controllerTglLahir.text ||
          _provider.birthPlace != _provider.controllerTempatLahirSesuaiIdentitas.text ||
          _provider.birthPlaceLOV != _provider.controllerTempatLahirSesuaiIdentitasLOV.text ||
          _provider.radioValueGenderTemp != _provider.radioValueGender ||
          _provider.areCode != _provider.controllerKodeArea.text ||
          _provider.phoneNumber != _provider.controllerTlpn.text ||
          _provider.cellPhoneNumber != _provider.controllerNoHp.text) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      }
      else {
        return true;
      }
    }
  }
}
