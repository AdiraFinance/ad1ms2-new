import 'package:ad1ms2_dev/shared/search_pep_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class SearchPEP extends StatefulWidget {
  @override
  _SearchPEPState createState() => _SearchPEPState();
}

class _SearchPEPState extends State<SearchPEP> {
  @override
  void initState() {
    super.initState();
    if(Provider.of<SearchPEPChangeNotifier>(context, listen: false).listPEP.isNotEmpty){
      Provider.of<SearchPEPChangeNotifier>(context, listen: false).getPEP();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        primaryColor: Colors.black,
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange
      ),
      child: Scaffold(
          key: Provider.of<SearchPEPChangeNotifier>(context, listen: false).scaffoldKey,
          appBar: AppBar(
            title: Consumer<SearchPEPChangeNotifier>(
              builder: (context, searchPEP, _) {
                return TextFormField(
                  controller: searchPEP.controllerSearch,
                  style: TextStyle(color: Colors.black),
                  textInputAction: TextInputAction.search,
                  onFieldSubmitted: (e) {
                    searchPEP.searchPEP(e);
                  },
                  onChanged: (e) {
                    searchPEP.changeAction(e);
                  },
                  cursorColor: Colors.black,
                  decoration: new InputDecoration(
                    hintText: "Cari Jenis PEP & High Risk Cust (minimal 3 karakter)",
                    hintStyle: TextStyle(color: Colors.black),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                      //  when the TextFormField in unfocused
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                      //  when the TextFormField in focused
                    ),
                  ),
                  textCapitalization: TextCapitalization.characters,
                  autofocus: true,
                );
              },
            ),
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
            actions: <Widget>[
              Provider.of<SearchPEPChangeNotifier>(context, listen: true)
                      .showClear
                  ? IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        Provider.of<SearchPEPChangeNotifier>(context,
                            listen: false).clearSearchTemp();
                        Provider.of<SearchPEPChangeNotifier>(context,
                                listen: false)
                            .controllerSearch
                            .clear();
                        Provider.of<SearchPEPChangeNotifier>(context,
                                listen: false)
                            .changeAction(Provider.of<SearchPEPChangeNotifier>(
                                    context,
                                    listen: false)
                                .controllerSearch
                                .text);
                      })
                  : SizedBox(
                      width: 0.0,
                      height: 0.0,
                    )
            ],
          ),
          body: Consumer<SearchPEPChangeNotifier>(
            builder: (context, searchPEP, _) {
              return searchPEP.listPEPTemp.isNotEmpty
                ? ListView.separated(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 57,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemCount: searchPEP.listPEPTemp.length,
                  itemBuilder: (listContext, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.pop(context, searchPEP.listPEPTemp[index]);
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "${searchPEP.listPEPTemp[index].KODE} - "
                                      "${searchPEP.listPEPTemp[index].DESKRIPSI}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                )
                : ListView.separated(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 57,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemCount: searchPEP.listPEP.length,
                  itemBuilder: (listContext, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.pop(context, searchPEP.listPEP[index]);
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                              "${searchPEP.listPEP[index].KODE} - "
                              "${searchPEP.listPEP[index].DESKRIPSI}",
                              style: TextStyle(fontSize: 16),
                            ))
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
            },
          )),
    );
  }
}
