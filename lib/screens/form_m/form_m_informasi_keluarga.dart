import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/list_informasi_keluarga.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormMInfomasiKeluarga extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMInfoKeluargaChangeNotif>(
      builder: (context, formMInfoKelChangeNotif, _) {
        return Padding(
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width/37,
            vertical: MediaQuery.of(context).size.height/57,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ListInfoKeluarga()));
                },
                child: Card(
                  elevation: 3.3,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                            "Jumlah Informasi Alamat Keluarga : ${formMInfoKelChangeNotif.listFormInfoKel.length}"),
                        Icon(Icons.add_circle_outline, color: primaryOrange,)
                      ],
                    ),
                  ),
                ),
              ),
              formMInfoKelChangeNotif.autoValidate
                  ? Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 37),
                      child: Text("Tidak boleh kosong",
                          style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                  : SizedBox(height: MediaQuery.of(context).size.height / 87),
            ],
          ),
        );
      },
    );
  }
}
