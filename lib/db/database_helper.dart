import 'dart:io';
import 'package:ad1ms2_dev/models/data_dedup_model.dart';
import 'package:ad1ms2_dev/models/last_step_model.dart';
// import 'package:ad1ms2_dev/models/last_step_model.dart';
import 'package:ad1ms2_dev/models/manajemen_pic_sqlite_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_fee_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_insurance_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_loan_detail.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_oto_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_prop_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_wmp_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_photo.dart';
import 'package:ad1ms2_dev/models/ms2_appl_refund_detail_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_refund_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_taksasi_model.dart';
import 'package:ad1ms2_dev/models/ms2_application_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_family_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_income_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_ind_occupation_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_photo_detail_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_photo_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_com_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_ind_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_company_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_individu_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_comp_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_ind_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_detail_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_model.dart';
import 'package:ad1ms2_dev/models/ms2_objek_sales_model.dart';
import 'package:ad1ms2_dev/models/ms2_objt_karoseri_model.dart';
import 'package:ad1ms2_dev/models/ms2_sby_assgnmt_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_asst_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_dtl_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

class DbHelper {
  Database db;
  static const NEW_DB_VERSION = 2;
  DateTime dateTime = DateTime.now();
  String createdBy;
  String orderNo;

  initDatabase() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");

    SharedPreferences _preferences = await SharedPreferences.getInstance();
    int _firstInstall = _preferences.getInt("firstInstall");
    if (_firstInstall == null) {
      await deleteDatabase(path);
      _preferences.setInt("firstInstall", 1);
    }
// Check if the database exists
    var exists = await databaseExists(path);

    if (!exists) {
      // Should happen only the first time you launch your application
      print("Creating new copy from asset");

      // Make sure the parent directory exists
      try {
        await Directory(dirname(path)).create(recursive: true);
      }
      catch (e) {
        print("error di db helper ${e.toString()}");
      }

      // Copy from asset
      ByteData data = await rootBundle.load("assets/Ad1MS2.db");
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Write and flush the bytes written
      await File(path).writeAsBytes(bytes, flush: true);
      //open the newly created db
      db = await openDatabase(path, version: NEW_DB_VERSION);
    }
    else {
      db = await openDatabase(path);
      int _dbVersion = await db.getVersion();
      print("check db version $_dbVersion");
      db.close();
      if(_dbVersion < NEW_DB_VERSION){
        db = await openDatabase(path,version: NEW_DB_VERSION);
        await db.execute('CREATE TABLE Test (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, path_dir TEXT)');
        db.close();
      }
      else{
//        checkDb();
      }
    }
  }

  insertFlagStepper() async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int id  = await db.rawInsert('INSERT INTO para_stepper(step, flag, form_type) VALUES(1,1,"FORM M")');
    print(id);
    db.close();
  }

  insertDedupData(Map data) async{
    print(data);
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int id1 = await db.rawInsert(
        'INSERT INTO para_dedup_individu(nomor_identitas, nama_lengkap, tanggal_lahir,tmpt_lahir_sesuai_identitas,nama_gadis_ibu_kandung,alamat_identitas) '
            'VALUES("${data['nomor_identitas']}", "${data['nama_lengkap']}", "${data['tanggal_lahir']}","${data['tmpt_lahir_sesuai_identitas']}",'
            '"${data['nama_gadis_ibu_kandung']}","${data['alamat_identitas']}")'
    );
    print(id1);
    db.close();
  }

  Future<List> getDedupIndividuData() async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    List _dataDedup = await db.rawQuery("select * from para_dedup_individu");
    return _dataDedup;
  }

  insertPathDir(String pathDir) async{
    print(pathDir);
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int _pathDir = await db.rawInsert("INSERT INTO Test(name,path_dir) VALUES('testing',$pathDir)");
    print(_pathDir);
    db.close();
  }


  // set variable orderNo
  Future<bool> setOrderNumber() async{
    bool _isSuccessSetOrderNo = false;
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    createdBy = _preferences.getString("username");
    orderNo = _preferences.getString("order_no");
    if(orderNo != null){
      _isSuccessSetOrderNo = true;
    }
    return _isSuccessSetOrderNo;
  }

  //insert form m foto header
  void insertMS2PhotoHeader(MS2ApplPhotoModel model) async{
    if(await setOrderNumber()){
      String _occupationKode = model.occupation != null ? model.occupation.KODE : "";
      String _occupationDesd = model.occupation != null ? model.occupation.DESKRIPSI : "";
      String _finTypeId = model.financial != null ? model.financial.financingTypeId : "";
      String _finTypeDesc = model.financial != null ? model.financial.financingTypeName : "";
      int _kegiatanUsaha = model.kegiatanUsaha != null ? model.kegiatanUsaha.id : null;
      String _kegiatanUsahaDesc = model.kegiatanUsaha != null ? model.kegiatanUsaha.text : "";
      int _jenisKegiatanUsaha = model.jenisKegiatanUsaha != null ? model.jenisKegiatanUsaha.id : null;
      String _jenisKegiatanUsahaDesc = model.jenisKegiatanUsaha != null ? model.jenisKegiatanUsaha.text : "";
      String _jenisKonsep = model.jenisKonsep != null ? model.jenisKonsep.id : "";
      String _jenisKonsepDesc = model.jenisKonsep != null ? model.jenisKonsep.text : "";
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("INSERT INTO MS2_CUST_PHOTO(order_no, work, work_desc, financing_type, financing_type_desc, "
          "business_activities, business_activities_desc, business_activities_type, business_activities_type_desc, concept_type, "
          "concept_type_desc, created_date, created_by, active)"
          "VALUES("
          "'$orderNo',"
          "'$_occupationKode',"
          "'$_occupationDesd',"
          "'$_finTypeId',"
          "'$_finTypeDesc',"
          "'$_kegiatanUsaha',"
          "'$_kegiatanUsahaDesc',"
          "'$_jenisKegiatanUsaha',"
          "'$_jenisKegiatanUsahaDesc',"
          "'$_jenisKonsep',"
          "'$_jenisKonsepDesc',"
          "'${DateTime.now()}',"
          "'admin',"
          "1)"
      );
      var _lastInsert = await db.rawQuery('SELECT last_insert_rowid()');
      var _lastID = _lastInsert[0]['last_insert_rowid()'];

      //insert foto tempat tinggal
      for(int i=0;i<model.listFotoTempatTinggal.length;i++){
        await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(photo_id, order_no, flag, path_photo, latitude, longitude, created_date, created_by, active) "
            "VALUES("
            "'$_lastID',"
            "'$orderNo',"
            "'1',"
            "'${model.listFotoTempatTinggal[i].path}',"
            "'${model.listFotoTempatTinggal[i].latitude}',"
            "'${model.listFotoTempatTinggal[i].longitude}',"
            "'$dateTime',"
            "'$createdBy',"
            "1)");
      }

      //insert foto tempat usaha
      for(int i=0;i<model.listFotoTempatUsaha.length;i++){
        await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(photo_id, order_no, flag, path_photo, latitude, longitude, created_date, created_by, active) "
            "VALUES("
            "'$_lastID',"
            "'$orderNo',"
            "'2',"
            "'${model.listFotoTempatUsaha[i].path}',"
            "'${model.listFotoTempatUsaha[i].latitude}',"
            "'${model.listFotoTempatUsaha[i].longitude}',"
            "'$dateTime',"
            "'$createdBy',"
            "1)");
      }
      insertMS2PhotoDetail(model, _lastID);
    }
  }

  Future<bool> deleteMS2PhotoHeader() async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    var _successDelete = false;
    int _lastID;

    var _lastInsert = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO where order_no = '$orderNo'");
    if(_lastInsert.isNotEmpty){
      _lastID = _lastInsert[0]['photo_id'];
      deleteMS2PhotoDetail2(_lastID);
      // deleteMS2PhotoDetail(_lastID);
      var _check = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO_DETAIL where photo_id = '$orderNo'");
      if(_check.isEmpty){
        await db.rawQuery("delete from MS2_CUST_PHOTO where order_no = '$orderNo'");
      }
      var _checkDetail = await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL where photo_id = $_lastID");
      var _checkDetail2 = await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL_2 where photo_id = $_lastID");
      if(_checkDetail.isEmpty && _checkDetail2.isEmpty){
        _successDelete = true;
      }
    }
    else{
      _successDelete = true;
    }

    return _successDelete;
    // db.close();
  }

  Future<List> selectMS2PhotoHeader() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    createdBy = _preferences.getString("username");
    orderNo = _preferences.getString("order_no");
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    return await db.rawQuery("SELECT * from MS2_CUST_PHOTO where order_no = '$orderNo'");
  }

  // Future<bool> deleteMS2PhotoHeader() async{
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  //   createdBy = _preferences.getString("username");
  //   orderNo = _preferences.getString("order_no");
  //   print(orderNo);
  //   var databasesPath = await getDatabasesPath();
  //   var path = join(databasesPath, "Ad1MS2.db");
  //   db = await openDatabase(path);
  //   var _successDelete = false;
  //
  //   var _lastInsert = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO where order_no = '$orderNo'");
  //   if(_lastInsert.isNotEmpty){
  //     var _lastID = _lastInsert[0]['photo_id'];
  //     if(await deleteMS2PhotoDetail2(_lastID)){
  //       await db.rawQuery("delete from MS2_CUST_PHOTO where order_no = '$orderNo'");
  //     }
  //   }
  //   // db.close();
  // }

  // void selectPhoto() async{
  //   var _data1 = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO where order_no = '$orderNo'");
  //   var _data2 = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO_DETAIL where photo_id = '$orderNo'");
  //   var _data3 = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO_DETAIL_2 where photo_id = '$orderNo'");
  //   if(_data1.isNotEmpty && _data2.isNotEmpty && _data3.isNotEmpty){
  //     deleteMS2PhotoHeader();
  //   } else {
  //
  //   }
  //   // insert
  // }

  //insert form m foto list detail

  void insertMS2PhotoDetail(MS2ApplPhotoModel model, lastID) async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);

    //insert list grup unit
    for(int i=0;i<model.listGroupUnitObject.length;i++){
      await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL(photo_id, order_no, flag, group_object, group_object_desc, unit_object, unit_object_desc,"
          "created_date, created_by, active)"
          "VALUES("
          "'$lastID',"
          "'$orderNo',"
          "'1',"
          "'${model.listGroupUnitObject[i].groupObjectUnit.id}',"
          "'${model.listGroupUnitObject[i].groupObjectUnit.groupObject}',"
          "'${model.listGroupUnitObject[i].objectUnit.id}',"
          "'${model.listGroupUnitObject[i].objectUnit.objectUnit}',"
          "'$dateTime',"
          "'$createdBy',"
          "1)"
      );
      var _lastInsert = await db.rawQuery('SELECT last_insert_rowid()');
      var _detailID = _lastInsert[0]['last_insert_rowid()'];
      //insert list grup unit foto
      if(model.listGroupUnitObject[i].listImageGroupObjectUnit != null){
        for(int j=0;j<model.listGroupUnitObject[i].listImageGroupObjectUnit.length;j++){
          await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(detail_id, photo_id, order_no, flag, path_photo, latitude, longitude, created_date, created_by, active) "
              "VALUES("
              "'$_detailID',"
              "'$lastID',"
              "'$orderNo',"
              "'3',"
              "'${model.listGroupUnitObject[i].listImageGroupObjectUnit[j].path}',"
              "'${model.listGroupUnitObject[i].listImageGroupObjectUnit[j].latitude}',"
              "'${model.listGroupUnitObject[i].listImageGroupObjectUnit[j].longitude}',"
              "'$dateTime',"
              "'$createdBy',"
              "1)");
        }
      }
    }

    //insert dokumen
    for(int i=0;i<model.listDocument.length;i++){
      await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL(photo_id, order_no, flag, document_type, document_type_desc, "
          "date_receive, created_date, created_by, active)"
          "VALUES("
          "'$lastID',"
          "'$orderNo',"
          "'2',"
          "'${model.listDocument[i].jenisDocument.id}',"
          "'${model.listDocument[i].jenisDocument.name}',"
          "'${model.listDocument[i].dateTime}',"
          "'$dateTime',"
          "'$createdBy',"
          "1)");
      var _lastInsert = await db.rawQuery('SELECT last_insert_rowid()');
      var _detailID = _lastInsert[0]['last_insert_rowid()'];

      //insert dokumen foto
      // for(int i=0;i<model.listDocument.length;i++){
        await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(detail_id, photo_id, order_no, flag, path_photo, latitude, longitude,"
            " created_date, created_by, active) "
            "VALUES("
            "'$_detailID',"
            "'$lastID',"
            "'$orderNo',"
            "'4',"
            "'${model.listDocument[i].path}',"
            "'${model.listDocument[i].latitude}',"
            "'${model.listDocument[i].longitude}',"
            "'$dateTime',"
            "'$createdBy',"
            "1)");
      // }
    }
    // db.close();
  }

  Future<List> selectMS2PhotoDetail() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    createdBy = _preferences.getString("username");
    orderNo = _preferences.getString("order_no");
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    return await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL where order_no = '$orderNo'");
  }

  Future<List> selectMS2PhotoDetail2() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    createdBy = _preferences.getString("username");
    orderNo = _preferences.getString("order_no");
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    return await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL_2 where order_no = '$orderNo'");
  }

  void deleteMS2PhotoDetail(int photoID) async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL where photo_id = $photoID");
  }

  void deleteMS2PhotoDetail2(int photoID) async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL_2 where photo_id = $photoID");
    deleteMS2PhotoDetail(photoID);
  }

  // Future<bool> deleteMS2PhotoDetail(int photoID) async{
  //   var databasesPath = await getDatabasesPath();
  //   var path = join(databasesPath, "Ad1MS2.db");
  //   db = await openDatabase(path);
  //   await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL where photo_id = $photoID");
  //   return _checkDataPhoto(photoID);
  // }
  //
  // Future<bool> deleteMS2PhotoDetail2(int photoID) async{
  //   var databasesPath = await getDatabasesPath();
  //   var path = join(databasesPath, "Ad1MS2.db");
  //   db = await openDatabase(path);
  //   await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL_2 where photo_id = $photoID");
  //   return deleteMS2PhotoDetail(photoID);
  // }
  //
  // Future<bool> _checkDataPhoto(int photoID) async{
  //   var _successDelete = false;
  //   var _checkDetail = await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL where photo_id = $photoID");
  //   var _checkDetail2 = await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL_2 where photo_id = $photoID");
  //   if(_checkDetail.isEmpty && _checkDetail2.isEmpty){
  //     _successDelete = true;
  //   }
  //   return _successDelete;
  // }

  //insert data info nasabah

  void insertMS2CustomerPersonal(MS2CustomerIndividuModel model) async{
    setOrderNumber();
    String _gcModelId = model.gcModel != null ? model.gcModel.id : "";
    String _gcModelName = model.gcModel != null ? model.gcModel.name : "";
    String _educationId = model.educationModel != null ? model.educationModel.id : "";
    String _educationName = model.educationModel != null ? model.educationModel.text : "";
    String _identityModelID = model.identityModel != null ? model.identityModel.id : "";
    String _identityModelName = model.identityModel != null ? model.identityModel.name : "";
    String _birthPlaceKotId = model.birthPlaceModel != null ? model.birthPlaceModel.KABKOT_ID : "";
    String _birthPlaceKotName = model.birthPlaceModel != null ? model.birthPlaceModel.KABKOT_NAME : "";
    String _maritalStatusId = model.maritalStatusModel != null ? model.maritalStatusModel.id : "";
    String _maritalStatusName = model.maritalStatusModel != null ? model.maritalStatusModel.text : "";
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert("INSERT INTO MS2_CUSTOMER_PERSONAL (order_no, cust_group, cust_group_desc, education, education_desc, "
            "id_type, id_desc, id_no, full_name_id, full_name, date_of_birth, place_of_birth, place_of_birth_kabkota, "
            "place_of_birth_kabkota_desc, gender, gender_desc, marital_status, marital_status_desc, handphone_no, "
            "created_date, created_by, active)"
            "VALUES("
            "'$orderNo',"
            "'$_gcModelId',"
            "'$_gcModelName',"
            "'$_educationId',"
            "'$_educationName',"
            "'$_identityModelID',"
            "'$_identityModelName',"
            "'${model.id_no}',"
            "'${model.full_name_id}',"
            "'${model.full_name}',"
            "'${model.date_of_birth}',"
            "'${model.place_of_birth}',"
            "'$_birthPlaceKotId',"
            "'$_birthPlaceKotName',"
            "'${model.gender}',"
            "'${model.gender_desc}',"
            "'$_maritalStatusId',"
            "'$_maritalStatusName',"
            "'${model.handphone_no}',"
            "'$dateTime',"
            "'$createdBy',"
            "'${model.active}')"
    );
    // print(_result);
    // db.close();
  }

  Future<bool> deleteMS2CustomerPersonal() async{
    setOrderNumber();
    bool _isDelete = false;
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    var _check = await db.rawQuery("select * from MS2_CUSTOMER_PERSONAL where order_no = '$orderNo'");

    if(_check.isNotEmpty){
      print("masuk if");
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_CUSTOMER_PERSONAL where order_no = '$orderNo'");
      _isDelete = true;
    }
    else {
      print("masuk else");
      _isDelete = true;
    }
    print("hasil delete");
    print(_check);
    return _isDelete;
    // db.close();
  }

  void insertMS2Customer(MS2CustomerModel model) async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    // int _result = await db.rawInsert(
    //     "INSERT INTO MS2_CUSTOMER VALUES("
    //         "${model.order_no},"
    //         "${model.id_obligor},"
    //         "${model.cust_type},"
    //         "${model.npwp_address},"
    //         "${model.npwp_name},"
    //         "${model.npwp_no},"
    //         "${model.npwpModel.id},"
    //         "${model.npwpModel.text},"
    //         "${model.flag_npwp},"
    //         "${model.pkp_flag},"
    //         "${model.is_guarantor},"
    //         "${model.corespondence},"
    //         "${model.debitur_code_12},"
    //         "${model.debitur_code_13},"
    //         "${model.last_known_cust_state},"
    //         "${model.created_date},"
    //         "${model.created_by},"
    //         "${model.modified_date},"
    //         "${model.modified_by},"
    //         "${model.active})"
    // );
    // print(_result);
    await db.rawQuery("INSERT INTO MS2_CUSTOMER(order_no, id_obligor, cust_type, npwp_address,npwp_name,npwp_no, "
          "created_date, created_by, active)"
    "VALUES("
        "'$orderNo',"
        "'null',"
        "'null',"
        "'${model.npwp_address}',"
        "'${model.npwp_name}',"
        "'${model.npwp_no}',"
        "'${model.created_date}',"
        "${model.created_by},"
        "'${model.active}')"
    );
    // db.close();
  }

  void deleteMS2Customer() async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_CUSTOMER where order_no = " + orderNo);
    // db.close();
  }

  void insertMS2CustomerCompany (MS2CustomerCompanyModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
        "INSERT INTO MS2_CUSTOMER_COMPANY(order_no,comp_type,comp_desc,profil,profil_desc,"
            "comp_name,establish_date,sector_economic,sector_economic_desc,"
            "nature_of_buss,nature_of_buss_desc,no_of_year_work,no_of_year_buss,created_date,created_by,active) "
            "VALUES("
            "'$orderNo',"
            "'${model.typeInstitutionModel.PARA_ID}',"
            "'${model.typeInstitutionModel.PARA_NAME}',"
            "'${model.profilModel.id}',"
            "'${model.profilModel.text}',"
            "'${model.comp_name}',"
            "'${model.establish_date}',"
            "'${model.sectorEconomicModel.KODE}',"
            "'${model.sectorEconomicModel.DESKRIPSI.trim()}',"
            "'${model.businessFieldModel.KODE}',"
            "'${model.businessFieldModel.DESKRIPSI.trim()}',"
            "'${model.no_of_year_work}',"
            "'${model.no_of_year_buss}',"
            "'${model.created_date}',"
            "'${model.created_by}',"
            "'${model.active}')"
    );
    db.close();
  }

  Future<bool> deleteMS2CustomerCompany() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_CUSTOMER_COMPANY where order_no = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_CUSTOMER_COMPANY where order_no = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }
  
  void insertMS2CustomerCompanyForPIC(ManagementPICSQLiteModel model, String orderNo) async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawUpdate("UPDATE MS2_CUSTOMER_COMPANY SET "
        "id_type = '${model.identityModel.id}',"
        "id_desc = '${model.identityModel.text}',"
        "id_no = '${model.id_no}',"
        "full_name_id = '${model.full_name_id}',"
        "full_name = '${model.full_name}',"
        "alias_name = '${model.alias_name}',"
        "degree = '${model.degree}',"
        "date_of_birth = '${model.date_of_birth}',"
        "place_of_birth = '${model.place_of_birth}',"
        "place_of_birth_kabkota = '${model.birthPlaceModel.KABKOT_ID}',"
        "place_of_birth_kabkota_desc = '${model.birthPlaceModel.KABKOT_NAME}',"
        "id_date = '${model.id_date}',"
        "id_expire_date = '${model.id_expire_date}',"
        "ac_level = '${model.ac_level}',"
        "handphone_no = '${model.handphone_no}',"
        "email = '${model.email}',"
        "shrldng_percent = '${model.shrldng_percent}',"
        "dedup_score = '${model.dedup_score}',"
        "created_date = '${model.created_date}',"
        "created_by = '${model.created_by}',"
        "modified_date = '${model.modified_date}',"
        "modified_by = '${model.modified_by}',"
        "active = '${model.active}' "
        "WHERE order_no = '$orderNo'");
    print(await db.rawQuery("SELECT * FROM MS2_CUSTOMER_COMPANY WHERE order_no = '000001'"));
    // db.close();
  }

  //insert data semua address jadi 1
  void insertMS2CustAddr (List<MS2CustAddrModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    // int _result = await db.rawInsert(
    //     "INSERT INTO MS2_CUST_ADDR VALUES("
    //         "${model.order_no},"
    //         "${model.koresponden},"
    //         "${model.matrix_addr},"
    //         "${model.address},"
    //         "${model.rt},"
    //         "${model.rt_desc},"
    //         "${model.rw},"
    //         "${model.rw_desc},"
    //         "${model.provinsi},"
    //         "${model.provinsi_desc},"
    //         "${model.kabkot},"
    //         "${model.kabkot_desc},"
    //         "${model.kecamatan},"
    //         "${model.kecamatan_desc},"
    //         "${model.kelurahan},"
    //         "${model.kelurahan_desc},"
    //         "${model.zip_code},"
    //         "${model.handphone_no},"
    //         "${model.phone1},"
    //         "${model.phone1_area},"
    //         "${model.phone_2},"
    //         "${model.phone_2_area},"
    //         "${model.fax},"
    //         "${model.fax_area},"
    //         "${model.addr_type},"
    //         "${model.addr_desc},"
    //         "${model.created_date},"
    //         "${model.created_by},"
    //         "${model.modified_date},"
    //         "${model.modified_by},"
    //         "${model.active},"
    // );
    // print(_result);
    for(int i=0; i<model.length; i++){
      await db.rawInsert("INSERT INTO MS2_CUST_ADDR(order_no, koresponden, matrix_addr, address, rt, rw, provinsi, provinsi_desc,"
          "kabkot, kabkot_desc, kecamatan, kecamatan_desc, kelurahan, kelurahan_desc, zip_code, phone1, phone1_area, addr_type,"
          "addr_desc, latitude, longitude, address_from_map, created_date, created_by, active) "
          "VALUES("
          "'$orderNo',"
          "'${model[i].koresponden}',"
          "'${model[i].matrix_addr}',"
          "'${model[i].address}',"
          "'${model[i].rt}',"
          "'${model[i].rw}',"
          "'${model[i].provinsi}',"
          "'${model[i].provinsi_desc}',"
          "'${model[i].kabkot}',"
          "'${model[i].kabkot_desc}',"
          "'${model[i].kecamatan}',"
          "'${model[i].kecamatan_desc}',"
          "'${model[i].kelurahan}',"
          "'${model[i].kelurahan_desc}',"
          "'${model[i].zip_code}',"
          "'${model[i].phone1}',"
          "'${model[i].phone1_area}',"
          "'${model[i].addr_type}',"
          "'${model[i].addr_desc}',"
          "'${model[i].latitude}',"
          "'${model[i].longitude}',"
          "'${model[i].addr_from_map}',"
          "'$dateTime',"
          "'$createdBy',"
          "'${model[i].active}')"
      );
    }
//    db.close();
  }

  Future<List> selectMS2CustAddr(String type) async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUST_ADDR where order_no = '$orderNo' AND matrix_addr = '$type'");
    }
    return _data;
  }

  Future<bool> deleteMS2CustAddr(String type) async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUST_ADDR where order_no = '$orderNo' AND matrix_addr = '$type'");
      if(_check.isNotEmpty){
        print("di if");
        await db.rawQuery("DELETE FROM MS2_CUST_ADDR WHERE order_no = '$orderNo' AND matrix_addr = '$type'");
        _isDelete = true;
      } else {
        print("di else");
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  void insertMS2CustFamily (MS2CustFamilyModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    // int _result = await db.rawInsert(
    //     "INSERT INTO MS2_CUST_FAMILY VALUES("
    //         "${model.order_no},"
    //         "${model.relation_status},"
    //         "${model.relation_status_desc},"
    //         "${model.full_name_id},"
    //         "${model.full_name},"
    //         "${model.id_no},"
    //         "${model.date_of_birth},"
    //         "${model.place_of_birth},"
    //         "${model.place_of_birth_kabkota},"
    //         "${model.place_of_birth_kabkota_desc},"
    //         "${model.gender},"
    //         "${model.gender_desc},"
    //         "${model.degree},"
    //         "${model.id_type},"
    //         "${model.id_desc},"
    //         "${model.created_date},"
    //         "${model.created_by},"
    //         "${model.modified_date},"
    //         "${model.modified_by},"
    //         "${model.active},"
    //         "${model.dedup_score},"
    //         "${model.phone1},"
    //         "${model.phone1_area},"
    //         "${model.handphone_no},"
    // );
    // print(_result);
    await db.rawInsert("INSERT INTO MS2_CUST_FAMILY(order_no, relation_stauts, relation_status_desc, full_name_id, full_name, "
        "id_no, date_of_birth, place_of_birth, place_of_birth_kabkota, place_of_birth_kabkota_desc, gender, gender_desc, id_type,"
        "id_desc, phone1, phone1_area, handphone_no, created_date, created_by, active)"
        "VALUES("
                "'$orderNo',"
                "'${model.relation_status}',"
                "'${model.relation_status_desc}',"
                "'${model.full_name_id}',"
                "'${model.full_name}',"
                "'${model.id_no}',"
                "'${model.date_of_birth}',"
                "'${model.place_of_birth}',"
                "'${model.place_of_birth_kabkota}',"
                "'${model.place_of_birth_kabkota_desc}',"
                "'${model.gender}',"
                "'${model.gender_desc}',"
                "'${model.id_type}',"
                "'${model.id_desc}',"
                "'${model.phone1}',"
                "'${model.phone1_area}',"
                "'${model.handphone_no}',"
                "'$dateTime',"
                "'$createdBy',"
                "'${model.active}')"
        );
    // db.close();
  }

  Future<bool> deleteMS2CustFamily() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUST_FAMILY where order_no = '$orderNo'");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){
        var databasesPath = await getDatabasesPath();
        var path = join(databasesPath, "Ad1MS2.db");
        db = await openDatabase(path);
        await db.rawQuery("delete from MS2_CUST_FAMILY where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  void insertMS2CustIncome(List<MS2CustIncomeModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i=0; i<model.length; i++){
      await db.rawInsert("INSERT INTO MS2_CUST_INCOME(cust_income_id, type_income_frml, income_type, income_desc,"
          "income_value, idx, created_date, crated_by, active) "
          "VALUES("
          "'$orderNo',"
          "'${model[i].type_income_frml}',"
          "'${model[i].income_type}',"
          "'${model[i].income_desc}',"
          "'${model[i].income_value}',"
          "'${model[i].idx}',"
          "'$dateTime',"
          "'$createdBy',"
          "'${model[i].active}')"
      );
    }
    // db.close();
  }

  Future<bool> deleteMS2CustIncome() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_CUST_INCOME where cust_income_id = '$orderNo'");
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_CUST_INCOME where cust_income_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }

  void selectIncome() async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    print(await db.rawQuery("SELECT * FROM MS2_CUST_INCOME"));
    // db.close();
  }

  void insertMS2CustIndOccupation (MS2CustIndOccupationModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      int _result = await db.rawInsert("INSERT INTO MS2_CUST_IND_OCCUPATION(order_no, occupation, occupation_desc, buss_name, "
          "buss_type, pep_type, pep_desc, emp_status, emp_status_desc, profession_type, profession_desc, sector_economic,"
          "sector_economic_desc, location_status, location_status_desc, buss_location, buss_location_desc, total_emp, "
          "no_of_year_work, no_of_year_buss, created_date, created_by, active) "
          "VALUES("
          "'$orderNo',"
          "'${model.occupation}',"
          "'${model.occupation_desc}',"
          "'${model.buss_name}',"
          "'${model.buss_type}',"
          "'${model.pep_type}',"
          "'${model.pep_desc}',"
          "'${model.emp_status}',"
          "'${model.emp_status_desc}',"
          "'${model.profession_type}',"
          "'${model.profession_desc}',"
          "'${model.sector_economic}',"
          "'${model.sector_economic_desc}',"
          "'${model.location_status}',"
          "'${model.location_status_desc}',"
          "'${model.buss_location}',"
          "'${model.buss_location_desc}',"
          "'${model.total_emp}',"
          "'${model.no_of_year_work}',"
          "'${model.no_of_year_buss}',"
          "'$dateTime',"
          "'$createdBy',"
          "'${model.active}')"
      );
      print(_result);
    }
    // db.close();
  }

  Future<List> selectMS2CustIndOccupation() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUST_IND_OCCUPATION where order_no = '$orderNo'");
    }
    return _data;
  }

  Future<bool> deleteMS2CustIndOccupation() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUST_IND_OCCUPATION where order_no = '$orderNo'");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){
        var databasesPath = await getDatabasesPath();
        var path = join(databasesPath, "Ad1MS2.db");
        db = await openDatabase(path);
        await db.rawQuery("delete from MS2_CUST_IND_OCCUPATION where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  void insertMS2CustPhoto (MS2CustPhotoModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int _result = await db.rawInsert(
        "INSERT INTO MS2_CUST_PHOTO VALUES("
            "${model.photo_id},"
            "${model.order_no},"
            "${model.work},"
            "${model.work_desc},"
            "${model.financing_type},"
            "${model.financing_type_desc},"
            "${model.business_activities},"
            "${model.business_activities_desc},"
            "${model.business_activities_type},"
            "${model.business_activities_type_desc},"
            "${model.concept_type},"
            "${model.concept_type_desc},"
            "${model.created_date},"
            "${model.created_by},"
            "${model.modified_date},"
            "${model.modified_by},"
            "${model.active},"
    );
    print(_result);
    // db.close();
  }

  void insertMS2CustPhotoDetail (MS2CustPhotoDetailModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int _result = await db.rawInsert(
        "INSERT INTO MS2_CUST_PHOTO_DETAIL VALUES("
            "${model.photo_id},"
            "${model.order_no},"
            "${model.index},"
            "${model.residence_photo},"
            "${model.business_photo},"
            "${model.group_unit_photo},"
            "${model.document_unit_photo},"
            "${model.created_date},"
            "${model.created_by},"
            "${model.modified_date},"
            "${model.modified_by},"
            "${model.active},"
    );
    print(_result);
    // db.close();
  }

  void insertMS2CustShrhldrCom (MS2CustShrhldrComModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int _result = await db.rawInsert(
        "INSERT INTO MS2_CUST_SHRHLDR_COM VALUES("
            "${model.shrhldr_comp_id},"
            "${model.typeInstitutionModel.PARA_ID},"
            "${model.typeInstitutionModel.PARA_NAME},"
            "${model.profil},"
            "${model.profil_desc},"
            "${model.comp_name},"
            "${model.akta_no},"
            "${model.akta_expire_date},"
            "${model.siup_no},"
            "${model.siup_start_date},"
            "${model.tdp_no},"
            "${model.tdp_start_date},"
            "${model.establish_date},"
            "${model.flag_comp},"
            "${model.basic_capital},"
            "${model.paid_capital},"
            "${model.npwp_address},"
            "${model.npwp_name},"
            "${model.npwp_no},"
            "${model.npwp_type},"
            "${model.npwp_type_desc},"
            "${model.flag_npwp},"
            "${model.pkp_flag.id},"
            "${model.sector_economic},"
            "${model.sector_economic_desc},"
            "${model.nature_of_buss},"
            "${model.nature_of_buss_desc},"
            "${model.location_status},"
            "${model.location_status_desc},"
            "${model.buss_location},"
            "${model.buss_location_desc},"
            "${model.total_emp},"
            "${model.no_of_year_work},"
            "${model.no_of_year_buss},"
            "${model.ac_level},"
            "${model.shrldng_percent},"
            "${model.dedup_score},"
            "${model.created_date},"
            "${model.created_by},"
            "${model.modified_date},"
            "${model.modified_by},"
            "${model.active},"
    );
    print(_result);
    // db.close();
  }

  void deleteMS2CustShrhldrCom() async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_CUST_SHRHLDR_COM where shrhldr_comp_id = " + orderNo);
    // db.close();
  }

  void insertMS2CustShrhldrInd (MS2CustShrhldrIndModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert("INSERT INTO MS2_CUST_SHRHLDR_IND(shrhldr_ind_id, dedup_score, relation_status, relation_status_desc, "
        "id_type, id_type_desc, id_no, full_name_id, full_name, created_date, created_by, active)  "
        "VALUES("
            "'$orderNo',"
            "'${model.dedup_score}',"
            "'${model.relation_status}',"
            "'${model.relation_status_desc}',"
            "'${model.id_type}',"
            "'${model.id_type_desc}',"
            "'${model.id_no}',"
            "'${model.full_name_id}',"
            "'${model.full_name}',"
            "'$dateTime',"
            "'$createdBy',"
            "1)"
    );
    // db.close();
  }

  Future<bool> deleteMS2CustShrhldrInd() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_CUST_SHRHLDR_IND where shrhldr_ind_id = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_CUST_SHRHLDR_IND where shrhldr_ind_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }

  void insertMS2Document (List<MS2DocumentModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i=0; i<model.length; i++){
      await db.rawInsert("INSERT INTO MS2_DOCUMENT(ac_appl_objt_id, ac_appl_no, file_header_id, file_name, "
          "document_type_id, document_type_desc, latitude, longitude, created_date, created_by) "
          "VALUES("
          "'${model[i].ac_appl_objt_id}',"
          "'${model[i].ac_appl_no}',"
          "'${model[i].file_header_id}',"
          "'${model[i].file_name}',"
          "'${model[i].document_type_id}',"
          "'${model[i].document_type_desc}',"
          "'${model[i].latitude}',"
          "'${model[i].longitude}',"
          "'$dateTime',"
          "'$createdBy')"
      );
    }
    // db.close();
  }

  Future<bool> deleteMS2Document() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_DOCUMENT where ac_appl_objt_id = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_DOCUMENT where ac_appl_objt_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }

  void insertMS2GrntrComp (List<MS2GrntrCompModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i=0; i<model.length;i++){
      await db.rawInsert("INSERT INTO MS2_GRNTR_COMP(grntr_comp_id, dedup_score, comp_type, comp_desc, comp_name, establish_date, "
          "npwp_no, flag_comp, created_date, created_by, active) "
          "VALUES("
          "'$orderNo',"
          "'${model[i].dedup_score}',"
          "'${model[i].comp_type}',"
          "'${model[i].comp_desc}',"
          "'${model[i].comp_name}',"
          "'${model[i].establish_date}',"
          "'${model[i].npwp_no}',"
          "1,"
          "'$dateTime',"
          "'$createdBy',"
          "1)"
      );
    }
    // db.close();
  }

  void deleteMS2GrntrComp() async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_GRNTR_COMP where grntr_comp_id = " + orderNo);
    // db.close();
  }

  void insertMS2GrntrInd (List<MS2GrntrIndModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i=0; i<model.length;i++){
      await db.rawInsert("INSERT INTO MS2_GRNTR_IND(grntr_ind_id, relation_status, relation_status_desc, dedup_score, id_type, "
          "id_desc, id_no, full_name_id, full_name, created_date, created_by, active) "
          "VALUES("
          "'$orderNo',"
          "'${model[i].relation_status}',"
          "'${model[i].relation_status_desc}',"
          "'${model[i].dedup_score}',"
          "'${model[i].id_type}',"
          "'${model[i].id_desc}',"
          "'${model[i].id_no}',"
          "'${model[i].full_name_id}',"
          "'${model[i].full_name}',"
          "'$dateTime',"
          "'$createdBy',"
          "1)"
      );
    }
    // db.close();
  }

  void deleteMS2GrntrInd() async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_GRNTR_IND where grntr_ind_id = " + orderNo);
    // db.close();
  }

  // Credit Limit
  void insertMS2LME (MS2LMEModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
      "INSERT INTO MS2_LME (ac_oid, ac_lme_id, ac_appl_no, ac_flag_eligible, ac_disburse_type, "
        "ac_product_id, ac_product_desc, ac_installment_amount, ac_ph_amount, ac_voucher_code, "
        "ac_tenor, created_date, created_by, modified_date, modified_by, "
        "active, customer_grading, jenis_penawaran, jenis_penawaran_desc, urutan_pencairan, "
        "jenis_opsi, jenis_opsi_desc, max_plafond, max_tenor, no_referensi, "
        "max_installment, portfolio, "
        "VALUES("
        "'${model.ac_oid}',"
        "'${model.ac_lme_id}',"
        "'${model.ac_appl_no}',"
        "'${model.ac_flag_eligible}',"
        "'${model.ac_disburse_type}',"
        "'${model.ac_product_id}',"
        "'${model.ac_product_desc}',"
        "'${model.ac_installment_amount}',"
        "'${model.ac_ph_amount}',"
        "'${model.ac_voucher_code}',"
        "'${model.ac_tenor}',"
        "'${model.created_date}',"
        "'${model.created_by}',"
        "'${model.modified_date}',"
        "'${model.modified_by}',"
        "'${model.active}',"
        "'${model.customer_grading}',"
        "'${model.jenis_penawaran}',"
        "'${model.jenis_penawaran_desc}',"
        "'${model.urutan_pencairan}',"
        "'${model.jenis_opsi}',"
        "'${model.jenis_opsi_desc}',"
        "'${model.max_plafond}',"
        "'${model.max_tenor}',"
        "'${model.no_refensi}',"
        "'${model.max_installment}',"
        "'${model.portfolio}',"
    );
    // db.close();
  }

  // Credit Limit
  void insertMS2LMEDetail (MS2LMEDetailModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
      "INSERT INTO MS2_LME_DETAIL (ac_source_reff_id, ac_appl_objt_id, ac_flag_lme, created_date, created_by, "
        "modified_date, modified_by, active)"
        "VALUES("
        "'${model.ac_source_reff_id}',"
        "'${model.ac_appl_objt_id}',"
        "'${model.ac_flag_lme}',"
        "'${model.created_date}',"
        "'${model.created_by}',"
        "'${model.modified_date}',"
        "'${model.modified_by}',"
        "'${model.active}',"
    );
    // db.close();
  }

  // Inf Salesman
  void insertMS2ObjekSales (List<MS2ObjekSalesModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_OBJEK_SALES (appl_sales_id, empl_id, sales_type, sales_type_desc, empl_head_id, "
            "empl_job, empl_job_desc, ref_contract_no, active, created_date, "
            "created_by, modified_date, modified_by, empl_head_job, empl_head_job_desc)"
          "VALUES("
          "'${model[i].appl_sales_id}',"
          "'${model[i].empl_id}',"
          "'${model[i].sales_type}',"
          "'${model[i].sales_type_desc}',"
          "'${model[i].empl_head_id}',"
          "'${model[i].empl_job}',"
          "'${model[i].empl_job_desc}',"
          "'${model[i].ref_contract_no}',"
          "'${model[i].active}',"
          "'${model[i].created_date}',"
          "'${model[i].created_by}',"
          "'${model[i].modified_date}',"
          "'${model[i].modified_by}',"
          "'${model[i].empl_head_job}',"
          "'${model[i].empl_head_job_desc}')"
      );
    }
//    db.close();
  }

  Future<bool> deleteMS2ObjekSales() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_OBJEK_SALES where appl_sales_id = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_OBJEK_SALES where appl_sales_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }

  // Inf Objek Karoseri
  void insertMS2ObjtKaroseri (List<MS2ObjtKaroseriModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_OBJT_KAROSERI (appl_karoseri_id, pks_karoseri, comp_karoseri, comp_karoseri_desc, karoseri, "
            "karoseri_desc, price, karoseri_qty, total_karoseri, created_date, "
            "created_by, modified_date, modified_by, active, dp_karoseri, flag_karoseri) "
          "VALUES("
          "'${model[i].appl_karoseri_id}',"
          "'${model[i].pks_karoseri}',"
          "'${model[i].comp_karoseri}',"
          "'${model[i].comp_karoseri_desc}',"
          "'${model[i].karoseri}',"
          "'${model[i].karoseri_desc}',"
          "'${model[i].price}',"
          "'${model[i].karoseri_qty}',"
          "'${model[i].total_karoseri}',"
          "'${model[i].created_date}',"
          "'${model[i].created_by}',"
          "'${model[i].modified_date}',"
          "'${model[i].modified_by}',"
          "'${model[i].active}',"
          "'${model[i].dp_karoseri}',"
          "'${model[i].flag_karoseri}')"
      );
    }
    // db.close();
  }

  Future<bool> deleteMS2ObjtKaroseri() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_OBJT_KAROSERI where appl_karoseri_id = '$orderNo'");
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_OBJT_KAROSERI where appl_karoseri_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }

  // Inf Aplikasi
  void insertMS2Application (MS2ApplicationModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
      "INSERT INTO MS2_APPLICATION (appl_no, flag_mayor, flag_fiducia, source_application, order_date, "
          "application_date, svy_appointment_date, order_type, flag_account, account_no_form, "
          "sentra_c, sentra_c_desc, unit_c, unit_c_desc, initial_recomendation, "
          "final_recomendation, brms_scoring, persetujuan_di_tempat, sign_pk, max_level_approval, "
          "flag_receive_information, konsep_type, konsep_type_desc, objt_qty, prop_insr_type, "
          "prop_insr_type_desc, unit, cola_type, cola_type_desc, appl_contract_no, "
          "calculated_at, last_known_appl_state, last_known_appl_position, last_known_appl_status, last_known_handled_by, "
          "created_date, created_by, modified_date, modified_by, active, "
          "doc_ver_status, cust_status, appr_aos_status, flag_das_pps, flag_upload, "
          "flag_ia, flag_dakor_ca, flag_bypasscapo, flag_dedup_ca, flag_disable_dedup_ca, "
          "order_ad1gate, user_ad1gate, dealer_note, flag_source_ms2 )"
        "VALUES("
        "'$orderNo',"
        "'${model.flag_mayor}',"
        "'${model.flag_fiducia}',"
        "'${model.source_application}',"
        "'${model.order_date}',"
        "'${model.application_date}',"
        "'${model.svy_appointment_date}',"
        "'${model.order_type}',"
        "'${model.flag_account}',"
        "'${model.account_no_form}',"
        "'${model.sentra_c}',"
        "'${model.sentra_c_desc}',"
        "'${model.unit_c}',"
        "'${model.unit_c_desc}',"
        "'${model.initial_recomendation}',"
        "'${model.final_recomendation}',"
        "'${model.brms_scoring}',"
        "'${model.persetujuan_di_tempat}',"
        "'${model.sign_pk}',"
        "'${model.max_level_approval}',"
        "'${model.flag_receive_information}',"
        "'${model.konsep_type}',"
        "'${model.konsep_type_desc}',"
        "'${model.objt_qty}',"
        "'${model.prop_insr_type}',"
        "'${model.prop_insr_type_desc}',"
        "'${model.unit}',"
        "'${model.cola_type}',"
        "'${model.cola_type_desc}',"
        "'${model.appl_contract_no}',"
        "'${model.calculated_at}',"
        "'${model.last_known_appl_state}',"
        "'${model.last_known_appl_position}',"
        "'${model.last_known_appl_status}',"
        "'${model.last_known_handled_by}',"
        "'$dateTime',"
        "'${model.created_by}',"
        "'${model.modified_date}',"
        "'${model.modified_by}',"
        "1,"
        "'${model.doc_ver_status}',"
        "'${model.cust_status}',"
        "'${model.appr_aos_status}',"
        "'${model.flag_das_pps}',"
        "'${model.flag_upload}',"
        "'${model.flag_ia}',"
        "'${model.flag_dakor_ca}',"
        "'${model.flag_bypasscapo}',"
        "'${model.flag_dedup_ca}',"
        "'${model.flag_disable_dedup_ca}',"
        "'${model.order_ad1gate}',"
        "'${model.user_ad1gate}',"
        "'${model.dealer_note}',"
        "'${model.flag_source_ms2}')"
    );
    // db.close();
  }

  Future<bool> deleteMS2Application() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_APPLICATION where appl_no = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_APPLICATION where appl_no = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }

  Future<List> selectMS2Application() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_APPLICATION where appl_no = '$orderNo'");
    }
    return _data;
  }

  // Inf Struktur Kredit Biaya
  void insertMS2ApplFee (List<MS2ApplFeeModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_APPL_FEE (fee_id, fee_type, fee_cash, fee_credit, total_fee, "
          "active, created_date, created_by)"
          "VALUES("
          "'$orderNo',"
          "'${model[i].fee_type}',"
          "'${model[i].fee_cash}',"
          "'${model[i].fee_credit}',"
          "'${model[i].total_fee}',"
          "'${model[i].active}',"
          "'${model[i].created_date}',"
          "'${model[i].created_by}')"
      );
    }
//    db.close();
  }

  void deleteMS2ApplFee() async{
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_APPL_FEE where fee_id = " + orderNo);
    db.close();
  }

  // Inf Struktur Kredit Stepping
  void insertMS2ApplLoanDetailStepping (List<MS2ApplLoanDetailModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_APPL_LOAN_DETAIL VALUES("
          "'$orderNo',"
          "'${model[i].ac_installment_no}',"
          "'${model[i].ac_percentage}',"
          "'${model[i].ac_amount}',"
          "'${model[i].active}',"
          "'${model[i].created_date}',"
          "'${model[i].created_by}',"
          "'${model[i].modified_date}',"
          "'${model[i].modified_by}',"
      );
    }
    // db.close();
  }

  // Inf Struktur Kredit Irreguler
  void insertMS2ApplLoanDetailIrreguler (List<MS2ApplLoanDetailModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_APPL_LOAN_DETAIL VALUES("
          "'$orderNo',"
          "'${model[i].ac_installment_no}',"
          "'${model[i].ac_percentage}',"
          "'${model[i].ac_amount}',"
          "'${model[i].active}',"
          "'${model[i].created_date}',"
          "'${model[i].created_by}',"
          "'${model[i].modified_date}',"
          "'${model[i].modified_by}',"
      );
    }
    // db.close();
  }

  // Inf Unit Object
  void insertMS2ApplObject (MS2ApplObjectModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
      "INSERT INTO MS2_APPL_OBJECT ("
          "appl_objt_id, "
          "financing_type, "
          "buss_activities, "
          "buss_activities_desc, "
          "buss_activities_type, "
          "buss_activities_type_desc, "
          "group_object, "
          "group_object_desc, "
          "object, "
          "object_desc, "
          "product_type, "
          "product_type_desc, "
          "brand_object, "
          "brand_object_desc, "
          "object_type, "
          "object_type_desc, "
          "object_model, "
          "object_model_desc, "
          "object_used, "
          "object_used_desc, "
          "object_purpose, "
          "object_purpose_desc, "
          "group_sales, "
          "group_sales_desc, "
          "order_source, "
          "order_source_name, "
          "flag_third_party, "
          "third_party_type, "
          "third_party_type_desc, "
          "third_party_id, "
          "work, "
          "work_desc, "
          "sentra_d, "
          "sentra_d_desc, "
          "unit_d, "
          "unit_d_desc, "
          "program, "
          "program_desc, "
          "rehab_type, "
          "rehab_type_desc, "
          "reference_no, "
          "appl_contrac_no, "
          "installment_type, "
          "installment_type_desc, "
          "appl_top, "
          "payment_method, "
          "payment_method_desc, "
          "eff_rate, "
          "flat_rate, "
          "object_price, "
          "karoseri_price, "
          "total_amt, "
          "dp_net, "
          "installment_decline_n, "
          "payment_per_year, "
          "dp_branch, "
          "dp_gross, "
          "principal_amt, "
          "installment_paid, "
          "interest_amt, "
          "ltv, "
          "disbursement, "
          "dsr, "
          "dir, "
          "dsc, "
          "irr, "
          "gp_type, "
          "new_tenor, "
          "total_stepping, "
          "balloon_type, "
          "last_percen_installment, "
          "last_installment_amt, "
          "is_auto_debit,"
          "bank_id, "
          "account_no, "
          "account_behalf, "
          "purpose_type, "
          "dp_source, "
          "va_no, "
          "va_amt, "
          "disburse_purpose, "
          "disburse_type, "
          "appl_objt_last_state, "
          "appl_objt_next_state, "
          "created_date, "
          "created_by, "
          "modified_date, "
          "modified_by, "
          "active, "
          "tact, "
          "tmax, "
          "appl_send_flag, "
          "is_without_colla,"
          "first_installment, "
          "flag_save_karoseri, "
          "pelunasan_amt, "
          "appl_withdrawal_flag, "
          "appl_model_dtl, "
          "insentif_sales, "
          "insentif_payment, "
          "nik, "
          "nama, "
          "jabatan, "
          "outlet_id, "
          "group_id, "
          "dealer_matrix, "
          "ppd_date, "
          "ppd_cancel_date, "
          "dp_chasis_net, "
          "dp_chasis_gross, "
          "ujrah_price, "
          "total_dp_krs)"
        "VALUES("
        "'$orderNo',"
        "'${model.financing_type}',"
        "'${model.buss_activities}',"
        "'${model.buss_activities_desc}',"
        "'${model.buss_activities_type}',"
        "'${model.buss_activities_type_desc}',"
        "'${model.group_object}',"
        "'${model.group_object_desc}',"
        "'${model.object}',"
        "'${model.object_desc}',"
        "'${model.product_type}',"
        "'${model.product_type_desc}',"
        "'${model.brand_object}',"
        "'${model.brand_object_desc}',"
        "'${model.object_type}',"
        "'${model.object_type_desc}',"
        "'${model.object_model}',"
        "'${model.object_model_desc}',"
        "'${model.object_used}',"
        "'${model.object_used_desc}',"
        "'${model.object_purpose}',"
        "'${model.object_purpose_desc}',"
        "'${model.group_sales}',"
        "'${model.group_sales_desc}',"
        "'${model.order_source}',"
        "'${model.order_source_name}',"
        "'${model.flag_third_party}',"
        "'${model.third_party_type}',"
        "'${model.third_party_type_desc}',"
        "'${model.third_party_id}',"
        "'${model.work}',"
        "'${model.work_desc}',"
        "'${model.sentra_d}',"
        "'${model.sentra_d_desc}',"
        "'${model.unit_d}',"
        "'${model.unit_d_desc}',"
        "'${model.program}',"
        "'${model.program_desc}',"
        "'${model.rehab_type}',"
        "'${model.rehab_type_desc}',"
        "'${model.reference_no}',"
        "'${model.appl_contrac_no}',"
        "'${model.installment_type}',"
        "'${model.installment_type_desc}',"
        "'${model.appl_top}',"
        "'${model.payment_method}',"
        "'${model.payment_method_desc}',"
        "'${model.eff_rate}',"
        "'${model.flat_rate}',"
        "'${model.object_price}',"
        "'${model.karoseri_price}',"
        "'${model.total_amt}',"
        "'${model.dp_net}',"
        "'${model.installment_decline_n}',"
        "'${model.payment_per_year}',"
        "'${model.dp_branch}',"
        "'${model.dp_gross}',"
        "'${model.principal_amt}',"
        "'${model.installment_paid}',"
        "'${model.interest_amt}',"
        "'${model.ltv}',"
        "'${model.disbursement}',"
        "'${model.dsr}',"
        "'${model.dir}',"
        "'${model.dsc}',"
        "'${model.irr}',"
        "'${model.gp_type}',"
        "'${model.new_tenor}',"
        "'${model.total_stepping}',"
        "'${model.balloon_type}',"
        "'${model.last_percen_installment}',"
        "'${model.last_installment_amt}',"
        "'${model.is_auto_debit}',"
        "'${model.bank_id}',"
        "'${model.account_no}',"
        "'${model.account_behalf}',"
        "'${model.purpose_type}',"
        "'${model.dp_source}',"
        "'${model.va_no}',"
        "'${model.va_amt}',"
        "'${model.disburse_purpose}',"
        "'${model.disburse_type}',"
        "'${model.appl_objt_last_state}',"
        "'${model.appl_objt_next_state}',"
        "'${model.created_date}',"
        "'${model.created_by}',"
        "'${model.modified_date}',"
        "'${model.modified_by}',"
        "'${model.active}',"
        "'${model.tact}',"
        "'${model.tmax}',"
        "'${model.appl_send_flag}',"
        "'${model.is_without_colla}',"
        "'${model.first_installment}',"
        "'${model.flag_save_karoseri}',"
        "'${model.pelunasan_amt}',"
        "'${model.appl_withdrawal_flag}',"
        "'${model.appl_model_dtl}',"
        "'${model.insentif_sales}',"
        "'${model.insentif_payment}',"
        "'${model.nik}',"
        "'${model.nama}',"
        "'${model.jabatan}',"
        "'${model.outlet_id}',"
        "'${model.group_id}',"
        "'${model.dealer_matrix}',"
        "'${model.ppd_date}',"
        "'${model.ppd_cancel_date}',"
        "'${model.dp_chasis_net}',"
        "'${model.dp_chasis_gross}',"
        "'${model.ujrah_price}',"
        "'${model.total_dp_krs}')"
    );
//    db.close();
  }

  Future<bool> deleteMS2ApplObject() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_APPL_OBJECT where appl_objt_id = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_APPL_OBJECT where appl_objt_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }


  void updateMS2ApplObjectInfStrukturKredit (MS2ApplObjectModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawUpdate("UPDATE MS2_APPL_OBJECT SET "
      "SET installment_paid = '${model.installment_paid}',"
      "SET installment_type_desc = '${model.installment_type_desc}',"
      "SET appl_top = '${model.appl_top}',"
      "SET payment_method = '${model.payment_method}',"
      "SET payment_method_desc = '${model.payment_method_desc}',"
      "SET eff_rate = '${model.eff_rate}',"
      "SET flat_rate = '${model.flat_rate}',"
      "SET object_price = '${model.object_price}',"
      "SET karoseri_price = '${model.karoseri_price}',"
      "SET total_amt = '${model.total_amt}',"
      "SET dp_net = '${model.dp_net}',"
      "SET dp_branch = '${model.dp_branch}',"
      "SET dp_gross = '${model.dp_gross}',"
      "SET principal_amt = '${model.principal_amt}',"
      "SET installment_paid = '${model.installment_paid}',"
      "SET interest_amt = '${model.interest_amt}',"
      "SET ltv = '${model.ltv}',"
      "SET modified_date = '${model.modified_date}',"
      "SET modified_by = '${model.modified_by}',"
      "WHERE appl_objt_id = $orderNo"
    );
   // db.close();
  }

  // Inf Struktur Kredit BP
  void updateMS2ApplObjectInfStrukturKreditGP (MS2ApplObjectModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawUpdate("UPDATE MS2_APPL_OBJECT SET "
        "SET gp_type = '${model.gp_type}',"
        "SET new_tenor = '${model.new_tenor}',"
        "WHERE appl_objt_id = 1"
    );
    // db.close();
  }

  // Inf Struktur Kredit Stepping
  void updateMS2ApplObjectInfStrukturStepping (MS2ApplObjectModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawUpdate("UPDATE MS2_APPL_OBJECT SET "
        "SET total_stepping = '${model.total_stepping}',"
        "WHERE appl_objt_id = 1"
    );
    // db.close();
  }

  // Inf Struktur Kredit BP
  void updateMS2ApplObjectInfStrukturKreditBP (MS2ApplObjectModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawUpdate("UPDATE MS2_APPL_OBJECT SET "
        "SET balloon_type = '${model.balloon_type}',"
        "SET last_percen_installment = '${model.last_percen_installment}',"
        "SET last_installment_amt = '${model.last_installment_amt}',"
        "SET modified_date = '${model.modified_date}',"
        "SET modified_by = '${model.modified_by}',"
        "WHERE appl_objt_id = 1"
    );
    // db.close();
  }

  // Inf Struktur Kredit Income
  void updateMS2ApplObjectInfStrukturKreditIncome (MS2ApplObjectModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawUpdate("UPDATE MS2_APPL_OBJECT SET "
        "SET dsr = '${model.dsr}',"
        "SET dsr = '${model.dir}',"
        "SET dsr = '${model.dsc}',"
        "SET dsr = '${model.irr}',"
        "WHERE appl_objt_id = 1"
    );
    // db.close();
  }

  // Inf Colla Oto
  void insertMS2ApplObjtCollOto (MS2ApplObjtCollOtoModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
      "INSERT INTO MS2_APPL_OBJT_COLL_OTO (coll_oto_id, flag_multi_coll, flag_co, flag_coll_na, id_type, "
        "id_type_desc, id_no, colla_name, date_of_birth, place_of_birth, "
        "place_of_birth_kabkota, place_of_birth_kabkota_desc, group_object, group_object_desc, object, "
        "object_desc, brand_object, brand_object_desc, object_type, object_type_desc, "
        "object_model, object_model_desc, object_purpose, object_purpose_desc, mfg_year, "
        "istration_year, yellow_plate, built_up, bpkb_no, frame_no, "
        "engine_no, police_no, grade_unit, utj_facilities, bidder_name, "
        "showroom_price, add_equip, mp_adira, rek, result, "
        "ltv, dp_jaminan, max_ph, taksasi_price, cola_purpose, "
        "cola_purpose_desc, capacity, color, made_in, made_in_desc, "
        "serial_no, no_invoice, stnk_valid, bpkb_address, active, "
        "created_date, created_by, modified_date, modified_by, bpkb_id_type, bpkb_id_type_desc, mp_adira_upld)"
        "VALUES("
        "'$orderNo',"
        "'${model.flag_multi_coll}',"
        "'${model.flag_coll_is_unit}',"
        "'${model.flag_coll_name_is_appl}',"
        "'${model.id_type}',"
        "'${model.id_type_desc}',"
        "'${model.id_no}',"
        "'${model.colla_name}',"
        "'${model.date_of_birth}',"
        "'${model.place_of_birth}',"
        "'${model.place_of_birth_kabkota}',"
        "'${model.place_of_birth_kabkota_desc}',"
        "'${model.group_object}',"
        "'${model.group_object_desc}',"
        "'${model.object}',"
        "'${model.object_desc}',"
        "'${model.brand_object}',"
        "'${model.brand_object_desc}',"
        "'${model.object_type}',"
        "'${model.object_type_desc}',"
        "'${model.object_model}',"
        "'${model.object_model_desc}',"
        "'${model.object_purpose}',"
        "'${model.object_purpose_desc}',"
        "'${model.mfg_year}',"
        "'${model.registration_year}',"
        "'${model.yellow_plate}',"
        "'${model.built_up}',"
        "'${model.bpkp_no}',"
        "'${model.frame_no}',"
        "'${model.engine_no}',"
        "'${model.police_no}',"
        "'${model.grade_unit}',"
        "'${model.utj_facilities}',"
        "'${model.bidder_name}',"
        "'${model.showroom_price}',"
        "'${model.add_equip}',"
        "'${model.mp_adira}',"
        "'${model.rekondisi_fisik}',"
        "'${model.result}',"
        "'${model.ltv}',"
        "'${model.dp_jaminan}',"
        "'${model.max_ph}',"
        "'${model.taksasi_price}',"
        "'${model.cola_purpose}',"
        "'${model.cola_purpose_desc}',"
        "'${model.capacity}',"
        "'${model.color}',"
        "'${model.made_in}',"
        "'${model.made_in_desc}',"
        "'${model.serial_no}',"
        "'${model.no_invoice}',"
        "'${model.stnk_valid}',"
        "'${model.bpkp_address}',"
        "'${model.active}',"
        "'${model.created_date}',"
        "'${model.created_by}',"
        "'${model.modified_date}',"
        "'${model.modified_by}',"
        "'${model.bpkb_id_type}',"
        "'${model.bpkb_id_type_desc}',"
        "'${model.mp_adira_upld}')"
    );
    // db.close();
  }

  //delete colla oto
  Future<bool> deleteApplCollaOto() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_APPL_OBJT_COLL_OTO where colla_oto_id = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_APPL_OBJT_COLL_OTO where colla_oto_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
  }

  // Inf Colla Properti
  void insertMS2ApplObjtCollProp (MS2ApplObjtCollPropModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
      "INSERT INTO MS2_APPL_OBJT_COLL_PROP (colla_prop_id, flag_multi_coll, flag_coll_name_is_appl, id_type, id_type_desc, "
        "id_no, cola_name, date_of_birth, place_of_birth, place_of_birth_kabkota, "
        "place_of_birth_kabkota_desc, sertifikat_no, seritifikat_type, seritifikat_type_desc, property_type, "
        "property_type_desc, building_area, land_area, dp_guarantee, max_ph, "
        "taksasi_price, cola_purpose, cola_purpose_desc, jarak_fasum_positif, jarak_fasum_negatif, "
        "land_price, njop_price, building_price, roof_type, roof_type_desc, "
        "wall_type, wall_type_desc, floor_type, floor_type_desc, foundation_type, "
        "foundation_type_desc, road_type, road_type_desc, flag_car_pas, no_of_house, "
        "guarantee_character, grntr_evdnce_ownr, pblsh_sertifikat_date, pblsh_sertifikat_year, license_name, "
        "no_srt_ukur, tgl_srt_ukur, srtfkt_pblsh_by, expire_right, imb_no, "
        "imb_date, size_of_imb, ltv, land_location, active, "
        "created_date, created_by, modified_date, modified_by)"
        "VALUES("
        "'$orderNo',"
        "'${model.flag_multi_coll}',"
        "'${model.flag_coll_name_is_appl}',"
        "'${model.id_type}',"
        "'${model.id_type_desc}',"
        "'${model.id_no}',"
        "'${model.cola_name}',"
        "'${model.date_of_birth}',"
        "'${model.place_of_birth}',"
        "'${model.place_of_birth_kabkota}',"
        "'${model.place_of_birth_kabkota_desc}',"
        "'${model.sertifikat_no}',"
        "'${model.seritifikat_type}',"
        "'${model.seritifikat_type_desc}',"
        "'${model.property_type}',"
        "'${model.property_type_desc}',"
        "'${model.building_area}',"
        "'${model.land_area}',"
        "'${model.dp_guarantee}',"
        "'${model.max_ph}',"
        "'${model.taksasi_price}',"
        "'${model.cola_purpose}',"
        "'${model.cola_purpose_desc}',"
        "'${model.jarak_fasum_positif}',"
        "'${model.jarak_fasum_negatif}',"
        "'${model.land_price}',"
        "'${model.njop_price}',"
        "'${model.building_price}',"
        "'${model.roof_type}',"
        "'${model.roof_type_desc}',"
        "'${model.wall_type}',"
        "'${model.wall_type_desc}',"
        "'${model.floor_type}',"
        "'${model.floor_type_desc}',"
        "'${model.foundation_type}',"
        "'${model.foundation_type_desc}',"
        "'${model.road_type}',"
        "'${model.road_type_desc}',"
        "'${model.flag_car_pas}',"
        "'${model.no_of_house}',"
        "'${model.guarantee_character}',"
        "'${model.grntr_evdnce_ownr}',"
        "'${model.pblsh_sertifikat_date}',"
        "'${model.pblsh_sertifikat_year}',"
        "'${model.license_name}',"
        "'${model.no_srt_ukur}',"
        "'${model.tgl_srt_ukur}',"
        "'${model.srtfkt_pblsh_by}',"
        "'${model.expire_right}',"
        "'${model.imb_no}',"
        "'${model.imb_date}',"
        "'${model.size_of_imb}',"
        "'${model.ltv}',"
        "'${model.land_location}',"
        "'${model.active}',"
        "'${model.created_date}',"
        "'${model.created_by}',"
        "'${model.modified_date}',"
        "'${model.modified_by}')"
    );
    // db.close();
  }

  //delete colla property
  Future<bool> deleteApplCollaProp() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_APPL_OBJT_COLL_PROP where colla_prop_id = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_APPL_OBJT_COLL_PROP where colla_prop_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
  }

  // Inf Asuransi Utama
  void insertMS2ApplInsuranceMain (List<MS2ApplInsuranceModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_APPL_INSURANCE (insurance_id, cola_type, insurance_type, company_id, product, "
          "period, type, type_1, type_2, sub_type, "
          "sub_type_coverage, coverage_type, coverage_amt, upper_limit_pct, upper_limit_amt, "
          "lower_limit_pct, lower_limit_amt, cash_amt, credit_amt, total_split, "
          "total_pct, total_amt, created_date, created_by, modified_date, "
          "modified_by, active, idx)"
          "VALUES("
          "'$orderNo',"
          "'${model[i].cola_type}',"
          "'${model[i].insurance_type}',"
          "'${model[i].company_id}',"
          "'${model[i].product}',"
          "'${model[i].period}',"
          "'${model[i].type}',"
          "'${model[i].type_1}',"
          "'${model[i].type_2}',"
          "'${model[i].sub_type}',"
          "'${model[i].sub_type_coverage}',"
          "'${model[i].coverage_type}',"
          "'${model[i].coverage_amt}',"
          "'${model[i].upper_limit_pct}',"
          "'${model[i].upper_limit_amt}',"
          "'${model[i].lower_limit_pct}',"
          "'${model[i].lower_limit_amt}',"
          "'${model[i].cash_amt}',"
          "'${model[i].credit_amt}',"
          "'${model[i].total_split}',"
          "'${model[i].total_pct}',"
          "'${model[i].total_amt}',"
          "'${model[i].created_date}',"
          "'${model[i].created_by}',"
          "'${model[i].modified_date}',"
          "'${model[i].modified_by}',"
          "'${model[i].active}',"
          "'${model[i].idx}',"
      );
    }
//    db.close();
  }

  // Inf Asuransi Tambahan
  void insertMS2ApplInsuranceAdditional (List<MS2ApplInsuranceModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_APPL_INSURANCE (insurance_id, cola_type, insurance_type, company_id, product, "
          "period, type, type_1, type_2, sub_type, "
          "sub_type_coverage, coverage_type, coverage_amt, upper_limit_pct, upper_limit_amt, "
          "lower_limit_pct, lower_limit_amt, cash_amt, credit_amt, total_split, "
          "total_pct, total_amt, created_date, created_by, modified_date, "
          "modified_by, active, idx)"
          "VALUES("
          "'$orderNo',"
          "'${model[i].cola_type}',"
          "'${model[i].insurance_type}',"
          "'${model[i].company_id}',"
          "'${model[i].product}',"
          "'${model[i].period}',"
          "'${model[i].type}',"
          "'${model[i].type_1}',"
          "'${model[i].type_2}',"
          "'${model[i].sub_type}',"
          "'${model[i].sub_type_coverage}',"
          "'${model[i].coverage_type}',"
          "'${model[i].coverage_amt}',"
          "'${model[i].upper_limit_pct}',"
          "'${model[i].upper_limit_amt}',"
          "'${model[i].lower_limit_pct}',"
          "'${model[i].lower_limit_amt}',"
          "'${model[i].cash_amt}',"
          "'${model[i].credit_amt}',"
          "'${model[i].total_split}',"
          "'${model[i].total_pct}',"
          "'${model[i].total_amt}',"
          "'${model[i].created_date}',"
          "'${model[i].created_by}',"
          "'${model[i].modified_date}',"
          "'${model[i].modified_by}',"
          "'${model[i].active}',"
          "'${model[i].idx}',"
      );
    }
    // db.close();
  }

  // Inf WMP
  void insertMS2ApplObjtWmp (List<MS2ApplObjtWmpModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i=0; i<model.length; i++){
      await db.rawInsert(
          "INSERT INTO MS2_APPL_OBJT_WMP(wmp_id, wmp_no, wmp_type, wmp_type_desc, wmp_subsidy_type_id, wmp_subsidy_type_id_desc, "
              "max_refund_amt, active, created_date, created_by) "
              "VALUES("
              "'$orderNo',"
              "${model[i].wmp_no},"
              "${model[i].wmp_type},"
              "${model[i].wmp_type_desc},"
              "${model[i].wmp_subsidy_type_id},"
              "${model[i].wmp_subsidy_type_desc},"
              "${model[i].max_refund_amt},"
              "1,"
              "'$dateTime',"
              "'$createdBy')"
      );
    }
    // db.close();
  }

  Future<bool> deleteMS2ApplOBJWmp() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_APPL_OBJECT where wmp_id = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_APPL_OBJT_WMP where wmp_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
  }

  // Inf Kredit Subsidi Detail
  void insertMS2ApplRefund (List<MS2ApplRefundModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_APPL_REFUND (order_no, giver_refund, type_refund, type_refund_desc, deduction_method, "
          "deduction_method_desc, refund_amt, eff_rate_bef, flat_rate_bef, dp_real, "
          "installment_amt, active, created_date, created_by, interest_rate, refund_amt_klaim)"
          "VALUES("
          "'$orderNo',"
          "'${model[i].giver_refund}',"
          "'${model[i].type_refund}',"
          "'${model[i].type_refund_desc}',"
          "'${model[i].deduction_method}',"
          "'${model[i].deduction_method_desc}',"
          "'${model[i].refund_amt}',"
          "'${model[i].eff_rate_bef}',"
          "'${model[i].flat_rate_bef}',"
          "'${model[i].dp_real}',"
          "'${model[i].installment_amt}',"
          "1,"
          "'$dateTime',"
          "'$createdBy',"
          "'${model[i].interest_rate}',"
          "'${model[i].refund_amt_klaim}')"
      );
      if(model[i].installmentDetail != null){
        var _lastInsert = await db.rawQuery('SELECT last_insert_rowid()');
        var _lastID = _lastInsert[0]['last_insert_rowid()'];
        for(int j=0; j<model[i].installmentDetail.length; j++){
          insertMS2ApplRefundDetail(MS2ApplRefundDetailModel(
              "$orderNo",
              int.parse(model[i].installmentDetail[j].installmentIndex),
              double.parse(model[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "")),
              null,
              null,
              null,
              null,
              null
          ), _lastID.toString());
        }
      }
    }
    // db.close();
  }

  void insertMS2ApplRefundDetail (MS2ApplRefundDetailModel model, String lastID) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert("INSERT INTO MS2_APPL_REFUND_DETAIL (subsidy_id, ac_installment_no, ac_installment_amt, "
        "created_date, created_by, active)"
        "VALUES("
        "'$lastID',"
        "'${model.ac_installment_no}',"
        "'${model.ac_installment_amt}',"
        "'$dateTime',"
        "'$createdBy',"
        "'1')"
    );
  }

  // delete header credit subsidy
  Future<bool> deleteMS2ApplRefundHeader() async{
    setOrderNumber();
    bool _isDelete = false;
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);

    var _lastInsert = await db.rawQuery("SELECT subsidy_id from MS2_APPL_REFUND where order_no = " + orderNo);
    if(_lastInsert.isNotEmpty){
      var _lastID = _lastInsert[0]['subsidy_id'];
      deleteMS2ApplRefundDetail(_lastID.toString());
      await db.rawQuery("delete from MS2_APPL_REFUND where order_no = " + orderNo);
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
    // db.close();
  }

  void deleteMS2ApplRefundDetail(String id) async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_APPL_REFUND_DETAIL where subsidy_id = " + id);
  }

  // Taksasi Unit
  void insertMS2ApplTaksasi (List<MS2ApplTaksasiModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i=0; i < model.length; i++){
      await db.rawInsert(
          "INSERT INTO MS2_APPL_TAKSASI (order_no, unit_type, unit_type_desc, taksasi_code, taksasi_desc, "
              "nilai_input_rekondisi, created_date, created_by, modified_date, modified_by, "
              "active)"
              "VALUES("
              "'$orderNo',"
              "'${model[i].unit_type}',"
              "'${model[i].unit_type_desc}',"
              "'${model[i].taksasi_code}',"
              "'${model[i].taksasi_desc}',"
              "'${model[i].nilai_input_rekondisi}',"
              "'${model[i].created_date}',"
              "'${model[i].created_by}',"
              "'${model[i].modified_date}',"
              "'${model[i].modified_by}',"
              "'${model[i].active}',"
      );
    }
    // db.close();
  }

  Future<bool> deleteMS2ApplTaksasi() async{
    setOrderNumber();
    bool _isDelete = false;
    var _check = await db.rawQuery("SELECT * from MS2_APPL_TAKSASI where order_no = '$orderNo'");
    print(_check);
    print(orderNo);
    if(_check.isNotEmpty){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_APPL_TAKSASI where order_no = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
  }

  void insertMS2SvyAssgnmt (MS2SvyAssgnmtModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
      "INSERT INTO MS2_SVY_ASSGNMT (order_no, survey_type_id, empl_nik, handphone_no, janji_survey_date, "
        "survey_reason, process_survey, flag_brms, status_survey, active, "
        "created_date, created_by, modified_date, modified_by, survey_result, "
        "survey_recomendation, notes, survey_result_date, distance_sentra, distance_deal, "
        "distance_objt_purp_sentra, status_eligible_cfo, job_surveyor)"
        "VALUES("
          "'$orderNo',"
          "'${model.survey_type_id}',"
          "'${model.empl_nik}',"
          "'${model.handphone_no}',"
          "'${model.janji_survey_date}',"
          "'${model.survey_reason}',"
          "'${model.process_survey}',"
          "'${model.flag_brms}',"
          "'${model.status_survey}',"
          "'${model.active}',"
          "'${model.created_date}',"
          "'${model.created_by}',"
          "'${model.modified_date}',"
          "'${model.modified_by}',"
          "'${model.survey_result}',"
          "'${model.survey_recomendation}',"
          "'${model.notes}',"
          "'${model.survey_result_date}',"
          "'${model.distance_sentra}',"
          "'${model.distance_deal}',"
          "'${model.distance_objt_purp_sentra}',"
          "'${model.status_eligible_cfo}',"
          "'${model.job_surveyor}',"
    );
    // db.close();
  }

  // Hasil Survey - Survey Detail
  void insertMS2SvyRsltDtl (List<MS2SvyRsltDtlModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i = 0; i < model.length; i++) {
      await db.rawInsert(
        "INSERT INTO MS2_SVY_RSLT_DTL (order_no, info_envirnmt, info_source, info_source_name, active, "
          "created_date, created_by, modified_date, modified_by)"
          "VALUES("
          "'$orderNo',"
          "'${model[i].info_envirnmt}',"
          "'${model[i].info_source}',"
          "'${model[i].info_source_name}',"
          "'${model[i].active}',"
          "'${model[i].created_date}',"
          "'${model[i].created_by}',"
          "'${model[i].modified_date}',"
          "'${model[i].modified_by}',"
      );
    }
//    db.close();
  }

  // Hasil Survey - Aset
  void insertMS2SvyRsltAsst (List<MS2SvyRsltAsstModel> model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    for(int i=0; i <model.length; i++){
      await db.rawInsert(
          "INSERT INTO MS2_SVY_RSLT_DTL (ORDER_NO, asset_type, asset_amt, asset_own, size_of_land, "
              "street_type, electricity, electricity_bill, expired_date_contract, no_of_stay, "
              "active, created_date, created_by, modified_date, modified_by, )"
              "VALUES("
              "'$orderNo',"
              "'${model[i].asset_type}',"
              "'${model[i].asset_amt}',"
              "'${model[i].asset_own}',"
              "'${model[i].size_of_land}',"
              "'${model[i].street_type}',"
              "'${model[i].electricity}',"
              "'${model[i].electricity_bill}',"
              "'${model[i].expired_date_contract}',"
              "'${model[i].no_of_stay}',"
              "'${model[i].active}',"
              "'${model[i].created_date}',"
              "'${model[i].created_by}',"
              "'${model[i].modified_date}',"
              "'${model[i].modified_by}',"
      );
    }
    // db.close();
  }


  // save last step
  void saveLastStep(int index) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _orderDate = _preferences.getString("order_date");
    String _custName = _preferences.getString("cust_name");
    String _lastKnownState = _preferences.getString("last_known_state");
    String _user = _preferences.getString("username");
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert("INSERT INTO MS2_TASK_LIST "
        "(order_no,order_date,cust_name,last_known_state,last_known_handled_by,idx,source_application)"
        " VALUES('$orderNo','$_orderDate','$_custName','$_lastKnownState','$_user','$index','AD1MS2')");
  }


  //delete save last step
  Future<bool> deleteSaveLastStep() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawDelete("delete from MS2_TASK_LIST where order_no = '$orderNo'");
      _checkData = await db.rawQuery("SELECT * FROM MS2_TASK_LIST WHERE Order_no = '$orderNo'");
    }
    // List _checkData = await db.rawQuery("SELECT * FROM MS2_TASK_LIST WHERE order_no = '$orderNo'");
    return _checkData.isEmpty;
  }

  // insert dedup data
  void insertDataDedup(DataDedupModel model,String orderNo) async{
    var _motherName = model.nama_gadis_ibu_kandung != null ? "${model.nama_gadis_ibu_kandung}" : "";
    var _birthPlace = model.tempat_lahir_sesuai_identitas != null ? "${model.tempat_lahir_sesuai_identitas}" : "";
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert("INSERT INTO MS2_DEDUP "
        "(order_no,flag,no_identitas,nama,tanggal_lahir,tempat_lahir_sesuai_identitas,nama_gadis_ibu_kandung,alamat,created_date,created_by,updated_date,updated_by)"
        " VALUES("
        "'$orderNo',"
        "'${model.no_identitas}',"
        "'${model.flag}',"
        "'${model.nama}',"
        "'${model.tanggal_lahir}',"
        "'$_birthPlace',"
        "'$_motherName',"
        "'${model.alamat}',"
        "'${model.created_date}',"
        "'${model.created_by}',"
        "'${model.updated_date}',"
        "'${model.updated_by}')");
  }


  //select data dedup
  Future<List> selectDataInfoNasabahFromDataDedup() async{
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
    }
    return await db.rawQuery("SELECT * FROM MS2_DEDUP WHERE order_no = '$orderNo'");
  }

  //select data info nasabah
  Future<List> selectDataInfoNasabah() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_CUSTOMER_PERSONAL WHERE order_no = '$orderNo'");
      db.close();
    }
    return _data;
  }

  //select data info keluarga
  Future<List> selectDataInfoKeluarga(String relationStatusId) async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      if(relationStatusId == "05"){
        _data = await db.rawQuery("SELECT * FROM MS2_CUST_FAMILY WHERE order_no = '$orderNo' AND relation_stauts = '$relationStatusId'");
      }
      else{
        _data = await db.rawQuery("SELECT * FROM MS2_CUST_FAMILY WHERE order_no = '$orderNo' AND relation_stauts != '05'");
      }
    }
    return _data;
  }

  //select data info nasabah NPWP
  Future<List> selectDataInfoNasabahNPWP() async{
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
    }
    return await db.rawQuery("SELECT * FROM MS2_CUSTOMER WHERE order_no = '$orderNo'");
  }

  //select data pendapatan
  Future<List> selectDataIncome() async{
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
    }
    return await db.rawQuery("SELECT * FROM MS2_CUST_INCOME WHERE order_no = '$orderNo'");
  }
}
