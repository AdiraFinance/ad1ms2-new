import 'dart:io';

class ImageFileModel {
  final File imageFile;
  final double latitude;
  final double longitude;
  final String path;

  ImageFileModel(this.imageFile, this.latitude, this.longitude, this.path);
}

class OccupationModel {
  final String KODE;
  final String DESKRIPSI;

  OccupationModel(this.KODE, this.DESKRIPSI);
}

class KegiatanUsahaModel {
  final int id;
  final String text;

  KegiatanUsahaModel(this.id, this.text);
}

class JenisKegiatanUsahaModel {
  final int id;
  final String text;

  JenisKegiatanUsahaModel(this.id, this.text);
}

class JenisKonsepModel{
  final String id;
  final String text;

  JenisKonsepModel(this.id, this.text);
}
