import 'package:ad1ms2_dev/models/product_model.dart';
import 'company_model.dart';
import 'insurance_type_model.dart';

class AdditionalInsuranceModel{
    final InsuranceTypeModel insuranceType;
    final CompanyModel company;
    final ProductModel product;
    final String periodType;
    final String coverageType;
    final String coverageValue;
    final String upperLimitRate;
    final String lowerLimitRate;
    final String upperLimit;
    final String lowerLimit;
    final String priceCash;
    final String priceCredit;
    final String totalPriceRate;
    final String totalPrice;

    AdditionalInsuranceModel(this.insuranceType, this.company, this.product, this.periodType, this.coverageType, this.coverageValue, this.upperLimitRate, this.lowerLimitRate, this.upperLimit, this.lowerLimit, this.priceCash, this.priceCredit, this.totalPriceRate, this.totalPrice);
}