class FoundationTypeModel{
  final String id;
  final String name;

  FoundationTypeModel(this.id, this.name);
}