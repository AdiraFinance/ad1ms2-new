class MS2CustIncomeModel {
  final String cust_income_id;
  final String type_income_frml;
  final String income_type;
  final String income_desc;
  final String income_value;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final int idx;

  MS2CustIncomeModel(
      this.cust_income_id,
      this.type_income_frml,
      this.income_type,
      this.income_desc,
      this.income_value,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      this.idx,
      );
}
