class ElectricityTypeModel{
  final String PARA_ELECTRICITY_ID;
  final String PARA_ELECTRICITY_NAME;

  ElectricityTypeModel(this.PARA_ELECTRICITY_ID, this.PARA_ELECTRICITY_NAME);
}