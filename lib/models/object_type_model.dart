class ObjectTypeModel {
  final String id;
  final String name;

  ObjectTypeModel(this.id, this.name);
}
