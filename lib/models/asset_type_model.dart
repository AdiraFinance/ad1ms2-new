class AssetTypeModel{
  final String id;
  final String name;

  AssetTypeModel(this.id, this.name);
}