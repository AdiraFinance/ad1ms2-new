import 'package:ad1ms2_dev/models/asset_type_model.dart';
import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/ownership_model.dart';
import 'package:ad1ms2_dev/models/road_type_model.dart';

class MS2SvyRsltAsstModel {
  final String ORDER_NO;
  final AssetTypeModel asset_type;
  final String asset_amt;
  final OwnershipModel asset_own;
  final String size_of_land;
  final RoadTypeModel street_type;
  final ElectricityTypeModel electricity;
  final double electricity_bill;
  final String expired_date_contract;
  final int no_of_stay;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;

  MS2SvyRsltAsstModel(
    this.ORDER_NO,
    this.asset_type,
    this.asset_amt,
    this.asset_own,
    this.size_of_land,
    this.street_type,
    this.electricity,
    this.electricity_bill,
    this.expired_date_contract,
    this.no_of_stay,
    this.active,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
  );
}
