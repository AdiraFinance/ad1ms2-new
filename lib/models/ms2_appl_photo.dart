import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/shared/document_unit_model.dart';
import 'package:ad1ms2_dev/shared/group_unit_object_model.dart';

import 'form_m_foto_model.dart';

class MS2ApplPhotoModel{
    List<ImageFileModel> listFotoTempatTinggal;
    OccupationModel occupation;
    List<ImageFileModel> listFotoTempatUsaha;
    FinancingTypeModel financial;
    KegiatanUsahaModel kegiatanUsaha;
    JenisKegiatanUsahaModel jenisKegiatanUsaha;
    JenisKonsepModel jenisKonsep;
    List<GroupUnitObjectModel> listGroupUnitObject;
    List<DocumentUnitModel> listDocument;

    MS2ApplPhotoModel(
      this.listFotoTempatTinggal,
      this.occupation,
      this.listFotoTempatUsaha,
      this.financial,
      this.kegiatanUsaha,
      this.jenisKegiatanUsaha,
      this.jenisKonsep,
      this.listGroupUnitObject,
      this.listDocument);
}