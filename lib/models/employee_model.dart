class EmployeeModel {
  final String id;
  final String name;

  EmployeeModel(this.id, this.name);
}
