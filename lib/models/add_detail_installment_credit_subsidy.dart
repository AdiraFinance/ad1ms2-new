class AddInstallmentDetailModel{
    final String installmentIndex;
    final String installmentSubsidy;

    AddInstallmentDetailModel(this.installmentIndex, this.installmentSubsidy);
}