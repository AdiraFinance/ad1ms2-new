import 'package:ad1ms2_dev/models/add_detail_installment_credit_subsidy.dart';

class MS2ApplRefundModel {
  final String subsidy_id;
  final String giver_refund;
  final String type_refund;
  final String type_refund_desc;
  final String deduction_method;
  final String deduction_method_desc;
  final double refund_amt;
  final int eff_rate_bef;
  final int flat_rate_bef;
  final int dp_real;
  final int installment_amt;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String interest_rate;
  final int refund_amt_klaim;
  final List<AddInstallmentDetailModel> installmentDetail;

  MS2ApplRefundModel (
      this.subsidy_id,
      this.giver_refund,
      this.type_refund,
      this.type_refund_desc,
      this.deduction_method,
      this.deduction_method_desc,
      this.refund_amt,
      this.eff_rate_bef,
      this.flat_rate_bef,
      this.dp_real,
      this.installment_amt,
      this.active,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.interest_rate,
      this.refund_amt_klaim, this.installmentDetail,
      );
}