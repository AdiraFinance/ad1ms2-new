import 'package:ad1ms2_dev/models/asset_type_model.dart';
import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/ownership_model.dart';
import 'package:ad1ms2_dev/models/road_type_model.dart';

class ResultSurveyAssetModel {
  final AssetTypeModel assetTypeModel;
  final String valueAsset;
  final OwnershipModel ownershipModel;
  final String surfaceBuildingArea;
  final RoadTypeModel roadTypeModel;
  final ElectricityTypeModel electricityTypeModel;
  final String electricityBills;
  final DateTime endDateLease;
  final String lengthStay;

  ResultSurveyAssetModel(
      this.assetTypeModel,
      this.valueAsset,
      this.ownershipModel,
      this.surfaceBuildingArea,
      this.roadTypeModel,
      this.electricityTypeModel,
      this.electricityBills,
      this.endDateLease,
      this.lengthStay);
}
