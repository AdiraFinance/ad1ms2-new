import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';

class FormMInfoKelModel {
  final IdentityModel identityModel;
  final RelationshipStatusModel relationshipStatusModel;
  final String noIdentitas;
  final String namaLengkapSesuaiIdentitas;
  final String namaLengkap;
  final String birthDate;
  final String gender;
  final String kodeArea;
  final String noTlpn;
  final String tmptLahirSesuaiIdentitas;
  final BirthPlaceModel birthPlaceModel;
  final String noHp;

  FormMInfoKelModel(
      this.identityModel,
      this.relationshipStatusModel,
      this.noIdentitas,
      this.namaLengkapSesuaiIdentitas,
      this.namaLengkap,
      this.birthDate,
      this.gender,
      this.kodeArea,
      this.noTlpn,
      this.tmptLahirSesuaiIdentitas,
      this.noHp, this.birthPlaceModel);
}
