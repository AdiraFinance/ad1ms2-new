class EmployeeHeadModel {
  final String id;
  final String employeeHeadName;

  EmployeeHeadModel(this.id, this.employeeHeadName);
}
