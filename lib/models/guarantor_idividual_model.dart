import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';

import 'form_m_informasi_alamat_model.dart';

class GuarantorIndividualModel {
  final RelationshipStatusModel relationshipStatusModel;
  final IdentityModel identityModel;
  final String identityNumber;
  final String fullNameIdentity;
  final String fullName;
  final DateTime birthDate;
  final String birthPlaceIdentity1;
  final String birthPlaceIdentity2;
  final String gender;
  final String cellPhoneNumber;
  final List<AddressModel> listAddressGuarantorModel;

  GuarantorIndividualModel(
      this.relationshipStatusModel,
      this.identityModel,
      this.identityNumber,
      this.fullNameIdentity,
      this.fullName,
      this.birthDate,
      this.birthPlaceIdentity1,
      this.birthPlaceIdentity2,
      this.listAddressGuarantorModel, this.cellPhoneNumber, this.gender);
}

class GuarantorCompanyModel {
  final TypeInstitutionModel typeInstitutionModel;
  final ProfilModel profilModel;
  final String establishDate;
  final String institutionName;
  final String npwp;
  final List<AddressGuarantorModelCompany> listAddressGuarantorModel;

  GuarantorCompanyModel(this.typeInstitutionModel, this.profilModel, this.establishDate,
      this.institutionName, this.npwp, this.listAddressGuarantorModel);
}
