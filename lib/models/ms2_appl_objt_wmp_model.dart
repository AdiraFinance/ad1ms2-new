class MS2ApplObjtWmpModel {
  final String wmp_id;
  final String wmp_no;
  final String wmp_type;
  final String wmp_type_desc;
  final String wmp_subsidy_type_id;
  final String wmp_subsidy_type_desc;
  final double max_refund_amt;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;

  MS2ApplObjtWmpModel (
      this.wmp_id,
      this.wmp_no,
      this.wmp_type,
      this.wmp_type_desc,
      this.wmp_subsidy_type_id,
      this.wmp_subsidy_type_desc,
      this.max_refund_amt,
      this.active,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      );
}