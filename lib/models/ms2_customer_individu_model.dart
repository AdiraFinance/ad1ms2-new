import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';

class MS2CustomerIndividuModel {
  final String order_no;
  final int no_of_liability;
  final GCModel gcModel;
  final EducationModel educationModel;
  final IdentityModel identityModel;
  final String id_no;
  final String full_name_id;
  final String full_name;
  final String alias_name;
  final String degree;
  final String date_of_birth;
  final String place_of_birth;
  final BirthPlaceModel birthPlaceModel;
  final String gender;
  final String gender_desc;
  final String id_date;
  final String id_expired_date;
  final ReligionModel religionModel;
  final MaritalStatusModel maritalStatusModel;
  final String handphone_no;
  final String email;
  final String facebook;
  final String bb_pin;
  final String kk_no;
  final String fav_color;
  final String fav_brand;
  final String relation_status_emg;
  final String full_name_id_emg;
  final String full_name_emg;
  final String degree_emg;
  final String email_emg;
  final String handphone_emg;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final int flag_id_lifetime;
  final String no_wa;
  final String no_wa_2;
  final String no_wa_3;

  MS2CustomerIndividuModel(
      this.order_no,
      this.no_of_liability,
      this.gcModel,
      this.educationModel,
      this.identityModel,
      this.id_no,
      this.full_name_id,
      this.full_name,
      this.alias_name,
      this.degree,
      this.date_of_birth,
      this.place_of_birth,
      this.birthPlaceModel,
      this.gender,
      this.gender_desc,
      this.id_date,
      this.id_expired_date,
      this.religionModel,
      this.maritalStatusModel,
      this.handphone_no,
      this.email,
      this.facebook,
      this.bb_pin,
      this.kk_no,
      this.fav_color,
      this.fav_brand,
      this.relation_status_emg,
      this.full_name_id_emg,
      this.full_name_emg,
      this.degree_emg,
      this.email_emg,
      this.handphone_emg,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      this.flag_id_lifetime,
      this.no_wa,
      this.no_wa_2,
      this.no_wa_3
      );
}
