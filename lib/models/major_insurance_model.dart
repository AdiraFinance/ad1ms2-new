import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/models/type_model.dart';

import 'assurance_model.dart';
import 'company_model.dart';
import 'coverage1_model.dart';
import 'coverage2_model.dart';
import 'insurance_type_model.dart';

class MajorInsuranceModel{
    final InsuranceTypeModel insuranceType;
    final String numberOfColla;
    final CompanyModel company;
    final ProductModel product;
    final String periodType;
    final String type;
    final CoverageModel coverage1;
    final CoverageModel coverage2;
    final String coverageType;
    final String coverageValue;
    final String upperLimit;
    final String lowerLimit;
    final String upperLimitRate;
    final String lowerLimitRate;
    final String priceCash;
    final String priceCredit;
    final String totalPriceSplit;
    final String totalPriceRate;
    final String totalPrice;

    MajorInsuranceModel(this.insuranceType, this.numberOfColla, this.company, this.product, this.periodType, this.type, this.coverage1, this.coverage2, this.coverageType, this.coverageValue, this.upperLimit, this.lowerLimit, this.upperLimitRate, this.lowerLimitRate, this.priceCash, this.priceCredit, this.totalPriceSplit, this.totalPriceRate, this.totalPrice);
}