import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';

class CompanyPemegangSahamPribadiAlamatModel {
  final JenisAlamatModel jenisAlamatModel;
  final KelurahanModel kelurahanModel;
  final String address;
  final String rt;
  final String rw;
  final String phoneAreaCode;
  final String phone;
  final bool isSameWithIdentity;

  CompanyPemegangSahamPribadiAlamatModel(this.jenisAlamatModel, this.kelurahanModel, this.address, this.rt, this.rw, this.phoneAreaCode, this.phone, this.isSameWithIdentity);
}
