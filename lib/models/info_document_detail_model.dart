import 'dart:io';

class InfoDocumentDetailModel{
    final String fileName;
    final File file;

    InfoDocumentDetailModel(this.fileName, this.file);
}