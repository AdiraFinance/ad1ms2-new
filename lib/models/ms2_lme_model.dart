class MS2LMEModel {
  final String ac_oid;
  final String ac_lme_id;
  final String ac_appl_no;
  final String ac_flag_eligible;
  final int ac_disburse_type;
  final String ac_product_id;
  final String ac_product_desc;
  final int ac_installment_amount;
  final int ac_ph_amount;
  final String ac_voucher_code;
  final int ac_tenor;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final String customer_grading;
  final String jenis_penawaran;
  final String jenis_penawaran_desc;
  final int urutan_pencairan;
  final String jenis_opsi;
  final String jenis_opsi_desc;
  final int max_plafond;
  final int max_tenor;
  final String no_refensi;
  final int max_installment;
  final String portfolio;

  MS2LMEModel(
    this.ac_oid,
    this.ac_lme_id,
    this.ac_appl_no,
    this.ac_flag_eligible,
    this.ac_disburse_type,
    this.ac_product_id,
    this.ac_product_desc,
    this.ac_installment_amount,
    this.ac_ph_amount,
    this.ac_voucher_code,
    this.ac_tenor,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
    this.customer_grading,
    this.jenis_penawaran,
    this.jenis_penawaran_desc,
    this.urutan_pencairan,
    this.jenis_opsi,
    this.jenis_opsi_desc,
    this.max_plafond,
    this.max_tenor,
    this.no_refensi,
    this.max_installment,
    this.portfolio,
  );
}
