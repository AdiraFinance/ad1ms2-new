class MS2SvyRsltDtlModel {
  final String order_no;
  final String info_envirnmt;
  final String info_source;
  final String info_source_name;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;

  MS2SvyRsltDtlModel(
    this.order_no,
    this.info_envirnmt,
    this.info_source,
    this.info_source_name,
    this.active,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
  );
}
