import 'package:flutter/cupertino.dart';

class InfoCreditStructureSteppingModel{
  final TextEditingController controllerPercentage;
  final TextEditingController controllerValue;
  bool isEnable;
  final String instalment;

  InfoCreditStructureSteppingModel(this.controllerPercentage,
      this.controllerValue, this.isEnable, this.instalment);
}