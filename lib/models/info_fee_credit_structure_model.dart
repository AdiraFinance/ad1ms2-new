import 'package:ad1ms2_dev/models/fee_type_model.dart';

class InfoFeeCreditStructureModel{
  final FeeTypeModel feeTypeModel;
  final String cashCost;
  final String creditCost;
  final String totalCost;

  InfoFeeCreditStructureModel(this.feeTypeModel, this.cashCost, this.creditCost, this.totalCost);

}