class StreetTypeModel{
  final String id;
  final String name;

  StreetTypeModel(this.id, this.name);
}