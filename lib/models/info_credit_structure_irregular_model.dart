import 'package:flutter/cupertino.dart';

class InfoCreditStructureIrregularModel{
  final TextEditingController controller;
  bool isEnable;
  bool autoValidate;
  final GlobalKey<FormState> keyIrregular;

  InfoCreditStructureIrregularModel(
      this.controller, this.isEnable, this.autoValidate, this.keyIrregular);
}