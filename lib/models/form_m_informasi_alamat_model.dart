class JenisAlamatModel {
  final String KODE;
  final String DESKRIPSI;

  JenisAlamatModel(this.KODE, this.DESKRIPSI);
}

class KelurahanModel {
  final String KEL_ID;
  final String KEL_NAME;
  final String KEC_ID;
  final String KEC_NAME;
  final String KABKOT_ID;
  final String KABKOT_NAME;
  final String PROV_ID;
  final String PROV_NAME;
  final String ZIPCODE;

  KelurahanModel(this.KEL_ID, this.KEL_NAME, this.KEC_NAME, this.KABKOT_NAME,
      this.PROV_NAME, this.ZIPCODE, this.KEC_ID, this.KABKOT_ID, this.PROV_ID);
}

class AddressModel {
  final JenisAlamatModel jenisAlamatModel;
  final KelurahanModel kelurahanModel;
  final String address;
  final String rt;
  final String rw;
  final String areaCode;
  final String phone;
  final bool isSameWithIdentity;
  final Map addressLatLong;
  bool isCorrespondence;

  AddressModel(this.jenisAlamatModel, this.kelurahanModel, this.address,
      this.rt, this.rw, this.areaCode, this.phone, this.isSameWithIdentity, this.addressLatLong, this.isCorrespondence);
}
