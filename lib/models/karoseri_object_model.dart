import 'company_model.dart';
import 'form_m_pekerjaan_model.dart';
import 'karoseri_model.dart';

class KaroseriObjectModel{
    final int PKSKaroseri;
    final CompanyTypeModel company;
    final KaroseriModel karoseri;
    final String jumlahKaroseri;
    final String price;
    final String totalPrice;

  KaroseriObjectModel(this.PKSKaroseri, this.company, this.karoseri, this.jumlahKaroseri, this.price, this.totalPrice);
}