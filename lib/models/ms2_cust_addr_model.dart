class MS2CustAddrModel {
  final String order_no;
  final String koresponden;
  final String matrix_addr;
  final String address;
  final String rt;
  final String rt_desc;
  final String rw;
  final String rw_desc;
  final String provinsi;
  final String provinsi_desc;
  final String kabkot;
  final String kabkot_desc;
  final String kecamatan;
  final String kecamatan_desc;
  final String kelurahan;
  final String kelurahan_desc;
  final String zip_code;
  final String handphone_no;
  final String phone1;
  final String phone1_area;
  final String phone_2;
  final String phone_2_area;
  final String fax;
  final String fax_area;
  final String addr_type;
  final String addr_desc;
  final String latitude;
  final String longitude;
  final String addr_from_map;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;

  MS2CustAddrModel(
      this.order_no,
      this.koresponden,
      this.matrix_addr,
      this.address,
      this.rt,
      this.rt_desc,
      this.rw,
      this.rw_desc,
      this.provinsi,
      this.provinsi_desc,
      this.kabkot,
      this.kabkot_desc,
      this.kecamatan,
      this.kecamatan_desc,
      this.kelurahan,
      this.kelurahan_desc,
      this.zip_code,
      this.handphone_no,
      this.phone1,
      this.phone1_area,
      this.phone_2,
      this.phone_2_area,
      this.fax,
      this.fax_area,
      this.addr_type,
      this.addr_desc,
      this.latitude,
      this.longitude,
      this.addr_from_map,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      );
}
