class MS2ApplTaksasiModel {
  final String order_no;
  final String unit_type;
  final String unit_type_desc;
  final String taksasi_code;
  final String taksasi_desc;
  final double nilai_input_rekondisi;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;

  MS2ApplTaksasiModel (
    this.order_no,
    this.unit_type,
    this.unit_type_desc,
    this.taksasi_code,
    this.taksasi_desc,
    this.nilai_input_rekondisi,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
  );
}