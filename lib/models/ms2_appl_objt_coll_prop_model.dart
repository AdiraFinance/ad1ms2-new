class MS2ApplObjtCollPropModel {
  final String colla_prop_id;
  final int flag_multi_coll;
  final int flag_coll_name_is_appl;
  final String id_type;
  final String id_type_desc;
  final String id_no;
  final String cola_name;
  final String date_of_birth;
  final String place_of_birth;
  final String place_of_birth_kabkota;
  final String place_of_birth_kabkota_desc;
  final String sertifikat_no;
  final String seritifikat_type;
  final String seritifikat_type_desc;
  final String property_type;
  final String property_type_desc;
  final double building_area;
  final double land_area;
  final double dp_guarantee;
  final double max_ph;
  final double taksasi_price;
  final String cola_purpose;
  final String cola_purpose_desc;
  final double jarak_fasum_positif;
  final double jarak_fasum_negatif;
  final double land_price;
  final double njop_price;
  final double building_price;
  final String roof_type;
  final String roof_type_desc;
  final String wall_type;
  final String wall_type_desc;
  final String floor_type;
  final String floor_type_desc;
  final String foundation_type;
  final String foundation_type_desc;
  final String road_type;
  final String road_type_desc;
  final int flag_car_pas;
  final int no_of_house;
  final String guarantee_character;
  final String grntr_evdnce_ownr;
  final String pblsh_sertifikat_date;
  final int pblsh_sertifikat_year;
  final String license_name;
  final String no_srt_ukur;
  final String tgl_srt_ukur;
  final String srtfkt_pblsh_by;
  final String expire_right;
  final String imb_no;
  final String imb_date;
  final double size_of_imb;
  final double ltv;
  final String land_location;
  final double active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;

  MS2ApplObjtCollPropModel (
      this.colla_prop_id,
      this.flag_multi_coll,
      this.flag_coll_name_is_appl,
      this.id_type,
      this.id_type_desc,
      this.id_no,
      this.cola_name,
      this.date_of_birth,
      this.place_of_birth,
      this.place_of_birth_kabkota,
      this.place_of_birth_kabkota_desc,
      this.sertifikat_no,
      this.seritifikat_type,
      this.seritifikat_type_desc,
      this.property_type,
      this.property_type_desc,
      this.building_area,
      this.land_area,
      this.dp_guarantee,
      this.max_ph,
      this.taksasi_price,
      this.cola_purpose,
      this.cola_purpose_desc,
      this.jarak_fasum_positif,
      this.jarak_fasum_negatif,
      this.land_price,
      this.njop_price,
      this.building_price,
      this.roof_type,
      this.roof_type_desc,
      this.wall_type,
      this.wall_type_desc,
      this.floor_type,
      this.floor_type_desc,
      this.foundation_type,
      this.foundation_type_desc,
      this.road_type,
      this.road_type_desc,
      this.flag_car_pas,
      this.no_of_house,
      this.guarantee_character,
      this.grntr_evdnce_ownr,
      this.pblsh_sertifikat_date,
      this.pblsh_sertifikat_year,
      this.license_name,
      this.no_srt_ukur,
      this.tgl_srt_ukur,
      this.srtfkt_pblsh_by,
      this.expire_right,
      this.imb_no,
      this.imb_date,
      this.size_of_imb,
      this.ltv,
      this.land_location,
      this.active,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      );
}