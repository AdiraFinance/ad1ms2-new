class MS2ApplInsuranceModel {
  final String insurance_id;
  final String cola_type;
  final String insurance_type;
  final String company_id;
  final String product;
  final int period;
  final String type;
  final String type_1;
  final String type_2;
  final String sub_type;
  final String sub_type_coverage;
  final double coverage_type;
  final double coverage_amt;
  final double upper_limit_pct;
  final double upper_limit_amt;
  final double lower_limit_pct;
  final double lower_limit_amt;
  final double cash_amt;
  final double credit_amt;
  final double total_split;
  final double total_pct;
  final double total_amt;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final int idx;

  MS2ApplInsuranceModel(
    this.insurance_id,
    this.cola_type,
    this.insurance_type,
    this.company_id,
    this.product,
    this.period,
    this.type,
    this.type_1,
    this.type_2,
    this.sub_type,
    this.sub_type_coverage,
    this.coverage_type,
    this.coverage_amt,
    this.upper_limit_pct,
    this.upper_limit_amt,
    this.lower_limit_pct,
    this.lower_limit_amt,
    this.cash_amt,
    this.credit_amt,
    this.total_split,
    this.total_pct,
    this.total_amt,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
    this.idx,
  );
}
