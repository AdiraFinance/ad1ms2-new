class ListOIDCompanyModel{
  final String AC_CUST_ID;
  final String AC_NPWP_NUMBER;
  final String AC_CUST_NAME;
  final String AC_ESTABILISHED_DATE;
  final String AC_MOTHER_NAME;
  final String AC_PLACE_BIRTH;
  final String AC_ADDRESS;
  final String PARA_CUST_TYPE_ID;
  final String AC_FLAG_SOURCE;
  final String SCORE;
  final String MODIFIED_DATE;
  final String AC_FLAG_DETAIL;
  final String COLUMN_JOIN;
  final String PARA_CUST_NO;
  final String AC_BR_ID;

  ListOIDCompanyModel(this.AC_CUST_ID, this.AC_NPWP_NUMBER, this.AC_CUST_NAME, this.AC_ESTABILISHED_DATE, this.AC_MOTHER_NAME, this.AC_PLACE_BIRTH, this.AC_ADDRESS, this.PARA_CUST_TYPE_ID, this.AC_FLAG_SOURCE, this.SCORE, this.MODIFIED_DATE, this.AC_FLAG_DETAIL, this.COLUMN_JOIN, this.PARA_CUST_NO, this.AC_BR_ID);
}