class FinancingTypeModel {
  final String financingTypeId;
  final String financingTypeName;

  FinancingTypeModel(this.financingTypeId, this.financingTypeName);
}
