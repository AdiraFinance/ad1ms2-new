//class ListOidModel {
//  final String NUMBER_IDENTITY;
//  final String FULLNAME;
//  final String BIRTH_DATE;
//  final String BIRTH_PLACE;
//  final String MOTHER_NAME;
//  final String IDENTITY_ADDRESS;
//  final String SCORE;
//  final String JOIN;
//
//  ListOidModel(this.NUMBER_IDENTITY, this.FULLNAME, this.BIRTH_DATE, this.BIRTH_PLACE, this.MOTHER_NAME, this.IDENTITY_ADDRESS, this.SCORE, this.JOIN);
//}

class ListOidModel{
  final String AC_CUST_ID;
  final String AC_ID_NO;
  final String AC_CUST_NAME;
  final String AC_DATE_BIRTH;
  final String AC_MOTHER_NAME;
  final String AC_PLACE_BIRTH;
  final String AC_ADDRESS;
  final String PARA_CUST_TYPE_ID;
  final String AC_FLAG_SOURCE;
  final String SCORE;
  final String MODIFIED_DATE;
  final String AC_FLAG_DETAIL;
  final String COLUMN_JOIN;
  final String PARA_CUST_NO;
  final String AC_BR_ID;

  ListOidModel(this.AC_CUST_ID, this.AC_ID_NO, this.AC_CUST_NAME, this.AC_DATE_BIRTH, this.AC_MOTHER_NAME, this.AC_PLACE_BIRTH, this.AC_ADDRESS, this.PARA_CUST_TYPE_ID, this.AC_FLAG_SOURCE, this.SCORE, this.MODIFIED_DATE, this.AC_FLAG_DETAIL, this.COLUMN_JOIN, this.PARA_CUST_NO, this.AC_BR_ID);
}