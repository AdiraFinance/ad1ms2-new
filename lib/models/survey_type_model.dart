class SurveyTypeModel{
  final String id;
  final String name;

  SurveyTypeModel(this.id, this.name);
}