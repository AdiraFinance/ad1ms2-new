import 'package:ad1ms2_dev/models/subsidy_type_model.dart';
import 'add_detail_installment_credit_subsidy.dart';
import 'cutting_method_model.dart';
import 'installment_index_model.dart';

class CreditSubsidyModel{
    final String giver;
    final SubsidyTypeModel type;
    final CuttingMethodModel cuttingMethod;
    final String value;
    final String claimValue;
    final String rateBeforeEff;
    final String rateBeforeFlat;
    final String totalInstallment;
    final String installmentIndex;
    final List<AddInstallmentDetailModel> installmentDetail;

    CreditSubsidyModel(this.giver, this.type, this.cuttingMethod, this.value, this.claimValue, this.rateBeforeEff, this.rateBeforeFlat, this.totalInstallment, this.installmentIndex, this.installmentDetail);
}