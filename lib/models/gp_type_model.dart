class GPTypeModel{
  final String id;
  final String name;

  GPTypeModel(this.id, this.name);
}