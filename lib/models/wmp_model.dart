class WMPModel {
  final String amount;
  final String deskripsi;
  final String deskripsiProposal;
  final String isHidden;
  final String noProposal;
  final String type;
  final String  typeSubsidi;

  WMPModel(this.amount, this.deskripsi, this.deskripsiProposal, this.isHidden, this.noProposal, this.type, this.typeSubsidi);
}