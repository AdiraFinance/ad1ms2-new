class MS2ApplObjtCollOtoModel {
  final String colla_oto_id;
  final int flag_multi_coll;
  final int flag_coll_is_unit;
  final int flag_coll_name_is_appl;
  final String id_type;
  final String id_type_desc;
  final String id_no;
  final String colla_name;
  final String date_of_birth;
  final String place_of_birth;
  final String place_of_birth_kabkota;
  final String place_of_birth_kabkota_desc;
  final String group_object;
  final String group_object_desc;
  final String object;
  final String object_desc;
  final String brand_object;
  final String brand_object_desc;
  final String object_type;
  final String object_type_desc;
  final String object_model;
  final String object_model_desc;
  final String object_purpose;
  final String object_purpose_desc;
  final int mfg_year;
  final int registration_year;
  final int yellow_plate;
  final int built_up;
  final String bpkp_no;
  final String frame_no;
  final String engine_no;
  final String police_no;
  final String grade_unit;
  final String utj_facilities;
  final String bidder_name;
  final int showroom_price;
  final String add_equip;
  final int mp_adira;
  final int rekondisi_fisik;
  final int result;
  final int ltv;
  final int dp_jaminan;
  final int max_ph;
  final int taksasi_price;
  final String cola_purpose;
  final String cola_purpose_desc;
  final int capacity;
  final String color;
  final String made_in;
  final String made_in_desc;
  final String serial_no;
  final String no_invoice;
  final String stnk_valid;
  final String bpkp_address;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String bpkb_id_type;
  final String bpkb_id_type_desc;
  final int mp_adira_upld;

  MS2ApplObjtCollOtoModel (
      this.colla_oto_id,
      this.flag_multi_coll,
      this.flag_coll_is_unit,
      this.flag_coll_name_is_appl,
      this.id_type,
      this.id_type_desc,
      this.id_no,
      this.colla_name,
      this.date_of_birth,
      this.place_of_birth,
      this.place_of_birth_kabkota,
      this.place_of_birth_kabkota_desc,
      this.group_object,
      this.group_object_desc,
      this.object,
      this.object_desc,
      this.brand_object,
      this.brand_object_desc,
      this.object_type,
      this.object_type_desc,
      this.object_model,
      this.object_model_desc,
      this.object_purpose,
      this.object_purpose_desc,
      this.mfg_year,
      this.registration_year,
      this.yellow_plate,
      this.built_up,
      this.bpkp_no,
      this.frame_no,
      this.engine_no,
      this.police_no,
      this.grade_unit,
      this.utj_facilities,
      this.bidder_name,
      this.showroom_price,
      this.add_equip,
      this.mp_adira,
      this.rekondisi_fisik,
      this.result,
      this.ltv,
      this.dp_jaminan,
      this.max_ph,
      this.taksasi_price,
      this.cola_purpose,
      this.cola_purpose_desc,
      this.capacity,
      this.color,
      this.made_in,
      this.made_in_desc,
      this.serial_no,
      this.no_invoice,
      this.stnk_valid,
      this.bpkp_address,
      this.active,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.bpkb_id_type,
      this.bpkb_id_type_desc,
      this.mp_adira_upld,
      );
}