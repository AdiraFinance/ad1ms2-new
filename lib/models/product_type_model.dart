class ProductTypeModel {
  final String id;
  final String name;

  ProductTypeModel(this.id, this.name);
}
