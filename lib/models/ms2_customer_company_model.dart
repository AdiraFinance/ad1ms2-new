import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';

class MS2CustomerCompanyModel{
  final String order_no;
  final TypeInstitutionModel typeInstitutionModel;
  final ProfilModel profilModel;
  final String comp_name;
  final String akta_no;
  final String akta_expired_date;
  final String siup_no;
  final String siup_start_date;
  final String tdp_no;
  final String tdp_start_date;
  final String establish_date;
  final String flag_comp;
  final String basic_capital;
  final String paid_capital;
  final SectorEconomicModel sectorEconomicModel;
  final BusinessFieldModel businessFieldModel;
  final String total_emp;
  final String no_of_year_work;
  final String no_of_year_buss;
  final IdentityModel identityModel;
  final String id_no;
  final String full_name_id;
  final String full_name;
  final String alias_name;
  final String degree;
  final String date_of_birth;
  final String place_of_birth;
  final BirthPlaceModel birthPlaceModel;
  final String gender;
  final String gender_desc;
  final String id_date;
  final String id_expire_date;
  final ReligionModel religionModel;
  final OccupationModel occupationModel;
  final String ac_level;
  final String handphone_no;
  final String email;
  final int shrldng_percent;
  final int dedup_score;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final MaritalStatusModel maritalStatusModel;

  MS2CustomerCompanyModel(
      this.order_no,
      this.typeInstitutionModel,
      this.profilModel,
      this.comp_name,
      this.akta_no,
      this.akta_expired_date,
      this.siup_no,
      this.siup_start_date,
      this.tdp_no,
      this.tdp_start_date,
      this.establish_date,
      this.flag_comp,
      this.basic_capital,
      this.paid_capital,
      this.sectorEconomicModel,
      this.businessFieldModel,
      this.total_emp,
      this.no_of_year_work,
      this.no_of_year_buss,
      this.identityModel,
      this.id_no,
      this.full_name_id,
      this.full_name,
      this.alias_name,
      this.degree,
      this.date_of_birth,
      this.place_of_birth,
      this.birthPlaceModel,
      this.gender,
      this.gender_desc,
      this.id_date,
      this.id_expire_date,
      this.religionModel,
      this.occupationModel,
      this.ac_level,
      this.handphone_no,
      this.email,
      this.shrldng_percent,
      this.dedup_score,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      this.maritalStatusModel,
  );
}