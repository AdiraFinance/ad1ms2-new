import 'package:ad1ms2_dev/models/environmental_information_model.dart';
import 'package:ad1ms2_dev/models/recources_survey_info_model.dart';

class ResultSurveyDetailSurveyModel{
  final EnvironmentalInformationModel environmentalInformationModel;
  final ResourcesInfoSurveyModel recourcesInfoSurveyModel;
  final String resourceInformationName;

  ResultSurveyDetailSurveyModel(this.environmentalInformationModel, this.recourcesInfoSurveyModel, this.resourceInformationName);
}