class RehabTypeModel {
  final String id;
  final String name;

  RehabTypeModel(this.id, this.name);
}
