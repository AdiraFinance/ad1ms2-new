class PaymentMethodModel{
  final String id;
  final String name;

  PaymentMethodModel(this.id, this.name);
}