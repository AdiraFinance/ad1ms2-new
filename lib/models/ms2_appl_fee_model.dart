class MS2ApplFeeModel {
  final String fee_id;
  final String fee_type;
  final int fee_cash;
  final int fee_credit;
  final int total_fee;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;

  MS2ApplFeeModel (
    this.fee_id,
    this.fee_type,
    this.fee_cash,
    this.fee_credit,
    this.total_fee,
    this.active,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
  );
}