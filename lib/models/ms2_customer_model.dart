import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';

class MS2CustomerModel{
  final String order_no;
  final String id_obligor;
  final String cust_type;
  final String npwp_address;
  final String npwp_name;
  final String npwp_no;
  final JenisNPWPModel npwpModel;
  final int flag_npwp;
  final String pkp_flag;
  final int is_guarantor;
  final String corespondence;
  final String debitur_code_12;
  final String debitur_code_13;
  final String last_known_cust_state;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;

  MS2CustomerModel(this.order_no, this.id_obligor, this.cust_type, this.npwp_address, this.npwp_name, this.npwp_no, this.npwpModel, this.flag_npwp, this.pkp_flag, this.is_guarantor, this.corespondence, this.debitur_code_12, this.debitur_code_13, this.last_known_cust_state, this.created_date, this.created_by, this.modified_date, this.modified_by, this.active);
}