class PositionModel {
  final String id;
  final String positionName;

  PositionModel(this.id, this.positionName);
}
