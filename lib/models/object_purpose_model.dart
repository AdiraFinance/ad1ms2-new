class ObjectPurposeModel {
  final String id;
  final String name;

  ObjectPurposeModel(this.id, this.name);
}
