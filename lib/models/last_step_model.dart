class LastStepModel{
  final String order_no;
  final String order_date;
  final String cust_name;
  final String last_known_state;
  final String last_known_handle_by;
  final String idx;
  final String source_application;
  final String priority;

  LastStepModel(this.order_no, this.order_date, this.cust_name, this.last_known_state, this.last_known_handle_by, this.idx, this.source_application, this.priority);
}