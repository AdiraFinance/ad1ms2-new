import 'dart:io';

class TypeIdentityModel {
  final String id;
  final String text;

  TypeIdentityModel(this.id, this.text);
}