import 'package:flutter/material.dart';

class TaksasiUnitModel{
  final String TAKSASI_CODE;
  final String TAKSASI_DESC;
  final String NILAI;
  List<bool> STATUS;
  final TextEditingController CONTROLLER;
  bool AUTOVALIDATE;
  final GlobalKey<FormState> KEYTAKSASI;

  TaksasiUnitModel(this.TAKSASI_CODE, this.TAKSASI_DESC, this.NILAI, this.STATUS, this.CONTROLLER, this.AUTOVALIDATE, this.KEYTAKSASI);
}