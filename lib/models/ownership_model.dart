class OwnershipModel{
  final String id;
  final String name;

  OwnershipModel(this.id, this.name);
}