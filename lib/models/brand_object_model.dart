class BrandObjectModel {
  final String id;
  final String name;

  BrandObjectModel(this.id,this.name);
}
