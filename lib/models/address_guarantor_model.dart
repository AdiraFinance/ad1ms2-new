import 'form_m_informasi_alamat_model.dart';

class AddressGuarantorModelIndividiual {
  final JenisAlamatModel jenisAlamatModel;
  final KelurahanModel kelurahanModel;
  final String address;
  final String rt;
  final String rw;
  final String areaCode;
  final String phone;
  final bool isSameWithIdentity;
  bool isCorrespondence;

  AddressGuarantorModelIndividiual(
      this.jenisAlamatModel,
      this.kelurahanModel,
      this.address,
      this.rt,
      this.rw,
      this.areaCode,
      this.phone,
      this.isSameWithIdentity,
      this.isCorrespondence);
}

class AddressGuarantorModelCompany {
  final JenisAlamatModel jenisAlamatModel;
  final KelurahanModel kelurahanModel;
  final String address;
  final String rt;
  final String rw;
  final String phoneArea1;
  final String phone1;
  final String phoneArea2;
  final String phone2;
  final String faxArea;
  final String fax;
  final bool isSameWithIdentity;
  final Map addressLatLong;
  bool isCorrespondence;

  AddressGuarantorModelCompany(
      this.jenisAlamatModel,
      this.kelurahanModel,
      this.address,
      this.rt,
      this.rw,
      this.phoneArea1,
      this.phone1,
      this.phoneArea2,
      this.phone2,
      this.faxArea,
      this.fax,
      this.isSameWithIdentity,
      this.addressLatLong,
      this.isCorrespondence);
}
