class CollateralTypeModel{
  final String id;
  final String name;

  CollateralTypeModel(this.id, this.name);
}