class ResourcesInfoSurveyModel{
  final String PARA_INFORMATION_SOURCE_ID;
  final String PARA_INFORMATION_SOURCE_NAME;

  ResourcesInfoSurveyModel(this.PARA_INFORMATION_SOURCE_ID, this.PARA_INFORMATION_SOURCE_NAME);
}